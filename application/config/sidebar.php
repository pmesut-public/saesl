<?php

defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'Panel' => 'dashboard/index',
	'Comités de calidad' => 'acreditado/index',
	/*'Comités de calidad' => array(
		'Todos'          => 'acreditado/index',
		//'Universidades'  => 'acreditado/universidad',
		//'Institutos'   => 'acreditado/instituto',		
		//'Test users'     => 'acreditado/test',
		//'Lista completa' => 'acreditado/all',
		'Eliminados'     => 'acreditado/eliminados',
	),*/
	'Autoevaluaciones' => 'autoevaluacion/index',
	/*'Autoevaluaciones' => array(
		'Todas'            => 'autoevaluacion/index',
		//'Universidades'    => 'autoevaluacion/universidad',
		//'Institutos'     => 'autoevaluacion/instituto',
		'Cerradas' => 'autoevaluacion/placseas',
	),*/
	/*
	'Reportes' => array(
		'Convocatorias'    => 'reportes/eventos/tipo/1',
		'Seguimientos'     => 'reportes/eventos/tipo/2',
		'ODP Indicador 1'  => 'reportes/odp1',
		'Factores'         => 'reportes/factores/institutos',
		'Resumen'          => 'reportes/summary',
		'Autoevaluaciones' => 'reportes/autoevaluaciones',
  	),
	*/
	'Usuarios'   => 'user',
	'Estándares' => array(
		'Licenciamiento' => 'objeto',
		'Condiciones'               => 'dimension',
		'Componentes'               => 'factor',
		'Indicadores'               => 'estandar',
		'Medios'                    => 'medio',
		'Formatos'                  => 'formato',
		/*'Niveles'                 => 'valoracion',
		'Estados'                   => 'estado',*/
	),
	'Mantenimiento' => array(
		'Subtipos de Instituciones' => 'subtipo',
		'Instituciones'             => 'institucion',
		'Carreras'                  => 'carrera',
		//'Roles'                   => 'role',
		'Modalidades'               => 'modalidad',
		'Eventos'                   => 'evento',
		//'Datos Institucionales'     => 'datos',
	),
	'Configuración' => 'settings',
	/*'Utilitarios'   => 'utils',*/
);

