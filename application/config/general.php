<?php defined('SYSPATH') OR die('No direct script access.');
 
return array(
	'site_shortname'     => 'SAESL',
	'site_title'     => 'Sistema de Autoevaluación de la Educacion Superior',
	'site_url'     => '',
	'admin_mail'     => 'notify@mail.com',
	'noreply_mail'	 => 'noreply@mail.com',
	'admin_password' => 'admin',
);
