<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_Ponderacion extends Model_Saes {
	
    // Tipos de Ponderacion
	const TIPO_BORRADOR = 'borrador';
	const TIPO_CERRADA	= 'cerrada';
	
	public static $tipos = array(
		self::TIPO_BORRADOR => 'Borrador',
		self::TIPO_CERRADA	=> 'Cerrada',
	);
	
	protected $_table_name = 'ponderacion';

	protected $_primary_key = 'pond_id';
    
    protected $_belongs_to = array(
		'oAcreditado' => array(
			'model' => 'Acreditado',
			'foreign_key' => 'acre_id',
		),
	);
	
	
}
