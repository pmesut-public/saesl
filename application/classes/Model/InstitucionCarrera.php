<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_InstitucionCarrera extends Model_Saes {

	protected $_table_name = 'institucion_carrera';

	protected $_primary_key = 'inca_id';

	protected $_belongs_to = array(
		'oCarrera' => array(
			'model' => 'Carrera',
			'foreign_key' => 'carr_id',
		),
		'oInstitucion' => array(
			'model' => 'Institucion',
			'foreign_key' => 'inst_id',
		),
	);
}
