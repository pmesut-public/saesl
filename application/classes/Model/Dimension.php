<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Dimension extends Model_Saes {

	public function labels()
	{
		return array(
			'dime_codigo'      => 'Código',
			'dime_titulo'      => 'Título',
			'dime_descripcion' => 'Descripción',
			'obac_id'          => 'Objeto de Licenciamiento',
			'dime_estado'      => 'Estado',
			'dime_informacion' => 'Información Institucional'
		);
	}
	
	public function rules()
	{
		return array(
			'dime_codigo' => array(
				array('not_empty'),
			),
			'dime_titulo' => array(
				array('not_empty'),
			),
			'obac_id' => array(
				array('not_empty'),
			),
            'dime_estado' => array(
				array('not_empty'),
			),
		);
	}
	
	protected $_table_name = 'dimension';

	protected $_primary_key = 'dime_id';

	protected $_belongs_to = array(
		'oObjetoAcreditacion' => array(
			'model' => 'ObjetoAcreditacion',
			'foreign_key' => 'obac_id',
		),
	);
	
	protected $_has_many = array(
		'aFactor' => array(
			'model' => 'Factor',
			'foreign_key' => 'dime_id',
		),
            'aAutoCondicion' => array(
                    'model' => 'AutoevaluacionCondicion',
                    'foreign_key' => 'dime_id',
		),
	);
	
	public function title()
	{
		return $this->dime_codigo.'. '.$this->dime_titulo;
	}
	
	public function titulo()
	{
		return $this->dime_titulo;
	}
    
    /**
	 * Obtiene las dimensiones
	 */
	public static function get_all()
	{
		return DB::select()
			->from('dimension')
			->where('dime_estado', '=', Model_Factor::STATUS_ACTIVO)
			->execute()
			->as_array('dime_id', 'dime_titulo');
	}
	
	/*public function aCriterioReport($auto_id, $flag1 = FALSE, $flag2 = FALSE)
	{
		$query = ORM::factory('Criterio')
			->select(array(DB::expr('count(esta_id)'), 'total_est'))
			->select(array(DB::expr('coalesce(SUM(if(aude_cumplimiento = 1, 1, 0)), 0)'), 'sum_est'))
			->select(array(DB::expr('coalesce(SUM(if(aude_cumplimiento = 1, aude_aceptacion, 0)), 0)'), 'puntaje_est'))
			->with('oFactor')
			->join('estandar')
				->using('crit_id')
			->join('autoevaluacion_detalle')
				->using('esta_id')
			->where('auto_id', '=', $auto_id)
			->where('dime_id', '=', $this->dime_id)
			->group_by('criterio.crit_id');
		
		if ($flag1)
		{
			$query->where(DB::expr('ifnull(aude_cumplimiento, 0)'), '=', 0);
		}
		if ($flag2)
		{
			$query->where('esta_clasificacion', '=', 'M');
		}
		
		return $query
			->find_all();
	}*/
	
	/*public function aFactorReport($auto_id, $flag1 = FALSE, $flag2 = FALSE)
	{
		$query = $this->aFactor//ORM::factory('Factor')
			//->select(array(DB::expr('count(esta_id)'), 'total_est'))
			//->select(array(DB::expr('coalesce(SUM(if(aude_cumplimiento = 1, 1, 0)), 0)'), 'sum_est'))
			//->select(array(DB::expr('coalesce(SUM(if(aude_cumplimiento = 1, aude_aceptacion, 0)), 0)'), 'puntaje_est'))
			->join('criterio')
				->using('fact_id')
			->join('estandar')
				->using('crit_id')
			->join('autoevaluacion_detalle')
				->using('esta_id')
			->where('auto_id', '=', $auto_id)
			//->where('dime_id', '=', $this->dime_id)
			->group_by('fact_id');
		
		if ($flag1)
		{
			$query->where(DB::expr('ifnull(aude_cumplimiento, 0)'), '=', 0);
		}
		if ($flag2)
		{
			$query->where('esta_clasificacion', '=', $flag2);
		}
		
		return $query
			->find_all();
	}*/
	
	/*public function aEstandarReport($auto_id)
	{
		return ORM::factory('Estandar')
			//->select(array(DB::expr('count(esta_id)'), 'total_est'))
			//->select(array(DB::expr('coalesce(SUM(if(aude_cumplimiento = 1, 1, 0)), 0)'), 'sum_est'))
			//->select(array(DB::expr('coalesce(SUM(if(aude_cumplimiento = 1, aude_aceptacion, 0)), 0)'), 'puntaje_est'))
			->with('oCriterio:oFactor')
			->join('autoevaluacion_detalle')
				->using('esta_id')
			->where('auto_id', '=', $auto_id)
			->where('dime_id', '=', $this->dime_id)
			//->group_by('estandar.crit_id')
			->find_all();
	}*/
	
	public static function get_actividad_relacionada($dime_id, $auto_id)
	{	
		return DB::select()
			->from('autoevaluacion_detalle_actividad')
			->join('autoevaluacion_detalle')->using('aude_id')
			->join('estandar')->using('esta_id')
			->join('factor')->using('fact_id')
			->where('dime_id', '=', $dime_id)
			->where('autoevaluacion_detalle.auto_id', '=', $auto_id)
			->execute();
	}
	
	public static function get_actividad($dime_id, $auto_id)
	{	
		return DB::select()
			->from('autoevaluacion_actividad')
			->join('autoevaluacion_detalle')->using('aude_id')
			->join('estandar')->using('esta_id')
			->join('factor')->using('fact_id')
			->where('dime_id', '=', $dime_id)
			->where('autoevaluacion_detalle.auto_id', '=', $auto_id)
			->execute();
	}

	public function has_estandares()
    {
        $count_estandar = DB::select(DB::expr("COUNT(esta_id) as count"))
            ->from('estandar')
            ->join('factor')->using('fact_id')
            ->where('dime_id', '=', $this->dime_id)
            ->and_where('esta_estado', '=', Model_Saes::STATUS_ACTIVO)
            ->execute()
            ->get('count');

        return $count_estandar > 0 ? TRUE : FALSE;
    }
}
