<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Cumplimiento extends Model_Saes {

	public function labels()
	{
		return array(
			'cump_numero'      => 'Nivel',
			'cump_descripcion' => 'Descripción',
			'cump_condicion'   => 'Condición de las fuentes de verificación',
			'esta_id'          => 'Estándar',
			'cump_estado'      => 'Estado',
		);
	}
	
	public function rules()
	{
		return array(
			/*'cump_codigo' => array(
				array('not_empty'),
			),*/
			'cump_numero' => array(
				array('not_empty'),
			),
			'cump_condicion' => array(
				array('not_empty'),
			),
			'esta_id' => array(
				array('not_empty'),
			),
		);
	}
	
	protected $_table_name = 'cumplimiento';

	protected $_primary_key = 'cump_id';

	protected $_belongs_to = array(
		'oEstandar' => array(
			'model' => 'Estandar',
			'foreign_key' => 'esta_id',
		),
	);
	
}
