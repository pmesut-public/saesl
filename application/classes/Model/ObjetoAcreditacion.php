<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 * @package    IS
 * @author     Procalidad C2 TEAM
 * @copyright  (c) 2018 ProCalidad
 */
class Model_ObjetoAcreditacion extends Model_Saes {

	protected $_table_name  = 'objeto_acreditacion';
	
	protected $_primary_key = 'obac_id';

	protected $_belongs_to = array(
		'oTipoInstitucion' => array(
			'model'       => 'TipoInstitucion',
			'foreign_key' => 'tiin_id',
		),
	);
	protected $_has_many = array(
		'aDimension' => array(
			'model'       => 'Dimension',
			'foreign_key' => 'obac_id',
		),
	);

	const TIPO_UNIVERSIDAD = 1;
	const TIPO_INSTITUTO   = 3;

	public static $tipos_licenciamiento = [
		self::TIPO_UNIVERSIDAD => 'Universidad',
	];

	public static $tipos = array(
		self::TIPO_UNIVERSIDAD => 'Universidad',
		self::TIPO_INSTITUTO   => 'Instituto',
	);

	const OBJETO_SEDE    = '1';
	const OBJETO_CARRERA = '2';

	public static $objetoEvaluacion = array(
		self::OBJETO_SEDE    => 'Sede',
		self::OBJETO_CARRERA => 'Carrera',
	);

	const ACREDITACION_INSTITUCION = 1;
	const ACREDITACION_CARRERA     = 2;

	public static $acreditaciones = array(
		self::ACREDITACION_INSTITUCION => 'Institución',
		self::ACREDITACION_CARRERA     => 'Carrera',
	);

	const FORM_DOC = 22;
	const CARR_SAL = 23;
	const CARR_TEC = 24;
	const OBAC_ID  = 67;

	public static $get_obacs_from_subtipo = array(
		'IEST' => array(self::CARR_SAL, self::CARR_TEC),
		'IESP' => array(self::FORM_DOC),
		'ISE'  => array(self::FORM_DOC, self::CARR_SAL, self::CARR_TEC),
		'ESFA' => array(self::FORM_DOC, self::OBAC_ID),
	);

	public function labels()
	{
		return array(
			'obac_nombre'            => 'Nombre Objeto lecenciamiento',
			'tiin_id'                => 'Tipo de institución',
			'obac_tipo_acreditacion' => 'Tipo de licenciamiento',
			'obac_estado'            => 'Estado',
		);
	}

	public function rules()
	{
		return array(
/*			
			'tiin_id' => array(
				array('not_empty'),
			),
*/
			'obac_nombre' => array(
				array('not_empty'),
			),
			'obac_tipo_acreditacion' => array(
				array('not_empty'),
			),
			'obac_estado' => array(
				array('not_empty'),
			),
		);
	}

	public function objeto_nombre()
	{
		return
			$this->oTipoInstitucion->tiin_nombre . ' - ' . $this->obac_nombre;
	}

	/*
		public static function get_total_standard($obac_id)
		{
			return DB::select(array(DB::expr("count(esta_id)"), 'count'))
			->from('dimension')
			->join('factor')->using('dime_id')
			->join('criterio')->using('fact_id')
			->join('estandar')->using('crit_id')
			->where('obac_id', '=', $obac_id)
			->where('esta_estado', '=', 'activo')
			->execute()
			->get('count');
	  	}
	 */

	/**
	 * Obtiene todas las dimensiones relacionadas al tipo de institucion.
	 *
	 * @param  tiin_id
	 * @return array
	 */
	public static function get_dimensiones($tiin_id)
	{
		return DB::select('a.*')
				->select(DB::expr("concat(dime_codigo, '. ', dime_titulo) dime_title"))
				->from(DB::expr("dimension a"))
				->where('a.obac_id', '=', $tiin_id)
				->order_by('dime_codigo')
				->execute()
				->as_array('dime_id', 'dime_title');
	}

	/**
	 * Obtiene todos los factores relacionados al tipo de institucion.
	 * 
	 * @param  tiin_id
	 * @return array
	 */
	public static function get_factores($tiin_id)
	{
		return DB::select('b.*')
				->select(DB::expr("concat(fact_codigo, '. ', fact_titulo) fact_title"))
				->from(DB::expr("factor b"))
				->join(DB::expr("dimension a"))
				->using('dime_id')
				->where('a.obac_id', '=', $tiin_id)
				->order_by(DB::expr('abs(fact_codigo)'))
				->execute()
				->as_array('fact_id', 'fact_title');
	}

	/**
	 * Obtiene todos los criterios relacionados al objeto de acreditacion.
	 *
	 * @param  obac_id
	 * @return array
	 */
	public static function get_criterios($obac_id)
	{
		return DB::select('c.*')
				->select(DB::expr("concat(crit_codigo, '. ', coalesce(crit_descripcion)) crit_title"))
				->from(DB::expr("criterio c"))
				->join(DB::expr("factor b"))
				->using('fact_id')
				->join(DB::expr("dimension a"))
				->using('dime_id')
				->where('a.obac_id', '=', $obac_id)
				->order_by(DB::expr('abs(crit_codigo)'))
				->execute()
				->as_array('crit_id', 'crit_title');
	}

	/**
	 * Obtiene todos los estandares relacionados al tipo de institucion.
	 *
	 * @param  obac_id
	 * @return array
	 */
	public static function get_estandares($obac_id)
	{	
		return DB::select('d.*')
				->select(DB::expr("concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta_title"))
				->from(DB::expr("estandar d"))
				->join(DB::expr("factor b"))
				->using('fact_id')
				->join(DB::expr("dimension a"))
				->using('dime_id')
				->where('a.obac_id', '=', $obac_id)
				->order_by(DB::expr('abs(esta_codigo)'))
				->execute()
				->as_array('esta_id', 'esta_title');
	}

	/**
	 * Obtiene los objetos de licenciamiento concatenado con el tipo de institucion.
	 *
	 * @param  $tiin_id
	 * @return array
	 */
	public static function get_options($tiin_id = NULL)
	{
		$query = DB::select('o.*')
			->from(DB::expr('objeto_acreditacion o'))
			->where('obac_estado', '=', Model_Saes::STATUS_ACTIVO);

		if ($tiin_id)
		{
			$query->where('tiin_id', '=', $tiin_id);
		}

		return $query
				->order_by('obac_nombre')
				->execute()
				->as_array('obac_id', 'obac_nombre');
	}

	/**
	 * Obtiene los objetos de licenciamiento concatenado con el tipo de institucion.
	 *
	 * @param  $tiin_id
	 * @return array
	 */
	public static function get_tipos($tiin_id = NULL)
	{
		$query = DB::select('o.*')
			->select(DB::expr("concat(tiin_nombre, ' - ', obac_nombre) objeto"))
			->from(DB::expr('objeto_acreditacion o'))
			->join(DB::expr('tipo_institucion ti'))
			->using('tiin_id')
			->where('obac_estado', '=', Model_Saes::STATUS_ACTIVO);

		if ($tiin_id)
		{
			$query->where('tiin_id', '=', $tiin_id);
		}

		return $query
				->order_by('tiin_id')
				->order_by('objeto')
				->execute()
				->as_array('obac_id', 'objeto');
	}

	/**
	 * Obtiene los Objetos de Acreditación (Sede - Carrera) de una Institución.
	 * 
	 * @param  integer $inst_id
	 * @param  array   $as_array
	 * @return array
	 */
	public static function get_obac_from_inst($inst_id, $as_array = TRUE)
	{
		$oInstitucion = ORM::factory('Institucion', $inst_id);

		if (!$oInstitucion->loaded()){ return array(); }
          
        $query_one = DB::select('o.*')
            ->from(array('objeto_acreditacion', 'o'))
            ->select(DB::expr("concat({$inst_id}, '-',obac_id) inst_obac"))
            ->join('carrera')->on('carr_obac_id', '=','obac_id')
            ->where('inst_id', '=', $inst_id)
            ->and_where('obac_estado', '=', Model_Saes::STATUS_ACTIVO)
            ->and_where('carr_estado', '=', Model_Saes::STATUS_ACTIVO)
            ->group_by('obac_tipo_acreditacion');

        $query = DB::select('o.*')
            ->from(array('objeto_acreditacion', 'o'))
            ->select(DB::expr("concat({$inst_id}, '-',obac_id) inst_obac"))
            ->join('institucion')->on('inst_obac_id', '=', 'o.obac_id')
            ->where('obac_estado', '=', Model_Saes::STATUS_ACTIVO)
            ->and_where('inst_id', '=', $inst_id)
            ->union($query_one)
            ->as_object()
            ->execute();
        
        return $query;
	}

	/**
	 * Obtiene los objeto de acreditación por tipo de Institución, tipo de acreditación.
	 *
	 * @param  integer $tiin_id
	 * @param  integer $tipo_acreditacion
	 * @param  array   $obac_ids
	 * @return array
	 */
	public static function get_all($tiin_id = NULL, $tipo_acreditacion = NULL, array $obac_ids = [])
   	{

        $query = ORM::factory('ObjetoAcreditacion');

        if (!empty($tiin_id)){

            $query->where('tiin_id', '=', $tiin_id);
        }

        if (!empty($tipo_acreditacion)){

            $query->where('obac_tipo_acreditacion', '=', $tipo_acreditacion);
        }

        $query = $query->where_open();

        $query = $query->where('obac_estado', '=', 'activo');

        (count($obac_ids) > 0) ? $query = $query->or_where('obac_id', 'IN', $obac_ids) : null;
        
        $query = $query->where_close();
        
        return $query->find_all()->as_array('obac_id', 'obac_nombre');
    }

}
