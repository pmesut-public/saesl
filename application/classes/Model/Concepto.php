<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Concepto extends Model_Saes {

	public function labels()
	{
		return array(
			'conc_codigo'      => 'Código',
			'conc_nombre'      => 'Nombre',
			'esta_id'          => 'Estándar',
			'conc_estado'      => 'Estado',
			'conc_obligatorio' => 'Obligatorio',
		);
	}
	
	public function rules()
	{
		return array(
			'conc_codigo' => array(
				array('not_empty'),
			),
			'esta_id' => array(
				array('not_empty'),
			),
		);
	}
	
	protected $_table_name = 'concepto';

	protected $_primary_key = 'conc_id';

	protected $_belongs_to = array(
		'oEstandar' => array(
			'model' => 'Estandar',
			'foreign_key' => 'esta_id',
		),
	);
	
	public function required()
	{
		return $this->conc_obligatorio ? 'Sí' : 'No';
	}
	
	public static function get_count($prefix, $obac_id, $moda_id)
	{
		return DB::select(DB::expr("{$prefix}_codigo, count(conc_id) count
				from dimension
				join factor using(dime_id)
				join criterio using(fact_id)
				join estandar using(crit_id)
				join concepto using(esta_id)
				where obac_id = {$obac_id}
				and moda_id <= {$moda_id}
				group by {$prefix}_id"))
			->execute()
			->as_array("{$prefix}_codigo", 'count');
	}
	
}
