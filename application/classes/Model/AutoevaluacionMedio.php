<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_AutoevaluacionMedio extends Model_Saes {
	
	protected $_table_name = 'autoevaluacion_medio';

	protected $_primary_key = 'aume_id';

	protected $_belongs_to = array(
		'oMedio' => array(
			'model' => 'Medio',
			'foreign_key' => 'medi_id',
		),
		'oDetalle' => array(
			'model' => 'AutoevaluacionDetalle',
			'foreign_key' => 'aude_id',
		),
	);
        
        protected $_has_many = array(
		'aAutoevaluacionDocumento' => array(
			'model' => 'AutoevaluacionDocumento',
			'foreign_key' => 'aume_id',
		),
	);
	
	
}
