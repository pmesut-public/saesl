<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @package    ProCalidad
 * @author     Procalidad C2 Team
 * @copyright  2018 ProCalidad
 */
class Model_Departamento extends Model_Saes
{
	/**
	 * Tabla asociada al Modelo.
	 */
	protected $_table_name = 'departamento';

	/**
	 * Primary Key (PK) del Modelo.
	 */
    protected $_primary_key = 'depa_id';

    /**
     * Obtiene todas las regiones (sin duplicidad) de la tabla institucion. Ejem. ['LIMA' => 'LIMA']
     *
     * @return array
     */
    public static function get_regiones() {

        return ORM::factory('Departamento')->find_all()->as_array('depa_nombre', 'depa_nombre');
    }

}