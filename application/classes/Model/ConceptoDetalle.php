<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_ConceptoDetalle extends Model_Saes {

	protected $_upload = 'uploads/autoevaluaciones/';
	
	protected $_table_name = 'concepto_detalle';

	protected $_primary_key = 'code_id';

	public static $options = array(
		1 => 'Muy adecuado',
		2 => 'Medianamente adecuado',
		3 => 'Poco adecuado',
	);
	
	protected $_belongs_to = array(
		'oAutoevaluacionDetalle' => array(
			'model' => 'AutoevaluacionDetalle',
			'foreign_key' => 'aude_id',
		),
		'oConcepto' => array(
			'model' => 'Concepto',
			'foreign_key' => 'conc_id',
		),
	);
	
	/*public function alt_checked()
	{
		return ($this->code_id AND $this->code_nombre != $this->conc_nombre);
	}*/
	
	public function acc_checked()
	{
		return ($this->code_id AND $this->code_cumple);
	}
	
	public function prefix()
	{
		return 'concepto['.$this->conc_id.']';
	}
	
	public function nombre()
	{
		return $this->code_nombre ? $this->code_nombre : $this->conc_nombre;
	}
	
	public function calidad()
	{
		$types = self::$options;
		
		return empty($types[$this->code_calidad]) ? '-' : $types[$this->code_calidad];
	}
	
	public function upload_concepto(Model_Autoevaluacion $oAutoevaluacion, $conc_id, $file)
	{
		if ( ! $this->_check_file($file))
			return FALSE;
		
		$dir = "{$this->_upload}{$oAutoevaluacion->auto_id}/";
		$full_dir = APPPATH.$dir;
		
		if ( ! is_dir($full_dir))
		{
			mkdir($full_dir);
		}
		
		$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
		$filename = "{$conc_id}-".date('YmdHis').".{$ext}";
		
        if ( ! Upload::save($file, $filename, $full_dir))
        {
            return FALSE;
        }
 
		//$this->{$column} = $dir.$filename;
		return $dir.$filename;
	}
	
}
