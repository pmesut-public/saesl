<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_AcreditadoObjeto extends Model_Saes {

	protected $_table_name = 'acreditado_objeto';

	protected $_primary_key = 'acob_id';

	protected $_belongs_to = array(
		'oCarrera' => array(
			'model' => 'Carrera',
			'foreign_key' => 'carr_id',
		),
		'oAcreditadoInstitucion' => array(
			'model' => 'AcreditadoInstitucion',
			'foreign_key' => 'acin_id',
		),
	);
}
