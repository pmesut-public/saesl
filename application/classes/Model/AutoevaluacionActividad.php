<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AutoevaluacionActividad extends Model_Saes {

    protected $_table_name = 'autoevaluacion_actividad';
    protected $_primary_key = 'auac_id';
	
	protected $_belongs_to = array(
		'oActDetalle' => array(
			'model' => 'AutoevaluacionDetalle',
			'foreign_key' => 'aude_id',
		)
	);
	
    protected $_has_many = array(
        'aAutoeveluacionDetalleActividad' => array(
            'model' => 'AutoevaluacionDetalleActividad',
            'foreign_key' => 'auac_id'
        ),
    );

    public function get_estandares()
    {
        $array_estandar = [];

        $aEstandares = DB::select("aude_id")
            ->from('autoevaluacion_detalle_actividad')
            ->where("auac_id", '=', $this->auac_id)
            ->execute()
            ->as_array();

        foreach ($aEstandares as $oEstandares)
        {
            array_push($array_estandar, $oEstandares['aude_id']);
        }

        return $array_estandar;
    }

    public static function get_by_esta_id($aude_id)
    {
        return ORM::factory('AutoevaluacionActividad')
                ->where('aude_id', '=', $aude_id)
                ->find_all();
    }

    public static function get_relacionado_by_esta_id($aude_id)
    {
		return ORM::factory('AutoevaluacionActividad')
			->select(DB::expr('ada.aude_id aude_relacionada'), 'ada.auda_avance', 'ada.auda_importancia_factor')
			->join(DB::expr('autoevaluacion_detalle_actividad ada'))->using('auac_id')
			->where('ada.aude_id', '=', $aude_id)
			->find_all();
    }

    /*public static function get_estandares_relacionados($auac_id = FALSE)
    {
        $actividad_id = $auac_id ? $auac_id : $this->auac_id;

        $oEstandarPadre = ORM::factory('Estandar')
            ->join('autoevaluacion_detalle')->using('esta_id')
            ->join('autoevaluacion_actividad')->using('aude_id')
            ->where('auac_id', '=', $actividad_id)
            ->find();

        $estandaresRelacionados = DB::select(array(DB::expr('GROUP_CONCAT(CONCAT("E", esta_codigo) ORDER BY esta_codigo*1 ASC SEPARATOR ",")'), 'estandares'))
            ->from('estandar')
            ->join('autoevaluacion_detalle')->using('esta_id')
            ->join('autoevaluacion_detalle_actividad')->using('aude_id')
            ->where('auac_id', '=', $actividad_id)
			->order_by(DB::expr('CAST(esta_codigo AS DECIMAL)'), 'ASC')
            ->execute()
            ->get('estandares');
		
        if (empty($estandaresRelacionados))
            return 'E' . $oEstandarPadre->esta_codigo;
        else
            return 'E' . $oEstandarPadre->esta_codigo . ',' . $estandaresRelacionados;
    }*/
	
	public function get_tipo_relación()
	{
		$oEstandar = ORM::factory('Estandar')
			->join('autoevaluacion_detalle')->using('esta_id')
			->where('aude_id', '=', $this->aude_relacionada)
			->find();
		
		if ($oEstandar->fact_id == $this->oActDetalle->oEstandar->fact_id)
			return 'D';
		else
			return 'R';
	}

}
