<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_Medio extends Model_Saes {


	// Estados
	const ACTIVO = 'activo';
	const ELIMINADO = 'eliminado';
    const PROCESO = 'proceso';
	const FINALIZADO   = 'finalizado';
	const REVISION   = 'revision';
    const VALIDADO = 'validado';
	const OBSERVADO = 'observado';
        
	public static $estadosRegular = array(
		self::PROCESO => 'Proceso',
		self::FINALIZADO   => 'Finalizado',
	);
        
	public static $estadosOperador = array(
		self::REVISION => 'Revisión',
		self::VALIDADO => 'Validado',
		self::OBSERVADO   => 'Observado',
	);


	public function labels()
	{
		return array(
			'medi_descripcion' => 'Descripción',
			'obac_id' => 'Objeto de Licenciamiento',
			'esta_id' => 'Indicador',
			'medi_estado' => 'Estado',
			'medi_observacion' => 'Observacion',
			'medi_consideracion' => 'Consideracion',
		);
	}

	public function rules()
	{
		return array(
			'esta_id' => array(
				array('not_empty'),
			),
			'medi_estado' => array(
				array('not_empty'),
			),
		);
	}

	public function filters()
	{
		return array(
			'medi_descripcion' => array(
				array('Model_Saes::filter_empty'),
			),
		);
	}

	protected $_table_name = 'medio';
	protected $_primary_key = 'medi_id';
	protected $_belongs_to = array(
		'oEstandar' => array(
			'model' => 'Estandar',
			'foreign_key' => 'esta_id',
		),
	);
	protected $_has_many = array(
		'aAutoMedio' => array(
			'model' => 'AutoevaluacionDetalle',
			'foreign_key' => 'medi_id',
		),
	);

	public function title()
	{
		return $this->medi_descripcion;
	}

	public function titulo()
	{
		return $this->medi_descripcion;
	}

}
