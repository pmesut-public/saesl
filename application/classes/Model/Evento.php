<?php

defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Evento extends Model_Saes {

	const TIPO_CONVOCATORIA = 1;
	const TIPO_SEGUIMIENTO = 2;

	public static $tipos = array(
		self::TIPO_CONVOCATORIA => 'Convocatoria',
		self::TIPO_SEGUIMIENTO => 'Seguimiento',
	);

	const OBJETO_MEJORA = 1;
	const OBJETO_LOGRO = 2;

	public static $objetos = array(
		self::OBJETO_MEJORA => 'Mejora del Estándar',
		self::OBJETO_LOGRO => 'Logro del Estándar',
	);

	const PONDERACION_SI = 1;
	const PONDERACION_NO = 0;
	const VINCULACION_SI = 1;
	const VINCULACION_NO = 0;

	public static $ponderacion = array(
		self::PONDERACION_SI => 'Si',
		self::PONDERACION_NO => 'No',
	);
	public static $vinculacion = array(
		self::VINCULACION_SI => 'Si',
		self::VINCULACION_NO => 'No',
	);

	public function labels()
	{
		return array(
			'even_nombre' => 'Nombre',
			'even_alias' => 'Alias (6 chars)',
//			'even_objeto' => 'Objeto de autoevaluación',
			'even_tipo' => 'Tipo',
			'tiin_id' => 'Tipo institución',
			'even_fecha_inicio' => 'Inicio',
			'even_fecha_fin' => 'Fin',
			'even_per_minimo' => 'Porcentaje mínimo de aprobación',
			'even_estado' => 'Estado'
		);
	}

	public static function get_additional_labels()
	{
		return array(
			'tipo_gestion' => 'Tipo de gestión',
		);
	}

	public function rules()
	{
		return array(
			'even_fecha_inicio' => array(
				array('date'),
			),
			'even_fecha_fin' => array(
				array('date'),
			),
			'even_estado' => array(
				array('not_empty'),
			),
		);
	}

	protected $_table_name = 'evento';
	protected $_primary_key = 'even_id';
	protected $_belongs_to = array(
		'oTipoInstitucion' => array(
			'model' => 'TipoInstitucion',
			'foreign_key' => 'tiin_id',
		),
	);
	protected $_has_many = array(
		'aModalidad' => array(
			'model' => 'Modalidad',
			'foreign_key' => 'even_id',
			'through' => 'evento_modalidad',
			'far_key' => 'moda_id',
		),
		'aAcreditadoEvento' => array(
			'model' => 'AcreditadoEvento',
			'foreign_key' => 'even_id',
		),
		'aTipoGestion' => array(
			'model' => 'TipoGestion',
			'foreign_key' => 'even_id',
			'through' => 'evento_tipo_gestion',
			'far_key' => 'tige_id',
		),
		/* 'aTipoFinanciamiento' => array(
		  'model' => 'TipoFinanciamiento',
		  'foreign_key' => 'even_id',
		  'through' => 'evento_tipo_financiamiento',
		  'far_key' => 'tifi_id',
		  ), */
		'aEventoConvocatoria' => array(
			'model' => 'Evento',
			'foreign_key' => 'even_segu_id',
			'through' => 'evento_relacionado',
			'far_key' => 'even_conv_id',
		),
	);

	/**
	 * Retorna los eventos filtrados de acuerdo al tiin_id
	 * @param tiin_id
	 * @param type
	 * @return array de eventos
	 */
	public static function get_all($tiin_id = NULL, $type = NULL)
	{

		$query = DB::select()
			->from('evento');
		//->where('even_estado', '=', Model_Saes::STATUS_ACTIVO)

		if ($tiin_id)
		{
			$query->where('tiin_id', '=', $tiin_id);
		}

		if ($type)
		{
			$query->where('even_tipo', '=', $type);
		}

		return $query
				->execute()
				->as_array('even_id', 'even_nombre');
	}
        
        public static function get_evento($tiin_id = NULL)
        {
            $query = ORM::factory('Evento')
                    ->where('even_estado', '=', Model_Saes::STATUS_ACTIVO);
 
                if ($tiin_id)
		{
                    $query->where('tiin_id', '=', $tiin_id);
		}

		return $query->limit('1')
                    ->find();
        }

        /**
	 * Setea al campo even_id el valor (0)
	 */
	public function is_libre()
	{
		return ($this->even_id == Model_Evento::EVEN_LIBRE);
	}

	public function mejora()
	{
		return self::OBJETO_MEJORA;
	}

	public function logro()
	{
		return self::OBJETO_LOGRO;
	}

}
