<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_UserLog extends Model_Saes {

	const INSERT = 1;
	const UPDATE = 2;
	const DELETE = 3;
	
	public function labels()
	{
		return array(
			'user_id' => 'Usuario',
			'type' => 'Tipo',
			'table' => 'Tabla',
			'row' => 'Fila(s)',
			'datetime' => 'Fecha',
			'comments' => 'Descripción',
		);
	}
	
	protected $_table_name = 'user_logs';

	protected $_primary_key = 'id';
	
}
