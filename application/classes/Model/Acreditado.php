<?php defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Acreditado extends Model_Saes
{

    /**
     * Tabla asociada al Modelo.
     */
    protected $_table_name = 'acreditado';

    /**
     * Primary Key (PK) del Modelo.
     */
    protected $_primary_key = 'acre_id';

    // Registros
    const REGISTRO_INSTITUCION = 'institucion';
    const REGISTRO_CARRERA     = 'carrera';

    public static $registros = array(
        self::REGISTRO_INSTITUCION  => 'Institución',
        self::REGISTRO_CARRERA      => 'Carrera',
    );

    // Estados
    const ESTADO_REGISTRADO = 5;
    const ESTADO_ACTIVO     = 1;
    const ESTADO_SUSPENDIDO = 0;

    public static $estados = array(
        self::ESTADO_REGISTRADO => 'Por aprobar',
        self::ESTADO_ACTIVO     => 'Aprobado',
        self::ESTADO_SUSPENDIDO => 'Eliminado',
    );

    // Tipos de usuario
    const TIPO_REAL = 0;
    const TIPO_TEST = 1;

    public static $tipos = array(
        self::TIPO_REAL => 'Reales',
        self::TIPO_TEST => 'De prueba',
    );

    // Tipo de registro
    const TIPO_REG_REGULAR = 0;
    const TIPO_REG_EXTERNO = 1;

    public static $tipos_registro = array(
        self::TIPO_REG_REGULAR => 'Regular',
        self::TIPO_REG_EXTERNO => 'Externo',
    );

    // Labels
    public function labels()
    {
        return array(
            //'objeto'        => 'Acreditación',
            'obac_id'         => 'Licenciamiento',
            'inst_name'       => 'Institución',
            'carr_nombre'     => 'Carrera',
            //'acre_registro' => 'Tipo de registro',
            'username'        => 'Username',
            'email'           => 'Email',
        );
    }

    //protected $_upload = 'uploads/documents/';

    protected $_oContacto;

    protected $_load_with = array('oUser', 'oObjetoAcreditacion');

    protected $_belongs_to = array(
        'oObjetoAcreditacion' => array(
            'model'       => 'ObjetoAcreditacion',
            'foreign_key' => 'obac_id',
        ),
        'oModalidad' => array(
            'model'       => 'Modalidad',
            'foreign_key' => 'moda_id',
        ),
        'oUser' => array(
            'model'       => 'User',
            'foreign_key' => 'acre_id',
        ),
    );

    protected $_has_many = array(
        'aPersona' => array(
            'model'       => 'Persona',
            'foreign_key' => 'acre_id',
        ),
        'aFile' => array(
            'model'       => 'Documento',
            'foreign_key' => 'acre_id',
        ),
        'aDocumento' => array(
            'model'       => 'AcreditadoDocumento',
            'foreign_key' => 'acre_id',
        ),
        'aMensaje' => array(
            'model'       => 'Mensaje',
            'foreign_key' => 'acre_id',
            'through'     => 'acreditado_mensajes',
            'far_key'     => 'mens_id',
        ),
        'aEvento' => array(
            'model'       => 'Evento',
            'foreign_key' => 'acre_id',
            'through'     => 'acreditado_evento',
            'far_key'     => 'even_id',
        ),
        'aAcreditadoEvento' => array(
            'model'       => 'AcreditadoEvento',
            'foreign_key' => 'acre_id',
        ),
        'aAcreditadoInstitucion' => array(
            'model'       => 'AcreditadoInstitucion',
            'foreign_key' => 'acre_id',
        ),
        'aPonderacion' => array(
            'model'       => 'Ponderacion',
            'foreign_key' => 'acre_id',
        ),
    );

    /**
     * Verifica si un documento esta activo
     */
    public function documento()
    {
        return $this
            ->aDocumento
            ->where('acdo_tipo', '=', 'activo')
            ->find()
            ->acdo_documento;
    }

    /**
     * Obtiene todos los documentos pendientes
     */
    public function documento_pendiente()
    {
        return $this->aDocumento
            ->where('acdo_tipo', '=', 'pendiente')
            ->find()
            ->acdo_documento;
    }

    /**
     * Obtiene el contacto de un comite
     */
    public function oContacto()
    {
        if (!$this->_oContacto) {
            $this->_oContacto = $this->aPersona->where('pers_tipo', '=', 'contacto')->find();
        }

        return $this->_oContacto;
    }

    /**
     * Obtiene las personas del comite de acuerdo al tipo (Contacto, Comision)
     */
    public function aComite()
    {
        return $this
            ->aPersona
            ->where('pers_tipo', 'in', array_keys(Model_Persona::$tipos))
            ->find_all();
    }

    /**
     * Retorna la comision que está pendiente por aprobar (pers_tipo = 'pending_contacto'
     * || pers_tipo = 'pending_comision')
     * @return type
     */
    public function aComitePending()
    {
        return $this
            ->aPersona
            ->where('pers_tipo', 'like', '%pending%')
            ->find_all();
    }

    /**
     * Obtiene la institucion relacionada al comite
     */
    public function oInstitucion()
    {
        $concat = "concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede)";

        return ORM::factory('AcreditadoInstitucion')
            ->select(DB::expr("$concat name"))
            ->with('oInstitucion')
            ->where('acre_id', '=', $this->pk())
            ->find();
    }

    /**
     * Obtiene objeto institucion relacionada al comite
     */
    public function get_oInstitucion()
    {
        return ORM::factory('AcreditadoInstitucion')
            ->select(DB::expr("*"))
            ->with('oInstitucion')
            ->where('acre_id', '=', $this->pk())
            ->find();
    }

    /**
     * Obtiene las carreras relacionadas al comite.
     */
    public function oCarrera()
    {
        return
            ORM::factory('AcreditadoObjeto')
                ->with('oCarrera')
                ->join('acreditado_institucion')->using('acin_id')
                //->on('acreditado_institucion.acin_id', '=', 'acob.acin_id')
                ->where('acre_id', '=', $this->pk())
                ->find();
    }

    /**
     * Agrega titulos
     */
    public function add_titles()
    {
        $this
            ->select(array(DB::expr("if(ao.acob_id is NULL,
				inst_nombre, 
				concat('Carrera de ', carr_nombre, ' de ', inst_nombre))"), 'subtitle'))
            ->join(DB::expr('acreditado_institucion ai'))->using('acre_id')
            ->join(DB::expr('institucion i'))->using('inst_id')
            ->join(DB::expr('acreditado_objeto ao'), 'left')->using('acin_id')
            ->join(DB::expr('carrera c'), 'left')->using('carr_id');

        return $this;
    }

    /**
     * Titulo de vista en el proceso de autoevaluacion
     */
    public function title()
    {

        $greeting = 'Bienvenidos, Sres. del Comité de Calidad de la '. $this->oInstitucion()->oInstitucion->inst_nombre;

        if ($this->oObjetoAcreditacion->obac_tipo_acreditacion == Model_ObjetoAcreditacion::ACREDITACION_CARRERA && $this->oCarrera()->loaded()) {

            $greeting .= ' - ' . $this->oCarrera()->oCarrera->carr_nombre;
        }
        else{

        }

        return $greeting;

        //return $greeting . $this->subtitle;
    }

    /**
     * Retorna el subtitulo
     */
    public function subtitle()
    {
        return $this->subtitle;
    }

    /**
     * Retorna el acre_id
     */
    public function acreditadoId()
    {
        return $this->acre_id;
    }


    /**
     * Obtiene la autoevaluacion activa
     * @return  Model_Autoevaluacion
     */
    public function oAutoevaluacionActiva()
    {
        return ORM::factory('Autoevaluacion')
            ->select(DB::expr('acreditado_evento.acre_id as acre_id'))
            ->join('acreditado_evento')->using('acev_id')
            ->where('acreditado_evento.acre_id', '=', $this->acre_id)
            ->where('auto_estado', '=', Model_Autoevaluacion::STATUS_ACTIVO)
            ->find();
    }

    /**
     * Obtiene la ultima autoevaluacion cerrada
     * @return  Model_Autoevaluacion
     */
    public function oAutoevaluacionCerrada()
    {
        return ORM::factory('Autoevaluacion')
            ->select(DB::expr('acreditado_evento.acre_id as acre_id'))
            ->join('acreditado_evento')->using('acev_id')
            ->where('acreditado_evento.acre_id', '=', $this->acre_id)
            ->where('auto_estado', '=', Model_Autoevaluacion::STATUS_CERRADO)
            ->order_by('auto_fecha_act', 'DESC')
            ->find();
    }

    /**
     * Obtiene la ultima autoevaluacion cerrada o abierta
     * @return  Model_Autoevaluacion
     */
    public function oAutoevaluacion()
    {
        $oAuto = $this->oAutoevaluacionActiva();
        if (!isset($oAuto->auto_id)) {
            $oAuto = $this->oAutoevaluacionCerrada();
        }
        return $oAuto;
    }

    /**
     * Valida si el campo "username" esta checkeado
     * @param $values
     * @return Obj Validation
     */
    public static function get_additional_validation($values)
    {
        return Validation::factory($values)
            //->rule('acre_documento', 'not_empty')
            ->rule('username', 'Model_Acreditado::check_registro', array(':validation', ':field'));
    }

    /**
     *
     */
    public static function check_registro($array, $field)
    {
        // Institucional
        if (!$array['carr_id']) {
            $query = strtr(
                'select count(*) count 
				from acreditado a
				join objeto_acreditacion o using(obac_id)
				join users u on a.acre_id = u.id
				join acreditado_institucion au using (acre_id)
				where au.inst_id = :inst_id
				and u.status = :status
				and a.moda_id = :moda_id
				and a.acre_test = 0
				and o.obac_tipo_acreditacion = :acre'
                , array(
                ':inst_id' => $array['inst_id'],
                ':status' => Model_User::STATUS_ACTIVO,
                ':acre' => Model_ObjetoAcreditacion::ACREDITACION_INSTITUCION,
                ':moda_id' => $array['moda_id'],
            ));

            if (DB::query(Database::SELECT, $query)->execute()->get('count')) {
                $array->error($field, 'fail_inst');
            }

        } // Carrera
        else {
            $query = strtr(
                'select count(*) count 
				from acreditado a
				join users u on a.acre_id = u.id
				join acreditado_institucion au using (acre_id)
				join acreditado_objeto ao using (acin_id)
				where ao.carr_id = :carr_id
				and u.status = :status
				and a.moda_id = :moda_id
				and a.acre_test = 0'
                , array(
                ':carr_id' => $array['carr_id'],
                ':status' => Model_User::STATUS_ACTIVO,
                ':moda_id' => $array['moda_id'],
            ));

            if (DB::query(Database::SELECT, $query)->execute()->get('count')) {
                $array->error($field, 'fail_carr');
            }
        }
    }

    /**
     * Crea un nuevo usuario
     */
    public static function create_new($post/*, $role = Model_Role::ROLE_REGULAR, $comite = TRUE, $document = TRUE*/)
    {
        extract($post);

        $oUser = Model_User::create_new($username, $password, $emails[$contact]);
        //->add('roles', $role);

        $oAcreditado = new self;

        try {
            $post['acre_id'] = $oUser->id;
            $post['acre_aprobado'] = 0;

            $oAcreditado->values($post, array(
                'obac_id',
                'acre_id',
                'moda_id',
                'acre_aprobado',
            ))->create(self::get_additional_validation($post));
        } catch (ORM_Validation_Exception $e) {
            $oUser->delete();

            throw $e;
        }

        // Comision
        //if ($comite)
        {
            $oAcreditado->save_comision($post);
        }

        // AcreditadoInstitucion
        $post['acin_estado'] = 'activo';
        $post['acca_estado'] = 'activo';
        $post['acre_id'] = $oAcreditado->acre_id;

        $oInstitucion = ORM::factory('AcreditadoInstitucion')->values($post, array(
            'acin_estado', 'acin_web', 'acin_telefono', 'acre_id', 'inst_id',
        ))->save();

        // AcreditadoObjeto
        $post['acin_id'] = $oInstitucion->acin_id;
        $post['acob_objeto_eval'] = $post['objeto_eval'];

        if ($post['carr_id'] == '') {
            ORM::factory('AcreditadoObjeto')->values($post, array(
                'acob_estado', 'acin_id', 'acob_objeto_eval',
            ))->save();
        } else {
            ORM::factory('AcreditadoObjeto')->values($post, array(
                'acob_estado', 'carr_id', 'acin_id', 'acob_objeto_eval',
            ))->save();
        }


        // Document
        //if ($document)
        {
            $oAcreditadoDocumento = ORM::factory('AcreditadoDocumento')
                ->values(array(
                    'acre_id' => $oAcreditado->acre_id,
                    'acdo_tipo' => 'activo',
                ));

            if ($oAcreditadoDocumento->save_file('acdo_documento', $username . Text::random(), $_FILES['file'])) {
                $oAcreditadoDocumento->save();
            }
        }

        // Datos institucionales
        if ($_FILES['filedi']['name'] != '') {
            ORM::factory('DocumentoInstitucional')->upload_archivo($_FILES['filedi']);
        }

        return $oAcreditado->reload();
    }

    /**
     * Guarda las personas que integrarán el comité de calidad.
     * @param post
     * @param prefix
     */
    public function save_comision($post, $prefix = '')
    {
        extract($post);

        foreach ($first_names as $index => $val) {
            $oPersona = ORM::factory('Persona');
            $oPersona->pers_estado = 'activo';
            $oPersona->pers_nombres = $first_names[$index];
            $oPersona->pers_apellidos = $last_names[$index];
            $oPersona->pers_cargo = $positions[$index];
            $oPersona->pers_correo = $emails[$index];
            $oPersona->pers_telefono = $phones[$index];

            $oPersona->pers_tipo = $prefix . (($contact == $index) ? 'contacto' : 'comision');

            $oPersona->acre_id = $this->acre_id;
            $oPersona->save();
        }
    }

    /**
     * Guarda la nueva comision del comité
     * Primero valida
     * @param type $post
     * @param type $requires_approval
     * @return \Model_Acreditado
     * @throws Exception_Saes
     */
    public function save_new_comision($post, $requires_approval)
    {
        if ($this->aComitePending()->count()) {
            throw new Exception_Saes('Ya ingresó un comité para ser aprobado.');
        }

        $prefix = 'pending_';
        $acdo_tipo = 'pendiente';

        $oAcreditadoDocumento = ORM::factory('AcreditadoDocumento')
            ->values(array(
                'acre_id' => $this->acre_id,
                'acdo_tipo' => $acdo_tipo,
            ));

        if ($oAcreditadoDocumento->save_file('acdo_documento', $this->oUser->username . Text::random(), $_FILES['file'])) {
            $oAcreditadoDocumento->save();
        }

        $this->save_comision($post, $prefix);

        return $this;
    }

    /**
     * Aprueba el cambio de comision en el comite de calidad
     */
    public function approve_comision()
    {
        $last = DB::select(DB::expr("max(pers_tipo) last"))
            ->from('persona')
            ->where('acre_id', '=', $this->acre_id)
            ->where('pers_tipo', 'like', 'comision_%')
            ->execute()
            ->get('last');

        //debug($last);
        $i = $last ? substr($last, -1) : 0;
        $i++;

        // OLD
        $old_comision = $this->aComite();//aPersona->where('pers_tipo', 'in', array_keys(Model_Persona::$tipos))->find_all();

        foreach ($old_comision as $oPersona) {
            $oPersona->pers_tipo = 'comision_' . $i;
            $oPersona->save();
        }

        $old_documento = $this->aDocumento->where('acdo_tipo', '=', 'activo')->find();
        if ($old_documento->loaded()) {
            $old_documento->acdo_tipo = 'comision_' . $i;
            $old_documento->save();
        }

        // NEW
        $new_comision = $this->aComitePending();//aPersona->where('pers_tipo', 'like', '%pending%')->find_all();

        foreach ($new_comision as $oPersona) {

            $oPersona->pers_tipo = str_replace('pending_', '', $oPersona->pers_tipo);
            $oPersona->save();

            if ($oPersona->pers_tipo == Model_Persona::TIPO_CONTACTO) {
                $email = $oPersona->pers_correo;
            }
        }

        $new_documento = $this->aDocumento->where('acdo_tipo', '=', 'pendiente')->find();
        if ($new_documento->loaded()) {
            $new_documento->acdo_tipo = 'activo';
            $new_documento->save();
        }

        // Update user email
        $this->oUser->email = $email;
        $this->oUser->save();
    }

    public function find($add = TRUE)
    {
        if ($add) {
            $this->add_titles();
        }

        return parent::find();
    }

    public function find_all($add = TRUE)
    {
        if ($add) {
            $this->add_titles();
        }

        return parent::find_all();
    }

    public function soft_delete()
    {
        if (!$this->_loaded)
            throw new Kohana_Exception('Cannot delete :model model because it is not loaded.', array(':model' => $this->_object_name));

        $this->oUser->status = 0;
        $this->oUser->save();

        return TRUE;
    }

    public function hard_delete()
    {
        $oUser = $this->oUser;

        $this->delete();
        $oUser->delete();

        return TRUE;
    }

    public function tipo()
    {
        return self::$estados[$this->tipo];
    }

    /**
     * Retorna el tipo de institucion de acuerdo al tiin_id (tipo institucion)
     */
    public function tipo_institucion()
    {
        return Model_ObjetoAcreditacion::$tipos[$this->oObjetoAcreditacion->tiin_id];
    }


    public function can_create_auto()
    {
        $oEventoActual = $this->oEventoActual();

        if ($oEventoActual->loaded()) {
            // Evento ya terminó, y EventoActual no está setteado como ANTERIOR
            if ($oEventoActual->oEvento->even_fecha_fin < date('Y-m-d')) {
                Log::error('Expresion de interés debe ser cerrada: ' . $oEventoActual->acev_id);
                return FALSE;
            }

            // SEGUIMIENTO
            if ($oEventoActual->oEvento->even_tipo == Model_Evento::TIPO_SEGUIMIENTO) {
                return TRUE;
            } // CONVOCATORIA and no autos
            elseif (!$oEventoActual->aAutoevaluacion->where('auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)->count_all()) {
                return TRUE;
            }

        }

        return FALSE;
    }

    /**
     *
     * @param  Model_Evento $oEvento
     * @return  boolean
     */
    /*private function _can_apply($oEvento)
    {
        if ($oEvento->even_tipo == Model_Evento::TIPO_SEGUIMIENTO)
            return TRUE;

        return ! $this->aAcreditadoEvento
            ->where('even_id', '=', $oEvento->even_id)
            ->where('acev_estado', '<>', Model_AcreditadoEvento::ESTADO_CANCELADO)
            ->find()
            ->loaded();
    }*/

    private function _close_evento_actual()
    {
        // Must close current event
        $oEventoActual = $this->oEventoActual();

        if ($oEventoActual->loaded()) {
            $oEventoActual->acev_estado = Model_AcreditadoEvento::ESTADO_ANTERIOR;
            $oEventoActual->save();
        }
    }

    /*
     * Crea el registro en la tabla acreditado_evento, con estado(ELEGIBLE)
     * @param Obj Evento
     * @param estado (estado del acreditado_evento)
     * @return Obj AcreditadoEvento
     */
    private function _create_evento($oEvento, $estado)
    {
        $oAcreditadoEvento = ORM::factory('AcreditadoEvento')
            ->values(array(
                'acre_id' => $this->acre_id,
                'even_id' => $oEvento->even_id,
                'acev_estado' => $estado,
                'acev_fecha_inicio' => date('Y-m-d'),
            ))
            ->create();

        return $oAcreditadoEvento;
    }

    /**
     *
     * @param  Model_Evento $oEvento
     * @return  Model_AcreditadoEvento
     * @throws  Exception_Saes
     */
    public function apply_to_evento(Model_Evento $oEvento)
    {
        if ($oEvento->even_tipo == Model_Evento::TIPO_CONVOCATORIA) {
            return $this->_add_evento_convocatoria($oEvento);
        } else {
            return $this->_add_evento_seguimiento($oEvento);
        }
    }

    private function _add_evento_convocatoria($oEvento)
    {
        // Can't apply
        /*if ( ! array_key_exists($oEvento->even_id, $this->aConvocatorias_disponibles()->as_array('even_id', 'even_nombre')))
        {
            throw new Exception_Saes('Usted no puede aplicar a este evento');
        }*/

        // Already applied
        /*if ($this->aAcreditadoEvento
            ->where('even_id', '=', $oEvento->even_id)
            ->where('acev_estado', '<>', Model_AcreditadoEvento::ESTADO_CANCELADO)
            ->find()
            ->loaded())
        {
            throw new Exception_Saes('Usted ya aplicó a este evento');
        }*/

        $this->_close_evento_actual();

        $oAcreditadoEvento = $this->_create_evento($oEvento, Model_AcreditadoEvento::ESTADO_ACTUAL);

        return $oAcreditadoEvento;
    }

    /*
     *
     */
    private function _add_evento_seguimiento($oEvento)
    {
        // Can't apply
        if (!array_key_exists($oEvento->even_id, $this->aSeguimientos_disponibles(/*$oEvento->even_objeto*/)->as_array('even_id', 'even_nombre'))) {
            throw new Exception_Saes('Usted no puede aplicar a este evento');
        }

        $this->_close_evento_actual();

        $oAcreditadoEvento = $this->_create_evento($oEvento, Model_AcreditadoEvento::ESTADO_ACTUAL);

        return $oAcreditadoEvento;
    }

    /*
     * Verifica si el evento existe en los posibles eventos de seguimiento.
     * Si existe, entonces lo crea con el
     * @param Obj Evento
     * @return Obj AcreditadoEvento
     */
    public function set_elegible($oEvento)
    {
        // Can't apply
        if (!array_key_exists($oEvento->even_id, $this->get_seguimientos_por_elegir())) {
            throw new Exception_Saes('El comité no puede aplicar a este evento');
        }

        $oAcreditadoEvento = $this->_create_evento($oEvento, Model_AcreditadoEvento::ESTADO_ELEGIBLE);

        return $oAcreditadoEvento;
    }

    public function get_total_standard()
    {
        return DB::select('esta_id')
            ->from('estandar')
            ->join('factor')->using('fact_id')
            ->join('dimension')->using('dime_id')
            ->as_object('Model_Estandar')
            ->execute();
    }

    public function tipo_gestion()
    {
        $ids = array(
            1 => 'PUBLICA',
            2 => 'PRIVADA',
        );

        return Arr::get(array_flip($ids), $this->oInstitucion()->oInstitucion->inst_gestion);
    }

    /**
     *
     * @return  Model_AcreditadoEvento
     */
    public function oEventoActual()
    {
        return $this->aAcreditadoEvento
            ->where('acev_estado', '=', Model_AcreditadoEvento::ESTADO_ACTUAL)
            ->find();
    }

    /**
     *
     * @return  Model_AcreditadoEvento
     */
    public function oEventoAnterior($objeto = '')
    {
        $query = $this->aAcreditadoEvento;
        /*if ($objeto != ''){
            $query->join(array('evento', 'e'))->using('even_id')
                ->where(DB::expr('e.even_objeto'), '=', $objeto);
        }   */
        $query->where('acev_estado', '=', Model_AcreditadoEvento::ESTADO_ANTERIOR)
            ->order_by('acev_id', 'DESC')
            ->find();

        return $query;
    }

    /**
     *
     * @return  Model_AcreditadoEvento
     */
    public function oEventoPendiente()
    {
        return $this->aAcreditadoEvento
            ->where('acev_estado', '=', Model_AcreditadoEvento::ESTADO_ACTUAL)
            ->find();
    }

    public function aConvocatorias_disponibles($objeto = '')
    {
        if ($objeto != '') {
            $query = ORM::factory('Evento')
                ->join('evento_modalidad')->using('even_id')
                ->join('evento_tipo_gestion')->using('even_id')
                ->where('even_tipo', '=', Model_Evento::TIPO_CONVOCATORIA)
                ->where('tiin_id', '=', $this->oObjetoAcreditacion->tiin_id)
                ->where('moda_id', '=', $this->moda_id);
            //->where('even_objeto', '=', $objeto);
            if ($objeto == 1) {
                $query->where('even_ponderacion', '=', Model_Evento::PONDERACION_NO);
            }
            return $query->where(DB::expr('curdate()'), 'between', DB::expr('even_fecha_inicio and even_fecha_fin'))
                ->find_all();
        } else {
            return ORM::factory('Evento')
                ->join('evento_modalidad')->using('even_id')
                ->join('evento_tipo_gestion')->using('even_id')
                ->where('even_tipo', '=', Model_Evento::TIPO_CONVOCATORIA)
                ->where('tiin_id', '=', $this->oObjetoAcreditacion->tiin_id)
                ->where('moda_id', '=', $this->moda_id)
                ->where(DB::expr('curdate()'), 'between', DB::expr('even_fecha_inicio and even_fecha_fin'))
                ->find_all()//->as_array('even_id', 'even_nombre')
                ;
        }
    }

    public function aSeguimientos_disponibles($objeto = '')
    {
        return DB::select('even_id', 'even_nombre')
            ->from('evento')
            ->where('tiin_id', '=', $this->oObjetoAcreditacion->tiin_id)
            ->where(DB::expr('curdate()'), 'between', DB::expr('even_fecha_inicio and even_fecha_fin'))
            ->where('even_tipo', '=', Model_Evento::TIPO_SEGUIMIENTO)
            ->execute();
        /*
        if ($objeto != '') {
            $query = ORM::factory('Evento')
                ->join('acreditado_evento')->using('even_id')
                ->where('even_tipo', '=', Model_Evento::TIPO_SEGUIMIENTO)
                ->where('tiin_id', '=', $this->oObjetoAcreditacion->tiin_id);
            //->where('even_objeto', '=', $objeto);
            if ($objeto == 1) {
                $query->where('even_ponderacion', '=', Model_Evento::PONDERACION_NO);
            }
            return $query->where('acre_id', '=', $this->acre_id)
                ->where(DB::expr('curdate()'), 'between', DB::expr('even_fecha_inicio and even_fecha_fin'))
                ->where('acev_estado', '=', Model_AcreditadoEvento::ESTADO_ELEGIBLE)
                ->find_all();
        } else {
            return ORM::factory('Evento')
                ->join('acreditado_evento')->using('even_id')
                ->where('even_tipo', '=', Model_Evento::TIPO_SEGUIMIENTO)
                ->where('tiin_id', '=', $this->oObjetoAcreditacion->tiin_id)
                ->where('acre_id', '=', $this->acre_id)
                ->where(DB::expr('curdate()'), 'between', DB::expr('even_fecha_inicio and even_fecha_fin'))
                ->where('acev_estado', '=', Model_AcreditadoEvento::ESTADO_ELEGIBLE)
                ->find_all()//->as_array('even_id', 'even_nombre')
                ;
        }
        */

    }

    /**
     * Obtiene todos las valoraciones
     */
    public function get_valoraciones()
    {
        return ORM::factory('Valoracion')
            ->where('valo_estado', '=', Model_Valoracion::STATUS_ACTIVO)
            ->find_all();
    }

    /**
     * Obtiene todos las valoraciones de las preguntas
     */
    public function get_valoraciones_pregunta()
    {
        return ORM::factory('ValoracionPregunta')
            ->where('vapr_estado', '=', Model_Valoracion::STATUS_ACTIVO)
            ->find_all();
    }

    /**
     * Obtiene todos los estados para el proceso de mejora
     */
    public function get_estados()
    {
        return ORM::factory('Estado')
            ->where('esta_estado', '=', Model_Estado::STATUS_ACTIVO)
            ->find_all();
    }

    /**
     * Obtiene todos los dimensiones
     */
    public function get_dimensiones()
    {
        return ORM::factory('Dimension')
            ->join('factor')->using('dime_id')
            ->join('estandar')->using('fact_id')
            /*->join(array('tipo_institucion', 't'))
                ->using('tiin_id')
            ->join(array('objeto_acreditacion', 'o'))
                ->using('tiin_id')*/
            ->where('obac_id', '=', $this->obac_id)
            ->and_where('dime_estado', '=', Model_Dimension::STATUS_ACTIVO)
            ->and_where('fact_estado', '=', Model_Factor::STATUS_ACTIVO)
            ->and_where('esta_estado', '=', Model_Saes::STATUS_ACTIVO)
            ->group_by('dime_id')
            ->find_all();
    }

    public function get_dimensiones_filter()
    {
        return ORM::factory('Dimension')
            /*->join(array('tipo_institucion', 't'))
                ->using('tiin_id')
            ->join(array('objeto_acreditacion', 'o'))
                ->using('tiin_id')*/
            ->where('dime_estado', '=', Model_Dimension::STATUS_ACTIVO)
            ->where('obac_id', '=', $this->obac_id)
            ->find_all();
    }

    /**
     * Obtiene todos los factores
     */
    public function get_factores()
    {
        return ORM::factory('Factor')
            ->join(array('dimension', 'd'))->using('dime_id')
            ->join('estandar')->on('estandar.fact_id', '=', 'factor.fact_id')
            /*->join(array('tipo_institucion', 't'))
                ->using('tiin_id')
            ->join(array('objeto_acreditacion', 'o'))
                ->using('tiin_id')*/
            ->where('d.obac_id', '=', $this->obac_id)
            ->and_where('dime_estado', '=', Model_Dimension::STATUS_ACTIVO)
            ->and_where('fact_estado', '=', Model_Factor::STATUS_ACTIVO)
            ->and_where('esta_estado', '=', Model_Saes::STATUS_ACTIVO)
            ->group_by('fact_id')
            ->find_all();
    }

    /**
     * Obtiene todos los estandares
     */
    public function get_estandares()
    {
        return ORM::factory('Estandar')
            ->select(DB::expr('d.dime_id as dime_id'))
            ->join(array('factor', 'f'))->using('fact_id')
            ->join(array('dimension', 'd'))->using('dime_id')
            /*->join(array('tipo_institucion', 't'))
                ->using('tiin_id')
            ->join(array('objeto_acreditacion', 'o'))
                ->using('tiin_id')*/
            ->where('d.obac_id', '=', $this->obac_id)
            ->and_where('d.dime_estado', '=', Model_Dimension::STATUS_ACTIVO)
            ->and_where('f.fact_estado', '=', Model_Factor::STATUS_ACTIVO)
            ->where('esta_estado', '=', Model_Estandar::STATUS_ACTIVO)
            ->find_all();
    }

    /**
     * Obtiene todos los medios
     */
    public function get_medios()
    {
        return ORM::factory('Medio')
            ->join(array('estandar', 'e'))->using('esta_id')
            ->join(array('factor', 'f'))->using('fact_id')
            ->join(array('dimension', 'd'))->using('dime_id')
            /*->join(array('tipo_institucion', 't'))
                ->using('tiin_id')
            ->join(array('objeto_acreditacion', 'o'))
                ->using('tiin_id')*/
            ->where('d.obac_id', '=', $this->obac_id)
            ->and_where('e.esta_estado', '=', Model_Estandar::STATUS_ACTIVO)
            ->and_where('d.dime_estado', '=', Model_Dimension::STATUS_ACTIVO)
            ->and_where('f.fact_estado', '=', Model_Factor::STATUS_ACTIVO)
            ->where('medi_estado', '=', Model_Medio::STATUS_ACTIVO)
            ->find_all();
    }

    /**
     * Obtiene todas las autoevaluaciones cerradas
     */
    public function get_autoevaluaciones($objeto = '')
    {
        $estado = Model_Autoevaluacion::STATUS_CERRADO;
        $acre_id = $this->acre_id;
        $where = '';
        /*  if ($objeto != '') {
              $where = " AND e.even_objeto = '$objeto'";
          }*/
        return DB::select(DB::expr("a.auto_id auto_id, CONCAT(a.auto_numero, '. ', e.even_nombre) display
				FROM autoevaluacion a
				JOIN acreditado_evento ae ON ae.acev_id = a.acev_id
				JOIN evento e ON e.even_id = ae.even_id
				WHERE ae.acre_id = $acre_id AND a.auto_estado = '$estado' $where"))
            ->as_object()
            ->execute();
    }

    /**
     * Obtiene todas las autoevaluacion detalle cerradas
     */
    public function get_autoevaluacion_detalle($objeto = '')
    {
        $estado = Model_Autoevaluacion::STATUS_CERRADO;
        $acre_id = $this->acre_id;
        $where = '';
        /*if ($objeto != '') {
            $where = " AND e.even_objeto = '$objeto'";
        }*/
        return DB::select(DB::expr(
            "au.esta_id esta_id, au.aude_ponderacion ponderacion, es.fact_id fact_id, au.auto_id auto_id 
				FROM autoevaluacion_detalle au
				JOIN estandar es ON es.esta_id = au.esta_id
				JOIN factor f ON f.fact_id = es.fact_id
				WHERE au.auto_id IN (
					SELECT a.auto_id FROM autoevaluacion a
					JOIN acreditado_evento ae ON ae.acev_id = a.acev_id
					JOIN evento e ON e.even_id = ae.even_id
					WHERE ae.acre_id = $acre_id AND a.auto_estado = '$estado' $where)"))
            ->as_object()
            ->execute();
    }

    /**
     *
     * Obtiene ponderacion cerrada
     *
     * @return Model_Ponderacion
     */
    public function get_ponderacion_cerrada()
    {
        return $this->aPonderacion
            ->where('pond_estado', '=', Model_Ponderacion::STATUS_ACTIVO)
            ->where('pond_tipo', '=', Model_Ponderacion::TIPO_CERRADA)
            //->where('acre_id', '=', $this->acre_id)
            ->find();
    }

    /**
     * Obtiene ponderacion borrador
     * *
     * @return Model_Ponderacion
     */
    public function get_ponderacion_borrador()
    {
        return $this->aPonderacion
            ->where('pond_estado', '=', Model_Ponderacion::STATUS_ACTIVO)
            ->where('pond_tipo', '=', Model_Ponderacion::TIPO_BORRADOR)
            //->where('acre_id', '=', $this->acre_id)
            ->find();
    }


    /*public function eventos_disponibles()
    {
        return $this->aConvocatorias_disponibles()//->as_array('even_id', 'even_nombre')
            + $this->aSeguimientos_disponibles()//->as_array('even_id', 'even_nombre')
            ;
    }*/

    /*
     * Obtiene los eventos asociados al comite
     * field acev_estado in (3,4)
     * return Obj Database Mysql Result
     */
    public function get_eventos_cerrados()
    {
        return ORM::factory('AcreditadoEvento')
            ->select(DB::expr('count(acev_id) count_fichas'))
            ->where('acre_id', '=', $this->acre_id)
            ->where('acev_estado', '<>', Model_AcreditadoEvento::ESTADO_CANCELADO)
            ->where('acev_estado', 'in', Model_AcreditadoEvento::$closed)
            ->group_by('acev_id')
            ->find_all();
    }

    /*
     * Obtiene los eventos (tipo seguimiento) que aún no están asociados al
     * comite, que no estén cancelados (5) y que tengan el tipo de institucion respectivo.
     */
    public function get_seguimientos_por_elegir()
    {
        $cancelado = Model_AcreditadoEvento::ESTADO_CANCELADO;

        return ORM::factory('Evento')
            ->join(DB::expr('acreditado_evento ae'), 'left')
            //->using('even_id')
            ->on(DB::expr(''), '',
                DB::expr("evento.even_id = ae.even_id AND acre_id = {$this->acre_id} AND acev_estado <> {$cancelado}"))
            ->where('acev_id', 'is', NULL)
            ->where('even_tipo', '=', Model_Evento::TIPO_SEGUIMIENTO)
            //->where('acev_estado', '=', )
            ->where('tiin_id', '=', $this->oObjetoAcreditacion->tiin_id)
            ->find_all()
            ->as_array('even_id', 'even_nombre');
    }

    private function _get_autoevaluaciones()
    {
        $aAutoevaluacion = DB::select('*')
            ->from('autoevaluacion')
            ->join('acreditado_evento')->using('acev_id')
            ->where('acre_id', '=', $this->oAcreditado->acre_id)
            ->and_where('auto_estado', '=', Model_Autoevaluacion::STATUS_CERRADO)
            ->as_object()
            ->execute();

        $autoevaluaciones = [];

        foreach ($aAutoevaluacion as $oAutoevaluacion) {
            $autoevaluaciones[] = [
                'id' => $oAutoevaluacion->auto_id,
                'display' => '',
                'data' => [],
            ];
        }

        return $autoevaluaciones;
    }

    public function get_eventos()
    {
        $query_event_convocatoria = DB::select('even_id')
            ->from('evento')
            ->join('acreditado_evento')->using('even_id')
            ->join('autoevaluacion')->using('acev_id')
            ->where('acre_id', '=', $this->acre_id)
            ->and_where('auto_estado', '=', Model_Autoevaluacion::STATUS_CERRADO)
            ->and_where('even_tipo', '=', Model_Evento::TIPO_CONVOCATORIA);

        $aEvento = DB::select('*')
            ->from('evento')
            ->where('even_id', 'not in', $query_event_convocatoria)
            ->and_where('tiin_id', '=', $this->oObjetoAcreditacion->tiin_id)
            ->and_where('even_fecha_inicio', '<=', DB::expr('CURDATE()'))
            ->and_where('even_fecha_fin', '>=', DB::expr('CURDATE()'))
            ->as_object()
            ->execute();

        $eventos = [];

        foreach ($aEvento as $oEvento) {
            $eventos[] = [
                'display' => $oEvento->even_nombre,
                'id' => $oEvento->even_id,
                'editable' => 0,
            ];
        }

        return $eventos;
    }

    public function matriz_completa()
    {
        $status = TRUE;

        $oObac = ORM::factory('ObjetoAcreditacion', $this->obac_id);

        if (!$oObac->loaded())
            return FALSE;

        $aDimension = $this->get_dimensiones();

        if(count($aDimension) == 0)
            return FALSE;

        foreach ($aDimension as $oDimension) {
            $aFactor = ORM::factory('Factor')
                ->where('dime_id', '=', $oDimension->dime_id)
                ->and_where('fact_estado', '=', Model_Saes::STATUS_ACTIVO)
                ->find_all();
            if (count($aFactor) == 0) {
                debug2($oDimension->dime_id);
                //$status = FALSE;
                //break;
            } else {
                foreach ($aFactor as $oFactor) {
                    $aEstandar = ORM::factory('Estandar')
                        ->where('esta_estado', '=', Model_Saes::STATUS_ACTIVO)
                        ->and_where('fact_id', '=', $oFactor->fact_id)
                        ->find_all();
                    if (count($aEstandar) == 0) {
                        debug2($oFactor->fact_id);
                        //$status = FALSE;
                        //break;
                    }
                }
            }

        }

        return $status;
    }

}
