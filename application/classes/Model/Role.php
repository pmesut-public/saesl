<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Role extends Model_Auth_Role {
	
	const ROLE_ADMIN = 1;
	const ROLE_OPERADOR = 2;
	const ROLE_REGULAR = 3;
	const ROLE_SUPERVISOR = 6;
	
	public static $roles = array(
		self::ROLE_ADMIN => 'Admin',
		self::ROLE_OPERADOR => 'Operador',
		self::ROLE_REGULAR => 'Regular',
		self::ROLE_SUPERVISOR => 'Supervisor',
		//self::ROLE_REGULAREXTERNO => 'Regular externo',
	);
	
	public function labels()
	{
		return array(
			'name' => 'Nombre',
		);
	}
	
	protected $_has_many = array(
		'users' => array(
			'model' => 'User',
			'foreign_key' => 'role_id',
			'through' => 'roles_users',
			'far_key' => 'user_id',
		),
	);
	
} // End Role Model