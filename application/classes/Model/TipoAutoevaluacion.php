<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_TipoAutoevaluacion extends Model_Saes {

	const TIPO_ETAPA1 = 1;
	const TIPO_ETAPA2 = 2;
	const TIPO_LIBRE  = 3;
	
	public static $tipos = array(
		self::TIPO_ETAPA1 => '1a etapa',
		self::TIPO_ETAPA2 => '2a etapa',
		self::TIPO_LIBRE  => 'Libre',
	);
	
	protected $_table_name = 'tipo_autoevaluacion';

	protected $_primary_key = 'tiau_id';

	public static function get_tipos()
	{
		 return ORM::factory('TipoAutoevaluacion')
			->find_all()
			->as_array('tiau_id', 'tiau_nombre');
	}
}
