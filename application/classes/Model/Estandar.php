<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Estandar extends Model_Saes {
    
    // Estados
	const NO_CUMPLE = '1';
	const CUMPLE    = '2';
	const NO_APLICA = '3';
        
    public static $estado_idicadores = array(
		self::NO_CUMPLE => 'No cumple',
		self::CUMPLE    => 'Cumple',
		self::NO_APLICA => 'No Aplica'
    );

        public function labels()
	{
		return array(
                    'esta_codigo' => 'Código',
                    'esta_titulo' => 'Título',
                    'esta_descripcion' => 'Descripción',
                    'esta_estado' => 'Estado',
                    'moda_id' => 'Modalidad',
		);
	}
	
	public function rules()
	{
		return array(
			'esta_codigo' => array(
				array('not_empty'),
			),
			'esta_estado' => array(
				array('not_empty'),
			),
			'fact_id' => array(
				array('not_empty'),
			),
                    'moda_id' => array(
				array('not_empty'),
			),
                    'esta_titulo' => array(
			array('not_empty'),
                    ),
		);
	}
	
	public function filters()
	{
		return array(
			'esta_titulo' => array(
				array('Model_Saes::filter_empty'),
			),
                    'moda_id' => array(
				array('Model_Saes::filter_empty'),
			),
			'esta_descripcion' => array(
				array('Model_Saes::filter_empty'),
			),
		);
	}
	
	protected $_table_name = 'estandar';

	protected $_primary_key = 'esta_id';
    
    /**
	 * Obtiene los estandares
	 */
	public static function get_all()
	{
		return DB::select()
			->from('estandar')
			->where('esta_estado', '=', Model_Estandar::STATUS_ACTIVO)
			->execute()
			->as_array('esta_id', 'esta_titulo');
	}

	protected $_belongs_to = array(
		'oFactor' => array(
			'model' => 'Factor',
			'foreign_key' => 'fact_id',
		),
	);
	
	protected $_has_many = array(
		'aMedio' => array(
			'model' => 'Medio',
			'foreign_key' => 'esta_id',
		),
		'aDetalle' => array(
			'model' => 'AutoevaluacionDetalle',
			'foreign_key' => 'esta_id',
		),
	);
	
	/**
	 * Retorna el campo esta_codigo aninando con la function titulo()
	 */
	public function title()
	{
		return $this->esta_codigo.'. '.$this->titulo();
	}
	
	/**
	 * Verifica si existe el campo esta_titulo, si existe lo imprime, de lo contrario
	 * imprime el campo esta_descripcion
	 */
	public function titulo()
	{
		return $this->esta_titulo ? $this->esta_titulo : $this->esta_descripcion;
	}
	
	
}
