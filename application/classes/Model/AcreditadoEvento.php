<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AcreditadoEvento extends Model_Saes {

	protected $_upload = 'uploads/fichas/';
	
	protected $_created_column = FALSE;
	
	const ESTADO_ELEGIBLE = 1;
	const ESTADO_ACTUAL = 3;
	const ESTADO_ANTERIOR = 4;
	const ESTADO_CANCELADO = 5;
	
	public static $estados = array(
		self::ESTADO_ELEGIBLE => 'Elegible',
		self::ESTADO_ACTUAL => 'Actual',
		self::ESTADO_ANTERIOR => 'Terminado',
		self::ESTADO_CANCELADO => 'Cancelado',
	);
	
	protected $_table_name = 'acreditado_evento';

	protected $_primary_key = 'acev_id';

	protected $_belongs_to = array(
		'oAcreditado' => array(
			'model' => 'Acreditado',
			'foreign_key' => 'acre_id',
		),
		'oEvento' => array(
			'model' => 'Evento',
			'foreign_key' => 'even_id',
		),
	);
	
	protected $_has_many = array(
		'aAutoevaluacion' => array(
			'model' => 'Autoevaluacion',
			'foreign_key' => 'acev_id',
		),
		'aFicha' => array(
            'model' => 'Ficha',
            'foreign_key' => 'acev_id',
            'through' => 'acreditado_evento_ficha',
            'far_key' => 'fich_id',            
        ),
	);
	
	public function create_new($acre_id, $even_id)
	{
		$new = $this->set('even_id', $even_id)
			->set('acre_id', $acre_id)
			->set('acev_estado', self::ESTADO_ACTUAL)
			->set('acev_fecha_inicio', date('Y-m-d'));
		$this->save();
		
		return $new;
	}

		/**
	 * Retorna un array de eventos por tipo de financiamiento filtrado por obac_tipo_acreditacion
	 * @return type
	 */
	public function aTipoFinanciamientoDisponible()
	{
		return $this->oEvento->aTipoFinanciamiento
			->where('tifi_tipo_acreditacion', '=', $this->oAcreditado->oObjetoAcreditacion->obac_tipo_acreditacion)
			->find_all()
			->as_array('tifi_id', 'tifi_nombre');
	}
	
	/*public function close()
	{
		$this->acev_estado = Model_AcreditadoEvento::ESTADO_ACTUAL;
		$this->save();
	}*/
	
	/**
	 * Setea al capo acev_estado el valor 5 (ESTADO CANCELADO).
	 */
	public function discard()
	{
		//if ($this->oEvento->even_tipo == Model_Evento::TIPO_CONVOCATORIA)
		//{
			$this->acev_estado = Model_AcreditadoEvento::ESTADO_CANCELADO;
		//}
		//else
		//{
			//$this->acev_estado = Model_AcreditadoEvento::ESTADO_ELEGIBLE;
		//}
		
		$this->save();
	}
	
	/**
	 * 
	 * @var type 
	 */
	public static $closed = array(
		self::ESTADO_ACTUAL,
		self::ESTADO_ANTERIOR,
	);
	
	/**
	 * Verifica si el valor del campo acev_estado tiene el valor 3o 4
	 * @return type boolean
	 */
	public function is_closed()
	{
		return in_array($this->acev_estado, self::$closed);
	}
	
	/**
	 * Setea al campo acev_estado de la tabla acreditado_evento el valor (3)
	 */
	public function is_actual()
	{
		return $this->acev_estado == self::ESTADO_ACTUAL;
	}
	
	public function estado()
	{
		return self::$estados[$this->acev_estado];
	}
	
	/**
	 * Permite crear una nueva autoevaluacion
	 * @return type
	 */
	public function admin_can_create_new_autoevaluacion()
	{
		$activas = $this->aAutoevaluacion
			->where('auto_estado', '=', Model_Autoevaluacion::STATUS_ACTIVO)
			->count_all();
		
		return $this->is_actual() AND ! $activas;
	}
	
	/**
	 * Retorna el numero de autoevaluaciones canceladas
	 * @return type
	 */
	public function can_be_cancelled()
	{
		//$this->acev_estado == Model_AcreditadoEvento::ESTADO_ACTUAL
		
		return ! $this->aAutoevaluacion
			->where('auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
			->count_all();
		
	}

	/**
	 * Retorna si es editable la ficha correspondiente
	 * @return string
	 */
	public function is_editable($fich_id) {
		$result = '0';
		if ($this->has('aFicha')) {			
			$result = DB::select('acfi_editable')
						->from('acreditado_evento_ficha')
						->where('acev_id','=',$this->acev_id)
						->and_where('fich_id','=',$fich_id)
						->execute()
						->get('acfi_editable');	
		}
		return $result == '0' ?  true : false;
	}

	/**
	 * Asocia los datos con la tabla Ficha
	 * @return void
	 */
	public function create_acreditado_evento_ficha(){
		$fichas = ORM::factory('Ficha')->find_all()->as_array('fich_id','fich_id');
		$this->add('aFicha', array_keys($fichas));
	}
}
