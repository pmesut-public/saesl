<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_AutoevaluacionDocumento extends Model_Saes {

	// Status
	const STATUS_BUENO = 'bueno';
	const STATUS_REGULAR = 'regular';
	const STATUS_MALO = 'malo';
	const STATUS_NONE = NULL;

	public static $statuses = array(
		self::STATUS_BUENO => 'Bueno',
		self::STATUS_REGULAR => 'Regular',
		self::STATUS_MALO => 'Malo',
		self::STATUS_NONE => 'None',
	);
	public static $statuses_report = array(
		self::STATUS_BUENO => 'B',
		self::STATUS_REGULAR => 'R',
		self::STATUS_MALO => 'M',
		self::STATUS_NONE => '',
	);
	protected $_table_name = 'autoevaluacion_documento';
	protected $_primary_key = 'audo_id';
	protected $_belongs_to = array(
		'oDocumento' => array(
			'model' => 'Documento',
			'foreign_key' => 'docu_id',
		),
		'oEvidencia' => array(
			'model' => 'Evidencia',
			'foreign_key' => 'auev_id',
		),
		'oAutoevaluacionMedio' => array(
			'model' => 'AutoevaluacionMedio',
			'foreign_key' => 'aume_id',
		),
	);

	public static function get_status_by_report($status)
	{
		return self::$statuses_report[$status];
	}

}
