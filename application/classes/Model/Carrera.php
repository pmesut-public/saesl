<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Carrera extends Model_Saes {

	public function labels()
	{
		return array(
			'carr_nombre'         => 'Nombre',
			'inst_id'             => 'Institución',
			'carr_codigo'         => 'Código',
			'carr_facultad'       => 'Facultad',
			'carr_estado'         => 'Estado',
			'carr_obac_id'        => 'Licenciamiento',
			'carr_comite_sineace' => 'Registro SINEACE',
		);
		
	}
	
	public function rules()
	{
		return array(
			'carr_nombre' => array(
				array('not_empty'),
				array(array($this, 'check_nombre'), array(':validation', ':field')),
			),
			'inst_id' => array(
				array('not_empty'),
			),
			'carr_comite_sineace' => array(
				array('not_empty'),
			),
		);
	}
	
	protected $_table_name = 'carrera';

	protected $_primary_key = 'carr_id';
	
	protected $_belongs_to = array(
		'oInstitucion' => array(
			'model' => 'Institucion',
			'foreign_key' => 'inst_id',
		),
	);
	
	public function check_nombre($array, $field)
	{
		$model = ORM::factory($this->object_name())
			->where('inst_id', '=', $this->inst_id)
			->where('carr_nombre', '=', $this->carr_nombre)
			->find();

		if ($this->loaded())
		{
			return ( ! ($model->loaded() AND $model->pk() != $this->pk()));
		}

		return ( ! $model->loaded());
	}
	
	public static function get_from_inst_and_obac($inst_id/*, $obac_id*/)
	{
            //echo $inst_id .' '. $obac_id; die;
            return ORM::factory('Carrera')
			->where('carr_estado', '=', Model_Saes::STATUS_ACTIVO)
			->where('inst_id', '=', $inst_id)
			//->where('carr_obac_id', '=', $obac_id)
			->find_all()
			->as_array('carr_id', 'carr_nombre');
                //debug($return);
	}
}
