<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_ValoracionPregunta extends Model_Saes {

	public function labels()
	{
		return array(
			'vapr_codigo' => 'Código',
			'vapr_titulo' => 'Título',
			'vapr_equivalencia' => 'Equivalencia',
			'vapr_descripcion' => 'Descripción',
			'vapr_evidencia' => 'Evidencia',
			'vapr_estado' => 'Estado',
		);
	}

	public function rules()
	{
		return array(
			'vapr_codigo' => array(
				array('not_empty'),
			),
			'vapr_titulo' => array(
				array('not_empty'),
			),
			'vapr_equivalencia' => array(
				array('not_empty'),
			),
			'vapr_estado' => array(
				array('not_empty'),
			),
		);
	}

	protected $_table_name = 'valoracion_pregunta';
	protected $_primary_key = 'vapr_id';

	public function title()
	{
		return $this->vapr_codigo . '. ' . $this->vapr_titulo;
	}

	public function titulo()
	{
		return $this->vapr_titulo;
	}

	/**
	 * Obtiene las valoraciones
	 */
	public static function get_all()
	{
		return DB::select()
				->from('valoracion_pregunta')
				->where('vapr_estado', '=', Model_Factor::STATUS_ACTIVO)
				->execute()
				->as_array('vapr_id', 'vapr_titulo');
	}

}
