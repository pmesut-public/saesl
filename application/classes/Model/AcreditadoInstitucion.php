<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AcreditadoInstitucion extends Model_Saes {

	protected $_table_name = 'acreditado_institucion';

	protected $_primary_key = 'acin_id';

	protected $_belongs_to = array(
		'oAcreditado' => array(
			'model' => 'Acreditado',
			'foreign_key' => 'acre_id',
		),
		'oInstitucion' => array(
			'model' => 'Institucion',
			'foreign_key' => 'inst_id',
		),
	);
}
