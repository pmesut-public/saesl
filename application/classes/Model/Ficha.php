<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @package    ProCalidad
 * @author     Procalidad C2 Team
 * @copyright  2018 ProCalidad
 */
class Model_Ficha extends Model_Saes
{
	/**
	 * Tabla asociada al Modelo.
	 */
	protected $_table_name = 'ficha';

	/**
	 * Primary key (PK) del Modelo.
	 */
	protected $_primary_key = 'fich_id';
	
	/**
	 * Relación Has Many del Modelo.
	 */
	protected $_has_many = array(
        'aAcreditadoEvento' => array(
            'model' => 'AcreditadoEvento',
            'foreign_key' => 'fich_id',
            'through' => 'acreditado_evento_ficha',
            'far_key' => 'acev_id',            
        ),
    );

	/**
	 * Labels iniciales del Modelo.
	 */
	public static $labels = [
		'fich_id'   => 'ID',
		'fich_name' => 'Nombre',
    ];

    const FICHA_AUTOEVALUACION = 1;
	const FICHA_FORMATO = 2;  
	
	const FICHA_NO_GUARDADA = 0;
	const FICHA_GUARDADA = 1;
	

    /**
     * Reglas del modelo
     */
    
    public function rules()
	{
		return array(
			'fich_nombre' => array(
				array('not_empty')
			)
		);
	}

	/**
	 * Obtener estado si está finalizado la ficha
	 */
	public static function is_finished($accev_id,$fich_id) {
		$is_finished = DB::select('acfi_editable')
						->from('acreditado_evento_ficha')
						->where('acev_id','=',$accev_id)
						->and_where('fich_id','=',$fich_id)
						->execute()
						->get('acfi_editable');

		return $is_finished;						
	}
	
}
