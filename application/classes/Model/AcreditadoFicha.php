<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AcreditadoFicha extends Model_Saes {

	/*const PENDIENTE = 'pendiente';
	const PROCESO = 'proceso';
	const CERRADO = 'cerrado';
	
	public static $estados = array(
		self::PENDIENTE => 'Pendiente',
		self::PROCESO => 'En proceso',
		self::CERRADO => 'Cerrado',
	);*/
	
	protected $_created_column = FALSE;
	
	public function labels()
	{
		return array(
			'moda_nombre' => 'Nombre',
			'moda_descripcion' => 'Descripción',
			'moda_estado' => 'Estado',
		);
	}
	
	protected $_table_name = 'acreditado_ficha';

	protected $_primary_key = 'acfi_id';
	
	protected $_belongs_to = array(
		'oAcreditadoEvento' => array(
			'model' => 'AcreditadoEvento',
			'foreign_key' => 'acev_id',
		),
		'oEventoFicha' => array(
			'model' => 'EventoFicha',
			'foreign_key' => 'evfi_id',
		),
	);
	
	/*public static function get_all()
	{
		return DB::select()
			->from('modalidad')
			->where('moda_estado', '=', Model_Modalidad::STATUS_ACTIVO)
			->execute()
			->as_array('moda_id', 'moda_nombre');
	}*/
	
}
