<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_IcCriterio extends Model_Saes {

	const ESTADO_ACTIVO		= 1;
	const ESTADO_INACTIVO   = 0;
	
	public static $estados = array(
		self::ESTADO_ACTIVO		=> 'Activo',
		self::ESTADO_INACTIVO   => 'Inactivo',
	);
	
	public function labels()
	{
		return array(
			'iccr_codigo'		=> 'Código',
			'iccr_titulo'		=> 'Título',
			'iccr_descripcion'	=> 'Descripción',
			'iccr_estado'		=> 'Estado',
		);
	}
	
	protected $_table_name = 'ic_criterio';

	protected $_primary_key = 'iccr_id';

	protected $_has_many = array(
		'aIcRequisito' => array(
			'model' => 'IcRequisito',
			'foreign_key' => 'iccr_id',
		),
	);
	
	public function title()
	{
		return $this->iccr_codigo.'. '.$this->titulo();
	}
	
	public function titulo()
	{
		return $this->iccr_titulo ? $this->iccr_titulo : $this->iccr_descripcion;
	}
	
	public static function get_all()
	{
		$concat = "concat_ws(' - ', iccr_codigo, iccr_titulo)";
		
		$criterios =  DB::select('iccr_id', 'iccr_titulo')
			//->select(DB::expr("$concat iccr_name"))
			->select(DB::expr("$concat iccr_name"))
			->from('ic_criterio')
			->where('iccr_estado', '=', 1)
			//->order_by('tiin_id')
			->order_by('iccr_name')
			->execute()
			->as_array('iccr_id', 'iccr_name')
			;
		//debug($criterios);
		return $criterios;
	}
	
}
