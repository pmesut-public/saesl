<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_TipoGestion extends Model_Saes {

	protected $_created_column = FALSE;
	
	public function labels()
	{
		return array(
			'tige_nombre' => 'Nombre',
		);
	}
	
	protected $_table_name = 'tipo_gestion';

	protected $_primary_key = 'tige_id';
	
	/**
	 * Obtiene el tipo de gestion (PUBLICA O PRIVADA)
	 */
	public static function get_all()
	{
		return DB::select()
			->from('tipo_gestion')
			->execute()
			->as_array('tige_id', 'tige_nombre');
	}
	
}
