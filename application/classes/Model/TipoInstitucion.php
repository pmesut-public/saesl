<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_TipoInstitucion extends Model_Saes {

	protected $_table_name = 'tipo_institucion';

	protected $_primary_key = 'tiin_id';

	protected $_belongs_to = array(
		'oObjetoAcreditacion' => array(
			'model' => 'ObjetoAcreditacion',
			'foreign_key' => 'tiin_id',
		),
	);
	
	public function nombre()
	{
		return $this->tiin_nombre;
	}

	public static function get_all() {

        return DB::select('tiin_id', 'tiin_nombre')
                        ->from('tipo_institucion')
                        ->where('tiin_estado', '=', 'activo')
                        ->order_by('tiin_id')
                        ->order_by('tiin_nombre')
                        ->execute()
                        ->as_array('tiin_id', 'tiin_nombre')
        ;
    }
}
