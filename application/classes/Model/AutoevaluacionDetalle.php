<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AutoevaluacionDetalle extends Model_Saes {

	const STATUS_PENDIENTE = 'pendiente';
	const STATUS_PROCESO   = 'proceso';
	const STATUS_CERRADO   = 'cerrado';
	
	public static $statuses = array(
		self::STATUS_PENDIENTE => 'Pendiente',
		self::STATUS_PROCESO   => 'Proceso',
		self::STATUS_CERRADO   => 'Cerrado',
	);
	
	const BY_USER   = 0;
	const BY_SYSTEM = 1;
	
	public static $closed_by = array(
		self::BY_USER   => 'Usuario',
		self::BY_SYSTEM => 'Sistema',
	);
	
	protected $_table_name = 'autoevaluacion_detalle';

	protected $_primary_key = 'aude_id';

	protected $_belongs_to = array(
		'oEstandar' => array(
			'model' => 'Estandar',
			'foreign_key' => 'esta_id',
		),
		'oAutoevaluacion' => array(
			'model' => 'Autoevaluacion',
			'foreign_key' => 'auto_id',
		),
	);
	
	protected $_has_many = array(
		'aAutoCriterio' => array(
			'model' => 'AutoevaluacionCriterio',
			'foreign_key' => 'aude_id',
		),
		'aAutoEvidencia' => array(
			'model' => 'Evidencia',
			'foreign_key' => 'auev_id',
		),
		'aAutoeveluacionActividad' => array(
			'model' => 'AutoevaluacionActividad',
			'foreign_key' => 'aude_id',
			'through' => 'autoevaluacion_detalle_actividad',
			'far_key' => 'auac_id',
		),
		'aAutoevaluacionMedio' => array(
			'model' => 'AutoevaluacionMedio',
			'foreign_key' => 'aude_id',
		),
	);
	
	public function set_cumplimiento($tipo, $level = NULL)
	{
		$tipos = [
			1 => 'universidad',
			3 => 'instituto',
		];
		
		$action = 'set_cumplimiento_' . Arr::get($tipos, $tipo, $tipo);
		
		$this->$action($level);
	}
	
	private function set_cumplimiento_universidad($level = NULL)
	{
		if ($level)
		{
			throw new Exception_Saes('save estandar tiin 1 with level');
		}
		
		$required = $this->oEstandar->aConcepto
			->where('conc_estado', '=', 'activo')
			->where('conc_obligatorio', '=', 1)
			->count_all();

		$accomplished = $this->aConcepto
			->join('concepto')->using('conc_id')
			->where('aude_id', '=', $this->aude_id)
			->where('conc_obligatorio', '=', 1)
			->where('code_cumple', '=', 1)
			->count_all();

		if ($required == $accomplished)
		{
			$this->aude_cumplimiento = 1;
		}
		else
		{
			$this->aude_cumplimiento = 0;
		}
	}
	
	private function set_cumplimiento_instituto($level = NULL)
	{
		if ( ! $level)
		{
			throw new Exception_Saes('save estandar tiin 3 without level');
		}
		
		$this->aude_aceptacion = $level;
		
		if ($level >= $this->oEstandar->esta_nivel_aceptacion)
		{
			$this->aude_cumplimiento = 1;
		}
		else
		{
			$this->aude_cumplimiento = 0;
		}
	}
	
	public function set_quality($quality)
	{
		$length = count($quality) ?: 1;
		
		if ($this->aude_cumplimiento == 1)
		{
			$new_values = array_map(function ($item) {
				return (3 - $item) / 2;
			}, $quality);
			//debug($new_values);
			
			$prom = array_sum($new_values) / (float) $length;
			$this->aude_calidad = ($prom == 0.5) ? 2 : (3 - round($prom, 0) * 2);
		}
		else
		{
			$this->aude_calidad = NULL;
		}
	}
	
	public function save(Validation $validation = NULL)
	{
		$this->aude_closed_by = self::BY_USER;

		$this->oAutoevaluacion->auto_fecha_fin = date('Y-m-d');
		$this->oAutoevaluacion->save();
		
		return parent::save($validation);
	}
	
	public function get_auto_last_actividad()
	{
		return ORM::factory('AutoevaluacionActividad')
			->join(DB::expr('autoevaluacion_detalle ad'))->on('ad.aude_id', '=', 'autoevaluacionactividad.aude_id')
			->where('ad.auto_id', '=', $this->auto_id)
			->order_by('autoevaluacionactividad.auac_id', 'DESC')
			->find();
	}
	
	/**
	 * Obtiene todas las actividades
	 * de la autoevaluación actual
	 */
	public static function get_autoactividad_by_estandar($aude_id)
	{		
		return ORM::factory('AutoevaluacionActividad')
			->join(DB::expr('autoevaluacion_detalle ad'))->on('ad.aude_id', '=', 'autoevaluacionactividad.aude_id')
			->where('ad.aude_id', '=', $aude_id)
			->find_all();
	}
	
	/**
	 * Obtiene todas las actividades relacionadas
	 * de la autoevaluación actual
	 */
	public static function get_autoactividad_relacionada($aude_id)
	{		
		return ORM::factory('AutoevaluacionActividad')
			->select(DB::expr('ada.aude_id aude_relacionada'))
			->join(DB::expr('autoevaluacion_detalle_actividad ada'))->using('auac_id')
			->where('ada.aude_id', '=', $aude_id)
			->find_all();
	}
	
	
}
