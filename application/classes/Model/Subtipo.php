<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Subtipo extends Model_Saes {

    public function labels()
    {
        return array(
//            'tiin_id' => 'Tipo de Institución',
            'subt_nombre' => 'Subtipo',
            'subt_codigo' => 'Código',
            'subt_abrev' => 'Abreviatura',
            'subt_estado' => 'Estado',
        );
    }

    public function columns()
    {
        return array(
//            'tiin_id' => 'Tipo de Institución',
            'subt_nombre' => 'Subtipo',
            'subt_codigo' => 'Código',
            'subt_abrev' => 'Abreviatura',
            'subt_estado' => 'Estado',
        );
    }

    public function rules()
    {
        return array(
/*            'tiin_id' => array(
                array('not_empty'),
            ),
*/            
            'subt_nombre' => array(
                array('not_empty'),
            ),
            'subt_codigo' => array(
                array('not_empty'),
            ),
            'subt_abrev' => array(
                array('not_empty'),
            ),
            'subt_estado' => array(
                array('not_empty'),
            )
        );
    }

    protected $_table_name = 'subtipo_institucion';
    protected $_primary_key = 'subt_id';

    public function get_all($tiin_id)
    {
        return ORM::factory('Subtipo')->where('tiin_id', '=', $tiin_id)->find_all()->as_array('subt_id','subt_abrev');
    }
}
