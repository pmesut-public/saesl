<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Persona extends Model_Saes {

	// Tipos
	const TIPO_CONTACTO = 'contacto';
	const TIPO_COMISION = 'comision';
	
	public static $tipos = array(
		self::TIPO_CONTACTO => 'Contacto',
		self::TIPO_COMISION => 'Comisión',
	);
	
	public function labels()
	{
		return array(
			'pers_nombres'  => 'Nombres',
			'pers_apellidos'=> 'Apellidos',
			'pers_telefono' => 'Número',
			'pers_correo'   => 'Correo',
			'pers_tipo'     => 'Contacto',
		);
	}
	
	protected $_table_name = 'persona';

	protected $_primary_key = 'pers_id';

	protected $_belongs_to = array(
		'oAcreditado' => array(
			'model'         => 'Acreditado',
			'foreign_key'   => 'acre_id',
		),
	);
}
