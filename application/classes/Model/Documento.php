<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_Documento extends Model_Saes
{

	protected $_upload = 'uploads/licenciamiento/';

	protected $_table_name = 'documento';

	protected $_primary_key = 'docu_id';

	protected $_belongs_to = array(
		'oAcreditado' => array(
			'model' => 'Acreditado',
			'foreign_key' => 'acre_id',
		),
	);

	protected $_has_many = array(
		'aAutoevaluacionDocumento' => array(
			'model' => 'AutoevaluacionDocumento',
			'foreign_key' => 'docu_id',
		),
		'aAutoFormato' => array(
            'model' => 'AutoevaluacionFormato',
            'foreign_key' => 'docu_id',
        ),		
		'aAutoCondicion' => array(
			'model' => 'AutoevaluacionCondicion',
			'foreign_key' => 'docu_id',
		),
	);

	const PENDIENTE = 0;

	const CONCLUIDO = 1;

	public static $estados = [
		self::PENDIENTE => 'Pendiente',
		self::CONCLUIDO => 'Concluido'
	];

	const ACTIVO = 'activo';

	const ELIMINADO = 'eliminado';

	const REEMPLAZADO = 'reemplazado';

	public static $estados_documento = [
		self::ACTIVO => 'activo',
		self::ELIMINADO => 'eliminado',
		//self::REEMPLAZADO => 'reemplazado'
	];

	const VIGENCIA_BUENA   = 1;

	const VIGENCIA_REGULAR = 3;

	const VIGENCIA_MALA    = 5;

	public static $vigencias_documento = [
		self::VIGENCIA_BUENA 	=> 'Vigente',
		self::VIGENCIA_REGULAR  => 'Próximo a perder vigencia',
		self::VIGENCIA_MALA     => 'Fuera de vigencia',
	];

	/*const BUENA = 3;

	const REGULAR = 4;

	const MALA = 5;*/

	const BUENA = 5;

	const REGULAR = 4;

	const MALA = 3;

	const MENOS25 = 6;

	const ENTRE2550 = 7;

	const ENTRE5175 = 8;

	const MAYOR75 = 9;


/*public static $sub_estados: [
		{parentId: 1, id: 3, display: 'buena', color: 'green'},
		{parentId: 1, id: 4, display: 'regular', color: 'yellow'},
		{parentId: 1, id: 5, display: 'mala', color: 'red'},
		{parentId: 0, id: 6, display: 'menos del 25% de avance', color: 'red'},
		{parentId: 0, id: 7, display: 'entre 25% y 50% de avance', color: 'red'},
		{parentId: 0, id: 8, display: 'entre 51% y 75% de avance', color: 'yellow'},
		{parentId: 0, id: 8, display: 'mayor a 75% de avance', color: 'yellow'},
	]*/

	public static $sub_estados = [
		self::BUENA => [
			'parentId' => 1,
			'id' => self::BUENA,
			'display' => 'Buena',
			'color' => 'green'
		],
		self::REGULAR => [
			'parentId' => 1,
			'id' => self::REGULAR,
			'display' => 'Regular',
			'color' => 'yellow'
		],
		self::MALA => [
			'parentId' => 1,
			'id' => self::MALA,
			'display' => 'Mala',
			'color' => 'red'
		],
		self::MENOS25 => [
			'parentId' => 0,
			'id' => self::MENOS25,
			'display' => 'Menos del 25% de avance',
			'color' => 'red'
		],
		self::ENTRE2550 => [
			'parentId' => 0,
			'id' => self::ENTRE2550,
			'display' => 'Entre 25% y 50% de avance',
			'color' => 'red'
		],
		self::ENTRE5175 => [
			'parentId' => 0,
			'id' => self::ENTRE5175,
			'display' => 'Entre 51% y 75% de avance',
			'color' => 'yellow'
		],
		self::MAYOR75 => [
			'parentId' => 0,
			'id' => self::MAYOR75,
			'display' => 'Mayor a 75% de avance',
			'color' => 'yellow'
		],
	];

	public static $niveles_calidad = [
		self::BUENA => 'Buena',
		self::REGULAR => 'Regular',
		self::MALA => 'Mala'
	];

	public function upload_archivo($acre_id, $file)
	{
		if (!$this->_check_file($file))
			return false;

		$dir = "{$this->_upload}{$acre_id}/";
		$full_dir = APPPATH . $dir;

		if (!is_dir($full_dir)) {
			mkdir($full_dir, 0777);
		}

		$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
		$name = pathinfo($file['name'], PATHINFO_FILENAME);
		$filename = Text::random() . "-" . date('YmdHis') . ".{$ext}";

		if (!Upload::save($file, $filename, $full_dir)) {
			return false;
		}

		return ['path' => $dir . $filename, 'name' => $name, 'ext' => $ext];
	}

	public static function get_documento($id)
	{
		return ORM::factory('Documento')
			->where('docu_id', '=', $id)
			->find();
	}

	public function get_path()
	{
		return $this->_upload;
	}

	public function has_formatos() {
		return ($this->aAutoFormato->find_all()->count() || $this->aAutoCondicion->find_all()->count()) > 0;
	}

	/**
	 * Retorna la vigencia de un documento.
	 * 
	 * @param  int    $olgura
	 * @param  int    $diasActualizacion
	 * @return string
	 */
	public static function get_vigencia($olgura, $diasActualizacion)
	{
		$vigencia = 0;

		if ($olgura >= $diasActualizacion) {
			
			$vigencia = self::VIGENCIA_BUENA;
		}
		else if($olgura < $diasActualizacion && $olgura > 0){

			$vigencia = self::VIGENCIA_REGULAR;
		}
		else{

			$vigencia = self::VIGENCIA_MALA;
		}

		return $vigencia;
	}

	public static function documentos_sueltos($acreditado_id, $autoevaluacion_id = null){

		/* Documentos del Repositorio */

		$oDocumentosRepositorio = ORM::factory('Documento')
									->where('acre_id', '=', $acreditado_id)
									->where('docu_estado', '=', 'activo')
									->find_all()
									->as_json_array('docu_id');

		/* Documentos del Formatos */

		$documentosFormatoA = QueryBuilder::factory('fichas/docs_formatoAC.sql', [
			':acre_id' => $acreditado_id,
			':formato' => 'A'
		])->dict('docu_id')->get();

		$documentosFormatoB = QueryBuilder::factory('fichas/docs_formatoB.sql', [
			':acre_id' => $acreditado_id,
		])->dict('docu_id')->get();

		$documentosFormatoC = QueryBuilder::factory('fichas/docs_formatoAC.sql', [
			':acre_id' => $acreditado_id,
			':formato' => 'C'
		])->dict('docu_id')->get();

		$aDocumentosFormatos = array_merge(
			array_keys($documentosFormatoA), array_keys($documentosFormatoB), array_keys($documentosFormatoC)
		);

		/* Resta de documentos */

		$oDocumentosEvaluacion = array_diff(array_keys($oDocumentosRepositorio), $aDocumentosFormatos);

		/* Documentos del usados en la Autoevaluación */

		$aDocumentosUsados = QueryBuilder::factory('fichas/documentos_autoevaluacion.sql', [
			':autoevaluacion_id' => $autoevaluacion_id,
		])->dict('docu_id')->get();

		$aDocumentosSueltosIds = array_diff($oDocumentosEvaluacion, array_keys($aDocumentosUsados));

		$aDocumentosSueltosIds = count($aDocumentosSueltosIds) ? $aDocumentosSueltosIds : [0];

		$oDocumentosSueltos = ORM::factory('Documento')
									->where('docu_id', 'IN', array_values($aDocumentosSueltosIds))
									->find_all()
									->as_json_array('docu_id');

		return $oDocumentosSueltos;
	}

}
