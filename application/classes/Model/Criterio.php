<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Criterio extends Model_Saes {

	public function labels()
	{
		return array(
			'crit_codigo' => 'Código',
			'crit_descripcion' => 'Descripción',
			'esta_id' => 'Estandar',
			'crit_estado' => 'Estado',
		);
	}

	public function rules()
	{
		return array(
			'crit_codigo' => array(
				array('not_empty'),
			),
			'esta_id' => array(
				array('not_empty'),
			),
			'crit_estado' => array(
				array('not_empty'),
			),
		);
	}

	public function filters()
	{
		return array(
			'crit_descripcion' => array(
				array('Model_Saes::filter_empty'),
			),
		);
	}

	protected $_table_name = 'criterio';
	protected $_primary_key = 'crit_id';
	protected $_belongs_to = array(
		'oEstandar' => array(
			'model' => 'Estandar',
			'foreign_key' => 'esta_id',
		),
	);

	/* public function aDetalle($auto_id)
	  {
	  return ORM::factory('Autoevaluacion', $auto_id)->aDetalle
	  ->with('oEstandar')
	  ->where('crit_id', '=', $this->crit_id)
	  ->find_all();
	  } */

	public function title()
	{
		return $this->crit_codigo . '. ' . $this->crit_descripcion;
	}

	public function titulo()
	{
		return $this->crit_descripcion;
	}

	/* public function assessment($auto_id)
	  {
	  return $this
	  ->select(array(DB::expr('count(esta_id)'), 'total_est'))
	  ->select(array(DB::expr('SUM(if(aude_cumplimiento = 1, 1, 0))'), 'sum_est'))
	  ->select(array(DB::expr('SUM(if(aude_cumplimiento = 1, aude_aceptacion, 0))'), 'puntaje_est'))
	  ->with('oFactor')
	  ->join('estandar')
	  ->using('crit_id')
	  ->join('autoevaluacion_detalle')
	  ->using('esta_id')
	  ->where('auto_id', '=', $auto_id)
	  ->group_by('criterio.crit_id');
	  } */

	/* public function dimension($dime_id)
	  {
	  return $this
	  ->where('dime_id', '=', $dime_id)
	  ->find_all();
	  } */

	public static function get_by_audeid($aude_id)
	{
		return ORM::factory('AutoevaluacionCriterio')
				->select('aucr_codigo', 'aucr_descripcion')
				->where('aude_id', '=', $aude_id)
				->find_all()
				->as_array('aucr_codigo', 'aucr_descripcion');
	}

}
