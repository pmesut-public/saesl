<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_DocumentoInstitucional extends Model_Saes {
    
    // Status
    const STATUS_PENDIENTE = 'pendiente';
    const STATUS_PROCESADO = 'procesado';
    const STATUS_ANULADO = 'anulado';

    public static $statuses = array(
        self::STATUS_PENDIENTE => 'Pendiente',
        self::STATUS_PROCESADO => 'Procesado',
	self::STATUS_ANULADO => 'Anulado',
    );
    protected $_upload = 'uploads/docusinstitucionales/';
    protected $_table_name = 'datos_institucionales';
    protected $_primary_key = 'dain_id';
    
    public function labels()
	{
		return array(
			'dain_name' => 'Nombre',
			'dain_path' => 'Archivo',
			'dain_estado'=> 'Estado',
		);
	}
    
    public function upload_archivo($file)
    {				
	if ( ! $this->_check_file($file))
            return FALSE;
		
	$dir = "{$this->_upload}";
	$full_dir = APPPATH.$dir;
		
	if ( ! is_dir($full_dir))
	{
            mkdir($full_dir, 0777);
	}
		
	$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        $name = pathinfo($file['name'], PATHINFO_FILENAME);
	$filename = Text::random()."-".date('YmdHis').".{$ext}";
		
        if ( ! Upload::save($file, $filename, $full_dir))
        {
            return FALSE;
        }
        
        $path = $dir.$filename;
        $this->save_data($name, $path);
    }
    
    public function save_data($name, $path)
    {
        $query = DB::insert($this->_table_name, 
                array('dain_fecha_reg', 'dain_estado', 'dain_name', 'dain_path')
            );
        $query->values(array(
            date('Y-m-d H:i:s'),
            self::STATUS_PENDIENTE,
            $name,
            $path
        ));

	$query->execute();
    }

    public static function get_documento($id)
    {
        return ORM::factory('DocumentoInstitucional')
            ->where('dain_id', '=', $id)
            ->find();
    }
	
	
}
