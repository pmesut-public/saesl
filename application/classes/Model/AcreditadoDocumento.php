<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AcreditadoDocumento extends Model_Saes {

	protected $_table_name = 'acreditado_documento';

	protected $_primary_key = 'acdo_id';

	protected $_upload = 'uploads/documents/';
	
	protected $_belongs_to = array(
		'oAcreditado' => array(
			'model' => 'Acreditado',
			'foreign_key' => 'acre_id',
		),
	);
}
