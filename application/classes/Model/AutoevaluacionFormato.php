<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_AutoevaluacionFormato extends Model_Saes
{

	protected $_table_name = 'autoevaluacion_formato';

	protected $_primary_key = 'aufo_id';

	protected $_belongs_to = array(
		'oFormato' => array(
			'model' => 'Formato',
			'foreign_key' => 'form_id',
		),
		'oAutoevaluacion' => array(
			'model' => 'Autoevaluacion',
			'foreign_key' => 'auto_id',
		),
		'oDocumento' => array(
			'model' => 'Documento',
			'foreign_key' => 'docu_id',
		),
	);


}
