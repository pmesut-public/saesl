<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Jean Carlo Canevello Salazar <jean.canevello@gmail.com>
 * @copyright  (c) 2018 ProCalidad
 */
class Model_Filiallocal extends Model_Saes {

    public function labels() {
        return array(
            'inst_nivel' => 'Nivel',
            'parent_id' => 'Pertenece a',
            'inst_nombre_sede' => 'Nombre Filial/Local',
            'inst_region' => 'Región',
            'inst_prov' => 'Provincia',
            'inst_dist' => 'Distrito',
            //'inst_gestion' => 'Gestión',
            'inst_codigo' => 'Código',
            'inst_telefono' => 'Teléfono',
            'inst_web' => 'Web',
            'inst_direccion' => 'Dirección',
            'inst_cri' => 'CRI',
            'inst_principal' => 'Director',
            'inst_situacion' => 'Situación',
            'inst_revalidado' => 'Revalidado',
            'inst_estado' => 'Estado',
            'inst_comite_sineace' => 'Registro SINEACE',
            'tiin_id' => '',
            'inst_nombre' => '',
            'parent_senior_id' => ''
        );
    }

    public function rules() {
        return array(
            'parent_id' => array(
                array('not_empty'),
            ),
            'inst_nivel' => array(
                array('not_empty'),
            ),
            /*'inst_obac_id' => array(
                array('not_empty'),
            ),*/
            /*'inst_nombre' => array(
                array('not_empty'),
                array(array($this, 'unique_name'), array(':validation', ':field')),
            ),
            'inst_region' => array(
                array('not_empty'),
            ),
            'inst_comite_sineace' => array(
                array('not_empty'),
            ),*/
        );
    }

    protected $_table_name = 'institucion';
    protected $_primary_key = 'inst_id';
    protected $_belongs_to = array(
        'oTipoInstitucion' => array(
            'model' => 'TipoInstitucion',
            'foreign_key' => 'tiin_id',
        ),
    );
    protected $_has_many = array(
        'aCarrera' => array(
            'model' => 'Carrera',
            'foreign_key' => 'inst_id',
        ),
    );
}
