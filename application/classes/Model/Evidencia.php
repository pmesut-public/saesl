<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_Evidencia extends Model_Saes {

	protected $_table_name = 'autoevaluacion_evidencia';
	protected $_primary_key = 'auev_id';
	protected $_belongs_to = array(
		'oDetalle' => array(
			'model' => 'AutoevaluacionDetalle',
			'foreign_key' => 'aude_id',
		),
	);
	protected $_has_many = array(
		'aAutoevaluacionDocumento' => array(
			'model' => 'AutoevaluacionDocumento',
			'foreign_key' => 'auev_id',
		),
	);

	public static function get_by_audeid($aude_id)
	{
		return ORM::factory('Evidencia')
				->select('auev_id', 'auev_titulo')
				->where('aude_id', '=', $aude_id)
				->find_all()
				->as_array('auev_id', 'auev_titulo');
	}

}
