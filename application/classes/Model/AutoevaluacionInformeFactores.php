<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AutoevaluacionInformeFactores extends Model_Saes {
	
	protected $_table_name = 'autoevaluacion_informe_factores';
	
	protected $_primary_key = 'auif_id';
	
	protected $_created_column = NULL;
	
	protected $_belongs_to = array(
		'oAutoInforme' => array(
			'model' => 'AutoevaluacionInforme',
			'foreign_key' => 'auto_id',
		),
	);
	
}
