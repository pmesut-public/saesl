<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_AutoevaluacionCondicion extends Model_Saes
{

	protected $_table_name = 'autoevaluacion_condicion';

	protected $_primary_key = 'auco_id';

	protected $_belongs_to = array(
		'oDimension' => array(
			'model' => 'Dimension',
			'foreign_key' => 'dime_id',
		),
		'oAutoevaluacion' => array(
			'model' => 'Autoevaluacion',
			'foreign_key' => 'auto_id',
		),
		'oDocumento' => array(
			'model' => 'Documento',
			'foreign_key' => 'docu_id',
		),
	);


}
