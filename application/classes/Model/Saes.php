<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Saes extends ORM {
	
	protected $_upload = 'uploads/';
	
	protected $_labels = array();
	
	// @todo remove non static property
	public $estado = array(
		'activo' => 'Activo',
		'eliminado' => 'Eliminado',
	);
	
	const STATUS_ACTIVO = 'activo';
	const STATUS_ELIMINADO = 'eliminado';
	
	public static $estados = array(
		self::STATUS_ACTIVO => 'Activo',
		self::STATUS_ELIMINADO => 'Eliminado',
	);
	
	protected $_created_column = TRUE;
	
	public function __construct($id = NULL)
	{
		$prefix = substr($this->_primary_key, 0, 4);
		$column = $prefix.'_fecha_reg';
		
		if ($this->_created_column === TRUE)
		{
			$this->_created_column = array(
				'column' => $column,
				'format' => 'Y-m-d H:i:s',
			);
		}
		
		parent::__construct($id);
	}
	
	/**
	 * Saves a file
	 * @param  string  $column
	 * @param  string  $filename
	 * @param  array  $file  $_FILES['file']
	 * @param  array  $params
	 * @return boolean
	 */
	public function save_file($column, $filename, $file, $params = NULL)
	{
		if ( ! $this->_check_file($file, $params))
			return FALSE;
		
		$dir = $this->_upload;
		$full_dir = APPPATH.$dir;
		
		if ( ! is_dir($full_dir))
		{
			mkdir($full_dir);
		}
		
		$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
		$filename = URL::title($filename, '-', TRUE).'.'.$ext;
		
        if ( ! Upload::save($file, $filename, $full_dir))
        {
            return FALSE;
        }
 
		$this->{$column} = $dir.$filename;
		return TRUE;
	}
	
	protected function _check_file($file, array $params = NULL)
	{
		if (! Upload::valid($file) OR
			! Upload::not_empty($file))
			return FALSE;
		
		if (! Upload::type($file, array('pdf', 'xls', 'xlsx', 'docx', 'doc', 'png', 'jpg', 'jpge')))
			return FALSE;
		
		if ($params !== NULL AND ! Upload::type($file, $params))
			return FALSE;
		
		return TRUE;
	}
	
	public static function filter_empty($value)
	{
		return $value ? $value : NULL;
	}
	
	public function values(array $values, array $expected = NULL, $clean = FALSE)
	{
		if ($clean)
		{
			foreach ($values as $key => $value)
			{
				if ($value === '') $values[$key] = NULL;
			}
		}
		
		return parent::values($values, $expected);
	}
}
