<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Actividad extends Model_Saes {

	public function labels()
	{
		return array(
			'acti_descripción' => 'Descripción',
			'acti_costo'       => 'Costo S/.',
			'acti_tiempo'      => 'Tiempo en h/hombre',
			'acti_responsable' => 'Responsable',
			'acti_estandares'  => 'Estándares',
		);
	}
	
	protected $_table_name = 'actividad';

	protected $_primary_key = 'acti_id';

	protected $_belongs_to = array(
		'oAutoevaluacion' => array(
			'model' => 'Autoevaluacion',
			'foreign_key' => 'auto_id',
		),
	);
}
