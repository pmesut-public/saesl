<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Institucion extends Model_Saes {

    /**
     * Tabla asociada al Modelo.
     */
    protected $_table_name = 'institucion';

    /**
     * Primary Key (PK) del Modelo.
     */
    protected $_primary_key = 'inst_id';

    protected $_belongs_to = array(
        'oTipoInstitucion' => array(
            'model'       => 'TipoInstitucion',
            'foreign_key' => 'tiin_id',
        ),
    );
    protected $_has_many = array(
        'aCarrera' => array(
            'model'       => 'Carrera',
            'foreign_key' => 'inst_id',
        ),
    );

    const C_REG    = 1;
    const C_NO_REG = 0;

    public static $comites = array(
        self::C_REG    => 'Registrado',
        self::C_NO_REG => 'No registrado',
    );

    const N_SEDE   = 1;
    const N_FILIAL = 2;
    const N_LOCAL  = 3;

    public static $niveles = array(
        self::N_SEDE   => 'Sede Central',
        self::N_FILIAL => 'Filial',
        self::N_LOCAL  => 'Local'
    );

    const IESP = 'IESP';
    const IEST = 'IEST';
    const ISE  = 'ISE';
    const ESFA = 'ESFA';

    public static $subtipos = array(
        self ::IESP => 'IESP',
        self ::IEST => 'IEST',
        self ::ISE  => 'ISE',
        self ::ESFA => 'ESFA',
    );

    const G_PUBLICA = 'PUBLICA';
    const G_PRIVADA = 'PRIVADA';

    public static $gestiones = array(
        self::G_PUBLICA => 'PUBLICA',
        self::G_PRIVADA => 'PRIVADA',
    );

    const TIPO_SEDE   = 'sede';
    const TIPO_FILIAL = 'filial';
    const TIPO_LOCAL  = 'local';

    public static $tipos = array(
        self::TIPO_SEDE   => 'Sede',
        self::TIPO_FILIAL => 'Filial',
        self::TIPO_LOCAL  => 'Local',
    );
    public static $attenders = array(
        Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD => 'SUNEDU',
        Model_ObjetoAcreditacion::TIPO_INSTITUTO   => 'MINEDU',
    );

    /**
     * Campos del AdminForm
     *
     * @return array
     */
    public function labels() {
        return array(
            //'inst_nivel'        => 'Nivel',
            'inst_codigo'         => 'Código',
            'inst_nombre'         => 'Nombre',
            'tiin_id'             => 'Tipo',        
            'subt_id'             => 'Subtipo',
            'inst_obac_id'        => 'Objeto de Licenciamiento',          
            //'parent_id'         => 'Pertenece a',
            //'inst_subtipo'      => 'Subtipo',
            'inst_region'         => 'Región',
            'inst_prov'           => 'Provincia',
            'inst_dist'           => 'Distrito',
            //'inst_nombre_sede'    => 'Nombre sede',
            'inst_gestion'        => 'Gestión',           
            
            'inst_telefono'       => 'Teléfono',
            'inst_web'            => 'Web',
            'inst_direccion'      => 'Dirección',
            'inst_cri'            => 'CRI',
            'inst_principal'      => 'Director',
            'inst_situacion'      => 'Situación',
            //'inst_revalidado'     => 'Revalidado',
            'inst_estado'         => 'Estado',
            'inst_comite_sineace' => 'Registro SINEACE',
            //'inst_tipo'         => 'Es una Sede/Filial/Local',
            //'inst_parent'       => 'Pertenece a',
        );
    }

    /**
     * Validación Fronted
     *
     * @return array
     */
    public static $labels_required = [
        'inst_nombre',
        'tiin_id',
        'inst_obac_id',
        'subt_id',
        'inst_region',
        'inst_prov',
        'inst_dist',
        'inst_gestion',
        'inst_direccion',
        'inst_estado',
        'inst_comite_sineace'
    ];

    /**
     * Validación Backend
     *
     * @return array
     */
    public function rules() {
        return array(
/*            
            'tiin_id' => array(
                array('not_empty'),
            ),
*/            
            'inst_nombre' => array(
                array('not_empty'),
                array(array($this, 'unique_name'), array(':validation', ':field')),
            ),
            'inst_region' => array(
                array('not_empty'),
            ),
            'inst_comite_sineace' => array(
                array('not_empty'),
            ),
        );
    }

    public static function attender_char($tiin_id) {
        return substr(self::$attenders[$tiin_id], 0, 1);
    }
    
    public function save(Validation $validation = NULL) {
        if($this->subt_id){
            $subtipo = ORM::factory('Subtipo', $this->subt_id);
            $this->inst_subtipo = $subtipo->subt_abrev;
        }
        parent::save($validation);
    }
    
    public function unique_name($array, $field) {
        $model = ORM::factory($this->object_name())
                ->where('inst_subtipo', '=', $array['inst_subtipo'] ?: NULL)
                ->where('inst_nombre', '=', $array['inst_nombre'])
                ->where('inst_region', '=', $array['inst_region'])
                ->where('inst_prov', '=', $array['inst_prov'] ?: NULL)
                ->where('inst_dist', '=', $array['inst_dist'] ?: NULL)
                ->where('inst_nombre_sede', '=', $array['inst_nombre_sede'] ?: NULL)
                ->where('inst_nivel', '=',$array['inst_nivel']?:NULL)
                ->find();

        if ($this->loaded()) {
            return (!($model->loaded() AND $model->pk() != $this->pk()));
        }

        return (!$model->loaded());
    }

    public function add_name() {
        $concat = "concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede)";

        return $this->select(DB::expr("$concat name"));
    }

    public function find($add = TRUE) {
        if ($add) {
            $this->add_name();
        }

        return parent::find();
    }

    public function find_all($add = TRUE) {
        if ($add) {
            $this->add_name();
        }

        return parent::find_all();
    }

    public static function get_all() {
        $concat = "concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede)";

        return DB::select('inst_id', 'inst_nombre')
                        ->select(DB::expr("$concat inst_name"))
                        ->from('institucion')
                        ->where('inst_estado', '=', 'activo')
                        ->order_by('tiin_id')
                        ->order_by('inst_nombre')
                        ->execute()
                        ->as_array('inst_id', 'inst_name')
        ;
    }
    
    public static function get_all_aditional(array $inst_ids = []) {
        $concat = "concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede)";

        $query = DB::select('inst_id', 'inst_nombre')
                        ->select(DB::expr("$concat inst_name"))
                        ->from('institucion');
            
        $query = $query->where_open();
            $query = $query->where('inst_estado', '=', 'activo');
            (count($inst_ids) > 0)?$query = $query->or_where('inst_id', 'IN', $inst_ids):null;
            $query = $query->where_close();
        
        return $query->order_by('tiin_id')
                        ->order_by('inst_nombre')
                        ->execute()
                        ->as_array('inst_id', 'inst_name')
        ;
    }

    public static function get_all2() {

        return DB::select('inst_id', 'inst_nombre')
                        ->from('institucion')
                        ->where('inst_estado', '=', 'activo')
                        ->where('inst_nivel', '=', '1')
                        ->order_by('tiin_id')
                        ->order_by('inst_nombre')
                        ->execute()
                        ->as_array('inst_id', 'inst_nombre');
    }

    /*
     * Obtiene todas las regiones (sin duplicidad) de la tabla institucion
     */

    public static function get_regiones() {
        return DB::select(DB::expr("distinct inst_region from institucion order by inst_region"))
                        ->execute()
                        ->as_array('inst_region', 'inst_region');
    }
    
    /**
     * Obtiene los subtipos de una Institución.
     *
     * @param  $tiin_id
     * @return array
     */
    public static function get_subtiposByTiinId($tiin_id)
    {
        return DB::select(DB::expr("subt_id as subt_id, CONCAT(subt_abrev, ' - ', subt_nombre) AS subt_abrev"))
            ->from('subtipo_institucion')
            ->where('subt_estado', '=', Model_Saes::STATUS_ACTIVO)
            ->where('tiin_id', '=', $tiin_id)
            ->order_by('subt_id', 'ASC')
            ->execute()
            ->as_array('subt_id', 'subt_abrev');
    }

    public static function get_parent($tiin_id = NULL, $id = NULL) {
        $concat = "concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede)";

        $query = DB::select('inst_id', 'inst_nombre')
                ->select(DB::expr("$concat inst_name"))
                ->from('institucion');

        if ($tiin_id) {
            $query->where('tiin_id', '=', $tiin_id);
        }

        return $query->where('inst_estado', '=', 'activo')
                        ->order_by('tiin_id')
                        ->order_by('inst_nombre')
                        ->execute()
                        ->as_array('inst_id', 'inst_name')
        ;
    }

    public static function get_parentId($id) {
        $id = var_filter($id, FILTER_SANITIZE_NUMBER_INT);
        return DB::select('inst_id', 'inst_nombre')
                        ->from('institucion')
                        ->where('inst_id', '=', $id)
                        ->where('inst_estado', '=', 'activo')
                        ->order_by('tiin_id')
                        ->order_by('inst_nombre')
                        ->execute()
                        ->as_array('inst_id', 'inst_name');
    }

    public static function get_by_nivel($parent_id)
    {
        list($inst_id, $nivel) = explode("-", $parent_id);

        $nivel_query = $nivel == Model_Institucion::N_LOCAL ? Model_Institucion::N_FILIAL : NULL;

        $query_union = DB::select('inst_id')
            ->select(DB::expr("(CASE inst_nivel WHEN 2 THEN CONCAT('Filial - ', inst_nombre_sede) WHEN 3 THEN CONCAT('Local - ', inst_nombre_sede) ELSE NULL END) as nombre"))
            ->from('institucion')
            ->where('inst_nivel', '=', $nivel_query)
            ->where('parent_senior_id', '=', $inst_id);
        //->union($query_union)
        //->execute()
        //->as_array('inst_id', 'nombre');

        $instituciones = DB::select('inst_id')
            ->select(DB::expr("CONCAT(inst_nombre, ' - Sede Central') as nombre"))
            ->from('institucion')
            ->where('inst_id', '=', $parent_id)
            ->union($query_union)
            ->execute()
            ->as_array('inst_id', 'nombre');

        return $instituciones;
    }

    public static function get_filial_local($inst_id)
    {
        $query_union = DB::select('inst_id')
            ->select(DB::expr("(CASE inst_nivel WHEN 2 THEN CONCAT('Filial - ', inst_nombre_sede) WHEN 3 THEN CONCAT('Local - ', inst_nombre_sede) ELSE NULL END) as nombre"))
            ->from('institucion')
            ->where('parent_senior_id', '=', $inst_id);

        return DB::select('inst_id')
            ->select(DB::expr("CONCAT(inst_nombre, ' - Sede Central') as nombre"))
            ->from('institucion')
            ->where('inst_id', '=', $inst_id)
            ->union($query_union)
            ->execute()
            ->as_array('inst_id', 'nombre');
    }


    // Metodo para generar estructura herarquica de institucion
    public static function reportpdf_getSearchCarrerasPersonas($carreras, $personas, $inst_id) {
        $data = [];
        foreach ($carreras as $i => $value) {
            
            if(@$value["inst_id"] == $inst_id){
                $p_encontro = false;
                $p_general = null; 
                $data[$i] = $value;
                $data[$i]["personas"] = [];
                            
                foreach ($personas as $j => $val) {
                    if(@$val["inst_id"] == $inst_id && @$val["carr_id"] == $value["carr_id"]){
                        $data[$i]["personas"][] = $val;
                        $p_encontro = true;
                    }
                    if(!@$val["carr_id"]){
                        $p_general = $val;
                    }
                }   
                if(!$p_encontro){
                    $data[$i]["personas"][] = $p_general;
                }             
            }
           
        }        
        return $data;
    }
    
    // Metodo para generar estructura herarquica de institucion
    private static function reportpdf_getFormatSedeInstitucion($institucion) {
        return [
                'inst_id' => $institucion["inst_id"],
                'parent_id' => $institucion["parent_id"],
                'institucion' => $institucion["inst_nombre"],
                'texto_institucion' => '... Texto de Sumatoria total ...',
                'sede_institucion' => [
                    'direccion'    => $institucion["inst_direccion"],
                    'region'       => $institucion["inst_region"],
                    'provincia'    => $institucion["inst_prov"],
                    'distrito'     => $institucion["inst_dist"],
                    'tipo_gestion' => $institucion["inst_gestion"],
                ],
                'sede_carreras' => [
                    'lista_carreras' => [],
                    'texto_carreras' => '...'
                ],
                'sede_locales' => [
                    'lista_locales' => [],
                ],
                'sede_filiales' => []
            ];
    }

    // Metodo para generar estructura herarquica de institucion
    private static function reportpdf_getFormatFiliales($filial, $carreras) {
        return [
            'inst_id' => $filial["inst_id"],
            'parent_id' => $filial["parent_id"],
            'filial_nombre' => ($filial["inst_nombre_sede"])?$filial["inst_nombre_sede"]:$filial["inst_nombre"],
            'filial_texto'  => '... Texto de Sumatoria de la Filial...',
            'filial_data'   => [
                'direccion'    => $filial["inst_direccion"],
                'region'       => $filial["inst_region"],
                'provincia'    => $filial["inst_prov"],
                'distrito'     => $filial["inst_dist"]
            ],
            'filial_carreras' => self::reportpdf_getFormatCarreras($carreras),
        ];
    }
    
    // Metodo para generar estructura herarquica de institucion
    private static function reportpdf_getFormatCarreras($carreras) {
        $data = [];
        foreach ($carreras as $value) {
            
            $carrera_data = [
                'comite'   => (@$value["personas"] && @$value["personas"][0])?@$value["personas"][0]["pers_nombres"]. " ".@$value["personas"][0]["pers_apellidos"] :"",
                'cargo'    => (@$value["personas"] && @$value["personas"][0])?@$value["personas"][0]["pers_cargo"]:"",
                'email'    => (@$value["personas"] && @$value["personas"][0])?@$value["personas"][0]["pers_correo"]:"",
                'telefono' => (@$value["personas"] && @$value["personas"][0])?@$value["personas"][0]["pers_telefono"]:"",
            ];
            $data[] = [
                    'carr_id' => $value["carr_id"],
                    'carrera_nombre' => $value["carr_nombre"],
                    'carrera_data' => $carrera_data,
                ];
        }
        return $data;
    }

    // Metodo para generar estructura herarquica de institucion
    private static function reportpdf_getFormatListaLocales($local, $carreras) {        
        
        return [
            'inst_id' => $local["inst_id"],
            'parent_id' => $local["parent_id"],
            'local_nombre' =>  ($local["inst_nombre_sede"])?$local["inst_nombre_sede"]:$local["inst_nombre"],
            'local_data' => [
                'direccion'    => $local["inst_direccion"],
                'region'       => $local["inst_region"],
                'provincia'    => $local["inst_prov"],
                'distrito'     => $local["inst_dist"]
            ],
            'local_carreras' => self::reportpdf_getFormatCarreras($carreras),
        ];
    }

    // Estructura jerarquica de la institucion
    public static function getStructuraHerarquica($pase_inst_id){

        $results = DB::select('inst_id','inst_nombre', 'inst_nivel', 'parent_id', 'parent_senior_id', 'inst_nombre_sede', 'inst_direccion', 'inst_estado', 'inst_region', 'inst_prov', 'inst_gestion', 'inst_dist', 'inst_tipo')
                ->from('institucion')
                ->where('inst_id', '=', $pase_inst_id)
                ->where('inst_estado', 'IN', ['activo'])
                ->execute()->as_array();

        $institucion = [];
        $personas =[];

        $data = [];
        
        if (@$results[0]) {
            
            $resultschild = DB::select('inst_id','inst_nombre', 'inst_nivel', 'parent_id', 'inst_nombre_sede', 'inst_direccion', 'inst_estado', 'inst_region', 'inst_prov', 'inst_gestion','inst_dist', 'inst_tipo')
                    ->from('institucion')
                    ->where('parent_senior_id', '=', $pase_inst_id)
                    ->where('inst_estado', 'IN', ['activo'])
                    ->order_by("inst_nivel")->execute()->as_array();
            
            $inst_ids = [];
            $inst_ids[] = $pase_inst_id;
            
            foreach ($resultschild as $i => $value) {                
                $inst_ids[] =  $value["inst_id"];
            }
            
            $personas = [];
            $carreras = [];
            
            if(count($inst_ids) > 0){
                
                $personas = DB::select()->from('persona')
                    ->join('acreditado')
                    ->on('acreditado.acre_id', '=', 'persona.acre_id')
                    ->join('acreditado_institucion')  
                    ->on('acreditado_institucion.acre_id', '=', 'acreditado.acre_id')
                    ->join('acreditado_objeto', 'left')  
                    ->on('acreditado_objeto.acin_id', '=', 'acreditado_institucion.acin_id')
                    ->where('acreditado_institucion.inst_id', 'IN', $inst_ids)
                    ->where('persona.pers_tipo','=','contacto')
//                    ->where('carrera.carr_estado', 'IN', ['activo'])
                    ->execute()->as_array();

                $carreras = DB::select()->from('carrera')
                                        ->where('inst_id', 'IN', $inst_ids)
                                        ->where('carr_estado', 'IN', ['activo'])
                                        ->execute()
                                        ->as_array();
            }            
            
            $institucion = $results[0];
            $institucion["carreras"] = self::reportpdf_getSearchCarrerasPersonas($carreras, $personas, $pase_inst_id);          
            $institucion["locales"] = [];
            $institucion["filiales"] = [];
            $institucion["total_carreras"] = 0;
            $institucion["total_locales"] = 0;
            
            
            $data = self::reportpdf_getFormatSedeInstitucion($institucion);
            $data["sede_carreras"]["lista_carreras"] = self::reportpdf_getFormatCarreras($institucion["carreras"]);
            
            $resultschild3 = $resultschild;
            
            foreach ($resultschild as $k => $val2) {
                if (@$val2["inst_nivel"] == 3 && @$val2["parent_id"] == $pase_inst_id) {
                    $data["sede_locales"]["lista_locales"][] = self::reportpdf_getFormatListaLocales($val2, self::reportpdf_getSearchCarrerasPersonas($carreras, $personas, $val2["inst_id"]));
                    $data["sede_locales"]['texto_locales'] = 'Texto de Sumatoria';
                }
            }
            
            foreach ($resultschild as $i => $val1) {
                
                if (@$val1["inst_nivel"] == 2) {                    
                    $sede_filiales = self::reportpdf_getFormatFiliales($val1, self::reportpdf_getSearchCarrerasPersonas($carreras, $personas, $val1["inst_id"]));
                    $sede_filiales["filial_locales"] = [];                   
                    
                    foreach ($resultschild3 as $k => $val2) {
                        if (@$val2["inst_nivel"] == 3 && @$val2["parent_id"] == $val1["inst_id"]) {
                            $sede_filiales["filial_locales"][] = self::reportpdf_getFormatListaLocales($val2, self::reportpdf_getSearchCarrerasPersonas($carreras, $personas, $val2["inst_id"]));
                        }
                    }                    
                    $data['sede_filiales'][] = $sede_filiales;
                }
            }

            /* Sumatoria de los Locales/Carreras a nivel Institucional */
            $locales_sede = $carreras_sede = 0;
            foreach ($data['sede_locales']['lista_locales'] as $local) {
                $locales_sede++;
                $carreras_sede += count($local['local_carreras']);
            }
            /*
            if(@$data["sede_carreras"] && @$data["sede_carreras"]["lista_carreras"]){
                $carreras_sede += count($data["sede_carreras"]["lista_carreras"]);
            }
            */
            $data['sede_locales']['texto_locales'] = 'Total: ' . $carreras_sede .' carrera(s), ' . $locales_sede . ' locales';
            /* Acumuladores */

            $acum_carreras = $carreras_sede;
            $acum_locales  = $locales_sede;
            /* Sumatoria de los Locales/Carreras por Filial */

            foreach ($data['sede_filiales'] as $i => $filial) {
                $carreras_filial = count($filial['filial_carreras']);
                $locales_filial = count($filial['filial_locales']);

                foreach ($filial['filial_locales'] as $local) {
                    $carreras_filial += count($local['local_carreras']);
                }

                $acum_carreras += $carreras_filial; 
                $acum_locales += $locales_filial;
                $texto = 'Total: '.$carreras_filial.' carreras, '.$locales_filial.' locales';
                $data['sede_filiales'][$i]['filial_texto'] = $texto;
            }

            /* Sumatoria Total */
            $num_filiales = count($data['sede_filiales']);
            $num_comitesCarrera = count($data['sede_carreras']['lista_carreras']);
            $data['texto_institucion'] = 'Total: '.$acum_carreras.' Carrera(s), '.$num_filiales. ' Filial(es), '.$acum_locales.' Local(es), '.$num_comitesCarrera.' Cómite(s) de Calidad registrados';

        }
        return $data;
    }

}
