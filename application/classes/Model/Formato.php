<?php

defined('SYSPATH') OR die('No direct script access.');

require_once Kohana::find_file('vendor', 'carbon/vendor/autoload');

use Carbon\Carbon;

/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_Formato extends Model_Saes {
    
    // Estados
    const CONCLUIDO = '1';
	const PARCIAL   = '2';
    const PENDIENTE = '3';
        
	public static $estados = array(
		self::CONCLUIDO => 'Concluido',
		self::PARCIAL   => 'Parcial',
		self::PENDIENTE => 'Pendiente',
	);

	public function labels()
	{
		return array(
			'form_nombre' => 'Titulo',
			'form_columna' => 'Columna',
			'form_estado' => 'Estado',
		);
	}

	public function rules()
	{
		return array(
			'form_nombre' => array(
				array('not_empty'),
			),
        	'form_columna' => array(
				array('not_empty'),
			),
			'form_estado' => array(
				array('not_empty'),
			),
		);
	}

	public function filters()
	{
		return array(
			'form_columna' => array(
				array('Model_Saes::filter_empty'),
			),
		);
	}
        
        // Columnas
	const COLUMNA_A = 'A';
	const COLUMNA_C = 'C';
	
	public static $columnas = array(
		self::COLUMNA_A => 'A',
		self::COLUMNA_C => 'C',
	);

	protected $_table_name  = 'formato';
	protected $_primary_key = 'form_id';

	protected $_has_many = array(
		'aAutoFormato' => array(
			'model'       => 'AutoevaluacionFormato',
			'foreign_key' => 'form_id',
		),
	);

	public function title()
	{
        return $this->form_nombre;
	}

	public function titulo()
	{
         return $this->form_nombre;
	}


	/**
	 * Documentos Históricos de Formatos y Medio de verificación de la Autoevaluación.
	 * 
	 * @param  int     $autoevaluacion_id
	 * @param  Request $request
	 * @return array
	 */
	public static function getDocumentosHistoricos($autoevaluacion_id, $request)
	{
		$documentosFormatoA = QueryBuilder::factory('formatos/historico_docsformatoAC.sql', [
			':autoevaluacion_id' => $autoevaluacion_id,
			':formato'           => 'A'
		])->dict('docu_id')->get();

		$documentosFormatoB = QueryBuilder::factory('formatos/historico_docsformatoB.sql', [
			':autoevaluacion_id' => $autoevaluacion_id
		])->dict('docu_id')->get();

		$documentosFormatoC = QueryBuilder::factory('formatos/historico_docsformatoAC.sql', [
			':autoevaluacion_id' => $autoevaluacion_id,
			':formato'           => 'C'
		])->dict('docu_id')->get();

		$documentosEvaluacion = QueryBuilder::factory('formatos/historico_documentosregistrados.sql', [
			':autoevaluacion_id' => $autoevaluacion_id
		])->get();

		$documentosTotal = array_merge(
			$documentosFormatoA, $documentosFormatoB, $documentosFormatoC, $documentosEvaluacion
		);

		$aDocumentosUnique = A::f($documentosTotal)
                ->map(function ($documento) {

					$documento['indicadores_formatos'] = '';
					return $documento;
                })
                ->to_dict('docu_id')
                ->value();

        $aDocumentos = A::f($aDocumentosUnique)
                ->map(function ($documento) use ($documentosTotal) {
              
                	foreach ($documentosTotal as $item) {
                		
                		if ($item['docu_id'] == $documento['docu_id']) {
                			
                			$documento['indicadores_formatos'] .= "{$item['docu_locacion']}, ";

                			if ($item['formato_nombre']) { $documento['formato_nombre'] = $item['formato_nombre'];	}
                		}
                	}

                	$calidadDocumento = in_array($documento['sub_estado'], array_keys(Model_Documento::$sub_estados)) 
											? Model_Documento::$sub_estados[$documento['sub_estado']]['display']
											: '-';

					$documentoEstado = in_array($documento['estados'], array_keys(Model_Documento::$estados)) 
											? Model_Documento::$estados[$documento['estados']]
											: '-';

					$documento['rd_ind_form']        = rtrim($documento['indicadores_formatos'], ', ');
					$documento['rd_periodo_desde']   = Carbon::parse($documento['fecha_inicio'])->format('d/m/Y');
					$documento['rd_periodo_hasta']   = Carbon::parse($documento['fecha_fin'])->format('d/m/Y');
					$documento['rd_periodo_dias']    = Carbon::parse($documento['fecha_fin'])->diffInDays(Carbon::parse($documento['fecha_inicio']));
					$documento['rd_plazo_estimado']  = $documento['da'] ? $documento['da'] : '-';
					$documento['rd_estado']          = $documentoEstado;
					$documento['rd_calidad']         = $calidadDocumento;
					$documento['rd_edad']            = Carbon::now()->diffInDays(Carbon::parse($documento['fecha_inicio']));
					$documento['dh_fecha_reemplazo'] = Carbon::parse($documento['docu_fecha_act'])->format('d/m/Y');
					$documento['dh_diferencia_dias'] = (int) $documento['rd_periodo_dias'] - $documento['rd_edad'];

                	return $documento;
                })
                ->to_dict('docu_id')
				->value();

		return $aDocumentos;
	}

	/**
	 * Customiza los documentos registrados.
	 * 
	 * @param  int     $autoevaluacion_id
	 * @param  Request $request
	 * @return array
	 */
	public static function getDocumentosFormatos($autoevaluacion_id, $request)
	{

		$documentosFormatoA = QueryBuilder::factory('formatos/docsformatoAC.sql', [
			':autoevaluacion_id' => $autoevaluacion_id,
			':formato'           => 'A'
		])->dict('docu_id')->get();

		$documentosFormatoB = QueryBuilder::factory('formatos/docsformatoB.sql', [
			':autoevaluacion_id' => $autoevaluacion_id
		])->dict('docu_id')->get();

		$documentosFormatoC = QueryBuilder::factory('formatos/docsformatoAC.sql', [
			':autoevaluacion_id' => $autoevaluacion_id,
			':formato'           => 'C'
		])->dict('docu_id')->get();

		/** Documentos en la Autoevaluación **

			$documentosEvaluacion = QueryBuilder::factory('formatos/documentosregistrados.sql', [
				':autoevaluacion_id' => $autoevaluacion_id
			])->get();
			
		*/

		/* Verifica sí se filtra los documentos por formatos o autoevaluación. */

		$docs = $request->query('docs') ? strtolower($request->query('docs')) : '';

		if ($docs) {

			switch ($docs) {

				case 'a': $documentosTotal = $documentosFormatoA; break;

				case 'b': $documentosTotal = $documentosFormatoB; break;

				case 'c': $documentosTotal = $documentosFormatoC; break;
				
				default:
					
					$documentosTotal = array_merge($documentosFormatoA, $documentosFormatoB, $documentosFormatoC);

					break;
			}
		}
		else{

			$documentosTotal = array_merge($documentosFormatoA, $documentosFormatoB, $documentosFormatoC);
		}

		$aDocumentosUnique = A::f($documentosTotal)
                ->map(function ($documento) {

					$documento['indicadores_formatos'] = '';
					return $documento;
                })
                ->to_dict('docu_id')
                ->value();

       	/* Filtrado por Estándar. */

		$estandar_id = $request->query('esta_codigo') ? $request->query('esta_codigo') : '';

		if ($estandar_id) {

			$aDocumentosUnique = A::f($documentosTotal)
				->map(function ($documento) {

					$documento['indicadores_formatos'] = '';
					return $documento;
                })
	            ->filter(function($documento) use ($estandar_id) {

	                return $estandar_id == $documento['estandar_codigo'];
	            })
	            ->value();
		}

		/* Iteración y personalización del Array. */

		$aDocumentos = A::f($aDocumentosUnique)
                ->map(function ($documento) use ($documentosTotal) {
              
                	foreach ($documentosTotal as $item) {
                		
                		if ($item['docu_id'] == $documento['docu_id']) {
                			
                			$documento['indicadores_formatos'] .= "{$item['docu_locacion']}, ";
                		}
                	}

					$documento['rd_ind_form']       = rtrim($documento['indicadores_formatos'], ', ');
					$documento['rd_periodo_desde']  = Carbon::parse($documento['fecha_inicio'])->format('d/m/Y');
					$documento['rd_periodo_hasta']  = Carbon::parse($documento['fecha_fin'])->format('d/m/Y');
					$documento['rd_periodo_dias']   = Carbon::parse($documento['fecha_fin'])->diffInDays(Carbon::parse($documento['fecha_inicio']));
					$documento['rd_plazo_estimado'] = $documento['da'] ? $documento['da'] : '-';
					$documento['rd_estado']        = in_array($documento['estados'], array_keys(Model_Documento::$estados)) 
															? Model_Documento::$estados[$documento['estados']]
															: '-';
					$documento['rd_calidad']        = in_array($documento['sub_estado'], array_keys(Model_Documento::$sub_estados)) 
															? Model_Documento::$sub_estados[$documento['sub_estado']]['display']
															: '-';
					$documento['rd_edad'] = Carbon::now()->diffInDays(Carbon::parse($documento['fecha_inicio']));
					
					$documento['dr_olgura']      = (int) $documento['rd_periodo_dias'] - (int) $documento['rd_edad'];
					$documento['dr_sugerencia']  = Model_Formato::getSugerenciaDocumento($documento['dr_olgura'], $documento['da']);

					$documento['dh_fecha_reemplazo'] = Carbon::parse($documento['docu_fecha_act'])->format('d/m/Y');


					$documento['dh_diferencia_dias'] = (int) $documento['rd_periodo_dias'] - $documento['rd_edad'];

                	return $documento;
                })
                ->to_dict('docu_id')
				->value();

		$aDocumentosFltr = Model_Formato::filterDocumentos($aDocumentos, $request);

		$aDocumentos = A::f($aDocumentosFltr)
                ->map(function ($documento) { return $documento; })
                ->to_dict('docu_id')
                ->value();

		return $aDocumentos;
	}

	/**
	 * Customiza los documentos registrados.
	 * 
	 * @param  int     $autoevaluacion_id
	 * @param  Request $request
	 * @return array
	 */
	public static function getDocumentosMedioVerificacion($autoevaluacion_id, $request)
	{
		$documentosTotal = QueryBuilder::factory('formatos/documentosregistrados.sql', [
			':autoevaluacion_id' => $autoevaluacion_id
		])->get();

		$aDocumentosUnique = A::f($documentosTotal)
                ->map(function ($documento) {
					$documento['indicadores_formatos'] = '';
					return $documento;
                })
                ->to_dict('docu_id')
                ->value();

       	/* Filtrado por Estándar */

		$estandar_id = $request->query('esta_codigo') ? $request->query('esta_codigo') : '';

		if ($estandar_id) {

			$aDocumentosUnique = A::f($documentosTotal)
				->map(function ($documento) {

					$documento['indicadores_formatos'] = '';
					return $documento;
                })
	            ->filter(function($documento) use ($estandar_id) {

	                return $estandar_id == $documento['estandar_codigo'];
	            })
	            ->value();
		}

		/* Iteración y personalización del Array. */

		$aDocumentos = A::f($aDocumentosUnique)
                ->map(function ($documento) use ($documentosTotal) {
              
                	foreach ($documentosTotal as $item) {
                		
                		if ($item['docu_id'] == $documento['docu_id']) {
                			
                			$documento['indicadores_formatos'] .= "{$item['docu_locacion']}, ";
                		}
                	}

                	$documentoCalidad = in_array($documento['sub_estado'], array_keys(Model_Documento::$sub_estados)) 
											? Model_Documento::$sub_estados[$documento['sub_estado']]['display']
											: '-';

					$documentoEstado = in_array($documento['estados'], array_keys(Model_Documento::$estados)) 
											? Model_Documento::$estados[$documento['estados']]
											: '-';

					$documento['rd_ind_form']       = rtrim($documento['indicadores_formatos'], ', ');
					$documento['rd_periodo_desde']  = Carbon::parse($documento['fecha_inicio'])->format('d/m/Y');
					$documento['rd_periodo_hasta']  = Carbon::parse($documento['fecha_fin'])->format('d/m/Y');
					$documento['rd_periodo_dias']   = Carbon::parse($documento['fecha_fin'])->diffInDays(Carbon::parse($documento['fecha_inicio']));
					$documento['rd_plazo_estimado'] = $documento['da'] ? $documento['da'] : '-';
					$documento['rd_estado']         = $documentoEstado;
					$documento['rd_calidad']        = $documentoCalidad;
					$documento['rd_edad']           = Carbon::now()->diffInDays(Carbon::parse($documento['fecha_inicio']));
					$documento['dr_olgura']         = (int) $documento['rd_periodo_dias'] - (int) $documento['rd_edad'];
					$documento['dr_sugerencia']     = Model_Formato::getSugerenciaDocumento($documento['dr_olgura'], $documento['da']);

                	return $documento;
                })
                ->to_dict('docu_id')
				->value();

		$aDocumentosFltr = Model_Formato::filterDocumentos($aDocumentos, $request);

		$aDocumentos = A::f($aDocumentosFltr)
                ->map(function ($documento) { return $documento; })
                ->to_dict('docu_id')
                ->value();

		return $aDocumentos;
	} 

	/**
	 * Filtrado de documentos.
	 * 
	 * @param  array   $aDocumentos
	 * @param  Request $request
	 * @return array
	 */
	public static function filterDocumentos($aDocumentos, $request){

		$aDocumentosFltr = $aDocumentos;

		$documento_query = $request->query('documento') ? $request->query('documento') : '';

		/* Filtro por búsqueda. */

		if ($documento_query) {

			$aDocumentosFltr = A::f($aDocumentosFltr)
	            ->filter(function($documento) use ($documento_query) {

	                return mb_strpos(strtolower($documento['docu_titulo']), strtolower($documento_query)) !== FALSE;
	            })
	            ->value();
		}

		/* Filtro por vigencia */

		$vig_desde = $request->query('vig_desde') ? $request->query('vig_desde') : '';

		$vig_hasta = $request->query('vig_hasta') ? $request->query('vig_hasta') : '';

		if ($vig_desde && $vig_hasta) {
			
			$aDocumentosFltr = A::f($aDocumentosFltr)
	            ->filter(function($documento) use ($vig_desde, $vig_hasta) {

	                return $vig_desde <= Carbon::parse($documento['fecha_inicio'])->format('Y-m-d') && Carbon::parse($documento['fecha_fin'])->format('Y-m-d') <= $vig_hasta;
	            })
	            ->value();
		}

		/* Filtro por olgura */

		$olg_desde = $request->query('olg_desde') ? $request->query('olg_desde') : 0;

		$olg_hasta = $request->query('olg_hasta') ? $request->query('olg_hasta') : '';

		if (isset($olg_desde) && $olg_hasta) {

			$aDocumentosFltr = A::f($aDocumentosFltr)
	            ->filter(function($documento) use ($olg_desde, $olg_hasta) {

	                return $olg_desde <= (int) $documento['dr_olgura'] && (int) $documento['dr_olgura'] <= $olg_hasta;
	            })
	            ->value();
		}

		return $aDocumentosFltr;
	}

	/**
	 * Define la sugerencia de un documento.
	 * 
	 * @param  int    $olgura
	 * @param  int    $diasActualizacion
	 * @return string
	 */
	public static function getSugerenciaDocumento($olgura, $diasActualizacion)
	{
		$sugerencia = '-';

		if (!$diasActualizacion) { return $sugerencia; }

		if ($olgura >= $diasActualizacion) {
			
			$sugerencia = "Dentro de los plazos";
		}
		else if($olgura < $diasActualizacion && $olgura > 0){

			$sugerencia = "Faltan {$olgura} dias para su desactualización";
		}
		else{

			$sugerencia = "Documento desactualizado hace ".abs($olgura)." dias";
		}

		return $sugerencia;
	}

}
