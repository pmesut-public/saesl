<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Danniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_Estado extends Model_Saes {

	public function labels()
	{
		return array(
			'esta_codigo'      => 'Código',
			'esta_titulo'      => 'Título',
			'esta_descripcion' => 'Descripción',
			'esta_evidencia'   => 'Evidencia',
			'esta_estado'      => 'Estado',
		);
	}
	
	public function rules()
	{
		return array(
			'esta_codigo' => array(
				array('not_empty'),
			),
			'esta_titulo' => array(
				array('not_empty'),
			),
                        'esta_estado' => array(
				array('not_empty'),
			),
		);
	}
	
	protected $_table_name = 'estados';

	protected $_primary_key = 'esta_id';
	
	public function title()
	{
		return $this->esta_codigo.'. '.$this->esta_titulo;
	}
	
	public function titulo()
	{
		return $this->esta_titulo;
	}
    
    /**
	 * Obtiene los estados
	 */
	public static function get_all()
	{
		return DB::select()
			->from('estados')
			->where('esta_estado', '=', Model_Estado::STATUS_ACTIVO)
			->execute()
			->as_array('esta_id', 'esta_titulo');
	}
	
	public static function get_by_id($esta_id)
	{
		return ORM::factory('Estado', ['esta_id' => $esta_id]);
	}
	
	
}
