<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AutoevaluacionDetalleActividad extends ORM {

	protected $_table_name = 'autoevaluacion_detalle_actividad';
}
