<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AutoevaluacionInforme extends Model_Saes {
	
	protected $_table_name = 'autoevaluacion_informe';
	
	protected $_primary_key = 'auto_id';
	
	protected $_created_column = NULL;
	
	protected $_upload = 'uploads/autoevaluaciones/';
	
	const PENDIENTE = 1;
	const UPLOADING = 2;
	const FINALIZADO = 3;
	
	public static $estados = [
		self::PENDIENTE => 'Pendiente',
		self::UPLOADING => 'Subiendo archivo',
		self::FINALIZADO => 'Finalizado',
	];
	
	public static $colores = [
		1 => 'bg-red',
		2 => 'bg-yellow',
		3 => 'bg-green',
	];
	
	public static function get_status($row, $row_seg)
	{
		$h1 = (100 - $row['per']) / 4;
		$h3 = 3 * $h1;
		$diff = $row_seg['per'] - $row['per'];
		
		return $diff < $h1 ? 1 : ($diff < $h3 ? 2 : 3);
	}
	
	protected $_belongs_to = array(
		'oAutoevaluacion' => array(
			'model' => 'Autoevaluacion',
			'foreign_key' => 'auto_id',
		),
	);
	
	protected $_has_many = array(
		'aAutoInformeFactor' => array(
			'model' => 'AutoevaluacionInformeFactores',
			'foreign_key' => 'auto_id',
		),
	);
	
	protected $editing = array(
		self::PENDIENTE,
		self::UPLOADING,
	);
	
	public function is_editing()
	{
		return in_array($this->auin_estado, $this->editing);
	}
	
	public function upload_file($file)
	{
		if ( ! $this->_check_file($file))
			return FALSE;
		
		$dir = "{$this->_upload}{$this->auto_id}/";
		$full_dir = APPPATH.$dir;
		//debug($full_dir);
		
		if ( ! is_dir($full_dir))
		{
			mkdir($full_dir);
		}
		
		$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
		$filename = "{$this->auto_id}-informe-".date('YmdHis').".{$ext}";
		
        if ( ! Upload::save($file, $filename, $full_dir))
        {
            return FALSE;
        }
		
		return $dir.$filename;
	}
	
}
