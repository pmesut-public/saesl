<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_AcreditadoCarrera extends Model_Saes {

	protected $_table_name = 'acreditado_carrera';

	protected $_primary_key = 'acca_id';

	protected $_belongs_to = array(
		'oCarrera' => array(
			'model' => 'Carrera',
			'foreign_key' => 'carr_id',
		),
		'oAcreditadoInstitucion' => array(
			'model' => 'AcreditadoInstitucion',
			'foreign_key' => 'acin_id',
		),
	);
}
