<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_IcRequisito extends Model_Saes {

	const ESTADO_ACTIVO		= 1;
	const ESTADO_INACTIVO   = 0;
	
	protected static $nocumplidos;
	protected static $total_cumplidos = array();
	protected static $total_nocumplidos = array();
	protected static $requisitos_vinculados;
	
	protected static $requisitos_todo = 0; //totalmente, los estándares cumplidos totalmente para cada requisito
	protected static $requisitos_ninguno; //ninguna medida, ningún estándar cumplido por requisito
	
	public static $estados = array(
		self::ESTADO_ACTIVO		=> 'Activo',
		self::ESTADO_INACTIVO   => 'Inactivo',
	);
	
	public function labels()
	{
		return array(
			'iccr_id'					=> 'Criterio ICACIT',
			'icre_codigo'				=> 'Código',
			'icre_titulo'				=> 'Título',
			'icre_descripcion'			=> 'Descripción',
			'icre_relacion_directa'		=> 'Relación Directa',
			'icre_relacion_indirecta'	=> 'Relación Indirecta',
			'icre_estado'				=> 'Estado',
		);
	}
	
	protected $_table_name = 'ic_requisito';

	protected $_primary_key = 'icre_id';

	protected $_belongs_to = array(
		'oIcCriterio' => array(
			'model' => 'IcCriterio',
			'foreign_key' => 'iccr_id',
		),
	);
	
	public function title()
	{
		return $this->icre_codigo.'. '.$this->titulo();
	}
	
	public function titulo()
	{
		return $this->icre_titulo ? $this->icre_titulo : $this->icre_descripcion;
	}
	
	/*static function get_cumplidos($result, $oRequisito, $cumple = NULL)
	{
		
		
		$arrRelDir = explode(",", $oRequisito['icre_relacion_directa']);
		$arrRelInd = explode(",", $oRequisito['icre_relacion_indirecta']);
		$arrRequisitos = array_merge( (array_values(array_unique($arrRelDir))), (array_values(array_unique($arrRelInd))));
		$cumplidos = $nocumplidos = [];
		
				
		foreach ($arrRequisitos as $key => $value) {
			foreach ($result as $key2 => $value2) {
				//debug($key2);
				if ($value == $key2 && $value2 == 1)
				{
					$cumplidos[] = $value;
					array_push(self::$total_cumplidos, $value);
				}
			}
		}
		debug2($cumplidos);
		debug2($arrRequisitos);
		$nocumplidos = array_diff($arrRequisitos, $cumplidos);
		
		//estándares que se cumplen por cada requisito, totalmente, parcialmente o ninguno.
		($arrRequisitos == $cumplidos) ? self::$requisitos_todo++ : '';
		($arrRequisitos == $nocumplidos) ? self::$requisitos_ninguno++ : '';
		
		array_push(self::$total_nocumplidos, implode("," , $nocumplidos));
		self::$nocumplidos = $nocumplidos;

		return ($cumplidos) ? implode("," , $cumplidos) : '';
	}*/
	
	/*static function get_no_cumplidos()
	{
		return (self::$nocumplidos) ? implode("," , self::$nocumplidos) : '';
	}*/
	
	/*static function resumen()
	{
		
		$resumen = [];
		$total_cumplidos = count(array_values(array_unique(self::$total_cumplidos)));
		$total_nocumplidos = count(array_values(array_unique(self::$total_nocumplidos)));
		//debug(self::$total_nocumplidos);
		$resumen['estandares_cumplidos'] = $total_cumplidos;
		$resumen['estandares_nocumplidos'] = $total_nocumplidos;
		$resumen['total_estandares'] = $total_cumplidos + $total_nocumplidos;
		$resumen['porcentaje_cumplidos'] = round($total_cumplidos / ($total_cumplidos + $total_nocumplidos) * 100, 2);
		
		$aRequisitos = ORM::factory('IcRequisito')
			->find_all()
			->as_array();
		
		$aReqNoVinculados = ORM::factory('IcRequisito')
			->where('icre_relacion_directa','=', null)
			->where('icre_relacion_indirecta','=', null)
			->find_all()
			;
		//debug(count($aReqNoVinculados));
		
		$resumen['total_requisitos'] = count($aRequisitos);
		
		debug( count($aRequisitos) - self::$requisitos_todo);
		
		$resumen['requisitos_vinculados'] = count($aRequisitos) - count($aReqNoVinculados);
		//debug(self::$requisitos_novinculados);
		return $resumen;
	}*/

//	static function get_no_cumplidos($result, $oRequisito){}
	
}

