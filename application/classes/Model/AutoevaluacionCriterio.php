<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_AutoevaluacionCriterio extends Model_Saes {
	
	protected $_table_name = 'autoevaluacion_criterio';

	protected $_primary_key = 'aucr_id';

	protected $_belongs_to = array(
		'oCriterio' => array(
			'model' => 'Criterio',
			'foreign_key' => 'crit_id',
		),
		'oDetalle' => array(
			'model' => 'AutoevaluacionDetalle',
			'foreign_key' => 'aude_id',
		),
	);
	
	
}
