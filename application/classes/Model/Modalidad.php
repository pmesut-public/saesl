<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Modalidad extends Model_Saes {
	
	public function labels()
	{
		return array(
			'moda_nombre'       => 'Nombre',
			'moda_descripcion'  => 'Descripción',
			'moda_estado'       => 'Estado',
		);
	}
	
	protected $_table_name = 'modalidad';

	protected $_primary_key = 'moda_id';
	
	/**
	 * Obtiene las modalidades
	 */
	public static function get_all()
	{
		return DB::select()
			->from('modalidad')
			->where('moda_estado', '=', Model_Modalidad::STATUS_ACTIVO)
			->execute()
			->as_array('moda_id', 'moda_nombre');
	}
	
	/**
	 * Retorna el campo moda_nombre
	 */
	public function __toString()
	{
		return $this->moda_nombre;
	}
}
