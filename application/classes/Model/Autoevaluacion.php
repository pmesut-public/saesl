<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Autoevaluacion extends Model_Saes
{

    protected $_table_name = 'autoevaluacion';

    protected $_primary_key = 'auto_id';

    protected $_has_many = array(
        'aDetalle' => array(
            'model'       => 'AutoevaluacionDetalle',
            'foreign_key' => 'auto_id',
        ),
        'aAutoFormato' => array(
            'model'       => 'AutoevaluacionFormato',
            'foreign_key' => 'auto_id',
        ),
        'aAutoCondicion' => array(
            'model'       => 'AutoevaluacionCondicion',
            'foreign_key' => 'auto_id',
        ),
    );

    protected $_belongs_to = array(
        'oAcreditadoEvento' => array(
            'model'       => 'AcreditadoEvento',
            'foreign_key' => 'acev_id',
        ),
    );

    const AUTOEVALUACION_COMPLETA   = 1;
    const AUTOEVALUACION_PENDIENTE  = 0;

    const STATUS_ACTIVO    = 'activo';
    const STATUS_CERRADO   = 'cerrado';
    const STATUS_CANCELADO = 'cancelado';

    const AUTOEVALUACION_VALIDA   = 1;
    const AUTOEVALUACION_INVALIDA = 0;

    public static $statuses = array(
        self::STATUS_ACTIVO    => 'Activo',
        self::STATUS_CERRADO   => 'Cerrado',
        self::STATUS_CANCELADO => 'Cancelado',
    );

    protected $_result;

    protected $_low_attender;
    
    public static function new_auto_numero($acev_id)
    {
        $sub_query = DB::select('acre_id')
            ->from('acreditado_evento')
            ->where('acev_id', '=', $acev_id);

        return DB::select(array(DB::expr("COALESCE(MAX(auto_numero), 0) + 1"), 'auto_numero'))
            ->from('autoevaluacion')
            ->join('acreditado_evento')->using('acev_id')
            ->where('acre_id', '=', $sub_query)
            ->and_where('auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
            ->execute()
            ->get('auto_numero');
    }
    
    public static function create_new($oAcreditadoEvento)
    {
        $oAutoevaluacion = ORM::factory('Autoevaluacion');
        
        $oAutoevaluacion->auto_numero = self::new_auto_numero($oAcreditadoEvento->acev_id);
        $oAutoevaluacion->auto_fecha_inicio = date('Y-m-d');
        $oAutoevaluacion->auto_estado = Model_Saes::STATUS_ACTIVO;
        $oAutoevaluacion->acev_id = $oAcreditadoEvento->acev_id;
        $oAutoevaluacion->save();
        // llenamos autoevaluacion_detalle
        if ($oAutoevaluacion->create_detalle()) {
            // llenamos autoevaluación_medio
            $oAutoevaluacion->set_medios();

            // llenamos autoevaluación_condicion
            $oAutoevaluacion->set_condiciones();

            // llenamos autoevaluación_formato
            $oAutoevaluacion->set_formatos();
        }
    }

    /**
     * Crea el detalle  (tabla: autoevaluacion_detalle)
     * @return type
     */
    public function create_detalle()
    {
        $oEstandar = $this->oAcreditadoEvento->oAcreditado->get_estandares();

        if (count($oEstandar) > 0) {

            $query = DB::insert('autoevaluacion_detalle', array('aude_fecha_reg', 'aude_estado', 'esta_id', 'auto_id'));

            foreach ($oEstandar as $st) {
                $query->values(array(
                    date('Y-m-d H:i:s'),
                    Model_AutoevaluacionDetalle::STATUS_PENDIENTE,
                    $st->esta_id,
                    $this->auto_id
                ));
            }

            $query->execute();

            return true;
        } else {

            return false;
        }
    }

    /**
     * Llena tabla autoevaluación_medio
     */
    public function set_medios()
    {
        $aMedios = ORM::factory('Medio')
            ->select(DB::expr('ad.aude_id'))
            ->join(DB::expr('autoevaluacion_detalle ad'))->using('esta_id')
            ->where('medi_estado', '=', Model_Medio::STATUS_ACTIVO)
            ->where('ad.auto_id', '=', $this->auto_id)
            ->find_all();
        
        if($aMedios->count() > 0){
            $query = DB::insert(
                'autoevaluacion_medio', array(
                    'aude_id',
                    'medi_id',
                    'esta_id',
                    'aume_fecha_reg',
                    'aume_estado',
                    'aume_descripcion',
                    'aume_observacion',
                    'aume_consideracion'
                )
            );

            foreach ($aMedios as $cr) {
                $query->values(array(
                    $cr->aude_id,
                    $cr->medi_id,
                    $cr->esta_id,
                    date('Y-m-d H:i:s'),
                    $cr->medi_estado,
                    "", //$cr->medi_descripcion,
                    "", //$cr->medi_observacion,
                    "", //$cr->medi_consideracion,
                ));
            }
            
            $query->execute();
        }
    }

    /**
     * Llena tabla autoevaluación_condicion
     */
    public function set_condiciones()
    {
        $aCondicion = ORM::factory('Dimension')
            ->join('factor')->using('dime_id')
            ->join('estandar')->using('fact_id')
            ->join('autoevaluacion_detalle')->using('esta_id')
            ->where('auto_id', '=', $this->auto_id)
            ->find_all();

        $query = DB::insert(
            'autoevaluacion_condicion', array(
                'auto_id',
                'dime_id',
                'auco_fecha_reg',
                'auco_estado'
            )
        );

        foreach ($aCondicion as $co) {
            $query->values(array(
                $this->auto_id,
                $co->dime_id,
                date('Y-m-d H:i:s'),
                Model_Formato::PENDIENTE
            ));
        }

        $query->execute();
    }

    /**
     * Llena tabla autoevaluación_formato
     */
    public function set_formatos()
    {
        $aFormatos = ORM::factory('Formato')
            //->where('form_estado', '=', Model_Saes::STATUS_ACTIVO)
            ->find_all();

        $query = DB::insert(
            'autoevaluacion_formato', array(
                'form_id',
                'auto_id',
                'aufo_fecha_reg',
                'aufo_estado'
            )
        );

        foreach ($aFormatos as $fo) {
            $query->values(array(
                $fo->form_id,
                $this->auto_id,
                date('Y-m-d H:i:s'),
                Model_Formato::PENDIENTE
            ));
        }

        $query->execute();
    }

    public function autoevaluacion_anterior($acev_id)
    {
        return ORM::factory('Autoevaluacion')
            ->where('acev_id', '=', $acev_id)
            ->where('auto_estado', '=', self::STATUS_CERRADO)
            ->order_by('auto_id', 'DESC')
            ->find();

        //echo $auto->auto_id; die;
    }

    public function autoevaluacion_cerrada()
    {
        $oAuto = ORM::factory('Autoevaluacion')
            ->where('auto_id', '=', $this->auto_id)
            ->find();
        $return = FALSE;
        if ($oAuto->auto_estado == self::STATUS_CERRADO) {
            $return = TRUE;
        }

        return $return;
    }

    /**
     * Obtiene el codigo aude_id
     * de un esta_id
     */
    public function get_aude_id($estaId)
    {
        return ORM::factory('AutoevaluacionDetalle')
            ->where('auto_id', '=', $this->auto_id)
            ->where('esta_id', '=', $estaId)
            ->find();
    }

    /**
     * Obtiene el codigo medi_id
     * de un esta_id
     */
    public function get_medi_id($estaId)
    {
        return ORM::factory('Medio')
            ->where('esta_id', '=', $estaId)
            ->find()
            ->medi_id;
    }

    /**
     * Obtiene todos los medios
     * de la autoevaluación actual
     */
    public function get_auto_medio()
    {
        return ORM::factory('AutoevaluacionMedio')
            ->join(DB::expr('autoevaluacion_detalle ad'))->using('aude_id')
            ->where('ad.auto_id', '=', $this->auto_id)
            ->find_all();
    }

    /**
     * Obtiene todos los formatos columna A
     * de la autoevaluación actual
     */
    public function get_auto_formatosA()
    {
        return ORM::factory('AutoevaluacionFormato')
            ->select(DB::expr('f.form_nombre, d.docu_titulo, d.docu_path, d.docu_estado'))
            ->join(DB::expr('autoevaluacion a'))->using('auto_id')
            ->join(DB::expr('formato f'))->using('form_id')
            ->join(DB::expr('documento d'), 'left')->using('docu_id')
            ->where('a.auto_id', '=', $this->auto_id)
            ->where('f.form_columna', '=', Model_Formato::COLUMNA_A)
            ->and_where('f.form_estado', '=', Model_Saes::STATUS_ACTIVO)
            //->and_where('d.docu_estado', 'IN', DB::expr('("activo", "'.NULL.'")'))
            ->find_all();
    }

    /**
     * Obtiene todos los formatos columna C
     * de la autoevaluación actual
     */
    public function get_auto_formatosC()
    {
        return ORM::factory('AutoevaluacionFormato')
            ->select(DB::expr('f.form_nombre, d.docu_titulo, d.docu_path, d.docu_estado'))
            ->join(DB::expr('autoevaluacion a'))->using('auto_id')
            ->join(DB::expr('formato f'))->using('form_id')
            ->join(DB::expr('documento d'), 'left')->using('docu_id')
            ->where('a.auto_id', '=', $this->auto_id)
            ->where('f.form_columna', '=', Model_Formato::COLUMNA_C)
            ->and_where('f.form_estado', '=', Model_Saes::STATUS_ACTIVO)
            ->find_all();
    }

    /**
     * Obtiene todas las condicones
     * de la autoevaluación actual
     */
    public function get_auto_condicion()
    {
        return ORM::factory('AutoevaluacionCondicion')
            ->select(DB::expr('c.dime_titulo,c.dime_informacion, c.dime_codigo, c.dime_descripcion, d.docu_titulo, d.docu_path, d.docu_estado'))
            ->join(DB::expr('autoevaluacion a'))->using('auto_id')
            ->join(DB::expr('dimension c'))->using('dime_id')
            ->join(DB::expr('documento d'), 'left')->using('docu_id')
            ->where('a.auto_id', '=', $this->auto_id)   
            ->group_by('dime_id')
            ->order_by('auco_id','ASC')
            ->find_all();   
    }

    /**
     * Obtiene todos los documentos
     * de la autoevaluación actual
     */
    public function get_auto_documento()
    {
        return ORM::factory('AutoevaluacionDocumento')
            ->select(DB::expr('d.docu_titulo titulo, d.docu_path path'))
            ->join(DB::expr('documento d'))->using('docu_id')
            ->join(DB::expr('autoevaluacion_medio ae'))->using('aume_id')
            ->join(DB::expr('autoevaluacion_detalle ad'))->using('aude_id')
            ->where('ad.auto_id', '=', $this->auto_id)
            ->where('d.docu_estado', '=', Model_Documento::ACTIVO)
            ->find_all();
    }

    public function documento_new($post)
    {
        foreach ($post as $ad) {
            //$estado = ($ad['audo_estado'] != '') ? $ad['audo_estado'] : NULL;

            //debug($estado); die;

            $oAutoDocu = ORM::factory('AutoevaluacionDocumento')
                ->set('aume_id', $ad['aume_id'])
                ->set('docu_id', $ad['docu_id'])
                //->set('audo_estado', Model_AutoevaluacionDocumento::$statuses[$estado])
                ->save();

            $oDocumento = Model_Documento::get_documento($ad['docu_id']);

            //debug($oDocumento); die;
            $return[] = [
                'aume_id' => $ad['aume_id'],
                'docu_id' => $ad['docu_id'],
                'audo_id' => $oAutoDocu->audo_id,
                'audo_estado' => $oDocumento->estados,
                'audo_subestado' => $oDocumento->sub_estado,
                'fecha_inicio' => $oDocumento->fecha_inicio,
                'fecha_fin' => $oDocumento->fecha_fin,
                'path' => $oDocumento->docu_path,
                'display' => $oDocumento->docu_titulo,
                'da' => $oDocumento->da
            ];
        }

        return $return;
    }

    public function documento_formato_new($docuId, $post)
    {
        $id = NULL;
        //debug($post);
        if ($post['tipo'] === 'FOR') {
            $oAutoF = ORM::factory('AutoevaluacionFormato')
                ->where('form_id', '=', $post['id'])
                ->where('auto_id', '=', $this->auto_id)
                ->find();

            //get docu_id previous
            $prev_docu = $oAutoF->docu_id;

            $oAutoF->set('docu_id', $docuId)
                ->save();

            $id = $oAutoF->aufo_id;
        }

        if ($post['tipo'] === 'COND') {
            $oAutoC = ORM::factory('AutoevaluacionCondicion')
                ->where('dime_id', '=', $post['id'])
                ->where('auto_id', '=', $this->auto_id)
                ->find();

                //get docu_id previous
            $prev_docu = $oAutoC->docu_id;

            $oAutoC->docu_id = $docuId;
            $oAutoC->save();

            $id = $oAutoC->auco_id;
        }

        if(intval($post['reemplazar']))
        {

            $oDocumentoPrevio = Model_Documento::get_documento($prev_docu);

            $oDocumentoPrevio->docu_estado      = Model_Documento::REEMPLAZADO;
            $oDocumentoPrevio->docu_fecha_act   = date('Y-m-d H:i:s');
            $oDocumentoPrevio->parent_id        = $docuId;
            $oDocumentoPrevio->parent_senior_id = $docuId;
            $oDocumentoPrevio->save();

            /* Actualización de los documentos anteriores */

            $oDocumentosAnteriores = ORM::factory('Documento')
                                        ->where('parent_senior_id', '=', $oDocumentoPrevio->docu_id)
                                        ->find_all()->as_array('docu_id');

            if (count($oDocumentosAnteriores)) {

                foreach ($oDocumentosAnteriores as $documento) {
                    
                    $documento->parent_senior_id = $docuId;

                    $documento->save();
                }
            }

        }

        $oDocumento = Model_Documento::get_documento($docuId);

        return ['docu' => $oDocumento, 'id' => $id];
    }

    public static function get_cumplimiento()
    {
        // Strings
        /*
         * Verifica si el estandar pertenece al Minedu (M) o  Sunedu (S)
         */
        $_basicos = "((tiin_id = 3 and esta_clasificacion = 'M') or (tiin_id = 1 and esta_clasificacion = 'S'))";

        /*
         * 	Verifica el cumplimiento de un estandar (1 o 0)
         */
        $_cumplimiento = "(aude_valoracion = 1)";

        /*
         * Verifica la calidad de un estandar, revisar script formula (gproc_planificacion_3_1.sql)
         */
        $_calidad = "((3 - aude_calidad) / 2)";

        /*
         * Verifica si un estandar es opcional (1) o  no (0)
         */
        $_not_opcional = "(esta_opcional = 0)";

        // Totales
        $total = "count(esta_id)";
        $cumplidos = "sum(if($_cumplimiento, 1, 0))";
        $no_cumplidos = "($total - $cumplidos)";
        $raw_cumplidos = "($cumplidos / $total * 100)";
        $raw_no_cumplidos = "($no_cumplidos / $total * 100)";
        $per_cumplidos = "format($raw_cumplidos, 2)";
        $per_no_cumplidos = "format($raw_no_cumplidos, 2)";
        $procesados = "sum(if(aude_estado = 'proceso', 1, 0))";
        $no_procesados = "($total - $procesados)";
        $per_procesados = "format($procesados / $total * 100, 2)";
        $per_diferencia = "($per_procesados - $per_cumplidos)";
        $resultado = "concat($procesados, ' de ', $total, ' procesados, ', $cumplidos, ' (', $per_cumplidos, '%) cumplen')";

        // GPROC
        $gproc_total = "sum(if($_not_opcional, 1, 0))";
        $gproc_cumplidos = "sum(if($_cumplimiento and $_not_opcional, 1, 0))";
        $gproc_raw_porcentaje = "coalesce(($gproc_cumplidos / $gproc_total * 100), 0)";
        $gproc_porcentaje = "format($gproc_raw_porcentaje, 2)";
        $auto_porcentaje = "if(a.moda_id = 1 and tiin_id = 1, $gproc_porcentaje, $per_cumplidos)";

        // Puntajes
        $puntaje_cumplidos = "coalesce(sum(if($_cumplimiento, aude_aceptacion, 0)), 0)";
        $puntaje_total = "coalesce(sum(aude_aceptacion), 0)";
        $puntaje_meta = "sum(esta_nivel_aceptacion)";

        // Calidad
        $calidad = "sum(aude_aceptacion * $_calidad)";
        $aprobados = "if(tiin_id = 1,
									sum(if($_cumplimiento and $_calidad > 0.5, 1, 0)),
									sum(if(aude_aceptacion * $_calidad >= esta_nivel_aceptacion, 1, 0)))";
        $per_aprobados = "format($aprobados / $total * 100, 2)";

        // Basicos
        $basicos_total = "sum(if($_basicos, 1, 0))";
        $basicos_cumplidos = "sum(if($_basicos and $_cumplimiento, 1, 0))";
        $basicos_no_cumplidos = "($basicos_total - $basicos_cumplidos)";
        $per_basicos_cumplidos = "format($basicos_cumplidos / $basicos_total * 100, 2)";
        $basicos_resultado = "concat($basicos_cumplidos, ' de ', $basicos_total, ' (', $per_basicos_cumplidos, '%) cumplen')";

        unset($_basicos, $_calidad, $_cumplimiento, $_not_opcional);

        $vars = get_defined_vars();
        //debug($vars);

        $columns = array_map(function ($key, $val) {
            return $val . ' ' . $key;
        }, array_keys($vars), $vars);
        //debug($columns);

        $expr = implode(', ' . PHP_EOL, $columns);
        //debug($expr);
        //echo '<pre>'.$expr.'</pre>';die();

        return DB::expr($expr);
    }

    public function result($key = NULL, $cached = TRUE)
    {
        if ($this->_result AND $cached) {
            return $key ? $this->_result[$key] : $this->_result;
        }

        $this->_result = $result = $this->aDetalle
            ->join('autoevaluacion')->using('auto_id')
            ->join('estandar')->using('esta_id')
            ->join('acreditado_evento')->using('acev_id')
            ->join(['acreditado', 'a'])->using('acre_id')
            ->join('objeto_acreditacion')->using('obac_id')
            ->select(DB::expr('auto_estado estado'))
            //->select(Model_Autoevaluacion::get_cumplimiento())
            ->find()
            ->as_array();
        //debug($result);

        return $key ? $result[$key] : $result;
    }

    public function get_pending_detalles()
    {
        return $this->aDetalle
            ->where('aude_estado', '=', Model_AutoevaluacionDetalle::STATUS_PENDIENTE)
            ->find_all();
    }

    public function get_first_pending()
    {
        $pending = $this->get_pending_detalles();

        if ($pending->count()) {
            return $pending->current()->esta_id;
        }

        return FALSE;
    }

    public function evento_actual()
    {
        return ($this->oAcreditadoEvento->acev_estado == Model_AcreditadoEvento::ESTADO_ACTUAL);
    }

    public function is_active()
    {
        return ($this->auto_estado == self::STATUS_ACTIVO);
    }

    public function discard()
    {
        $this->auto_estado = Model_Autoevaluacion::STATUS_CANCELADO;
        $this->auto_fecha_fin = date('Y-m-d');
        $this->save();
        // cancelamos acreditado evento
        $oEventoActual = $this->oAcreditadoEvento->oAcreditado->oEventoActual();
        if (!$oEventoActual->loaded()) {
            throw new Exception_Saes('Model not found');
        }

        $aAssessment = $oEventoActual->aAutoevaluacion
            ->where('auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
            ->find_all();

        foreach ($aAssessment as $oAssessment) {
            $oAssessment->discard();
        }

        $oEventoActual->discard();
    }

    public function finish()
    {
        // cerramos acreditado_evento
        $this->oAcreditadoEvento
            ->where('acev_id', '=', $this->acev_id)
            ->set('acev_estado', Model_AcreditadoEvento::ESTADO_ANTERIOR)
            ->set('acev_fecha_fin', date('Y-m-d'))
            ->save();

        // Cerramos autoevaluacion
        $this->auto_estado = Model_Autoevaluacion::STATUS_CERRADO;
        $this->auto_completa = '1';
        $this->auto_fecha_fin = date('Y-m-d');
        $this->save();

        // cerramos autoevaluacion_detalle
        $cerrado = Model_AutoevaluacionDetalle::STATUS_CERRADO;
        $byUser = Model_AutoevaluacionDetalle::BY_USER;
        DB::update('autoevaluacion_detalle')
            ->where('auto_id', '=', $this->auto_id)
            ->value('aude_estado', $cerrado)
            ->value('aude_closed_by', $byUser)
            ->execute();

        
    }

    public function get_reportes()
    {
        /*$objeto = $this->oAcreditadoEvento->oEvento->even_objeto;
        if ($objeto == 1)
        {
            $reportes['reportM1'] = array(
                'short' => 'Reporte Mejora',
                'title' => 'Resultado de Autoevaluación',
            );
        }*/
        //else
        /*{
            $reportes['reportA1'] = array(
                'short' => 'Autoevaluación -SP',
                'title' => 'Resultado de Autoevaluación por Equivalencia',
            );
            $reportes['reportB1'] = array(
                'short' => 'Autoevaluación %SP',
                'title' => 'Resultado de Autoevaluación por % de Cumplimiento',
            );
            $reportes['reportC1'] = array(
                'short' => 'Nuevo reporte',
                'title' => 'Resultado de Autoevaluación',
            );

            $evento = $this->oAcreditadoEvento->oEvento->even_ponderacion;
            if ($evento != 0) {
                $reportes['reportA2'] = array(
                    'short' => 'Autoevaluación  CP',
                    'title' => 'Resultado de Autoevaluación por Equivalencia con Ponderación',
                );
                $reportes['reportB2'] = array(
                    'short' => 'Autoevaluación %CP',
                    'title' => 'Resultado de Autoevaluación por % de Cumplimiento con Ponderación',
                );
            }

            $reportes['reportAC'] = array(
                'short' => 'Autoevaluación  AC',
                'title' => 'Reporte de Autoevaluación por Actividades',
            );
            $reportes['reportAC1'] = array(
                'short' => 'Autoevaluación  DFEA',
                'title' => 'Reporte de Autoevaluación por Dimensiones, Factores, Estándares y Actividades Relacionadas',
            );
            $reportes['reportAC2'] = array(
                'short' => 'Autoevaluación  DFEA(%)',
                'title' => 'Reporte de Autoevaluación por Dimensiones, Factores, Estándares y Actividades Relacionadas(%)',
            );
            $reportes['reportAC3'] = array(
                'short' => 'Autoevaluación  FAE',
                'title' => 'Reporte de Autoevaluación por Factores, Actividades y Estándares Relacionados',
            );
            $reportes['reportAC4'] = array(
                'short' => 'Resultado A',
                'title' => 'Resultado del proceso de Autoevaluación',
            );
            $reportes['reportAC5'] = array(
                'short' => 'Resultado AAV',
                'title' => 'Resultado del proceso de Autoevaluación con Actividades Valorizadas',
            );
            $reportes['reportAC6'] = array(
                'short' => 'Resultado AAVD',
                'title' => 'Resultado del proceso de Autoevaluación con Actividades Valorizadas Detallado',
            );
        }*/

        $reportes['reporte'] = array(
            'short' => 'Autoevaluación - ECBC',
            'title' => 'Reporte del Estado de las Condiciones Básicas de calidad',
        );

        return $reportes;
    }

    /**
     * Obtiene todos los estandares por un documento x
     * de la autoevaluación actual
     */
    public function get_estandar_autodetalle($docu_id, $acre_id, $auto_id)
    {
        if ($auto_id === NULL)
            return '';

        $query = ORM::factory('AutoevaluacionDetalle')
            ->join(array('autoevaluacion', 'b'))->using('auto_id')
            ->join(array('acreditado_evento', 'ae'))->using('acev_id')
            ->join(array('autoevaluacion_medio', 'ame'))->using('aude_id')
            ->join(array('autoevaluacion_documento', 'ad'))->using('aume_id')
            ->where('ae.acre_id', '=', $acre_id)
            ->where('b.auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
            ->where('ad.docu_id', '=', $docu_id)
            ->group_by('esta_id');

        if ($auto_id) {
            $query->where('auto_id', '=', $auto_id);
        } else {
        }

        $oEstaIdes = $query->find_all();

        $rs = '';
        foreach ($oEstaIdes as $st) {
            $rs .= $st->esta_id . ' ';
        }

        return $rs ?: FALSE;
    }

    /**
     * Obtiene todos los form_id asociados a un documento
     * de la autoevaluación actual o historico
     */
    public function get_formato_documento($docu_id, $acre_id, $auto_id)
    {
        $qForId = ORM::factory('AutoevaluacionFormato')
            ->join(array('autoevaluacion', 'b'))->using('auto_id')
            ->join(array('acreditado_evento', 'ae'))->using('acev_id')
            ->where('ae.acre_id', '=', $acre_id)
            ->where('b.auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
            ->where('docu_id', '=', $docu_id)
            ->group_by('form_id');

        if ($auto_id) {
            $qForId->where('auto_id', '=', $auto_id);
        } else {
        }

        $oFormIdes = $qForId->find_all();

        $rs = '';
        foreach ($oFormIdes as $st) {
            $rs .= $st->form_id . ' ';
        }

        return $rs ?: FALSE;
    }

    /**
     * Obtiene todos los cond_id asociados a un documento
     * de la autoevaluación actual o historico
     */
    public function get_condicion_documento($docu_id, $acre_id, $auto_id)
    {
        $query = ORM::factory('AutoevaluacionCondicion')
            ->join(array('autoevaluacion', 'b'))->using('auto_id')
            ->join(array('acreditado_evento', 'ae'))->using('acev_id')
            ->where('ae.acre_id', '=', $acre_id)
            ->where('b.auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
            ->where('docu_id', '=', $docu_id)
            ->group_by('dime_id');

        if ($auto_id) {
            $query->where('auto_id', '=', $auto_id);
        } else {
        }

        $oCondIdes = $query->find_all();

        $rs = '';
        foreach ($oCondIdes as $st) {
            $rs .= $st->dime_id . ' ';
        }

        return $rs ?: FALSE;
    }

    public function con_ponderacion()
    {
        return $this->oAcreditadoEvento->oEvento->even_ponderacion == 1 ? TRUE : FALSE;
    }

    public static function create_copia($oAcreditadoEvento, $oAEanterior)
    {
        $oAutoevaluacion = ORM::factory('Autoevaluacion');        
        $oAutoevaluacion->auto_numero = self::new_auto_numero($oAcreditadoEvento->acev_id);
        $oAutoevaluacion->auto_fecha_inicio = date('Y-m-d');
        $oAutoevaluacion->auto_estado = Model_Saes::STATUS_ACTIVO;
        $oAutoevaluacion->acev_id = $oAcreditadoEvento->acev_id;
        $oAutoevaluacion->save();
        
        $auto = $oAutoevaluacion->get_auto_anterior();
        $oAutoevaluacion->copy_auto_id = $auto->auto_id;
        $oAutoevaluacion->save();
        
        if ($oAutoevaluacion->copiar_autoevaluacion($auto->auto_id)) {
            // llenamos autoevaluación_condicion
            $oAutoevaluacion->copiar_condiciones($auto->auto_id);
            // llenamos autoevaluación_formato
            $oAutoevaluacion->copiar_formatos($auto->auto_id);
        }
    }


    /**
     * Crea el detalle  (tabla: autoevaluacion_detalle)
     * @return type
     */
    public function copiar_autoevaluacion($auto_anterior)
    {
        $oAutoDetalle = $this->get_autodetalle($auto_anterior);

        if (count($oAutoDetalle) > 0) {
            foreach ($oAutoDetalle as $ad) {
                $oAutoDetalleNew = ORM::factory('AutoevaluacionDetalle')
                    ->set('aude_fecha_reg', date('Y-m-d H:i:s'))
                    ->set('aude_estado', $ad->aude_estado)
                    ->set('esta_id', $ad->esta_id)
                    ->set('auto_id', $this->auto_id)
                    ->set('indi_estado', $ad->indi_estado)
                    ->save();

                $aAutoMedio = ORM::factory('AutoevaluacionMedio')
                    ->where('aude_id', '=', $ad->aude_id)
                    ->find_all();

                foreach ($aAutoMedio as $am) {
                    $oAutoMedioNew = ORM::factory('AutoevaluacionMedio')
                        ->set('aude_id', $oAutoDetalleNew->aude_id)
                        ->set('medi_id', $am->medi_id)
                        ->set('esta_id', $am->esta_id)
                        ->set('aume_fecha_reg', date('Y-m-d H:i:s'))
                        ->set('aume_estado', $am->aume_estado)
                        ->set('aume_descripcion', $am->aume_descripcion)
                        ->set('aume_observacion', $am->aume_observacion)
                        ->set('aume_consideracion', $am->aume_consideracion)
                        ->save();

                    $aAutoDocumento = ORM::factory('AutoevaluacionDocumento')
                        ->where('aume_id', '=', $am->aume_id)
                        ->find_all();

                    $query_autodocumento_new = DB::insert('autoevaluacion_documento', array(
                        'aume_id', 'docu_id', 'audo_estado', 'audo_fecha_reg'
                    ));

                    if(count($aAutoDocumento) > 0)
                    {
                        foreach ($aAutoDocumento as $ad) {
                            $query_autodocumento_new->values(array(
                                $oAutoMedioNew->aume_id,
                                $ad->docu_id,
                                $ad->audo_estado,
                                date('Y-m-d H:i:s')
                            ));
                        }

                        $query_autodocumento_new->execute();
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }
    /**
     * Llena tabla autoevaluación_condicion
     */
    public function copiar_condiciones($auto_anterior)
    {
        $aAutoCondicion = DB::select('*')
            ->from('autoevaluacion_condicion')
            ->where('auto_id', '=', $auto_anterior)
            ->as_object()
            ->execute();

        $query = DB::insert(
            'autoevaluacion_condicion', array(
                'auto_id',
                'dime_id',
                'docu_id',
                'auco_fecha_reg',
                'auco_estado'
            )
        );

        foreach ($aAutoCondicion as $ac) {
            $query->values(array(
                $this->auto_id,
                $ac->dime_id,
                $ac->docu_id,
                date('Y-m-d H:i:s'),
                $ac->auco_estado
            ));
        }

        $query->execute();
    }

    /**
     * Llena tabla autoevaluación_formato
     */
    public function copiar_formatos($auto_anterior)
    {
        $aAutoFormatos = DB::select('*')
            ->from('autoevaluacion_formato')
            ->where('auto_id', '=', $auto_anterior)
            ->as_object()
            ->execute();

        $query = DB::insert(
            'autoevaluacion_formato', array(
                'form_id',
                'auto_id',
                'docu_id',
                'aufo_fecha_reg',
                'aufo_estado'
            )
        );

        foreach ($aAutoFormatos as $af) {
            $query->values(array(
                $af->form_id,
                $this->auto_id,
                $af->docu_id,
                date('Y-m-d H:i:s'),
                $af->aufo_estado
            ));
        }

        $query->execute();
    }

    public function get_autodetalle($auto_anterior)
    {
        return DB::select('*')
            ->from('autoevaluacion_detalle')
            ->where('auto_id', '=', $auto_anterior)
            ->as_object()
            ->execute();
    }

    public function get_auto_anterior()
    {
        return DB::select('auto_id')
            ->from('autoevaluacion')
            ->join('acreditado_evento')->using('acev_id')
            ->where('acre_id', '=', $this->oAcreditadoEvento->acre_id)
            ->and_where('auto_estado', '=', Model_Autoevaluacion::STATUS_CERRADO)
            ->order_by('auto_id', 'DESC')
            ->limit(1)
            ->as_object()
            ->execute()
            ->current();
    }

    /**
     * Limpiar los docu_id de las tablas autoevaluacion_formato y autoevaluacion_condicion,
     * además de eliminar los documentos asociados en la tabla documento
     * @return void
     */
    public function limpiar_formatos() {
        
        $docuIds = [];
        
        $doc_formatos = DB::select(DB::expr("COUNT(documento.docu_id) as count_docu"), "docu_id")
                            ->from('autoevaluacion_formato')
                            ->join(DB::expr('documento'), 'left')->using('docu_id')
                            ->where('documento.docu_estado', '=', Model_Autoevaluacion::STATUS_ACTIVO)
                            ->where('documento.acre_id', '=', $this->oAcreditadoEvento->acre_id)
                            ->group_by("documento.docu_id")
                            ->execute();
        
        foreach ($doc_formatos as $key => $docu) {
            if($docu["count_docu"] == 1){
                $docuIds[] = $docu["docu_id"];
            }
        }
        
        $doc_condicion = DB::select(DB::expr("COUNT(documento.docu_id) as count_docu"), "docu_id")
                            ->from('autoevaluacion_condicion')
                            ->join(DB::expr('documento'), 'left')->using('docu_id')
                            ->where('documento.docu_estado', '=', Model_Autoevaluacion::STATUS_ACTIVO)
                            ->where('documento.acre_id', '=', $this->oAcreditadoEvento->acre_id)
                            ->group_by("documento.docu_id")
                            ->execute();
        
        foreach ($doc_condicion as $key => $docu) {
            if($docu["count_docu"] == 1){                
                $docuIds[] = $docu["docu_id"];
            }
        }
		
	DB::update('autoevaluacion_formato')
			->set(['docu_id' => null])
			->where('auto_id','=', $this->auto_id)
            ->execute();            

	DB::update('autoevaluacion_condicion')
			->set(['docu_id' => null])
			->where('auto_id','=', $this->auto_id)
            ->execute();
            
        if (count($docuIds) > 0)
        {
            DB::update('documento')
                ->set(['docu_estado' => Model_Documento::ELIMINADO])
                ->where('docu_id', 'in' , $docuIds)
                ->execute();
        }
    }

    /**
     * Elimina los documentos y medios de verificación asociados que no estén predefinidos
     * @return void 
     */
    public function limpiar_autoevaluacion() {
        
        $oAcev = $this->oAcreditadoEvento;
        $aDocuIds=[];
        
        // docuenmentos que solo estan vinculados a esta evaluacion en medios.
        $autoDocuemnto = DB::select("documento.docu_id", DB::expr("COUNT(documento.docu_id) as count_docu"), "autoevaluacion_detalle.auto_id", "aufo_id", "auco_id")
                        ->from('documento')
                        ->join(DB::expr('autoevaluacion_documento'), 'left')->using('docu_id')
                        ->join(DB::expr('autoevaluacion_medio'), 'left')->using('aume_id')
                        ->join(DB::expr('autoevaluacion_detalle'), 'left')->using('aude_id')
                        ->join(DB::expr('autoevaluacion_formato'), 'left')->on('autoevaluacion_formato.docu_id', '=', 'documento.docu_id')
                        ->join(DB::expr('autoevaluacion_condicion'), 'left')->on('autoevaluacion_condicion.docu_id', '=', 'documento.docu_id')
                        ->where('aufo_id', '=', NULL)
                        ->where('auco_id', '=', NULL)
                        ->where('documento.docu_estado', '=', Model_Autoevaluacion::STATUS_ACTIVO)
                        ->where('documento.acre_id', '=', $oAcev->acre_id)
                        ->group_by("documento.docu_id")
                        ->execute()->as_array('docu_id');
                
        foreach ($autoDocuemnto as $docu) {            
            if($docu["auto_id"] == NULL || ($docu["count_docu"] == 1 && $docu["auto_id"] == $this->auto_id)){
                $aDocuIds[] = $docu["docu_id"];
            }
        }
        
        if(count($aDocuIds) > 0){
            DB::update('documento')
                ->set(['docu_estado' => Model_Documento::ELIMINADO])
                ->where('docu_id', 'in' , $aDocuIds)
                ->execute();
        }
        
        DB::delete('autoevaluacion_detalle')
            ->where('auto_id', '=', $this->auto_id)
            ->execute(); 

        $this->create_detalle();
        $this->set_medios();

    }

    /**
	 * Actualiza el registro editable en la tabla pivot acreditado_evento_ficha
	 */
	public function update_acreditado_evento_ficha($editable,$fich_id = null) {
        $query = DB::update('acreditado_evento_ficha')
                    ->set(array('acfi_editable' => $editable))        
                    ->where('acev_id', '=', $this->oAcreditadoEvento->acev_id);                
        if ($fich_id) {
            $query->and_where('fich_id','=',$fich_id);
        }   
        $query->execute();
	}

    /**
     * Verifica si la Ficha esta guardada en Acreditado_Evento_Ficha.
     */
    public function verificar_ficha_guardarda($ficha_id){

        $counter = DB::select('*')
                    ->from('acreditado_evento_ficha')
                    ->where('acev_id', '=', $this->oAcreditadoEvento->acev_id)
                    ->where('fich_id', '=', $ficha_id)
                    ->where('acfi_editable', '=', Model_Ficha::FICHA_GUARDADA)
                    ->execute()
                    ->count();

        return (bool) $counter;
    }
    
}
