<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_User extends Model_Auth_User {

	const STATUS_ACTIVO   = 1;
	const STATUS_INACTIVO = 0;

	const SUPER_ADMIN_ID  = 19050;
	
	public static $statuses = array(
		self::STATUS_ACTIVO   => 'Activo',
		self::STATUS_INACTIVO => 'Inactivo',
	);

	protected $_belongs_to = array(
		'oRole' => array(
			'model' => 'Role',
			'foreign_key' => 'role_id',
		),
	);
	
	protected $_has_one = array(
		'oAcreditado' => array(
			'model'       => 'Acreditado',
			'foreign_key' => 'acre_id',
		),
	);
	
	public function unique_key($value){

		return 'username';
	}

	/**
     * Validación Fronted
     *
     * @return array
     */
	public static $columns_form =[
		'email',
		'username',
		'role_id',
		'status',
		'password',
		'password_confirm'
	];

	/**
     * Validación Fronted
     *
     * @return array
     */
	public static $labels_required = [
        'email',
        'username',
        'role_id',
        'status'
    ];
	/**
     * Validación Backend
     *
     * @return array
     */
	public function rules()
	{
		return array(
			'username' => array(
				array('not_empty'),
				array(array($this, 'unique'), array('username', ':value')),
				array('min_length', array(':value', 4)),
			),
			'password' => array(
				array('min_length', array(':value', 4)),
				array('not_empty'),
			),
			'email' => array(
				array('not_empty'),
			),
		);
	}

	/**
     * Campos del AdminForm
     *
     * @return array
     */
	public function labels()
	{
		return array(
			'email'      => 'Email',
			'username'   => 'Username',
			'role_id'    => 'Rol',
			'logins'     => 'Logins',
			'last_login' => 'Último login',
			'status'     => 'Estado',
			'password'   => 'Contraseña',
		);
	}

	public function add($alias, $far_keys)
	{
		if ( ! is_array($far_keys))
		{
			$far_keys = array($far_keys);
		}
		
		$new_keys = array();
		foreach ($far_keys as $key)
		{
			if (is_string($key))
			{
				$new_keys[] = ORM::factory('Role', array('name' => $key));
			}
			else
			{
				$new_keys[] = $key;
			}
		}
		
		return parent::add($alias, $new_keys);
	}
	
	public function remove($alias, $far_keys = NULL)
	{
		if ($far_keys == NULL)
		{
			return parent::remove($alias, $far_keys);
		}
		
		if ( ! is_array($far_keys))
		{
			$far_keys = array($far_keys);
		}

		$new_keys = array();
		foreach ($far_keys as $key)
		{
			if (is_string($key))
			{
				$new_keys[] = ORM::factory('Role', array('name' => $key));
			}
			else
			{
				$new_keys[] = $key;
			}
		}
		
		parent::remove($alias, $new_keys);
	}
	
	/**
	 * Recibe el email, obtiene de la base de datos validando el status = 1.
	 * 
	 * @param  string $mail
	 * @return void
	 */
	public function reset_password($mail, $config)
	{
		$aUser = $this
			->where('email', '=', $mail)
			->where('status', '=', self::STATUS_ACTIVO)
			->find_all();

		if ($aUser->count() > 1)
			throw new Exception_Saes('Existe más de un usuario con ese correo. Pruebe ingresando su nombre de usuario');
		
		if ($aUser->count() == 0)
		{
			$oUser = $this->reset()->where('username', '=', $mail)->find();
			
			if ( ! $oUser->loaded())
				throw new Exception_Saes('Correo de contacto no registrado');
		}
		else
		{
			$oUser = $aUser->current();
		}

		$new_pass = Text::random('alnum');

		//file_put_contents(APPPATH.'file.txt', $new_pass);

		$oUser->password = $new_pass;

		$oUser->save();

		/*mail($oUser->email,
			'Procalidad - Password',
			'Credenciales de ingreso'.M_EOL
			.'Usuario: '.$oUser->username.M_EOL
			.'Password: '.$new_pass);*/		

		$msg = "Credenciales de ingreso".M_EOL;
		$msg .= "Usuario: {$oUser->username}".M_EOL;
		$msg .= "Password: {$new_pass}".M_EOL;
			
		$email = Email::factory("{$config->site_shortname} Recuperacion de contraseña",$msg)
			->to($mail)
			->from($config->noreply_mail, $config->site_shortname)
			->send();
		
	}
	
	/**
	 * Crea un nuevo usuario a partir de los datos. 
	 *
	 * @param  string $username
	 * @param  string $password
	 * @param  string $email
	 * @return Model
	 */
	public static function create_new($username, $password, $email)
	{
		$oUser = new self;

		$oUser->values(array(
			'username' => $username,
			'password' => $password,
			'email'    => $email,
			'role_id'  => Model_Role::ROLE_REGULAR,
		))->create();

		return $oUser;
	}
	
	public static function get_password_validation($values)
	{
		return Validation::factory($values)
			->rule('password', 'min_length', array(':value', 5))
			->rule('password_confirm', 'matches', array(':validation', ':field', 'password'));
	}
	
	//VALIDA SI EL CAMPO PASSWORD ESTA VACIO NO ACTUALIZARA EL MISMO
	public function update(\Validation $validation = NULL)
	{
		$values = $this->original_values();

		if ($this->password == '')
		{
			$this->password = $values['password'];
		}

		parent::update($validation);
	}

	public function is_acreditado()
	{
		//return $this->has('roles', Model_Role::ROLE_REGULAR);
		return $this->role_id == Model_Role::ROLE_REGULAR;
	}
	
	/*
	public function get_reportes(Model_Autoevaluacion $oAutoevaluacion)
	{
		$reportes = $oAutoevaluacion->get_reportes();
		
		if ($this->is_acreditado())
		{
			return array_slice($reportes, 0, 2);
		}
		
		return $reportes;
	}
	*/
	
	/**
	 * Verifica si el usuario actual tiene rol de administrador
	 *
	 * @return bool
	 */
	public function is_admin() {

		return ($this->role_id == Model_Role::ROLE_ADMIN) ? TRUE : FALSE;
	}
	
}

// End User Model
