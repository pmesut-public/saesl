<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_Factor extends Model_Saes {

	public function labels()
	{
		return array(
			'fact_codigo'      => 'Código',
			'fact_titulo'      => 'Título',
			'fact_descripcion' => 'Descripción',
			'dime_id'          => 'Condición',
			'fact_estado'      => 'Estado',
		);
	}
	
	public function rules()
	{
		return array(
			'fact_codigo' => array(
				array('not_empty'),
			),
			'fact_titulo' => array(
				array('not_empty'),
			),
			'dime_id' => array(
				array('not_empty'),
			),
            'fact_estado' => array(
				array('not_empty'),
			),
		);
	}
	
	protected $_table_name = 'factor';

	protected $_primary_key = 'fact_id';

	protected $_belongs_to = array(
		'oDimension' => array(
			'model' => 'Dimension',
			'foreign_key' => 'dime_id',
		),
	);
	
	protected $_has_many = array(
		'aEstandar' => array(
			'model' => 'Estandar',
			'foreign_key' => 'esta_id',
		),
	);
	
	public function title()
	{
		return $this->fact_codigo.'. '.$this->fact_titulo;
	}
	
	public function titulo()
	{
		return $this->fact_titulo;
	}
    
    /**
	 * Obtiene los factores
	 */
	public static function get_all()
	{
		return DB::select()
			->from('factor')
			->where('fact_estado', '=', Model_Factor::STATUS_ACTIVO)
			->execute()
			->as_array('fact_id', 'fact_titulo');
	}

    public static function get_all2()
    {
        return DB::select()
            ->from('factor')
            //->where('fact_estado', '=', Model_Factor::STATUS_ACTIVO)
            ->execute()
            ->as_array('fact_id', 'fact_titulo');
    }
	
	/*public function aEstandarReport($auto_id, $flag1 = FALSE, $flag2 = FALSE)
	{
		$query = ORM::factory('Estandar')
			->select('autoevaluacion_detalle.*')
			->select(array(DB::expr("if(aude_cumplimiento = 1, (3 - aude_calidad) / 2, '-')"), 'calidad'))
			->select(array(DB::expr("if(aude_cumplimiento = 1, ((3 - aude_calidad) / 2) * aude_aceptacion, '-')"), 'aprobacion'))
			->with('oCriterio')
			->join('autoevaluacion_detalle')
				->using('esta_id')
			->where('auto_id', '=', $auto_id)
			//->where('dime_id', '=', $this->dime_id)
			->where('fact_id', '=', $this->fact_id);
		
		if ($flag1)
		{
			$query->where(DB::expr('ifnull(aude_cumplimiento, 0)'), '=', 0);
		}
		if ($flag2)
		{
			$query->where('esta_clasificacion', '=', $flag2);
		}
		
		return $query
			->find_all();
	}*/
	
	public static function get_actividad_relacionada($fact_id, $auto_id)
	{	
		return DB::select()
			->from('autoevaluacion_detalle_actividad')
			->join('autoevaluacion_detalle')->using('aude_id')
			->join('estandar')->using('esta_id')
			->where('estandar.fact_id', '=', $fact_id)
			->where('autoevaluacion_detalle.auto_id', '=', $auto_id)
			->execute();
	}
	
	public static function get_actividad($fact_id, $auto_id)
	{	
		return DB::select()
			->from('autoevaluacion_actividad')
			->join('autoevaluacion_detalle')->using('aude_id')
			->join('estandar')->using('esta_id')
			->where('estandar.fact_id', '=', $fact_id)
			->where('autoevaluacion_detalle.auto_id', '=', $auto_id)
			->execute();
	}

	public function has_estandares()
    {
        $count_estandar = DB::select(DB::expr("COUNT(esta_id) as count"))
            ->from('estandar')
            ->where('fact_id', '=', $this->fact_id)
            ->and_where('esta_estado', '=', Model_Saes::STATUS_ACTIVO)
            ->execute()
            ->get('count');

        return $count_estandar > 0 ? TRUE : FALSE;
    }
	
}
