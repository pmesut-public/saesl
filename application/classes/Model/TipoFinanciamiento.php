<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Model_TipoFinanciamiento extends Model_Saes {

	protected $_created_column = FALSE;
	
	const PLAN_MEJORA_INSTITUCIONAL = 1;
	const PLAN_MEJORA_CARRERA = 2;
	
	public static $planes_mejora = array(
		self::PLAN_MEJORA_INSTITUCIONAL,
		self::PLAN_MEJORA_CARRERA,
	);
	
	public function labels()
	{
		return array(
			'tifi_nombre' => 'Nombre',
			'tifi_alias' => 'Alias',
			'tifi_tipo_acreditacion' => 'Tipo de acreditación',
		);
	}
	
	protected $_table_name = 'tipo_financiamiento';

	protected $_primary_key = 'tifi_id';
	
	/*
	 * Obtiene todos los tipos de financiamiento
	 */
	public static function get_all()
	{
		return DB::select()
			->from('tipo_financiamiento')
			->execute()
			->as_array('tifi_id', 'tifi_nombre');
	}
	
}
