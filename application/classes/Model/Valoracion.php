<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Model_Valoracion extends Model_Saes {

	public function labels()
	{
		return array(
			'valo_codigo'      => 'Código',
			'valo_titulo'      => 'Título',
                        'valo_equivalencia'=> 'Equivalencia',
			'valo_descripcion' => 'Descripción',
			'valo_evidencia'   => 'Evidencia',
			'valo_estado'      => 'Estado',
		);
	}
	
	public function rules()
	{
		return array(
			'valo_codigo' => array(
				array('not_empty'),
			),
			'valo_titulo' => array(
				array('not_empty'),
			),
                        'valo_equivalencia' => array(
				array('not_empty'),
			),
                        'valo_estado' => array(
				array('not_empty'),
			),
		);
	}
	
	protected $_table_name = 'valoracion';

	protected $_primary_key = 'valo_id';
	
	public function title()
	{
		return $this->valo_codigo.'. '.$this->valo_titulo;
	}
	
	public function titulo()
	{
		return $this->valo_titulo;
	}
    
    /**
	 * Obtiene las valoraciones
	 */
	public static function get_all()
	{
		return DB::select()
			->from('valoracion')
			->where('valo_estado', '=', Model_Factor::STATUS_ACTIVO)
			->execute()
			->as_array('valo_id', 'valo_titulo');
	}
	
	
}
