<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_Autoevaluacion extends Repository {
	
	public function with_resumen()
	{
		$this->value
			->join('autoevaluacion_detalle')->using('auto_id')
			->join('estandar')->using('esta_id')
			->join('acreditado_evento')->using('acev_id')
			->join(['acreditado', 'a'])->using('acre_id')
			->join('objeto_acreditacion')->using('obac_id')
			->select(Model_Autoevaluacion::get_cumplimiento());
		
		return $this;
	}
	
	/*public function from_acre($acre_id)
	{
		$this->value
			->join('acreditado_evento')->using('acev_id')
			->where('acre_id', '=', $acre_id);
		
		return $this;
	}
	
	public function get_latest()
	{
		$this->value
			->join(DB::expr("
				(select acev_id, max(auto_id) max_auto_id
				from autoevaluacion
				where auto_estado = '".Model_Autoevaluacion::STATUS_CERRADO."'
				group by acev_id) max_auto"))->on('auto_id', '=', 'max_auto_id');
		
		return $this;
	}
	
	public function filter_fec()
	{
		$this->value
			->join(DB::expr('evento e'))->using('even_id')
			->where('even_fec', '=', Model_Evento::EVEN_FEC);
		
		return $this;
	}
	
	public function filter_acev($statuses)
	{
		$this->value
			->where('acev_estado', 'in', $statuses);
		
		return $this;
	}*/
	
}
