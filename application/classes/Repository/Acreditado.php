<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_Acreditado extends Repository {
	
	public function with_titles()
	{
		$this->value
			->add_titles()
			->with('oUser')
			->with('oObjetoAcreditacion:oTipoInstitucion')
			->with('oModalidad')
			->select(DB::expr("concat(tiin_nombre, ' - ', obac_nombre) obac"))
			->select('i.*', 'c.*')
			->select(['i.inst_id', 'inst_id']);
		
		return $this;
	}
	
	public function filter()
	{
		$this->value
			->where('oUser.status', '=', Model_User::STATUS_ACTIVO)
			->where('acre_aprobado', '=', Model_Acreditado::ESTADO_ACTIVO);
			//->where('acre_test', '=', Model_Acreditado::TIPO_REAL);
		
		return $this;
	}
	
	public function fec_list()
	{
		$acre_id = $this->value->acre_id;
		
		return DB::select('e.*', 'ae.acev_id', 'max_auto_id')
			->from(DB::expr('acreditado_evento ae'))
			->join(DB::expr('evento e'))->using('even_id')
			->join(DB::expr('autoevaluacion b'))->using('acev_id')
			->join(DB::expr('autoevaluacion_detalle ad'))->using('auto_id')
			
			// max auto id
			->join(DB::expr("
				(select acev_id, max(auto_id) max_auto_id
				from autoevaluacion
				where auto_estado = '".Model_Autoevaluacion::STATUS_CERRADO."'
				group by acev_id) max_auto"))->on('b.auto_id', '=', 'max_auto_id')
			
			->where('even_fec', '=', Model_Evento::EVEN_FEC)
			->where('acev_estado', 'in', Model_AcreditadoEvento::$closed)
			->where('acre_id', '=', $acre_id);
	}
	
	public function from_even($even_id)
	{
		$this->value = Report::factory('gproc_users', array('_even_id' => $even_id))
			->execute()
			->get_data();
		
		return $this;
	}
	
}
