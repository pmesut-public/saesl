<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Repository_Evento extends Repository {
	
	public function with_full_name()
	{
		$this->value
			->select('tiin_nombre')
			->select(DB::expr("concat(tiin_nombre, ' - ', even_nombre) even_name"))
			->join('tipo_institucion')->using('tiin_id');
		
		return $this;
	}
	
}
