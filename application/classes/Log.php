<?php defined('SYSPATH') OR die('No direct script access.');

class Log extends Kohana_Log {
	
	public static function error($message, $params = array())
	{
		$file = 'errores.txt';
		
		self::put($file, $message, $params);
	}
	
	public static function placseas($message, $params = array())
	{
		$file = 'placseas.txt';
		
		self::put($file, $message, $params);
	}
	
	public static function put($file, $message, $params)
	{
		$message = strtr($message, $params);
		
		$file = APPPATH.'logs/'.$file;
		
		$full_msg = date('Y-m-d H:i:s').' - '.$message.PHP_EOL;
		
		file_put_contents($file, $full_msg, FILE_APPEND);
	}
	
	/**
	 * 
	 * @param  string  $message
	 * @param  array  $params
	 */
	public static function access($message, $params = array())
	{
		$user = ACL::instance()->get_user()->username;
		
		$message = str_pad($user, 20).$message;
		
		$file = 'access.txt';
		
		self::put($file, $message, $params);
	}
	
}
