<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class PlacseasProxy {
	
	protected $_config = array(
		Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD => array(
			'course'     => 'PAU01',
			'c_id'       => 16,
		),
		Model_ObjetoAcreditacion::TIPO_INSTITUTO => array(
			'course'     => 'PAI01',
			'c_id'       => 17,
		),
		'course_path' => array(
			SERVER_PROD  => '/var/www/vhosts/sael/httpdocs/campus/courses/:course/document',
			SERVER_DEMO  => '',
			SERVER_DEV   => 'C:/xampp/htdocs/campus/courses/:course/document',
		),
		'even_path'      => '/Reportes/:even',
		'reportes_path'  => '/Reportes',
		'reportes_title' => 'REPORTES DE AUTOEVALUACION',
		'report_mask'    => 'SAES_:auto_:report',
		//'url'            => '/reportes/excel/:report?id=:auto&view=1',
	);
	
	/**
	 *
	 * @var Model_Acreditado
	 */
	protected $oAcreditado;
	
	/**
	 *
	 * @var Model_Autoevaluacion
	 */
	protected $_assessment;
	
	protected $_tipo_reg;
	protected $_tipo_inst;
	protected $_course;
	protected $_c_id;
	protected $_institution;
	protected $_carrera;
	protected $_course_path;
	//protected $_inst_path;
	//protected $_carr_path;
	protected $_even_path;
	protected $_even_alias;
	
	/**
	 *
	 * @var Database
	 */
	protected $_db;
	
	public function __construct(Model_Autoevaluacion $oAssessment)
	{
		//$oAssessment//->clear()
			//->with('oAcreditoEvento:oAcreditado:oObjetoAcreditacion:oTipoInstitucion')
			//->select('tiin_id')
			//->select(DB::expr("concat(tiin_nombre, ' - ', obac_nombre) objeto"))
			//->select(DB::expr("coalesce(auto_fecha_fin, auto_fecha_inicio) fecha_cierre"))
			//->find();
			//debug($oAssessment);
		
		
		$config = $this->_config;
		
		$course_path = $config['course_path'][Kohana::$server_name];
		
		/*if (Kohana::$environment == Kohana::PRODUCTION)
		{
			$config['course_path'] = '/var/www/vhosts/procalidad.gob.pe/httpdocs/campus/courses/:course/document';
		}*/
		
		$this->oAcreditado = $oAssessment->oAcreditadoEvento->oAcreditado;
		$this->_assessment = $oAssessment;
		
		$this->_tipo_reg = $this->oAcreditado->oObjetoAcreditacion->obac_tipo_acreditacion; // carrera o institucion
		$this->_tipo_inst = $this->oAcreditado->oObjetoAcreditacion->tiin_id; // universidad o instituto (1 o 3)
		
		$this->_course = $config[$this->_tipo_inst]['course'];
		$this->_c_id = $config[$this->_tipo_inst]['c_id'];
		
		$this->_institution = $this->oAcreditado->oInstitucion()->name;
		$this->_carrera = $this->oAcreditado->oCarrera()->oCarrera->carr_nombre;
		
		$this->_course_path = strtr($course_path /*$config['course_path']*/, array(
			':course' => $this->_course,
		));
		
		$this->_even_alias = $oAssessment->oAcreditadoEvento->oEvento->even_alias;
		$this->_even_path = strtr($config['even_path'], array(
			':even' => $this->_even_alias,
		));
		
		$this->_db = Database::instance('placseas');
	}
	
	public function save()
	{
		if ($this->_do_nothing())
		{
			return FALSE;
		}
		
		$paths = $this->_create_folders();
		
		$reportes = $this->get_reportes();
		
		$this->_execute($reportes, $paths);
	}
	
	public function get_reportes_placseas()
	{
		if ($this->_do_nothing())
		{
			return array();
		}
		
		$reportes = $this->get_reportes();
		
		//$paths = $this->_get_paths();
		
		$reportes_placseas = array();
		foreach (array_keys($reportes) as $report)
		{
			/*$filename = $this->_get_filename($report);
			$rel_path = $paths['rel_path'].'/'.URL::title($filename, '-', TRUE).'.xls';
			
			$exists = DB::select('*')
				->from('c_document')
				->where('path', '=', $rel_path)
				->where('title', '=', $filename)
				->execute($this->_db)
				->count();
			
			$reportes_placseas[$report] = (bool) $exists;*/
			
			$reportes_placseas[$report] = $this->_report_exists($report);
			
		}
		
		return $reportes_placseas;
	}
	
	public function delete()
	{
		if ($this->_do_nothing())
		{
			return FALSE;
		}
		
		$reportes = $this->get_reportes();
		//debug2($reportes);
		
		foreach ($reportes as $report => $params)
		{
			if ($this->_report_exists($report))
			{
				$this->_delete_report($report);
			}
		}
		
		return TRUE;
	}
	
	private function _do_nothing()
	{
		return (Kohana::$server_name == SERVER_DEMO OR Kohana::$server_name == SERVER_DEV);
	}
	
	protected function _create_folders()	
	{
		// Reportes directory
		$path = $this->_course_path.$this->_config['reportes_path'];
		$rel_path = $this->_config['reportes_path'];
		
		if ( ! file_exists($path))
		{
			$this->_create_dir($path, $rel_path, $this->_config['reportes_title']);
		}
		
		// Event directory
		$path = $this->_course_path.$this->_even_path;
		$rel_path = $this->_even_path;
		
		if ( ! file_exists($path))
		{
			$this->_create_dir($path, $rel_path, $this->_even_alias);
		}
		
		return array(
			'path' => $path,
			'rel_path' => $rel_path,
		);
	}
	
	protected function _get_paths()
	{
		$path = $this->_course_path.$this->_even_path;
		$rel_path = $this->_even_path;
		
		return array(
			'path' => $path,
			'rel_path' => $rel_path,
		);
	}
	
	protected function _execute($reportes, $paths)
	{
		$path = $paths['path'];
		$rel_path = $paths['rel_path'];
		
		// @ TEMP!!!!!
		//$reportes = $this->_assessment->_get_reportes();
		
		foreach (array_keys($reportes) as $report)
		{
			$response = $this->_get_report($report);
			//debug($response);
			
			$file = $this->_get_filename($report);
			
			$this->_create_file($path, $rel_path, $file, $response, $report);
		}
		
	}
	
	private function _get_report($report)
	{
		$oAutoevaluacion = ORM::factory('Autoevaluacion')
			->add_reportes_stuff()
			->where('auto_id', '=', $this->_assessment->auto_id)
			->find();
		
		Theme::instance()->css(array());
		
		return Theme_View::factory('reportes/template')
			->set('content', Report_Auto::factory($report, $oAutoevaluacion)->get_excel());
	}
	
	protected function _get_filename($report)
	{
		$reportes = $this->get_reportes();
		
		$file_mask = ':even - :inst - :carr - :number. :report';
		$file = strtr($file_mask, array(
			':even' => $this->_even_alias,
			':inst' => $this->_institution,
			':carr' => $this->_carrera,
			':number' => $this->_assessment->auto_numero,
			':report' => $reportes[$report]['short'],
		));
		
		return $file;
	}
	
	/**
	 * 
	 * @param  string  $path		real path
	 * @param  string  $rel_path	path to be saved in DB
	 * @param  string  $title		title
	 */
	protected function _create_dir($path, $rel_path, $title)
	{
		$a = mkdir($path);
		
		$b = DB::insert('c_document')->columns(array(
				'c_id', 'id', 'path', 'comment', 'title', 'filetype', 'size', 'readonly', 'session_id',
			))->values(array(
				$this->_c_id, NULL, $rel_path, '', $title, 'folder', 0, 0, 0,
			))->execute($this->_db);
		
		$c = DB::insert('c_item_property')->columns(array(
				'c_id', 'id', 'tool', 'insert_user_id', 'insert_date', 'lastedit_date', 'ref', 
				'lastedit_type', 'lastedit_user_id', 'to_group_id', 'to_user_id', 'visibility', 
				'start_visible', 'end_visible', 'id_session',
			))->values(array(
				$this->_c_id, NULL, 'document', 1, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'), $b[0], 
				'FolderCreated', 1, 0, NULL, 1, 
				'0000-00-00 00:00:00', '0000-00-00 00:00:00', 0,
			))->execute($this->_db);
	}
	
	protected function _create_file($path, $rel_path, $filename, $data, $report)
	{
		$path .= '/'.URL::title($filename, '-', TRUE).'.xls';
		$rel_path .= '/'.URL::title($filename, '-', TRUE).'.xls';
		
		$info = array(
			':report' => $report,
			':auto' => $this->_assessment->auto_id,
		);
		
		if ($this->_report_exists($report))
		{
			//Log::placseas('** Report :report for Autoevaluacion :auto already exists **', $info);
			$this->_log('** Report :report for Autoevaluacion :auto already exists **', $info);
			
			return;
		}
		
		$a = file_put_contents($path, $data);
		
		$b = DB::insert('c_document')->columns(array(
				'c_id', 'id', 'path', 'comment', 'title', 'filetype', 'size', 'readonly', 'session_id',
			))->values(array(
				$this->_c_id, NULL, $rel_path, $this->_report_id($report), $filename, 'file', filesize($path), 0, 0,
			))->execute($this->_db);
		
		$c = DB::insert('c_item_property')->columns(array(
				'c_id', 'id', 'tool', 'insert_user_id', 'insert_date', 'lastedit_date', 'ref', 
				'lastedit_type', 'lastedit_user_id', 'to_group_id', 'to_user_id', 'visibility', 
				'start_visible', 'end_visible', 'id_session',
			))->values(array(
				$this->_c_id, NULL, 'document', 1, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'), $b[0], 
				'DocumentAdded', 1, 0, NULL, 1, 
				'0000-00-00 00:00:00', '0000-00-00 00:00:00', 0,
			))->execute($this->_db);
		
		//Log::placseas('Report :report for Autoevaluacion :auto created', $info);
		$this->_log('Report :report for Autoevaluacion :auto created', $info);
	}
	
	protected function _report_id($report)
	{
		return strtr($this->_config['report_mask'], array(
			':auto' => $this->_assessment->auto_id,
			':report' => $report,
		));
	}
	
	protected function _report_exists($report)
	{
		return (bool) $this->_get_query_c_document(DB::select('*'), $report)
			->count();
	}
	
	protected function _delete_report($report)
	{
		$id = $this->_get_query_c_document(DB::select('*'), $report)
			->get('id');
			//debug2($id);
		
		$items = $this->_get_query_c_item_property(DB::select('*'), $id);
		
		if (($count = $items->count()) != 1) throw new Exception_Saes('Should only find 1 c_item_property. Found: '.$count);
		//debug2($items->current());
		
		$this->_get_query_c_item_property(DB::delete(), $id);
		
		$this->_get_query_c_document(DB::delete(), $report);
		
		$info = array(
			':report' => $report,
			':auto' => $this->_assessment->auto_id,
		);
		
		//Log::placseas('Report :report for Autoevaluacion :auto deleted', $info);
		$this->_log('Report :report for Autoevaluacion :auto deleted', $info);
	}
	
	private function _get_query_c_document(Database_Query_Builder $DB, $report)
	{
		$method = method_exists($DB, 'from') ? 'from' : 'table';
		
		return $DB
			->{$method}('c_document')
			->where('comment', '=', $this->_report_id($report))
			->where('path', 'not like', '%DELETED%')
			->execute($this->_db);
	}
	
	private function _get_query_c_item_property(Database_Query_Builder $DB, $id)
	{
		$method = method_exists($DB, 'from') ? 'from' : 'table';
		
		return $DB
			->{$method}('c_item_property')
			->where('ref', '=', $id)
			->where('c_id', '=', $this->_c_id)
			->execute($this->_db);
	}
	
	public function get_reportes()
	{
		$reportes = $this->_assessment->get_reportes();
		
		unset($reportes['report3']);
		unset($reportes['report6']);
		
		return $reportes;
	}
	
	/*private function get_url($report)
	{
		return strtr($this->_config['url'], array(
			':report' => $report,
			':auto' => $this->_assessment->auto_id,
		));
	}*/
	
	protected function _log($message, $params)
	{
		Log::placseas($message, $params);
		
		$prev = Session::instance()->get('info');
		Session::instance()->set('info', $prev.H_EOL.strtr($message, $params));
	}
	
}
