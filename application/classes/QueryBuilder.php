<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @package    Procalidad
 * @author     Procalidad Team
 * @copyright  ProCalidad 2018
 */
class QueryBuilder{

	protected $sqlpathname = [];

	protected $parameters = [];

	public function __construct($sqlPathname, $parameters){

		$this->sqlpathname = $sqlPathname;
		$this->parameters  = $parameters;
	}
	
	public static function factory($sqlPathname = '', $parameters = []){

		return new QueryBuilder($sqlPathname, $parameters);
	}

	public function path(){

		return APPPATH."scripts/{$this->sqlpathname}";;
	}

	public function raw(){

		$path = $this->path();

		if (!file_exists($path)) {
			
			debug("No se ha encontrado el archivo: ". $path);
		}

		return file_get_contents($path);
	}

	public function query(){

		$query = strtr($this->raw(), $this->parameters);

		return $query;
	}

	public function execute(){

		return DB::query(Database::SELECT, $this->query())->execute();
	}

	public function dict($key){

		$this->dict = $key;

		return $this;
	}

	public function get(){

		if (property_exists($this, 'dict')) {
			
			return $this->execute()->as_array($this->dict);
		}

		return $this->execute()->as_array();
	}

	public function getParameters(){

		return $this->parameters;
	}

}
