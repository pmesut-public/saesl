<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Ficha_FICHA002 extends Ficha {
	
	public function get_context()
	{
		$context = parent::get_context();
		
		$oAcreditadoEvento = $context['oAcreditadoEvento'];
		$oFicha = $context['oFicha'];
		//debug($oFicha);
		
		$context['plan_mejora'] = in_array($oAcreditadoEvento->tifi_id, Model_TipoFinanciamiento::$planes_mejora);
		
		$context['ficha1'] = Ficha::factory($oAcreditadoEvento->oFicha())
			->get_data();
		
		//debug($context['ficha1']);
		//debug($context);
		return $context;
	}
	
}
