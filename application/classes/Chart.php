<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Chart {
	
	protected $properties;
	
	/**
	 *
	 * @var  Report
	 */
	protected $report;
	
	protected $json;
	
	protected $buttons = array(
		'print', 'pdf', 'png', 'excel',
	);
	
	public static function factory(Report $report)
	{
		$class = 'Chart_'.ucfirst(Inflector::camelize($report->get_name()));
		
		return new $class($report);
	}
	
	public function __construct(Report $report)
	{
		Theme::instance()
			->js('default', 'highcharts', '/saes_lte/js/plugins/highcharts/highcharts.js')
			->js('default', 'highcharts.3d', '/saes_lte/js/plugins/highcharts/highcharts-3d.js')
			->js('default', 'highcharts.exporting', '/saes_lte/js/plugins/highcharts/exporting.js');
		
		$this->report = $report;
	}
	
	public function get()
	{
		$this->set_json();
		
		return Theme_View::factory('reportes/chart_template')
			->set('json', $this->json)
			->set('name', $this->report->get_name())
			->set('params', $this->report->get_params())
			->set('buttons', $this->get_buttons());
	}
	
	protected function set_json()
	{
		//$this->json = $this->properties;
	}
	
	protected function get_buttons()
	{
		$buttonsHTML = array(
			'print' => HTML::anchor('#', 'Imprimir', array('data-action' => 'print')),
			'pdf' => HTML::anchor('#', 'Exportar como PDF', array('data-action' => 'pdf')),
			'png' => HTML::anchor('#', 'Exportar como PNG', array('data-action' => 'png')),
			'excel' => HTML::anchor('/admin/report/excel/'.$this->report->get_name().URL::query($this->report->get_params(), FALSE),
				'Exportar datos como Excel', array('class' => 'btn-excel')),
		);
		
		return array_intersect_key($buttonsHTML, array_flip($this->buttons));
	}
	
}
