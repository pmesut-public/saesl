<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class ACL extends Kohana_ACL {
	
	const SUPER_ID = 1;
	const ROLE_SUPER = 'super';
	
	public function __construct($init = FALSE)
	{
		parent::__construct($init);

		$this->register();
	}
	
	private function register()
	{
		if (Auth::instance()->get_user()->id == self::SUPER_ID)
		{
			$this->_auth->set_role(self::ROLE_SUPER);
		}
		
		$this->add_resources();
		
		$this->add_roles();
		
		$this->add_rules();
	}
	
	private function add_resources()
	{
		$this->add_controllers('backend');
		$this->add_controllers('frontend', TRUE);
		//debug($this);
	}
	
	public function add_controllers($resource, $add_prefix = FALSE)
	{
		$prefix = $add_prefix ? $resource.'.' : NULL;
		
		$this->add_resource($resource);
		
		$controllers = Kohana::list_files('classes/Controller/'.ucfirst($resource), array(APPPATH));
		
		foreach ($controllers as $controller => $path)
		{
			$controller = lcfirst(basename($controller, EXT));
			//debug2($controller);
			
			$this->add_resource($prefix.$controller, $resource);
		}
		
		//debug($this);
	}
	
	private function add_roles()
	{
		$this->add_role('regular');
		$this->add_role('supervisor');
		$this->add_role('operador', 'supervisor');
		$this->add_role('admin', 'operador');
		$this->add_role('super', 'admin');
		
		//debug($this);
	}
	
	private function add_rules()
	{
		// Regular
		$this->allow('regular', 'frontend');
		$this->deny('regular', 'frontend.reportes', array('report3', 'report4', 'report5', 'report8'));
		$this->deny('regular', 'frontend.autoevaluacion', 'force_finish');
		
		// Supervisor
		$this->allow('supervisor', 'dashboard', 'index');
		$this->allow('supervisor', 'acreditado', array('index', 'universidad', 'instituto'));
		$this->allow('supervisor', 'autoevaluacion', array('index', 'universidad', 'instituto'));
		$this->allow('supervisor', 'reportes');
		$this->allow('supervisor', 'report');
		$this->allow('supervisor', 'usuario', 'index');
		
		$this->allow('supervisor', 'frontend.reportes');
		$this->allow('supervisor', 'frontend.autoevaluaciones', 'index');
		$this->allow('supervisor', 'frontend.eventos', 'index');
		
		// Operador
		$this->allow('operador', 'dashboard');
		$this->allow('operador', 'acreditado', array('view', 'committee', 'activate', 'mark_test', 'approve_comision', 'add_seguimiento', 'add_autoevaluacion'));
		$this->allow('operador', 'institucion');
		$this->allow('operador', 'carrera');
		$this->allow('operador', 'mensaje', array('send_comite', 'send_general', 'send_mensaje', 'get_message'));
		
		// Super & Admin
		$this->allow('super', 'backend');
		$this->allow('admin', 'backend');
		$this->allow('admin', 'frontend');
		
		$this->deny('admin', 'acreditado', array('all'));
		$this->deny('admin', 'autoevaluacion', array('placseas', 'generate_reports', 'force_finish'));
		$this->deny('admin', 'frontend.autoevaluacion', 'force_finish');
		//$this->deny('admin', 'acreditadomensaje');
		//$this->deny('admin', 'acreditado', 'force');
		$this->deny('admin', 'utils');
		
		//$a = $this->is_allowed('admin', 'backend', 'index');
		//debug($a);
	}
	
	public function get_sidebar()
	{
		$items = Kohana::$config->load('sidebar')->as_array();
		//debug($items);
		
		$sidebar = Arr::filter_recursive($items, array($this, 'check_resource'));
		//debug($sidebar);
		
		return $sidebar;
	}
	
	public function check_resource($path)
	{
		//debug2($path);
		
		$item = explode('/', $path);
		//debug($item);
		
		$resource = $item[0];
		$privilege = Arr::get($item, 1);
		//debug($privilege);
		//return $this->is_allowed('admin', $resource, $privilege) ? $path : FALSE;
		return $this->check($resource, $privilege) ? $path : FALSE;
	}
	
	public function get_role()
	{
		return $this->_auth->get_role_id();
	}
	
	public function check_role($role)
	{
		//return $this->get_role() === $role;
		return $this->get_role_id() === $role;
	}
	
	public function get_role_id()
	{
		return ACL::get_user()->role_id;
	}
	
}
