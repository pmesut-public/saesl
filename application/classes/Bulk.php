<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Bulk {
	
	/**
	 * 
	 * @param  type $type
	 * @return  Bulk
	 */
	public static function factory($type)
	{
		$class = 'Bulk_'.ucfirst($type);
		
		return new $class();
	}
	
	protected $_error;
	
	protected $_data;
	
	protected $_fields;
	
	public function process($file)
	{
		$this->_check_file($file);
		
		$this->_get_data($file);
	}
	
	public function get_status()
	{
		return $this->_error ? $this->_error : TRUE;
	}
	
	protected function _check_file($file)
	{
		if (! Upload::valid($file) OR
			! Upload::not_empty($file) OR
			! Upload::type($file, array('csv')))
		{
			throw new Exception_Saes('Archivo inválido.');
		}
	}
	
	protected function _get_data($file)
	{
		$filename = $file['tmp_name'];
		
		$csv = new parseCSV();
		//$csv->encoding('ISO-8859-1', 'UTF-8');
		$csv->parse($filename);
		//debug($csv->titles);
		$this->_data = $csvData = $csv->data;
		$this->_fields = $csv->titles;
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($csvData);
	}
}
