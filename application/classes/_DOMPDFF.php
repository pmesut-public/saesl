<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class _DOMPDFF extends Common_DOMPDF {
	
	protected $author = 'GPROC - Procalidad';
	
	protected $title = 'GPROC - Procalidad';
	
}
