<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Ficha {
	
	protected $name;
	
	protected $oAcreditadoEvento;
	
	protected $oFicha;
	
	/**
	 * 
	 * @param  type  $oAcreditadoFicha
	 * @return  Ficha
	 */
	public static function factory($oAcreditadoFicha)
	{
		//debug2($oAcreditadoFicha->oEventoFicha);
		$name = $oAcreditadoFicha->oEventoFicha->evfi_nombre;
		//debug2($name);
		
		// @ERROR weirdest error ever...
		//if ($name === 'FICHA002');
		{
			//$name = 'FICHA002';
		}
		
		$class = "Ficha_$name";
		//debug($class);
		
		return new $class ($oAcreditadoFicha, $name);
	}
	
	public function __construct($oAcreditadoFicha, $name)
	{
		$this->name = $name;
		
		$this->oAcreditadoEvento = $oAcreditadoFicha->oAcreditadoEvento;
		
		$this->oFicha = $oAcreditadoFicha;
	}
	
	public function get_data()
	{
		return json_decode($this->oFicha->acfi_data, TRUE);
	}
	
	public function get($action = NULL)
	{
		$this->set_assets();
		
		$ficha = Theme_View::factory('fichas/'.$this->name, $this->get_context())
			->set('action', $action);
		
		return $ficha;
	}
	
	public function get_pdf()
	{
		$ficha = Theme_View::factory('fichas/pdf/'.$this->name, $this->get_context());
		
		return PDF::factory()
			->set('view', $ficha)
			->get();
	}
	
	public function get_context()
	{
		$oAcreditadoEvento = $this->oAcreditadoEvento;
		
		$oAcreditado = $oAcreditadoEvento->oAcreditado;
		
		$title = $oAcreditadoEvento->oEvento->even_nombre;
		
		$oFicha = $this->oFicha;
		
		$ficha = $this->name;
		
		$aTipoFinanciamiento = $oAcreditadoEvento->aTipoFinanciamientoDisponible();
		
		$institucional = //TRUE;
			($oAcreditado->oObjetoAcreditacion->obac_tipo_acreditacion == 
			Model_ObjetoAcreditacion::ACREDITACION_INSTITUCION);
		
		$universidad = //FALSE;
			($oAcreditado->oObjetoAcreditacion->tiin_id == 
			Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD);
		
		$aPersona = DB::select()
			->from('persona')
			->where('acre_id', '=', $oAcreditado->acre_id)
			->where('pers_tipo', 'in', array_keys(Model_Persona::$tipos))
			->as_assoc()
			->execute()
			->as_array();
		
		$data = json_decode($oFicha->acfi_data, TRUE);
		//debug($data);
		
		return get_defined_vars();
	}
	
	private function set_assets()
	{
		Theme::instance()
			->css('page', 'ficha', "/saes_lte/css/{$this->name}.css")
			->js('page', 'ficha', "/saes_lte/js/{$this->name}.js");
	}
	
}
