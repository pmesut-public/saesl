<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

class ORM_Validation_Exception extends Kohana_ORM_Validation_Exception {
	
	/**
	 * Get first error message
	 * 
	 * @param type $directory
	 * @param type $translate
	 * @return type
	 */
	public function message($directory = NULL, $translate = TRUE)
	{
		$error = $this->errors($directory ?: '');
		
		return is_array(current($error)) ? current(current($error)) : current($error);
	}
	
}