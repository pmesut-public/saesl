<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_AutoPercentages extends Report_Tree {
	
	protected $title = 'Autoevaluaciones completas por cumplimiento';
	
	protected $indicadores = array(
		'inst_subtipo',
		'q',
	);
	
}
