<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Factores extends Report_Tree {
	
	protected $title = 'Resultado del proceso de evaluación a nivel nacional';
	
	protected $indicadores = array(
		'dime_titulo',
		'fact_titulo',
		'gestion',
		'inst_region',
	);
	
	public function get_context()
	{
		$subtitle = $this->get_subtitle();
		
		$regiones = $this->get_regiones();
		//debug(get_defined_vars());
		
		return array_merge(parent::get_context(), get_defined_vars());
	}
	
	private function get_subtitle()
	{
		$oObjetoAcreditacion = ORM::factory('ObjetoAcreditacion', $this->params['_obac_id']);
		
		return 'Tipo de carrera: '.$oObjetoAcreditacion->obac_nombre;
	}
	
	private function get_regiones()
	{
		$tree = TreeArray::factory($this->data, array('inst_region'));
		return array_keys($tree->get());
	}
	
}
