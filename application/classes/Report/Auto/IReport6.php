<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_IReport6 extends Report_Auto {
	
	protected $indicadores = array(
		'crit_id' => array('dime_codigo', 'dime_titulo', 'fact_codigo', 'fact_titulo', 'crit_codigo', 'crit_titulo', 'crit_descripcion'),
		'esta_id' => array('esta', 'esta_tipo', 'aude_cumplimiento', 'aude_aceptacion', 'esta_nivel_aceptacion'),
		'conc_id' => array(),
	);
	
}
