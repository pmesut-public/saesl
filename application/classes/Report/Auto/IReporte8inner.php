<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_IReporte8inner extends Report_Auto_UReporte8inner {
	
	protected function set_data()
	{
		$this->data = $this->get_data('I_reporte8')
			->as_array();
	}
	
}
