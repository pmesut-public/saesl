<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_UReport2 extends Report_Auto {
	
	protected $indicadores = array(
		'esta_tipo' => array('cump', 'total'),
		'esta_id' => array('esta', 'aude_cumplimiento'),
		'conc_id' => array(),
	);
	
}
