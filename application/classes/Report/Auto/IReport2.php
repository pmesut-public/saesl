<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_IReport2 extends Report_Auto {
	
	protected $indicadores = array(
		'dime_id' => array('dime_codigo', 'dime_titulo'),
		'fact_id' => array('fact_codigo', 'fact_titulo'),
		'esta_id' => array(),
	);
	
	protected function set_result()
	{
		parent::set_result();
		//debug($this->result);
		
		$estandares = TreeArray::factory($this->data, array('dime_id', 'esta_id'))->get();
		//debug($estandares);
		
		foreach ($this->result as $dime_id => $row)
		{
			$this->result[$dime_id]['estandares'] = count($estandares[$dime_id]);
		}
		//debug($this->result);
	}
	
}
