<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_UReport9 extends Report_Auto {
	
	protected $indicadores = array(
		'iccr_id' => array('iccr_codigo', 'iccr_titulo'),
		'icre_id' => array(),
	);
	
	protected $aCriterio = [];
	protected $aRequisitoIcacit = [];
	
	protected function set_result()
	{
		$this->result = $this->data;
		//debug($this->result);
		
		$aRequisito = ORM::factory('IcRequisito')
			->with('oIcCriterio')
			->select('iccr_codigo')
			->select('iccr_titulo')
			->find_all()
			->as_json_array();
		
		//debug($aRequisito);
		
		$this->aRequisitoIcacit = $this->oAutoevaluacion->get_cumplimiento_icacit($aRequisito);
		$this->aCriterio = TreePowered::factory($aRequisito, $this->indicadores)->get();
	}
	
	protected function set_data()
	{
		$this->data = $this->get_data()
			->as_array('esta_codigo', 'aude_cumplimiento');
	}
	
	public function get_context()
	{
		return parent::get_context() + [
			'aCriterio' => $this->aCriterio,
			'aRequisitoIcacit' => $this->aRequisitoIcacit,
			'aResumenIcacit' => $this->oAutoevaluacion->get_resumen_incacit($this->aRequisitoIcacit),
		];
	}
	
}
