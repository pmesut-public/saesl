<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Report_Auto_ReportC1 extends Report_Auto {
	
	protected $indicadores = array(
		'dime_id' => array('dime', 'dime_codigo', 'dime_valo'),
		'fact_id' => array('fact', 'fact_codigo', 'fact_total', 'fact_valo'),
		'esta_id' => array(),
	);
	
	public function get()
	{
		$result = parent::get();
                //debug($result);
		
		$indicadores = array(
			'dime',
			'fact',
			'esta',
		);
		
		$aDimension = TreeArray::factory($this->data, $indicadores)->get();
		//debug($this->data);
		
		Theme::instance()->template->sidebar = Theme_View::factory('reportes/auto/sidebar')
			->set(compact('aDimension'));
		
		return $result;
	}
	
	public function get_excel()
	{
		return parent::get();
	}
}
