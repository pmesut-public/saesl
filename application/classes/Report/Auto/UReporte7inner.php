<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_UReporte7inner extends Report_Auto {
	
	protected $indicadores = ['dime_codigo', 'fact_codigo'];
	
	protected function set_data()
	{
		$this->data = $this->get_data('U_reporte7')
			->as_array();
	}
	
	protected function set_result()
	{
		$this->result = TreeArray::factory($this->data, $this->indicadores)->get();
		//$this->result = $this->data;
	}
	
}
