<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_UReport4 extends Report_Auto_UReport3 {
	
	public function get()
	{
		$result = parent::get();
		
		$indicadores = array(
			'dime',
			'fact',
			'crit',
			'esta_codigo'
		);
		
		$aDimension = TreeArray::factory($this->data, $indicadores)->get();
		//debug($aDimension);
		
		Theme::instance()->template->sidebar->aDimension = $aDimension;
		
		return $result;
	}
	
	public function get_excel()
	{
		return parent::get();
	}
	
}
