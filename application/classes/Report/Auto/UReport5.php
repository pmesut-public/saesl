<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_UReport5 extends Report_Auto_UReport4 {
	
	public function get_data($script = NULL)
	{
		$this->params['_char'] = $this->oAutoevaluacion->low_attender_char();
		
		return parent::get_data($script);
	}
	
}
