<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_UReport8 extends Report_Auto {
	
	protected $data_conv;
	
	/**
	 *
	 * @var  Model_Autoevaluacion
	 */
	protected $oAutoConv;
	
	public function set_data()
	{
		$this->data = Report_Auto::factory('reporte8inner', $this->oAutoevaluacion)
			->execute()
			->get_result();
			//debug($this->data);
		
		$this->set_data_conv();
	}
	
	private function set_data_conv()
	{
		$this->oAutoConv = $this->oAutoevaluacion->get_auto_convocatoria();
		//debug($oAutoConv);
		
		if ( ! $this->oAutoConv->loaded())
		{
			throw new Exception_Saes('Auto2 not loaded');
		}
		
		$this->data_conv = Report_Auto::factory('reporte8inner', $this->oAutoConv)
			->execute()
			->get_result();
			//debug($this->data_conv);
	}
	
	public function get_context()
	{
		$obac_id = $this->oAutoevaluacion->oAcreditadoEvento->oAcreditado->obac_id;
		$moda_id = $this->oAutoevaluacion->oAcreditadoEvento->oAcreditado->moda_id;
		
		$dime_count = Model_Concepto::get_count('dime', $obac_id, $moda_id);
		$fact_count = Model_Concepto::get_count('fact', $obac_id, $moda_id);
		$esta_count = Model_Concepto::get_count('esta', $obac_id, $moda_id);
		
		return parent::get_context() + 
			[
				'result_conv' => $this->data_conv,
				'oAutoConv' => $this->oAutoConv,
			] +
			compact('dime_count', 'fact_count', 'esta_count');
	}
	
	protected function set_result()
	{
		$this->result = $this->data;
	}
	
}
