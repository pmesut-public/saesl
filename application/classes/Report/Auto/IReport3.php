<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_IReport3 extends Report_Auto_UReport3 {
	
	public function get_data($script = NULL)
	{
		return parent::get_data('U_report3');
	}
	
}
