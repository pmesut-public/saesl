<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_UReporte8inner extends Report_Auto {
	
	/*protected $indicadores = [
		'dime_id' => ['dime', 'dime_codigo'],
		'fact_id' => ['fact', 'fact_codigo'],
		'esta_id' => ['esta', 'esta_codigo'],
		'conc_id' => [],
	];*/
	protected $indicadores = ['dime_codigo', 'fact_codigo', 'esta_codigo', 'conc_codigo'];
	
	protected function set_data()
	{
		$this->data = $this->get_data('U_reporte8')
			->as_array();
	}
	
	protected function set_result()
	{
		//debug($this->data);
		
		//$this->result = TreeArray::factory($this->data, $this->indicadores)->get();
		$this->result = $this->data;
	}
	
	/*public function get_context()
	{
		$obac_id = $this->data[0]['obac_id'];
		
		$dime_count = Model_Concepto::get_count('dime', $obac_id);
		$fact_count = Model_Concepto::get_count('fact', $obac_id);
		//$esta_count = Model_Concepto::get_count('esta', $obac_id);
		
		//debug2($dime_count, $fact_count, $esta_count, 'die');
		
		return parent::get_context() + compact('dime_count', 'fact_count');
	}*/
	
}
