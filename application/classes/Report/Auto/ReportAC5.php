<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Report_Auto_ReportAC5 extends Report_Auto {
	
	protected $indicadores = array(
		'dime_id' => array('dime', 'dime_id', 'dime_codigo'),
		'fact_id' => array('fact', 'fact_id', 'fact_codigo'),
		'esta_id' => array('esta', 'esta_codigo')
	);
	
	public function get()
	{
		$result = parent::get();
		
		$indicadores = array(
			'dime',
			'fact',
			'esta',
		);
		
//		debug($result);
		
		$aDimension = TreeArray::factory($this->data, $indicadores)->get();
//		debug($this->data);
		
		Theme::instance()->template->sidebar = Theme_View::factory('reportes/auto/sidebar')
			->set(compact('aDimension'));
		
		return $result;
	}
	
	public function get_excel()
	{
		return parent::get();
	}
	
}
