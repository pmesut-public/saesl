<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_UReport7 extends Report_Auto {
	
	protected $data_conv;
	
	/**
	 *
	 * @var  Model_Autoevaluacion
	 */
	protected $oAutoConv;
	
	public function set_data()
	{
		$this->data = Report_Auto::factory('reporte7inner', $this->oAutoevaluacion)
			->execute()
			->get_result();
			//debug($this->data);
		
		$this->set_data_conv();
	}
	
	private function set_data_conv()
	{
		$this->oAutoConv = $this->oAutoevaluacion->get_auto_convocatoria();
		//debug($oAutoConv);
		
		if ( ! $this->oAutoConv->loaded())
		{
			throw new Exception_Saes('Auto2 not loaded');
		}
		
		$this->data_conv = Report_Auto::factory('reporte7inner', $this->oAutoConv)
			->execute()
			->get_result();
			//debug($this->data_conv);
	}
	
	public function get_context()
	{
		$oAutoInforme = $this->oAutoevaluacion->oAutoInforme;
		
		$edit = ($oAutoInforme->auin_estado == Model_AutoevaluacionInforme::PENDIENTE);
		
		$informe = $oAutoInforme->as_array();
		$informe['descripcion'] = json_decode($informe['auin_descripcion'], TRUE);
		
		$informe_factores = $oAutoInforme
			->aAutoInformeFactor
			->find_all()
			->as_json_array('fact_codigo');
			//debug($informe_factores);
		
		return parent::get_context() + 
			[
				'result_conv' => $this->data_conv,
				'oAutoConv' => $this->oAutoConv,
			] + 
			compact('informe', 'informe_factores', 'edit');
	}
	
	protected function set_result()
	{
		$this->result = $this->data;
	}
	
	public function get_pdf()
	{
		if ( ! $path = $this->oAutoevaluacion->oAutoInforme->auin_path)
		{
			return parent::get_pdf();
		}
		
		$filename = APPPATH.$path;
		
		Response::factory()
			->send_file($filename);
	}
	
}
