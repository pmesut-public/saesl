<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto_IReport7 extends Report_Auto_UReport7 {
	
	protected function get_view()
	{
		return Theme_View::factory("reportes/{$this->folder}U_report7", $this->get_context());
	}
	
}
