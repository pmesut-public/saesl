<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_DashboardSummary extends Report {
	
	protected $title = 'Resumen';
	
	public function set_result()
	{
		$this->result = current($this->data);
	}
	
}
