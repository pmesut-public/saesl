<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Report_ComitesRegionSumm extends Report {
	
	protected $title = 'Comités de Calidad por región | :inst';
	
	protected $tiin_id;
	
	public function execute()
	{
		parent::execute();
		
		$this->title = strtr($this->title, array(
			':inst' => Inflector::plural(
				Model_ObjetoAcreditacion::$tipos[$this->tiin_id])
			));
		
		return $this;
	}
	
	public function set_data()
	{
		$this->data = Report::factory('comites_region')
			->execute()
			->get_result();
			//debug($this->data);
	}
	
	public function get_chart()
	{
		$this->execute();
		
		return Chart::factory($this)
			->get();
	}
	
}
