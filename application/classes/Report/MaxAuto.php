<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_MaxAuto extends Report {
	
	public $header = array(
		array(
			'Tipo de acreditación' => array('style' => 'width: 15%;'),
			'Modalidad' => array('style' => 'width: 10%;'),
			'Institución' => array('style' => 'width: 30%;'),
			'Carrera' => array('style' => 'width: 20%;'),
			'Gestión' => array('style' => 'width: 10%;'),
			'Fecha de cierre' => array('style' => 'width: 10%;'),
			'Cumplimiento (%)' => array('style' => 'width: 5%;'),
		),
	);
	
}
