<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Cuantitativo extends Report {
	
	protected $title = 'Estadística Cuantitativa de Autoevaluaciones (cerradas y completas)';
	
	public function get_chart()
	{
		$this->execute();
		
		if ( ! $this->result)
			return 'No se registran autoevaluaciones (cerradas y completas)';
		
		return Chart::factory($this)
			->get();
	}
	
}
