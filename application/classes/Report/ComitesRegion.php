<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_ComitesRegion extends Report_Tree {
	
	protected $title = 'Comités de Calidad por región';
	
	protected $indicadores = array(
		'inst_region',
		'gestion',
		'inst_subtipo',
	);
	
}
