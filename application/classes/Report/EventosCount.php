<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_EventosCount extends Report {
	
	public function execute()
	{
		$this->params['_statuses'] = implode(',', Model_AcreditadoEvento::$closed);
		
		$this->title = Inflector::plural(Model_ObjetoAcreditacion::$tipos[$this->params['_tiin_id']]);
		
		return parent::execute();
		
	}
	
}
