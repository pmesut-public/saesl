<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_SummaryComites extends Report {
	
	protected $title = 'Resumen Comités de Calidad';
	
	protected function set_data()
	{
		$this->data = Report::factory('comites_region')
			->execute()
			->get_result();
	}
	
	public function get_context()
	{
		return array_merge(parent::get_context(), array(
			'regiones' => Report::factory('regiones_participantes')
				->get_data()
				->get('count'),
		));
	}
	
}
