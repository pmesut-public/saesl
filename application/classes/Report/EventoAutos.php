<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_EventoAutos extends Report {
	
	public $title;
	
	public $header = array(
		array(
			'COMITÉ DE CALIDAD' => array(
				'colspan' => 2,
				'style' => 'width: 58%;',
			),
			'RESULTADO EVALUACIÓN ESTÁNDARES' => array(
				'colspan' => 4,
				'style' => 'width: 24%;',
			),
			'RESPONSABLES DE BRINDAR ASISTENCIA' => array(
				'colspan' => 3,
				'style' => 'width: 18%;',
			),
		),
		array(
			'Tipo de licenciamiento',
			'Institución (Carrera)',
			//'Carrera',
			'Total',
			'Evaluados',
			'Cumplidos',
			'%',
			'Sunedu',
			'Sineace <=40%',
			'ProCalidad >40%',
		),
	);
	
	public function get_context()
	{
		$this->title = ORM::factory('Evento', $this->params['_even_id'])
			->even_nombre;
		
		return parent::get_context();
	}
	
}
