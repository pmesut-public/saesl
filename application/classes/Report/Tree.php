<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Tree extends Report {
	
	protected $indicadores = array();
	
	protected function set_result()
	{
		$this->result = TreeArray::factory($this->data, $this->indicadores)->get();
	}
	
}
