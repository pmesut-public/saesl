<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Odp1 extends Report_Tree {
	
	protected $title = 'Número de carreras en las disciplinas clave e IES que hayan completado la autoevaluación bajo los estándares de CONEAU o CONEACES';
	
	protected $indicadores = array(
		'inst_region',
		'acpi_nombre',
		'gestion',
		'trim',
	);
	
}
