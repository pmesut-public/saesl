<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_AutoCompletas extends Report {
	
	protected $title = 'Autoevaluaciones completas (100% estándares)';
	
	protected function set_data()
	{
		$this->data = Report::factory($this->name)
			->get_data()
			->as_array('inst_subtipo');
	}
	
}
