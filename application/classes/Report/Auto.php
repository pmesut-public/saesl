<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_Auto extends Report_Tree {
	
	/**
	 *
	 * @var  Model_Autoevaluacion
	 */
	protected $oAutoevaluacion;
	
	protected $folder = 'auto/';
	
	protected $report;
	
	public static function factory($name, $params = NULL)
	{		
		$class = 'Report_Auto_'.ucfirst($name);
		
		return new $class($name, $params);
	}
	
	public function __construct($name, $params = NULL)
	{
		$this->oAutoevaluacion = $params;
		$this->report = $name;
		
		$params = array('_auto_id' => $this->oAutoevaluacion->auto_id);
		
		parent::__construct("{$name}", $params);
	}
	
	public function get_context()
	{
		$this->title = Arr::path($this->oAutoevaluacion->get_reportes(), "{$this->report}.title");
		
		return parent::get_context() + array(
			'response' => $this->oAutoevaluacion->result(),
			'oAutoevaluacion' => $this->oAutoevaluacion,
			'subtitle' => $this->oAutoevaluacion->oAcreditadoEvento->oAcreditado->subtitle(),
			'objeto' => $this->oAutoevaluacion->oAcreditadoEvento->oAcreditado->oObjetoAcreditacion->objeto_nombre(),
		);
	}
	
	protected function set_result()
	{
		//debug($this->data); die;
		
		$this->result = TreePowered::factory($this->data, $this->indicadores)->get();
	}
	
}
