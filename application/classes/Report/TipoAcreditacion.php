<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Report_TipoAcreditacion extends Report {
	
	protected $title = 'Comités de calidad | :inst';
	
	public function execute()
	{
		parent::execute();
		
		$this->title = strtr($this->title, array(
			':inst' => Inflector::plural(Model_ObjetoAcreditacion::$tipos[$this->params['_tiin_id']]),
		));
	}
	
	public function get_chart()
	{
		$this->execute();
		
		//debug($this->data);
		//return var_dump($this->data);
		
		return Chart::factory($this)->get();
	}
	
}
