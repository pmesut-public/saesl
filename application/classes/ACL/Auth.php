<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class ACL_Auth extends Kohana_ACL_Auth {
	
	protected $_role;
	
	public function __construct()
	{
		parent::__construct();
		
		if ($this->_user->id == 1)
		{
			$this->_role = 'super';
		}
	}
	
	// @temp
	public function set_role($role)
	{
		$this->_role = $role;
	}
	
}
