<?php defined("SYSPATH") OR die("No direct script access.");
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Chart_ComitesRegionSumm extends Chart {
	
	protected $buttons = array(
		'pdf', 'png', 'excel',
	);
	
	protected $properties = 
		'{
			"chart": {
				"type": "column"
			},
			"title": {
				"text": "Stacked column chart"
			},
			"xAxis": {
				"categories": ["Apples", "Oranges", "Pears", "Grapes", "Bananas"]
			},
			"yAxis": {
				"min": 0,
				"title": {
					"text": "Total fruit consumption"
				},
				"stackLabels": {
					"enabled": true,
					"style": {
						"fontWeight": "bold",
						"color": "gray"
					}
				}
			},
			"credits": {
				"text": "SAES - ProCalidad powered by Highcharts"
			},
			"legend": {
				"align": "right",
				"x": -30,
				"verticalAlign": "top",
				"y": 25,
				"floating": true,
				"backgroundColor": "white",
				"borderColor": "#CCC",
				"borderWidth": 1,
				"shadow": false
			},
			"tooltip": {
				"formatter": ""
			},
			"navigation": {
				"buttonOptions": {
					"enabled": false
				}
			},
			"plotOptions": {
				"column": {
					"stacking": "normal",
					"dataLabels": {
						"enabled": true,
						"color": "white",
						"style": {
							"textShadow": "0 0 3px black"
						}
					}
				}
			},
			"series": [{
				"name": "John",
				"data": [5, 3, 4, 7, 2]
			}, {
				"name": "Jane",
				"data": [2, 2, 3, 2, 1]
			}, {
				"name": "Joe",
				"data": [3, 4, 4, 2, 5]
			}]
		}';
	
	protected function set_json()
	{
		$this->data = array_slice($this->report->get_result(), 0, -1);
		
		$properties = json_decode($this->properties, TRUE);
		
		$properties['tooltip']['formatter'] = 
			'function () {
				return "<b>" + this.x + "</b><br/>" +
					this.series.name + ": " + this.y + "<br/>" +
					"Total: " + this.point.stackTotal;
			}';
		
		
		$properties['title']['text'] = $this->report->get_title();
		$properties['series'] = $this->get_data();
		
		$properties['xAxis']['categories'] = array_keys($this->data);
		$properties['yAxis']['title']['text'] = 'Comités de calidad';
		//debug($properties);
		
		//echo "<pre>".json_encode($properties)."</pre>"; die();*/
		
		$this->json = json_encode($properties);
		
	}
	
}
