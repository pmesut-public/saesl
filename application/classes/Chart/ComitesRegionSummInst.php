<?php defined("SYSPATH") OR die("No direct script access.");
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Chart_ComitesRegionSummInst extends Chart_ComitesRegionSumm {
	
	protected function get_data()
	{
		$publica = array();
		$privada = array();
		
		foreach ($this->data as $region => $row)
		{
			$pub = (int) Arr::path($row, 'Pública.TOTAL_I.count');
			$pri = (int) Arr::path($row, 'Privada.TOTAL_I.count');
			
			if ($pub OR $pri)
			{
				$publica[] = $pub;
				$privada[] = $pri;
			}
			else
			{
				unset($this->data[$region]);
			}
		}
		//debug2($publica);
		
		return array(
			array('name' => 'Pública', 'data' => $publica),
			array('name' => 'Privada', 'data' => $privada),
		);
	}
	
}
