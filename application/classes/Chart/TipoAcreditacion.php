<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Chart_TipoAcreditacion extends Chart {
	
	protected $properties = 
		'{
			"chart":{
				"_plotBackgroundColor":null,
				"_plotBorderWidth":null,
				"_plotShadow":false,
				"_width":500,
				"type":"pie",
				"options3d": {
					"enabled": true,
					"alpha": 45,
					"beta": 0
				}
			},
			"credits":{
				"text": "SAES licenciamiento - ProCalidad"
			},
			"title":{
				"text":"Browser market shares at a specific website, 2014"
			},
			"legend": {
				"align": "right",
				"verticalAlign": "top",
				"layout": "vertical",
				"_floating": true,
				"_backgroundColor": "rgba(255,255,255,0.5)",
				"x": 0,
				"y": 40,
				"labelFormat": "{name} ({y})",
				"itemStyle":{
					"fontWeight":"normal",
					"fontSize":"10px"
				}
			},
			"navigation": {
				"buttonOptions": {
					"enabled": false
				}
			},
			"tooltip":{
				"pointFormat":"{series.name}: <b>{point.percentage:.1f}%</b>"
			},
			"plotOptions":{
				"pie":{
					"allowPointSelect":true,
					"cursor":"pointer",
					"dataLabels":{
						"enabled":false
					},
					"showInLegend":true,
					"depth":35,
					"innerSize":"50%",
					"_size": "140%"
				}
			},
			"series":[{
				"type":"pie",
				"name":"Porcentaje",
				"data":[
					["Firefox",45],
					["IE",26.8],
					{
						"name":"Chrome",
						"y":12.8,
						"sliced":true,
						"selected":true
					},
					["Safari",8.5],
					["Opera",6.2],
					["Others",0.7]
				]
			}]
		}';
	
	protected function set_json()
	{
		//echo '<html>';
		$properties = json_decode($this->properties, TRUE);
		//debug2($properties);
		
		$properties['title']['text'] = $this->report->get_title();
		$properties['series'][0]['data'] = $this->get_data();
		
		//debug2($properties);
		
		//echo '<pre>'.json_encode($properties).'</pre>'; die();
		
		$this->json = json_encode($properties);
		
	}
	
	private function get_data()
	{
		$data = $this->report->get_result();
		
		array_pop($data);
		//debug($data);
		
		return array_map(function ($x) {
			$x['y'] = (int)$x['y'];
			return $x;
		}, $data);
	}
	
}
