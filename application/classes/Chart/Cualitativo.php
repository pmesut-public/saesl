<?php defined("SYSPATH") OR die("No direct script access.");
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Chart_Cualitativo extends Chart {
	
	protected $buttons = array(
		'pdf', 'png',
	);
	
	protected $properties = 
		'{
			"chart": {
				"type": "column",
				"_animation": false,
				"_margin": [100, 75, 75, 75],
				"_spacingTop": 0,
				"options3d": {
					"enabled": true,
					"alpha": 15,
					"beta": 15,
					"depth": 110,
					"frame": {
						"back": {
							"size": 10
						},
						"bottom": {
							"size": 10
						},
						"side": {
							"size": 10
						}
					}
				}
			},
			"plotOptions": {
				"column": {
					"depth": 80,
					"grouping": false,
					"groupZPadding": 80,
					"dataLabels": {
						"align": "center",
						"verticalAlign": "middle",
						"defer": false
					}
				},
				"series": {
					"_animation": false
				}
			},
			"title": {
				"text": "Stacked column chart"
			},
			"xAxis": {
				"categories": ["Apples", "Oranges", "Pears", "Grapes", "Bananas"]
			},
			"yAxis": {
				"min": 0,
				"title": {
					"text": "Total fruit consumption"
				},
				"allowDecimals": false
			},
			"credits": {
				"text": "SAES licenciamiento - ProCalidad"
			},
			"tooltip": {
				"formatter": ""
			},
			"navigation": {
				"buttonOptions": {
					"enabled": false
				}
			},
			"series": [{
				"name": "John",
				"data": [5, 3, 4, 7, 2]
			}, {
				"name": "Jane",
				"data": [2, 2, 3, 2, 1]
			}, {
				"name": "Joe",
				"data": [3, 4, 4, 2, 5]
			}]
		}';
	
	protected function set_json()
	{
		$result = $this->report->get_result();
		//debug2($result);
		
		$dimensiones = array_values(Model_ConceptoDetalle::$options);
		//debug2($dimensiones);
		
		$data = array_map(function ($x, $i) { return array(
			'name' => $x['name'],
			'data' => array_map('intval', explode(',', $x['data'])),
			'zIndex' => 10 - $i,
		); }, $result, array_keys($result));
		//debug($data);
		
		$properties = json_decode($this->properties, TRUE);
		//debug($properties);
		
		$properties['title']['text'] = $this->report->get_title();
		$properties['series'] = $data;
		
		$properties['xAxis']['categories'] = $dimensiones;
		$properties['yAxis']['title']['text'] = 'Estándares';
		//debug($properties);
		
		//echo "<pre>".json_encode($properties)."</pre>"; die();
		
		$this->json = json_encode($properties);
		
	}
	
}
