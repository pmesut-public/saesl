<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Api extends Controller {

	private $token = 'F5cYRLZF';

	const INVLD_TOKEN = 1;
	const LOGIN_ERROR = 2;
	const NO_AUTOEVAL = 3;
	const ERR_PUNTAJE = 4;

	private $error_codes = array(
		self::INVLD_TOKEN => 'Invalid token',
		self::LOGIN_ERROR => 'Usuario o contraseña incorrectos',
		self::NO_AUTOEVAL => 'No registra autoevaluaciones cerradas en una convocatoria',
		self::ERR_PUNTAJE => 'Usted no alcanzó el puntaje mínimo en su autoevaluación',
		5 => '',
	);

	public function before()
	{
		parent::before();

		// check token
		if ($this->request->param('id') != $this->token)
		{
			$this->error(self::INVLD_TOKEN);
		}
	}

	public function action_login()
	{
		extract($this->request->post());

		$oAcreditado = ORM::factory('Acreditado')
			->add_titles()
			->with('oUser')
			->with('oObjetoAcreditacion:oTipoInstitucion')
			->with('oModalidad')
			->select(DB::expr("concat(tiin_nombre, ' - ', obac_nombre) obac"))
			->select(DB::expr('username, email, i.*, c.carr_nombre'))
			->select(DB::expr('oModalidad.*'))
			->select('tiin_nombre')
			->where('username', '=', $username)
			->where('password', '=', Auth::instance()->hash($password))
			->where('oUser.status', '=', 1)
			->where('acre_aprobado', '=', 1)
			->find(FALSE)
		;
		
		if ( ! $oAcreditado->loaded())
		{
			$this->error(self::LOGIN_ERROR);
		}
		
		$oAutoevaluacion = $this->get_autoevaluacion($oAcreditado->acre_id);
		
		if ( ! $oAutoevaluacion->loaded())
		{
			$this->error(self::NO_AUTOEVAL);
		}
		
		if ($oAutoevaluacion->porcentaje < $oAutoevaluacion->even_per_minimo)
		{
			$this->error(self::ERR_PUNTAJE);
		}
		
		$response = array_merge(array('status' => TRUE), $oAcreditado->as_array(), $oAutoevaluacion->as_array());

		unset($response['oUser']);
		//debug($response);

		$this->response->body(json_encode($response));
	}
	
	public function action_getFactores()
	{
		$aTipoInstitucion = [Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD, Model_ObjetoAcreditacion::TIPO_INSTITUTO];
		
		$aFactores = A::f($aTipoInstitucion)
			->map(function($tiin_id) {
				return DB::select(DB::expr("*
					from objeto_acreditacion
					join dimension using(obac_id)
					join factor using(dime_id)
					where tiin_id = {$tiin_id}
					group by fact_titulo"))
					->execute()
					->as_array();
			})
			->value();
			
		$this->response->body(json_encode($aFactores));
	}
	
	/*private function file_contents_exist($url, $response_code = 200)
	{
		$headers = get_headers($url);

		if (substr($headers[0], 9, 3) == $response_code)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}*/
	
	/**
	 * Congela los datos de la cuarta convocatoria (even_id 10 y 11), que tengan estado "cerrado"
	 * Se creó una carpeta freeza, y dentro una carpeta con el id de cada autoevaluación, dentro de ésta se encuentrna los archivos
	 */
	private function check_freeze()
	{
		$base_dir = APPPATH.'freeze/';
		$report = @file_get_contents($base_dir.$this->request->query('auto_id').'/'.$this->request->action().'.txt');
		
		if ($report)
		{
			$this->response->body($report);
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	/**
	 * Retorna los resultados de cada autoevaluación, a partir de auto_id
	 */
	public function action_getAutoevaluacion()
	{
		$auto_id = $this->request->query('auto_id');
		
		if ($this->check_freeze($auto_id))
		{
			return;
		}

		$q = DB::select(DB::expr("moda_id, tiin_id
				from autoevaluacion
				join acreditado_evento using(acev_id)
				join acreditado using(acre_id)
				join objeto_acreditacion o using(obac_id)
				where auto_id = {$auto_id}"))
			->execute();

		$moda_id = $q->get('moda_id');
		$tiin_id = $q->get('tiin_id');

		$script = "gproc_planificacion_{$tiin_id}_{$moda_id}";
		//debug($script);

		$report = Report::factory($script, ['_auto_id' => $auto_id])
			->execute()
			->get_result();
		
		$this->response->body(json_encode($report));
	}

	/**
	 * Retorna los datos de la Expresión de Interés
	 */
	public function action_getExpIntData()
	{
		$oAcreditadoEvento = ORM::factory('AcreditadoEvento', $this->request->query('acev_id'));

		if ( ! $oAcreditadoEvento->loaded())
		{
			$this->error('Model not found');
		}

		$oAcreditado = $oAcreditadoEvento->oAcreditado;

		//$title = $oAcreditadoEvento->oEvento->even_nombre;
		//$oFicha = $this->oFicha;
		//$ficha = $this->name;

		$aTipoFinanciamiento = $oAcreditadoEvento->aTipoFinanciamientoDisponible();

		$institucional = //TRUE;
			($oAcreditado->oObjetoAcreditacion->obac_tipo_acreditacion ==
			Model_ObjetoAcreditacion::ACREDITACION_INSTITUCION);
		//debug($institucional);

		$universidad = //FALSE;
			($oAcreditado->oObjetoAcreditacion->tiin_id ==
			Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD);

		$aPersona = DB::select()
			->from('persona')
			->where('acre_id', '=', $oAcreditado->acre_id)
			->where('pers_tipo', 'in', array_keys(Model_Persona::$tipos))
			->as_assoc()
			->execute()
			->as_array();

		//return get_defined_vars();
		unset($oAcreditadoEvento, $oAcreditado);
		$this->response->body(json_encode(get_defined_vars()));
	}

	private function error($code, $msg = NULL)
	{
		if ($msg === NULL)
		{
			$msg = Arr::get($this->error_codes, $code, 'Error desconocido');
		}

		die(json_encode([
		  'status' => FALSE,
		  'message' => $msg,
		  'code' => $code,
		  ]));
	}
	
	/**
	 * Se obtienen los medios de verificación, que son los mismos para todas las autoevaluaciones
	 */
	public function action_getMedios()
	{
		$auto_id = $this->request->query('auto_id');
		
		if ($this->check_freeze($auto_id))
		{
			return;
		}
		
		$report = Report::factory('gproc_monitoreo', array('_auto_id' => $auto_id))
			->execute()
			->get_result();
		//debug($report);

		$this->response->body(json_encode($report));
		return;
	}
	
	/**
	 * Se obtienen los comites de evaluación, no es necesario congelar
	 */
	public function action_getComites()
	{
		$users = Report::factory('gproc_users', array('_even_id' => $this->request->query('even_id')))
			->execute()
			->get_result();

		$evento = ORM::factory('Evento')
			->select('tiin_nombre')
			->join('tipo_institucion')->using('tiin_id')
			//->with('oTipoInstitucion')
			->where('even_id', '=', $this->request->query('even_id'))
			->find()
			->as_array();
		//->even_nombre;
		//debug($users);

		$this->response->body(json_encode(get_defined_vars()));
	}
	
	/**
	 * Se utiliza en el controller gproc, para instanciar en session "objetos", contiene los datos de usuario
	 */
	public function action_getDataUser()
	{
		/*$auto_id = $this->request->query('auto_id');
		
		if ($this->check_freeze($auto_id))
		{
			return;
		}*/
		
		$acev_id = $this->request->query('acev_id');

		$oAcreditado = ORM::factory('Acreditado')
			->add_titles()
			//->with('oUser')
			->with('oObjetoAcreditacion:oTipoInstitucion')
			->with('oModalidad')
			->select(DB::expr("concat(tiin_nombre, ' - ', obac_nombre) obac"))
			->select(DB::expr('username, email, i.*, c.carr_nombre'))
			->select(DB::expr('oModalidad.*'))
			->select('tiin_nombre')
			->join('acreditado_evento')->using('acre_id')
			->where('acev_id', '=', $acev_id)
			->find(FALSE)
		;

		$oAutoevaluacion = $this->get_autoevaluacion($oAcreditado->acre_id, $acev_id);

		$response = array_merge($oAcreditado->as_array(), $oAutoevaluacion->as_array());

		unset($response['oUser']);
		//debug($response);

		$this->response->body(json_encode($response));
	}
	
	/**
	 * Contiene la lista de los eventos, que se muestran en la pantalla inicial de gproc.
	 */
	public function action_getEventos()
	{
		$aEvento = ORM::factory('Evento')
			->with('oTipoInstitucion')
			->select(DB::expr("concat(tiin_nombre, ' - ', even_nombre) even_name"))
			//->select(DB::expr("even_nombre even_name"))
			->order_by('tiin_id')
			->order_by('even_id')
			->find_all()
			->as_json_array();

		$this->response->body(json_encode($aEvento));
	}
     
	/**
	 * se utiliza en el login
	 */
	private function get_autoevaluacion($acre_id, $acev_id = NULL)
	{
		if ($acev_id)
		{
			$cond = DB::expr('ae.acev_id = '.$acev_id);
		}
		else
		{
			$cond = DB::expr('ae.acev_estado = '.Model_AcreditadoEvento::ESTADO_ACTUAL);
		}
		
		$_cumplimiento		= "(aude_cumplimiento = 1)";
		$total				= "count(esta_id)";
		$cumplidos			= "sum(if($_cumplimiento, 1, 0))";
		$per_cumplidos		= "format($cumplidos / $total * 100, 2)";
		
		$oAutoevaluacion = ORM::factory('Autoevaluacion')
			->add_reportes_stuff()
			->select(DB::expr("$per_cumplidos porcentaje"))
			->select(DB::expr('even_id, even_nombre, even_per_minimo'))
			->join(DB::expr('autoevaluacion_detalle ad'))->using('auto_id')
			->join(DB::expr('evento'))->using('even_id')
			//->where('ae.acev_estado', '=', Model_AcreditadoEvento::ESTADO_ACTUAL)
			->where(DB::expr(''), '', $cond)
			->where('auto_estado', '=', Model_Autoevaluacion::STATUS_CERRADO)
			//->where('auto_estado', '=', Model_Autoevaluacion::STATUS_ACTIVO)
			->join(
				DB::expr("(select max(auto_id) max_auto_id 
				from autoevaluacion 
				join acreditado_evento ae using(acev_id)
				where acre_id = {$acre_id}
				and {$cond}
				and auto_estado = 'cerrado'
				group by acev_id) as max"))
			->on(DB::expr(''), '', DB::expr('ad.auto_id = max.max_auto_id'))
			->where('acre_id', '=', $acre_id)
			//->order_by('auto_id', 'desc')
			->find();
		//debug($oAutoevaluacion);
		$base_dir = APPPATH.'freeze/';
		//$base_url = 'http://'.Kohana::$server_name.'/';
		$filename = $base_dir.'/porcentajes.txt';
		
		if (file_exists($filename)) // Si existe en el freeze (auto_id), reemplazar por el porcentaje del archivo freezeado
		{
			$porcentajes = json_decode(file_get_contents($filename), TRUE);
			
			if ($porcentaje = Arr::get($porcentajes, $oAutoevaluacion->auto_id))
			{
				$oAutoevaluacion->porcentaje = $porcentaje;
			}
		}
		
		return $oAutoevaluacion;
	}
	
}
