<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Frontend_Dashboard extends Controller_Frontend {
	
	public function action_index()
	{
		$view = Theme_View::factory('frontend/dashboard');
		
		Theme::instance()
			->template
			->content = $view;
	}
	
}
