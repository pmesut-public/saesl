<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Frontend_AutoevaluacionInforme extends Controller_Frontend {
	
	/**
	 *
	 * @var Model_Autoevaluacion
	 */
	private $oAutoevaluacion;
	
	/**
	 *
	 * @var Model_AutoevaluacionInforme;
	 */
	private $oAutoInforme;

	//private $tipo;
	
	public function before()
	{
		parent::before();
		
		$this->check_autoevaluacion();
		
		$this->check_informe();
		
		$this->set_template_data();
	}
	
	public function action_index()
	{
		if ($this->oAutoInforme->auin_estado == Model_AutoevaluacionInforme::UPLOADING)
		{
			$this->redirect('/autoevaluacionInforme/upload');
		}
		
		$report = Report_Auto::factory('report7', $this->oAutoevaluacion)
			->get();
		
		//debug($report);
		$view = Theme_View::factory('frontend/autoevaluacioninforme/index', ['report' => $report]);
		
		if ($this->request->method() == 'POST')
		{
			$this->oAutoInforme
				->set('auin_estado', Model_AutoevaluacionInforme::UPLOADING)
				->save();
			
			$this->redirect('/autoevaluacionInforme/upload');
		}
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_upload()
	{
		if ($this->oAutoInforme->auin_estado == Model_AutoevaluacionInforme::PENDIENTE)
		{
			$this->redirect('/autoevaluacionInforme');
		}
		
		$report = Report_Auto::factory('report7', $this->oAutoevaluacion)
			->get();
		
		//debug($report);
		$view = Theme_View::factory('frontend/autoevaluacioninforme/upload', ['report' => $report]);
		
		if ($this->request->method() == 'POST')
		{
			$filename = $this->oAutoInforme
				->upload_file($_FILES['file']);
			
			//debug($this->oAutoevaluacion->oAutoInforme);
			
			if ($filename)
			{
				$this->oAutoInforme
					->set('auin_path', $filename)
					->set('auin_estado', Model_AutoevaluacionInforme::FINALIZADO)
					->save();
				
				$this->oAutoevaluacion->finish();
				
				Session::instance()->set('info', 'Ficha guardada');
				
				$this->redirect('/autoevaluaciones');
			}

			Session::instance()->set('error', 'No se pudo guardar el documento. Inténtelo nuevamente');
		}
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_pdf()
	{
		$report = Report_Auto::factory('report7', $this->oAutoevaluacion)
			->get_pdf();
		
		Response::factory()
			->body($report)
			->send_file(TRUE, "report7.pdf", array('inline' => (bool) $this->request->query('view')));
	}
	
	public function action_edit()
	{
		$this->oAutoInforme
			->set('auin_estado', Model_AutoevaluacionInforme::PENDIENTE)
			->save();
		
		$this->redirect('/autoevaluacionInforme');
	}
	
	// @ajax
	public function action_saveFactorData()
	{
		//debug($this->request->post());
		extract($this->request->post());
		
		foreach ($informe_factores as $codigo => $factor)
		{
			$oAutoInformeFactor = $this->oAutoevaluacion
				->oAutoInforme
				->aAutoInformeFactor
				->where('fact_codigo', '=', $codigo)
				->find();

			$oAutoInformeFactor
				->values($factor)
				->set('auto_id', $this->oAutoevaluacion->auto_id)
				->save();
		}
		
		die('ok');
	}
	
	// @ajax
	public function action_saveInformeData()
	{
		//debug($this->request->post());
		extract($this->request->post());
		
		$this->oAutoevaluacion
			->oAutoInforme
			->set('auin_descripcion', json_encode($informe['descripcion']))
			->save();
		
		die('ok');
	}
	
	private function check_autoevaluacion()
	{
		$this->oAutoevaluacion = $oAutoevaluacion = $this->oAcreditado
			->oAutoevaluacionActiva()
			->reset(FALSE)
			->add_reportes_stuff()
			->reload();
		//debug($oAutoevaluacion);
		
		if ( ! $oAutoevaluacion->loaded())
		{
			$this->redirect('/eventos');
		}
		//$this->tipo = strtolower($this->oAcreditado->tipo_institucion());
	}
	
	private function check_informe()
	{
		$this->oAutoInforme = $this->oAutoevaluacion->oAutoInforme;
		
		if ( ! $this->oAutoInforme->is_editing())
		{
			$this->redirect('/autoevaluacion/calculator');
		}
	}
	
	private function set_template_data()
	{
		$theme = Theme::instance();
		
		//$theme->css('page', 'autoevaluacioninforme', '/saes_lte/css/autoevaluacioninforme.css');
		$theme->js('page', 'autoevaluacioninforme', '/saes_lte/js/autoevaluacioninforme.js');
	}
	
}
