<?php

defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Frontend_Eventos extends Controller_Frontend {
	/*
	 * Vista Usuario Regular
	 * Lista las autoevaluaciones actuales (cerradas [3], anteriores [4])
	 * return Obj Theme
	 */

	public function action_index()
	{
		$aAcreditadoEvento = $this->oAcreditado
			->get_eventos_cerrados();

		$view = Theme_View::factory('frontend/eventos/list', compact('aAcreditadoEvento'));

		Theme::instance()
			->template
			->content = $view;
	}

	public function action_objeto()
	{
        $oAutoevaluacion = $this->oAcreditado->oAutoevaluacionActiva();

        if ($oAutoevaluacion->loaded())
        {
            $this->redirect('/autoevaluaciones');
        }

        $oAcreditado = $this->oAcreditado;

        /*if(!$oAcreditado->matriz_completa()){
            Session::instance()->set('error', 'La matriz del objeto de acreditación está incompleta');
            $this->redirect('/autoevaluaciones');
        }*/

        $oDimensiones = $oAcreditado->get_dimensiones();
        $result['dimensiones'] = [];
        foreach ($oDimensiones as $dm)
        {
                $result['dimensiones'][] = ['display' => $dm->dime_titulo, 'id' => $dm->dime_id];
        }

        $oFactores = $oAcreditado->get_factores();
        $result['factores'] = [];
        foreach ($oFactores as $ft)
        {
                $result['factores'][] = ['display' => $ft->fact_titulo, 'id' => $ft->fact_id, 'dime_id' => $ft->dime_id];
        }

        $oEstandares = $oAcreditado->get_estandares();
        $result['estandares'] = [];
        foreach ($oEstandares as $st)
        {
            $result['estandares'][] = [
                'display' => $st->esta_titulo,
                'id' => $st->esta_id,
                'fact_id' => $st->fact_id
            ];
        }

        $result['eventos'] = $oAcreditado->get_eventos();

        $result['autoevaluaciones'] = $this->_get_autoevaluaciones();

        $result['ponderacion'] = array(
            'cerrada' => array(),
            'borrador' => array()
        );

        $view = Theme_View::factory('frontend/ponderaciones/index')
            ->set('result', json_encode($result));

        $url = REACT_MEDIA . 'ponderacion.js';
        Theme::instance()
            ->extra('<script src="' . $url . '"></script>')
            ->template
            ->content = $view;

		/*$oAutoevaluacion = $this->oAcreditado->oAutoevaluacionActiva();

		if ($oAutoevaluacion->loaded())
		{
			$this->redirect('/autoevaluaciones');
		}
		
		$this->redirect('/formatos');*/
	}

    private function _get_autoevaluaciones()
    {
        $aAutoevaluacion = DB::select('*')
            ->from('autoevaluacion')
            ->join('acreditado_evento')->using('acev_id')
            ->where('acre_id', '=', $this->oAcreditado->acre_id)
            ->and_where('auto_estado', '=', Model_Autoevaluacion::STATUS_CERRADO)
            ->as_object()
            ->execute();

        $autoevaluaciones = [];

        foreach ($aAutoevaluacion as $oAutoevaluacion) {
            $autoevaluaciones[] = [
                'id' => $oAutoevaluacion->auto_id,
                'display' => '',
                'data' => [],
            ];
        }

        return $autoevaluaciones;
    }

	public function action_view()
	{
		$view = $this->get_ficha()
			->get('view');

		Theme::instance()
			->template
			->content = $view;
	}

	/* public function action_pdf()
	  {
	  $view = $this->get_ficha()
	  ->get_pdf();

	  Response::factory()
	  ->body($view)
	  ->send_file(TRUE, "{$this->oFicha->oEventoFicha->evfi_nombre}.pdf", array('inline' => (bool) $this->request->query('view')));
	  } */
}
