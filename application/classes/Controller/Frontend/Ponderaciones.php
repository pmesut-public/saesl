<?php

defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 Inspired Solutions
 */
class Controller_Frontend_Ponderaciones extends Controller_Frontend {

	public function action_index()
	{
//		$objeto = $this->request->param('id') ?: '1';
		$objeto = 2;

		$oAcreditado = $this->oAcreditado;

		$result = [];

		$result['type'] = $objeto;

		$result['eventos'] = $this->_get_eventos_disponibles($objeto);
//		$result['eventos'] = $this->_get_eventos_disponibles();
		
//		debug($result);

		$oDimensiones = $oAcreditado->get_dimensiones();
		foreach ($oDimensiones as $dm)
		{
			$result['dimensiones'][] = ['display' => $dm->dime_titulo, 'id' => $dm->dime_id];
		}

		$oFactores = $oAcreditado->get_factores();
		foreach ($oFactores as $ft)
		{
			$result['factores'][] = ['display' => $ft->fact_titulo, 'id' => $ft->fact_id, 'dime_id' => $ft->dime_id];
		}

		$oEstandares = $oAcreditado->get_estandares();
		foreach ($oEstandares as $st)
		{
			$result['estandares'][] = [
				'display' => $st->esta_titulo,
				'id' => $st->esta_id,
				'fact_id' => $st->fact_id
			];
		}

		$result['autoevaluaciones'] = $this->_get_autoevaluaciones($objeto);

		$result['ponderaciones'] = $this->_get_ponderaciones();

		$view = Theme_View::factory('frontend/ponderaciones/index')
			->set('result', json_encode($result))
			->set('objeto', $objeto);

		$url = REACT_MEDIA . 'ponderacion.js';
		Theme::instance()
				->extra('<script src="' . $url . '"></script>')
			->template
			->content = $view;
	}

	public function action_save()
	{
		$this->auto_render = FALSE; // Deshabilita theme
		$status = 'OK';
		$msg = '';

		$acre_id = $this->oAcreditado->acre_id;

		$post = json_decode($this->request->body(), TRUE);

		$oPonderacion = $this->oAcreditado->get_ponderacion_borrador();
		$oPonderacion->set('acre_id', $acre_id)
			->set('pond_estado', Model_Ponderacion::STATUS_ACTIVO)
			->set('pond_tipo', Model_Ponderacion::TIPO_BORRADOR)
			->set('pond_data', json_encode($post));

		if (!$oPonderacion->save())
		{
			$status = 'DENIED';
			$msg = 'No se puedo guardar la ponderación';
		}

		$result = ['status' => $status, 'msg' => $msg];

		$this->response->body(json_encode($result));
	}

	public function action_close()
	{
		$this->auto_render = FALSE; // Deshabilita theme
		$status = 'OK';
		$msg = '';

		/*
		$acre_id = $this->oAcreditado->acre_id;

		$oPonderacion_cerrada = $this->oAcreditado->get_ponderacion_cerrada();
		if ($oPonderacion_cerrada->pond_id != NULL)
		{
			$oPonderacion_cerrada->delete();
		}

		$post = json_decode($this->request->body(), TRUE);

		$oPonderacion = $this->oAcreditado->get_ponderacion_borrador();
		$oPonderacion->set('acre_id', $acre_id)
			->set('pond_estado', Model_Ponderacion::STATUS_ACTIVO)
			->set('pond_tipo', Model_Ponderacion::TIPO_CERRADA)
			->set('pond_data', json_encode($post));

		if (!$oPonderacion->save())
		{
			$status = 'DENIED';
			$msg = 'No se puedo cerrar la ponderación';
		}
		*/

		$result = ['status' => $status, 'msg' => $msg];

		$this->response->body(json_encode($result));
	}

	private function _get_eventos_disponibles($objeto)
	{
		$aConvocatorias = $this->oAcreditado->aConvocatorias_disponibles($objeto);

		$evento_convocatoria = [];
		foreach ($aConvocatorias as $ec)
		{
			$evento_convocatoria[] = [
				'display' => $ec->even_nombre,
				'id' => $ec->even_id,
				'editable' => $ec->even_ponderacion,
			];
		}

		$aSeguimientos = $this->oAcreditado->aSeguimientos_disponibles($objeto);

		$evento_seguimiento = [];
		foreach ($aSeguimientos as $es)
		{
			$evento_seguimiento[] = [
				'display' => $es->even_nombre,
				'id' => $es->even_id,
				'editable' => $es->even_ponderacion,
			];
		}

		return array_merge($evento_convocatoria, $evento_seguimiento);
	}

	private function _get_ponderaciones()
	{
		$valida = 0;
		$oPonderacion_cerrada = $this->oAcreditado->get_ponderacion_cerrada();
        $ponderacion['cerrada'] = [];
		if ($oPonderacion_cerrada->pond_data != NULL)
		{
			$ponderacion['cerrada'] = json_decode($oPonderacion_cerrada->pond_data);
			$valida = 1;
		}

		$oPonderacion_borrador = $this->oAcreditado->get_ponderacion_borrador();
        $ponderacion['borrador'] = [];
		if ($oPonderacion_borrador->pond_data != NULL)
		{
			$ponderacion['borrador'] = json_decode($oPonderacion_borrador->pond_data);
			$valida = 1;
		}

		if ($valida == 0)
		{
			$ponderacion = new stdClass();
		}

		return $ponderacion;
	}

	private function _get_autoevaluaciones($objeto = '')
	{
		$aAutoevaluacion = $this->oAcreditado->get_autoevaluaciones($objeto);

		$auto_detalles = $this->oAcreditado->get_autoevaluacion_detalle($objeto);

		$autoevaluaciones = [];

		foreach ($aAutoevaluacion as $oAutoevaluacion)
		{
			$estandares = [];
			foreach ($auto_detalles as $detalle)
			{
				if ($detalle->auto_id == $oAutoevaluacion->auto_id)
				{
					$estandares [] = [
						'esta_id' => $detalle->esta_id,
						'fact_id' => $detalle->fact_id,
						'ponderacion' => $detalle->ponderacion,
					];
				}
			}

			$autoevaluaciones[] = [
				'id' => $oAutoevaluacion->auto_id,
				'display' => $oAutoevaluacion->display,
				'data' => [
					'estandares' => $estandares,
				],
			];
		}

		return $autoevaluaciones;
	}

}
