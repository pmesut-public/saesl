<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Frontend_Profile extends Controller_Frontend {
	
	/**
	 * Perfil del usuario (comité)
	 * Muestra el detalle del comite (Acreditación,Institución,email,Nombre de Usuario, 
	 * fecha de registro,Documento de autorizacion del comite), y los miembros del comite
	 * Permite cambiar la contraseña(m. change_password), agregar nuevo comite(m. change) 
	 * 
	 * @return Obj Theme View
	 */
	public function action_index()
	{
		$query = DB::select('*')
			->select(DB::expr("if(pers_tipo = 'contacto', 'Sí', 'No') pers_contacto"))
			->from('persona')
			->where('acre_id', '=', $this->oAcreditado->acre_id)
			->where('pers_tipo', 'in', array_keys(Model_Persona::$tipos));
		
		$table = Table::factory('Persona')
			->query($query)
			->columns(array('pers_nombres', 'pers_apellidos', 'pers_correo', 'pers_telefono', 'pers_contacto'))
			->labels('pers_contacto', 'Contacto')
			->build();

		$view = Theme_View::factory('frontend/profile/index')
			->set('oAcreditado', $this->oAcreditado)
			->set('table', $table)
			->set('change', $this->request->query('change'))
			->set('change_password', $this->request->query('change_password'));
		
		if ($this->request->method() == 'POST')
		{
			if ($this->request->query('change_password'))
			{
				$oUser = $this->oAcreditado->oUser;
				
				if (ACL::instance()->allowed('acreditado', 'change_password') OR 
					($oUser->password == Auth::instance()->hash($this->request->post('old_password'))))
				{
					try
					{
						$oUser->update_user($this->request->post(), array('password'));
						
						Session::instance()->set('info', 'Se cambió su contraseña correctamente');
					}
					catch (ORM_Validation_Exception $e)
					{
						//$error = $e->errors('');
						//$error = is_array(current($error)) ? current(current($error)) : current($error);
						$error = $e->message();
					}
				}
				else
				{
					$error = 'Contraseña antigua incorrecta.';
				}
				
				Session::instance()->bind('message', $error);
			}
			
			elseif ($this->request->query('change'))
			{
				//debug($this->request->post());
				$oAcreditado = $this->oAcreditado;
				
				$requires_approval = //$oAcreditado->oUser->has('roles', Model_Role::ROLE_REGULAR) ? TRUE : FALSE;
					TRUE;
				
				try
				{
					$oAcreditado->save_new_comision($this->request->post(), $requires_approval);
					
					$info = 'Se registró el nuevo comité correctamente.';
					
					//if ($requires_approval)
					{
						$info .= ' Un administrador aprobará la solicitud.';
						
						mail(Kohana::$config->load('general.admin_mail'),
							'Procalidad - Nuevo comité registrado', 
							'El usuario '.$oAcreditado->oUser->username.' ha registrado un nuevo comité.');
					}
					
					Session::instance()->set('info', $info);
					
					$this->redirect('/profile');
				}
				catch (Exception_Saes $e)
				{
					$error = $e->getMessage();
				}
				
				Session::instance()->bind('error', $error);
			}
		}

		Theme::instance()
			->template
			->content = $view;
	}
	
}
