<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @package    SAES L
 * @author     ProCalidad TEAM
 * @copyright  (c) 2018 ProCalidad
 */

require_once Kohana::find_file('vendor', 'carbon/vendor/autoload');

use Carbon\Carbon;

class Controller_Frontend_Reportes extends Controller_Frontend {

	private $title = 'Reporte';

	private $oAutoevaluacion;

	private $report;

	public function before()
	{
		parent::before();

		$this->check_autoevaluacion();

		$this->set_template_data();
	}

	public function action_view()
	{

		/* Queries */

		$aReporte = QueryBuilder::factory('reportegeneral/reporte_general.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->get();

		$aDimensiones = QueryBuilder::factory('reportegeneral/reporte_dimension.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->dict('dime_id')->get();

		$aFactores = QueryBuilder::factory('reportegeneral/reporte_factor.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->dict('fact_id')->get();

		$aEstandares = QueryBuilder::factory('reportegeneral/reporte_estandar.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->dict('esta_id')->get();

		$aDetalle = QueryBuilder::factory('reportegeneral/reporte_detalle.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->dict('aude_id')->get();

		$aMedios = QueryBuilder::factory('reportegeneral/reporte_medio.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->dict('aume_id')->get();

		$aDocumentos = QueryBuilder::factory('reportegeneral/reporte_documentos.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->dict('docu_id')->get();

		/* Personalización de la data */

		$aDocumentos = A::f($aDocumentos)
            ->map(function ($documento) {

				$periodoDias = Carbon::parse($documento['fecha_fin'])->diffInDays(Carbon::parse($documento['fecha_inicio']));
				$edad        = Carbon::now()->diffInDays(Carbon::parse($documento['fecha_inicio']));
				$olgura      = (int) $periodoDias - (int) $edad;
				$vigencia    = Model_Documento::get_vigencia($olgura, $documento['da']);
				
				$documento['_docu_estado']   = $documento['estados'];
				$documento['_docu_calidad']  = $documento['sub_estado'];
				$documento['_docu_vigencia'] = $vigencia;
				return $documento;
            })
            ->to_dict('docu_id')
            ->value();

		$aReporteGrouped = array_group_by($aReporte, 'dime_id', 'fact_id', 'esta_id', 'aude_id', 'aume_id', 'audo_id');

		/* Render de la vista */

		$tipoInstitucion    = $this->oAcreditado->tipo_institucion();

		$objetoAcreditacion = $this->oAcreditado->oObjetoAcreditacion->obac_nombre;

		$institucion 		= $this->oAcreditado->oInstitucion()->oInstitucion->inst_nombre;

		$carrera 			= $this->oAcreditado->oCarrera()->oCarrera->carr_nombre;

		$subtitle 			= $institucion . ($carrera ? ' - '.$carrera : '');

		$data = [
			'aReporte'        => $aReporteGrouped,
			'aDimensiones'    => $aDimensiones,
			'aFactores'       => $aFactores,
			'aEstandares'     => $aEstandares,
			'aDetalle'        => $aDetalle,
			'aMedios'         => $aMedios,
			'aDocumentos'     => $aDocumentos,
			'oAutoevaluacion' => $this->oAutoevaluacion,
			'title'           => 'Reporte del Estado de las Condiciones Básicas de Calidad',
			'subtitle'        => $subtitle,
			'objeto'          => $tipoInstitucion . ' - '. $objetoAcreditacion,
		];

		$view     = Theme_View::factory('frontend/formatos/general/reporte_generalPDF', $data);

		$download = $this->request->query('download') ? $this->request->query('download') : '';

		$filename = 'reporte_general';

		switch ($download) {

			case "excel":

				$theme = Theme::instance();
        		$theme->css(array());
				$theme->template = Theme_View::factory('reportes/template');
        		$theme->template->content = $view;

				Response::factory()
							->headers(['Content-Type' => 'application/vnd.ms-excel'])
							->body($theme->template)
							->send_file(TRUE, "{$filename}.xls");	
			break;

			case "pdf":

				$view = PDF::factory()->set('view', $view)->get();

				Response::factory()
							->headers(['Content-Type' => 'application/pdf'])
							->body($view)
							->send_file(TRUE, "{$filename}.pdf");
			break;
			
			default:

				$buttonData = ['path' => '/reportes/view/reporte'];

				$data = array_merge($data, [
					'buttonData'    => $buttonData,
					'requestParams' => $this->request->query()
				]);

				$theme = Theme::instance();

				$theme->template->body_class = 'fixed';

				$view = Theme_View::factory('frontend/formatos/general/reporte_general', $data);

				$theme->template->content = $view;
			break;
		}

	}

	public function action_pdf()
	{
		$view = Report_Auto::factory($this->report, $this->oAutoevaluacion)->get_pdf();

		Response::factory()
			->body($view)
			->send_file(TRUE, "{$this->report}.pdf", array('inline' => (bool) $this->request->query('view')));
	}

	public function action_excel()
	{
		$theme = Theme::instance();

		$theme->css(array());

		$theme->template = Theme_View::factory('reportes/template');

		$theme->template->content = Report_Auto::factory($this->report, $this->oAutoevaluacion)->get_excel();

		Response::factory()
			->body($theme->template)
			->send_file(TRUE, "{$this->report}.xls", array('inline' => (bool) $this->request->query('view')));
	}

	private function set_template()
	{
		$theme = Theme::instance();

		$theme->template->body_class = 'fixed';

		$new_header = Theme_View::factory('frontend/reportes/header', array(
			'header' => $theme->template->header,
		));

		$theme->template->header                  = $new_header;
		$theme->template->header->oAutoevaluacion = $this->oAutoevaluacion;
		$theme->template->header->response        = $this->oAutoevaluacion->result();

		$theme->template->sidebar = Theme_View::factory('frontend/autoevaluacion/sidebar', $this->get_sidebar_data());
	}

	private function set_template_data()
	{
		$theme = Theme::instance();

		$theme->js('default', 'smooth', '/saes_lte/js/plugins/smooth-scroll.js');
		$theme->css('page', 'auto_report', '/saes_lte/css/auto_report.css');
		$theme->js('page', 'auto_report', '/saes_lte/js/auto_report.js');
	}

	private function get_sidebar_data()
	{
		$data = DB::select(DB::expr("*
				, concat(dime_codigo, '. ', dime_titulo) dime
				, concat(fact_codigo, '. ', fact_titulo) fact
				, concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta
				from dimension d
				join factor f using(dime_id)
				join estandar e using(fact_id)
				join autoevaluacion_detalle ad using(esta_id)"))
			->where('auto_id', '=', $this->oAutoevaluacion->auto_id)
			->execute()
			->as_array();

		$indicadores = array(
			'dime',
			'fact',
			'esta',
			'esta_codigo'
		);

		$aDimension = TreeArray::factory($data, $indicadores)->get();

		return get_defined_vars();
	}

	protected function _check_authorization()
	{
		$this->report = $this->request->param('id');

		if (!ACL::instance()->allowed('frontend.reportes', $this->report))
		{
			throw new HTTP_Exception_403('Thou shalt not pass');
		}
	}

	protected function get_acreditado_from_backend()
	{
		$this->oAutoevaluacion = ORM::factory('Autoevaluacion')
			//->add_reportes_stuff()
			->where('auto_id', '=', $this->request->query('id'))
			->where('auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
			->find();

		if (!$this->oAutoevaluacion->loaded())
		{
			throw new Exception_Saes('Autoevaluacion not found');
		}

		//return $this->oAutoevaluacion->oAcreditadoEvento->oAcreditado;
		return parent::get_acreditado_from_backend();
	}

	protected function check_autoevaluacion()
	{
        $reports = $this->oAutoevaluacion->get_reportes();

        if (empty($reports[$this->request->param('id')]))
            throw new HTTP_Exception_403('Acceso no autorizado');

        if ($this->oAcreditado->acre_id != $this->oAutoevaluacion->oAcreditadoEvento->acre_id)
            throw new HTTP_Exception_403('Acceso no autorizado');

        if (ACL::instance()->get_role() == 'regular' AND
            $this->oAutoevaluacion->oAcreditadoEvento->acre_id != $this->oAcreditado->acre_id)
        {
            throw new HTTP_Exception_403('Thou shalt not pass');
        }
	}

	public function after()
	{
		if ($this->auto_render){

			Theme::instance()
				->template
				->header
				->title = $this->title;
		}

		parent::after();
	}

	public static function get_document($aume_id)
	{
		$aDocuments = ORM::factory('Documento')
			->select('audo_estado')
			->join(array('autoevaluacion_documento', 'ad'))->using('docu_id')
			->where('aume_id', '=', $aume_id)
			->find_all();

		return $aDocuments;
	}

}
