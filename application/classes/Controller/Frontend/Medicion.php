<?php

defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Controller_Frontend_Medicion extends Controller_Frontend {

	private $status = 'OK';
	private $msg = '';
	
	/**
	 *
	 * @var Model_Autoevaluacion
	 */
	private $oAutoevaluacion;
	
	public function before()
	{
		parent::before();

		$esta = $this->check_autoevaluacion();

		$this->set_template_data();
	}

	public function action_index()
	{
		$context = $this->_context();
		
		$initial = $this->_initial();

//		debug($initial);

		$view = Theme_View::factory('frontend/autoevaluacion/' . $this->tipo)
			->set('context', json_encode($context))
			->set('initial', json_encode($initial));

		$url = REACT_MEDIA . 'medicion.js';
		Theme::instance()
				->js('default', 'plupload', '/saes_lte/js/plugins/plupload/plupload.full.min.js')
				->extra('<script src="' . $url . '"></script>')
			->template
			->content = $view;
	}

	private function _context()
	{
		$oAcreditado = $this->oAcreditado;

		$context = [];

		$oDimensiones = $oAcreditado->get_dimensiones();
		foreach ($oDimensiones as $dm)
		{
			$context['dimensiones'][] = [
				'display' => $dm->dime_titulo,
				'id' => $dm->dime_id,
				'descripcion' => $dm->dime_descripcion,
			];
		}

		$oFactores = $oAcreditado->get_factores();
		foreach ($oFactores as $ft)
		{
			$context['factores'][] = [
				'display' => $ft->fact_titulo,
				'id' => $ft->fact_id,
				'dime_id' => $ft->dime_id,
				'descripcion' => $ft->fact_descripcion,
			];
		}

		$oEstandares = $oAcreditado->get_estandares();
		foreach ($oEstandares as $st)
		{
			$context['estandares'][] = [
				'display' => $st->esta_titulo,
				'id' => $st->esta_id,
				'fact_id' => $st->fact_id,
				'descripcion' => $st->esta_descripcion,
				'esta_codigo' => $st->esta_codigo,
			];
		}

		$oAutoDetalle = $this->oAutoevaluacion->aDetalle->find_all();
		foreach ($oAutoDetalle as $ad)
		{
			$context['auto_detalles'][] = [
				'esta_id' => $ad->esta_id,
				'aude_id' => $ad->aude_id ?: ''
			];
		}

		return $context;
	}

	private function _initial()
	{
		$valida = 0;
		
		$aAutoActividadRelacionada = $this->oAutoevaluacion->get_auto_actividades_relacionadas();
		
		foreach ($aAutoActividadRelacionada as $ar)
		{
			$initial['auto_detalle_actividad'][] = [
				'auac_id' => $ar['auac_id'],
				'aude_id' => $ar['aude_id'],
				'nivel_factor' => $ar['auda_importancia_factor'],
				'nivel_avance' => $ar['auda_avance'],
			];
			$valida = 1;
		}

		$aAutoActividad = $this->oAutoevaluacion->get_auto_actividad();
		
		//debug($oAutoDetalle); die;
		foreach ($aAutoActividad as $aa)
		{			
			$initial['auto_actividades'][] = [
				'auac_id' => $aa->auac_id,
				'act_cod' => $aa->auac_numeracion,
				'aude_id' => $aa->aude_id,
				'estandares' => $aa->get_estandares(),
				'nivel_factor' => $aa->auac_importancia_factor,
				'nivel_avance' => $aa->auac_avance,
				'descripcion' => $aa->auac_titulo
			];
			$valida = 1;
		}

		if ($valida == 0)
		{
			$initial = new stdClass();
		}

		return $initial;
	}

	public function action_save()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);
        $auto_actividades = $post["auto_actividades"];
        $auto_detalle_actividades = $post["auto_detalle_actividad"];

		foreach ($auto_actividades as $ad)
		{
			$oAutoActividad = ORM::factory('AutoevaluacionActividad', 
				['auac_id' => $ad['auac_id']]);

			$oAutoActividad->set('auac_avance', $ad['nivel_avance'])
				->set('auac_importancia_factor', $ad['nivel_factor']);

			if (!$oAutoActividad->save())
			{
				$this->status = 'DENIED';
				$this->msg = 'No se pudo guardar el avance y el factor';
			}
		}
        
        foreach ($auto_detalle_actividades as $ada)
        {
            DB::update('autoevaluacion_detalle_actividad')
                ->value('auda_avance', $ada['nivel_avance'])
                ->value('auda_importancia_factor', $ada['nivel_factor'])
                ->where('aude_id', '=', $ada['aude_id'])
                ->and_where('auac_id', '=', $ada['auac_id'])
                ->execute();
        }

		$result = ['status' => $this->status, 'msg' => $this->msg];
		$this->response->body(json_encode($result));
	}
	
	private function check_autoevaluacion()
	{
		$this->oAutoevaluacion = $oAutoevaluacion = $this->oAcreditado->oAutoevaluacionActiva();

		if (!$oAutoevaluacion->loaded())
		{
			$this->redirect('/eventos');
		}

		$this->tipo = strtolower($this->oAcreditado->tipo_institucion());
	}
	
	private function set_template_data()
	{
		$theme = Theme::instance();

		$theme->js('default', 'smooth', '/saes_lte/js/plugins/smooth-scroll.js');
		$theme->js('default', 'jquery_form', '/saes_lte/js/plugins/jquery.form.min.js');
		$theme->css('page', 'medicion', '/saes_lte/css/medicion.css');
		$theme->js('page', 'medicion', '/saes_lte/js/medicion.js');
	}

}
