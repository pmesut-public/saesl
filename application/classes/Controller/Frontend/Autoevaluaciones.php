<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Frontend_Autoevaluaciones extends Controller_Frontend
{

    protected $oAutoevaluacion;

    public function action_index()
    {
        $aAutoevaluacion = ORM::factory('Autoevaluacion')
            ->join('acreditado_evento')->using('acev_id')
            ->where('acreditado_evento.acre_id', '=', $this->oAcreditado->acre_id)
            ->where('auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
            ->order_by('auto_numero')
            ->find_all();

        /*
        $report1 = Report::factory('cuantitativo')
            ->set('_acre_id', $this->oAcreditado->acre_id)
            ->get_chart();

        $report2 = Report::factory('cualitativo')
            ->set('_acre_id', $this->oAcreditado->acre_id)
            ->get_chart();
        */

        $view = Theme_View::factory('frontend/autoevaluaciones/list')->set(get_defined_vars());

        Theme::instance()
            ->template
            ->content = $view;
    }

    /**
     * Genera una nueva autoevaluación
     */
    public function action_new()
    {
        $oAcreditado = $this->oAcreditado;
        $oAutoevaluacion = $oAcreditado->oAutoevaluacionActiva();

        if ($oAutoevaluacion->loaded())
            $this->redirect('/autoevaluaciones');

        if ($this->request->method() == 'POST') {
            extract($this->request->post());
            $oEvento = ORM::factory('Evento', $even_id);
            $oAcreditado_evento = $oAcreditado->apply_to_evento($oEvento);
            $oAcreditado_evento->create_acreditado_evento_ficha();

            try {
                if ($tipo_auto === 'NUEVO') {
                    Model_Autoevaluacion::create_new($oAcreditado->oEventoActual());
                } else {
                    Model_Autoevaluacion::create_copia(
                        $oAcreditado->oEventoActual(),
                        $oAcreditado->oEventoAnterior()
                    );
                }
                $this->redirect('/autoevaluacion/calculator');

            } catch (Exception_Saes $e) {
                Session::instance()->set('error', $e->getMessage());
                $this->redirect('/autoevaluaciones');
            }
        }

        $this->redirect('/autoevaluaciones');


        /*
        $oAcreditado = $this->oAcreditado;

        // Auto activa
        if ($oAcreditado->oAutoevaluacionActiva()->loaded()) {
            $this->redirect('/autoevaluacion/calculator');
        }

        // Just create the autoevaluacion
        if ($oAcreditado->can_create_auto()) {
            Model_Autoevaluacion::create_new($oAcreditado->oEventoActual());
            $this->redirect('/autoevaluacion/calculator');
        }

        // Cuando se envia un post
        if ($this->request->method() == 'POST') {
            extract($this->request->post());
            $oEvento = ORM::factory('Evento', $even_id);

            try {
                if ($tipo_auto === 'NUEVO') {
                    $oAcreditado->apply_to_evento($oEvento);

                    Model_Autoevaluacion::create_new($oAcreditado->oEventoActual());

                    if ($oEvento->even_objeto == 1) {
                        $this->redirect('/mejora/calculator');
                    } else {
                        $this->redirect('/autoevaluacion/calculator');
                    }
                } else {
                    $oAcreditado->apply_to_evento($oEvento);

                    /*$deb = $oAcreditado->oEventoAnterior($oEvento->even_objeto);

                    debug($deb); die; */
        /*
                    Model_Autoevaluacion::create_copia(
                        $oAcreditado->oEventoActual(),
                        $oAcreditado->oEventoAnterior($oEvento->even_objeto)
                    );

                    if ($oEvento->even_objeto == 1) {
                        $this->redirect('/mejora/calculator');
                    } else {
                        $this->redirect('/autoevaluacion/calculator');
                    }
                }

            } catch (Exception_Saes $e) {
                Session::instance()->set('error', $e->getMessage());
                $this->redirect('/autoevaluaciones');
            }

            $this->redirect('/autoevaluaciones');
        }

        $result = [];

        $view = Theme_View::factory('frontend/autoevaluaciones/nueva')
            //->set(compact('aEvento'));
            ->set('result', json_encode($result));

        Theme::instance()
            ->template
            ->content = $view;
        */
    }


}
