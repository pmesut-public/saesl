<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Controller_Frontend_Formatos extends Controller_Frontend {

	private $title = 'Proceso de Licenciamiento';
	private $status = 'OK';
	private $msg = '';

	private $editable;
    private $ficha = Model_Ficha::FICHA_FORMATO;

	/**
	 *
	 * @var Model_Autoevaluacion
	 */
	private $oAutoevaluacion;
	private $tipo;

	public function before()
	{
		parent::before();

		$this->check_autoevaluacion();

		$this->set_template_data();
	}

	public function action_index()
	{
		$context = $this->_context();
		
		$initial = $this->_initial();

		$view = Theme_View::factory('frontend/formatos/index')
			->set('context', json_encode($context))
			->set('initial', json_encode($initial));

		$url = REACT_MEDIA . 'licenciamiento.js';
		Theme::instance()
				->js('default', 'plupload', '/saes_lte/js/plugins/plupload/plupload.full.min.js')
				->extra('<script src="' . $url . '"></script>')
			->template
			->content = $view;
	}

	public function action_documento_new()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = $this->request->post();
		//debug($_FILES);
                    //debug($post);
		// Subimos documento

		if ($_FILES['file']['size'] > $this->_config->mb_file_size * 1048576) 
		{ 
			$msg = 'Archivo excede los ' . $this->_config->mb_file_size . ' MB';
			$status = 422;

			$result = [
				'path'      => null,
				'display'   => null,
				'docu_id'   => null,
				'status'    => $status,
				'msg'       => $msg,
			];			
			
		} else {			
			$acre_id = $this->oAcreditado->acre_id;
			$filename = ORM::factory('Documento')->upload_archivo($acre_id, $_FILES['file']);
			// Grabamos en la tabla documento
			$docuId = $this->_new_documento_formato($filename, $acre_id, $post);

			$retorno = $this->oAutoevaluacion->documento_formato_new($docuId, $post);
			$result = [
				'path' => $retorno['docu']->docu_path,
				'display' => $retorno['docu']->docu_titulo,
				'docu_id' => $retorno['docu']->docu_id,
				'status' => $this->status,
				'msg' => $this->msg,
			];
		}		

		$this->response->body(json_encode($result));
	}

	public function _new_documento_formato($filename, $acre_id, $post)
	{
		if ($filename === FALSE)
		{
			throw new Exception_Saes('File could not be saved');
		}

		$status = 'OK';
		$msg = '';

		$_exist = $this->_if_exists($filename);

		$name = $_exist['name'];
		$ext = $filename['ext'];

		if ($_exist['exist'] === TRUE)
		{
			$status = 'EXISTING_FILE';
			$msg = 'Ya existe un archivo con el mismo nombre';
		}

		$oDocumento = ORM::factory('Documento');
		$oDocumento->set('acre_id', $acre_id)
			->set('docu_estado', Model_Documento::ACTIVO)
			->set('sub_estado', $post['sub_estado'])
			->set('estados', $post['estado'])
			->set('docu_titulo', $name . '.' . $ext)
			->set('docu_path', $filename['path'])
			->set('parent_id', NULL);
		$oDocumento->save();

		return $oDocumento->docu_id;
	}

	private function _if_exists($file, $id = NULL)
	{
		$name = $file['name'];
		$ext = $file['ext'];
		$exists = FALSE;

		if ($id)
		{
			$exists = !!$this->oAcreditado->aFile
					->where('docu_titulo', '=', $name . '.' . $ext)
					->where('docu_id', '<>', $id)
					->count_all();
		}
		else
		{
			while ($this->oAcreditado->aFile
				->where('docu_titulo', '=', $name . '.' . $ext)
				->count_all() > 0)
			{
				$exists = TRUE;
				$name = $name . ' new';
			}
		}

		return ['exist' => $exists, 'name' => $name];
	}

	public function action_documento_remove()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		$this->_documento_delete($post);

		$result = [
			'status' => $this->status,
			'msg'    => $this->msg,
		];

		$this->response->body(json_encode($result));
/*
		$documentoID = $post['docu_id'];

		debug($documentoID);


		$referidoEvaluacion = ORM::factory('AutoevaluacionDocumento')
								->where('docu_id', '=', $documentoID)
								->count_all();

		if ($referidoEvaluacion > 0){

			$documentosEvaluacion = QueryBuilder::factory('formatos/referencia_documentoEvaluacion.sql', [
				':documento_id' => $documentoID
			])->dict('docu_id')->get();

			$indicadores = $documentosEvaluacion[$documentoID]['esta_locacion'];

			$result = [
				'id'     => $documentoID,
				'status' => 'DENIED',
				'msg'    => 'No se puede eliminar el archivo porque está siendo referenciado en los indicador(es) '. $indicadores. ' de la autoevaluación.',
			];

		}
		else{

			$this->_documento_delete($post);

			$result = [
				'status' => $this->status,
				'msg'    => $this->msg,
			];
		}
*/
		$this->response->body(json_encode($result));
	}

	private function _documento_delete($post)
	{
		$this->auto_render = FALSE; // Deshabilita theme
		$oAutoevaluacion = $this->oAutoevaluacion;

		if ($post['tipo'] === 'FOR')
		{
			$oAutoDocu = ORM::factory('AutoevaluacionFormato')
				->where('form_id', '=', $post['id'])
				->where('docu_id', '=', $post['docu_id'])
				->where('auto_id', '=', $this->oAutoevaluacion->auto_id)
				->find();
		}
		if ($post['tipo'] === 'COND')
		{
			$oAutoDocu = ORM::factory('AutoevaluacionCondicion')
				->where('dime_id', '=', $post['id'])
				->where('docu_id', '=', $post['docu_id'])
				->where('auto_id', '=', $this->oAutoevaluacion->auto_id)
				->find();
		}

		//CHANGE STATUS DOCU_ESTADO (ELIMINADO)

		$docu_id = $oAutoDocu->docu_id;

		$Documento = Model_Documento::get_documento($docu_id);

		$Documento->docu_estado = Model_Documento::ELIMINADO;

        $Documento->save();

		$oAutoDocu->set('docu_id', NULL)->save();
	}

	public function action_save()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$this->_formato_update();

		$result = ['status' => $this->status, 'msg' => $this->msg];

		$this->response->body(json_encode($result));
	}

	private function _formato_update()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		if (count($post['lic_a']) > 0)
		{
			foreach ($post['lic_a'] as $la)
			{
				$oAutoFA = ORM::factory('AutoevaluacionFormato')
					->where('form_id', '=', $la['for_id'])
					->where('auto_id', '=', $this->oAutoevaluacion->auto_id)
					//->where('fecha_inicio', '=', $la['fecha_inicio'])
					//->where('fecha_fin', '=', $la['fecha_fin'])
					->find();

				/*$oAutoFA = $oAutoFA->set('aufo_estado', $la['estado'])
					->save();*/

				$this->_update_documentoasoc_formato($oAutoFA, $la);
			}
		}

		if (count($post['lic_c']) > 0)
		{
			foreach ($post['lic_c'] as $lc)
			{
				$oAutoFC = ORM::factory('AutoevaluacionFormato')
					->where('form_id', '=', $lc['for_id'])
					->where('auto_id', '=', $this->oAutoevaluacion->auto_id)
					//->where('fecha_inicio', '=', $lc['fecha_inicio'])
					//->where('fecha_fin', '=', $lc['fecha_fin'])
					->find();

				/*$oAutoFC = $oAutoFC->set('aufo_estado', $lc['estado'])
					->save();*/

				$this->_update_documentoasoc_formato($oAutoFC, $lc);
			
			}
		}

		if (count($post['cond']) > 0)
		{
			foreach ($post['cond'] as $co)
			{
				$oAutoFC = ORM::factory('AutoevaluacionCondicion')
					->where('dime_id', '=', $co['cond_id'])
					->where('auto_id', '=', $this->oAutoevaluacion->auto_id)
					//->where('fecha_inicio', '=', $co['fecha_inicio'])
					//->where('fecha_fin', '=', $co['fecha_fin'])
					->find();

				/*$oAutoFC = $oAutoFC->set('auco_estado', $co['estado'])
					->save();*/

				$this->_update_documentoasoc_formato($oAutoFC, $co);
			}
		}
	}

	/**
	 * Actualiza el documento asociado a cada formato de la autoevaluación.
	 */
	private function _update_documentoasoc_formato($oAutoFormato, $data){

		$documentoAsociado = $oAutoFormato->oDocumento;

		if ($documentoAsociado->loaded()) {
			
			$oAutoFormato
				->oDocumento
				->set('fecha_inicio', $data['fecha_inicio'] ? $data['fecha_inicio'] : NULL)
				->set('fecha_fin', $data['fecha_fin'] ? $data['fecha_fin'] : NULL)
				->set('da', $data['da'])
				->save();
		}
	}

	/**
	 * Maneja la finalización la Ficha de Formato.
	 */
	public function action_finish()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		//$this->_formato_update();

		$this->oAutoevaluacion->update_acreditado_evento_ficha(Model_Ficha::FICHA_GUARDADA, $this->ficha);

		$result = [
			'status' => 'OK',
			'msg'    => 'Ficha de Formatos completa.'
		];
		
		$this->response->body(json_encode($result));

	}

	/**
	 * Cierra la Autoevaluación y redirecciona a /autoevaluaciones.
	 */
	public function action_finish_evento(){

		$this->oAutoevaluacion->finish(); 

		$this->redirect('/autoevaluaciones'); 
	}

	public function action_force_finish()
	{
		if ($this->oAcreditado->acre_test != Model_Acreditado::TIPO_TEST)
			die('Sólo se puede forzar en usuarios de prueba');

		if ($this->oAutoevaluacion->auto_estado <> Model_Autoevaluacion::STATUS_ACTIVO)
		{
			throw new Exception_Saes('Autoevaluación no activa');
		}

		$this->oAutoevaluacion->force_finish();

		$this->redirect('/autoevaluaciones');
	}

	private function set_template()
	{
		$theme = Theme::instance();
		$theme->template->body_class = 'fixed';

		$new_header = Theme_View::factory('frontend/autoevaluacion/header', array(
				'header' => $theme->template->header,
		));

		$theme->template->header = $new_header;
		$theme->template->header->response = $this->oAutoevaluacion->result();

		$theme->template->sidebar = Theme_View::factory('frontend/autoevaluacion/sidebar', $this->get_sidebar_data());
	}

	private function check_autoevaluacion()
	{
		$this->oAutoevaluacion = $oAutoevaluacion = $this->oAcreditado->oAutoevaluacionActiva();
		if (!$oAutoevaluacion->loaded())
		{
			$oAcreditado = $this->oAcreditado;
			$oInstitucion = $oAcreditado->get_oInstitucion();

			// Seteamos el evento
			$oEvento = Model_Evento::get_evento($oInstitucion->tiin_id);
			//debug($oEvento);
			$oAcreditado->apply_to_evento($oEvento);
			//debug($oEvento);
			Model_Autoevaluacion::create_new($oAcreditado->oEventoActual());

			$this->redirect('/formatos');
		}

		$this->tipo = strtolower($this->oAcreditado->tipo_institucion());
	}

	private function set_template_data()
	{
		$theme = Theme::instance();

		$theme->js('default', 'smooth', '/saes_lte/js/plugins/smooth-scroll.js');
		$theme->js('default', 'jquery_form', '/saes_lte/js/plugins/jquery.form.min.js');
		$theme->css('page', 'autoevaluacion', '/saes_lte/css/autoevaluacion.css');
		$theme->css('page', 'calendar', '/saes_lte/css/calendar.css');
		$theme->js('page', 'autoevaluacion', '/saes_lte/js/autoevaluacion.js');
	}

	private function get_sidebar_data()
	{
		$data = DB::select('*')
			->select(DB::expr("concat(dime_codigo, '. ', dime_titulo) dime"))
			->select(DB::expr("concat(fact_codigo, '. ', fact_titulo) fact"))
			->select(DB::expr("concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta"))
			->select(DB::expr("concat(crit_codigo, '. ', coalesce(crit_descripcion)) crit"))
			->from('dimension')
			->join('factor')
			->using('dime_id')
			->join('estandar')
			->using('fact_id')
			->join('criterio')
			->using('esta_id')
			->join('autoevaluacion_detalle')
			->using('esta_id')
			->where('auto_id', '=', $this->oAutoevaluacion->auto_id)
			->execute()
			->as_array();

		$indicadores = array(
			'dime',
			'fact',
			'esta_codigo',
			'crit',
		);

		//debug($data);

		$tree = new TreeArray($data, $indicadores);

		$aDimension = $tree->get();

		return get_defined_vars();
	}

	public function after()
	{
		if ($this->auto_render)
		{
			Theme::instance()
				->template
				->header
				->title = $this->title;
		}

		parent::after();
	}

	private function _context()
	{
		$oAcreditado = $this->oAcreditado;
		$oInstitucion = $oAcreditado->get_oInstitucion();
		$this->editable = $this->oAutoevaluacion->oAcreditadoEvento->is_editable($this->ficha);
		$context = [
			'institucion' => $oInstitucion->inst_nombre,
			'modalidad' => $oAcreditado->oModalidad->moda_nombre,
			'gestion' => $oInstitucion->inst_gestion,
			'region' => $oInstitucion->inst_region,
			'editable' => $this->editable
		];

		$oEstadoIdicadores = (object) Model_Estandar::$estado_idicadores;
		foreach ($oEstadoIdicadores as $key => $value)
		{
			$context['estado_indicadores'][] = [
				'id' => $key,
				'display' => $value,
			];
		}

		/*$oEstados = (object) Model_Formato::$estados;
		foreach ($oEstados as $key => $value)
		{
			$context['estados'][] = [
				'id' => $key,
				'display' => $value,
			];
		}*/

		$oEstadosDocumento = (object) Model_Documento::$estados;
		foreach ($oEstadosDocumento as $key => $value) {
			$context['estados'][] = [
				'id' => $key,
				'display' => $value
			];
		}

		$sub_estados = (object) Model_Documento::$sub_estados;
		foreach ($sub_estados as $key => $value) {
			$context['sub_estados'][] = [
				'parentId' => $value['parentId'],
				'id' => $value['id'],
				'display' => $value['display'],
				'color' => $value['color']
			];
		}
		
		$oEstandares = $oAcreditado->get_estandares();
		foreach ($oEstandares as $st)
		{
			$context['ind'][] = [
				'cond_id' => $st->dime_id,
				'ind_id' => $st->esta_id,
				'cod' => $st->esta_codigo,
			];
		}

		$oAutoDetalle = $this->oAutoevaluacion->aDetalle->find_all();
		foreach ($oAutoDetalle as $ad)
		{
			$context['auto_detalles'][] = [
				'esta_id' => $ad->esta_id,
				'aude_id' => $ad->aude_id ?: '',
				'aude_estado' => $ad->indi_estado ?: '',
			];
		}

		$oAutoMedio = $this->oAutoevaluacion->get_auto_medio();
		foreach ($oAutoMedio as $am)
		{
			$context['auto_medio'][] = [
				'aume_id' => $am->aume_id,
				'aude_id' => $am->aude_id,
			];
		}

		$oAutoDocus = $this->oAutoevaluacion->get_auto_documento();
		foreach ($oAutoDocus as $ad)
		{
			$context['auto_documentos'][] = [
				'aume_id' => $ad->aume_id,
				//'audo_estado' => $ad->audo_estado,
				'audo_estado' => $ad->oDocumento->estados,
				'audo_subestado' => $ad->oDocumento->sub_estado,
				'da' => $ad->oDocumento->da,
				'fecha_inicio' => $ad->oDocumento->fecha_inicio,
				'fecha_fin' => $ad->oDocumento->fecha_fin,
				'display' => $ad->titulo
			];
		}

		return $context;
	}

	private function _initial()
	{
		$valida = 0;

		$oAutoFormA = $this->oAutoevaluacion->get_auto_formatosA();

        $initial['lic_a'] = [];
		foreach ($oAutoFormA as $fa)
		{
			if($fa->docu_estado == Model_Documento::ELIMINADO)
			{
//				$fa->docu_id = "";
//
//				$fa->docu_path = "";
//
//				$fa->docu_titulo = "";
			}

			$initial['lic_a'][] = [
				'for_id' => $fa->form_id,
				'display' => $fa->form_nombre,
				'estado' => $fa->aufo_estado,
				'docu_id' => $fa->docu_id,
				'path' => $fa->docu_path,
				'file_name' => $fa->docu_titulo,
				'fecha_inicio' => $fa->oDocumento->fecha_inicio,
				'fecha_fin' => $fa->oDocumento->fecha_fin,
				'estado' => $fa->oDocumento->estados,
				'docu_estado' => $fa->docu_estado,
				'sub_estado' => $fa->oDocumento->sub_estado,
				'da' => $fa->oDocumento->da
			];
			$valida = 1;
		}

		$oAutoFormC = $this->oAutoevaluacion->get_auto_formatosC();
        $initial['lic_c'] = [];
		foreach ($oAutoFormC as $fc)
		{
			if($fc->docu_estado == Model_Documento::ELIMINADO)
			{
//				$fc->docu_id = "";
//
//				$fc->docu_path = "";
//
//				$fc->docu_titulo = "";
			}

			$initial['lic_c'][] = [
				'for_id' => $fc->form_id,
				'display' => $fc->form_nombre,
				'estado' => $fc->aufo_estado,
				'docu_id' => $fc->docu_id,
				'path' => $fc->docu_path,
				'file_name' => $fc->docu_titulo,
				'fecha_inicio' => $fc->oDocumento->fecha_inicio,
				'fecha_fin' => $fc->oDocumento->fecha_fin,
				'estado' => $fc->oDocumento->estados,
				'docu_estado' => $fc->docu_estado,
				'sub_estado' => $fc->oDocumento->sub_estado,
				'da' => $fc->oDocumento->da
			];
			$valida = 1;
		}

		$oAutoCondicion = $this->oAutoevaluacion->get_auto_condicion();
        $initial['cond'] = [];
		foreach ($oAutoCondicion as $ac)
		{
			if($ac->docu_estado == Model_Documento::ELIMINADO)
			{
//				$ac->docu_id = "";
//
//				$ac->docu_path = "";
//
//				$ac->docu_titulo = "";
			}

			$initial['cond'][] = [
				'cond_id' => $ac->dime_id,
				'display' => $ac->dime_descripcion,
				'title' => $ac->dime_informacion,
				'titulo' => $ac->dime_titulo,
				'docu_id' => $ac->docu_id,
				'estado' => $ac->auco_estado,
				'path' => $ac->docu_path,
				'file_name' => $ac->docu_titulo,
				'fecha_inicio' => $ac->oDocumento->fecha_inicio,
				'fecha_fin' => $ac->oDocumento->fecha_fin,
				'estado' => $ac->oDocumento->estados,
				'docu_estado' => $ac->docu_estado,
				'sub_estado' => $ac->oDocumento->sub_estado,
				'da' => $ac->oDocumento->da
			];
			$valida = 1;
		}

		if ($valida == 0)
		{
			$initial = new stdClass();
		}

		//debug($initial);
		return $initial;
	}

	public function action_reporte()
	{
		$idAuto = $this->oAutoevaluacion->auto_id;
		$this->redirect('reportes/view/reporte/?id=' . $idAuto);
	}

	/**
	 * Maneja la ruta de /formatos/documentosformatos
	 */
	public function action_documentosformatos(){

		$oAutoevaluacion = $this->oAutoevaluacion;

		$oInstitucion = $this->oAcreditado->get_oInstitucion();

		$aDocumentos = Model_Formato::getDocumentosFormatos(
			$oAutoevaluacion->auto_id, $this->request
		);

		$aIDs = count(array_keys($aDocumentos)) == 0 ? [0] : array_keys($aDocumentos);

		$queryDocumentos = ORM::factory('Documento')->where('docu_id', ' IN ', $aIDs);

		$qDocumentos = clone $queryDocumentos;

        $qDocumentos = $qDocumentos->count_all();

        $pagination = Pagination::factory(['total_items' => $qDocumentos, 'items_per_page' => 20]);

        $oDocumentos = $queryDocumentos
            ->offset($pagination->offset)
            ->limit($pagination->items_per_page)
            ->find_all()
            ->as_array('docu_id');

        $title = 'Reporte vigencia A - B - C';

        $data =  compact('oAutoevaluacion', 'oInstitucion', 'aDocumentos', 'oDocumentos', 'title');

		$view = Theme_View::factory('frontend/formatos/reportes/docsformatosPDF', $data);

		$download = $this->request->query('download') ? $this->request->query('download') : '';

		$filename = 'reporte_vigencia';

		switch ($download) {

			case "excel":

				$theme = Theme::instance();
        		$theme->css(array());
				$theme->template = Theme_View::factory('reportes/template');
        		$theme->template->content = $view;

				Response::factory()
							->headers(['Content-Type' => 'application/vnd.ms-excel'])
							->body($theme->template)
							->send_file(TRUE, "{$filename}.xls");	
			break;

			case "pdf":

				$view = PDF::factory()->set('view', $view)->get();

				Response::factory()
							->headers(['Content-Type' => 'application/pdf'])
							->body($view)
							->send_file(TRUE, "{$filename}.pdf");
			break;
			
			default:

				$buttonData = ['path' => '/formatos/documentosformatos'];

				$aEstandares = QueryBuilder::factory('formatos/estandaresEvaluacion.sql', [
					':autoevaluacion_id' => $oAutoevaluacion->auto_id,
				])->get();

				$data = array_merge($data, [
					'aEstandares'   => $aEstandares,
					'buttonData'    => $buttonData,
					'pagination'    => $pagination,
					'requestParams' => $this->request->query()
				]);

				$viewLayout = Theme_View::factory('frontend/formatos/reportes/docsformatos', $data);

				$theme = Theme::instance();

				$theme->css('select_css', 'documento', '/saes_lte/js/plugins/select2/select2.css');
				$theme->js('select_js', 'documento', '/saes_lte/js/plugins/select2/select2.js');
        		$theme->js('select_js', 'formvalidation', '/saes_lte/js/reporte/docsregistrados.js');

				$theme->template->content = $viewLayout;
			break;
		}

	}

	/**
	 * Maneja la ruta de /formatos/documentosmedioverificacion
	 */
	public function action_documentosmedioverificacion(){

		$oAutoevaluacion = $this->oAutoevaluacion;

		$oInstitucion = $this->oAcreditado->get_oInstitucion();

		$aDocumentos = Model_Formato::getDocumentosMedioVerificacion(
			$oAutoevaluacion->auto_id, $this->request
		);

		$aIDs = count(array_keys($aDocumentos)) == 0 ? [0] : array_keys($aDocumentos);

		$queryDocumentos = ORM::factory('Documento')->where('docu_id', ' IN ', $aIDs);

		$qDocumentos = clone $queryDocumentos;

        $qDocumentos = $qDocumentos->count_all();

        $pagination = Pagination::factory(['total_items' => $qDocumentos, 'items_per_page' => 20]);

        $oDocumentos = $queryDocumentos
            ->offset($pagination->offset)
            ->limit($pagination->items_per_page)
            ->find_all()
            ->as_array('docu_id');

		$title = 'Reporte vigencia MV';
		
		$data  =  compact('oAutoevaluacion', 'oInstitucion', 'aDocumentos', 'oDocumentos', 'title');

		$view  = Theme_View::factory('frontend/formatos/reportes/docsmedioverificacionPDF', $data);

		$download = $this->request->query('download') ? $this->request->query('download') : '';

		$filename = 'reporte_vigencia';

		switch ($download) {

			case "excel":

				$theme = Theme::instance();
        		$theme->css(array());
				$theme->template = Theme_View::factory('reportes/template');
        		$theme->template->content = $view;

				Response::factory()
							->headers(['Content-Type' => 'application/vnd.ms-excel'])
							->body($theme->template)
							->send_file(TRUE, "{$filename}.xls");	
			break;

			case "pdf":

				$view = PDF::factory()->set('view', $view)->get();

				Response::factory()
							->headers(['Content-Type' => 'application/pdf'])
							->body($view)
							->send_file(TRUE, "{$filename}.pdf");
			break;
			
			default:

				$buttonData = ['path' => '/formatos/documentosmedioverificacion'];

				$aEstandares = QueryBuilder::factory('formatos/estandaresEvaluacion.sql', [
					':autoevaluacion_id' => $oAutoevaluacion->auto_id,
				])->get();

				$data = array_merge($data, [
					'aEstandares'   => $aEstandares,
					'buttonData'    => $buttonData,
					'pagination'    => $pagination,
					'requestParams' => $this->request->query()
				]);

				$viewLayout = Theme_View::factory('frontend/formatos/reportes/docsmedioverificacion', $data);

				$theme = Theme::instance();

				$theme->css('select_css', 'documento', '/saes_lte/js/plugins/select2/select2.css');
				$theme->js('select_js', 'documento', '/saes_lte/js/plugins/select2/select2.js');
        		$theme->js('select_js', 'formvalidation', '/saes_lte/js/reporte/docsregistrados.js');

				$theme->template->content = $viewLayout;
			break;
		}

	}

	/**
	 * Maneja la ruta de /formatos/documentoshistoricos
	 */
	public function action_documentoshistoricos(){

		$oAutoevaluacion = $this->oAutoevaluacion;

		$oInstitucion = $this->oAcreditado->get_oInstitucion();

		$aDocumentos = Model_Formato::getDocumentosHistoricos(
			$oAutoevaluacion->auto_id, $this->request
		);

		$aIDs = count(array_keys($aDocumentos)) == 0 ? [0] : array_keys($aDocumentos);

		$queryDocumentos = ORM::factory('Documento')->where('docu_id', ' IN ', $aIDs)->order_by('docu_id', 'DESC');

		$qDocumentos = clone $queryDocumentos;

        $qDocumentos = $qDocumentos->count_all();

        $pagination = Pagination::factory([
			'total_items'    => $qDocumentos,
			'items_per_page' => 20,
        ]);

        $oDocumentos = $queryDocumentos
            ->offset($pagination->offset)
            ->limit($pagination->items_per_page)
            ->find_all()
            ->as_array('docu_id');

        $title = 'Reporte Documentos Históricos';

		$data =  compact('oAutoevaluacion', 'oInstitucion', 'aDocumentos', 'oDocumentos', 'title');

		$view = Theme_View::factory('frontend/formatos/reportes/docshistoricosPDF', $data);

		$download = $this->request->query('download') ? $this->request->query('download') : '';

		$filename = 'reporte_historico';

		switch ($download) {

			case "excel":

				$theme = Theme::instance();
        		$theme->css(array());
				$theme->template = Theme_View::factory('reportes/template');
        		$theme->template->content = $view;

				Response::factory()
							->headers(['Content-Type' => 'application/vnd.ms-excel'])
							->body($theme->template)
							->send_file(TRUE, "{$filename}.xls");	
			break;

			case "pdf":

				$view = PDF::factory()->set('view', $view)->get();

				Response::factory()
							->headers(['Content-Type' => 'application/pdf'])
							->body($view)
							->send_file(TRUE, "{$filename}.pdf");
			break;
			
			default:

				$buttonData = [
					'path' => '/formatos/documentoshistoricos'
				];

				$data = array_merge($data, [
					'buttonData'    => $buttonData,
					'pagination'    => $pagination,
				]);

				$viewLayout = Theme_View::factory('frontend/formatos/reportes/docshistoricos', $data);

				Theme::instance()->template->content = $viewLayout;
			break;
		}
	}

	/**
	 * Deja en blanco los datos asociados a los formatos A,B y C
	 */
	public function action_cancelar_autoevaluacion() {

		$oAutoevaluacion = $this->oAutoevaluacion;	

		$oAutoevaluacion->limpiar_formatos();	

		Session::instance()->set('warning', 'La Autoevaluación de formatos A, B, C fue cancelada.');

		$this->redirect('/formatos');				
	}

	/**
	 * Cancela la autoevaluación completa	
	 */
	public function action_cancelar_evento() {

		$oAutoevaluacion = $this->oAutoevaluacion;	

		$oAutoevaluacion->update_acreditado_evento_ficha(Model_Ficha::FICHA_GUARDADA);
		$oAutoevaluacion->limpiar_formatos();	
		$oAutoevaluacion->discard();

		Session::instance()->set('warning', 'El evento fue cancelado.');

		$this->redirect('/autoevaluaciones');
	}

	/**
	 * Valida el estado de la Ficha de Autoevaluación.
	 * 
	 * @return JSON $result resultado
	 */
	public function action_autoevaluacion_status() {

		$this->auto_render = FALSE; 	// Deshabilita theme

		$estado = Model_Ficha::is_finished(
			$this->oAutoevaluacion->oAcreditadoEvento->acev_id, Model_Ficha::FICHA_AUTOEVALUACION
		);

		$result = [
			'status' => 'OK',
			'msg'    => '',
			'estado' => (bool) $estado			
		];

		$this->response->body(json_encode($result));
	}
        
        public function action_estado_fichas() {

		$this->auto_render = FALSE; 	// Deshabilita theme
                
                $condiciones_basicas_cancelado = 0;
                $autoevaluacion_formatos_cancelado = 0;
                if ($this->request->method() == 'POST'){
                    
                    $count_data_auto = 0;
                    $count_data_format = 0;
                    $result1 = DB::select(DB::expr("COUNT(auto_id) as count_data"))
                            ->from('autoevaluacion_detalle')                     
                            ->join(DB::expr('autoevaluacion_medio'))->using('aude_id')
                            ->join(DB::expr('autoevaluacion_documento'))->using('aume_id')
                            ->where('autoevaluacion_detalle.auto_id', '=', $this->oAutoevaluacion->auto_id)->execute()->current();
                    ($result1)?$count_data_auto += $result1["count_data"]:null;

                    $result2 = DB::select(DB::expr("COUNT(auto_id) as count_data"))
                            ->from('autoevaluacion_formato')
                            ->where('docu_id', '<>', NULL)
                            ->where('auto_id', '=', $this->oAutoevaluacion->auto_id)->execute()->current();
                    ($result2)?$count_data_format += $result2["count_data"]:null;

                    $result3 = DB::select(DB::expr("COUNT(auto_id) as count_data"))
                            ->from('autoevaluacion_condicion')
                            ->where('docu_id', '<>', NULL)
                            ->where('auto_id', '=', $this->oAutoevaluacion->auto_id)->execute()->current();
                    ($result3)?$count_data_format += $result3["count_data"]:null;                
                    
                    $condiciones_basicas_cancelado = ($count_data_auto === 0)?1:0;
                    $autoevaluacion_formatos_cancelado = ($count_data_format === 0)?1:0;
                }
                
                $result = [
                    'condiciones_basicas_cancelado'=> $condiciones_basicas_cancelado, //1 true, 0 false,
                    'autoevaluacion_formatos_cancelado' => $autoevaluacion_formatos_cancelado, //1 true , 0 false
                    'status' => 'OK',
                    'msg' => '',
		];
                
		$this->response->body(json_encode($result));
	}
}