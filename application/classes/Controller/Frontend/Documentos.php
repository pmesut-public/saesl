<?php

defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Controller_Frontend_Documentos extends Controller_Frontend {

	private $oAutoevaluacion;

	public function before()
	{
		parent::before();

		$this->check_autoevaluacion();
	}

	public function action_index()
	{
		$result = $this->_get_documentos();

		//debug($result);

		$view = Theme_View::factory('frontend/documentos/list')
			->set('result', json_encode($result));

		$url = REACT_MEDIA . 'documentos.js';
		Theme::instance()
				->js('default', 'plupload', '/saes_lte/js/plugins/plupload/plupload.full.min.js')
				->extra('<script src="' . $url . '"></script>')
			->template
			->content = $view;
	}

	public function action_get()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$result = $this->_get_documentos();
		
		$result['status'] = 'OK';
		$result['msg'] = '';

		$this->response->body(json_encode($result));
	}

	public function action_create()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$acre_id = $this->oAcreditado->acre_id;

		if ($this->request->post_max_size_exceeded())
		{
			throw new Exception_Saes('File too big');
		}

		$post = $this->request->post();

		$filename = ORM::factory('Documento')->upload_archivo($acre_id, $_FILES['file']);

		if ($filename === FALSE)
		{
			throw new Exception_Saes('File could not be saved');
		}

		$status = 'OK';
		$msg    = '';
		$_exist = $this->_if_exists($filename);

		$nombreDocumento   = $_exist['name'].'.'. $filename['ext'];
		$documentoSeniorId = null;
		$nuevasRelaciones  = [];

		/* Creación del Nuevo Documento */

		$oDocumento = ORM::factory('Documento');

		$oDocumentoNuevo = $oDocumento->set('acre_id', $acre_id)
										->set('docu_estado', Model_Documento::ACTIVO)
										->set('estados', @$post['estado'])
										->set('sub_estado', @$post['sub_estado'])
										->set('da', @$post['da'] ? $post['da'] : 0)
										->set('fecha_inicio', @$post['fecha_inicio'] ? $post['fecha_inicio'] : NULL)
										->set('fecha_fin', @$post['fecha_fin'] ? $post['fecha_fin'] : NULL)
										->set('docu_titulo', $nombreDocumento)
										->set('docu_path', $filename['path'])
										->set('replace_id', $documentoSeniorId)
										->save();

		$replaceFile = (bool) $post['replace_file'];

		/* Sí el documento existe realiza lo siguiente: */

		if ($_exist['exist'] === TRUE && $replaceFile)
		{

			$nombreArchivo  = $filename['name'].'.'.$filename['ext'];

			/* Busca el último documento acreditado con el mismo nombre y actualizalo */

			$oDocumentosOld = $this->oAcreditado->aFile->where('docu_titulo', '=', $nombreArchivo)
														->order_by('docu_id', 'DESC')
														->find();

			$nombreDocumento   = $oDocumentosOld->docu_titulo;
			$documentoSeniorId = $oDocumentosOld->docu_id;

			$oDocumentosOld->docu_estado    = Model_Documento::REEMPLAZADO;
			$oDocumentosOld->docu_fecha_act = date('Y-m-d H:i:s');
            $oDocumentosOld->save();

            /* Actualizamos el nuevo documento con el replace_id */

			$oDocumentoNuevo->docu_titulo = $nombreArchivo;
			$oDocumentoNuevo->replace_id  = $documentoSeniorId;
			$oDocumentoNuevo->save();

			$status = 'OK';
			$msg    = '';

			/* Documentos relacionados en AutoEvaluación Documentos */

			$aDocumentosAutoOld = ORM::factory('AutoevaluacionDocumento')
						            ->where('docu_id', '=', $documentoSeniorId)
						            ->find_all()
						            ->as_array('audo_id');

			if (count($aDocumentosAutoOld)) {

				/*
					DB::update('autoevaluacion_documento')
						->set(['docu_id' => $oDocumento->docu_id])
						->where('docu_id', '=', $documentoSeniorId)
						->execute();
				*/

				foreach ($aDocumentosAutoOld as $oDocumentoAutoOld) {
					
					$nuevoDocuAuto = ORM::factory('AutoevaluacionDocumento')
											->set('aume_id', $oDocumentoAutoOld->aume_id)
											->set('docu_id', $oDocumento->docu_id)
										->save();

					$oReemplazado = [
						'audo_id'        => $nuevoDocuAuto->audo_id,
						'aume_id'        => $nuevoDocuAuto->aume_id,
						'docu_id'        => $nuevoDocuAuto->docu_id,
						'audo_estado'    => $oDocumento->estados,
						'audo_subestado' => $oDocumento->sub_estado,
						'fecha_inicio'   => $oDocumento->fecha_inicio,
						'fecha_fin'      => $oDocumento->fecha_fin,
						'da'             => $oDocumento->da,
						'path'           => $oDocumento->docu_path,
						'titulo'         => $oDocumento->docu_titulo,
						'definido'       => false,
					];

					$nuevasRelaciones[] = $oReemplazado;

				}
		
			}
		}

		$result = [
			'path'         => $filename['path'],
			'display'      => $nombreDocumento,
			'id'           => $oDocumento->docu_id,
			'status'       => $status,
			'msg'          => $msg,
			'fecha_inicio' => $oDocumento->fecha_inicio,
			'fecha_fin'    => $oDocumento->fecha_fin,
			'da'           => $oDocumento->da,
			'reemplazos'   => $nuevasRelaciones
		];

		$this->response->body(json_encode($result));
	}

	public function action_rename()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		$result = [
			'id' => $post['id'],
			'status' => 'DENIED',
			'msg' => 'No se puede, ya existe un archivo con este nombre',
		];

		$file = explode('.', $post['display']);

		$_exist = $this->_if_exists(['name' => $file[0], 'ext' => $file[1]], $post['id']);

		if ($_exist['exist'] === FALSE)
		{
			$oDocumento = ORM::factory('Documento', $post['id']);
			$oDocumento->docu_titulo = $post['display'];
			$oDocumento->save();

			$result = [
				'path' => $oDocumento->docu_path,
				'display' => $oDocumento->docu_titulo,
				'id' => $oDocumento->docu_id,
				'status' => 'OK',
				'msg' => '',
			];
		}

		$this->response->body(json_encode($result));
	}

	public function action_delete()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);
		$id = $post['id'];

		$referido = ORM::factory('AutoevaluacionDocumento')
				->where('docu_id', '=', $id)->count_all();

		$result = ['id' => $id, 'status' => 'OK', 'msg' => '',];

		//debug($referido); die;

		if ($referido > 0)
		{
			$result = [
				'id' => $id,
				'status' => 'DENIED',
				'msg' => 'No se puede eliminar el archivo porque está siendo referenciado en una evidencia',
			];
		}
		else
		{
			$oDocumento = ORM::factory('Documento', $id);
			$oDocumento->docu_estado = Model_Documento::STATUS_ELIMINADO;
			$oDocumento->save();

			Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
				':model' => 'Documento',
				':pk' => $oDocumento->primary_key(),
				':id' => $oDocumento->pk(),
				':mode' => 'deleted',
			)));

			Log::access($msg);
		}

		$this->response->body(json_encode($result));
	}

	private function _if_exists($file, $id = NULL)
	{
		$name   = $file['name'];
		$ext    = $file['ext'];
		$exists = FALSE;

		if ($id)
		{
			$exists = !!$this->oAcreditado->aFile
					->where('docu_titulo', '=', $name . '.' . $ext)
					->where('docu_id', '<>', $id)
					->count_all();
		}
		else
		{
			while ($this->oAcreditado->aFile
				->where('docu_titulo', '=', $name . '.' . $ext)
				->count_all() > 0)
			{
				$exists = TRUE;
				$name = $name . ' new';
			}
		}

		return ['exist' => $exists, 'name' => $name];
	}

	private function _get_documentos()
	{
		$oEstandares = $this->oAcreditado->get_estandares();
		$result['estandares'] = [];
                $result['ind'] = [];
		foreach ($oEstandares as $st)
		{
			$result['estandares'][] = [
				'esta_id' => $st->esta_id,
				'display' => $st->esta_titulo,
				'esta_codigo' => $st->esta_codigo,
				'fact_id' => $st->fact_id,
			];
                        
                        $result['ind'][] = [
				'cond_id' => $st->dime_id,
				'ind_id' => $st->esta_id,
				'cod' => $st->esta_codigo,
			];
		}

		$oAutoFormA = $this->oAutoevaluacion->get_auto_formatosA();
        $result['lic_a'] = [];
		foreach ($oAutoFormA as $fa)
		{
			$result['lic_a'][] = [
				'for_id' => $fa->form_id,
				'display' => $fa->form_nombre,
			];
			$valida = 1;
		}

		$oAutoFormC = $this->oAutoevaluacion->get_auto_formatosC();
        $result['lic_c'] = [];
		foreach ($oAutoFormC as $fc)
		{
			$result['lic_c'][] = [
				'for_id' => $fc->form_id,
				'display' => $fc->form_nombre,
			];
			$valida = 1;
		}

		$oEstadosDocumento = (object) Model_Documento::$estados;
		foreach ($oEstadosDocumento as $key => $value) {
			$result['estados'][] = [
				'id' => $key,
				'display' => $value
			];
		}

		$sub_estados = (object) Model_Documento::$sub_estados;
		foreach ($sub_estados as $key => $value) {
			$result['sub_estados'][] = [
				'parentId' => $value['parentId'],
				'id' => $value['id'],
				'display' => $value['display'],
				'color' => $value['color']
			];
		}

		$oAutoCondicion = $this->oAutoevaluacion->get_auto_condicion();
        $result['cond'] = [];
		foreach ($oAutoCondicion as $ac)
		{
			$result['cond'][] = [
				'cond_id' => $ac->dime_id,
				'display' => $ac->dime_descripcion,
			];
			$valida = 1;
		}

		$oAutoDetalle = $this->oAutoevaluacion->aDetalle->find_all();
        $result['auto_detalles'] = [];
		foreach ($oAutoDetalle as $ad)
		{
			$result['auto_detalles'][] = [
				'esta_id' => $ad->esta_id,
				'aude_id' => $ad->aude_id ?: '',
			];
		}

		$oAutoMedio = $this->oAutoevaluacion->get_auto_medio();
        $result['auto_medio'] = [];
		foreach ($oAutoMedio as $am)
		{
			$result['auto_medio'][] = [
				'aume_id' => $am->aume_id,
				'aude_id' => $am->aude_id,
			];
		}
                
                // autoevaluacion ya sea activa o cerrada
		$auto = $this->oAcreditado->oAutoevaluacion();
		$acreditado = $this->oAcreditado->acreditadoId();
                
//		$aDocumentos = $this->_finsAll_documentos_autoevaluacion($auto);
                
                $aDocumentos = $this->oAcreditado->aFile
                    ->where('docu_estado', '=', Model_Documento::STATUS_ACTIVO)                                       
                    ->find_all();

        $result['files'] = [];

		foreach ($aDocumentos as $dc)
		{
			$eIdes = $this->oAutoevaluacion->get_estandar_autodetalle($dc->docu_id, $acreditado, $auto->auto_id);
			$explode = $eIdes ? explode(' ', trim($eIdes)) : [];
			$eIdesPas = $this->oAutoevaluacion->get_estandar_autodetalle($dc->docu_id, $acreditado, FALSE);
			$explodePast = $eIdesPas ? explode(' ', trim($eIdesPas)) : [];

			$formCond = $this->_form_cond_ides($dc->docu_id, $acreditado, $auto->auto_id);

			$documento = ORM::factory('Documento',$dc->docu_id);
			
			$formato = $documento->has_formatos() ? 1 : 0;

			$result['files'][] = [
				'id' => $dc->docu_id,
				'display' => $dc->docu_titulo,
				'path' => $dc->docu_path,
				'esta_ids' => $explode,
				'past_esta_ids' => $explodePast,
				'for_ids' => $formCond['for'],
				'past_for_ids' => $formCond['forPast'],
				'cond_ids' => $formCond['cond'],
				'past_cond_ids' => $formCond['condPast'],
				'docu_fecha_reg' => $dc->docu_fecha_reg,
				'docu_fecha_act' => $dc->docu_fecha_act,
				'fecha_inicio' => $dc->fecha_inicio,
				'fecha_fin' => $dc->fecha_fin,
				'estado' => $dc->estados,
				'sub_estado' => $dc->sub_estado,
				'da' => $dc->da,
				'formato' => $formato
			];
		}
		return $result;
	}
        
        private function _finsAll_documentos_autoevaluacion(Model_Autoevaluacion $auto) {            
            $aDocuIds=[];
            $autoDocuemnto = ORM::factory('Documento')
                            ->join(DB::expr('autoevaluacion_documento'), 'left')->using('docu_id')
                            ->join(DB::expr('autoevaluacion_medio'), 'left')->using('aume_id')
                            ->join(DB::expr('autoevaluacion_detalle'), 'left')->using('aude_id')
                            ->where_open()
                                ->where('autoevaluacion_detalle.auto_id', '=', $auto->copy_auto_id)
                                ->or_where('autoevaluacion_detalle.auto_id', '=', $auto->auto_id)
                                ->or_where('autoevaluacion_detalle.auto_id', '=', NULL)
                            ->where_close()
                            ->group_by("docu_id")
                            ->find_all();
            
            foreach ($autoDocuemnto as $docu) {
                $aDocuIds[] = $docu->docu_id;
            }            
            $autoDocuemnto = ORM::factory('AutoevaluacionFormato')
                            ->where('docu_id', '<>', NULL)
                            ->where_open()
                                ->where('auto_id', '=', $auto->copy_auto_id)
                                ->or_where('auto_id', '=', $auto->auto_id)
                            ->where_close()
                            ->group_by("docu_id")->find_all();
            foreach ($autoDocuemnto as $docu) {
                $aDocuIds[] = $docu->docu_id;
            }
                
            return $this->oAcreditado->aFile
                    ->where('docu_estado', '=', Model_Documento::STATUS_ACTIVO)
                    ->where("docu_id", "IN", $aDocuIds)                                          
                    ->find_all();
        }

	private function _form_cond_ides($docuId, $acreId, $autoId)
	{
		// Formatos
		$fIdes = $this->oAutoevaluacion->get_formato_documento($docuId, $acreId, $autoId);
		$explodeF = $fIdes ? explode(' ', trim($fIdes)) : [];
		$fIdesPas = $this->oAutoevaluacion->get_formato_documento($docuId, $acreId, FALSE);
		$PastF = $fIdesPas ? explode(' ', trim($fIdesPas)) : [];

		// Condiciones (dimensiones)
		$cIdes = $this->oAutoevaluacion->get_condicion_documento($docuId, $acreId, $autoId);
		$explodeC = $cIdes ? explode(' ', trim($cIdes)) : [];
		$cIdesPas = $this->oAutoevaluacion->get_condicion_documento($docuId, $acreId, FALSE);
		$PastC = $cIdesPas ? explode(' ', trim($cIdesPas)) : [];

		return ['for' => $explodeF, 'forPast' => $PastF, 'cond' => $explodeC, 'condPast' => $PastC];
	}

	public function action_create_zip()
	{
		$this->auto_render = FALSE; // Deshabilita theme
		$post = json_decode($this->request->body(), TRUE);

		// acreditado
		$acreId = $this->oAcreditado->acreditadoId();

		$oDocumento = ORM::factory('Documento');
		$path = $oDocumento->get_path() . $acreId . '/';

		$zip = new ZipArchive();
		$filename = APPPATH . $path . $acreId . '.zip';

		if(file_exists($filename))
		    unlink($filename);

		$zip->open($filename, ZIPARCHIVE::CREATE);
		foreach ((object) $post['ids'] as $id)
		{
			$docu = $oDocumento->get_documento($id);
			$localfile = basename(APPPATH . $docu->docu_path);
			$zip->addFile(APPPATH . $docu->docu_path, $docu->docu_titulo);
		}
		$zip->close();

		$result = [
			'path' => $path . $acreId . '.zip',
			'display' => $acreId . '.zip',
			'status' => 'OK',
			'msg' => '',
		];

		$this->response->body(json_encode($result));
	}

	private function check_autoevaluacion()
	{
		$this->oAutoevaluacion = $this->oAcreditado->oAutoevaluacionActiva();
	}

	/**
	 * Descargar los archivos del repositorio.
	 */
	public function action_create_tree_zip(){
		
		$this->auto_render = FALSE;

		/* Bootstrapping */
		
		$input  = json_decode($this->request->body(), TRUE);
		
		$acreId = $this->oAcreditado->acreditadoId();
		
		$oDocumento     = ORM::factory('Documento');
		
		$pathAcreditado = $oDocumento->get_path() . $acreId . '/';
		
		$archivoZIP   = new ZipArchive();

		$fileDownload = $pathAcreditado . $acreId . '.zip';
		
		$filename     = APPPATH . $fileDownload;

		if(file_exists($filename)){ unlink($filename); }

		/* Queries */

		$aReporte = QueryBuilder::factory('reportegeneral/reporte_general.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->get();

		$aReporteGrouped = array_group_by($aReporte, 'dime_id', 'fact_id', 'esta_id', 'aude_id', 'aume_id', 'audo_id');

		$aDimensiones = QueryBuilder::factory('reportegeneral/reporte_dimension.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->dict('dime_id')->get();

		$aFactores = QueryBuilder::factory('reportegeneral/reporte_factor.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->dict('fact_id')->get();

		$aEstandares = QueryBuilder::factory('reportegeneral/reporte_estandar.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->dict('esta_id')->get();

		$aDocumentosEvaluacion = QueryBuilder::factory('fichas/tree_documentos.sql', [
			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
		])->dict('docu_id')->get();

		/* Generación del ZIP */

		$archivoZIP->open($filename, ZIPARCHIVE::CREATE);

		foreach ($aReporteGrouped as $dime_id => $arFactores) {

			$folderCondicion = $aDimensiones[$dime_id]['dime_codigo'];
		
			foreach ($arFactores as $fact_id => $arEstandar) {
				
				foreach ($arEstandar as $esta_id => $arDetalle) {

					$folderIndicador = $aEstandares[$esta_id]['esta_codigo'];
						
					foreach ($arDetalle as $aude_id => $arMedio) {

						foreach ($arMedio as $aume_id => $arAutoDocumentos) {

							if (count($arAutoDocumentos)) {
								
								foreach ($arAutoDocumentos as $element) {
									
									$documento = $element[0];

									$documento = $aDocumentosEvaluacion[$documento['docu_id']];

									$documentoPath = APPPATH . $documento['docu_path'];

									$localPathZIP  = "Medio de verificacion/Condicion {$folderCondicion}/Indicador {$folderIndicador}/{$documento['docu_titulo']}";

									$archivoZIP->addFile($documentoPath, $localPathZIP);
								}

							} else {

								$localPathZIP  = "Medio de verificacion/Condicion {$folderCondicion}/Indicador {$folderIndicador}/";
								
								$archivoZIP->addFile($documentoPath, $localPathZIP);
							}

						}
					}
					
				}
			}	
		}

		$oDocumentosSueltos = Model_Documento::documentos_sueltos($acreId, $this->oAutoevaluacion->auto_id);

		if (count($oDocumentosSueltos)) {

			foreach ($oDocumentosSueltos as $documento) {
			
				$documentoPath = APPPATH . $documento['docu_path'];

				$localPathZIP  = "Varios/{$documento['docu_titulo']}";

				$archivoZIP->addFile($documentoPath, $localPathZIP);
			}
		}

		$archivoZIP->close();

		/* Response */

		$response = [
			'path'    => $fileDownload,
			'display' => $acreId . '.zip',
			'status'  => 'OK',
			'msg'     => '',
		];

		$this->response->body(json_encode($response));

	}
}
