<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Controller_Frontend_Mejora extends Controller_Frontend {

	private $title = 'Proceso de Mejora';
	private $status = 'OK';
	private $msg = '';

	/**
	 *
	 * @var Model_Autoevaluacion
	 */
	private $oAutoevaluacion;
	private $tipo;

	public function action_index()
	{
		$this->redirect('/autoevaluaciones');
	}

	public function before()
	{
		parent::before();

		$esta = $this->check_autoevaluacion();

		$this->set_template_data();
	}

	public function action_calculator()
	{
		$context = $this->_context();

		$initial = $this->_initial();

//		debug($initial);

		$view = Theme_View::factory('frontend/mejora/' . $this->tipo)
			->set('context', json_encode($context))
			->set('initial', json_encode($initial));

		$url = REACT_MEDIA . 'mejora.js';
		Theme::instance()
				->js('default', 'plupload', '/saes_lte/js/plugins/plupload/plupload.full.min.js')
				->extra('<script src="' . $url . '"></script>')
			->template
			->content = $view;
	}

	private function _update()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$this->_autodetalle_update();
	}

	public function action_save()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$this->_update();

		$result = ['status' => $this->status, 'msg' => $this->msg];

		$this->response->body(json_encode($result));
	}

	public function action_finish()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$this->_update();

		$this->oAutoevaluacion->finish();

		$this->redirect('/autoevaluaciones');
	}

	private function _autodetalle_update()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

//		debug($post['mejo_detalles']);

		if (count($post['mejo_detalles']) > 0)
		{
			foreach ($post['mejo_detalles'] as $md)
			{
				$oAutoDetalle = ORM::factory('AutoevaluacionDetalle')
					->where('aude_id', '=', $md['mede_id'])
					->find();
				
//				debug($oAutoDetalle);

				/*$oAutoDetalle->set('aude_valoracion', $md['mede_estado'])
					->set('aude_justificacion', $md['mede_actividades'])
					->set('aude_comentario', $md['mede_impacto'])
					->set('aude_pregunta1', $md['mede_nivel'])
					->set('aude_pregunta2', $md['mede_actividades'])
					->set('aude_pregunta3', $md['mede_impacto'])
					->save();*/
			}
		}
	}

	public function action_save_session()
	{
		$oAutoevaluacion = $this->oAutoevaluacion;

		$oAutoevaluacion->auto_fecha_fin = date('Y-m-d');

		// Manually add modification date (It won't update automatically if auto_fecha_fin has not been changed)
		$oAutoevaluacion->auto_fecha_act = date('Y-m-d H:i:s');

		$oAutoevaluacion->save();

		$this->redirect('/autoevaluaciones');
	}

	public function action_cancel()
	{
		$this->oAutoevaluacion->discard();

		$this->redirect('/autoevaluaciones');
	}

	public function action_force_finish()
	{
		if ($this->oAcreditado->acre_test != Model_Acreditado::TIPO_TEST)
			die('Sólo se puede forzar en usuarios de prueba');

		//if ( ! Auth::instance()->logged_in('admin'))
		//die('Not allowed');

		if ($this->oAutoevaluacion->auto_estado <> Model_Autoevaluacion::STATUS_ACTIVO)
		{
			throw new Exception_Saes('Autoevaluación no activa');
		}

		$this->oAutoevaluacion->force_finish();

		// Actividades
		//if ($this->oAutoevaluacion->show_ficha_actividades())
		{
			//$this->redirect('/autoevaluacion/actividades');
		}

		$this->redirect('/autoevaluaciones');
	}

	private function set_template()
	{
		$theme = Theme::instance();
		$theme->template->body_class = 'fixed';

		$new_header = Theme_View::factory('frontend/mejora/header', array(
				'header' => $theme->template->header,
		));

		$theme->template->header = $new_header;
		$theme->template->header->response = $this->oAutoevaluacion->result();

		$theme->template->sidebar = Theme_View::factory('frontend/mejora/sidebar', $this->get_sidebar_data());
	}

	private function check_autoevaluacion()
	{
		$this->oAutoevaluacion = $oAutoevaluacion = $this->oAcreditado->oAutoevaluacionActiva();

		if (!$oAutoevaluacion->loaded())
		{
			$this->redirect('/eventos');
		}

		$this->tipo = strtolower($this->oAcreditado->tipo_institucion());
	}

	private function set_template_data()
	{
		$theme = Theme::instance();

		$theme->js('default', 'smooth', '/saes_lte/js/plugins/smooth-scroll.js');
		$theme->js('default', 'jquery_form', '/saes_lte/js/plugins/jquery.form.min.js');
		$theme->css('page', 'autoevaluacion', '/saes_lte/css/autoevaluacion.css');
		$theme->js('page', 'autoevaluacion', '/saes_lte/js/autoevaluacion.js');
	}

	private function get_sidebar_data()
	{
		$data = DB::select('*')
			->select(DB::expr("concat(dime_codigo, '. ', dime_titulo) dime"))
			->select(DB::expr("concat(fact_codigo, '. ', fact_titulo) fact"))
			->select(DB::expr("concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta"))
			->select(DB::expr("concat(crit_codigo, '. ', coalesce(crit_descripcion)) crit"))
			->from('dimension')
			->join('factor')
			->using('dime_id')
			->join('estandar')
			->using('fact_id')
			->join('criterio')
			->using('esta_id')
			->join('autoevaluacion_detalle')
			->using('esta_id')
			->where('auto_id', '=', $this->oAutoevaluacion->auto_id)
			->execute()
			->as_array();

		$indicadores = array(
			'dime',
			'fact',
			'esta_codigo',
			'crit',
		);

		//debug($data);

		$tree = new TreeArray($data, $indicadores);

		$aDimension = $tree->get();

		return get_defined_vars();
	}

	public function after()
	{
		if ($this->auto_render)
		{
			Theme::instance()
				->template
				->header
				->title = $this->title;
		}

		parent::after();
	}

	private function _context()
	{
		$oAcreditado = $this->oAcreditado;

		$context = [];

		$oEstados = $oAcreditado->get_estados();
		foreach ($oEstados as $es)
		{
			$context['estados'][] = [
				'display' => $es->esta_evidencia,
				'id' => $es->esta_id
			];
		}

		$oDimensiones = $oAcreditado->get_dimensiones();
		foreach ($oDimensiones as $dm)
		{
			$context['dimensiones'][] = [
				'display' => $dm->dime_titulo,
				'id' => $dm->dime_id,
				'descripcion' => $dm->dime_descripcion,
			];
		}

		$oFactores = $oAcreditado->get_factores();
		foreach ($oFactores as $ft)
		{
			$context['factores'][] = [
				'display' => $ft->fact_titulo,
				'id' => $ft->fact_id,
				'dime_id' => $ft->dime_id,
				'descripcion' => $ft->fact_descripcion,
			];
		}

		$oEstandares = $oAcreditado->get_estandares();
		foreach ($oEstandares as $st)
		{
			$context['estandares'][] = [
				'display' => $st->esta_titulo,
				'id' => $st->esta_id,
				'fact_id' => $st->fact_id,
				'descripcion' => $st->esta_descripcion,
				'esta_codigo' => $st->esta_codigo,
			];
		}

		$oAutoDetalle = $this->oAutoevaluacion->aDetalle->find_all();
		foreach ($oAutoDetalle as $ad)
		{
			$context['auto_detalles'][] = [
				'esta_id' => $ad->esta_id,
				'aude_id' => $ad->aude_id ?: ''
			];
		}

		$oAutoCriterio = $this->oAutoevaluacion->get_auto_criterio();
		foreach ($oAutoCriterio as $ac)
		{
			$context['auto_criterios'][] = [
				'descripcion' => $ac->aucr_descripcion,
				'aucr_id' => $ac->aucr_id,
				'aude_id' => $ac->aude_id,
			];
		}

		return $context;
	}

	private function _initial()
	{
		$valida = 0;

		$oAutoDetalle = $this->oAutoevaluacion->aDetalle->find_all();
		//debug($oAutoDetalle); die;
		foreach ($oAutoDetalle as $ad)
		{
			$initial['mejo_detalles'][] = [
				'esta_id' => $ad->esta_id,
				'mede_id' => $ad->aude_id ?: '',
				//'mede_estado' => $ad->aude_valoracion ?: '',
				'mede_nivel' => $ad->aude_pregunta1 ?: '',
				'mede_actividades' => $ad->aude_pregunta2 ?: '',
				'mede_impacto' => $ad->aude_pregunta3 ?: ''
			];
			$valida = 1;
		}

		if ($valida == 0)
		{
			$initial = new stdClass();
		}

		return $initial;
	}

}
