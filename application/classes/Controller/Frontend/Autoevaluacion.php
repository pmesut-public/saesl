<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Frontend_Autoevaluacion extends Controller_Frontend {

	private $title = 'Proceso de Autoevaluación';
	private $status = 'OK';
	private $msg = '';

	private $editable;
    private $ficha = Model_Ficha::FICHA_AUTOEVALUACION;

	/**
	 *
	 * @var Model_Autoevaluacion
	 */
	private $oAutoevaluacion;
	private $tipo;

	public function before()
	{
		parent::before();

		$esta = $this->check_autoevaluacion();

		$this->set_template_data();
	}

	public function action_new()
    {
        $view = Theme_View::factory('frontend/eventos/list', compact('aAcreditadoEvento'));

        Theme::instance()
            ->template
            ->content = $view;
    }

	public function action_calculator()
	{
		$context = $this->_context();

		$initial = $this->_initial();

		$view = Theme_View::factory('frontend/autoevaluacion/' . $this->tipo)
			->set('context', json_encode($context))
			->set('initial', json_encode($initial));

		$url = REACT_MEDIA . 'autoevaluacion.js';
		Theme::instance()
				->js('default', 'plupload', '/saes_lte/js/plugins/plupload/plupload.full.min.js')
				->extra('<script src="' . $url . '"></script>')
			->template
			->content = $view;
	}

	public function action_medio_new()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		$audeId = $this->oAutoevaluacion->get_aude_id($post['esta_id'])->aude_id;

		$oAutoMedio = ORM::factory('AutoevaluacionMedio')
			->set('esta_id', $post['esta_id']) // puede ser aude_id
			->set('aude_id', $audeId)
			->set('aume_fecha_reg', date('Y-m-d H:i:s'))
			->set('aume_estado', Model_AutoevaluacionMedio::STATUS_ACTIVO);

		if (!$oAutoMedio->save())
		{
			$this->status = 'DENIED';
			$this->msg = 'No se puedo crear nuevo Medio';
		}

		$result = [
			'aume_id' => $oAutoMedio->aume_id,
			'status' => $this->status,
			'msg' => $this->msg,
		];

		$this->response->body(json_encode($result));
	}

	public function action_medio_delete()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		$oAutoMedio = ORM::factory('AutoevaluacionMedio')
			->where('aume_id', '=', $post['aume_id'])
			->find();

		if (!$oAutoMedio->delete())
		{
			$this->status = 'DENIED';
			$this->msg = 'No se pudo eliminar Medio';
		}

		$result = ['status' => $this->status, 'msg' => $this->msg];

		$this->response->body(json_encode($result));
	}

	public function action_documento_new()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		$result = [
			'documentos' => $this->oAutoevaluacion->documento_new($post['documentos']),
			'status' => $this->status,
			'msg' => $this->msg,
		];

		$this->response->body(json_encode($result));
	}

	public function action_documento_batch_update()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		//debug($post);
		// Eliminamos
		if (count($post['delete']) > 0)
		{
			foreach ($post['delete'] as $ad)
			{
				$this->_documento_delete($ad['audo_id']);
			}
		}

		// Guardamos lo nuevo
		$documentos = array();
		if (count($post['new']) > 0)
		{
			$documentos = $this->oAutoevaluacion->documento_new($post['new']);
		}

		$result = [
			'documentos' => $documentos,
			'status' => $this->status,
			'msg' => $this->msg,
		];

		$this->response->body(json_encode($result));
	}

	private function _documento_delete($audo_id)
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$oAutoDocu = ORM::factory('AutoevaluacionDocumento')
			->where('audo_id', '=', $audo_id)
			->find();

		$oAutoDocu->delete();
	}

	private function _update()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$this->_autodetalle_update();

		$this->_automedio_update();

		$this->_documento_update();
	}

	public function action_save()
	{
		$this->auto_render = FALSE; 	// Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		$this->_update(); 	// Actualiza las dependencias.

		$this->oAutoevaluacion->update_acreditado_evento_ficha(
			Model_Ficha::FICHA_NO_GUARDADA, Model_Ficha::FICHA_AUTOEVALUACION
		);

		$result = ['status' => $this->status, 'msg' => $this->msg];

		$this->response->body(json_encode($result));
	}

	private function _automedio_update()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		if (@$post['auto_medio'] && count($post['auto_medio']) > 0)
		{
			foreach ($post['auto_medio'] as $am)
			{
				$oAutoMedio = ORM::factory('AutoevaluacionMedio')
					->where('aume_id', '=', $am['aume_id'])
					->find();

				$oAutoMedio->set('aume_descripcion', $am['descripcion'])
					->set('aume_observacion', $am['observacion'])
					->set('aume_consideracion', $am['consideracion'])
					->set('aume_estado', $am['estado'])
					->save();
			}
		}
	}

	/**
	 * Maneja la finalización la Ficha de Evaluación.
	 */
	public function action_finish()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$this->_update(); 			// Actualiza las relaciones de la Ficha AutoEvaluación.

		/* Actualiza la Ficha Autoevaluación en relación al ACEV_ID. */
		
		$this->oAutoevaluacion->update_acreditado_evento_ficha(
			Model_Ficha::FICHA_GUARDADA, Model_Ficha::FICHA_AUTOEVALUACION
		);

		$this->oAutoevaluacion->finish();
		$this->redirect('/autoevaluaciones'); 
		
		//$this->redirect('/formatos');
	/*
		$this->oAutoevaluacion->finish(); 								// Cierra la Autoevaluación.
		$this->redirect('/autoevaluaciones'); 							// Redirect
		$isClosed = $this->oAutoevaluacion->autoevaluacion_cerrada(); 	// Verifica si la autoevaluación esta cerrada.

		$result = [
			'status' => 'Ok',
			'msg'    => 'Ficha de Evaluación completa. Cierra la Autoevaluación en la vista de Formatos'
		];

		$this->response->body(json_encode($result));
	*/
	}

	private function _autodetalle_update()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		//debug($post['auto_detalles']);

		if (@$post['auto_detalles'] && count($post['auto_detalles']) > 0)
		{
			foreach ($post['auto_detalles'] as $ad)
			{
				$oAutoDetalle = ORM::factory('AutoevaluacionDetalle')
					->where('aude_id', '=', $ad['aude_id'])
					->where('esta_id', '=', $ad['esta_id'])
					->find();

				$oAutoDetalle->set('indi_estado', $ad['aude_estado'])
					->save();
			}
		}
	}

	private function _documento_update()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		if (@$post['auto_documentos'] && count($post['auto_documentos']) > 0)
		{
			foreach ($post['auto_documentos'] as $ad)
			{
				$oAutoDocu = ORM::factory('AutoevaluacionDocumento')
					->where('audo_id', '=', $ad['audo_id'])
					->find();

				$estado = Model_AutoevaluacionDocumento::$statuses[$ad['audo_estado']];
				$oAutoDocu->set('audo_estado', $estado)
					->save();
			}
		}
	}

	// @ajax
	public function action_upload_concepto()
	{
		$this->oAcreditado;
		$this->oAutoevaluacion;

		$conc_id = $this->request->post('conc_id');

		if ($this->request->post_max_size_exceeded())
		{
			throw new Exception_Saes('File too big');
		}

		$filename = ORM::factory('ConceptoDetalle')
			->upload_concepto($this->oAutoevaluacion, $conc_id, $_FILES['file']);

		if ($filename === FALSE)
		{
			throw new Exception_Saes('File could not be saved');
		}

		//debug($filename);
		$this->response->body($filename);
	}

	public function action_save_session()
	{
		$oAutoevaluacion = $this->oAutoevaluacion;

		$oAutoevaluacion->auto_fecha_fin = date('Y-m-d');

		// Manually add modification date (It won't update automatically if auto_fecha_fin has not been changed)
		$oAutoevaluacion->auto_fecha_act = date('Y-m-d H:i:s');

		$oAutoevaluacion->save();

		$this->redirect('/autoevaluaciones');
	}

	public function action_cancel()
	{
		$oAutoevaluacion = $this->oAutoevaluacion;
		
		$oAutoevaluacion->limpiar_autoevaluacion();

		Session::instance()->set('warning', 'La Autoevaluación fue cancelada.');

		$this->redirect('/autoevaluacion/calculator');
	}

	public function action_force_finish()
	{
		if ($this->oAcreditado->acre_test != Model_Acreditado::TIPO_TEST)
			die('Sólo se puede forzar en usuarios de prueba');

		//if ( ! Auth::instance()->logged_in('admin'))
		//die('Not allowed');

		if ($this->oAutoevaluacion->auto_estado <> Model_Autoevaluacion::STATUS_ACTIVO)
		{
			throw new Exception_Saes('Autoevaluación no activa');
		}

		$this->oAutoevaluacion->force_finish();

		// Actividades
		//if ($this->oAutoevaluacion->show_ficha_actividades())
		{
			//$this->redirect('/autoevaluacion/actividades');
		}

		$this->redirect('/autoevaluaciones');
	}

	private function set_template()
	{
		$theme = Theme::instance();
		$theme->template->body_class = 'fixed';

		$new_header = Theme_View::factory('frontend/autoevaluacion/header', array(
				'header' => $theme->template->header,
		));

		$theme->template->header = $new_header;
		$theme->template->header->response = $this->oAutoevaluacion->result();

		$theme->template->sidebar = Theme_View::factory('frontend/autoevaluacion/sidebar', $this->get_sidebar_data());
	}

	private function check_autoevaluacion()
	{
		$this->oAutoevaluacion = $oAutoevaluacion = $this->oAcreditado->oAutoevaluacionActiva();

		if (!$oAutoevaluacion->loaded())
		{
			$this->redirect('/autoevaluaciones');
		}

		$this->tipo = strtolower($this->oAcreditado->tipo_institucion());
	}

	private function set_template_data()
	{
		$theme = Theme::instance();

		$theme->js('default', 'smooth', '/saes_lte/js/plugins/smooth-scroll.js');
		$theme->js('default', 'jquery_form', '/saes_lte/js/plugins/jquery.form.min.js');
		$theme->css('page', 'autoevaluacion', '/saes_lte/css/autoevaluacion.css');
		$theme->css('page', 'calendar', '/saes_lte/css/calendar.css');
		$theme->js('page', 'autoevaluacion', '/saes_lte/js/autoevaluacion.js');
	}

	private function get_sidebar_data()
	{
		$data = DB::select('*')
			->select(DB::expr("concat(dime_codigo, '. ', dime_titulo) dime"))
			->select(DB::expr("concat(fact_codigo, '. ', fact_titulo) fact"))
			->select(DB::expr("concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta"))
			->select(DB::expr("concat(crit_codigo, '. ', coalesce(crit_descripcion)) crit"))
			->from('dimension')
			->join('factor')
			->using('dime_id')
			->join('estandar')
			->using('fact_id')
			->join('criterio')
			->using('esta_id')
			->join('autoevaluacion_detalle')
			->using('esta_id')
			->where('auto_id', '=', $this->oAutoevaluacion->auto_id)
			->execute()
			->as_array();

		$indicadores = array(
			'dime',
			'fact',
			'esta_codigo',
			'crit',
		);

		//debug($data);

		$tree = new TreeArray($data, $indicadores);

		$aDimension = $tree->get();

		return get_defined_vars();
	}

	public function after()
	{
		if ($this->auto_render)
		{
			Theme::instance()
				->template
				->header
				->title = $this->title;
		}

		parent::after();
	}

	private function _context()
	{
		$oAcreditado = $this->oAcreditado;
		$context = [];

		$this->editable = $this->oAutoevaluacion->oAcreditadoEvento->is_editable($this->ficha);
        $context['editable'] = $this->editable;
		$oRole = $oAcreditado->oUser->oRole;
		$context['role'] = $oRole;	
		$medio_estado = [];

		switch($oRole->id){
			case  Model_Role::ROLE_REGULAR:
				$medio_estado = Model_Medio::$estadosRegular;
				break;
			case  Model_Role::ROLE_OPERADOR:
				$medio_estado = Model_Medio::$estadosOperador;
				break;
		}

		/*foreach ($medio_estado as $key => $value)
		{
			$context['medio_estado'][] = [
				'id' => $key,
				'display' => $value,
			];
		}*/

		$oEstadoIdicadores = (object) Model_Estandar::$estado_idicadores;
		foreach ($oEstadoIdicadores as $key => $value)
		{
			$context['estado'][] = [
				'id' => $key,
				'display' => $value,
			];

			$context['medio_estado'][] = [
				'id' => $key,
				'display' => $value,
			];
		}

		$oEstadosDocumento = (object) Model_Documento::$estados;
		foreach ($oEstadosDocumento as $key => $value) {
			$context['estados'][] = [
				'id' => $key,
				'display' => $value
			];
		}

		$sub_estados = (object) Model_Documento::$sub_estados;
		foreach ($sub_estados as $key => $value) {
			$context['sub_estados'][] = [
				'parentId' => $value['parentId'],
				'id' => $value['id'],
				'display' => $value['display'],
				'color' => $value['color']
			];
		}

		$oDimensiones = $oAcreditado->get_dimensiones();
		foreach ($oDimensiones as $dm)
		{
			$context['dimensiones'][] = [
				'display' => $dm->dime_descripcion,
				'id' => $dm->dime_id,
				'title' => $dm->dime_titulo,
			];
		}

		$oFactores = $oAcreditado->get_factores();
		foreach ($oFactores as $ft)
		{
			$context['factores'][] = [
				'display' => $ft->fact_descripcion,
				'id' => $ft->fact_id,
				'dime_id' => $ft->dime_id,
				'title' => $ft->fact_titulo,
			];
		}

		$oEstandares = $oAcreditado->get_estandares();
		foreach ($oEstandares as $st)
		{
			$context['estandares'][] = [
				'display' => $st->esta_descripcion,
				'id' => $st->esta_id,
				'fact_id' => $st->fact_id,
				'esta_codigo' => $st->esta_codigo,
				'title' => $st->esta_titulo,
			];
		}

		$oMedios = $oAcreditado->get_medios();
		foreach ($oMedios as $st)
		{
			$context['medios'][] = [
				'display' => $st->medi_descripcion,
				'id' => $st->medi_id,
				'esta_id' => $st->esta_id,
				'title' => $st->medi_consideracion,
			];
		}

		//debug($context);
		return $context;
	}

	private function _initial()
	{
		$valida = 0;

		$initial = [];

		$initial['editable'] = $this->oAutoevaluacion->oAcreditadoEvento->is_editable($this->ficha);

		$oAutoDetalle = $this->oAutoevaluacion->aDetalle->find_all();
		foreach ($oAutoDetalle as $ad)
		{
			$initial['auto_detalles'][] = [
				'esta_id' => $ad->esta_id,
				'aude_id' => $ad->aude_id ?: '',
				'aude_estado' => $ad->indi_estado ?: '',
			];
			$valida = 1;
		}

		$oAutoMedio = $this->oAutoevaluacion->get_auto_medio();
		foreach ($oAutoMedio as $am)
		{
			$initial['auto_medio'][] = [
				'medi_id' => $am->medi_id,
				'aude_id' => $am->aude_id,
				'aume_id' => $am->aume_id,
				'descripcion' => $am->aume_descripcion,
				'observacion' => $am->aume_observacion,
				'consideracion' => $am->aume_consideracion,
				'estado' => $am->aume_estado,
				'definido' => FALSE,
			];
			$valida = 1;
		}

		$oAutoDocumento = $this->oAutoevaluacion->get_auto_documento();
		foreach ($oAutoDocumento as $ad)
		{
			$initial['auto_documentos'][] = [
				'aume_id' => $ad->aume_id,
				'docu_id' => $ad->docu_id,
				'audo_id' => $ad->audo_id,
				'audo_estado' => $ad->oDocumento->estados,
				'audo_subestado' => $ad->oDocumento->sub_estado,
				'fecha_inicio' => $ad->oDocumento->fecha_inicio,
				'fecha_fin' => $ad->oDocumento->fecha_fin,
				'da' => $ad->oDocumento->da,
				'path' => $ad->path,
				'titulo' => $ad->titulo,
				'definido' => FALSE,
			];
			$valida = 1;
		}
		
		if ($valida == 0)
		{
			$initial = new stdClass();
		}

		//debug($initial);
		return $initial;
	}

	public function action_actividad_new()
	{
		$this->auto_render = FALSE;
		$auac_id = NULL;
		$act_cod = NULL;
		$numeracion = 1;

		$post = json_decode($this->request->body(), TRUE);

		$oAutoDetalle = ORM::factory('AutoevaluacionDetalle', ['aude_id' => $post['aude_id']]);

		$oUltimaActividad = $oAutoDetalle->get_auto_last_actividad();

		if ($oUltimaActividad->loaded())
			$numeracion = $oUltimaActividad->auac_numeracion + 1;

		$oActividad = ORM::factory('AutoevaluacionActividad')
			->set('auac_numeracion', $numeracion)
			->set('aude_id', $oAutoDetalle->aude_id)
			->set('auac_estado', Model_AutoevaluacionActividad::STATUS_ACTIVO)
			->set('auac_fecha_reg', date('Y-m-d H:i:s'));

		if (!$oActividad->save())
		{
			$this->status = 'DENIED';
			$this->msg = 'No se puedo crear la actividad';
		}

		$result = [
			'auac_id' => $oActividad->auac_id,
			'act_cod' => $oActividad->auac_numeracion,
			'status' => $this->status,
			'msg' => $this->msg,
		];

		$this->response->body(json_encode($result));
	}

	public function action_actividad_delete()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		$oAutoActividad = ORM::factory('AutoevaluacionActividad', ['auac_id' => $post['auac_id']]);

		if (!$oAutoActividad->delete())
		{
			$this->status = 'DENIED';
			$this->msg = 'No se pudo eliminar la Actividad';
		}

		$result = [
			'status' => $this->status,
			'msg' => $this->msg
		];

		$this->response->body(json_encode($result));
	}

	public function action_actividad_update_vinculacion()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$post = json_decode($this->request->body(), TRUE);

		foreach ($post['new_estandares'] as $estandar)
		{
			// Comprueba si ya existe una relación
			$queryExisteAutoDetaAct = DB::select()
				->from('autoevaluacion_detalle_actividad')
				->where('aude_id', '=', $estandar)
				->and_where('auac_id', '=', $post['auac_id'])
				->execute();

			if ($queryExisteAutoDetaAct->count() == 0)
			{
				//Se crea la nueva relación detalle-actividad
				$oAutoDetalleActividad = ORM::factory('AutoevaluacionDetalleActividad')
					->set('aude_id', $estandar)
					->set('auac_id', $post['auac_id']);

				if (!$oAutoDetalleActividad->save())
				{
					$this->status = 'DENIED';
					$this->msg = 'No se pudieron vincular algunos estándares. Vuelve a intentarlo';
				}
			}
		}

		foreach ($post['delete_estandares'] as $estandar)
		{
			$queryDelete = DB::delete('autoevaluacion_detalle_actividad')
				->where('aude_id', '=', $estandar)
				->and_where('auac_id', '=', $post['auac_id']);

			if (!$queryDelete->execute())
			{
				$this->status = 'DENIED';
				$this->msg .= 'No se pudieron eliminar algunas relaciones con estándares. Vuelve a intentarlo';
			}
		}

		$result = [
			'status' => $this->status,
			'msg' => $this->msg
		];

		$this->response->body(json_encode($result));
	}

	public function action_medicion()
	{
		$this->auto_render = FALSE; // Deshabilita theme

		$oAcreditado = $this->oAcreditado;

		$context = [];
		$initial = [];

		$oDimensiones = $oAcreditado->get_dimensiones();
		foreach ($oDimensiones as $dm)
		{
			$context['dimensiones'][] = [
				'display' => $dm->dime_titulo,
				'id' => $dm->dime_id,
				'descripcion' => $dm->dime_descripcion,
			];
		}

		$oFactores = $oAcreditado->get_factores();
		foreach ($oFactores as $ft)
		{
			$context['factores'][] = [
				'display' => $ft->fact_titulo,
				'id' => $ft->fact_id,
				'dime_id' => $ft->dime_id,
				'descripcion' => $ft->fact_descripcion,
			];
		}

		$oEstandares = $oAcreditado->get_estandares();
		foreach ($oEstandares as $st)
		{
			$context['estandares'][] = [
				'display' => $st->esta_titulo,
				'id' => $st->esta_id,
				'fact_id' => $st->fact_id,
				'descripcion' => $st->esta_descripcion,
				'esta_codigo' => $st->esta_codigo,
			];
		}

		$oAutoDetalle = $this->oAutoevaluacion->aDetalle->find_all();
		foreach ($oAutoDetalle as $ad)
		{
			$context['auto_detalles'][] = [
				'esta_id' => $ad->esta_id,
				'aude_id' => $ad->aude_id ?: ''
			];
		}

		$aAutoActividadRelacionada = $this->oAutoevaluacion->get_auto_actividades_relacionadas();

		foreach ($aAutoActividadRelacionada as $ar)
		{
			$initial['auto_detalle_actividad'][] = [
				'auac_id' => $ar['auac_id'],
				'aude_id' => $ar['aude_id'],
				'nivel_factor' => $ar['auda_importancia_factor'],
				'nivel_avance' => $ar['auda_avance'],
			];
		}

		$aAutoActividad = $this->oAutoevaluacion->get_auto_actividad();

		foreach ($aAutoActividad as $aa)
		{
			$initial['auto_actividades'][] = [
				'auac_id' => $aa->auac_id,
				'act_cod' => $aa->auac_numeracion,
				'aude_id' => $aa->aude_id,
				'estandares' => $aa->get_estandares(),
				'nivel_factor' => $aa->auac_importancia_factor,
				'nivel_avance' => $aa->auac_avance,
				'descripcion' => $aa->auac_titulo
			];
		}

		$result = array(
			'__CONTEXT__' => $context,
			'__INITIAL_STATE__' => $initial
		);

		$this->response->body(json_encode($result));
	}

	public function action_formatos()
	{
		$this->auto_render = FALSE; // Deshabilita theme
		
		$result = [
			'status' => $this->status,
			'msg' => $this->msg,
			'__INITIAL_STATE__' => $this->_initial_formatos()
		];

		$this->response->body(json_encode($result));
	}

	private function _initial_formatos()
	{
		$valida = 0;

		$oAutoFormA = $this->oAutoevaluacion->get_auto_formatosA();
		foreach ($oAutoFormA as $fa)
		{
			$initial['lic_a'][] = [
				'for_id' => $fa->form_id,
				'display' => $fa->form_nombre,
				'estado' => $fa->aufo_estado,
				'docu_id' => $fa->docu_id,
				'path' => $fa->docu_path,
				'file_name' => $fa->docu_titulo,
				'fecha_inicio' => $fa->oDocumento->fecha_inicio,
				'fecha_fin' => $fa->oDocumento->fecha_fin,
				'da' => $fa->oDocumento->da
			];
			$valida = 1;
		}

		$oAutoFormC = $this->oAutoevaluacion->get_auto_formatosC();
		foreach ($oAutoFormC as $fc)
		{
			$initial['lic_c'][] = [
				'for_id' => $fc->form_id,
				'display' => $fc->form_nombre,
				'estado' => $fc->aufo_estado,
				'docu_id' => $fc->docu_id,
				'path' => $fc->docu_path,
				'file_name' => $fc->docu_titulo,
				'fecha_inicio' => $fc->oDocumento->fecha_inicio,
				'fecha_fin' => $fc->oDocumento->fecha_fin,
				'da' => $fc->oDocumento->da
			];
			$valida = 1;
		}

		$oAutoCondicion = $this->oAutoevaluacion->get_auto_condicion();
		foreach ($oAutoCondicion as $ac)
		{
			$initial['cond'][] = [
				'cond_id' => $ac->dime_id,
				'display' => $ac->dime_descripcion,
				'titulo' => $ac->dime_titulo,
				'docu_id' => $ac->docu_id,
				'estado' => $ac->auco_estado,
				'path' => $ac->docu_path,
				'file_name' => $ac->docu_titulo,
				'fecha_inicio' => $ac->oDocumento->fecha_inicio,
				'fecha_fin' => $ac->oDocumento->fecha_fin,
				'da' => $ac->oDocumento->da
			];
			$valida = 1;
		}

		if ($valida == 0)
		{
			$initial = new stdClass();
		}

		//debug($initial);
		return $initial;
	}

	/**
	 * Valida sí en la autoevaluación hay documentos sueltos sin usarse.
	 *
	 * @return json
	 */
	public function action_validar_documentos(){

		$this->auto_render = FALSE;

//		$oDocumentosEvaluacion = ORM::factory('Documento')
//									->where('acre_id', '=', $this->oAcreditado->acre_id)
//									->where('docu_estado', '=', 'activo')
//									->find_all()
//									->as_json_array('docu_id');
//
//		$aDocumentosUsados = QueryBuilder::factory('fichas/documentos_autoevaluacion.sql', [
//			':autoevaluacion_id' => $this->oAutoevaluacion->auto_id,
//		])->dict('docu_id')->get();
//
//		$aDocumentosSueltos = array_diff(array_keys($oDocumentosEvaluacion), array_keys($aDocumentosUsados));

		$documentos_sueltos = -1;
                $autoDocuemnto=[];
                if ($this->request->method() == 'POST'){
                    $autoDocuemnto = DB::select(DB::expr("COUNT(documento.docu_id) as count_docu"))
                            ->from('documento')
                            ->join(DB::expr('autoevaluacion_documento'), 'left')->using('docu_id')
                            ->join(DB::expr('autoevaluacion_medio'), 'left')->using('aume_id')
                            ->join(DB::expr('autoevaluacion_detalle'), 'left')->using('aude_id')
                            ->join(DB::expr('autoevaluacion_formato'), 'left')->on('autoevaluacion_formato.docu_id', '=', 'documento.docu_id')
                            ->join(DB::expr('autoevaluacion_condicion'), 'left')->on('autoevaluacion_condicion.docu_id', '=', 'documento.docu_id')
                            ->where('aufo_id', '=', NULL)
                            ->where('auco_id', '=', NULL)
                            ->where('autoevaluacion_detalle.auto_id', '=', NULL)
                            ->where('documento.docu_estado', '=', Model_Autoevaluacion::STATUS_ACTIVO)
                            ->where('documento.acre_id', '=', $this->oAcreditado->acre_id)
                            ->execute()->current();

                    $documentos_sueltos = (!@$autoDocuemnto)?0:$autoDocuemnto['count_docu'];
                }
		$response = [
			'valido' => $documentos_sueltos == 0 ? Model_Autoevaluacion::AUTOEVALUACION_VALIDA : Model_Autoevaluacion::AUTOEVALUACION_INVALIDA,
			'msg'    => 'Existen '. $documentos_sueltos .' documento(s) sin usarse en la autoevaluación',
			'status' => 'OK',
		];

		$this->response->body(json_encode($response));
	}

}
