<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Frontend_Event extends Controller_Frontend {
	
	/**
	 *
	 * @var  Model_AcreditadoEvento
	 */
	private $oEventoPendiente;
	
	/**
	 *
	 * @var  Model_AcreditadoEvento
	 */
	private $oEventoActual;
	
	public function before()
	{
		parent::before();
		
		$this->oEventoPendiente = $this->oAcreditado->oEventoPendiente();
		$this->oEventoActual = $this->oAcreditado->oEventoActual();
	}
	
	public function action_index()
	{
		extract($this->prepare_data());
		
		$view = Ficha::factory($oFicha)
			->get('save');
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_save()
	{
		extract($this->prepare_data());
		
		if ($this->request->method() == 'POST')
		{
			$post = $this->request->post();
			$save_only = $post['save_only'];
			unset($post['save_only']);
			
			$oEventoPendiente->tifi_id = $this->request->post('tifi_id');
			$oEventoPendiente->save();

			$oFicha->acfi_data = json_encode($post);
			$oFicha->save();
			
			Session::instance()->set('info', 'Ficha guardada');
			
			if ($save_only)
			{
				$this->redirect('/eventos');
			}
		}
		
		$view = Ficha::factory($oFicha)
			->get('finish');
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_finish()
	{
		extract($this->prepare_data());
		
		if ($this->request->method() == 'POST')
		{
			$filename = $oEventoPendiente->acev_id.'-'.$oFicha->oEventoFicha->evfi_nombre;
				
			if ($oFicha->save_file('acfi_documento', $filename, $_FILES['file']))
			{
				$oFicha->save();
				//$oEventoPendiente->acev_estado = Model_AcreditadoEvento::ESTADO_ACTUAL;
				//$oEventoPendiente->save();
				$this->create_auto();
			}

			Session::instance()->set('error', 'No se pudo guardar el documento. Inténtelo nuevamente');
		}
		
		$view = Ficha::factory($oFicha)
			->get('finish');
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	public function action_discard()
	{
		// Evento pendiente
		
		if ($this->request->method() == 'GET')
		{
			$oEventoPendiente = $this->oEventoPendiente;
		
			if ( ! $oEventoPendiente->loaded())
			{
				throw new Exception_Saes('Model not found');
			}

			$oEventoPendiente->discard();

			$this->redirect('/autoevaluaciones');
		}
		
		// Evento actual
		
		if ($this->request->method() == 'POST')
		{
			$oEventoActual = $this->oEventoActual;

			if ( ! $oEventoActual->loaded())
			{
				throw new Exception_Saes('Model not found');
			}

			// Discard only one or all ???

			//$oAssessment = $this->oAcreditado->oAutoevaluacionActiva();
			$aAssessment = $oEventoActual->aAutoevaluacion
				->where('auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
				->find_all();

			//if ($oAssessment->loaded())
			foreach ($aAssessment as $oAssessment)
			{
				$oAssessment->discard();
			}

			$oEventoActual->discard();

			$this->redirect('/autoevaluaciones');
		}
	}
	
	/*public function action_discard_auto()
	{
		$oEventoActual = $this->oEventoActual;
		
		if ( ! $oEventoActual->loaded())
		{
			throw new Exception_Saes('Event not found');
		}
		
		$oAssessment = $oEventoActual->aAutoevaluacion
			->where('auto_id', '=', $this->request->param('id'))
			->where('auto_estado', '=', Model_Autoevaluacion::STATUS_CERRADO)
			->find();
		
		if ( ! $oAssessment->loaded())
		{
			throw new Exception_Saes('Auto not found');
		}
		
		$oAssessment->discard();
		
		$handler = new PlacseasProxy($oAssessment);
		$handler->remove();
		
		$this->redirect('/eventos');
	}*/
	
	/**
	 * 
	 * @throws Exception_Saes
	 */
	private function prepare_data()
	{
		$oEventoPendiente = $this->oEventoPendiente;
		
		if ( ! $oEventoPendiente->loaded())
			throw new Exception_Saes('Model not found');
		
		$oFicha = $oEventoPendiente->oFicha();
		
		// No ficha, just create the auto
		if ( ! $oFicha->loaded())
		{
			$this->create_auto();
		}
		
		return get_defined_vars();
	}
	
	private function create_auto()
	{
		$oEventoPendiente = $this->oEventoPendiente;
		
		$oEventoPendiente->acev_estado = Model_AcreditadoEvento::ESTADO_ACTUAL;
		//debug($oEventoPendiente);
		$oEventoPendiente->save();
		
		Model_Autoevaluacion::create_new($oEventoPendiente);
		$this->redirect('/autoevaluacion/calculator');
	}
	
}
