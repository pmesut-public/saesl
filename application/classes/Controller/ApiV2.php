<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_ApiV2 extends Controller {

	use Middleware_TraitAjax;
	
	public $token = 'F5cYRLZF';

	public function before()
	{
		$this->handle_middleware_exceptions();
		
		parent::before();
	}
	
	public function set_middlewares()
	{
		$this->middleware('ApiToken');
	}
	
	protected function send_response($response)
	{
		$response = ['status' => TRUE] + $response;
		
		die(json_encode($response));
	}
	
	protected function send_error($e)
	{
		die(json_encode([
			'status' => FALSE,
			'message' => $e->getMessage(),
			'code' => $e->getCode(),
		]));
	}
	
}
