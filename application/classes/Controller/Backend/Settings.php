<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Settings extends Controller_Backend {

	protected $theme = 'adminlte';
	
	protected $title = 'Configuración';
	
	public function action_index()
	{
		if ($this->request->method() == 'POST')
		{
			extract($this->request->post());
			
			if ($this->_config->site_shortname != $site_shortname)
			{
				$this->_config->site_shortname = $site_shortname;
			}
			if ($this->_config->site_title != $site_title)
			{
				$this->_config->site_title = $site_title;
			}
			if ($this->_config->site_url != $site_url)
			{
				$this->_config->site_url = $site_url;
			}
			if ($this->_config->admin_mail != $admin_mail)
			{
				$this->_config->admin_mail = $admin_mail;
			}
			if ($this->_config->mb_file_size != $mb_file_size)
			{
				$this->_config->mb_file_size = $mb_file_size;
			}
			if ($this->_config->noreply_mail != $noreply_mail)
			{
				$this->_config->noreply_mail = $noreply_mail;
			}
			/*if ($admin_password)
			{
				$this->_config->admin_password = $admin_password;
			}*/
			
			//$this->_message = 'Data guardada correctamente';
			Session::instance()->set('info', 'Data guardada correctamente');
		}
		
		$view = Theme_View::factory('backend/settings')
			->set('config', $this->_config)
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title);
	}
	
}
