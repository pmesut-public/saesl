<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Controller_Backend_Medio extends Controller_Backend {

	protected $theme = 'adminlte';
	protected $title = 'Medios';

	/**
	 * Invoca a la function publica action_list
	 */
	public function action_index()
	{
		$this->action_list();
	}

	/**
	 * Muestra todas las medios
	 */
	public function action_list()
	{
		$query = $this->_get_query()
			->order_by('m.medi_id');;

		$labels = array(
			'esta_id' => 'Indicador',
			'obac_id' => 'Objeto de Licenciamiento',
		);

		$table = Table::factory('Medio')
				->query($query)
				->labels($labels)
				->columns(array('obac_id', 'esta_id', 'medi_descripcion', 'medi_estado'))
				->bulk_actions('new')
				->actions(array('edit', 'delete'))
				->options('medi_estado', Model_Saes::$estados)
				->options('esta_id', Model_Estandar::get_all())
				->options('obac_id', Model_ObjetoAcreditacion::get_all())
				->filter('obac_id')
				->filter('medi_estado')
				->sizes(array(
					'medi_descripcion' => Table::SIZE_XL,
					'medi_estado' => Table::SIZE_XS,
				))->build();

		$subtitle = '';

		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}

	/**
	 * Miga de pan de la vista medio
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/medio');
	}

	protected function _get_query()
	{
		return DB::select(DB::expr('SQL_CALC_FOUND_ROWS m.*'), 'obac_id', 'esta_id')
				->select(array(DB::expr("concat(e.esta_codigo, '. ', e.esta_titulo)"), 'indicador'))
				->from(array('medio', 'm'))
				->join(array('estandar', 'e'))
				->using('esta_id')
				->join(array('factor', 'b'))
				->using('fact_id')
				->join(DB::expr('dimension a'))
				->using('dime_id')
				->join(DB::expr('objeto_acreditacion o'))
				->using('obac_id');
	}

	/**
	 * Invoca a la funcion action_edit
	 */
	public function action_new()
	{
		$this->action_edit();
	}

	/**
	 * Crea o edita un medio
	 */
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$query = $this->_get_query()
			->where('medi_id', '=', $id);
		
		$columns = array(
			'medi_descripcion',
			'obac_id',
			'esta_id',
			'medi_estado',
			'medi_observacion',
			'medi_consideracion',
		);

		$obac_id = DB::select('a.obac_id')
			->from(DB::expr('medio m'))
			->join(array('estandar', 'e'))
			->using('esta_id')
			->join(array('factor', 'b'))
			->using('fact_id')
			->join(DB::expr('dimension a'))
			->using('dime_id')
			->join(DB::expr('objeto_acreditacion t'))
			->using('obac_id')
			->where('m.medi_id', '=', $id)
			->execute()
			->get('obac_id');

		$esta_id = DB::select('esta_id')
			->from('medio')
			->where('medi_id', '=', $id)
			->execute()
			->get('esta_id');

		$aEstandar = Model_ObjetoAcreditacion::get_estandares($obac_id);

		$form = AdminForm::factory('Medio')
			->query($query)
			->types('medi_descripcion', 'textarea')
			->columns($columns)
			->options('medi_estado', Model_Saes::$estados)
			->options('obac_id', Model_ObjetoAcreditacion::get_all())
			->options('esta_id', $aEstandar)
			->custom('obac_id', array(
				'class' => 'ajax select2',
				'data-path' => '/ajax/get_tiin_estandares/',
				'data-target' => '#esta_id',
			))
			->custom('esta_id', array(
				'data-selected' => $esta_id,
                                'class' => 'select2',
			))
			->build();

		$subtitle = $id ? 'Editar #' . $id : 'Nuevo';

		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}

	/**
	 * Elimina un medio
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');

		$oMedio = ORM::factory('Medio', $id);
		$oMedio->medi_estado = Model_Saes::STATUS_ELIMINADO;
		$oMedio->save();

		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'Medio',
			':pk' => $oMedio->primary_key(),
			':id' => $oMedio->pk(),
			':mode' => 'deleted',
		)));

		Log::access($msg);

		$this->redirect($this->request->referrer());
	}

}
