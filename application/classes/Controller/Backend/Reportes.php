<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Controller_Backend_Reportes extends Controller_Backend {
	
	protected function _check_authorization()
	{
		if ( ! ACL::instance()->allowed('reportes'))
		{
			throw new HTTP_Exception_403('Thou shalt not pass');
		}
	}
	
}
