<?php 
defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Controller_Backend_Dimension extends Controller_Backend {

	protected $theme = 'adminlte';
	
	protected $title = 'Condiciones';
	
	/**
	 * Invoca a la function publica action_list
	 */
	public function action_index()
	{
		$this->action_list();
	}
	
	/**
	 * Muestra todas las dimensiones
	 */
	public function action_list()
	{
		$table = Table::factory('Dimension')
			->columns(array('obac_id', 'dime_codigo', 'dime_titulo','dime_informacion', 'dime_estado'))
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->options('dime_estado', Model_Saes::$estados)
			->options('obac_id', Model_ObjetoAcreditacion::get_all())
			->filter('obac_id')
			->filter('dime_estado')
			->search('dime_titulo')
			->sizes(array(
				'dime_codigo' => Table::SIZE_XS,
				'dime_titulo' => Table::SIZE_XL,
				'dime_informacion' => Table::SIZE_XL,
				'dime_estado' => Table::SIZE_XS,
			))->build();
		
		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	/**
	 * Miga de pan de la vista dimension
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/dimension');
	}
	
	/**
	 * Invoca a la funcion action_edit
	 */
	public function action_new()
	{
		$this->action_edit();
	}
	
	/**
	 * Crea o edita una dimension
	 */
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$form = AdminForm::factory('Dimension')
			->types('dime_descripcion', 'textarea')
			->types('dime_informacion','textarea')
			->options('dime_estado', Model_Saes::$estados)
			->options('obac_id', Model_ObjetoAcreditacion::get_all())
                        ->custom('obac_id', array(
                            'class' => 'select2'
                        ))
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'Nuevo';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Elimina una dimension
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oDimension = ORM::factory('Dimension', $id);
		$oDimension->dime_estado = Model_Saes::STATUS_ELIMINADO;
		$oDimension->save();
		
		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'Dimension',
			':pk' => $oDimension->primary_key(),
			':id' => $oDimension->pk(),
			':mode' => 'deleted',
		)));
		
		Log::access($msg);
		
		$this->redirect($this->request->referrer());
	}
}
