<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Controller_Backend_Estado extends Controller_Backend {

	protected $theme = 'adminlte';
	
	protected $title = 'Estados de la Mejora';
	
	/**
	 * Invoca a la function publica action_list
	 */
	public function action_index()
	{
		$this->action_list();
	}
	
	/**
	 * Muestra todas los estados
	 */
	public function action_list()
	{
		$table = Table::factory('Estado')
			->columns(array('esta_codigo', 'esta_titulo', 'esta_descripcion', 'esta_estado'))
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->options('esta_estado', Model_Saes::$estados)
			->filter('esta_estado')
			->search('esta_titulo')
			->sizes(array(
				'esta_codigo'       => Table::SIZE_XS,
				'esta_titulo'       => Table::SIZE_XS,
                                'esta_descripcion'  => Table::SIZE_XL,
				'esta_evidencia'    => Table::SIZE_XL,
				'esta_estado'       => Table::SIZE_XS,
			))->build();
		
		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	/**
	 * Miga de pan de la vista estado
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/estado');
	}
	
	/**
	 * Invoca a la funcion action_edit
	 */
	public function action_new()
	{
		$this->action_edit();
	}
	
	/**
	 * Crea o edita un estado
	 */
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$form = AdminForm::factory('Estado')
            ->types('esta_descripcion', 'textarea')
			->types('esta_evidencia', 'textarea')
			->options('esta_estado', Model_Saes::$estados)
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'Nuevo';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Elimina un estado
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oEstado = ORM::factory('Estado', $id);
		$oEstado->esta_estado = Model_Saes::STATUS_ELIMINADO;
		$oEstado->save();
		
		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'Estado',
			':pk' => $oEstado->primary_key(),
			':id' => $oEstado->pk(),
			':mode' => 'deleted',
		)));
		
		Log::access($msg);
		
		$this->redirect($this->request->referrer());
	}
}
