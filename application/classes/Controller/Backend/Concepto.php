<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Concepto extends Controller_Backend {

	protected $theme = 'adminlte';
	
	protected $title = 'Fuentes de verificación';
	
	/**
	 * Invoca la function action_list
	 */
	public function action_index()
	{
		$this->action_list();
	}
	
	/**
	 * Muestra todas las fuentes de verificacion
	 */
	public function action_list()
	{
		$labels = array('estandar' => 'Estándar', 'obac_id' => 'Tipo licenciamiento');
		
		$query = $this->_get_query();
		
		$table = Table::factory('Concepto')
			->query($query)
			->columns(array('obac_id', 'conc_codigo', 'conc_nombre', 'estandar', 'conc_estado'))
			->labels($labels)
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->options('conc_estado', Model_Saes::$estados)
			->options('obac_id', Model_ObjetoAcreditacion::get_tipos())
			->filter('obac_id')
			->filter('conc_estado')
			->search('conc_codigo')
			->search('conc_nombre')
			->sizes(array(
				'conc_codigo' => Table::SIZE_XS,
				'conc_nombre' => Table::SIZE_LG,
				'estandar'   => Table::SIZE_XL,
				'conc_estado' => Table::SIZE_XS,
				))
			->build();
		
		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	/**
	 * Miga de pan de la vista Fuentes de verificacion
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/concepto');
	}
	
	/**
	 * Consulta SQL
	 * @return type
	 */
	protected function _get_query()
	{
		return DB::select(DB::expr('SQL_CALC_FOUND_ROWS co.*'), 'obac_id')
			->select(DB::expr("concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) estandar"))
			->from(DB::expr('concepto co'))
			->join(DB::expr('estandar e'))
				->using('esta_id')
			->join(DB::expr('criterio c'))
				->using('crit_id')
			->join(DB::expr('factor f'))
				->using('fact_id')
			->join(DB::expr('dimension d'))
				->using('dime_id')
			;
	}
	
	/**
	 * Invoca a la function action_edit
	 */
	public function action_new()
	{
		$this->action_edit();
	}
	
	/**
	 * Crea o edita una fuente de verificacion (tener en cuenta si el valor es 1 es obligatorio)
	 */
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$query = $this->_get_query()
			->where('conc_id', '=', $id);
		
		$obac_id = DB::select('a.obac_id')
			->from(DB::expr("concepto co"))
			->join(DB::expr("estandar e"))
				->using('esta_id')
			->join(DB::expr("criterio c"))
				->using('crit_id')
			->join(DB::expr("factor b"))
				->using('fact_id')
			->join(DB::expr("dimension a"))
				->using('dime_id')
			->where('co.conc_id', '=', $id)
			->execute()
			->get('obac_id');
		
		$esta_id = DB::select('esta_id')
			->from('concepto')
			->join('estandar')
				->using('esta_id')
			->where('conc_id', '=', $id)
			->execute()
			->get('esta_id');
		
		$aEstandar = Model_ObjetoAcreditacion::get_estandares($obac_id);
		
		$form = AdminForm::factory('Concepto')
			->query($query)
			->labels(array('obac_id' => 'Tipo licenciamiento'))
			->columns(array('obac_id', 'esta_id', 'conc_codigo', 'conc_nombre', 'conc_obligatorio', 'conc_estado'))
			->options('conc_estado', Model_Saes::$estados)
			->options('obac_id', Model_ObjetoAcreditacion::get_tipos())
			->options('esta_id', $aEstandar)
			->custom('obac_id', array(
				'class' => 'ajax',
				'data-path' => '/ajax/get_obac_estandares/',
				'data-target' => '#esta_id',
			))
			->custom('esta_id', array(
				'data-selected' => $this->request->post('esta_id') ?: $esta_id,
			))
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'Nuevo';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form','subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Eliminar una fuente de verificacion
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oConcepto = ORM::factory('Concepto', $id);
		$oConcepto->conc_estado = Model_Saes::STATUS_ELIMINADO;
		$oConcepto->save();
		
		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'Concepto',
			':pk' => $oConcepto->primary_key(),
			':id' => $oConcepto->pk(),
			':mode' => 'deleted',
		)));
		
		Log::access($msg);
		
		$this->redirect($this->request->referrer());
	}
	
}
