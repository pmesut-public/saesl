<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Estandar extends Controller_Backend {

	protected $theme = 'adminlte';
	protected $title = 'Indicadores';

	/**
	 * Muestra el listado de estandares
	 */
	public function action_index()
	{
		$query = $this->_get_query();

		$labels = array(
			'fact_id' => 'Componente',
			'obac_id' => 'Objeto de Licenciamiento',
		);

		$table = Table::factory('Estandar')
			->query($query)
			->labels($labels)
			->columns(array('obac_id', 'fact_id', 'esta_codigo', 'esta_titulo', 'esta_estado'))
			->actions(array('edit', 'delete'))
			->bulk_actions(array('new', 'excel', 'print'))
			->options('esta_estado', Model_Estandar::$estados)
			->options('fact_id', Model_Factor::get_all2())
			->options('obac_id', Model_ObjetoAcreditacion::get_all())
			->filter('obac_id')
			->filter('esta_estado')
			->search('esta_titulo', 'Indicador')
			->search('fact_titulo', 'Componente')
			->sizes(array(
				'esta_codigo' => Table::SIZE_XS,
				'esta_titulo' => Table::SIZE_XL,
				'esta_estado' => Table::SIZE_XS,
			))
			->build();

		$subtitle = '';

		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}

	/**
	 * Miga de pan de la vista de estandares
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/estandar');
	}

	protected function _get_query()
	{
		return DB::select(DB::expr('SQL_CALC_FOUND_ROWS d.*'), 'obac_id', 'fact_id')
				->select(array(DB::expr("coalesce(esta_titulo, esta_descripcion)"), 'esta_title'))
				->select(array(DB::expr("concat(fact_codigo, '. ', coalesce(fact_titulo, fact_descripcion))"), 'factor'))
				->from(array('estandar', 'd'))
				->join(array('factor', 'b'))
				->using('fact_id')
				->join(DB::expr('dimension a'))
				->using('dime_id')
				->join(DB::expr('objeto_acreditacion o'))
				->using('obac_id')
		;
	}

	/**
	 * Crea un nuevo estandar
	 */
	public function action_new()
	{
		$this->action_edit();
	}

	/**
	 * Edita un estandar
	 */
	public function action_edit()
	{
		$id = $this->request->param('id');

		$query = $this->_get_query()
			->where('d.esta_id', '=', $id);

		$columns = array(
			'obac_id',
			'fact_id',
			'esta_codigo',
			'esta_titulo',
			'esta_descripcion',
			'moda_id',
			'esta_estado',
		);

		$tiin_id = DB::select('a.obac_id')
			->from(DB::expr("estandar e"))
			->join(array('factor', 'b'))
			->using('fact_id')
			->join(DB::expr('dimension a'))
			->using('dime_id')
			->join(DB::expr('objeto_acreditacion t'))
			->using('obac_id')
			->where('e.esta_id', '=', $id)
			->execute()
			->get('obac_id');

		$fact_id = DB::select('fact_id')
			->from('estandar')
			->where('esta_id', '=', $id)
			->execute()
			->get('fact_id');

		$aFactor = Model_ObjetoAcreditacion::get_factores($tiin_id);

		$form = AdminForm::factory('Estandar')
			->query($query)
			->labels(array('obac_id' => 'Objeto de Licenciamiento', 'fact_id' => 'Componente'))
			->columns($columns)
			->options('esta_estado', Model_Estandar::$estados)
			->options('moda_id', Model_Modalidad::get_all())
			->options('obac_id', Model_ObjetoAcreditacion::get_all())
			->options('fact_id', $aFactor)
			->custom('obac_id', array(
				'class' => 'ajax select2',
				'data-path' => '/ajax/get_tiin_factores/',
				'data-target' => '#fact_id',
			))
			->custom('fact_id', array(
				'data-selected' => $this->request->post('fact_id') ?: $fact_id,
                                'class' => 'select2',
			))
			->types('esta_descripcion', 'textarea')
			->build();

		$subtitle = $id ? 'Editar #' . $id : 'Nuevo';

		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}

	/**
	 * Elimina un estandar
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');

		$oEstandar = ORM::factory('Estandar', $id);
		$oEstandar->esta_estado = Model_Saes::STATUS_ELIMINADO;
		$oEstandar->save();

		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'Estandar',
			':pk' => $oEstandar->primary_key(),
			':id' => $oEstandar->pk(),
			':mode' => 'deleted',
		)));

		Log::access($msg);

		$this->redirect($this->request->referrer());
	}

}
