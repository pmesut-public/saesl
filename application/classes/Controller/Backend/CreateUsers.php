<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_CreateUsers extends Controller_Backend {

	public $auto_render = FALSE;
	
	public function action_index()
	{
		//CPARA CADA 5 USUARIOS, CAMBIAR EL OBAC_ID, EL CARR_ID, USERNAME, $CANTIDAD, Y SI ES OTRO TIPO DE INSTITUCION CAMBIAR TIIN_ID Y $TIPO_INSTITUCION
		//Crear instituto, cambiar tipo de institucion
		$user_limit = 10;
		
		$ti = ['u', 'i'];
		$tid = [1, 3];
		$obac = [2, 22];
		
		$moda = [1, 2];
		
		$this->create(1, 1, 1, 'i');
		$this->create(2, 1, 1, 'c');
		$this->create(3, 1, 2, 'i');
		$this->create(4, 1, 2, 'c');
		$this->create(1, 3, 1, 'i');
		$this->create(2, 3, 1, 'c');
		$this->create(3, 3, 2, 'i');
		$this->create(4, 3, 2, 'c');
		
		debug("fin");
	}
	
	public function create($index, $tiin_id, $moda_id, $tipo)
	{
		$prefix = [1 => 'u', 3 => 'i'];
		$obac = [1 => 2, 3 => 22];
		
		$tipo_institucion = $prefix[$tiin_id];
		$obac_id = $obac[$tiin_id];
		
		$gestion = 'PUBLICA';
		$username = 'usuario'.$index.'_'.$tipo_institucion;
		$password = '123456';
		
		//while($i--)
		//for ($index = 1; $index <= $user_limit; $index++)
		{
			//debug(date("Y-m-d H:i:s"));
			$institucion = DB::insert('institucion', array(
					"tiin_id", 
					"inst_estado", 
					"inst_nombre", 
					"inst_gestion", 
					"inst_subtipo", 
					"inst_comite_sineace",
				))
				->values(array(
					$tiin_id, 
					"activo", 
					$username,
					$gestion, 
					"", 
					"1",
				));
			
			$inst_id = $institucion->execute()[0];
			
			//debug($inst_id);
			//Crear carrera de este instituto
			$carr_id = '';
			if ($tipo == 'c')
			{
				$carrera = DB::insert('carrera', array(
						"inst_id", 
						"carr_estado", 
						"carr_nombre", 
						"carr_comite_sineace"
					))
					->values(array(
						$inst_id, 
						"activo", 
						"carr".$index, 
						"1"
					));

				$carr_id = $carrera->execute()[0];
			}
			
			$post = [
				"tiin_id" => $tiin_id,
				"inst_id" => $inst_id,
				"obac_id" => $obac_id,
				"carr_id" => $carr_id,
				"moda_id" => $moda_id,
				"contact" => 0,
				"first_names" => array(
					0 => "mm",
				),
				"last_names" => array(
					0 => "mm",
				),
				"positions" => array(
					0 => "mm",
				),
				"emails" => array(
					0 => "mm.com",
				),
				"phones" => array(
					0 => "1231231",
				),
				"username" => $username,
				"password" => $password,
				"confirm_password" => $password,
			];
			
			Model_Acreditado::create_new($post);
			
			debug2('usuario creado => '.$username);
		}
		
		//debug("fin");
		
	}
}
