<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Autoevaluacion extends Controller_Backend {

	//protected $theme = 'adminlte';
	
	protected $title = 'Autoevaluaciones';
	
	protected $subtitle;
	
	/**
	 *
	 * @var  Table
	 */
	protected $table;
	
	/*
	 * Carga la vista principal, con todas las autoevaluaciones 
	 */
	public function action_index()
	{
		$this->build_table();
		
		$this->build_view();

		Breadcrumb::add($this->title);
	}
	
	/*
	 * Filtra solo autoevaluaciones de universidades
	 */
	public function action_universidad()
	{
		$this->subtitle = 'Universidades';
		
		$this->by_tipo(Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD);
	}
	
	/*
	 * Filtra solo autoevaluaciones de institutos
	 */
	public function action_instituto()
	{
		$this->subtitle = 'Institutos';
		
		$this->by_tipo(Model_ObjetoAcreditacion::TIPO_INSTITUTO);
	}
	
	/*
	 * Se encarga de filtrar de acuerdo al tiin_id (Tipo de institucion)
	 * @param $tiin_id (Tipo de institucion 1 o 3)
	 */
	private function by_tipo($tiin_id)
	{
		$this->build_table()
			->options('obac_id',  Model_ObjetoAcreditacion::get_tipos($tiin_id))
			->filter('obac_id', 'Tipo de licenciamiento')
			->filter('even_id', NULL, Model_Evento::get_all($tiin_id))
			->query()
				->where('tiin_id', '=', $tiin_id);
		
		$this->build_view();
		
		Breadcrumb::add($this->title);
	}
	
	/*
	 * 
	 */
	private function build_table()
	{
		$query = $this->_get_query();
		
		$labels = ORM::factory('Acreditado')->labels() + array(
			'auto_fecha_inicio' => 'Fecha de inicio',
			'auto_fecha_fin' => 'Fecha de cierre',
			'auto_estado' => 'Estado',
			'obac_id' => 'Licenciamiento',
			'resultado' => 'Cumplimiento',
			'basicos_resultado' => 'Minedu/Sunedu',
			'mens_nombre' => 'Mensaje',
			'max_acme_fecha_reg' => 'Fecha de envío',
			'acme_response' => 'Respuesta',
			'even_id' => 'Evento',
			'tiin_id' => 'Tipo de institución',
			'inst_subtipo' => 'Subtipo',
			'inst_gestion' => 'Gestión',
			'inst_region' => 'Región',
			'registro_sineace' => 'Registro SINEACE',
		);
		
		$columns = array('tiin_id' ,'inst_name', /*'carr_nombre',*/ 'even_id', 'obac_id', 'auto_fecha_inicio', 'auto_fecha_fin', 'auto_estado');
		
		/*if ($this->request->query('extra'))
		{
			$columns = array_merge(array('mens_nombre', 'max_acme_fecha_reg', 'acme_response'), $columns);
		}*/
		
		$min = '2016-08-12';
		$max = date('Y-m-d');
		
		if (($start = $this->request->query('start_date')) AND ($end = $this->request->query('end_date')))
		{
			$query->where('auto_fecha_fin', 'between', [$start, $end]);
			$min = $start;
			$max = $end;
		}
		
		return $this->table = Table::factory('Autoevaluacion')
			->query($query)
			->labels($labels)
			->columns($columns)
			//->columns('evaluados')
			//->columns('resultado')
			//->columns('basicos_resultado')
			//->columns('even_id')
			
			//->mask_thead('resultado', 'raw_cumplidos')
			
			->options('obac_id', Model_ObjetoAcreditacion::get_options())
			->options('tiin_id', Model_TipoInstitucion::get_all())
			//->options('inst_subtipo', Model_Institucion::$subtipos)
			->options('auto_estado', array_slice(Model_Autoevaluacion::$statuses, 0, 3))
			->options('even_id', Model_Evento::get_all())
			->options('inst_gestion', Model_Institucion::$gestiones)
			->options('inst_region', Model_Departamento::get_regiones())
			//->options('registro_sineace', Model_Institucion::$comites)
						
			
			->filter('even_id')
			->filter('obac_id', 'Licenciamiento')
			->filter('tiin_id')
			//->filter('inst_subtipo')					
			->filter('inst_gestion')
			->filter('inst_region')
			->filter('auto_estado')	
			//->filter('registro_sineace')			
			//->format('resultado', array($this, 'format'))			
			->search('inst_nombre', 'Institución')
			//->search('carr_nombre')
			
			->custom_text('Fecha de cierre (min)', dateformat(date('d \d\e F \d\e\l Y', strtotime($min))))
			->custom_text('Fecha de cierre (max)', dateformat(date('d \d\e F \d\e\l Y', strtotime($max))))
			;
	}
	
	private function build_view()
	{
		$aEvento = ORM::factory('Evento')
			->where('even_estado', '=', Model_Saes::STATUS_ACTIVO)
			->find_all()
			->as_array('even_id', 'even_nombre');
		
		$view = Theme_View::factory('backend/autoevaluacion/list')
			->set(compact('aEvento'))
			->set('subtitle', $this->subtitle)
			->set('title', $this->title)
			->set('table', $this->table->build())
			->set('start_date', $this->request->query('start_date'))
			->set('end_date', $this->request->query('end_date'));
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
	}
	
	public function action_placseas()
	{
		$this->title = 'Autoevaluaciones cerradas y completas';
		
		$this->subtitle = 'Placseas';
		
		$this->build_table();
		
		$this->table->query()
			->where('auto_completa', '=', Model_Autoevaluacion::AUTOEVALUACION_COMPLETA)
			->where('auto_estado', '=', Model_Autoevaluacion::STATUS_CERRADO);
		
		$this->table
			->drop_column('last_login_formatted')
			->drop_column('minedu')
			->drop_filter('auto_estado')
			->drop_filter('auto_completa')
			//->filter('auto_completa', 'Completa', array(0 => 'No', 1 => 'Sí'))
			;
		//echo ((string)$this->table->query());die();
		
		$this->build_view();
		
		Theme::instance()
			->template
			->content
			->set_filename('backend/autoevaluacion/placseas');
	}
	
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/acreditado');
	}
	
	private function _get_query()
	{
		Model_Autoevaluacion::get_cumplimiento();
		
		$concat = "concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede)";
		
		return DB::select(DB::expr('SQL_CALC_FOUND_ROWS b.*'), 'u.last_login', 'obac_id', 'inst_region', 'inst_gestion', 'institucion.tiin_id', 'subt_id' ,'inst_subtipo', 'acev.even_id')
			->select(DB::expr('a.acre_id id'))
			
			// Comité aprobado SINEACE
			->select(DB::expr("if(
				(acob_id is null and inst_comite_sineace = 1) or 
				(acob_id is not null and carr_comite_sineace = 1), 1, 0) registro_sineace"))
			
			// Table
			->from(DB::expr('autoevaluacion b'))
			
			// No cancelled
			->where('b.auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
			
			// Only active & real users
			->where('a.acre_aprobado', '=', 1)
			->where('u.status', '=', 1)
			->where('a.acre_test', '=', 0)
			
			// AcreditadoEvento
			->join(DB::expr('acreditado_evento acev'))
				->using('acev_id')
			
			// Acreditado
			->join(DB::expr('acreditado a'))
				->on('a.acre_id', '=', 'acev.acre_id')
			
			// User
			->join(DB::expr('users u'))
				->on('a.acre_id', '=', 'u.id')
			
			// Fecha
			->select(DB::expr("DATE_FORMAT(acre_fecha_reg, '%D %M %Y @ %h.%i %p') acre_fecha_registro"))
			//->select(DB::expr("acre_fecha_reg acre_fecha_registro"))
			->select(DB::expr("FROM_UNIXTIME(last_login, '%D %M %Y @ %h.%i %p') last_login_formatted"))
			
			// get subtitle
			->join(DB::expr('acreditado_institucion ai'))
				->on('a.acre_id', '=', 'ai.acre_id')
			->join('institucion')->using('inst_id')
			->join('acreditado_objeto', 'left')->using('acin_id')
			->join('carrera', 'left')->using('carr_id')
			
			// institucion & carrera
			->select('inst_nombre', 'carr_nombre')
			->select(DB::expr("$concat inst_name"))
			
			// Last messages
			/*->select('mens_nombre', 'max_acme_fecha_reg', 'acme_response')
			->join(DB::expr("(select acre_id, max(acme_fecha_reg) max_acme_fecha_reg from acreditado_mensajes group by acre_id) mm"), 'left')
				->on('a.acre_id', '=', 'mm.acre_id')
			->join(DB::expr("acreditado_mensajes am"), 'left')
				->on(DB::expr(''), '', DB::expr("a.acre_id = am.acre_id and mm.max_acme_fecha_reg = am.acme_fecha_reg"))
			->join(DB::expr("mensajes m"), 'left')
				->using('mens_id')*/
			
			// Cumplimiento
			//->select(Model_Autoevaluacion::get_cumplimiento())
			
			->join('autoevaluacion_detalle')->using('auto_id')
			->join('estandar')->using('esta_id')
			->group_by('auto_id')
			//->order_by('auto_id')
				
			;
	}
	
	public function action_generate_reports()
	{
		$id = $this->request->param('id');
		
		$oAutoevaluacion = ORM::factory('Autoevaluacion', $id);
		
		$oAutoevaluacion->generate_reports();
		
		Log::access('Se generaron los reportes de la autoevaluación: '.$oAutoevaluacion->auto_id);
		
		$this->redirect($this->request->referrer());
	}
	
	public function action_force_finish()
	{
		$id = $this->request->param('id');
		
		$oAutoevaluacion = ORM::factory('Autoevaluacion', $id);
		
		if ($oAutoevaluacion->auto_estado <> Model_Autoevaluacion::STATUS_ACTIVO)
		{
			throw new Exception_Saes('Autoevaluación no activa');
		}
		
		$oAutoevaluacion->force_finish();
		
		Log::access('Se forzó el cierre de la autoevaluación: '.$oAutoevaluacion->auto_id);
		
		$this->redirect($this->request->referrer());
	}
	
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oAutoevaluacion = ORM::factory('Autoevaluacion', $id);
		
		if ( ! $oAutoevaluacion->loaded())
		{
			throw new Exception_Saes('Model not found');
		}
		
		$proxy = new PlacseasProxy($oAutoevaluacion);
		
		if ( ! $proxy->delete())
		{
			throw new Exception_Saes('Se produjo un error al eliminar');
		}
		
		$oAutoevaluacion->discard();
		
		$this->redirect($this->request->referrer());
	}
	
	protected function _set_template_blocks()
	{
		if ($this->auto_render)
		{
			$theme = Theme::instance();
			$theme->css('default', 'daterangepicker', '/adminlte/css/daterangepicker/daterangepicker-bs3.css');
			$theme->js('default', 'daterangepicker', '/adminlte/js/plugins/daterangepicker/daterangepicker.js');
		}
		
		parent::_set_template_blocks();
	}
	
}
