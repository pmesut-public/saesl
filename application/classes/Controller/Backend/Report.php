<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Report extends Controller_Backend {
	
	private $report;
	
	public function before()
	{
		parent::before();
		
		$this->report = $this->request->param('id');
		
		Theme::instance()
			->screen = FALSE;
	}
	
	public function action_print()
	{
		$theme = Theme::instance();
		
		$theme->template = Theme_View::factory('reportes/template');
		$theme->template->content = Report::factory($this->report, $this->request->query())->get_print();
	}
	
	public function action_excel()
	{
		$theme = Theme::instance();
		
		$theme->css(array());
		
		$theme->template = Theme_View::factory('reportes/template');
		$theme->template->content = Report::factory($this->report, $this->request->query())->get_excel();
		
		Response::factory()
			->body($theme->template)
			->send_file(TRUE, "{$this->report}.xls");
	}
	
	public function action_pdf()
	{
		set_time_limit(0);
		
		$view = Report::factory($this->report, $this->request->query())->get_pdf();
		
		Response::factory()
			->body($view)
			->send_file(TRUE, "{$this->report}.pdf", array('inline' => (bool) $this->request->query('view')));
	}
	
}
