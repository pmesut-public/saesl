<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Controller_Backend_Datos extends Controller_Backend {
	
	protected $theme = 'adminlte';
	
	protected $title = 'Datos Institucionales';
	
	/**
	 * Obtiene los datos institucionales existentes
	 */
	public function action_index()
	{
		$table = Table::factory('DocumentoInstitucional')
			//->bulk_actions('new')
			->actions(array('edit'/*, 'delete'*/))
			->options('dain_estado', Model_DocumentoInstitucional::$statuses)
			->build();

		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	/**
	 * Miga de pan 
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/role');
	}
	
	/**
	 * Invoca la funcion action_edit
	 */
	public function action_new()
	{
		$this->action_edit();
	}
	
	/**
	 * Crea o edita una modalidad
	 */
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$form = AdminForm::factory('DocumentoInstitucional')
			->options('dain_estado', Model_DocumentoInstitucional::$statuses)
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'Nuevo';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Elimina una modalidad
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oModalidad = ORM::factory('DocumentoInstitucional', $id);
		$pk = $oModalidad->primary_key();
                
                $oModalidad->delete();
			
			Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
				':model' => 'DocumentoInstitucional',
				':pk' => $pk,
				':id' => $id,
				':mode' => 'deleted',
			)));
			
			Log::access($msg);
		
		$this->redirect($this->request->referrer());
	}
	
}
