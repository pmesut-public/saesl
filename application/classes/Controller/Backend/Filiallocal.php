<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Filiallocal extends Controller_Backend {

    protected $theme = 'adminlte';
    protected $title = 'Filiales/Locales de la institución ';
    protected $oInstitucion = NULL;

    /**
     * Obtiene todas las instituciones con los siguientes campos (Nombre,Tipo,Estado,Registro SINEACE,n° carreras)
     */
    public function before() {
        parent::before();

        $oInstitucion = ORM::factory('Institucion', $this->request->param('id'));

        if (!$oInstitucion->loaded())
            $this->redirect('/admin/institucion');

        $this->oInstitucion = $oInstitucion;
        $this->title .= $oInstitucion->inst_nombre;
    }

    public function action_index() {
        $this->redirect('/admin/institucion');
    }

    public function action_all() {
        $table = Table::factory('Filiallocal')
                ->query($this->_get_query())
                ->columns(array('inst_nombre', 'inst_nivel', 'parent_id', 'inst_estado', 'inst_comite_sineace', 'carreras'))
                ->labels(array('carreras' => 'N° carreras', 'inst_nombre' => 'Filial/Local'))
                ->bulk_actions(array(
                    'pdf' => array('target' => '_blank'),
                    'pdf' => array('path' => "/admin/filiallocal/reporte/{$this->oInstitucion->inst_id}/?download=pdf"),
                    'excel' => array('path' => "/admin/filiallocal/reporte/{$this->oInstitucion->inst_id}/?download=excel"),
                    'new' => array('path' => "/admin/filiallocal/new/{$this->oInstitucion->inst_id}"),
                ))
                ->actions(array('some_button' => array(
                        'btn_class' => 'btn btn-sm btn-flat btn-default',
                        'icon_class' => 'fa fa-list-ul',
                        'data-title' => 'Ver carreras',
                        'path' => '/admin/carrera?filter__inst_id=',
            )))
                ->actions(array('edit', 'delete'))
                ->options('inst_estado', Model_Saes::$estados)
                ->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
                ->options('inst_comite_sineace', Model_Institucion::$comites)
                ->options('inst_nivel', Model_Institucion::$niveles)
                ->options('parent_id', Model_Institucion::get_filial_local($this->oInstitucion->inst_id))
                //->search('inst_nombre')
                ->filter('inst_estado')
                ->filter('inst_region', NULL, Model_Institucion::get_regiones())
                ->filter('inst_comite_sineace')
                //->filter('inst_nivel')
                ->sizes(array(
                    'inst_nombre' => Table::SIZE_XL,
                    'tiin_id' => Table::SIZE_XS,
                    'inst_estado' => Table::SIZE_XS,
                ))
                ->build();

        $subtitle = '';

        if (@$_GET["print"] == 1 && @$_GET["pdf"] == 1) {
            $oInstitucion = $this->oInstitucion;
            $this->title = $oInstitucion->inst_nombre;
            $view = Theme_View::factory('template/reportepdf')
                    ->set(compact('table', 'subtitle', 'oInstitucion'))
                    ->set('title', $this->title);
        } else {
            $oInstitucion = $this->oInstitucion;
            $view = Theme_View::factory('template/list')
                    ->set(compact('table', 'subtitle', 'oInstitucion'))
                    ->set('title', $this->title);
        }

        Theme::instance()
                ->template
                ->content = $view;

        $this->breadcrumb();
        Breadcrumb::add($this->title);
    }

    /**
     * Ruta del reporte en pdf: /admin/filiallocal/reporte/{inst_id}
     */
    public function action_reporte() {

        $data         = Model_Institucion::getStructuraHerarquica($this->oInstitucion->inst_id);
        $subtitle     = '';       
        $oInstitucion = $this->oInstitucion;
        $this->title  = $oInstitucion->inst_nombre;
        $title        = $oInstitucion->inst_nombre;

        $download = $this->request->query('download') ? $this->request->query('download') : '';

        $view = Theme_View::factory('template/reportepdf')->set(compact('data', 'subtitle', 'title', 'oInstitucion', 'download'));

        $filename = str_replace(" ", "_", strtolower(trim($title)));
        
        switch ($download) {

            case "excel":

                Response::factory()
                            ->headers(['Content-Type' => 'application/vnd.ms-excel'])
                            ->body($view)
                            ->send_file(TRUE, "{$filename}.xls");   
            break;
            
            default:
                
                $view = PDF::factory()->set('view', $view)->get();

                Response::factory()
                            ->headers(['Content-Type' => 'application/pdf'])
                            ->body($view)
                            ->send_file(TRUE, "{$filename}.pdf");

            break;
        }     
        
    }

    /**
     * Miga de pan de vista Institucion
     */
    private function breadcrumb() {
        Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
        Breadcrumb::add($this->title, '/admin/institucion');
    }

    /**
     * Obtiene las instituciones teniendo como campo la cantidad de carreras asociadas
     */
    private function _get_query() {
        return DB::select(DB::expr('SQL_CALC_FOUND_ROWS *'))
                        ->select(DB::expr("concat_ws(' - ', inst_nombre_sede, inst_region, inst_prov, inst_dist) inst_nombre, inst_nivel"))
                        ->select(DB::expr("count(carr_id) carreras"))
                        ->from(DB::expr('institucion i'))
                        ->join(DB::expr('carrera c'), 'left')->using('inst_id')
                        ->where('i.parent_senior_id', '=', $this->oInstitucion->inst_id)
                        ->group_by('inst_id');
    }

    /**
     * Invoca la function action_new
     */
    public function action_new() {
        $this->action_edit(FALSE);
    }

    /**
     * Crea o edita una institucion
     */
    public function action_edit($edit = TRUE) {
        $oIntitucion = ORM::factory('Institucion', $this->request->param('id'));

        if (!$oIntitucion->loaded() and ! $edit) {
            Session::instance()->set('error', 'No se ha encontrado la institución seleccionada');
            $this->redirect('/admin/institucion');
        }

        $nivel_sede = array(
            Model_Institucion::N_FILIAL => Model_Institucion::$niveles[Model_Institucion::N_FILIAL],
            Model_Institucion::N_LOCAL => Model_Institucion::$niveles[Model_Institucion::N_LOCAL]
        );

        $form = AdminForm::factory('Filiallocal')
                ->set_id($edit ? $oIntitucion->inst_id : NULL)
                ->options('inst_nivel', $nivel_sede)
                ->options('parent_id', $edit ? Model_Institucion::get_by_nivel("{$oIntitucion->parent_senior_id}-{$oIntitucion->inst_nivel}") : array())
                ->options('inst_region', Model_Institucion::get_regiones())
                ->options('inst_gestion', Model_Institucion::$gestiones)
                ->options('inst_comite_sineace', Model_Institucion::$comites)
                ->options('inst_estado', Model_Saes::$estados)
                ->custom('inst_nivel', array(
                    'class' => 'nivel-ajax-institucion',
                    'data-path' => '/ajax/get_instituciones_by_nivel/',
                    'data-target' => '#parent_id',
                    'data-id' => $edit ? $oIntitucion->parent_id : $oIntitucion->inst_id
                ))
                ->custom('inst_region', array(
                    'class' => 'select2',
                ))
                ->custom('tiin_id', array(
                    'class' => 'all-hidden',
                ))
                ->custom('inst_nombre', array(
                    'class' => 'all-hidden',
                ))
                ->custom('parent_senior_id', array(
                    'class' => 'all-hidden',
                ))
                ->set_values('tiin_id', $oIntitucion->tiin_id)
                ->set_values('inst_nombre', $oIntitucion->inst_nombre)
                ->set_values('parent_senior_id', $oIntitucion->inst_id)
                ->no_redirect()
                ->build();

        if ($this->request->method() == 'POST') {
            if (!$form->has_error) {
                $model = $form->model;
                $model->tiin_id = $oIntitucion->tiin_id;
                $model->inst_nombre = $oIntitucion->inst_nombre;
                $model->save();

                $this->redirect("/admin/filiallocal/all/{$model->parent_senior_id}");
            } else {
                $form = $form->view;
            }
        }

        $subtitle = $edit ? 'Editar #' . $oIntitucion->inst_id : 'Nuevo';

        $view = Theme_View::factory('template/edit')
                ->set(compact('form', 'subtitle'))
                ->set('title', $this->title);

        Theme::instance()
                ->template
                ->content = $view;

        $this->breadcrumb();
        Breadcrumb::add($subtitle);
    }

    /**
     * Eliminar institucion por medio del (inst_id)
     */
    public function action_delete() {
        $id = $this->request->param('id');

        $oInstitucion = ORM::factory('Institucion', $id);
        $oInstitucion->inst_estado = Model_Saes::STATUS_ELIMINADO;
        $oInstitucion->save();

        Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
            ':model' => 'Institucion',
            ':pk' => $oInstitucion->primary_key(),
            ':id' => $oInstitucion->pk(),
            ':mode' => 'deleted',
        )));

        Log::access($msg);

        $this->redirect($this->request->referrer());
    }

}
