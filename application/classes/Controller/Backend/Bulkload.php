<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Bulkload extends Controller_Backend {

	protected $theme = 'adminlte';
	
	protected $title = 'Carga masiva';
	
	public static $types = array(
		1 => 'university',
		3 => 'institute',
	);
	
	protected $_header_title = 'Admin';
	
	//protected $_error;
	
	//protected $_msg;
	
	public function action_index()
	{
		$this->action_table();
	}
	
	public function action_table()
	{
		/*$oObjetoAcreditacion = ORM::factory('ObjetoAcreditacion');
		
		$aTipo = ORM::factory('TipoInstitucion')
			->where('tiin_estado', '=', 'activo')
			->find_all()
			->as_array('tiin_id', 'tiin_nombre');*/
		
		if ($this->request->method() == 'POST')
		{
			extract($this->request->post());
			
			$type = Model_ObjetoAcreditacion::$tipos[ORM::factory('ObjetoAcreditacion', $obac_id)
				->tiin_id];
			//debug($type);

			$this->_clean_obac($obac_id);
			
			$status = Bulk::factory($type)
				->obac($obac_id)
				->process($_FILES['file']);
			
			if ($status)
			{
				Session::instance()->set('info', 'Data cargada correctamente');
			}
		}
		
		$view = Theme_View::factory('backend/bulkload/table')
			->set(compact('oObjetoAcreditacion', 'aTipo'));
			//->set('error', $this->_error)
			//->set('msg', $this->_msg);
		
		//$this->template->content = $view;
		Theme::instance()
			->template
			->content = $view;
	}
	
	/*
	 * Permite subir un archivo (.csv) con los estandares opcionales
	 */
	public function action_estaOpcionales()
	{
		if ($this->request->method() == 'POST')
		{
			$status = Bulk::factory('EstaOpcionales')
				->process($_FILES['file']);
			
			if ($status)
			{
				Session::instance()->set('info', 'Data cargada correctamente');
			}
		}
		
		$view = Theme_View::factory('backend/bulkload/esta_opcionales')
			->set(compact('oObjetoAcreditacion', 'aTipo'));
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	/*public function action_institution()
	{
		$oObjetoAcreditacion = ORM::factory('ObjetoAcreditacion');
		
		$aTipo = ORM::factory('TipoInstitucion')
			->where('tiin_estado', '=', 'activo')
			->find_all()
			->as_array('tiin_id', 'tiin_nombre');
		
		if ($this->request->method() == 'POST')
		{
			extract($this->request->post());
			//debug($this->request->post());
			//debug($_FILES);
			
		
			//$type = self::$types[$institution];
			//debug($institution);
			
			$this->_clean_inst($institution);
			
			$status = Bulk::factory('Institution')
				->type($institution)
				->process($_FILES['file']);
			
			if ( ! $this->_error)
			{
				$this->_msg = 'Data cargada correctamente';
			}
		}
		
		$view = Theme_View::factory('backend/bulkload/institution')
			->set(compact('oObjetoAcreditacion', 'aTipo'))
			->set('error', $this->_error)
			->set('msg', $this->_msg);
		
		//$this->template->content = $view;
		Theme::instance()
			->template
			->content = $view;
	}*/
	
	public function _clean_obac($obac_id)
	{
		DB::delete('dimension')
			->where('obac_id', '=', $obac_id)
			->execute();
		
		//die();
	}
	
	public function _clean_inst($institution)
	{
		DB::delete('carrera')
			//->join('institucion')
				//->using('inst_id')
			//->where('inst_id', '=', $institution)
			->where('inst_id', 'IN', 
				DB::expr('(select inst_id from institucion where tiin_id = '.$institution.')'))
			->execute();
		
		DB::delete('institucion')
			->where('tiin_id', '=', $institution)
			->execute();
		
		//die();
	}
	
}
