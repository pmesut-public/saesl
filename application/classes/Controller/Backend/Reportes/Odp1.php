<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Reportes_Odp1 extends Controller_Backend_Reportes {
	
	protected $title = 'Objetivos de Desarrollo del Proyecto';
	
	public function action_index()
	{
		$content = Report::factory('odp1')->get();
		
		$buttons = array(
			Theme::button('excel', NULL, '/admin/report/excel/odp1'), 
			//Theme::button('pdf', NULL, '/admin/report/pdf/odp1'),
		);
		
		$view = Theme_View::factory('backend/reportes/template')
			->set(compact('content', 'buttons'))
			->set('title', $this->title)
			->set('subtitle', NULL);
		
		Theme::instance()
			->template
			->content = $view;
	}
	
}
