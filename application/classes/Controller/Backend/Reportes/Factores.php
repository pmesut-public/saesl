<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Reportes_Factores extends Controller_Backend_Reportes {
	
	protected $title = 'Resultado del proceso de evaluación a nivel nacional | Institutos';
	
	public function action_index()
	{
		$this->redirect('/admin/reportes/factores/institutos');
	}
	
	public function action_institutos()
	{
		/*if ( ! $this->request->query('_obac_id'))
		{
			$this->request->query('_obac_id', Model_ObjetoAcreditacion::FORM_DOC);
		}*/
		//debug($this->request->query());
		
		$aObjeto = ORM::factory('ObjetoAcreditacion')
			->where('tiin_id', '=', Model_ObjetoAcreditacion::TIPO_INSTITUTO)
			->find_all()
			->as_array('obac_id', 'obac_nombre');
		
		//$obac_id = $this->request->query('_obac_id');
		
		//debug($aObjeto); die;	
		
		//$factores = Report::factory('factores', $this->request->query())
			//->get();
		
		$buttons = array(
			Theme::button('excel', NULL, '/admin/report/excel/factores'.URL::query()),
			//Theme::button('pdf', NULL, '/admin/report/pdf/factores'.URL::query()),
		);
		
		$view = Theme_View::factory('backend/reportes/factores')
			->set(compact(/*'subtitle', */'buttons', /*'factores',*/ 'aObjeto'/*, 'obac_id'*/))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
	}
	
}
