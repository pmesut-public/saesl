<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Reportes_Autoevaluaciones extends Controller_Backend_Reportes {
	
	protected $titles = [
		'max_auto' => 'Autoevaluación con mejor cumplimiento por comité',
		'last_auto' => 'Última autoevaluación por comité',
	];
	
	public function action_index()
	{
		$this->auto('max_auto');
	}
	
	public function action_last()
	{
		$this->auto('last_auto');
	}
	
	private function auto($script)
	{
		$tiin_id = $this->request->query('_tiin_id') ?: Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD;
		$this->request->query('_tiin_id', $tiin_id);
		
		$content = Report::factory($script, $this->request->query())
			->get();
		die;
		$buttons = array(
			Theme::button('excel', NULL, "/admin/report/excel/{$script}".URL::query()), 
			Theme::button('pdf', NULL, "/admin/report/pdf/{$script}".URL::query()),
		);
		
		$view = Theme_View::factory('backend/reportes/autoevaluaciones')
			->set(compact('content', 'buttons', 'tiin_id'))
			->set('title', $this->titles[$script])
			->set('subtitle', NULL);
		
		Theme::instance()
			->template
			->content = $view;
	}
	
}
