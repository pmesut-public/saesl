<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Reportes_Summary extends Controller_Backend_Reportes {
	
	protected $title = 'Resumen';
	
	public function action_index()
	{
		$subtitle = NULL;
		
		$fn_buttons = function ($report) {
			return array(
				Theme::button('excel', NULL, "/admin/report/excel/$report"), 
				Theme::button('pdf', NULL, "/admin/report/pdf/$report"),
			);
		};
		
		$comites_region = Report::factory('comites_region')->get();
		
		$summary_comites = Report::factory('summary_comites')->get();
		
		$total_autoevaluaciones = Report::factory('total_autoevaluaciones')
			->get_data()
			->as_array();
		
		$auto_completas = Report::factory('auto_completas')->get();
		
		//$auto_percentages = Report::factory('auto_percentages')->get();
		
		$view = Theme_View::factory('backend/reportes/summary')
			->set(compact(
				'subtitle', 
				'fn_buttons', 
				'comites_region', 
				'summary_comites', 
				'total_autoevaluaciones', 
				'auto_completas', 
				'auto_percentages'
			))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
	}
	
	/*public function action_single()
	{
		$report = $this->request->param('id');
		
		$theme = Theme::instance();
		
		$view = Report::factory($report)->get();
		
		$theme->template->content = $view;
	}*/
	
}
