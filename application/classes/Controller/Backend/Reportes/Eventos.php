<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Reportes_Eventos extends Controller_Backend_Reportes {
	
	protected $title = 'Reportes por eventos';
	
	public function action_tipo()
	{
		$even_tipo = $this->request->param('id');
		
		$title = 'Reportes por eventos';
		$subtitle = Inflector::plural(Model_Evento::$tipos[$even_tipo]);
		
		$eventosU = Report::factory('eventos_count')
			->set('_even_tipo', $even_tipo)
			->set('_tiin_id', Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD)
			->get();
		
		$eventosI = Report::factory('eventos_count')
			->set('_even_tipo', $even_tipo)
			->set('_tiin_id', Model_ObjetoAcreditacion::TIPO_INSTITUTO)
			->get();
		
		$eventos = array(
			'Universidades' => $eventosU,
			'Institutos' => $eventosI,
		);
		
		$view = Theme_View::factory('backend/reportes/eventos/tipo')
			->set(compact('eventos', 'subtitle', 'title'));
		
		Theme::instance()
			->template
			->content = $view;
		
		//$this->breadcrumb();
		//Breadcrumb::add($this->get_title($even_tipo));
	}
	
	public function action_ver()
	{
		$id = $this->request->param('id');
		
		$oEvento = ORM::factory('Evento', $id);
		
		$title = $this->title;
		$subtitle = '';
		
		$params = array(
			'_even_id' => $id,
			//'_tiin_id' => $oEvento->tiin_id,
			'_char' => ($oEvento->tiin_id == Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD) ? 'S' : 'M',
		);
		
		$autos = Report::factory('evento_autos', $params)
			//->get_data();
			//debug($autos);
			->get();
		
		$buttons = array(
			Theme::button('excel', NULL, '/admin/report/excel/evento_autos'.URL::query($params)),
			Theme::button('print', NULL, '/admin/report/print/evento_autos'.URL::query($params)),
		);
		
		$view = Theme_View::factory('backend/reportes/eventos/view')
			->set(compact('table', 'title', 'subtitle', 'autos', 'buttons'));
		
		Theme::instance()
			->template
			->content = $view;
		
		//$this->breadcrumb();
		//Breadcrumb::add($this->get_title($oEvento->even_tipo), '/admin/reportes/eventos/tipo/'.$oEvento->even_tipo);
		//Breadcrumb::add(Text::limit_words($title, 3, '...'));
	}
	
	/*private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		//Breadcrumb::add($this->title, '/admin/reportes');
	}
	
	private function get_title($even_tipo)
	{
		return $this->title.Inflector::plural(Model_Evento::$tipos[$even_tipo]);
	}*/
	
}
