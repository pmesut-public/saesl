<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Reportes_Comites extends Controller_Backend_Reportes {
	
	protected $title = 'Instituciones candidatas para Asistencia Técnica';
	
	protected $labels = array(
		'inst_name'     => 'Institución',
		'inst_gestion'  => 'Gestión',
		'inst_subtipo'  => 'Tipo',
		'inst_region'   => 'Región',
		'inst_prov'     => 'Provincia',
		'inst_dist'     => 'Distrito',
		'carr_nombre'   => 'Carrera',
		'sum'           => 'Estándares cumplen',
		'total'         => 'Estándares',
		'per'           => 'Porcentaje',
	);
	
	protected $sizes = array(
		'inst_name'     => Table::SIZE_XL,
		'carr_nombre'   => Table::SIZE_XL,
		'sum'           => Table::SIZE_XS,
		'total'         => Table::SIZE_XS,
		'sum_est'       => Table::SIZE_XS,
		'per'           => Table::SIZE_XS,
	);
	
	public function action_asistencia()
	{
		$data = $this->_get_data_asistencia();
		
		$buttons = array(
			Theme::button('excel', NULL, URL::query(array('print' => 1, 'excel' => 1))), 
			Theme::button('print', NULL, URL::query(array('print' => 1)))
		);
		
		$filters = array('tiin_id' => array(
			'name' => 'Tipo de institución',
			'type' => NULL,
			'options' => Model_ObjetoAcreditacion::$tipos,
		));
		
		$filter_view = Theme_View::factory('filters')
			->set(compact('filters'))
			->set('request', $this->request)
			->set('site', URL::site($this->request->uri()));
		
		$view = Theme_View::factory('backend/reportes/asistencia')
			->set(compact('data', 'buttons', 'filter_view'))
			->set('title', $this->title)
			->set('subtitle', '')
			->set('labels', $this->labels)
			->set('print', $this->request->query('print'))
			->set('count', count($data));
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
		
	}
	
	private function _get_data_asistencia()
	{
		$inst_name = "concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede)";
		
		$query = "
			foo.* 
			, max(per) max_per
			from institucion i
			join
			(select acre_id, i.*, c.carr_nombre, $inst_name inst_name
			, sum(if(aude_cumplimiento=1,1,0)) sum
			, count(esta_id) total
			, concat(format(sum(if(aude_cumplimiento=1,1,0)) / count(esta_id) * 100, 2), '%') per

			from autoevaluacion b
			join autoevaluacion_detalle ad using(auto_id)
			join acreditado a using(acre_id)
			join users u on u.id = a.acre_id
			join acreditado_institucion ai using(acre_id)
			join institucion i using(inst_id)
			left join acreditado_objeto ac using(acin_id)
			left join carrera c using(carr_id)

			where auto_estado = 'cerrado'
			and auto_completa = 1
			and u.status = 1
			and a.acre_aprobado = 1
			and a.acre_test = 0
			group by auto_id
			order by inst_nombre, carr_nombre) foo

			using(inst_id)
			#group by acre_id
			#having max_per < 75
			";
		
		$query = DB::select(DB::expr($query))
			->group_by('acre_id')
			->having('max_per', '<', 75);
		
		if ($tiin_id = $this->request->query('filter__tiin_id'))
		{
			$query->where('foo.tiin_id', '=', $tiin_id);
		}
		
		return $query
			->execute()
			->as_array();
	}
	
	
	public function action_ver()
	{
		$id = $this->request->param('id');
		
		$oEvento = ORM::factory('Evento', $id);
		
		$title = $oEvento->even_nombre;
		$subtitle = '';
		
		$my_func = function (Table $table)
		{
			//debug($table);
			foreach ($table->view()->tbody as & $row)
			{
				if(preg_match('/>0</', $row[3]))
				{
					$row[3] = HTML::tag('td', 'No se registran autoevaluaciones', array('colspan' => 7));
					unset($row[4], $row[5], $row[6], $row[7], $row[8], $row[9]);
				}
			}
		};
		
		$table = AdminReport::factory('Acreditado')
			->columns(array_keys($this->labels))
			->labels($this->labels)
			->query($this->_get_query($id))
			->sizes($this->sizes)
			->callback($my_func)
			->bulk_actions(array(/*'pdf', */'excel', 'print'))
			->build();
		
		
		//debug($table->tbody);
		
		
		
		$view = Theme_View::factory('backend/reportes/list')
			->set(compact('table', 'title', 'subtitle'));
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->get_title($oEvento->tiin_id), '/admin/reportes/eventos/'.$oEvento->tiin_id);
		Breadcrumb::add(Text::limit_words($title, 3, '...'));
	}
	
	public function action_instituto()
	{
		$subtitle = 'Institutos';
		
		$table = AdminReport::factory('Acreditado')
			->columns(array_keys($this->labels))
			->labels($this->labels)
			->query($this->_get_query(2))
			->sizes($this->sizes)
			->build();
		
		$view = Theme_View::factory('backend/reportes/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	private function _get_query($even_id)
	{
		$concat = "concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede)";
		
		return DB::select(DB::expr("a.acre_id, /*ti.tiin_nombre,*/ o.obac_nombre, inst_nombre, carr_nombre
			, $concat inst_name
			
			# Evaluados)
			, SUM(if(aude_estado = 'proceso', 1, 0)) sum_processed

			# Total
			, count(esta_id) total_est
			
			# Cumplidos
			, coalesce(SUM(if(aude_cumplimiento = 1, 1, 0)), 0) sum_est
			
			# Porcentaje
			, concat(round(coalesce(SUM(if(aude_cumplimiento = 1, 1, 0)), 0) / count(esta_id) * 100, 2), '%') perc

			# Minedu
			, coalesce(SUM(if(coalesce(aude_cumplimiento, 0) <> 1 and esta_clasificacion = 'M', 1, 0)), 0) sum_minedu
			
			# Sineace
			, if(coalesce(SUM(if(aude_cumplimiento = 1, 1, 0)), 0) / count(esta_id) * 100 <= 40, 
				SUM(if(coalesce(aude_cumplimiento, 0) <> 1 and coalesce(esta_clasificacion, 0) <> 'M', 1, 0)), '-') sineace
			
			# Procalidad
			, if(coalesce(SUM(if(aude_cumplimiento = 1, 1, 0)), 0) / count(esta_id) * 100 > 40, 
				SUM(if(coalesce(aude_cumplimiento, 0) <> 1 and coalesce(esta_clasificacion, 0) <> 'M', 1, 0)), '-') procalidad
			
			"))
			
			->from(DB::expr('acreditado a'))
			
			->join(DB::expr('objeto_acreditacion o'))->using('obac_id')
			->join(DB::expr('acreditado_evento ae'))->on(DB::expr(''), '', DB::expr("a.acre_id = ae.acre_id and ae.even_id = $even_id"))
			->join(DB::expr('acreditado_institucion ai'))->on('a.acre_id', '=', 'ai.acre_id')
			->join(DB::expr('institucion i'))->using('inst_id')
			->join(DB::expr('acreditado_objeto ac'), 'left')->using('acin_id')
			->join(DB::expr('carrera c'), 'left')->using('carr_id')
			
			->join(DB::expr("(select acre_id, max(auto_fecha_reg) max_auto_fecha_reg 
					from autoevaluacion
					where even_id = $even_id and auto_estado = 'cerrado'
					group by acre_id) foo"
				), 'left')->on(DB::expr(''), '', DB::expr("foo.acre_id = a.acre_id"))// and max_auto_fecha_reg = au.auto_fecha_reg"))
			
			->join(DB::expr('autoevaluacion au'), 'left')->on(DB::expr(''), '', DB::expr("a.acre_id = au.acre_id and max_auto_fecha_reg = au.auto_fecha_reg"))
			->join(DB::expr('autoevaluacion_detalle ad'), 'left')->using('auto_id')
			->join(DB::expr('estandar e'), 'left')->using('esta_id')
			
			//->where('ae.even_id', '=', $even_id)
			//->where('au.auto_estado', '=', Model_Autoevaluacion::STATUS_CERRADO)
			->where('a.acre_test', '=', Model_Acreditado::TIPO_REAL)
			
			->group_by('acre_id')
			
			->order_by('inst_name')
			->order_by('carr_nombre')
			;
	}
	
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		//Breadcrumb::add($this->title, '/admin/reportes');
	}
	
	private function get_title($tiin_id)
	{
		return $this->title.Inflector::plural(Model_ObjetoAcreditacion::$tipos[$tiin_id]);
	}
	
}
