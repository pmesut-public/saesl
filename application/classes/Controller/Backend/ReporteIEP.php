<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_ReporteIEP extends Controller_Backend {
	
	//const MAX = 7;
	
	private $max;
	
	public $auto_render = FALSE;
	
	public function action_obac()
	{
		$id = $this->request->param('id');
		
		Bulk::factory('IEP')
			->obac($id)
			->moda(1)
			->name('a')
			->method($this->request->method())
			->process(NULL);
		
		Bulk::factory('IEP')
			->obac($id)
			->moda(2)
			->name('b')
			->method($this->request->method())
			->process(NULL);
		
		if ($this->request->method() == 'POST')
		{
			$this->redirect($this->request->referrer());
		}
		else
		{
			die();
		}
	}
	
	public function action_all()
	{
		set_time_limit(0);
		
		$aObjeto = ORM::factory('ObjetoAcreditacion')
			->select(DB::expr("concat(tiin_nombre, ' _ ', obac_nombre) name"))
			->with('oTipoInstitucion')
			->find_all()
			->as_array('obac_id');
		
		//debug2($aObjeto);
		
		foreach ($aObjeto as $id => $oObjeto)
		{
			Bulk::factory('IEP')
				->obac($id)
				->moda(1)
				->name($oObjeto->name)
				->method($this->request->method())
				->process(NULL);
			
			Bulk::factory('IEP')
				->obac($id)
				->moda(2)
				->name($oObjeto->name)
				->method($this->request->method())
				->process(NULL);
		}
		
		if ($this->request->method() == 'POST')
		{
			$this->redirect($this->request->referrer());
		}
		else
		{
			die();
		}
	}
	
	/*public function action_first()
	{
		$obac_id = $this->request->param('id', 22);
		
		$this->set_max($obac_id);
		
		$r = $this->get_result($obac_id);
		//debug($r);
		
		$head = $this->get_head($r);
		
		$body = $this->get_body($r);
		//debug($body);
		
		$csv = new parseCSV();
		$csv->encoding('UTF-8', 'ISO-8859-1//TRANSLIT');
		
		//$a = $csv->output(NULL, [[1,2], [5,6]], ['a', 'b']);
		$a = $csv->output(NULL, $body, $head);
		//echo '<pre>'.$a;
		//die('</pre>');
		
		
		if ($this->request->query('view'))
		{
			$this->response->headers('Content-Type', 'text/html; charset=ISO-8859-1');
			$this->response->body(
				'<!DOCTYPE html><html><head><meta charset="ISO-8859-1" /></head><body>'
					.HTML::tag('pre', $a)
					.HTML::tag('pre', 'Total: '.count($body))
				.'</body></html>');
		}
		else if ($this->request->query('internal'))
		{
			$this->response->body($a);
		}
		else
		{
			$this->response
				->body($a)
				->send_file(TRUE, "obac_{$obac_id}.csv"/*, ['inline' => $this->request->query('view')]* /);
		}
	}
	
	private function set_max($obac_id)
	{
		$max = DB::select(DB::expr("count(conc_id) fuentes
			from concepto co
			join estandar e using(esta_id)
			join criterio c using(crit_id)
			join factor f using(fact_id)
			join dimension d using(dime_id)
			where obac_id = {$obac_id}
			group by esta_id
			order by fuentes desc"))
			->execute()
			->get('fuentes');
		
		//debug($max);
		$this->max = $max;
	}
	
	private function get_result($obac_id)
	{
		$r = Report::factory('last_auto_reporte_iep', ['_obac_id' => $obac_id])
			->get_data();
		
		//debug($r);
		return $r;
	}
	
	private function get_head($r)
	{
		$estandares = TreeArray::factory($r, ['esta'])->get();
		//debug($estandares);
		
		$head = [
			'Cod auto',
			'Tipo',
			'Modalidad',
			'Región',
			'Gestión',
			'Institución',
			'Carrera',
			'Fecha inicio',
			'Fecha cierre',
		];
		
		foreach ($estandares as $key => $val)
		{
			//debug($val);
			$head[] = 'D'.$val['dime_codigo'];
			$head[] = 'F'.$val['fact'];
			$head[] = 'E'.$key;
			$head[] = 'C_'.$key;
			$head[] = 'P_'.$key;
			
			for ($i = 0; $i < $this->max; $i++)
			{
				$head[] = 'FV'.($i + 1).'_'.$key;
			}
		}
		$head[] = 'Porcentaje';
		
		//debug($head);
		return $head;
	}
	
	private function get_body($r)
	{
		$autos = TreeArray::factory($r, ['auto_id', 'esta', 'conc_codigo'])->get();
		//debug($autos);
		
		$body = [];
		foreach ($autos as $auto => $estandares)
		{
			$data = current(current($estandares));
			//debug($data);
			
			$row = [
				$auto,
				$data['tiin_nombre'].' - '.$data['obac_nombre'],
				$data['moda_nombre'],
				$data['inst_region'],
				$data['inst_gestion'],
				$data['acre_id'],
				$data['carr_nombre'],
				$data['auto_fecha_inicio'],
				$data['auto_fecha_fin'],
			];
			//debug($row);
			
			foreach ($estandares as $estandar => $fuentes)
			{
				$data = current($fuentes);
				//debug($fuentes);

				$row[] = $data['dime_codigo'];
				$row[] = $data['fact_codigo'];
				$row[] = $estandar;
				$row[] = $data['cumplimiento'];
				$row[] = $data['puntaje'];

				for ($i = 0; $i < $this->max; $i++)
				{
					$fuente = Arr::get($fuentes, 'FV' . ($i + 1));

					$row[] = Arr::get($fuente, 'cumple_fuente', 0);
				}
				
				$per = $data['per'];
			}
			$row[] = numberformat($per, 2);
	
			//debug($row);
			$body[] = $row;
		}
		
		//debug($body);
		return $body;
	}
	
	public function _action_all()
	{
		$aObjeto = ORM::factory('ObjetoAcreditacion')
			->select('tiin_nombre')
			->with('oTipoInstitucion')
			->find_all()
			->as_array('obac_id');
		
		debug2($aObjeto);
		
		foreach ($aObjeto as $obac_id => $oObjeto)
		{
			$x = Request::factory("/admin/reporteIEP/first/{$obac_id}?internal=1")->execute();
			
			$filename = URL::title("{$oObjeto->tiin_nombre}_{$oObjeto->obac_nombre}", '_', TRUE).".csv";
			
			file_put_contents(APPPATH.$filename, $x);
			
			echo $filename.H_EOL.PHP_EOL;
		}
		
		//echo $x;
		die('done');
	}*/
	
}
