<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_EventoFicha extends Controller_Backend {
	
	protected $theme = 'adminlte';
	
	protected $title = ':evento - Fichas';
	
	protected $oEvento;
	
	public function action_evento()
	{
		$id = $this->request->param('id');
		
		$this->oEvento = ORM::factory('Evento', $id);
		
		$query = DB::select()
			->from('evento_ficha')
			->where('even_id', '=', $id);
		
		$table = Table::factory('EventoFicha')
			->query($query)
			->bulk_actions(array('new' => array('path' => '/admin/eventoFicha/new/'.$id)))
			->actions(array('view' => array(
				'btn_class' => 'btn btn-sm btn-flat btn-default',
				'icon_class' => 'fa fa-eye',
				'path' => '/admin/eventoFicha/view/',
				'data-title' => 'Ver ficha',
			)))
			->actions(array('edit', 'delete'))
			->build();
		
		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->_get_title());
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->_get_title());
	}
	
	private function _get_title()
	{
		$oEvento = $this->oEvento;
		
		$tiin = Arr::get(Model_ObjetoAcreditacion::$tipos, $oEvento->tiin_id);
		
		$name = $oEvento->even_nombre;
		
		if ($tiin)
			$name .= ' ('.$tiin.')';
		
		return strtr($this->title, array(
			':evento' => $name,
		));
	}
	
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add('Eventos', '/admin/evento');
		Breadcrumb::add($this->_get_title(), '/admin/eventoFicha/evento/'.$this->oEvento->even_id);
	}
	
	public function action_new()
	{
		$id = $this->request->param('id');
		
		$this->oEvento = ORM::factory('Evento', $id);
		
		$this->action_form();
	}
	
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$this->oEvento = ORM::factory('EventoFicha', $id)
			->oEvento;
		
		$this->action_form();
	}
	
	public function action_form()
	{
		$form = AdminForm::factory('EventoFicha')
			->callback('new', 'before', array($this, 'set_evento'))
			->build();
		
		$subtitle = $this->request->action() == 'edit' ? 'Editar ficha #'.$this->request->param('id') : 'Nueva ficha';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->_get_title());
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	public function action_view()
	{
		$id = $this->request->param('id');
		
		$oEventoFicha = ORM::factory('EventoFicha', $id);
		
		$subtitle = 'Ver #'.$id;
		
		$view = Theme_View::factory('backend/eventoficha/view');
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	/*private function set_evento($oEventoFicha)
	{
		$oEventoFicha->even_id = $this->request->param('id');
	}*/
	
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oEventoFicha = ORM::factory('EventoFicha', $id);
		$pk = $oEventoFicha->primary_key();
		
		$oEventoFicha->delete();
		
		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'EventoFicha',
			':pk' => $pk,
			':id' => $id,
			':mode' => 'deleted',
		)));
		
		Log::access($msg);
		
		$this->redirect($this->request->referrer());
	}
	
}
