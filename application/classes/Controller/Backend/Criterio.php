<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Controller_Backend_Criterio extends Controller_Backend {

	protected $theme = 'adminlte';
	
	protected $title = 'Criterios';
	
	/**
	 * Muestra el listado de criterios
	 */
	public function action_index()
	{
		$query = $this->_get_query();
		
		$labels = array(
			'esta_id'   => 'Estandar',
            'tiin_id'   => 'Tipo institución',
		);
		
		$table = Table::factory('Criterio')
			->query($query)
			->labels($labels)
			->columns(array('tiin_id', 'esta_id', 'crit_codigo', 'crit_descripcion', 'crit_estado'))
			->actions(array('edit', 'delete'))
			->bulk_actions(array('new', 'excel', 'print'))
			->options('crit_estado', Model_Estandar::$estados)
			->options('esta_id', Model_Estandar::get_all())
            ->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
			->filter('tiin_id')
			->filter('crit_estado')
			->search('crit_descripcion', 'Criterio')
			->sizes(array(
				'crit_codigo'           => Table::SIZE_XS,
				'crit_descripcion'      => Table::SIZE_XL,
				'crit_estado'           => Table::SIZE_XS,
			))
			->build();
		
		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	/**
	 * Miga de pan de la vista de criterios
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/criterio');
	}
	
	protected function _get_query()
	{
		return DB::select(DB::expr('SQL_CALC_FOUND_ROWS c.*'), 'tiin_id', 'esta_id')
			->select(array(DB::expr("coalesce(crit_descripcion)"), 'crit_title'))
			->select(array(DB::expr("concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion))"), 'estandar'))
			->from(array('criterio', 'c'))
            ->join(array('estandar', 'e'))
                ->using('esta_id')
			->join(array('factor', 'b'))
                ->using('fact_id')
            ->join(DB::expr('dimension a'))
				->using('dime_id')
			->join(DB::expr('tipo_institucion t'))
				->using('tiin_id')
			;
	}
	
	/**
	 * Crea un nuevo criterio
	 */
	public function action_new()
	{
		$this->action_edit();
	}
	
	/**
	 * Edita un criterio
	 */
	public function action_edit()
	{	
		$id = $this->request->param('id');
		
		$query = $this->_get_query()
			->where('c.crit_id', '=', $id);
		
		$columns = array(
            'tiin_id',
			'esta_id',
			'crit_codigo',
			'crit_descripcion',
			'crit_estado',
		);
        
        $tiin_id = DB::select('a.tiin_id')
			->from(DB::expr("criterio c"))
            ->join(array('estandar', 'e'))
                ->using('esta_id')
            ->join(array('factor', 'b'))
                ->using('fact_id')
            ->join(DB::expr('dimension a'))
				->using('dime_id')
			->join(DB::expr('tipo_institucion t'))
				->using('tiin_id')
			->where('c.crit_id', '=', $id)
			->execute()
			->get('tiin_id');
		
        $esta_id = DB::select('esta_id')
			->from('criterio')
			->where('crit_id', '=', $id)
			->execute()
			->get('esta_id');
		
        $aEstandar = Model_ObjetoAcreditacion::get_estandares($tiin_id);
		
		$form = AdminForm::factory('Criterio')
			->query($query)
			->labels(array('tiin_id' => 'Tipo institución', 'esta_id' => 'Estandar'))
			->columns($columns)
			->options('crit_estado', Model_Criterio::$estados)
            ->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
			->options('esta_id', $aEstandar)
            ->custom('tiin_id', array(
				'class' => 'ajax',
				'data-path' => '/ajax/get_tiin_estandares/',
				'data-target' => '#esta_id',
			))
			->custom('esta_id', array(
				'data-selected' => $this->request->post('esta_id') ?: $esta_id,
			))
			->types('crit_descripcion', 'textarea')
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'Nuevo';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form','subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Elimina un criterio
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oCriterio = ORM::factory('Criterio', $id);
		$oCriterio->crit_estado = Model_Criterio::STATUS_ELIMINADO;
		$oCriterio->save();
		
		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'Criterio',
			':pk' => $oCriterio->primary_key(),
			':id' => $oCriterio->pk(),
			':mode' => 'deleted',
		)));
		
		Log::access($msg);
		
		$this->redirect($this->request->referrer());
	}
	
}
