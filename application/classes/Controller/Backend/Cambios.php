<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Cambios extends Controller_Backend {

	public function action_obacs()
	{
		$a = ORM::factory('ObjetoAcreditacion')
			->find_all()
			->as_array('obac_id', 'obac_nombre');
		
		debug($a);
	}
	
	public function action_etapa1()
	{
		$filename = APPPATH."estandares/etapa1.csv";
		
		Bulk::factory('Etapa1')
			->method($this->request->method())
			->process($filename);
		
		die();
	}
	
	public function action_etapa2()
	{
		set_time_limit(0);
		
		$filename = APPPATH."estandares/etapa1.csv";
		
		Bulk::factory('Etapa2')
			->method($this->request->method())
			->process($filename);
		
		die();
	}
	
	public function action_etapa3()
	{
		set_time_limit(0);
		
		$filename = APPPATH."estandares/etapa1.csv";
		
		Bulk::factory('Etapa3')
			->method($this->request->method())
			->process($filename);
		
		die();
	}
	
	public function action_etapa4()
	{
		set_time_limit(0);
		
		$filename = APPPATH."estandares/etapa1.csv";
		
		Bulk::factory('Etapa4')
			->method($this->request->method())
			->process($filename);
		
		die();
	}
	
	public function action_etapa5()
	{
		$filename = APPPATH."estandares/etapa1.csv";
		
		Bulk::factory('Etapa5')
			->method($this->request->method())
			->process($filename);
		
		die();
	}
	
	public function action_etapa6()
	{
		$filename = APPPATH."estandares/etapa1.csv";
		
		Bulk::factory('Etapa6')
			->method($this->request->method())
			->process($filename);
		
		die();
	}
	
	public function action_etapa7()
	{
		$id = $this->request->param('id');
		
		$filename = APPPATH."estandares/obac{$id}.csv";
		
		Bulk::factory('Etapa7')
			->obac($id)
			->method($this->request->method())
			->process($filename);
		
		if ($this->request->method() == 'POST')
		{
			$this->redirect($this->request->referrer());
		}
		else
		{
			die();
		}
	}
	
	public function action_etapa7all()
	{
		set_time_limit(0);
		
		$obacs = $this->get_obacs();
		
		foreach ($obacs as $id)
		{
			$filename = APPPATH."estandares/obac{$id}.csv";
		
			Bulk::factory('Etapa7')
				->obac($id)
				->method($this->request->method())
				->process($filename);
		}
		
		if ($this->request->method() == 'POST')
		{
			$this->redirect($this->request->referrer());
		}
		else
		{
			die();
		}
	}
	
	public function action_etapa8()
	{
		$id = $this->request->param('id');
		
		Bulk::factory('Etapa8')
			->obac($id)
			->method($this->request->method())
			->process(NULL);
		
		if ($this->request->method() == 'POST')
		{
			$this->redirect($this->request->referrer());
		}
		else
		{
			die();
		}
	}
	
	public function action_etapa8all()
	{
		set_time_limit(0);
		
		$obacs = $this->get_obacs();
		
		foreach ($obacs as $id)
		{
			Bulk::factory('Etapa8')
				->obac($id)
				->method($this->request->method())
				->process(NULL);
		}
		
		if ($this->request->method() == 'POST')
		{
			$this->redirect($this->request->referrer());
		}
		else
		{
			die();
		}
	}
	
	public function action_etapa9()
	{
		$filename = APPPATH."estandares/etapa9.csv";
		
		Bulk::factory('Etapa9')
			->method($this->request->method())
			->process($filename);
		
		die();
	}
	
	private function get_obacs()
	{
		return ORM::factory('ObjetoAcreditacion')
			->find_all()
			->as_array(NULL, 'obac_id');
	}
	
}
