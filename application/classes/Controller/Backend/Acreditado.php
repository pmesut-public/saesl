<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Acreditado extends Controller_Backend {

	//protected $theme = 'adminlte';
	
	protected $title = 'Comités de calidad';
	
	protected $subtitle;
	
	protected $table;
	
	/**
     * Filtro del Controlador.
     *
     * @return filter
     */
	public function before()
	{
		set_time_limit(0);
		
		parent::before();
	}
	
	/**
     * Muestra todos los comite de calidad (Universidades e Instituciones).
     *
     * @return void
     */
	public function action_index()
	{
		$this->subtitle = 'Universidades e Institutos';
		
		$this->build_table(TRUE);
		
		$this->build_view();
		
		Breadcrumb::add($this->title);
	}
	
	/**
     * Genera las "Migas de Pan", teniendo en cuenta el titulo que se encuentra en la variable title de la clase.
     *
     * @return void
     */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/acreditado');
	}
	
	/**
     * Obtiene los registro de sólo Universidades.
     *
     * @return Response
     */
	public function action_universidad()
	{
		$this->subtitle = 'Universidades';
		
		$this->build_table(TRUE)
			//->filter('obac_id', 'Tipo de licenciamiento', Model_ObjetoAcreditacion::get_tipos(Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD))
			->query()
				->where('i.tiin_id', '=', Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD);

		$this->build_view();
		
		Breadcrumb::add($this->subtitle);
	}
	
	/**
     * Obtiene los registro de sólo Institutos (DEPRECATED).
     *
     * @return Response
     */
	public function action_instituto()
	{
		$this->subtitle = 'Institutos';
		
		$this->build_table(TRUE)
			//->filter('obac_id', 'Tipo de licenciamiento', Model_ObjetoAcreditacion::get_tipos(Model_ObjetoAcreditacion::TIPO_INSTITUTO))
			->query()
				->where('i.tiin_id', '=', Model_ObjetoAcreditacion::TIPO_INSTITUTO);
		
		$this->build_view();
		
		Breadcrumb::add($this->subtitle);
	}
	
	/**
	 * Muestra los cambio de comite pendientes por aprobar.
	 *
	 * @return Response
	 */
	public function action_committee()
	{
		$this->subtitle = 'Solicitudes de cambio de comité';
		
		$this->build_table(TRUE)
			->query()
		    ->join(DB::expr('persona p'))
			->on(DB::expr(''), '', DB::expr("a.acre_id = p.acre_id and p.pers_tipo = 'pending_contacto'"));
		
		$this->build_view();
		
		Breadcrumb::add($this->subtitle);
	}
	
	/**
     * Muestra los comite de calidad que han sido eliminados.
     *
     * @return Response
     */
	public function action_eliminados()
	{
		$this->subtitle = 'Eliminados';
		
		$this->build_table()
			->drop_column('tipo')
			->drop_filter('tipo')
			->query()
			->having('tipo', '=', Model_Acreditado::ESTADO_SUSPENDIDO);
		
		$this->build_view();
		
		Breadcrumb::add($this->subtitle);
	}
	
	 /**
     * Retorna todos los usuarios de prueba (acre_test = 1)
     *
     * @return Response
     */
	public function action_test()
	{
		$this->subtitle = 'Usuarios de prueba';
		
		$this->build_table()
			->query()
			->where('acre_test', '=', Model_Acreditado::TIPO_TEST);
		
		$this->build_view();
		
		Breadcrumb::add($this->subtitle);
	}
	
	// @admin
	public function action_all()
	{
		$this->subtitle = 'Lista completa';
		
		$this->build_table()
			->query();
		
		$this->build_view();
		
		Breadcrumb::add($this->subtitle);
	}
	
	/**
	 * Genera una el reporte usando Table Class. 
	 *
	 * @param   bool  $filter  Filter real, not suspended, and not externo?
	 * @return  Table
	 */
	private function build_table($filter = FALSE)
	{
		$query = $this->_get_query();
		
		$estados = Model_Acreditado::$estados;
		
		$columns = array('tiin_id','inst_name', 'obac_id', 'moda_id', 'inst_gestion',  
			/*'carr_nombre',*/ 'n_eval', 'tipo', 'acre_fecha_registro' /*, 'acre_externo'*/);
		
		if ($filter){

			//unset($estados[Model_Acreditado::ESTADO_SUSPENDIDO]);
			
			$query
				->where('acre_test', '=', Model_Acreditado::TIPO_REAL)
				->having('tipo', '<>', Model_Acreditado::ESTADO_SUSPENDIDO);
		}
		
		if ($this->request->query('no_eval')){

			$query->having('n_eval', '=', 0);
		}
		
		if (true || $this->request->query('extra')){

			$columns = array_merge(array('username', 'email'), $columns);
		}
		
		$labels = array(
			'username' => 'Usuario',
			'email' => 'Correos electrónicos',
			'tiin_id' 			  => 'Tipo institución',
			'n_eval'              => 'N° Auto-evaluaciones',
			'tipo'                => 'Estado',
			'acre_fecha_registro' => 'Fecha registro',
			'moda_id'             => 'Modalidad',
			'inst_gestion'        => 'Gestión',
			'inst_region'         => 'Región',
			'registro_sineace'    => 'Registro SINEACE',
		);
		
		return $this->table = Table::factory('Acreditado')
			->query($query ?: $this->_get_query()->get_sql())
			->columns($columns)
			->sizes(array(
				'n_eval'      => Table::SIZE_XS,
				'tipo'        => Table::SIZE_XS,
				'inst_name'   => Table::SIZE_LG,
				'carr_nombre' => Table::SIZE_LG,
			))
			->labels($labels)
			->mask_thead('acre_fecha_registro', 'acre_fecha_reg')
			->options('tipo', $estados)
			->options('moda_id', Model_Modalidad::get_all())
			->options('obac_id', Model_ObjetoAcreditacion::get_options())
			->options('inst_gestion', Model_Institucion::$gestiones)
			->options('inst_region', Model_Departamento::get_regiones())
			->options('tiin_id', Model_TipoInstitucion::get_all())
			//->options('registro_sineace', Model_Institucion::$comites)

			->filter('tiin_id')
			->filter('obac_id', 'Licenciamiento')
			->filter('moda_id')			
			->filter('inst_gestion')
			->filter('inst_region')
			->filter('tipo')
			
			//->filter('registro_sineace')
			->title($this->title)
			->search('inst_nombre', 'Institución')
			//->search('carr_nombre')
			;
	}
	
	/**
	 * Construye la vista a partir de un theme, le envía el subtitle, title y table.
	 *
	 * @return View
	 */
	private function build_view()
	{
		$view = Theme_View::factory('backend/acreditado/list')
			->set('subtitle', $this->subtitle)
			->set('title', $this->title)
			->set('table', $this->table->build());

		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
	}
	
	/**
	 * Consulta para la generación del reporte.
	 *
	 * @return Query
	 */
	private function _get_query()
	{
		$concat = "concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede)";
		
		return DB::select(DB::expr('SQL_CALC_FOUND_ROWS a.*'), 'i.tiin_id', 'i.inst_subtipo', 'i.*', 'u.*')
			
			// Comité aprobado SINEACE
			->select(DB::expr("if(
				(acob_id is null and inst_comite_sineace = 1) or 
				(acob_id is not null and carr_comite_sineace = 1), 1, 0) registro_sineace"))
			
			// Fecha
			->select(DB::expr("DATE_FORMAT(acre_fecha_reg, '%D %M %Y @ %h.%i %p') acre_fecha_registro"))
			//->select(DB::expr("acre_fecha_reg acre_fecha_registro"))
			
			// N reportes
			->select(DB::expr("sum(if(b.auto_id IS NOT NULL, 1, 0)) n_eval"))
			
			->join(DB::expr('acreditado_evento acev'), 'left')
				->using('acre_id')
			
			->join(DB::expr('autoevaluacion b'), 'left')
				->on(DB::expr(''), '', DB::expr("acev.acev_id = b.acev_id AND b.auto_estado <> :can", array(
					':can' => Model_Autoevaluacion::STATUS_CANCELADO,
				)))
			
			// Comite
			->select(DB::expr("email_concat email"))
			->join(DB::expr("(select acre_id, group_concat(pers_correo separator ', ') email_concat 
					from persona 
					where pers_tipo in ('contacto', 'comision')
					group by acre_id) foo"))->on('a.acre_id', '=', 'foo.acre_id')
				//->on('a.acre_id', '=', 'p.acre_id')
				//->on(DB::expr(''), '', DB::expr("a.acre_id = p.acre_id and p.pers_tipo in ('contacto', 'comision')"))
			//->where('p.pers_tipo', 'in', array_keys(Model_Persona::$tipos))
			
			// get subtitle
			->join(DB::expr('acreditado_institucion ai'))//->using('acre_id')
				->on('a.acre_id', '=', 'ai.acre_id')
			->join(DB::expr('institucion i'))->using('inst_id')
			->join('acreditado_objeto', 'left')->using('acin_id')
			->join('carrera', 'left')->using('carr_id')
			
			// status
			->select(DB::expr("u.status estado"))
			->select(DB::expr("if(u.status = 1 AND a.acre_aprobado = 0, :reg, u.status) tipo", array(':reg' => Model_Acreditado::ESTADO_REGISTRADO)))
			->join(DB::expr('users u'))
				->on('a.acre_id', '=', 'u.id')
			
			// institucion & carrera
			->select('inst_nombre', 'carr_nombre')
			->select(DB::expr("$concat inst_name"))
			//->select(DB::expr("institucion.tiin_id tipoinst"))
			
			// Table
			->from(DB::expr('acreditado a'))
			
			// Group autoevaluaciones by acreditado
			->group_by('a.acre_id');
	}
	
	/**
	 * Muestra el detalle del Comité de Calidad.
	 *
	 * @throws Exception_Saes
	 * @return View
	 */
	public function action_view()
	{
		$id = $this->request->param('id');
		
		$oAcreditado = ORM::factory('Acreditado')
			->with('oModalidad')
			->select(DB::expr("if(u.status = 1 AND acre_aprobado = 0, :reg, u.status) tipo", 
				array(':reg' => Model_Acreditado::ESTADO_REGISTRADO)))
			->join(DB::expr('users u'))
				->on('acre_id', '=', 'u.id')
			->where('acre_id', '=', $id)
			->find();
		
		if ( ! $oAcreditado->loaded())
			throw new Exception_Saes('Model not found');
		
		$aEvento      = $oAcreditado->get_eventos_cerrados();
		
		$aSeguimiento = $oAcreditado->get_seguimientos_por_elegir();
		
		$seguimientos = $oAcreditado->aSeguimientos_disponibles();
		
		$aPersona     = $oAcreditado->aComite();
		
		$aNewPersona  = $oAcreditado->aComitePending();
		
		$subtitle = 'Ver #'.$id;
		
		$view = Theme_View::factory('backend/acreditado/view')
			->set(compact('oAcreditado', 'aPersona', 'aNewPersona', 'subtitle', 'aEvento', 'aSeguimiento', 'seguimientos'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();

		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Retorna un obj de tipo Model_Acreditado filtrado por su id.
	 * 
	 * @return Model Acreditado
	 */
	private function get_acreditado()
	{
		$oAcreditado = ORM::factory('Acreditado', $this->request->param('id'));
		
		if ( ! $oAcreditado->loaded())
		{
			throw new Exception_Saes('Model not found');
		}

		return $oAcreditado;
	}
	
	/**
	 * Verifica si es posible agregar un evento del tipo seguimiento,
	 * si es posible, se agregará.
	 */
	public function action_add_seguimiento()
	{
		$oAcreditado = $this->get_acreditado();
		
		$even_id = $this->request->query('even_id');

		$oEvento = ORM::factory('Evento', $even_id);

		try{

			$oAcreditado->set_elegible($oEvento);
			
			Session::instance()->set('info', "Se agregó el evento {$oEvento->even_nombre} al comité seleccionado");
			
			Log::access("Se agregó el evento {$oEvento->even_nombre} al comité seleccionado: ".$oAcreditado->acre_id);
		}
		catch (Exception_Saes $e){

			Session::instance()->set('error', $e->getMessage());
		}
		
		/*
			if ($oAcreditado->add_evento($oEvento)) {
				Session::instance()->set('info', "Se agregó el evento {$oEvento->even_nombre} al comité seleccionado");
				
				Log::access("Se agregó el evento {$oEvento->even_nombre} al comité seleccionado: ".$oAcreditado->acre_id);
			}
			else{

				Session::instance()->set('error', 'Este comité ya tiene asignado el evento seleccionado');
			}
		*/
		
		$this->redirect($this->request->referrer());
	}
	
	/**
	 * Elimina un Comité de Calidad.
	 * 
	 * @return Redirect
	 */
	public function action_delete()
	{
		$oAcreditado = $this->get_acreditado();

		if (ACL::instance()->get_role() == 'super' AND $oAcreditado->oUser->status == Model_User::STATUS_INACTIVO)
		{
			$resp = $oAcreditado->hard_delete();
		}
		else
		{
			$resp = $oAcreditado->soft_delete();
		}
		
		if ($resp === TRUE)
		{
			Session::instance()->set('info', "Se eliminó el acreditado ".$oAcreditado->acre_id);
			Log::access("Se eliminó el acreditado ".$oAcreditado->acre_id);
		}
		else
		{
			Session::instance()->set('error', "No se pudo eliminar el acreditado {$oAcreditado->acre_id}: $resp");
		}
		
		$this->redirect($this->request->referrer());
	}
	
	/**
	 * Aprueba un Comité de Calidad
	 *
	 * @return Redirect
	 */
	public function action_activate()
	{
		$oAcreditado = $this->get_acreditado();
		
		$oAcreditado->acre_aprobado = Model_Acreditado::ESTADO_ACTIVO;
		$oAcreditado->save();
		
		Log::access("Se activó el acreditado ".$oAcreditado->acre_id);
		
		$oUser = $oAcreditado->oUser;
		
		$mail = $oUser->email;
		
		/*mail($mail,
			'Procalidad - Usuario aprobado', 
			'Su usuario '.$oUser->username.' ha sido aprobado.'.M_EOL
			//.'Password: '.$oUser->password.M_EOL
			.'Acceda al sistema en la siguiente URL: http://saes.procalidad.gob.pe');*/
		
		$msg = "Su usuario ha sido aprobado.".M_EOL;		
		$msg .= "Usuario: {$oUser->username}".M_EOL;
		$msg .= "Email: {$oUser->email}".M_EOL;
		$msg .= "Acceda al sistema en la siguiente URL: {$this->_config->site_url}".M_EOL;
		
		$email = Email::factory("{$this->_config->site_shortname} Usuario aprobado",$msg)
			->to($mail)
			->from($this->_config->noreply_mail, $this->_config->site_shortname)
			->send();
		
		$this->redirect('/admin/acreditado/view/'.$oAcreditado->acre_id);
	}
	
	/**
	 * Convierte un Comité en usuario de prueba.
	 * 
	 * @return Redirect
	 */
	public function action_mark_test()
	{
		$oAcreditado = $this->get_acreditado();
		
		$oAcreditado->acre_test = Model_Acreditado::TIPO_TEST;
		$oAcreditado->save();
		
		Log::access("Usuario marcado como test: ".$oAcreditado->acre_id);
		
		$this->redirect('/admin/acreditado/view/'.$oAcreditado->acre_id);
	}
	
	/**
	 * Fuerza la entrada como usuario y muestra las autoevaluaciones-
	 *
	 * @return Redirect
	 */
	public function action_force()
	{
		$oAcreditado = $this->get_acreditado();

		Log::access("Se loggeó como usuario: ".$oAcreditado->acre_id);
		
		Session::instance()->set('back_to_backend', $this->request->referrer());
		Session::instance()->set('oAcreditado', $oAcreditado);
		
		$this->redirect('/autoevaluaciones');
		//$this->redirect('/autoevaluaciones?acre_id='.$this->request->param('id'));
	}
	
	/**
	 * Muestra el listado de Comité de Calidad por aprobar.
	 *
	 * @return Redirect
	 */
	public function action_approve_comision()
	{
		$oAcreditado = $this->get_acreditado();
		
		$oAcreditado->approve_comision();
		
		Session::instance()->set('info', 'Nuevo comité aprobado');
		Log::access('Nuevo comité aprobado: '.$oAcreditado->acre_id);
		
		$oUser = $oAcreditado->oUser;
		
		$mail = $oUser->email;
		
		/*mail($mail,
			'Procalidad - Comité aprobado', 
			'El nuevo Comité de Calidad de su usuario '.$oUser->username.' ha sido aprobado.'.M_EOL
			.'Acceda al sistema en la siguiente URL: http://saes.procalidad.gob.pe');*/

		$msg = "El nuevo Comité de Calidad de su usuario {$oUser->username} ha sido aprobado.".M_EOL;
		$msg .= "Acceda al sistema en la siguiente URL: {$this->_config->site_url}".M_EOL;
		
		$email = Email::factory("{$this->_config->site_shortname} Comité aprobado",$msg)
			->to($mail)
			->from($this->_config->noreply_mail, $this->_config->site_shortname)
			->send();
		
		$this->redirect($this->request->referrer());
	}
	
	public function _action_errores()
	{
		$concat = "concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede)";
		
		$query = DB::select(DB::expr("SQL_CALC_FOUND_ROWS *, count(auto_id) n_eval"))
			->select(DB::expr("$concat inst_name"))
			->from(DB::expr('carrera c'))
			->join(DB::expr('acreditado_objeto ac'))->using('carr_id')
			->join(DB::expr('acreditado_institucion ai'))->using('acin_id')
			->join(DB::expr('institucion i'))->on('ai.inst_id', '=', 'i.inst_id')
			->join(DB::expr('acreditado a'))->on('a.acre_id', '=', 'ai.acre_id')
			->join(DB::expr('persona p'))->on(DB::expr(''), '', DB::expr("a.acre_id = p.acre_id AND p.pers_tipo = 'contacto'"))
			->join(DB::expr('autoevaluacion ae'), 'left')->on('a.acre_id', '=', 'ae.acre_id')
			->join(DB::expr('users u'))->on('a.acre_id', '=', 'u.id')
			->where('carr_obac_id', 'is', NULL)
			->group_by('a.acre_id');
		
		$labels = array(
			'inst_name' => 'Institución',
			'carr_nombre' => 'Carrera',
			'obac_id' => 'Tipo licenciamiento',
			'username' => 'Username',
			'pers_correo' => 'Email',
			'pers_telefono' => 'Teléfono',
			'n_eval' => 'N° evaluaciones',
		);
		
		$table = Table::factory('Acreditado')
			->query($query)
			->columns(array_keys($labels))
			->sizes(array(
				'n_eval' => Table::SIZE_XS,
				'inst_name' => Table::SIZE_LG,
				'carr_nombre' => Table::SIZE_LG,
			))
			->labels($labels)
			->options('obac_id', Model_ObjetoAcreditacion::get_tipos())
			->title($this->title)
			->build();
		
		$subtitle = 'Errores';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	public function action_add_autoevaluacion()
	{
		$oAcreditado = $this->get_acreditado();
		
		$oAcreditadoEvento = $oAcreditado
			->aAcreditadoEvento
			->where('acev_id', '=', $this->request->query('acev_id'))
			->find();
		
		//debug($oAcreditadoEvento);
		
		if ($oAcreditadoEvento->admin_can_create_new_autoevaluacion())
		{
			Model_Autoevaluacion::create_new($oAcreditadoEvento);
			
			Session::instance()->set('info', 'Nueva autoevaluación creada');
		}
		else
		{
			Session::instance()->set('error', 'No se puede crear la autoevaluación para este acreditado_evento');
		}
		
		$this->redirect($this->request->referrer());
	}
	
	public function action_cancel_evento()
	{
		$acev_id = $this->request->param('id');
		
		$oAcreditadoEvento = ORM::factory('AcreditadoEvento', $acev_id);
		
		if ( ! $oAcreditadoEvento->loaded())
		{
			throw new Exception_Saes('Model not found');
		}

		$aAutoevaluacion = $oAcreditadoEvento->aAutoevaluacion
			->where('auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
			->find_all();
		
		//debug($oAcreditadoEvento);
		
		foreach ($aAutoevaluacion as $oAutoevaluacion)
		{
			$oAutoevaluacion->discard();
		}

		$oAcreditadoEvento->discard();
		
		Session::instance()->set('info', 'Se eliminó el registro '.$acev_id.' de acreditado_evento');
		
		$this->redirect($this->request->referrer());
	}
	
	/*public function restore_evento()
	{
		$acev_id = $this->request->param('id');
		
		$oAcreditadoEvento = ORM::factory('AcreditadoEvento', $acev_id);
		
	}*/
	
}
