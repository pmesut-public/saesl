<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Dashboard extends Controller_Backend {

	protected $theme = 'adminlte';
	
	/**
	 *
	 * @var  View
	 */
	private $content;
	
	public function action_index()
	{
		$this->content = Theme_View::factory('backend/dashboard/view')
			->set('title', 'Panel')
			->set('subtitle', Auth::instance()->get_user()->roles->find()->name);
		
		//if (Auth::instance()->logged_in('operador'))
		{
			$this->operador();
		}
		
		//if (Auth::instance()->logged_in('admin'))
		{
			//$this->admin();
		}
		
		Theme::instance()
			->template
			->content = $this->content;
		
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add('Panel');
	}
	
	public function operador()
	{
		$dashboard_summary = Report::factory('dashboard_summary')
			->execute()
			->get_result();
//                debug($dashboard_summary);die;
		
		$pie_univ = Report::factory('tipo_acreditacion')
			->set('_tiin_id', Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD)
			->get_chart();
		
		$pie_inst = Report::factory('tipo_acreditacion')
			->set('_tiin_id', Model_ObjetoAcreditacion::TIPO_INSTITUTO)
			->get_chart();
		
		$inst_participantes = Report::factory('inst_participantes')
			->get();
		
		$bar_univ = Report::factory('comites_region_summ_univ')
			->get_chart();
		
		$bar_inst = Report::factory('comites_region_summ_inst')
			->get_chart();
		
		$this->content
			->set($dashboard_summary)
			->set(get_defined_vars());
	}
	
	public function admin()
	{
		// Additional for admin
	}
	
} // End Welcome