<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * @package    ProCalidad
 * @author     ProCalidad Team
 * @copyright  (c) 2018 ProCalidad
 */
class Controller_Backend_User extends Controller_Backend {

	protected $theme = 'adminlte';
	
	private $title = 'Usuarios';
	
	public function action_index()
	{
		$this->action_list();
	}
	
	public function action_list()
	{
		$roles = array(
			Model_Role::ROLE_ADMIN,
			Model_Role::ROLE_OPERADOR,
			Model_Role::ROLE_SUPERVISOR,
		);

		$query = $this->_get_query()->where('u.role_id', 'in', $roles);
		
		$labels = array(
			'id'                   => 'ID',
			'email'                => 'Email',
			'username'             => 'Username',
			'logins'               => 'Logins',
			'last_login_formatted' => 'Último login',
			'role_id'              => 'Rol',
			'status'               => 'Estado',
		);
		
		$table = Table::factory('User')
			->query($query)
			->labels($labels)
			->columns(array_keys($labels))
			->mask_thead('last_login_formatted', 'last_login')
			->options('role_id', Model_Role::$roles)
			->options('status', Model_User::$statuses)
			->filter('role_id', NULL, array_intersect_key(Model_Role::$roles, array_flip($roles)))
			->filter('status')
			->search('username', 'Username')
			->search('email', 'Email')
			->build();
		
		$subtitle = 'Reporte de usuarios';
		
		$view = Theme_View::factory('backend/user/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();

		Breadcrumb::add($this->title);
	}
	
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/acreditado');
	}
	
	public function action_new()
	{
		$this->action_edit();
	}
	
	public function action_edit()
	{
		$id = $this->request->param('id');

		$query = $this->_get_query()
			->where('u.role_id', '<>', Model_Role::ROLE_REGULAR)
			->where('u.id', '=', $id);
		
		$labels = array(
			'role_id'          => 'Rol',
			'status'           => 'Estado',
			'password'         => 'Contraseña',
			'password_confirm' => 'Confirmar contraseña',
		);
		
		$extra     = Model_User::get_password_validation($this->request->post())->rule('role_id', 'not_empty');
		
		$helptexts = [];
		
		$roles     = Model_Role::$roles;

		unset($roles[Model_Role::ROLE_REGULAR]);

		$labels_required = Model_User::$labels_required;
		
		if ($this->request->action() == 'new'){

			$labels_required = array_merge($labels_required, ['password', 'password_confirm']);

			$extra->rule('password', 'not_empty');
		}
		else{

			$helptexts = array_merge($helptexts, [
				'password_confirm' => 'Ingrese las contraseñas sólo sí desea cambiarla.'
			]);
		}
		
		$form = AdminForm::factory('User')
			->query($query)
			->columns(Model_User::$columns_form)
			->labels($labels)
			->readonly(array(
				'logins',
				'last_login',
			))
			->types(array(
				'password'         => 'password',
				'password_confirm' => 'password',
			))
			->helptexts($helptexts)
			->required($labels_required)
			->options('status', Model_User::$statuses)
			->options('role_id', $roles)
			//->callback('new', 'after', array($this, 'set_rol'))
			//->callback('edit', 'after', array($this, 'set_rol'))
			->extra_validation($extra)
			->build();
		
		$subtitle = $id ? ('Editar #'.$id) : 'Nuevo';
		
		$view = Theme_View::factory('backend/user/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		
		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Genera la Query del Controller.
	 *
	 * @return Query
	 */
	protected function _get_query()
	{
		return DB::select(DB::expr('SQL_CALC_FOUND_ROWS u.*'))
			//->select(DB::expr("ru.role_id rol"))
			->select(DB::expr("FROM_UNIXTIME(last_login, '%D %M %Y @ %h.%i %p') last_login_formatted"))
			->from(DB::expr('users u'))
			//->join(DB::expr('roles_users ru'))
			//->on('u.id', '=', 'ru.user_id')
			->as_object('Model_User');
	}
	
	/*
	public function set_rol($oUser)
	{
		$rol = (int) $this->request->post('rol');
		
		if ( ! $oUser->has('roles', $rol))
		{
			$oUser->remove('roles');
			$oUser->add('roles', $rol);
		}
	}
	*/
	
	/**
	 * Borra un usuario de los registros.
	 *
	 * @return Redirect
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oUser = ORM::factory('User', $id);
		
		if ($oUser->id == Model_User::SUPER_ADMIN_ID)
		{
			throw new Exception_Saes('Super user cannot be deleted');
		}
		
		if ($oUser->role_id == Model_Role::ROLE_REGULAR)
		{
			throw new Exception_Saes('User regular cannot be deleted using this way');
		}
		
		if (Auth::instance()->is_super() AND $oUser->status == Model_User::STATUS_INACTIVO)
		{
			$oUser->delete();
		}
		else
		{
			$oUser->status = Model_User::STATUS_INACTIVO;
			$oUser->save();
		}
		
		Session::instance()->set('info', "Se eliminó el usuario ".$id);
		
		Log::access('Se eliminó el usuario: '.$id);
		
		$this->redirect($this->request->referrer());
	}
	
}
