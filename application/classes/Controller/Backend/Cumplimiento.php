<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Cumplimiento extends Controller_Backend {

	protected $theme = 'adminlte';
	
	protected $title = 'Cumplimientos';
	
	/**
	 * Invoca a la function action_list
	 */
	public function action_index()
	{
		$this->action_list();
	}
	
	/**
	 * Muestra el listado de los cumplimientos (son opciones solo para institutos)
	 */
	public function action_list()
	{
		$labels = array('estandar' => 'Estándar', 'obac_id' => 'Tipo licenciamiento');
		
		$query = $this->_get_query();
		
		$table = Table::factory('Cumplimiento')
			->query($query)
			->columns(array('obac_id', 'cump_numero', 'cump_descripcion', 'estandar', 'cump_estado'))
			->labels($labels)
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->options('cump_estado', Model_Saes::$estados)
			->options('obac_id', Model_ObjetoAcreditacion::get_tipos(Model_ObjetoAcreditacion::TIPO_INSTITUTO))
			->filter('obac_id')
			->filter('cump_estado')
			->search('cump_descripcion')
			->sizes(array(
				'cump_numero' => Table::SIZE_XS,
				'cump_descripcion' => Table::SIZE_XL,
				'estandar'   => Table::SIZE_XL,
				'cump_estado' => Table::SIZE_XS,
				))
			->build();
		
		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/cumplimiento');
	}
	
	protected function _get_query()
	{
		return DB::select(DB::expr('SQL_CALC_FOUND_ROWS cu.*'), 'obac_id')
			->select(DB::expr("concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) estandar"))
			->from(DB::expr('cumplimiento cu'))
			->join(DB::expr('estandar e'))
				->using('esta_id')
			->join(DB::expr('criterio c'))
				->using('crit_id')
			->join(DB::expr('factor f'))
				->using('fact_id')
			->join(DB::expr('dimension d'))
				->using('dime_id')
			;
	}
	
	public function action_new()
	{
		$this->action_edit();
	}
	
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$query = $this->_get_query()
			->where('cump_id', '=', $id);
		
		$obac_id = DB::select('a.obac_id')
			->from(DB::expr("cumplimiento cu"))
			->join(DB::expr("estandar e"))
				->using('esta_id')
			->join(DB::expr("criterio c"))
				->using('crit_id')
			->join(DB::expr("factor b"))
				->using('fact_id')
			->join(DB::expr("dimension a"))
				->using('dime_id')
			->where('cu.cump_id', '=', $id)
			->execute()
			->get('obac_id');
		
		$esta_id = DB::select('esta_id')
			->from('cumplimiento')
			->join('estandar')
				->using('esta_id')
			->where('cump_id', '=', $id)
			->execute()
			->get('esta_id');
		
		$aEstandar = Model_ObjetoAcreditacion::get_estandares($obac_id);
		
		$form = AdminForm::factory('Cumplimiento')
			->query($query)
			->labels(array('obac_id' => 'Tipo licenciamiento'))
			->columns(array('obac_id', 'esta_id', 'cump_numero', 'cump_descripcion', 'cump_condicion', 'cump_estado'))
			->types('cump_descripcion', 'textarea')
			->options('cump_estado', Model_Saes::$estados)
			->options('obac_id', Model_ObjetoAcreditacion::get_tipos(Model_ObjetoAcreditacion::TIPO_INSTITUTO))
			->options('esta_id', $aEstandar)
			->custom('obac_id', array(
				'class' => 'ajax',
				'data-path' => '/ajax/get_obac_estandares/',
				'data-target' => '#esta_id',
			))
			->custom('esta_id', array(
				'data-selected' => $this->request->post('esta_id') ?: $esta_id,
			))
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'Nuevo';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form','subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Elimina un cumplimiento
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oCumplimiento = ORM::factory('Cumplimiento', $id);
		$oCumplimiento->cump_estado = Model_Saes::STATUS_ELIMINADO;
		$oCumplimiento->save();
		
		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'Cumplimiento',
			':pk' => $oCumplimiento->primary_key(),
			':id' => $oCumplimiento->pk(),
			':mode' => 'deleted',
		)));
		
		Log::access($msg);
		
		$this->redirect($this->request->referrer());
	}
	
}
