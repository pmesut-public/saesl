<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Objeto extends Controller_Backend {

	protected $theme = 'adminlte';
	
	protected $title = 'Objetos de licenciamiento';
	
	// @temp
	public $types = array(
		1 => 'universidad',
		3 => 'instituto',
	);
	
	/**
	 * Muestra todos los objetos de acreditacion
	 */
	public function action_index()
	{
		$query = DB::select(DB::expr('SQL_CALC_FOUND_ROWS o.*'))
			->select(DB::expr('count(tiin_id) tipo'))
			->from(DB::expr('objeto_acreditacion o'))
			->join(DB::expr('tipo_institucion t'), 'left')
				->using('tiin_id')
			->group_by('obac_id')
			;
		
		$table = Table::factory('ObjetoAcreditacion')
			->query($query)
			->columns(array('obac_nombre', 'tiin_id', 'obac_tipo_acreditacion', 'obac_estado'))
			->labels('tipo', 'Tipo institucion')
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->options('obac_estado', Model_Saes::$estados)
			->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
			->options('obac_tipo_acreditacion', Model_ObjetoAcreditacion::$acreditaciones)

			->search('obac_nombre')
			->filter('tiin_id')
			->filter('obac_tipo_acreditacion')
			->filter('obac_estado')
			->build();
		
		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	/**
	 * Miga de pan de objeto de acreditacion
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/objeto');
	}
	
	/**
	 * Crea un nuevo objeto de acreditacion
	 */
	public function action_new()
	{
		$this->action_edit($new = TRUE);
	}
	
	/**
	 * Edita un objeto de acreditacion
	 */
	public function action_edit($new = FALSE)
	{
		$id = $this->request->param('id');
		
		$form = AdminForm::factory('ObjetoAcreditacion')
			->options('obac_estado', Model_Saes::$estados)
			->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
			->options('obac_tipo_acreditacion', Model_ObjetoAcreditacion::$acreditaciones)
			->set_values('tiin_id', Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD)
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'Nuevo';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Elimina un objeto de acreditacion
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oObjeto = ORM::factory('ObjetoAcreditacion', $id);
		$oObjeto->obac_estado = Model_Saes::STATUS_ELIMINADO;
		$oObjeto->save();
		
		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'ObjetoAcreditacion',
			':pk' => $oObjeto->primary_key(),
			':id' => $oObjeto->pk(),
			':mode' => 'deleted',
		)));
		
		Log::access($msg);
		
		$this->redirect($this->request->referrer());
	}
}
