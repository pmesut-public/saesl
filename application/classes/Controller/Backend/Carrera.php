<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Carrera extends Controller_Backend {

	protected $theme = 'adminlte';
	
	protected $title = 'Carreras';
	
	public function action_index()
	{
		$this->action_list();
	}
	
	/**
	 * Lista todas las carreras 
	 */
	public function action_list()
	{
		$concat = "concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede)";
		
		$query = DB::select(DB::expr('SQL_CALC_FOUND_ROWS *'))
			->select(DB::expr("$concat inst_nombre, inst_region"))
			->from(DB::expr('carrera c'))
			->join(DB::expr('institucion i'))
				->using('inst_id');
		
		$table = Table::factory('Carrera')
			->query($query)
			->columns(array('tiin_id','carr_nombre', 'inst_nombre', 'carr_obac_id', 'carr_estado', 'inst_region'))
			->labels(array('inst_nombre' => 'Institución', 'tiin_id' => 'Tipo de institución', 'inst_region' => 'Región'))
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->options('carr_estado', Model_Saes::$estados)
			->options('carr_obac_id', Model_ObjetoAcreditacion::get_tipos())
			->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
			->options('inst_region', Model_Departamento::get_regiones())
			//->options('carr_comite_sineace', Model_Institucion::$comites)
			->search('inst_nombre')
			->search('carr_nombre')

			->filter('tiin_id')
			->filter('carr_estado')			
			->filter('inst_region')
			->filter('carr_obac_id')
			//->filter('carr_comite_sineace')
			->build();
		
		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	/**
	 * Miga de pan de vista Carrera
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/institucion');
	}
	
	/**
	 * Invoca la function action_new
	 */
	public function action_new()
	{
		$this->action_edit();
	}
	
	/**
	 * Edita una carrera
	 */
	public function action_edit()
	{	
		$id = $this->request->param('id');
		
                $carrera = ORM::factory('Carrera', $id);
                
                $oIntitucion = (@$carrera->carr_id) ? ORM::factory('Institucion', $carrera->inst_id):null;
                $aObjeto = (@$oIntitucion->inst_id)? Model_ObjetoAcreditacion::get_all($oIntitucion->tiin_id, Model_ObjetoAcreditacion::ACREDITACION_CARRERA, [@$carrera->carr_obac_id]):[];
       
		$form = AdminForm::factory('Carrera')
			->options('carr_estado', Model_Saes::$estados)
			->options('inst_id', Model_Institucion::get_all_aditional([@$oIntitucion->inst_id]))
			->options('carr_obac_id', $aObjeto)
			->options('carr_comite_sineace', Model_Institucion::$comites)
                        ->custom('inst_id', array(
                            'class' => 'ajax-carrera-inst_id select2',
                            'data-path' => '/ajax/get_obac_typeacre_inst/',
                            'data-target' => '#carr_obac_id',
                            'data-obac_tipo_acreditacion' => Model_ObjetoAcreditacion::ACREDITACION_CARRERA
                        ))
                        ->custom('carr_obac_id', array(
                            'class' => 'select2'
                        ))
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'Nuevo';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Elimina una carrera
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oCarrera = ORM::factory('Carrera', $id);
		$oCarrera->carr_estado = Model_Saes::STATUS_ELIMINADO;
		$oCarrera->save();
		
		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'Carrera',
			':pk' => $oCarrera->primary_key(),
			':id' => $oCarrera->pk(),
			':mode' => 'deleted',
		)));
		
		Log::access($msg);
		
		$this->redirect($this->request->referrer());
	}
}
