<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Utils extends Controller_Backend {

	public $auto_render = FALSE;
	
	public function action_index()
	{
		die('cleanall, clean, clean_users, backup');
	}
	
	public function action_clean_users()
	{
		$query = "select a.*
			, if(sum(ru.role_id = 1), 'activo', if(a.acre_aprobado = 1, 'suspendido', 'registrado')) tipo
			from acreditado a
			join roles_users ru on a.acre_id = ru.user_id
			join users u on a.acre_id = u.id
			group by a.acre_id
			having tipo = 'suspendido'
			";
		
		$DBquery = DB::query(Database::SELECT, $query);
		
		debug($DBquery->execute()->as_array('acre_id', 'tipo'), FALSE);
		
		$content = Form::open()
			.Form::submit(NULL, NULL)
			.Form::close();
		
		if ($this->request->method() == 'POST')
		{
			$aAcreditado = $DBquery->as_object('Model_Acreditado')->execute();
			foreach ($aAcreditado as $oAcreditado)
			{
				$oUser = $oAcreditado->oUser;
				debug($oUser->id, false);
				
				$oAcreditado->delete();
				$oUser->delete();
			}
			
			$content .= 'done';
		}
		
		$this->response->body($content);
	}
	
	public function action_backup()
	{
		$config = Kohana::$config->load('database')->default['connection'];
		//debug($config);
		
		$string = 'mysqldump -u:user -p:pass :db';
		
		$cmd = strtr($string, array(
			':user'	=> $config['username'],
			':pass'	=> $config['password'],
			':db'	=> $config['database'],
		));
		
		$result = shell_exec($cmd);
		//echo $result;
		//die();
		//debug($result);
		
		$file = date('Ymd').'.sql';
		
		file_put_contents(APPPATH.'backups/'.$file, $result);
		
		$this->response->headers('Content-Type', 'application/octet-stream');//; charset=utf-8');
		$this->response->headers('Content-Disposition', 'attachment; filename='.$file);

		$this->response->body($result);
	}
	
	public function action_reports()
	{
		set_time_limit(0);
		
		Theme::init($this->theme);
		
		View::set_global('theme', Theme::instance());
		
		$aAssessment = ORM::factory('Autoevaluacion')
			->join(DB::expr('acreditado_evento ae'))->using('acev_id')
			->join(DB::expr('acreditado a'))->using('acre_id')
			->join(DB::expr('users u'))->on('a.acre_id', '=', 'u.id')
			->join(DB::expr('objeto_acreditacion o'))->using('obac_id')
			
			// Cerrados & completos
			->where('auto_completa', '=', Model_Autoevaluacion::TIPO_COMPLETO)
			->where('auto_estado', '=', Model_Autoevaluacion::STATUS_CERRADO)
			
			// Real & Active
			->where('a.acre_test', '=', Model_Acreditado::TIPO_REAL)
			->where('u.status', '=', Model_User::STATUS_ACTIVO)
			
			//->limit(100)
			->find_all();
		
		debug2($aAssessment->as_array('auto_id', 'auto_estado'));
		
		$content = Form::open()
			.Form::submit(NULL, NULL)
			.Form::close();
		
		if ($this->request->method() == 'POST')
		{
			foreach ($aAssessment as $oAssessment)
			{
				// Set Acreditado
				//Session::instance()->set('oAcreditado', $oAssessment->oAcreditadoEvento->oAcreditado);

				$handler = new PlacseasProxy($oAssessment);

				$handler->save();
				
				//Session::instance()->delete('oAcreditado');
			}

			Log::placseas('DONE');
			die('done');
		}
		
		$this->response->body($content);
	}
	
	public function action_email()
	{
		$email = Email::factory('Hello, World', 'This is my body, it is nice.')
			->to('helpse@gmail.com')
			->from('mensajes@procalidad.gob.pe', 'SAES licenciamiento - PROCALIDAD')
			//->from('mensajes@emailmax.procalidad.gob.pe', 'My Name')
			->send()
			;
		
		debug($email); // 1 = OK
		
		die('end');
	}
	
	public function action_foo()
	{
		//debug($foo);
		
		$csv = new parseCSV();
		$csv->encoding('UTF-8', 'ISO-8859-1');
		
		$file = $csv->output(NULL, $foo, array_keys(reset($foo)));
		
		file_put_contents(APPPATH.'foo.csv', $file);
	}
	
	public function action_close_activas()
	{
		set_time_limit(0);
		
		Theme::init($this->theme);
		
		View::set_global('theme', Theme::instance());
		
		$aAutoevaluacion = ORM::factory('Autoevaluacion')
			->join('acreditado_evento')->using('acev_id')
			->where('auto_estado', '=', Model_Autoevaluacion::STATUS_ACTIVO)
			->where('even_id', 'in', array(18, 19))
			->find_all();
		
		debug2($aAutoevaluacion);
		
		debug2($aAutoevaluacion->as_array('auto_id', 'auto_estado'));
		
		$content = Form::open()
			.Form::submit(NULL, NULL)
			.Form::close();
		
		if ($this->request->method() == 'POST')
		{
			foreach ($aAutoevaluacion as $oAutoevaluacion)
			{
				//Session::instance()->set('oAcreditado', $oAutoevaluacion->oAcreditadoEvento->oAcreditado);

				echo $oAutoevaluacion->auto_id . ': ' . $oAutoevaluacion->auto_estado . ' -> ';

				$oAutoevaluacion->force_finish();

				echo $oAutoevaluacion->auto_estado . '<br>';

				//Session::instance()->delete('oAcreditado');
			}

			die('ok');
		}
		
		$this->response->body($content);
	}
	
	public function action_bar()
	{
		//Error::log('do');
		//Error::log('as');
		Log::error('foo');
	}
	
	public function action_routes()
	{
		$foo = '/admin/acreditado/eliminados';
		
		$uri = $this->request->uri();
		
		$url = $this->request->url();
		
		/**
		 * Route
		 */
		$route = $this->request->route();
		
		$defaults = $route->defaults();
		
		$name = R;
		
		$x = Request::factory($foo);
		
		
		debug(get_defined_vars());
		
		ACL::instance()
			->add_resource()
			->add_role()
			->allow($roles);
		
		//ACL::
		
	}
	
	public function action_acl()
	{
		$controllers = Kohana::list_files('classes/Controller/Admin', array(APPPATH));
		
		foreach ($controllers as $controller => $path)
		{
			if (is_array($path)) continue;
			
			$file = file_get_contents($path);
			
			$matches = array();
			$count = preg_match_all('/function action_(\w+)/', $file, $matches);
			
			$actions = $matches[1];
			debug2($controller, $actions);
		}
		
	}
	
	public function action_repo()
	{
		$q = 'ad.auto_id
			, d.dime_codigo#, d.dime_titulo
			, f.fact_codigo#, f.fact_titulo
			, e.esta_codigo#, coalesce(e.esta_titulo, e.esta_descripcion) e_titulo
			, co.conc_codigo#, co.conc_nombre

			#, coalesce(code_nombre, conc_nombre) concepto

			, coalesce(ad.aude_cumplimiento, 0) cumple_estandar
			, coalesce(cd.code_cumple, 0) cumple_fuente

			from autoevaluacion a
			join autoevaluacion_detalle ad using(auto_id)
			join estandar e using(esta_id)
			join concepto co using(esta_id)
			left join concepto_detalle cd on(co.conc_id = cd.conc_id and cd.aude_id = ad.aude_id) #using(conc_id)
			join criterio c using(crit_id)
			join factor f using(fact_id)
			join dimension d using(dime_id)

			where obac_id = 22
			and a.auto_completa = 1
			order by auto_id, dime_codigo, fact_codigo, esta_codigo, cast(substr(conc_codigo, 3) as signed) #conc_codigo
			limit 100000
			';
		
		$r = DB::select(DB::expr($q))
			->execute();
		
		//debug($r);
		
		$csv = new parseCSV();
		$csv->encoding('UTF-8', 'ISO-8859-1//TRANSLIT');
		
		$a = $csv->output(NULL, [[1,2], [5,6]], ['a', 'b']);
		echo '<pre>'.$a;
		die('</pre>');
		
	}
	
	public function action_fix_calidad()
	{
		$filename = APPPATH.'estandares/fix_calidad.csv';
		
		$q = '#*
			distinct aude_id
			#distinct code_calidad
			#distinct auto_id, b.*, o.*, a.acre_id, ae.even_id
			from concepto_detalle cd
			join autoevaluacion_detalle ad using(aude_id)
			join estandar e using(esta_id)
			join autoevaluacion b using(auto_id)
			join acreditado_evento ae using(acev_id)
			join acreditado a using(acre_id)
			join objeto_acreditacion o using(obac_id)
			where code_calidad not in (1,2,3)
			or aude_calidad not in (1,2,3)';
		
		$audes = DB::select(DB::expr($q))
			->execute()
			->as_array(NULL, 'aude_id');
		
		//debug($audes);
		
		$log = LogTable::factory(['border' => 1, 'cellspacing' => 1, 'cellpadding' => 5]);
		
		if ( ! $audes)
		{
			echo 'Changes already made';
			
			$log->read($filename);
			$log->stop();
		}
		
		$aAutoevaluacionDetalle = ORM::factory('AutoevaluacionDetalle')
			->where('aude_id', 'in', $audes)
			->find_all();
		
		//debug($aAutoevaluacionDetalle);
		
		foreach ($aAutoevaluacionDetalle as $oAutoevaluacionDetalle)
		{
			$log->log([
				'aude_id' => $oAutoevaluacionDetalle->aude_id,
				'aude_cumplimiento' => $oAutoevaluacionDetalle->aude_cumplimiento,
				'aude_calidad' => $oAutoevaluacionDetalle->aude_calidad,
			]);
			
			$aConcepto = $oAutoevaluacionDetalle->aConcepto->find_all();
			
			$quality = [];
			
			foreach ($aConcepto as $oConcepto)
			{
				$log->partial([
					'code_id' => $oConcepto->code_id,
					'code_cumple' => $oConcepto->code_cumple,
					'code_calidad' => $oConcepto->code_calidad,
				]);
				
				if ($oConcepto->code_cumple AND ! in_array($oConcepto->code_calidad, [1,2,3]))
				{
					$oConcepto
						->set('code_calidad', 2);
					
					if ($this->request->method() == 'POST')
					{
						$oConcepto->save();
					}
				}
				
				$log->log(['new_code_calidad' => $oConcepto->code_calidad]);
				
				if ($oConcepto->code_cumple)
				{
					$quality[] = $oConcepto->code_calidad;
				}
				
			}
			
			$log->partial(['quality' => implode(',', $quality)]);
			
			$oAutoevaluacionDetalle->set_quality($quality);
			
			$log->log(['new_aude_calidad' => $oAutoevaluacionDetalle->aude_calidad]);
			
			if ($this->request->method() == 'POST')
			{
				$oAutoevaluacionDetalle->save();
			}
		}
		
		echo $log->show();
		
		if ($this->request->method() == 'POST')
		{
			$log->write($filename);
			
			$this->redirect($this->request->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
		
	}
	
	public function action_fix_cambios()
	{
		$filename = APPPATH.'estandares/fix_cambios.csv';
		
		$q = 'ad.aude_id
			, ad.esta_id ad_esta_id, ad_e.esta_codigo ad_esta_codigo
			, co.esta_id co_esta_id, co_e.esta_codigo co_esta_codigo
			, co.conc_codigo, conc_id, code_id, auto_id
			, ad_e.moda_id ad_moda_id, co_e.moda_id co_moda_id
			, a.moda_id

			from autoevaluacion_detalle ad
			join concepto_detalle cd using(aude_id)
			join concepto co using(conc_id)
			join estandar co_e on co.esta_id = co_e.esta_id
			join estandar ad_e on ad.esta_id = ad_e.esta_id
			
			join autoevaluacion b using(auto_id)
			join acreditado_evento ae using(acev_id)
			join acreditado a using(acre_id)
			
			where ad.esta_id <> co.esta_id
			order by ad.esta_id';
		
		$codes = DB::select(DB::expr($q))
			->execute();
		
		//debug2($codes, $codes[0]);
		
		$log = LogTable::factory(['border' => 1, 'cellspacing' => 1, 'cellpadding' => 5]);
		
		if ( ! $codes->count())
		{
			echo 'Changes already made';
			
			$log->read($filename);
			$log->stop();
		}
		
		foreach ($codes as $row)
		{
			$new_aude = ORM::factory('AutoevaluacionDetalle')
				->with('oEstandar')
				->where('auto_id', '=', $row['auto_id'])
				->where('esta_codigo', '=', $row['co_esta_codigo'])
				->find();
			
			$oConcepto = ORM::factory('ConceptoDetalle', $row['code_id']);
			
			$log->partial($row);
			
			if ($new_aude->loaded())
			{
				$oConcepto->aude_id = $new_aude->aude_id;
				
				if ($this->request->method() == 'POST')
				{
					$oConcepto->save();
				}
			}
			else
			{
				$log->partial(['code' => json_encode($oConcepto->as_array())]);
				
				if ($this->request->method() == 'POST')
				{
					$oConcepto->delete();
				}
			}
			
			$log->log($row + ['new_aude' => $new_aude->aude_id]);
		}
		
		echo $log->show();
		
		if ($this->request->method() == 'POST')
		{
			$log->write($filename);
			
			$this->redirect($this->request->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
		
	}
	
	public function action_freeze()
	{
		$base_dir = APPPATH.'freeze/';
		$base_url = 'http://'.Kohana::$server_name.'/';

		$_cumplimiento = "(aude_cumplimiento = 1)";
		$total = "count(esta_id)";
		$cumplidos = "sum(if($_cumplimiento, 1, 0))";
		$per_cumplidos = "format($cumplidos / $total * 100, 2)";
		
		$aAutoevaluacion = ORM::factory('Autoevaluacion')
			//->add_reportes_stuff()
			->select(DB::expr("$per_cumplidos porcentaje"))
			
			->with('oAcreditadoEvento')
			->join(DB::expr('autoevaluacion_detalle ad'))->using('auto_id')
			->where('oAcreditadoEvento.even_id', 'in', [10, 11])
			->where('auto_estado', '=', Model_Autoevaluacion::STATUS_CERRADO)
			->group_by('auto_id')
			
			->find_all()
			->as_array();
		
		//debug($aAutoevaluacion);
		//debug(Kohana::$server_name);
		
		$porcentajes = [];
		
		foreach ($aAutoevaluacion as $oAutoevaluacion)
		{
			$dir = $base_dir.$oAutoevaluacion->auto_id;
			
			if ( ! file_exists($dir))
			{
				mkdir($dir, 0777);
			}
			
			$getautoevaluacion = file_get_contents($base_url.'api/getautoevaluacion/F5cYRLZF?auto_id='.$oAutoevaluacion->auto_id);
			file_put_contents($base_dir.$oAutoevaluacion -> auto_id.'/getAutoevaluacion.txt', $getautoevaluacion);
			
			$getmedios = file_get_contents($base_url.'api/getMedios/F5cYRLZF?auto_id='.$oAutoevaluacion->auto_id);
			file_put_contents($base_dir.$oAutoevaluacion->auto_id.'/getMedios.txt', $getmedios);
			
			$porcentajes[$oAutoevaluacion->auto_id] = $oAutoevaluacion->porcentaje;
			
		}
		
		echo "Los archivos y carpetas se crearon con éxito";
		file_put_contents(APPPATH.'freeze/porcentajes.txt', json_encode($porcentajes));
		//debug($auto_porcentaje);
	}
	
	public function action_concat()
	{
		set_time_limit(0);
		Theme::init('adminlte');
		
		$obacs = Model_ObjetoAcreditacion::get_tipos();
		//debug($obacs);
		
		foreach ($obacs as $id => $name)
		{
			$this->process_obac($id, $name);
			//break;
		}
		
		die('ok');
	}
	
	private function process_obac($id, $name)
	{
		$base = APPPATH.'obacs/';
		
		if ( ! is_dir($base))
		{
			mkdir($base);
		}
		
		$file = $base.URL::title($name, '-', TRUE).'.xls';
		
		if (file_exists($file))
		{
			return;
		}
		
		$autos = Report::factory('max_auto_obac', ['_obac_id' => $id])
			->execute()
			->get_result();

		//debug($autos);
		
		$content = '';
		foreach ($autos as $auto)
		{
			$content .= $this->process_auto($auto);
		}
		
		file_put_contents($file, View::factory('adminlte/backend/utils/obacs', compact('content')));
		
		//echo $content; die();
	}
	
	private function process_auto($auto)
	{
		$oAutoevaluacion = ORM::factory('Autoevaluacion')
			->add_reportes_stuff()
			->where('auto_id', '=', $auto['auto_id'])
			->find();
		
		View::set_global('oAcreditado', $oAutoevaluacion->oAcreditadoEvento->oAcreditado);
		
		$content = Report_Auto::factory('report2', $oAutoevaluacion)->get_excel();
		//debug($content);
		//echo $content; die();
		
		return $content;
	}
	
}
