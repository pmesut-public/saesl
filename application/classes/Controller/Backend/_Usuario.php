<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Usuario extends Controller_Backend {
	
	protected $theme = 'adminlte';
	
	protected $title = 'Cambiar contraseña';
	
	public function action_index()
	{
		$oUser = Auth::instance()->get_user();
		$post = $this->request->post();
		
		$validation = Validation::factory($post)
			->rule('old_password', 'equals', array(Auth::instance()->hash(Arr::get($post, 'old_password')), $oUser->password))
			->rule('old_password', 'not_empty')
			->rule('new_password', 'min_length', array(':value', 5))
			->rule('new_password', 'matches', array(':validation', 'new_password', 'confirm_password'))
			->rule('new_password', 'not_empty')
			->labels(array(
				'old_password' => 'Contraseña antigua', 
				'new_password' => 'Contraseña nueva', 
				'confirm_password' => 'Confirmar contraseña',
			));
		
		if ($this->request->method() == 'POST')
		{
			if ($validation->check())
			{
				$oUser->password = $post['new_password'];
				$oUser->save();
				
				Session::instance()->set('info', 'Se cambió su contraseña correctamente');
				$this->redirect($this->request->referrer());
			}
		}
		
		$errors = $validation->errors('user');
		//debug2($errors);
		
		$view = Theme_View::factory('backend/user/password')
			->set('title', $this->title)
			->set(compact('oUser', 'errors'));
		
		Theme::instance()
			->template
			->content = $view;
		
		//$this->breadcrumb();
		//Breadcrumb::add($this->title);
	}
	
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/role');
	}
	
}
