<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Institucion extends Controller_Backend {

    protected $theme = 'adminlte';
    protected $title = 'Instituciones';

    /**
     * Obtiene todas las instituciones con los siguientes campos (Nombre, Tipo, Estado,R egistro SINEACE, N° carreras).
     *
     * @return Response
     */
    public function action_index() {

        $table = Table::factory('Institucion')
            ->query($this->_get_query())
            ->columns(array('tiin_id', 'inst_nombre', 'inst_estado', 'inst_region', 'carreras'))
            ->labels(array('carreras' => 'N° carreras', 'tiin_id' => 'Tipo de Institución'))
            ->bulk_actions('new')
            ->actions(array('some_button' => array(
                'btn_class'  => 'btn btn-sm btn-flat btn-default',
                'icon_class' => 'fa fa-list-ul',
                'data-title' => 'Ver Carreras',
                'path'       => '/admin/carrera?filter__inst_id=',
            )))
            ->actions(array('edit', 'delete'))
            ->options('inst_estado', Model_Saes::$estados)
            ->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
            //->options('inst_comite_sineace', Model_Institucion::$comites)
            ->options('inst_region', Model_Departamento::get_regiones())
            //->options('inst_nivel', Model_Institucion::$niveles)
            ->search('inst_nombre')
            ->filter('tiin_id')
            ->filter('inst_estado')
            ->filter('inst_region')
            //->filter('inst_comite_sineace')
            //->filter('inst_nivel')
            ->sizes(array(
                'inst_nombre' => Table::SIZE_XL,
                'tiin_id'     => Table::SIZE_XS,
                'inst_estado' => Table::SIZE_XS,
            ))
            ->build();

        $subtitle = '';

        $view = Theme_View::factory('template/list')
                ->set(compact('table', 'subtitle'))
                ->set('title', $this->title);

        Theme::instance()
                ->template
                ->content = $view;

        $this->breadcrumb();

        Breadcrumb::add($this->title);
    }

    /**
     * Miga de pan de vista Institucion
     */
    private function breadcrumb() {
        Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
        Breadcrumb::add($this->title, '/admin/institucion');
    }

    /**
     * Obtiene las instituciones teniendo como campo la cantidad de carreras asociadas
     */
    private function _get_query() {
        return DB::select(DB::expr('SQL_CALC_FOUND_ROWS *'))
                        ->select(DB::expr("concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede) inst_nombre"))
                        ->select(DB::expr("count(carr_id) carreras"))
                        ->from(DB::expr('institucion i'))
                        ->join(DB::expr('carrera c'), 'left')->using('inst_id')
                        ->where('i.inst_nivel', '=', Model_Institucion::N_SEDE)
                        ->group_by('inst_id');
    }

    /**
     * Invoca la function action_new
     */
    public function action_new() {

        $this->action_edit();
    }

    /**
     * Crea o edita una institucion.
     *
     * @return Response
     */
    public function action_edit()
    {

        $id = $this->request->param('id');

        $oIntitucion = ORM::factory('Institucion', $id);
        //$aObjeto = (@$oIntitucion->inst_id) ? Model_ObjetoAcreditacion::get_all($oIntitucion->tiin_id, Model_ObjetoAcreditacion::ACREDITACION_INSTITUCION, [@$oIntitucion->inst_obac_id]) : [];
        $aObjeto = Model_ObjetoAcreditacion::get_all($oIntitucion->tiin_id, Model_ObjetoAcreditacion::ACREDITACION_INSTITUCION, [@$oIntitucion->inst_obac_id]);
        
        $form = AdminForm::factory('Institucion')
                ->options('inst_estado', Model_Saes::$estados)
                ->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
                ->options('inst_obac_id', $aObjeto)
                ->options('inst_region', Model_Institucion::get_regiones())
                ->options('inst_gestion', Model_Institucion::$gestiones)
                ->options('inst_comite_sineace', Model_Institucion::$comites)
                
                //->options('parent_id', Model_Institucion::get_all2())
                //->options('inst_subtipo', Model_Institucion::$subtipos)
                //->options('inst_tipo', Model_Institucion::$tipos)

                ->options('subt_id', Model_Institucion::$subtipos)

                /*->custom('subt_id', [
                    'class' => 'ajax',
                    'data-path' => '/ajax/get_subtipos_institucion/',
                    'data-target' => '#tiin_id',
                ])*/

                ->build();

        if ($this->request->method() == 'POST') {

            if (!$form->has_error) {

                $form->model->inst_nivel = Model_Institucion::N_SEDE;

                $form->model->depa_id = ORM::factory('Departamento')
                                            ->where('depa_nombre', '=', $form->model->inst_region)
                                            ->find()
                                            ->get('depa_id');

                $form->model->save();

                $this->redirect('/admin/institucion');

            } else {

                $form = $form->view;
            }
        }

        $subtitle = $id ? 'Editar #' . $id : 'Nuevo';

        $view = Theme_View::factory('template/edit')
                ->set(compact('form', 'subtitle'))
                ->set('title', $this->title);

        Theme::instance()
                ->template
                ->content = $view;

        $this->breadcrumb();

        Breadcrumb::add($subtitle);
    }

    /**
     * Eliminar institucion por medio del (inst_id)
     */
    public function action_delete() {
        $id = $this->request->param('id');

        $oInstitucion = ORM::factory('Institucion', $id);
        $oInstitucion->inst_estado = Model_Saes::STATUS_ELIMINADO;
        $oInstitucion->save();

        Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
            ':model' => 'Institucion',
            ':pk' => $oInstitucion->primary_key(),
            ':id' => $oInstitucion->pk(),
            ':mode' => 'deleted',
        )));

        Log::access($msg);

        $this->redirect($this->request->referrer());
    }

}
