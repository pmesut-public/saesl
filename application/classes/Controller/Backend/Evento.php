<?php

defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Evento extends Controller_Backend {

	protected $theme = 'adminlte';
	protected $title = 'Eventos';

	/**
	 * Muestra todos los eventos
	 */
	public function action_index()
	{
		$query = DB::select(DB::expr('SQL_CALC_FOUND_ROWS e.*'))
			->from(DB::expr('evento e'));

		$table = Table::factory('Evento')
			->query($query)
			->columns(array('even_nombre','even_alias','even_tipo', 'even_fecha_inicio','even_fecha_fin','even_per_minimo','even_estado'))	
			->bulk_actions('new')			
			->actions(array('edit', 'delete'))
			->options('even_estado', Model_Saes::$estados)
			//->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
			->options('even_objeto', Model_Evento::$objetos)
			->options('even_tipo', Model_Evento::$tipos)					
			->build();
		//debug($table);
		$subtitle = '';

		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}

	/**
	 * Miga de pan de eventos
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/evento');
	}

	/**
	 * Crea un nuevo evento
	 */
	public function action_new()
	{
		$this->action_edit();
	}

	/**
	 * Edita un evento
	 */
	public function action_edit()
	{

//		debug($this->request->body());
		$id = $this->request->param('id');

		$form = AdminForm::factory('Evento')
			->types(array(
				'even_fecha_inicio' => 'date',
				'even_fecha_fin' => 'date',
			))
			->labels(ORM::factory('Evento')->labels() + array(
				'tipo_gestion' => 'Tipo de gestión',
				'modalidad' => 'Modalidad',
				//'tipo_financiamiento' => 'Tipo de financiamiento',
				'even_conv' => 'Evento convocatoria',
				//'even_ponderacion' => 'Ponderación',
				//'even_vinculacion' => 'Medición de Actividades',
			))
			->options('even_estado', Model_Saes::$estados)
			->set_values('tiin_id', 1)
			->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
//			->options('even_objeto', Model_Evento::$objetos)
			->options('even_tipo', Model_Evento::$tipos)
			->options('even_ponderacion', Model_Evento::$ponderacion)
			->options('even_vinculacion', Model_Evento::$vinculacion)
			->options('even_conv', [])
			/*->custom('tiin_id', [
				'class' => 'ajax',
				'data-path' => '/ajax/get_eventos/',
				'data-target' => '#even_conv',
			])
			*/			
			->custom('even_conv', array(
				'data-selected' => ORM::factory('Evento', $id)->aEventoConvocatoria->find()->even_id,
                                'class' => 'select2'
			))
			->helptexts('even_conv', 'Si se relaciona con un evento anterior, especifique cuál')
			->options('tipo_gestion', Model_TipoGestion::get_all())
			->multiple('tipo_gestion', array(
				'alias' => 'aTipoGestion',
				'column_name' => 'tige_nombre',
			))
			->options('modalidad', Model_Modalidad::get_all())
			->multiple('modalidad', array(
				'alias' => 'aModalidad',
				'column_name' => 'moda_nombre',
			))

			/* ->options('tipo_financiamiento', Model_TipoFinanciamiento::get_all())
			  ->multiple('tipo_financiamiento', array(
			  'alias' => 'aTipoFinanciamiento',
			  'column_name' => 'tifi_nombre',
			  )) */
			->callback('new', 'after', [$this, 'after_save'])
			->callback('edit', 'after', [$this, 'after_save'])
			->build();

		$subtitle = $id ? 'Editar #' . $id : 'Nuevo';

		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}

	/**
	 * Elimina un evento
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');

		$oEvento = ORM::factory('Evento', $id);
		$pk = $oEvento->primary_key(); {
			$oEvento->delete();

			Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
				':model' => 'Evento',
				':pk' => $pk,
				':id' => $id,
				':mode' => 'deleted',
			)));

			Log::access($msg);
		}

		$this->redirect($this->request->referrer());
	}

	public function after_save($oEvento)
	{
		//debug($oEvento);
		//debug($this->request->post());
		if (!$even_conv = $this->request->post('even_conv'))
		{
			return;
		}

		// Already has
		if ($oEvento->has('aEventoConvocatoria'))
		{
			// There is a new even
			if ($even_conv)
			{
				// It has one but it's not the same as new even
				if (!$oEvento->has('aEventoConvocatoria', $even_conv))
				{
					$oEvento->remove('aEventoConvocatoria');
					$oEvento->add('aEventoConvocatoria', $even_conv);
				}
			}
			// There is no new even
			else
			{
				$oEvento->remove('aEventoConvocatoria');
			}
		}
		// Doesn't has
		else
		{
			$oEvento->add('aEventoConvocatoria', $even_conv);
		}
	}

}
