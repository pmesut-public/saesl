<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Role extends Controller_Backend {
	
	protected $theme = 'adminlte';
	
	protected $title = 'Roles';
	
	public function action_index()
	{
		$table = Table::factory('Role')
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->build();
		
		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/role');
	}
	
	public function action_new()
	{
		$this->action_edit();
	}
	
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$form = AdminForm::factory('Role')
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'Nuevo';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oRole = ORM::factory('Role', $id);
		$pk = $oRole->primary_key();
	
		if ($oRole->users->count_all())
		{
			Session::instance()->set('error', 'Role has Users that depend on it.');
		}
		else
		{
			$oRole->delete();
			
			Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
				':model' => 'Role',
				':pk' => $pk,
				':id' => $id,
				':mode' => 'deleted',
			)));
			
			Log::access($msg);
		}
			
		
		$this->redirect($this->request->referrer());
	}
	
}
