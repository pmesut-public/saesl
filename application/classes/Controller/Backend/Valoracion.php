<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Controller_Backend_Valoracion extends Controller_Backend {

	protected $theme = 'adminlte';
	
	protected $title = 'Niveles de Logro';
	
	/**
	 * Invoca a la function publica action_list
	 */
	public function action_index()
	{
		$this->action_list();
	}
	
	/**
	 * Muestra todas las valoraciones
	 */
	public function action_list()
	{
		$table = Table::factory('Valoracion')
			->columns(array('valo_codigo', 'valo_titulo', 'valo_equivalencia', 'valo_descripcion', 'valo_estado'))
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->options('valo_estado', Model_Saes::$estados)
			->filter('valo_estado')
			->search('valo_titulo')
			->sizes(array(
				'valo_codigo'       => Table::SIZE_XS,
				'valo_titulo'       => Table::SIZE_XS,
                                'valo_equivalencia' => Table::SIZE_XS,
                                'valo_descripcion'  => Table::SIZE_XL,
				'valo_evidencia'    => Table::SIZE_XL,
				'valo_estado'       => Table::SIZE_XS,
			))->build();
		
		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	/**
	 * Miga de pan de la vista valoracion
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/valoracion');
	}
	
	/**
	 * Invoca a la funcion action_edit
	 */
	public function action_new()
	{
		$this->action_edit();
	}
	
	/**
	 * Crea o edita una valoracion
	 */
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$form = AdminForm::factory('Valoracion')
            ->types('valo_descripcion', 'textarea')
			->types('valo_evidencia', 'textarea')
			->options('valo_estado', Model_Saes::$estados)
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'Nuevo';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Elimina una valoracion
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oValoracion = ORM::factory('Valoracion', $id);
		$oValoracion->valo_estado = Model_Saes::STATUS_ELIMINADO;
		$oValoracion->save();
		
		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'Valoracion',
			':pk' => $oValoracion->primary_key(),
			':id' => $oValoracion->pk(),
			':mode' => 'deleted',
		)));
		
		Log::access($msg);
		
		$this->redirect($this->request->referrer());
	}
}
