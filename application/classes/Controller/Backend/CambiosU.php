<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_CambiosU extends Controller_Backend_Cambios {

	public function action_create()
	{
		$id = $this->request->param('id');
		$log = json_decode(file_get_contents(APPPATH."extra_{$id}.txt"), TRUE);
		
		$view = Debug::vars($log);
		
		$view .= Form::open().Form::submit('action', 'change').Form::submit('action', 'rollback').Form::close();
		
		Theme::instance()
			->template
			->content = HTML::tag('aside', $view, ['class' => 'right-side']);
		
		if ($this->request->method() == 'POST')
		{
			//debug($this->request->post());
			//die();
			foreach ($log as $row)
			{
				if ($this->request->post('action') == 'change')
				{
					$x = DB::update('concepto')
						->set(['esta_id' => $row['new_esta_id']])
						->where('conc_id', '=', $row['conc_id'])
						->execute();
					
					echo H_EOL.H_EOL.'concepto '.$row['conc_id'].' - updated:'.$x;
					
					foreach ($row['codes'] as $code_id => $code)
					{
						echo H_EOL.'- '.$code_id;
						
						$oldAutoevaluacionDetalle = ORM::factory('AutoevaluacionDetalle', $code['old_aude_id']);
						
						$newAutoevaluacionDetalle = ORM::factory('AutoevaluacionDetalle', $code['new_aude_id']);
						
						if ( ! $newAutoevaluacionDetalle->loaded())
						{
							$cumplimiento = $oldAutoevaluacionDetalle->aude_estado == Model_AutoevaluacionDetalle::STATUS_PENDIENTE ?
								NULL :
								0;
							
							$newAutoevaluacionDetalle
								->values([
									'aude_estado' => $oldAutoevaluacionDetalle->aude_estado,
									'aude_cumplimiento' => $cumplimiento,
									'esta_id' => $row['new_esta_id'],
									'auto_id' => $oldAutoevaluacionDetalle->auto_id,
								])->create();
							
							echo ' created '.$newAutoevaluacionDetalle->aude_id;
						}
						
						$x = DB::update('concepto_detalle')
							->set(['aude_id' => $newAutoevaluacionDetalle->aude_id])
							->where('code_id', '=', $code_id)
							->execute();
						
						echo ' updated code:'.$x;
					}
				}
				else
				{
					$x = DB::update('concepto')
						->set(['esta_id' => $row['esta_id']])
						->where('conc_id', '=', $row['conc_id'])
						->execute();
					
					echo H_EOL.H_EOL.'concepto '.$row['conc_id'].' - updated:'.$x;
					
					foreach ($row['codes'] as $code_id => $code)
					{
						echo H_EOL.'- '.$code_id;
						
						$oldAutoevaluacionDetalle = ORM::factory('AutoevaluacionDetalle', $code['old_aude_id']);
						
						$newAutoevaluacionDetalle = ORM::factory('AutoevaluacionDetalle', $code['new_aude_id']);
						
						if ( ! $newAutoevaluacionDetalle->loaded())
						{
							$newAutoevaluacionDetalle = ORM::factory('AutoevaluacionDetalle',
								[
									'auto_id' => $oldAutoevaluacionDetalle->auto_id,
									'esta_id' => $row['new_esta_id'],
								]);
							
							$aude_id = $newAutoevaluacionDetalle->aude_id;
							
							$newAutoevaluacionDetalle
								->delete();
							
							echo ' deleted '.$aude_id;
						}
						
						$x = DB::update('concepto_detalle')
							->set(['aude_id' => $oldAutoevaluacionDetalle->aude_id])
							->where('code_id', '=', $code_id)
							->execute();
						
						echo ' updated code:'.$x;
					}					

				}
				
			}
			
			die(H_EOL.'done');
		}
	}
	
	public function action_change()
	{
		$id = $this->request->param('id');
		$log = json_decode(file_get_contents(APPPATH."obac_{$id}.txt"), TRUE);
		
		$view = Debug::vars($log);
		
		$view .= Form::open().Form::submit('action', 'change').Form::submit('action', 'rollback').Form::close();
		
		Theme::instance()
			->template
			->content = HTML::tag('aside', $view, ['class' => 'right-side']);
		
		if ($this->request->method() == 'POST')
		{
			//debug($this->request->post());
			
			foreach ($log as $row)
			{
				$c = $this->request->post('action') == 'change' ? $row['new_condicion'] : $row['old_condicion'];

				$x = DB::update('Concepto')
					->set(['conc_obligatorio' => $c])
					->where('esta_id', '=', $row['esta_id'])
					->where('conc_codigo', '=', $row['conc_codigo'])
					->execute();
				
				debug2($x);
			}
			
			die('done');
		}
	}
	
	public function action_autoevaluacion()
	{
		$id = $this->request->param('id');
		$log = json_decode(file_get_contents(APPPATH."obac_{$id}.txt"), TRUE);
		
		$aAutoevaluacion = ORM::factory('Autoevaluacion')
			->with('oAcreditadoEvento:oAcreditado')
			->where('obac_id', '=', $id)
			->find_all();
			//debug($aAutoevaluacion);
		
		//$oAutoevaluacion = $aAutoevaluacion[0];
		//debug2($oAutoevaluacion->result());
		
		//$aDetalle = $oAutoevaluacion->aDetalle
		
		echo '<pre>';
		
		$up = [];
		$down = [];
		
		foreach ($aAutoevaluacion as $i => $oAutoevaluacion)
		{
			echo H_EOL."[$i] ".$oAutoevaluacion->auto_id;
			//$oAutoevaluacion = $aAutoevaluacion[10]; // INC 8,11,24,25 (15) // MC 11,25 (3)
			$c = NULL;
			
			//debug($log);
			$data = TreeArray::factory($log, ['esta_id', 'conc_codigo'])->get();
			//debug2($data);
			
			foreach ($data as $e => $fuentes)
			{
				//debug($fuentes);
				
				foreach ($fuentes as $f => $fuente)
				{
					$x = H_EOL
						.$e.' | '
						.str_pad($fuente['esta_codigo'], 3).' | '
						.$f.' | '
						.str_pad($fuente['old_condicion'], 1).' | '
						.str_pad($fuente['new_condicion'], 1);
					
					echo $x;
				}
				
				$oDetalle = $oAutoevaluacion->aDetalle->where('esta_id', '=', $e)->find();
				//$d = $oDetalle->as_array();
				//debug($oDetalle);
				
				// Moda_id 2
				if ( ! $oDetalle->loaded())
				{
					echo ' --';
					continue;
				}
				
				$required = $oDetalle->oEstandar->aConcepto
					->where('conc_obligatorio', '=', 1)
					->count_all();
				
				$accomplished = $oDetalle->aConcepto
					->with('oConcepto')
					//->join('concepto')->using('conc_id')
					//->where('aude_id', '=', $oDetalle->aude_id)
					->where('conc_obligatorio', '=', 1)
					->where('code_cumple', '=', 1)
					->count_all();
				
				//debug2($required, $accomplished);
				$old_cumplimiento = $oDetalle->aude_cumplimiento;
				$new_cumplimiento = ($required == $accomplished) ? 1 : 0;
				
				echo ' - '.$required.' '.$accomplished.' || '.$oDetalle->aude_id.' - '.$old_cumplimiento.' : '.$new_cumplimiento;
				
				if ($old_cumplimiento != $new_cumplimiento)
				{
					echo ' * ';
					
					$dir = $old_cumplimiento < $new_cumplimiento ? 'up' : 'down';
					
					echo $dir;
					
					${$dir}[] = [
						'index' => $i,
						'auto_id' => $oDetalle->auto_id,
						'esta_id' => $e,
						'esta_codigo' => $fuente['esta_codigo'],
						'old_cumplimiento' => $old_cumplimiento,
						'new_cumplimiento' => $new_cumplimiento,
					];
					
				}
				
				//if ($old_c)
				
			}
			
			echo H_EOL;
		}
		
		debug2($up, $down);
		
		if ($up OR $down)
		{
			$file = APPPATH."autos_{$id}.txt";
			
			file_put_contents($file, json_encode(compact('up', 'down')));
			debug2('log creado: '.$file);
		}
		
		die('</pre>');
	}
	
	public function action_execute()
	{
		$id = $this->request->param('id');
		
		$log = json_decode(file_get_contents(APPPATH."autos_{$id}.txt"), TRUE);
		
		$up = $log['up'];
		$down = $log['down'];
		
		//debug2($up, $down);
		//die();
		$view = Debug::vars($up, $down);
		
		$view .= Form::open().Form::submit('action', 'change').Form::submit('action', 'rollback').Form::close();
		
		if ($this->request->method() == 'POST')
		{
			$p = $this->request->post('action') == 'change' ? 'new' : 'old';
			
			$autos = [];
			foreach (['up', 'down'] as $x)
			{
				$c = NULL;
				foreach ($$x as $i => $row)
				{
					$a = $row['auto_id'];
					$e = $row['esta_id'];
					
					$oAutoevaluacion = ORM::factory('Autoevaluacion', $a);
					
					${$x}[$i]['old_per'] = $oAutoevaluacion->result('per_cumplidos', FALSE);
					
					$oDetalle = $oAutoevaluacion->aDetalle->where('esta_id', '=', $e)->find();
					
					$oDetalle
						//->set('aude_aceptacion', $row[$p.'_aceptacion'])
						->set('aude_cumplimiento', $row[$p.'_cumplimiento'])
						->save();
					
					${$x}[$i]['new_per'] = $oAutoevaluacion->result('per_cumplidos', FALSE);
					
					if ( ! isset($autos[$a]))
					{
						$autos[$a] = [
							'start' => ${$x}[$i]['old_per'],
						];
					}
					
					$autos[$a]['end'] = ${$x}[$i]['new_per'];
					
				}
			}
			
			debug2($up, $down);
			debug($autos);
			die();
			
		}
		
		Theme::instance()
			->template
			->content = HTML::tag('aside', $view, ['class' => 'right-side']);
	}
	
}
