<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Daniel Zamora <danielzam.c@gmail.com>
 * @copyright  (c) 2017 ProCalidad
 */
class Controller_Backend_Formato extends Controller_Backend {

	protected $theme = 'adminlte';
	
	protected $title = 'Formatos';
	
	/**
	 * Invoca a la function publica action_list
	 */
	public function action_index()
	{
		$this->action_list();
	}
	
	/**
	 * Muestra todas los formatos
	 */
	public function action_list()
	{
		$table = Table::factory('Formato')
			->columns(array('form_nombre', 'form_columna', 'form_estado'))
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->options('form_estado', Model_Saes::$estados)
			->options('form_columna', Model_Formato::$columnas)
			->filter('form_estado')
			->sizes(array(
				'form_nombre' => Table::SIZE_XL,
				'form_estado' => Table::SIZE_XS,
			))->build();
		
		$subtitle = '';
		
		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}
	
	/**
	 * Miga de pan de la vista formato
	 */
	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/formato');
	}
	
	/**
	 * Invoca a la funcion action_edit
	 */
	public function action_new()
	{
		$this->action_edit();
	}
	
	/**
	 * Crea o edita un formato
	 */
	public function action_edit()
	{
		$id = $this->request->param('id');
		
		$form = AdminForm::factory('Formato')
			->options('form_estado', Model_Saes::$estados)
			->options('form_columna', Model_Formato::$columnas)
			->build();
		
		$subtitle = $id ? 'Editar #'.$id : 'Nuevo';
		
		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);
		
		Theme::instance()
			->template
			->content = $view;
		
		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}
	
	/**
	 * Elimina un formato
	 */
	public function action_delete()
	{
		$id = $this->request->param('id');
		
		$oFormato = ORM::factory('Formato', $id);
		$oFormato->form_estado = Model_Saes::STATUS_ELIMINADO;
		$oFormato->save();
		
		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'Formato',
			':pk' => $oFormato->primary_key(),
			':id' => $oFormato->pk(),
			':mode' => 'deleted',
		)));
		
		Log::access($msg);
		
		$this->redirect($this->request->referrer());
	}
}
