<?php defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Subtipo extends Controller_Backend
{

    protected $theme = 'adminlte';

    protected $title = 'Subtipos';

    public function action_index()
    {
        $this->action_list();
    }

    /**
     * Lista todas las subactividades
     */
    public function action_list()
    {
        $table = Table::factory('Subtipo')
//            ->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
            ->bulk_actions('new')
            ->actions(array('edit', 'delete'))
            ->build();

        $subtitle = '';

        $view = Theme_View::factory('template/list')
            ->set(compact('table', 'subtitle'))
            ->set('title', $this->title);

        Theme::instance()
            ->template
            ->content = $view;

        $this->breadcrumb();
        Breadcrumb::add($this->title);
    }

    /**
     * Miga de pan de vista Carrera
     */
    private function breadcrumb()
    {
        Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
        Breadcrumb::add($this->title, '/admin/institucion');
    }

    /**
     * Invoca la function action_new
     */
    public function action_new()
    {
        $this->action_edit();
    }

    /**
     * Edita una carrera
     */
    public function action_edit()
    {
        $id = $this->request->param('id');

        $oTipoInstitucion = ORM::factory('Subtipo', $id);

        $form = AdminForm::factory('Subtipo')
//            ->options('tiin_id', Model_ObjetoAcreditacion::$tipos)
            ->options('subt_estado', Model_Saes::$estados)
/*            ->custom('tiin_id', array(
                'data-selected' => $this->request->post('tiin_id') ?: $oTipoInstitucion->tiin_id,
            ))
*/          ->set_values('tiin_id', Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD)
            ->build();

        $subtitle = $id ? 'Editar #' . $id : 'Nuevo';

        $view = Theme_View::factory('template/edit')
            ->set(compact('form', 'subtitle'))
            ->set('title', $this->title);

        Theme::instance()
            ->template
            ->content = $view;

        $this->breadcrumb();
        Breadcrumb::add($subtitle);
    }

    /**
     * Elimina una carrera
     */
    public function action_delete()
    {
        $id = $this->request->param('id');

        $oSubtipo = ORM::factory('Subtipo', $id);
        $oSubtipo->subt_estado = Model_Saes::STATUS_ELIMINADO;
        $oSubtipo->save();

        Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
            ':model' => 'Carrera',
            ':pk' => $oSubtipo->primary_key(),
            ':id' => $oSubtipo->pk(),
            ':mode' => 'deleted',
        )));

        Log::access($msg);

        $this->redirect($this->request->referrer());
    }
}
