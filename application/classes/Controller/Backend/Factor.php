<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Backend_Factor extends Controller_Backend {

	protected $theme = 'adminlte';
	protected $title = 'Componentes';

	public function action_index()
	{
		$this->action_list();
	}

	public function action_list()
	{
		$labels = array('dimension' => 'Condición') + ORM::factory('Dimension')->labels();

		$query = $this->_get_query();

		$table = Table::factory('Factor')
			->query($query)
			->columns(array('obac_id', 'dimension', 'fact_codigo', 'fact_titulo', 'fact_estado'))
			->labels($labels)
			->bulk_actions('new')
			->actions(array('edit', 'delete'))
			->options('fact_estado', Model_Saes::$estados)
			->options('obac_id', Model_ObjetoAcreditacion::get_all())
			->filter('obac_id')
			->filter('fact_estado')
			->search('fact_titulo', 'Componente')
            ->search('dime_titulo', 'Condición')
			->sizes(array(
				'fact_codigo' => Table::SIZE_XS,
				'fact_titulo' => Table::SIZE_XL,
				'dimension' => Table::SIZE_XL,
				'fact_estado' => Table::SIZE_XS,
			))
			->build();

		$subtitle = '';

		$view = Theme_View::factory('template/list')
			->set(compact('table', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($this->title);
	}

	private function breadcrumb()
	{
		Breadcrumb::add('Home', '/admin', 'fa fa-dashboard');
		Breadcrumb::add($this->title, '/admin/factor');
	}

	protected function _get_query()
	{
		return DB::select(DB::expr('SQL_CALC_FOUND_ROWS f.*'), 'obac_id', 'dime_titulo')
				->select(DB::expr("concat(dime_codigo, '. ', dime_titulo) dimension"))
				->from(DB::expr('factor f'))
				->join(DB::expr('dimension d'))
				->using('dime_id')
		;
	}

	public function action_new()
	{
		$this->action_edit();
	}

	public function action_edit()
	{
		$id = $this->request->param('id');

		$query = $this->_get_query()
			->where('fact_id', '=', $id);

		$tiin_id = DB::select('a.obac_id')
			->from(DB::expr("factor b"))
			->join(DB::expr("dimension a"))
			->using('dime_id')
			->where('b.fact_id', '=', $id)
			->execute()
			->get('obac_id');

		$dime_id = DB::select('dime_id')
			->from('factor')
			->where('fact_id', '=', $id)
			->execute()
			->get('dime_id');

		$aDimension = Model_ObjetoAcreditacion::get_dimensiones($tiin_id);

		$form = AdminForm::factory('Factor')
			->query($query)
			->labels(array('obac_id' => 'Objeto de Licenciamiento', 'dimension' => 'Condición'))
			->columns(array('obac_id', 'dime_id', 'fact_codigo', 'fact_titulo', 'fact_descripcion', 'fact_estado'))
			->types('fact_descripcion', 'textarea')
			->options('fact_estado', Model_Estandar::$estados)
			->options('obac_id', Model_ObjetoAcreditacion::get_all())
			->options('dime_id', $aDimension)
			->custom('obac_id', array(
				'class' => 'ajax select2',
				'data-path' => '/ajax/get_tiin_dimensiones/',
				'data-target' => '#dime_id',
			))
			->custom('dime_id', array(
				'data-selected' => $this->request->post('dime_id') ?: $dime_id,
                                'class' => 'select2',
			))
			->build();

		$subtitle = $id ? 'Editar #' . $id : 'Nuevo';

		$view = Theme_View::factory('template/edit')
			->set(compact('form', 'subtitle'))
			->set('title', $this->title);

		Theme::instance()
			->template
			->content = $view;

		$this->breadcrumb();
		Breadcrumb::add($subtitle);
	}

	public function action_delete()
	{
		$id = $this->request->param('id');

		$oFactor = ORM::factory('Factor', $id);
		$oFactor->fact_estado = Model_Saes::STATUS_ELIMINADO;
		$oFactor->save();

		Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
			':model' => 'Factor',
			':pk' => $oFactor->primary_key(),
			':id' => $oFactor->pk(),
			':mode' => 'deleted',
		)));

		Log::access($msg);

		$this->redirect($this->request->referrer());
	}

}
