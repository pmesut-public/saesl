<?php defined('SYSPATH') OR die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Controller_Backend extends Theme_Controller {
	
	//public $template = 'admin/template';

	protected $theme = 'adminlte';
	
	protected $_config;
	
	protected $_header_title = 'Admin';
	
	public function before()
	{
		$this->_back_from_frontend();
		
		$this->_check_ajax();
		
		parent::before();
	
		if ($this->auto_render)
		{
			//
		}
		
		$this->_check_authentication();
		$this->_check_authorization();
		
		$this->_set_controller_globals();
		$this->_set_view_globals();
		$this->_set_template_blocks();
	}

	protected function _back_from_frontend()
	{
		$session = Session::instance();
		
		// Back from backend
		//if ($session->get('oAcreditado'))
		{
			// Do not force anymore
			//$session->delete('oAcreditado');
			
			if ($redirect = $session->get_once('back_to_backend'))
			{
				$this->redirect($redirect);
			}
		}
	}
	
	protected function _check_ajax()
	{
		if ($this->request->is_ajax())
		{
			$this->auto_render = FALSE;
			
			// @experimental
			Theme::init($this->theme);
		}
	}
	
	protected function _check_authentication()
	{
		if ( ! Auth::instance()->logged_in())
		{
			$this->redirect('/?r='.urlencode($this->request->uri().URL::query()));
		}
	}
	
	protected function _check_authorization()
	{
		$controller = lcfirst($this->request->controller());
		$action = $this->request->action();
		
		if ( ! ACL::instance()->allowed($controller, $action))
		{
			throw new HTTP_Exception_403('Thou shalt not pass');
		}
	}

	protected function _set_controller_globals()
	{
		$this->_config = Kohana::$config->load('general');
	}
	
	protected function _set_view_globals()
	{
		View::set_global('auth', Auth::instance());
		
		if ($this->auto_render)
		{
			View::set_global('theme', Theme::instance());
		}
	}

	protected function _set_template_blocks()
	{
		if ($this->auto_render)
		{
			$theme = Theme::instance();
                        
			$theme->css('page', 'select2', '/admin/css/select2.min.css');
			$theme->js('page', 'select2', '/admin/js/select2.min.js');
			$theme->css('page', 'admin', '/saes_lte/css/admin.css');
			$theme->js('page', 'admin', '/saes_lte/js/admin.js');
			
			$theme->template->header = Theme_View::factory('backend/template/header');
			
			$theme->template->sidebar = Theme_View::factory('backend/template/sidebar')
				->set('sidebar', ACL::instance()->get_sidebar());
			
			$theme->template->footer = Theme_View::factory('template/footer');
		}
	}
	
	public function after()
	{
		if ($this->auto_render)
		{
			//
		}
		
		parent::after();
		
		$this->response->headers('Expires', 0);
	}
	
} // End Controller_Template