<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Controller_Frontend extends Theme_Controller {
	
	protected $theme = 'adminlte';
	
	/**
	 *
	 * @var  Model_User
	 */
	protected $oUser;
	
	/**
	 *
	 * @var  Model_Acreditado
	 */
	protected $oAcreditado;

	protected $_config;
	
	public function before()
	{
		//$this->_check_ajax();
		
		parent::before();
	
		$this->_check_authentication();
		$this->_check_authorization();
		
		$this->_check_acreditado();
		$this->_set_controller_globals();
		
		$this->_set_view_globals();
		$this->_set_template_blocks();
		
		return;
	}
	
	protected function _check_ajax()
	{
		if ($this->request->is_ajax())
		{
			$this->auto_render = FALSE;
			
			// @experimental
			Theme::init($this->theme);
		}
	}
	
	protected function _check_authentication()
	{
		//die('Servidor en mantenimiento. Por favor regrese en 5 minutos');
		
		if ( ! Auth::instance()->logged_in())
		{
			$this->redirect('/?r='.urlencode($this->request->uri().URL::query()));
		}
	}
	
	protected function _check_authorization()
	{
		$controller = lcfirst($this->request->controller());
		$action = $this->request->action();
		
		if ( ! ACL::instance()->allowed('frontend.'.$controller, $action))
		{
			throw new HTTP_Exception_403('Thou shalt not pass');
		}
	}
	
	protected function _check_acreditado()
	{
		$backend_acreditado = $this->get_acreditado_from_backend();
		$session_acreditado = Session::instance()->get('oAcreditado');
		
		if ($backend_acreditado->loaded())
		{
			Session::instance()->delete('oAcreditado');
			$this->oAcreditado = $backend_acreditado;
		}
		elseif ($session_acreditado AND $session_acreditado instanceof Model_Acreditado)
		{
			$this->oAcreditado = $session_acreditado;
		}
		else
		{
			throw new Exception_Saes('Acreditado not found');
		}
		
	}
	
	protected function get_acreditado_from_backend()
	{
		return ORM::factory('Acreditado', $this->request->query('acre_id'));
	}

	protected function _set_controller_globals()
	{
		$this->_config = Kohana::$config->load('general');
	}
	
	protected function _set_view_globals()
	{
		View::set_global('auth', Auth::instance());
		View::set_global('oAcreditado', $this->oAcreditado);
		
		View::set_global('theme', Theme::instance());
		View::set_global('welcome', $this->oAcreditado->title());
	}
	
	protected function _set_template_blocks()
	{
		if ($this->auto_render)
		{
			$theme = Theme::instance();
			$theme->css('page', 'regular', '/saes_lte/css/regular.css');
			$theme->js('page', 'regular', '/saes_lte/js/regular.js');
			
			if (ACL::instance()->get_role() == 'regular')
			{
				$theme->template->header = Theme_View::factory('frontend/template/header', array(
					'oEventoActual' => $this->oAcreditado->oEventoActual(),
				));
			}
			else
			{
				$theme->template->header = Theme_View::factory('backend/template/header', array(
					'oAcreditado' => $this->oAcreditado,
				));
			}
			
			$theme->template->sidebar = NULL;
			$theme->template->footer = Theme_View::factory('template/footer');
		}
	}
	
	public function after()
	{
		if ($this->auto_render)
		{
			//
		}
		
		parent::after();
		
		$this->response->headers('Expires', 0);
	}
	
}
