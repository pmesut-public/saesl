<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Api_GetRegiones extends Controller_ApiV2 {
	
	public function action_index()
	{
		$regiones = Model_Institucion::get_regiones();
		
		$this->send_response($regiones);
	}
	
}
