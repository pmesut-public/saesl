<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Api_GetAutoDataFromApi extends Controller_ApiV2 {
	
	public $oAutoevaluacion;
	
	public $ignored_keys = [
		'code_fecha_reg',
		'code_fecha_act',
		'conc_fecha_reg',
		'conc_fecha_act',
		'aude_fecha_reg',
		'aude_fecha_act',
		'esta_fecha_reg',
		'esta_fecha_act',
		'crit_fecha_reg',
		'crit_fecha_act',
		'fact_fecha_reg',
		'fact_fecha_act',
		'dime_fecha_reg',
		'dime_fecha_act',
		'obac_fecha_reg',
		'obac_fecha_act',
		'auto_fecha_reg',
		'auto_fecha_act',
		'acre_fecha_reg',
		'acre_fecha_act',
		'auto_estandar',
	];
	
	public function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('Autoevaluacion');
	}
	
	public function action_index()
	{
		$response = [
			'auto_resumen' => $this->get_resumen(),
			'auto_estandares' => $this->get_estandares(),
			'auto_fuentes' => $this->get_fuentes(),
		];
		//debug($response);
		
		$this->send_response($response);
	}
	
	private function get_resumen()
	{
		$resumen = $this->remove_keys($this->oAutoevaluacion->as_array());
		//debug($resumen);
		
		// @TODO TEMP
		//$resumen['auto_porcentaje'] = $resumen['per_cumplidos'];
		
		$base_dir = APPPATH.'freeze/';
		$filename = $base_dir.'/porcentajes.txt';
		
		if (file_exists($filename)) // Si existe en el freeze (auto_id), reemplazar por el porcentaje del archivo freezeado
		{
			$porcentajes = json_decode(file_get_contents($filename), TRUE);
			
			if ($porcentaje = Arr::get($porcentajes, $this->oAutoevaluacion->auto_id))
			{
				$resumen['auto_porcentaje'] = $porcentaje;
			}
		}
		
		//debug($resumen);
		return $resumen;
	}
	
	private function get_estandares()
	{
		$API = Request::factory('api/getAutoevaluacion/'.$this->token.URL::query())
			->execute()
			->body();
		
		//echo $API;die();
		//return $API;
		
		$API_estandares = json_decode($API, TRUE);
		//debug($API_estandares);
		
		$estandares = array_map(function ($estandar) {
			return $this->remove_keys($estandar);
		}, $API_estandares);
		
		//debug($estandares);
		return $estandares;
	}
	
	private function get_fuentes()
	{
		$API = Request::factory('api/getMedios/'.$this->token.URL::query())
			->execute()
			->body();
		
		//echo $API;die();
		//return $API;
		
		$API_fuentes = json_decode($API, TRUE);
		//debug($API_fuentes);
		
		$fuentes = array_map(function ($fuente) {
			return $this->remove_keys($fuente);
		}, $API_fuentes);
		
		//debug($fuentes);
		return $fuentes;
	}
	
	private function remove_keys($array)
	{
		return array_diff_key($array, array_flip($this->ignored_keys));
	}
	
}
