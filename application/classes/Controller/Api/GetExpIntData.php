<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Api_GetExpIntData extends Controller_ApiV2 {
	
	/**
	 *
	 * @var  Model_AcreditadoEvento
	 */
	public $oAcreditadoEvento;
	
	public function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('AcreditadoEvento');
	}
	
	public function action_index()
	{
		$oAcreditadoEvento = $this->oAcreditadoEvento;

		$oAcreditado = $oAcreditadoEvento->oAcreditado;

		$oTipoFinanciamiento = $oAcreditadoEvento->aTipoFinanciamientoDisponible();
		
		$aTipoFinanciamiento = Arr::assoc_to_array($oTipoFinanciamiento, ['id', 'text']);

		$institucional =
			($oAcreditado->oObjetoAcreditacion->obac_tipo_acreditacion ==
			Model_ObjetoAcreditacion::ACREDITACION_INSTITUCION);

		$universidad =
			($oAcreditado->oObjetoAcreditacion->tiin_id ==
			Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD);

		$aPersona = DB::select()
			->from('persona')
			->where('acre_id', '=', $oAcreditado->acre_id)
			->where('pers_tipo', 'in', array_keys(Model_Persona::$tipos))
			->as_assoc()
			->execute()
			->as_array();

		//return get_defined_vars();
		unset($oAcreditadoEvento, $oAcreditado);
		
		$this->send_response(get_defined_vars());
	}
	
}
