<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Api_GetEventosFEC extends Controller_ApiV2 {
	
	public function action_index()
	{
		$eventos = Repository::factory('Evento')
			->with_full_name()
			->where('even_fec', '=', Model_Evento::EVEN_FEC)
			->find_all()
			->as_json_array('even_id');
		//debug($eventos);
		
		$this->send_response($eventos);
	}
	
}
