<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Api_GetFactores extends Controller_ApiV2 {
	
	public $oEvento;
	
	public function action_index()
	{
		$aTipoInstitucion = [Model_ObjetoAcreditacion::TIPO_UNIVERSIDAD, Model_ObjetoAcreditacion::TIPO_INSTITUTO];
		
		$aFactores = A::f($aTipoInstitucion)
			->map(function($tiin_id) {
				return DB::select(DB::expr("f.*
					from factor f
					join dimension using(dime_id)
					join objeto_acreditacion using(obac_id)
					where tiin_id = {$tiin_id}
					and fact_codigo <> '10'
					group by fact_codigo
					order by fact_codigo"))
					->execute()
					->as_array();
			})
			->value();
			
		$this->response->body(json_encode($aFactores));
	}
	
}
