<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Api_GetUserData extends Controller_ApiV2 {
	
	public $oAcreditado;
	
	public $acev_list;
	
	public function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('Acreditado');
		$this->middleware('AcreditadoEventosFEC');
	}
	
	public function action_index()
	{
		//$acreditado = ['oUser' => NULL] + $this->oAcreditado->as_array();
		//unset($acreditado['oUser']);
		
		$acreditado = ['password' => NULL] + Arr::flatten($this->oAcreditado->as_array());
		
		$response = $acreditado + ['acev_list' => $this->acev_list];
		
		$this->send_response($response);
	}
	
}
