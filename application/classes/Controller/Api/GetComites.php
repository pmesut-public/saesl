<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Api_GetComites extends Controller_ApiV2 {
	
	public $oEvento;
	
	public function set_middlewares()
	{
		parent::set_middlewares();
		
		$this->middleware('Evento');
	}
	
	public function action_index()
	{
		$even_id = $this->oEvento->even_id;
		
		$comites = Repository::factory('Acreditado')
			->from_even($even_id)
			->as_array('acev_id');
		//debug($comites);
		
		$this->send_response($comites);
	}
	
}
