<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Api_GetAcpi extends Controller_ApiV2 {
	
	public function action_index()
	{
		$acpi = DB::select()
			->from('acreditacion_pip')
			->execute()
			->as_array('acpi_id');
		
		$this->send_response($acpi);
	}
	
}
