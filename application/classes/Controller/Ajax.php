<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 * @package    ProCalidad
 * @author     ProCalidad C2 Team
 * @copyright  (c) 2018 ProCalidad
 */
class Controller_Ajax extends Controller {

    public function action_getInstitution() {

        $val = $this->request->param('id');

        $query = ORM::factory('Institucion')
                //->add_name()
                ->where('inst_estado', '=', 'activo')
                //->where('inst_nivel', '=', '1')
                ->where('tiin_id', '=', $val);

        $result = $query
                ->order_by('inst_nombre')
                ->find_all()
                ->as_array('inst_id', 'inst_nombre');

        $html = $this->_build($result, 'Seleccione institución');

        $this->response->body($html);
    }

    public function action_getObac() {

        $inst_id = $this->request->param('id');

        $aObjeto = Model_ObjetoAcreditacion::get_obac_from_inst($inst_id, FALSE);

        /* $aObjeto = Model_ObjetoAcreditacion::$objetoEvaluacion; */

        $html = $this->_build_obac($aObjeto, 'Seleccione objeto de evaluacion');

        $this->response->body($html);
    }
    
    public function action_get_obac_typeacre_typeinst() {
        list($obac_tipo_acreditacion, $tiin_id) = split("-", $this->request->param('id'));
        $aObjeto = Model_ObjetoAcreditacion::get_all($tiin_id, $obac_tipo_acreditacion);
        /* $aObjeto = Model_ObjetoAcreditacion::$objetoEvaluacion; */
        $html = $this->_build($aObjeto, '--');

        $this->response->body($html);
    }
    
    /**
     * Obtiene los objetos de acreditación según la institución.
     *
     * @return response
     */
    public function action_get_tiin_objetos_institucion() {

        $tiin_id = $this->request->param('id');

        $aObjectoAcredion = Model_ObjetoAcreditacion::get_all($tiin_id, Model_ObjetoAcreditacion::ACREDITACION_INSTITUCION);

        $html = $this->_build($aObjectoAcredion, '--');

        $this->response->body($html);
    }

    /**
     * Obtiene los subtipos según la institución.
     *
     * @return response
     */
    public function action_get_subtipos_institucion()
    {
        $tiin_id = $this->request->param('id');

        $aSubtipos = Model_Institucion::get_subtiposByTiinId($tiin_id);

        $html = $this->_build($aSubtipos, '--');

        $this->response->body($html);
    }
    
    public function action_get_obac_typeacre_inst() {
        list($obac_tipo_acreditacion, $inst_id) = split("-", $this->request->param('id'));
        $institucion = ORM::factory('Institucion', $inst_id);
        $aObjeto = Model_ObjetoAcreditacion::get_all(@$institucion->tiin_id, $obac_tipo_acreditacion);
        /* $aObjeto = Model_ObjetoAcreditacion::$objetoEvaluacion; */
        $html = $this->_build($aObjeto, '--');

        $this->response->body($html);
    } 

    public function action_getCareerObac() {
        $id = $this->request->param('id');

        try {
            list($inst_id, $obac_id) = explode('-', $id);

            $aCarrera = Model_Carrera::get_from_inst_and_obac($inst_id);
        } catch (Exception $e) {
            $aCarrera = array();
        }
        //debug($aCarrera);
        $html = $this->_build($aCarrera, 'Seleccione carrera');

        $this->response->body($html);
    }

    public function action_getCareerByTipo() {
        $val = $this->request->param('id');

        $tiin_id = ORM::factory('Institucion', $val)->tiin_id;

        $carreras = DB::select('carr_nombre')
                ->from('carrera')
                ->where('inst_id', '=', $val);

        $result = ORM::factory('Carrera')
                ->join('institucion')
                ->using('inst_id')
                ->where('carr_estado', '=', 'activo')
                ->where('tiin_id', '=', $tiin_id)
                ->where('carr_nombre', 'not in', $carreras)
                ->order_by('carr_nombre')
                ->group_by('carr_nombre')
                ->find_all()
                ->as_array('carr_id', 'carr_nombre');

        $html = $this->_build($result, 'Seleccione carrera');

        $html .= HTML::tag('option', 'Otra...', array('value' => '0'));

        $this->response->body($html);
    }

    public function action_getCareer() {
        $val = $this->request->param('id');

        $result = ORM::factory('Carrera')
                //->join('institucion_carrera')
                //->on('carrera.carr_id', '=', 'institucion_carrera.carr_id')
                ->where('carr_estado', '=', 'activo')
                ->where('inst_id', '=', $val)
                ->order_by('carr_nombre')
                ->find_all()
                ->as_array('carr_id', 'carr_nombre');

        $html = $this->_build($result, 'Seleccione carrera');

        $this->response->body($html);
    }

    public function action_getCareerType() {
        $val = $this->request->param('id');

        $result = ORM::factory('ObjetoAcreditacion')
                ->where('obac_estado', '=', 'activo')
                ->where('tiin_id', '=', $val)
                ->find_all()
                ->as_array('obac_id', 'obac_nombre');

        $html = $this->_build($result, 'Tipo de Institución/Carrera');

        $this->response->body($html);
    }

    public function action_getDimension() {
        $val = $this->request->param('id');
        $val = explode('-', $val);

        $result = ORM::factory('Dimension')
                ->where('dime_estado', '=', 'activo')
                ->where('obac_id', 'IN', $val)
                ->find_all();
        //->as_array('fact_id', 'fact_titulo');

        $html = $this->_build_title($result);

        $this->response->body($html);
    }

    public function action_getFactor() {
        $val = $this->request->param('id');
        $val = explode('-', $val);

        $result = ORM::factory('Factor')
                ->where('fact_estado', '=', 'activo')
                ->where('dime_id', 'IN', $val)
                ->find_all();
        //->as_array('fact_id', 'fact_titulo');

        $html = $this->_build_title($result);

        $this->response->body($html);
    }

    public function action_getCriterion() {
        $val = $this->request->param('id');
        $val = explode('-', $val);

        $result = ORM::factory('Criterio')
                ->where('crit_estado', '=', 'activo')
                ->where('fact_id', 'IN', $val)
                ->find_all();
        //->as_array('crit_id', 'crit_titulo');

        $html = $this->_build_title($result);

        $this->response->body($html);
    }

    public function action_getStandard() {
        $val = $this->request->param('id');
        $val = explode('-', $val);

        $result = ORM::factory('Estandar')
                ->where('esta_estado', '=', 'activo')
                //->where('crit_id', 'IN', $val)
                ->find_all();
        //->as_array('esta_id', 'esta_titulo');

        $html = $this->_build_title($result);

        $this->response->body($html);
    }

    public function action_getStandardByModaId() {
        $oAcreditado = Session::instance()->get('oAcreditado');

        $moda_id = $oAcreditado ? $oAcreditado->moda_id : NULL;

        $val = $this->request->param('id');
        $val = explode('-', $val);

        $result = ORM::factory('Estandar')
                ->where('esta_estado', '=', Model_Saes::STATUS_ACTIVO)
                //->where('crit_id', 'IN', $val)
                //->where('moda_id', '<=', $moda_id)
                ->find_all();
        //->as_array('esta_id', 'esta_titulo');

        $html = $this->_build_title($result);

        $this->response->body($html);
    }

    public function action_getConcept() {
        $val = $this->request->param('id');
        $val = explode('-', $val);

        $result = ORM::factory('Concepto')
                ->where('conc_estado', '=', 'activo')
                ->where('esta_id', 'IN', $val)
                ->find_all();
        //->as_array('esta_id', 'esta_titulo');

        $html = View::factory('admin/cumplimiento/concepts')
                ->set('aConcepto', $result);

        $this->response->body($html);
    }

    public function action_get_tiin_dimensiones() {
        $tiin_id = $this->request->param('id');

        $aDimension = Model_ObjetoAcreditacion::get_dimensiones($tiin_id);

        $html = $this->_build($aDimension, '--');

        $this->response->body($html);
    }

    public function action_get_tiin_factores() {
        $tiin_id = $this->request->param('id');

        $aFactor = Model_ObjetoAcreditacion::get_factores($tiin_id);

        $html = $this->_build($aFactor, '--');

        $this->response->body($html);
    }

    public function action_get_obac_criterios() {
        $obac_id = $this->request->param('id');

        $aCriterio = Model_ObjetoAcreditacion::get_criterios($obac_id);

        $html = $this->_build($aCriterio, '--');

        $this->response->body($html);
    }

    public function action_get_tiin_estandares() {
        $tiin_id = $this->request->param('id');

        $aEstandar = Model_ObjetoAcreditacion::get_estandares($tiin_id);

        $html = $this->_build($aEstandar, '--');

        $this->response->body($html);
    }

    public function action_get_eventos() {
        $tiin_id = $this->request->param('id');

        $aEvento = $tiin_id ? Model_Evento::get_all($tiin_id, Model_Evento::TIPO_CONVOCATORIA) : [];

        $html = $this->_build($aEvento, '--');

        $this->response->body($html);
    }

    public function action_get_parent() {
        $tiin_id = $this->request->param('id');

        $aParent = $tiin_id ? Model_Institucion::get_parent($tiin_id) : [];

        $html = $this->_build($aParent, '--');

        $this->response->body($html);
    }

    protected function _build($result, $default = NULL) {
        $html = $this->request->query('multiple') ? '' : HTML::default_option($default);

        foreach ($result as $key => $val) {
            $html .= HTML::tag('option', $val, array('value' => $key));
        }
        return $html;
    }

    protected function _build_obac($aObjeto, $default = NULL) {
        $html = HTML::default_option($default);
        $s = 0;
        $c = 0;
        foreach ($aObjeto as $oObjeto) {
            $oObjeto->obac_nombre = ($oObjeto->obac_tipo_acreditacion == 1) ? 'Sede' : 'Carrera';
            if ($oObjeto->obac_tipo_acreditacion == 1 && $s == 0) {
                $html .= HTML::tag('option', $oObjeto->obac_nombre, array('value' => $oObjeto->inst_obac, 'data-tipo' => $oObjeto->obac_tipo_acreditacion));
                $s = 1;
            }
            if ($oObjeto->obac_tipo_acreditacion == 2 && $c == 0) {
                $html .= HTML::tag('option', $oObjeto->obac_nombre, array('value' => $oObjeto->inst_obac, 'data-tipo' => $oObjeto->obac_tipo_acreditacion));
                $c = 1;
            }
        }
        return $html;
    }

    protected function _build_title($result) {
        $html = $this->request->query('multiple') ? '' : HTML::default_option();

        foreach ($result as $object) {
            $html .= HTML::tag('option', $object->title(), array('value' => $object->pk(), 'title' => $object->titulo()));
        }
        return $html;
    }
    
    public function action_get_instituciones_by_nivel()
    {
        $parent_id = $this->request->param('id');
        $aInstituciones = array();

        if(isset($parent_id))
        {
            $aInstituciones = Model_Institucion::get_by_nivel($parent_id);
        }

        $html = $this->_build($aInstituciones, '--');

        $this->response->body($html);
    }

}

// End Controller_Template
