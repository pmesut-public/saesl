<?php

defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Controller_Welcome extends Controller_Template
{

    public $template = 'welcome/template';
    protected $_oUser;
    protected $_config;
    protected $_error;

    public function before()
    {
        $this->_check_ajax();

        parent::before();

        $this->_set_controller_globals();
        $this->_set_view_globals();
        $this->_set_template_blocks();
    }

    /**
     * En primera instancia verifica si hay sesión iniciada, y redirecciona de acuerdo al Role.
     * Si encuentra una petición POST, valida el usuario y el password y redirecciona,
     * en caso no sea válido envia un mensaje de error y devuelve al login.
     *
     * @return View 
     */
    public function action_index()
    {
        $this->template = 'welcome/template-login';
        
        $this->before();

        $this->_check_login();

        if ($this->request->method() == 'POST') {

            if (Auth::instance()->login($this->request->post('username'), $this->request->post('password'))) {

                $this->_check_login();

                exit;
            }

            $this->_error = 'Usuario o contraseña incorrectos';
        }

        $url  = $this->request->param('id') ? : 'eKQqPRAj2m4';

        $view = View::factory('welcome/login')
            ->set('titu', '¿Cómo registrarse en el <span>SAES licenciamiento?')
            ->set('url', $url);

        $view->error = $this->_error;

        $this->template->content = $view;
    }

    /*
     * Olvidaste tu Contraseña
     * Valida en primera instancia si hay alguna sesión activa
     *
     *
     */

    public function action_forgot()
    {
        $this->_check_login();

        $message = 'Se le enviará un correo con instrucciones para reestablecer su contraseña';

        if ($this->request->method() == 'POST') {
            $mail = $this->request->post('email');

            try {
                ORM::factory('User')->reset_password($mail, $this->_config);

                $message = 'Su nueva contraseña fue enviada a su correo';
            } catch (Exception_Saes $e) {
                $this->_error = $e->getMessage();
            }
        }

        $view = View::factory('welcome/forgot')
            ->set(compact('message'))
            ->set('error', $this->_error);

        $this->template->content = $view;
    }

    /**
     * Vista de Registro de la Herramienta.
     *
     * @return View
     */
    public function action_register()
    {
        $aInstitucion = Model_Institucion::get_all();

        $aModalidad = ORM::factory('Modalidad')
            ->where('moda_estado', '=', 'activo')
            ->find_all()
            ->as_array('moda_id', 'moda_nombre');

        $aTipoInstitucion = Model_TipoInstitucion::get_all();

        if ($this->request->method() == 'POST') {

            $this->_save_acreditado();
        }

        $view = View::factory('welcome/register')
            ->set(compact('aInstitucion', 'aModalidad', 'aTipoInstitucion'))
            ->set('error', $this->_error);
            
        $this->template->content = $view;
    }

    protected function _save_acreditado()
    {
        $post = $this->request->post();        
        list($inst_id, $obac_id) = explode('-', $post['obac_id']);
        $oInstitucion = ORM::factory('Institucion', $post['inst_id']);
        if (!empty($post['carr_id'])) {
            $oCarrera = ORM::factory('Carrera', $post['carr_id']);
            $post['obac_id'] = $oCarrera->carr_obac_id;
        } else {
            $post['obac_id'] = $obac_id;
        }

        try {
            $oAcreditado = Model_Acreditado::create_new($post);            
            /*mail($this->_config->admin_mail,
              'Procalidad - Nuevo usuario',
              'Usuario: '.$oAcreditado->oUser->username.M_EOL
              .'Institución/Carrera: '.$oAcreditado->subtitle());*/

            $msg = "Usuario: {$oAcreditado->oUser->username}".M_EOL;
            $msg .= "Institución: {$oInstitucion->inst_nombre} - {$oInstitucion->inst_region}".M_EOL;
            @$oCarrera ? $msg .= "Carrera: {$oCarrera->carr_nombre}".M_EOL : null;

            $email = Email::factory('SAESL - Nuevo usuario',$msg)
                ->to($this->_config->admin_mail)
                ->from($this->_config->admin_mail, 'SAESL')
                ->send();

            $this->redirect('/welcome/thanks');
        } catch (ORM_Validation_Exception $e) {
            //debug($e);
            if(@$post['carr_id']){
                $this->_error = "Ya existe un Comite de calidad para la carrera: ".strtoupper($oCarrera->carr_nombre). " de la institucion: ". strtoupper($oInstitucion->inst_nombre);
            }else{
                $this->_error = "Ya existe un Comite de calidad para la institución: ". strtoupper($oInstitucion->inst_nombre);
            }
//            $this->_error = $e->message();
        }
    }

    public function action_carrera()
    {
        $aInstitucion = Model_Institucion::get_all();

        if ($this->request->method() == 'POST') {
            //debug($_POST);

            $post = $this->request->post();

            if ($post['carr_id'] !== '0') {
                $post['carr_nombre'] = ORM::factory('Carrera', $post['carr_id'])
                    ->carr_nombre;
            }

            try {
                $oCarrera = ORM::factory('Carrera')
                    ->values($post, array('carr_nombre', 'inst_id'))
                    ->create();

                Session::instance()->set('info', 'Se registró su solicitud. Un administrador lo revisará en los próximos minutos');
            } catch (ORM_Validation_Exception $e) {
                //$error = $e->errors('');
                //$error = is_array(current($error)) ? current(current($error)) : current($error);

                Session::instance()->set('error', $e->message());
            }
        }

        $view = View::factory('welcome/carrera')
            ->set(compact('aInstitucion'));

        $this->template->content = $view;
    }

    // @ajax
    public function action_uniqueUser()
    {
        $val = $this->request->param('id');

        $valid = ORM::factory('User')->unique('username', $val);

        $response = array(
            'status' => $valid ? 'ok' : 'error',
            'msg' => 'Ese usuario ya existe',
        );

        $this->response->body(json_encode($response));
    }

    public function action_thanks()
    {
        $view = View::factory('welcome/thanks');

        $this->template->content = $view;
    }

    public function action_salir()
    {
        Auth::instance()->logout(TRUE);
        $this->redirect('/');
    }

    /*
     * Verifica si hay peticiones ajax
     * Si hubiera auto_render = FALSE , no renderiza el login.
     */

    protected function _check_ajax()
    {
        if ($this->request->is_ajax())
            $this->auto_render = FALSE;
    }

    /*
     * Setea data en variables de clase
     * oUser = (información del usuario)
     * _config = (información general del SAES)
     */

    protected function _set_controller_globals()
    {
        $this->_oUser = Auth::instance()->get_user();
        //debug($this->_oUser);
        $this->_config = Kohana::$config->load('general');
    }

    /*
     * Setea en variable global de la vista
     * site_title = (titulo del SAES)
     */

    protected function _set_view_globals()
    {
        View::bind_global('site_title', $this->_config->site_title);
    }

    /*
     * Verifica si se renderiza la vista
     * Añade los templates a ser renderizados
     */

    protected function _set_template_blocks()
    {
        if ($this->auto_render) {
            $this->template->styles = View::factory('welcome/styles');
            $this->template->scripts = View::factory('welcome/scripts');
            $this->template->header = View::factory('welcome/header');
            $this->template->footer = View::factory('welcome/footer');
        }
    }

    /*
     * Verifica si hay alguna instancia de sesión
     * Obtiene el role y de acuerdo a esto redirecciona al Controller adecuado.
     */

    protected function _check_login()
    {
        $auth = Auth::instance();

        if ($oUser = $auth->get_user()) {
            // Backend (TRAE EL ROL DEL USUARIO INGRESANTE)
            if (ACL::instance()->get_role() != 'regular') {
                $this->redirect('/admin');
            } // Regular
            else {
                $oAcreditado = $oUser->oAcreditado;


                if (!$oAcreditado->loaded()) {
                    throw new Exception_Saes('Regular user with no acreditado assigned');
                }

                Session::instance()->set('oAcreditado', $oAcreditado);

                $this->redirect('/autoevaluaciones');
            }
        }
    }

    public static function redirect($uri = '', $code = 302)
    {
        if ($redirect = Request::$current->query('r'))
            $uri = $redirect;

        parent::redirect($uri, $code);
    }

}

// End Welcome
