<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_ApiToken extends Middleware {
	
	public function execute()
	{
		if ($this->controller->request->param('id') !== $this->controller->token)
		{
			throw new Middleware_Exception_ApiToken();
		}
	}
	
}
