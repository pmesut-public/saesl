<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_Autoevaluacion extends Middleware {
	
	public function execute()
	{
		$auto_id = $this->controller->request->query('auto_id');
		//debug($auto_id);
		
		$this->controller->oAutoevaluacion = Repository::factory('Autoevaluacion')
			->with_resumen()
			->where('auto_id', '=', $auto_id)
			->find();
		
		if ( ! $this->controller->oAutoevaluacion->loaded())
		{
			throw new Middleware_Exception_ModelNotFound("", [':model' => 'Autoevaluacion', ':id' => $auto_id]);
		}
	}
	
}
