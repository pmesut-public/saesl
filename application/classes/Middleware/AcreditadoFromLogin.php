<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoFromLogin extends Middleware {
	
	public function execute()
	{
		$post = $this->controller->request->post();
		
		$this->controller->oAcreditado = Repository::factory('Acreditado')
			->with_titles()
			->filter()
			->where('username', '=', $post['username'])
			->where('password', '=', Auth::instance()->hash($post['password']))
			->find(FALSE);
		
		if ( ! $this->controller->oAcreditado->loaded())
		{
			throw new Middleware_Exception_AcreditadoLogin();
		}
	}
	
}
