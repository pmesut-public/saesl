<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_Exception_AcreditadoNoEventosFEC extends Middleware_Exception {
	
	protected $api_code = 3;
	
	protected $message =
		'Usted no ha participado en ningún evento convocatoria FEC.
		Si considera que esto es un error, 
		por favor contáctese con un administrador.';
	
}
