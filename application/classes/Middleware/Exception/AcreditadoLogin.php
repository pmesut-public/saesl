<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_Exception_AcreditadoLogin extends Middleware_Exception {
	
	public function __construct($message = "", array $variables = NULL, $code = 0, \Exception $previous = NULL)
	{
		parent::__construct();
	}
	protected $api_code = 2;
	protected $message = 'Usuario o contraseña inválidos';
	
}
