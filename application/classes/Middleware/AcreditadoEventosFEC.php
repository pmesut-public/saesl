<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEventosFEC extends Middleware {
	
	public function execute()
	{
		$acre_id = $this->controller->oAcreditado->acre_id;
		
		$this->controller->acev_list = Repository::factory('Acreditado', $acre_id)
			->fec_list()
			->execute()
			->as_array('acev_id');
		
		if ( ! $this->controller->acev_list)
		{
			throw new Middleware_Exception_AcreditadoNoEventosFEC;
		}
	}
	
}
