<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_AcreditadoEvento extends Middleware {
	
	public function execute()
	{
		$acev_id = $this->controller->request->query('acev_id');
		
		$this->controller->oAcreditadoEvento = ORM::factory('AcreditadoEvento', $acev_id);
		
		if ( ! $this->controller->oAcreditadoEvento->loaded())
		{
			throw new Middleware_Exception_ModelNotFound("", [':model' => 'AcreditadoEvento', ':id' => $acev_id]);
		}
	}
	
}
