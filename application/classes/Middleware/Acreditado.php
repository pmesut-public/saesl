<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Middleware_Acreditado extends Middleware {
	
	public function execute()
	{
		$acre_id = $this->controller->request->query('acre_id');
		
		$this->controller->oAcreditado = Repository::factory('Acreditado')
			->with_titles()
			->where('acre_id', '=', $acre_id)
			->find(FALSE);
		
		if ( ! $this->controller->oAcreditado->loaded())
		{
			throw new Middleware_Exception_ModelNotFound("", [':model' => 'Acreditado', ':id' => $acre_id]);
		}
	}
	
}
