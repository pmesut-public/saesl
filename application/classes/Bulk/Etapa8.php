<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * 8.	Cálculos
 * 
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Etapa8 extends Bulk_Etapa7 {

	public function process($file)
	{
		$filename = APPPATH."estandares/etapa8_{$this->_obac_id}.csv";
		
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($this->_data);
		
		$log = LogTable::factory(['border' => 1]);
		
		if (file_exists($filename))
		{
			echo 'Changes already made';
			
			$log->read($filename);
			
			echo $log->show();
			return;
		}
		
		if ( ! in_array($this->_obac_id, [22,23,24]))
		{
			$this->process_univ($filename);
		}
		else
		{
			$this->process_inst($filename);
		}
	}
	
	public function process_univ($filename)
	{
		$log = LogTable::factory(['border' => 1]);
		
		$aAutoevaluacion = $this->get_autoevaluaciones();
		
		foreach ($aAutoevaluacion as $oAutoevaluacion)
		{
			$aDetalle = $oAutoevaluacion
				->aDetalle
				->with('oEstandar')
				->select('esta_codigo')
				->select(DB::expr('group_concat(code_calidad) calidad'))
				->join('concepto_detalle', 'left')->using('aude_id')
				->where('aude_estado', '=', 'proceso')
				->group_by('aude_id')
				->find_all();
				//debug2($aDetalle);
			
			//$old_porcentaje = $oAutoevaluacion->result('per_aprobados', FALSE);
			
			$changes = $cumplidos = 0;
			
			foreach ($aDetalle as $oDetalle)
			{
				$quality = explode(',', $oDetalle->calidad);
				
				$old_cumplimiento = $oDetalle->aude_cumplimiento;
				$old_quality = $oDetalle->aude_calidad;
				
				$log->partial([
					'obac' => $this->_obac_id,
					'aude_id' => $oDetalle->aude_id,
					'esta_id' => $oDetalle->esta_id,
					'esta_codigo' => $oDetalle->esta_codigo,
					'qualities' => $oDetalle->calidad,
				]);
				
				$oDetalle->set_cumplimiento(1);
				$oDetalle->set_quality($quality);
				
				$new_cumplimiento = $oDetalle->aude_cumplimiento;
				$new_quality = $oDetalle->aude_calidad;
				
				$log->partial(compact('old_cumplimiento', 'new_cumplimiento', 'old_quality', 'new_quality'));
				
				if ($new_cumplimiento)
				{
					$cumplidos++;
				}
				
				if ($old_cumplimiento == $new_cumplimiento AND $old_quality == $new_quality)
				{
					//$log->partial(['foo' => '*']);
					$log->clean();
					continue;
				}
				
				$changes++;
				
				$log->log();
				
				if ($this->save)
				{
					$oDetalle->update();
				}
			}
			
			$old_porcentaje = numberformat($oAutoevaluacion->cumplidos / $oAutoevaluacion->audes * 100, 2);
			$new_porcentaje = numberformat($cumplidos / $oAutoevaluacion->audes * 100, 2);
			
			if ($changes)
			{
				$log->log([
					'obac' => $this->_obac_id,
					'auto_id' => $oAutoevaluacion->auto_id,
					'old_porcentaje' => $old_porcentaje,
					'new_porcentaje' => $new_porcentaje,
				]);
			}
		}
		
		echo $log->show();
		
		if ($this->save)
		{
			$log->write($filename);
			
			//HTTP::redirect(Request::current()->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
	}
	
	public function process_inst($filename)
	{
		$log = LogTable::factory(['border' => 1]);
		
		$aAutoevaluacion = $this->get_autoevaluaciones();
		
		$aCumplimiento = ORM::factory('Cumplimiento')
			->with('oEstandar:oCriterio:oFactor:oDimension')
			->select(DB::expr('group_concat(cump_condicion) cumplimientos'))
			->where('obac_id', '=', $this->_obac_id)
			->group_by('esta_id')
			->find_all()
			->as_array('esta_id', 'cumplimientos');
			//debug($aCumplimiento);
		
		foreach ($aAutoevaluacion as $oAutoevaluacion)
		{
			$aDetalle = $oAutoevaluacion
				->aDetalle
				->select('esta_codigo')
				->select(DB::expr('group_concat(code_calidad order by cast(substr(conc_codigo, 3) as signed)) calidades'))
				->select(DB::expr("group_concat(conc_codigo order by cast(substr(conc_codigo, 3) as signed)) codigos"))
				->select(DB::expr("group_concat(coalesce(code_cumple, 0) order by cast(substr(conc_codigo, 3) as signed) separator '') condiciones"))
				->with('oEstandar')
				->join('concepto')
					->on('oEstandar.esta_id', '=', 'concepto.esta_id')
				->join('concepto_detalle', 'left')
					->on(DB::expr(''), '', DB::expr("concepto.conc_id = concepto_detalle.conc_id and concepto_detalle.aude_id = autoevaluaciondetalle.aude_id"))
				->where('aude_estado', '=', 'proceso')
				->group_by('aude_id')
				->find_all();
				//debug2($aDetalle, $aDetalle->current()->as_array(), 'die');
			
			//$old_porcentaje = $oAutoevaluacion->result('per_cumplidos', FALSE);
			
			$changes = $cumplidos = 0;
			
			foreach ($aDetalle as $oDetalle)
			{
				//debug($oDetalle->calidades);
				//$quality = $oDetalle->calidades ? explode(',', $oDetalle->calidades) : [];
				$quality = explode(',', $oDetalle->calidades);
				
				$old_aceptacion = $oDetalle->aude_aceptacion;
				$old_cumplimiento = $oDetalle->aude_cumplimiento;
				$old_quality = $oDetalle->aude_calidad;
				
				$cumplimientos = $aCumplimiento[$oDetalle->esta_id];
				
				$cump_actual = $this->get_cump_actual($cumplimientos, $oDetalle->aude_aceptacion);
				
				$log->partial([
					'obac' => $this->_obac_id,
					'auto_id' => $oAutoevaluacion->auto_id,
					'aude_id' => $oDetalle->aude_id,
					'esta_id' => $oDetalle->esta_id,
					'esta_codigo' => $oDetalle->esta_codigo,
					'min_aceptacion' => $oDetalle->oEstandar->esta_nivel_aceptacion,
					'calidades' => $oDetalle->calidades,
					'condiciones' => $oDetalle->condiciones,
					'cumplimientos' => $cumplimientos,
					'cump_actual' => $cump_actual,
				]);
				
				$cumple = $this->cumple($oDetalle->condiciones, $cump_actual);
				
				$new_aceptacion = $cumple ? $old_aceptacion : $this->get_nivel($oDetalle->condiciones, $cumplimientos);
				
				$oDetalle->aude_aceptacion = $new_aceptacion;
				
				$log->partial(['cumple' => $cumple ?: 0]);
				
				$oDetalle->set_cumplimiento(3, $new_aceptacion);
				$oDetalle->set_quality($quality);
				
				$new_cumplimiento = $oDetalle->aude_cumplimiento;
				$new_quality = $oDetalle->aude_calidad;
				
				$log->partial(compact('old_aceptacion', 'new_aceptacion'));
				$log->partial(compact('old_cumplimiento', 'new_cumplimiento', 'old_quality', 'new_quality'));
				
				if ($new_cumplimiento)
				{
					$cumplidos++;
				}
				
				if ($old_aceptacion == $new_aceptacion AND $old_cumplimiento == $new_cumplimiento AND $old_quality == $new_quality)
				{
					//$log->partial(['foo' => '*']);
					$log->clean();
					continue;
				}
				
				$changes++;
				
				$log->log();
				
				if ($this->save)
				{
					$oDetalle->update();
				}
			}
			
			$old_porcentaje = numberformat($oAutoevaluacion->cumplidos / $oAutoevaluacion->audes * 100, 2);
			$new_porcentaje = numberformat($cumplidos / $oAutoevaluacion->audes * 100, 2);
			//$new_porcentaje = $oAutoevaluacion->result('per_cumplidos', FALSE);
			
			if ($changes)
			{
				$log->log([
					'obac' => $this->_obac_id,
					'auto_id' => $oAutoevaluacion->auto_id,
					'old_porcentaje' => $old_porcentaje,
					'new_porcentaje' => $new_porcentaje,
				]);
			}
		}
		
		echo $log->show();
		
		if ($this->save)
		{
			$log->write($filename);
			
			//HTTP::redirect(Request::current()->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
	}
	
	private function get_cump_actual($cumplimientos, $aude_aceptacion)
	{
		return Arr::get(explode(',', $cumplimientos), $aude_aceptacion - 1);
	}
	
	private function cumple($nivel, $condicion)
	{
		//debug2($nivel, $condicion);
		
		$len_niv = strlen($nivel);
		$len_con = strlen($condicion);

		if ($len_niv < $len_con)
		{
			die('condiciones <> length');
		}
		
		for ($i = 0; $i < strlen($nivel); $i++)
		{
			if ($nivel[$i] < $condicion[$i]) 
				return FALSE;
		}
		
		return TRUE;
	}
	
	private function get_nivel($nivel, $all_cump)
	{
		//debug2($all_cump, $nivel);
		
		$cumps = explode(',', $all_cump);
		$i = count($cumps);
		
		while ($i)
		{
			if ($this->cumple($nivel, $cumps[--$i]))
			{
				return ++$i;
			}
		}
	}
	
}
