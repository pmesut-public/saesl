<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_IEP extends Bulk {

	protected $save;
	
	protected $_obac_id;
	
	protected $_moda_id;
	
	protected $filename;
	
	private $max;
	
	public function method($method)
	{
		$this->save = $method == 'POST' ? TRUE : FALSE;
		
		return $this;
	}
	
	public function obac($obac_id)
	{
		$this->_obac_id = $obac_id;
		return $this;
	}
	
	public function moda($moda_id)
	{
		$this->_moda_id = $moda_id;
		return $this;
	}
	
	public function name($name)
	{
		$modas = [
			1 => 'iniciacion-al-cambio',
			2 => 'camino-a-la-excelencia',
		];
		
		$this->filename = APPPATH."iep/".URL::title($name, '-', TRUE).'_'.$modas[$this->_moda_id].'.csv';
		//debug($this->filename);
		
		return $this;
	}
	
	public function process($file)
	{
		//$filename = APPPATH."iep/obac_{$this->_obac_id}_{$this->_moda_id}.csv";
		
		if ( ! $this->filename)
		{
			die('No filename set');
		}
		
		$log = LogTable::factory(['border' => 1]);
		
		if (file_exists($this->filename))
		{
			echo 'File already exists';
			
			$log->read($this->filename);
			
			echo $log->show();
			return;
		}
		
		$r = Report::factory('last_auto_reporte_iep', [
				'_obac_id' => $this->_obac_id,
				'_moda_id' => $this->_moda_id,
				'_date' => '2016-06-30',
			])
			->get_data();
		
		$this->set_max();
		
		$head = $this->get_head($r);
		
		$body = $this->get_body($r, $head);
		
		$log->fill($head, $body);
		
		echo $log->show();
		
		if ( ! $body)
		{
			echo 'No autos';
			return;
		}
		
		if ($this->save)
		{
			$log->write($this->filename);
		}
		
		echo Form::open().Form::submit(NULL, 'write').Form::close();
	}
	
	private function set_max()
	{
		$max = DB::select(DB::expr("count(conc_id) fuentes
				from concepto co
				join estandar e using(esta_id)
				join criterio c using(crit_id)
				join factor f using(fact_id)
				join dimension d using(dime_id)
				where obac_id = {$this->_obac_id}
				group by esta_id
				order by fuentes desc"))
			->execute()
			->get('fuentes');
		
		//debug($max);
		$this->max = $max;
	}
	
	private function get_head($r)
	{
		$estandares = TreeArray::factory($r->as_array(), ['esta'])->get();
		//debug2($estandares);
		
		//uasort($estandares, array($this, 'compare'));
		//debug($estandares);
		
		$head = [
			'Cod auto',
			'Tipo',
			'Modalidad',
			'Región',
			'Gestión',
			'Institución',
			'Carrera',
			'Fecha inicio',
			'Fecha cierre',
		];
		
		foreach ($estandares as $key => $val)
		{
			//debug($val);
			$head[] = 'D'.$val['dime_codigo'];
			$head[] = 'F'.$val['fact'];
			$head[] = 'E'.$key;
			$head[] = 'C_'.$key;
			$head[] = 'P_'.$key;
			
			for ($i = 0; $i < $this->max; $i++)
			{
				$head[] = 'FV'.($i + 1).'_'.$key;
			}
		}
		$head[] = 'Porcentaje';
		
		//debug($head);
		return $head;
	}
	
	/*private function compare($a, $b)
	{
		if ($a['esta_id'] == $b['esta_id'])
		{
			return 0;
		}
		
		return ($a['esta_id'] < $b['esta_id']) ? -1 : 1;
	}*/

	private function get_body($r, $head)
	{
		$autos = TreeArray::factory($r, ['auto_id', 'esta', 'conc_codigo'])->get();
		//debug($autos);
		
		$body = [];
		foreach ($autos as $auto => $estandares)
		{
			$data = current(current($estandares));
			//debug($data);
			
			$row = [
				'Cod auto'		=>	$auto,
				'Tipo'			=>	$data['tiin_nombre'].' - '.$data['obac_nombre'],
				'Modalidad'		=>	$data['moda_nombre'],
				'Región'		=>	$data['inst_region'],
				'Gestión'		=>	$data['inst_gestion'],
				'Institución'	=>	$data['acre_id'],
				'Carrera'		=>	$data['carr_nombre'],
				'Fecha inicio'	=>	$data['auto_fecha_inicio'],
				'Fecha cierre'	=>	$data['auto_fecha_fin'],
			];
			//debug($row);
			
			foreach ($estandares as $estandar => $fuentes)
			{
				$data = current($fuentes);
				//debug($fuentes);
				
				$row['D'.$data['dime_codigo']] = $data['dime_codigo'];
				$row['F'.$data['fact']] = $data['fact_codigo'];
				$row['E'.$estandar] = $estandar;
				$row['C_'.$estandar] = $data['cumplimiento'];
				$row['P_'.$estandar] = $data['puntaje'];
				
				for ($i = 0; $i < $this->max; $i++)
				{
					$fuente = Arr::get($fuentes, 'FV' . ($i + 1));
					
					$row['FV'.($i + 1).'_'.$estandar] = Arr::get($fuente, 'cumple_fuente', 0);
				}
				
				$per = $data['per'];
			}
			$row['Porcentaje'] = numberformat($per, 2);
			
			//debug($row);
			$body[] = $row;
			//debug2($head, $row, 'die');
			//$body[] = array_combine($head, $row);
		}
		
		//debug($body);
		return $body;
	}
	
	protected function _check_file($file)
	{
		if ( ! file_exists($file))
		{
			throw new Exception_Saes('Archivo inválido.');
		}
	}
	
	protected function _get_data($file)
	{
		parent::_get_data(['tmp_name' => $file]);
	}
	
}
