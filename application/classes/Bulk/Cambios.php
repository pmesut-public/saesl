<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Cambios extends Bulk {

	protected $save;
	
	protected $estandares = [];
	
	protected $autoevaluaciones = [];
	
	public function method($method)
	{
		$this->save = $method == 'POST' ? TRUE : FALSE;
		
		return $this;
	}
	
	public function show($array)
	{
		$html = '<table border="1"><tbody>';
		
		foreach ($array as $key => $val)
		{
			$html .= "<tr><td>$key</td><td>$val</td></tr>";
		}
		
		return $html.'</tbody></table>';
	}
	
	protected function _check_file($file)
	{
		if ( ! file_exists($file))
		{
			throw new Exception_Saes('Archivo inválido.');
		}
	}
	
	protected function _get_data($file)
	{
		parent::_get_data(['tmp_name' => $file]);
	}
	
	protected function get_estandares($obac_id = NULL)
	{
		$obac_id = $obac_id ?: $this->_obac_id;
		
		if ( ! $estandares = Arr::get($this->estandares, $obac_id))
		{
			$this->estandares[$obac_id] = ORM::factory('Estandar')
				->with('oCriterio:oFactor:oDimension')
				->where('obac_id', '=', $obac_id)
				->find_all()
				->as_array('esta_codigo', 'esta_id');
		}
		
		return $this->estandares[$obac_id];
	}
	
	protected function get_autoevaluaciones($obac_id = NULL)
	{
		$obac_id = $obac_id ?: $this->_obac_id;
		
		if ( ! $autoevaluaciones = Arr::get($this->autoevaluaciones, $obac_id))
		{
			return ORM::factory('Autoevaluacion')
				->with('oAcreditadoEvento:oAcreditado')
				->join('autoevaluacion_detalle')->using('auto_id')
				->select(DB::expr('sum(if(aude_cumplimiento = 1,1,0)) cumplidos'))
				->select(DB::expr('count(aude_id) audes'))
				//->with('oConcepto')
				//->with('oAutoevaluacionDetalle:oEstandar:oCriterio:oFactor:oDimension')
				->where('obac_id', '=', $obac_id)
				->where('auto_estado', '<>', Model_Autoevaluacion::STATUS_CANCELADO)
				//->where('conc_codigo', '=', $conc_codigo)
				->group_by('auto_id')
				->order_by('auto_id')
				->find_all();
		}
		
		return $this->autoevaluaciones[$obac_id];
	}
	
	/*protected function get_conceptos()
	{
		return ORM::factory('Estandar')
			->with('oCriterio:oFactor:oDimension')
			->where('obac_id', '=', $this->_obac_id)
			->find_all()
			->as_array('esta_codigo', 'esta_id');
	}*/
	
	protected function write($name, $log)
	{
		if ($log)
		{
			$file = APPPATH."estandares/{$name}_{$this->_obac_id}.txt";
			
			file_put_contents($file, json_encode($log));
			debug2('log creado: '.$file);
		}
	}
	
}
