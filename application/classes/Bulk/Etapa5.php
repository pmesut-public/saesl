<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * 5.	Transposition
 *		FV3 >> FV2
 * 
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Etapa5 extends Bulk_Cambios {

	public function process($file)
	{
		$filename = APPPATH.'estandares/etapa5_log.csv';
		
		$this->_check_file($file);
		
		$this->_get_data($file);
		
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($this->_data);
		
		$log = LogTable::factory(['border' => 1]);
		
		if (file_exists($filename))
		{
			echo 'Changes already made';
			
			$log->read($filename);
			$log->stop();
		}
		
		foreach ($this->_data as $row)
		{
			extract($row);
			
			if ($STEP != 5) continue;
			
			if ($DONE) continue;
			
			$estandares = $this->get_estandares($obac_id);
			
			//debug2($row);
			$matches = [];
			preg_match('/([^\s>]+)\s*>>\s*([^\s]+)/', $A, $matches);
			
			//debug($matches);
			
			$first = $matches[1];
			$second = $matches[2];
			
			$firstConcepto = ORM::factory('Concepto')
				->where('conc_codigo', '=', $first)
				->where('esta_id', '=', $estandares[$esta_id])
				->find();
			
			$secondConcepto = ORM::factory('Concepto')
				->where('conc_codigo', '=', $second)
				->where('esta_id', '=', $estandares[$esta_id])
				->find();
			
			$log->partial([
				'obac_id' => $obac_id,
				'esta_id' => $estandares[$esta_id],
				'esta_codigo' => $esta_id,
				'f_code' => $first,
				'f_concepto' => $firstConcepto->conc_id,
				's_code' => $second,
				's_concepto' => $secondConcepto->conc_id,
			]);
			
			if ( ! $firstConcepto->loaded()) die('wtf');
			
			if ($secondConcepto->loaded())
			{
				$log->partial(['' => '*']);
			}
			
			$log->log();
			
			if ($this->save)
			{
				$firstConcepto->conc_codigo = $second;
				$firstConcepto->update();
			}
		}
		
		echo $log->show();
		
		if ($this->save)
		{
			$log->write($filename);
			
			HTTP::redirect(Request::current()->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
	}
	
}
