<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_EstaOpcionales extends Bulk {
	
	/* cabecera del csv tiene los siguientes campos: 
	 * obac_id	2	9	10	11	12	13	14	15	16	17	18	19	20	21	25	26	54	55	56	57	58	59	60
	 * que es obac_id, y los numeros son cada obac_id
	*/
	public function process($file)
	{
		parent::process($file);
		
		$obac_id = $this->_fields;
		$data = $this->_data;
		$indicadores = array('name', 'codigo');

		unset($obac_id[0]);

		$opcionales = [];
		foreach ($obac_id as $key => $obac)
		{	
			foreach ($data as $valued)
			{
				foreach ($valued as $key3 => $value3)
				{
					if($obac == $key3)
						$opcionales[$obac][] = $value3;
				}
			}
		}
		
		foreach ($opcionales as $key => $row)
		{
			for($i = 0; $i < 9; $i++)
			{
				$oEstandar = ORM::factory('Estandar')
					->with('oCriterio:oFactor:oDimension:oObjetoAcreditacion')
					->where('oCriterio:oFactor:oDimension.obac_id', '=', $key)
					->where('esta_codigo', '=', $row[$i])
					->find();

				if($oEstandar->loaded())
				{	
					$oEstandar
						->set('esta_opcional', Model_Estandar::ESTA_OPCIONAL)
						->update();
				}
				/*else
				{
					die("objeto no cargado");
				}*/
				//debug($oEstandar);
			}
		}
		return TRUE;
	}
}
