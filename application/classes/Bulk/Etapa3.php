<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * 3.	Change nombres
 *		~FV1
 * 
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Etapa3 extends Bulk_Cambios {

	public function process($file)
	{
		$filename = APPPATH.'estandares/etapa3_log.csv';
		
		$this->_check_file($file);
		
		$this->_get_data($file);
		
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($this->_data);
		
		$log = LogTable::factory(['border' => 1]);
		
		if (file_exists($filename))
		{
			echo 'Changes already made';
			
			$log->read($filename);
			$log->stop();
		}
		
		foreach ($this->_data as $row)
		{
			extract($row);
			
			if ($STEP != 3) continue;
			
			if ($DONE) continue;
			
			$estandares = $this->get_estandares($obac_id);
			
			//debug2($row);
			$matches = [];
			preg_match('/~(.*)/', $A, $matches);
			
			//debug($matches);
			
			$code = $matches[1];
			
			$oConcepto = ORM::factory('Concepto')
				->where('conc_codigo', '=', $code)
				->where('esta_id', '=', $estandares[$esta_id])
				->find();
			
			$log->log([
				'obac_id' => $obac_id,
				'esta_codigo' => $esta_id,
				'esta_id' => $estandares[$esta_id],
				'conc_codigo' => $code,
				'conc_id' => $oConcepto->conc_id,
				'name' => $oConcepto->conc_nombre,
				'new_name' => $conc_nombre,
			]);
			
			$aAutoevaluacion = $this->get_autoevaluaciones($obac_id);
			
			//debug($aAutoevaluacion);
			
			foreach ($aAutoevaluacion as $oAutoevaluacion)
			{
				$oConceptoDetalle = ORM::factory('ConceptoDetalle')
					->with('oAutoevaluacionDetalle')
					->where('auto_id', '=', $oAutoevaluacion->auto_id)
					->where('esta_id', '=', $estandares[$esta_id])
					->where('conc_id', '=', $oConcepto->conc_id)
					->find();
				
				$log->partial([
					'auto_id' => $oAutoevaluacion->auto_id,
					'aude_id' => $oConceptoDetalle->aude_id,
					'code_id' => $oConceptoDetalle->code_id ?: 'not_loaded',
					'name' => $oConceptoDetalle->code_nombre,
					'new_name' => $conc_nombre,
				]);
				
				if ( ! $oConceptoDetalle->loaded())
				{
					$log->clean();
					continue;
				}
				
				if ($oConceptoDetalle->code_nombre != $oConcepto->conc_nombre)
				{
					$log->clean();
					continue;
				}
				
				$oConceptoDetalle->code_nombre = $conc_nombre;
				
				if ($this->save)
				{
					$oConceptoDetalle->update();
				}
				
				$log->log();
			}
			
			if ($this->save)
			{
				$oConcepto->conc_nombre = $conc_nombre;
				$oConcepto->update();
			}
		}
		
		echo $log->show();
		
		if ($this->save)
		{
			$log->write($filename);
			
			HTTP::redirect(Request::current()->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
	}
	
}
