<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Instituto extends Bulk {

	const D1 = 'DIMENSION';
	const D2 = 'DESCRIPCION DIMENSION';
	const F1 = 'FACTOR';
	const F2 = 'DESCRIPCION FACTOR';
	const C1 = 'CRITERIO';
	const C3 = 'NOMBRE CRITERIO';
	const E1 = 'ESTANDAR';
	const E2 = 'NOMBRE ESTANDAR';
	const E3 = 'DESCRIPCION DEL ESTANDAR';
	//const E4 = 'ETIPO';
	const EA = 'ACEPTACION';
	const EP = 'PONDERACION';
	const V1 = 'FUENTE DE VERIFICACION';
	const V2 = 'Nombres de las Fuentes';
	
	const A1 = 'NIVEL ACEPTACION';
	const A2 = 'NOMBRE ACEPTACION';
	const VC = 'CONDICION';
	
	const EM = 'MODALIDAD';
	
	protected $_obac_id;
	
	public function obac($obac_id)
	{
		$this->_obac_id = $obac_id;
		return $this;
	}
	
	public function process($file)
	{
		parent::process($file);
		
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($this->_data);
		
		Database::instance()->begin();
		
		try
		{
			$dimensiones = $this->_get_dimension();
			$factores = $this->_get_factor($dimensiones);
			$criterios = $this->_get_criterion($factores);
			$estandares = $this->_get_standard($criterios);
			$this->_get_concept($estandares);
			$this->_get_accomplishment($estandares);
		}
		catch (Exception $e)
		{
			Database::instance()->rollback();
			throw $e;
		}
		
		Database::instance()->commit();
		return TRUE;
	}
	
	protected function _get_dimension()
	{
		$data = $this->_data;
		$obac_id = $this->_obac_id;
		
		// Get dimensiones from data
		$rows = array();
		foreach ($data as $row)
		{
			$rows[$row[self::D1]] = array(
				self::D1 => $row[self::D1],
				self::D2 => $row[self::D2],
			);
		}
		//debug($rows);
		if (count($rows) != 4)
		{
			throw new Exception_Saes('Data inválida, se contaron :n dimensiones',
				array(':n' => count($rows)));
		}
		
		// Insert into database
		$query = DB::insert('dimension', array('obac_id', 'dime_fecha_reg', 'dime_estado', 'dime_codigo', 'dime_titulo'));
		foreach ($rows as $row)
		{
			$query->values(array($obac_id, date('Y-m-d H:i:s'), '1', $row[self::D1], $row[self::D2]));
		}
		//debug((string)$query);
		$result = $query->execute();
		//debug($result);
		
		// Get ids and return
		$keys = array_keys($rows); //Arr::pluck($rows, self::D1);
		$values = range($result[0], $result[0] + $result[1] - 1);
		$dimensiones = array_combine($keys, $values);
		//debug($dimensiones);
		return $dimensiones;
	}
	
	protected function _get_factor($dimensiones)
	{
		//debug($dimensiones, FALSE);
		$data = $this->_data;

		// Get factores from data
		$rows = array();
		foreach ($data as $row)
		{
			$rows[$row[self::F1]] = array(
				self::D1 => $row[self::D1],
				self::F1 => $row[self::F1],
				self::F2 => $row[self::F2],
			);
		}
		//debug($rows);
		
		// Insert into database
		$query = DB::insert('factor', array('dime_id', 'fact_fecha_reg', 'fact_estado', 'fact_codigo', 'fact_titulo'));
		foreach ($rows as $row)
		{
			$query->values(array($dimensiones[$row[self::D1]], date('Y-m-d H:i:s'), '1', $row[self::F1], $row[self::F2]));
		}
		//debug((string)$query);
		$result = $query->execute();
		//debug($result, false);
		
		// Get ids and return
		$keys = array_keys($rows); //Arr::pluck($rows, self::F1);
		$values = range($result[0], $result[0] + $result[1] - 1);
		$factores = array_combine($keys, $values);
		//debug($factores);
		return $factores;
	}
	
	protected function _get_criterion($factores)
	{
		//debug($factores, FALSE);
		$data = $this->_data;

		// Get factores from data
		$rows = array();
		foreach ($data as $row)
		{
			$rows[$row[self::C1]] = array(
				self::F1 => $row[self::F1],
				self::C1 => $row[self::C1],
				self::C3 => $row[self::C3],
			);
		}
		//debug($rows);
		
		// Insert into database
		$query = DB::insert('criterio', array(
			'fact_id', 'crit_fecha_reg', 'crit_estado', 'crit_codigo', 'crit_descripcion'));
		foreach ($rows as $row)
		{
			$query->values(array(
				$factores[$row[self::F1]], date('Y-m-d H:i:s'), '1', $row[self::C1], $row[self::C3]));
		}
		//debug((string)$query);
		$result = $query->execute();
		//debug($result);
		
		// Get ids and return
		$keys = array_keys($rows); //Arr::pluck($rows, self::C1);
		$values = range($result[0], $result[0] + $result[1] - 1);
		$criterios = array_combine($keys, $values);
		//debug($criterios);
		return $criterios;
	}
	
	protected function _get_standard($criterios)
	{
		//debug($criterios, FALSE);
		$data = $this->_data;

		// Get factores from data
		$rows = array();
		foreach ($data as $row)
		{
			$rows[$row[self::E1]] = array(
				self::C1 => $row[self::C1],
				self::E1 => $row[self::E1],
				self::E2 => $row[self::E2],
				self::E3 => $row[self::E3],
				self::EP => $row[self::EP],
				self::EA => $row[self::EA],
				self::EM => $row[self::EM],
			);
		}
		//debug($rows);
		
		// Insert into database
		$query = DB::insert('estandar', array(
			'crit_id', 'esta_fecha_reg', 'esta_estado', 'esta_codigo',
			'esta_nivel_aceptacion', 'esta_titulo', 'esta_descripcion', 'esta_ponderacion', 'moda_id'));
		foreach ($rows as $row)
		{
			$query->values(array(
				$criterios[$row[self::C1]], date('Y-m-d H:i:s'), '1', $row[self::E1],
				$row[self::EA], $row[self::E2], $row[self::E3], $row[self::EP], $row[self::EM]));
		}
		//debug((string)$query);
		$result = $query->execute();
		//debug($result);
		
		// Get ids and return
		$keys = array_keys($rows); //Arr::pluck($rows, self::E1);
		$values = range($result[0], $result[0] + $result[1] - 1);
		$estandares = array_combine($keys, $values);
		//debug($estandares);
		return $estandares;
	}
	
	protected function _get_concept($estandares)
	{
		//debug($estandares, FALSE);
		$data = $this->_data;

		// Get factores from data
		$rows = array();
		foreach ($data as $row)
		{
			$rows[$row[self::E1].'|'.$row[self::V1]] = array(
				self::E1 => $row[self::E1],
				self::V1 => $row[self::V1],
				self::V2 => $row[self::V2],
				self::VC => $row[self::VC],
			);
		}
		//debug($rows);
		
		// Insert into database
		$query = DB::insert('concepto', array(
			'esta_id', 'conc_fecha_reg', 'conc_estado', 'conc_codigo',
			'conc_nombre', 'conc_obligatorio'));
		foreach ($rows as $row)
		{
			$query->values(array(
				$estandares[$row[self::E1]], date('Y-m-d H:i:s'), '1', $row[self::V1],
				$row[self::V2], 0));
		}
		//debug((string)$query);
		$result = $query->execute();
		//debug($result);
		
		// Get ids and return
		//$keys = Arr::pluck($rows, self::V1);
		//$values = range($result[0], $result[0] + $result[1] - 1);
		//$conceptos = array_combine($keys, $values);
		//debug($conceptos);
		//return $conceptos;
	}
	
	protected function _get_accomplishment($estandares)
	{
		//debug($estandares);
		$data = $this->_data;
		
		// Get cumplimientos from data
		$rows = array();
		foreach ($data as $row)
		{
			$index = $row[self::E1].'|'.$row[self::A1];
			
			if (isset($rows[$index]))
			{
				$rows[$index][self::VC] .= $row[self::VC];
			}
			else {
				$rows[$index] = array(
					self::E1 => $row[self::E1],
					self::A1 => $row[self::A1],
					self::A2 => $row[self::A2],
					self::VC => $row[self::VC],
				);
			}
		}
		//debug($rows);
		
		// Insert into database
		$query = DB::insert('cumplimiento', array(
			'esta_id', 'cump_fecha_reg', 'cump_estado', 'cump_numero', 'cump_descripcion', 'cump_condicion'));
		foreach ($rows as $row)
		{
			$query->values(array(
				$estandares[$row[self::E1]], date('Y-m-d H:i:s'), '1', $row[self::A1], $row[self::A2], $row[self::VC]));
		}
		//debug((string)$query);
		$result = $query->execute();
	}
	
}
