<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Institution extends Bulk {

	protected $_type;
	
	public function type($type)
	{
		$this->_type = $type;
		return $this;
	}
	
	public function process($file)
	{
		parent::process($file);
		
		Database::instance()->begin();
		
		try
		{
			$inst = $this->_get_inst();
			$this->_get_carr($inst);
		}
		catch (Exception $e)
		{
			Database::instance()->rollback();
			throw $e;
		}
		
		Database::instance()->commit();
		return TRUE;
	}
	
	protected function _get_inst()
	{
		//echo '<html><head><meta charset="utf-8"></head><body>';
		$data = $this->_data;
		//debug($data[0], false);
		
		$columns = array_keys($data[0]);
		$columns = array_filter($columns, function ($e) {
			if(substr($e, 0, 4) == 'inst') return TRUE;
		});
		$columns = array_merge($columns, array('inst_fecha_reg', 'inst_estado', 'tiin_id'));
		//debug($columns, false);
		
		// Get universities from data
		$rows = array();
		foreach ($data as $row)
		{
			//debug($row);
			$rows[$row['inst_nombre']] = $row;
		}
		//debug($rows);
		
		// Insert into database
		$query = DB::insert('institucion', $columns);
		foreach ($rows as $row)
		{
			$values = array();
			foreach ($row as $field => $value)
			{
				if (in_array($field, $columns))
				{
					$values[] = $value;
				}
			}
			$values = array_merge($values, array(date('Y-m-d H:i:s'), 1, $this->_type));
			//debug($values, false);
			
			$query->values($values);
		}
		//die();
		//debug((string)$query);
		$result = $query->execute();
		//debug($result);
		
		// Get ids and return
		$keys = array_keys($rows); //Arr::pluck($rows, self::D1);
		$values = range($result[0], $result[0] + $result[1] - 1);
		$inst = array_combine($keys, $values);
		//debug($inst);
		return $inst;
	}
	
	protected function _get_carr($inst)
	{
		//debug($inst, false);
		//echo '<html><head><meta charset="utf-8"></head><body>';
		$data = $this->_data;
		//debug($data[0], false);
		
		$columns = array_keys($data[0]);
		$columns = array_filter($columns, function ($e) {
			if(substr($e, 0, 4) == 'carr') return TRUE;
		});
		$columns = array_merge($columns, array('carr_fecha_reg', 'carr_estado', 'inst_id'));
		//debug($columns, false);
		
		// Get universities from data
		
		// Insert into database
		$query = DB::insert('carrera', $columns);
		foreach ($data as $row)
		{
			$values = array();
			foreach ($row as $field => $value)
			{
				if (in_array($field, $columns))
				{
					$values[] = $value;
				}
			}
			$values = array_merge($values, array(date('Y-m-d H:i:s'), 1, $inst[$row['inst_nombre']]));
			//debug($values);
			
			$query->values($values);
		}
		//die();
		//debug((string)$query);
		$result = $query->execute();
		//debug($result);
		
		return $result;
	}
	
}
