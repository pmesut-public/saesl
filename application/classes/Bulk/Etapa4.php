<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * 4.	Deletion
 *		-FV1
 * 
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Etapa4 extends Bulk_Cambios {

	public function process($file)
	{
		$filename = APPPATH.'estandares/etapa4_log.csv';
		
		$this->_check_file($file);
		
		$this->_get_data($file);
		
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($this->_data);
		
		$log = LogTable::factory(['border' => 1]);
		
		if (file_exists($filename))
		{
			echo 'Changes already made';
			
			$log->read($filename);
			$log->stop();
		}
		
		foreach ($this->_data as $row)
		{
			extract($row);
			
			if ($STEP != 4) continue;
			
			if ($DONE) continue;
			
			$estandares = $this->get_estandares($obac_id);
			
			//debug2($row);
			$matches = [];
			preg_match('/-(.*)/', $A, $matches);
			
			//debug($matches);
			
			$code = $matches[1];
			
			$oConcepto = ORM::factory('Concepto')
				->where('conc_codigo', '=', $code)
				->where('esta_id', '=', $estandares[$esta_id])
				->find();
			
			$log->log([
				'obac_id' => $obac_id,
				'esta_codigo' => $esta_id,
				'esta_id' => $estandares[$esta_id],
				'conc_codigo' => $code,
				'conc_id' => $oConcepto->conc_id,
				'concepto' => json_encode($oConcepto->as_array()),
			]);
			
			$aAutoevaluacion = $this->get_autoevaluaciones($obac_id);
			
			//debug($aAutoevaluacion);
			
			foreach ($aAutoevaluacion as $oAutoevaluacion)
			{
				$oConceptoDetalle = ORM::factory('ConceptoDetalle')
					->with('oAutoevaluacionDetalle')
					->where('auto_id', '=', $oAutoevaluacion->auto_id)
					->where('esta_id', '=', $estandares[$esta_id])
					->where('conc_id', '=', $oConcepto->conc_id)
					->find();
				
				$log->partial([
					'auto_id' => $oAutoevaluacion->auto_id,
					'aude_id' => $oConceptoDetalle->aude_id,
					'code_id' => $oConceptoDetalle->code_id ?: 'not_loaded',
					'concepto' => json_encode($oConceptoDetalle->as_array()),
				]);
				
				if ( ! $oConceptoDetalle->loaded())
				{
					$log->clean();
					continue;
				}
				
				$log->log();
				
				if ($this->save)
				{
					$oConceptoDetalle->delete();
				}
			}
			
			if ($this->save)
			{
				DB::delete('concepto_detalle')
					->where('conc_id', '=', $oConcepto->conc_id)
					->execute();
				
				$oConcepto->delete();
			}
		}
		
		echo $log->show();
		
		if ($this->save)
		{
			$log->write($filename);
			
			HTTP::redirect(Request::current()->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
	}
	
}
