<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * 1.	Cambiar FV’s entre estándares 
 *		FV1: 11 >> 12
 * 
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Etapa1 extends Bulk_Cambios {

	public function process($file)
	{
		$filename = APPPATH.'estandares/etapa1_log.csv';
		
		$this->_check_file($file);
		
		$this->_get_data($file);
		
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($this->_data);
		
		$log = LogTable::factory(['border' => 1]);
		
		if (file_exists($filename))
		{
			echo 'Changes already made';
			
			$log->read($filename);
			$log->stop();
		}
		
		foreach ($this->_data as $row)
		{
			extract($row);
			
			if ($STEP != 1) continue;
			
			if ($DONE) continue;
			
			$estandares = $this->get_estandares($obac_id);
			
			$matches = [];
			preg_match('/([^\s>]+)\s*>>\s*([^\s]+)/', $A, $matches);
			
			$old = $matches[1];
			$new = $matches[2];
			
			$oConcepto = ORM::factory('Concepto')
				->where('conc_codigo', '=', $conc_codigo)
				->where('esta_id', '=', $estandares[$old])
				->find();
			
			$oConcepto->esta_id = $estandares[$new];
			
			if ($this->save)
			{
				$oConcepto->update();
			}
			
			$log->log([
				'obac_id' => $obac_id,
				'conc_id' => $oConcepto->conc_id,
				'conc_codigo' => $conc_codigo,
				'old_esta_id' => $estandares[$old],
				'old_esta_codigo' => $old,
				'new_esta_id' => $estandares[$new],
				'new_esta_codigo' => $new,
			]);
			
			$aAutoevaluacion = $this->get_autoevaluaciones($obac_id);
			//debug($aAutoevaluacion);
			
			foreach ($aAutoevaluacion as $oAutoevaluacion)
			{
				$log->partial([
					'obac_id' => $obac_id,
					'auto_id' => $oAutoevaluacion->auto_id,
				]);
				
				$old_aude = $oAutoevaluacion
					->aDetalle
					->where('esta_id', '=', $estandares[$old])
					->find();
				
				$new_aude = $oAutoevaluacion
					->aDetalle
					->where('esta_id', '=', $estandares[$new])
					->find();
				
				$oConceptoDetalle = $old_aude
					->aConcepto
					->with('oConcepto')
					->where('conc_codigo', '=', $conc_codigo)
					->find();
				
				$log->partial([
					'old_aude_id' => $old_aude->aude_id ?: 'not_loaded',
					'new_aude_id' => $new_aude->aude_id ?: 'not_loaded',
					'code_id' => $oConceptoDetalle->code_id ?: 'not_loaded',
				]);
				
				if ( ! $oConceptoDetalle->loaded())
				{
					$log->clean();
					continue;
				}
				
				if ($new_aude->loaded())
				{
					$oConceptoDetalle->aude_id = $new_aude->aude_id;
					
					if ($this->save)
					{
						$oConceptoDetalle->update();
					}
				}
				else
				{
					$log->partial(['code' => json_encode($oConceptoDetalle->as_array())]);
					
					if ($this->save)
					{
						$oConceptoDetalle->delete();
					}
				}
				
				$log->log();
				continue;
				
			}
			
		}
		
		echo $log->show();
		
		if ($this->save)
		{
			$log->write($filename);
			
			HTTP::redirect(Request::current()->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
	}
	
}
