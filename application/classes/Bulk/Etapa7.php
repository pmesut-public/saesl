<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * 7.	Condiciones FV’s + Cumplimientos
 * 
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Etapa7 extends Bulk_Cambios {

	protected $_obac_id;
	
	public function obac($obac_id)
	{
		$this->_obac_id = $obac_id;
		return $this;
	}
	
	public function process($file)
	{
		$filename = APPPATH."estandares/etapa7_{$this->_obac_id}.csv";
		
		$this->_check_file($file);
		
		$this->_get_data($file);
		
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($this->_data);
		
		$log = LogTable::factory(['border' => 1]);
		
		if (file_exists($filename))
		{
			echo 'Changes already made';
			
			$log->read($filename);
			
			echo $log->show();
			return;
		}
		
		if ( ! in_array($this->_obac_id, [22,23,24]))
		{
			$this->process_univ($filename);
		}
		else
		{
			$this->process_inst($filename);
		}
	}
	
	private function process_univ($filename)
	{
		$log = LogTable::factory(['border' => 1]);
		
		$data = $this->_data;
		
		foreach ($this->get_conceptos() as $i => $oConcepto)
		{
			$row = $data[$i];
			
			$concepto = $oConcepto->as_array();
			unset($concepto['oEstandar']);
			//debug2($concepto, $row);
			
			if ( ! $this->compare($concepto, $row))
			{
				debug2($concepto, $row);
			}
			//die();
			
			$log->partial([
				'obac' => $this->_obac_id,
				'esta_id' => $concepto['esta_id'],
				'esta_codigo' => $concepto['esta_codigo'],
				'conc_id' => $concepto['conc_id'],
				'conc_codigo' => $concepto['conc_codigo'],
				'condicion' => $concepto['conc_obligatorio'],
			]);
			
			if ($concepto['conc_obligatorio'] == $row['VC'])
			{
				$log->clean();
				continue;
			}
			
			$oConcepto->conc_obligatorio = $row['VC'];
			
			if ($this->save)
			{
				$oConcepto->update();
			}
			
			$log->log(['new_condicion' => $oConcepto->conc_obligatorio]);
		}
		
		echo $log->show();
		
		if ($this->save)
		{
			$log->write($filename);
			
			//HTTP::redirect(Request::current()->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
	}
	
	private function process_inst($filename)
	{
		$log = LogTable::factory(['border' => 1]);
		
		//debug($this->_data);
		
		$data = TreeArray::factory($this->_data, ['E1', 'A1', 'V1'])->get();
		//debug($data);
		
		//debug($this->get_conceptos()->as_array());
		
		$aEstandar = $this->get_estandares();
		//debug($aEstandar[0]);
		
		foreach ($data as $esta => $cumplimientos)
		{
			$oEstandar = $aEstandar->current();
			
			$aCumplimiento = $oEstandar
				->aCumplimiento
				->order_by('cump_numero')
				->find_all()
				->as_array('cump_numero');
			
			if ($oEstandar->esta_codigo != $esta)
			{
				die('1. <> esta_codigos');
			}
			
			if (count($cumplimientos) != 5)
			{
				die('2. Cumplimientos != 5');
			}
			
			$log->partial([
				'obac_id' => $this->_obac_id,
				'esta_id' => $oEstandar->esta_id,
				'esta_codigo' => $esta,
			]);
			
			foreach ($cumplimientos as $cump => $fuentes)
			{
				$oCumplimiento = $aCumplimiento[$cump];
				
				$old_count = $oEstandar->count;
				$new_count = count($fuentes);
				
				if ($old_count != $new_count)
				{
					$log->log(compact('old_count', 'new_count'));
					$log->stop();
					die('3. <> counts');
				}
				
				$old_fuentes = $oEstandar->conceptos;
				$new_fuentes = implode(',', array_keys($fuentes));
				
				if ($old_fuentes !== $new_fuentes)
				{
					$log->log(compact('old_fuentes', 'new_fuentes'));
					$log->stop();
					die('4. <> fuentes');
				}
				
				$new_condicion = '';
				
				foreach ($fuentes as $conc => $fuente)
				{
					$new_condicion .= $fuente['VC'];
					
					if ($cump == 1 AND $conc == 'FV1')
					{
						$old_aceptacion = $oEstandar->esta_nivel_aceptacion;
						$new_aceptacion = $fuente['EA'];
						
						$log->log(compact('old_aceptacion', 'new_aceptacion', 'old_count', 'new_count', 'old_fuentes', 'new_fuentes'));
					}
				}
				
				$old_condicion = $oCumplimiento->cump_condicion;
				
				if ($new_count != strlen($new_condicion))
				{
					die('5. count <> len(condicion)');
				}
				
				if ($old_condicion === $new_condicion)
				{
					continue;
				}
				
				if ($this->save)
				{
					$oCumplimiento->cump_condicion = $new_condicion;
					$oCumplimiento->save();
				}
				
				$log->log(['cump_id' => $oCumplimiento->cump_id] + compact('cump', 'old_condicion', 'new_condicion'));
			}
			
			if ($old_aceptacion != $new_aceptacion)
			{
				die('6. <> aceptacion - not yet implemented');
				//$oEstandar->esta_nivel_aceptacion = $new_aceptacion;
			}
			
			$aEstandar->next();
		}
		
		echo $log->show();
		
		if ($this->save)
		{
			$log->write($filename);
			
			//HTTP::redirect(Request::current()->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
	}
	
	private function get_conceptos()
	{
		return ORM::factory('Concepto')
			->with('oEstandar:oCriterio:oFactor:oDimension')
			->select('dime_codigo', 'fact_codigo', 'crit_codigo', 'esta_codigo', 'conc_codigo')
			->where('obac_id', '=', $this->_obac_id)
			//->order_by(DB::expr('cast(esta_codigo as signed)'))
			->order_by('esta_id')
			->order_by(DB::expr('cast(substr(conc_codigo, 3) as signed)'))
			->find_all();
	}
	
	protected function get_estandares($obac_id = NULL)
	{
		return ORM::factory('Estandar')
			->with('oCriterio:oFactor:oDimension')
			->join('concepto')->using('esta_id')
			->select('dime_codigo', 'fact_codigo', 'crit_codigo', 'esta_codigo')
			->select(DB::expr('count(conc_id) as count'))
			->select(DB::expr('group_concat(conc_codigo order by cast(substr(conc_codigo, 3) as signed)) as conceptos'))
			->where('obac_id', '=', $this->_obac_id)
			->group_by('esta_id')
			->order_by('esta_id')
			//->order_by(DB::expr('cast(substr(conc_codigo, 3) as signed)'))
			->find_all();
	}
	
	private function compare($concepto, $row)
	{
		//debug2($oConcepto->as_array(), $row); die();
		
		$matches = [
			'esta_codigo' => 'E1',
			'conc_codigo' => 'V1',
			//'conc_nombre' => 'V2',
		];
		
		foreach ($matches as $x => $y)
		{
			if ($concepto[$x] != $row[$y])
				return FALSE;
		}
		
		return TRUE;
	}
	
}
