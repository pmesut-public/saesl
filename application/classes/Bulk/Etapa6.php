<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * 6.	Creation
 *		+FV1
 * 
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Etapa6 extends Bulk_Cambios {

	public function process($file)
	{
		$filename = APPPATH.'estandares/etapa6_log.csv';
		
		$this->_check_file($file);
		
		$this->_get_data($file);
		
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($this->_data);
		
		$log = LogTable::factory(['border' => 1]);
		
		if (file_exists($filename))
		{
			echo 'Changes already made';
			
			$log->read($filename);
			$log->stop();
		}
		
		foreach ($this->_data as $row)
		{
			extract($row);
			
			if ($STEP != 6) continue;
			
			if ($DONE) continue;
			
			$estandares = $this->get_estandares($obac_id);
			
			//debug2($row);
			$matches = [];
			preg_match('/\+(.*)/', $A, $matches);
			
			//debug($matches);
			
			$code = $matches[1];
			
			$oConcepto = ORM::factory('Concepto')
				->where('conc_codigo', '=', $code)
				->where('esta_id', '=', $estandares[$esta_id])
				->find();
			
			if ($oConcepto->loaded()) die('wtf');
			
			$oConcepto->values([
				'esta_id' => $estandares[$esta_id],
				'conc_estado' => 'activo',
				'conc_codigo' => $code,
				'conc_nombre' => $conc_nombre,
				'conc_obligatorio' => $conc_condicion ?: 0,
			]);
			
			$log->partial(compact('obac_id') + 
				['esta_codigo' => $esta_id] + 
				$oConcepto->as_array()
			);
			
			if ($this->save)
			{
				$oConcepto->create();
			}
			
			$log->log(['conc_id' => $oConcepto->conc_id]);
		}
		
		echo $log->show();
		
		if ($this->save)
		{
			$log->write($filename);
			
			HTTP::redirect(Request::current()->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
	}
	
}
