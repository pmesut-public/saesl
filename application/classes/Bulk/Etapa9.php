<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * 9.	Cambios manuales
 *		etapa9.csv
 * 
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Etapa9 extends Bulk_Cambios {

	public function process($file)
	{
		$filename = APPPATH.'estandares/etapa9_log.csv';
		
		$this->_check_file($file);
		
		$this->_get_data($file);
		
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($this->_data);
		
		$log = LogTable::factory(['border' => 1]);
		
		if (file_exists($filename))
		{
			echo 'Changes already made';
			
			$log->read($filename);
			$log->stop();
		}
		
		foreach ($this->_data as $row)
		{
			//extract($row);
			//debug2($row);
			
			$log->partial($row);
			
			if ($id = $row['esta'])
			{
				$object = ORM::factory('Estandar')
					->with('oCriterio:oFactor:oDimension')
					->where('obac_id', '=', $row['obac_id'])
					->where('esta_codigo', '=', $id)
					->find();
			}
			
			if ($id = $row['crit'])
			{
				$object = ORM::factory('Criterio')
					->with('oFactor:oDimension')
					->where('obac_id', '=', $row['obac_id'])
					->where('crit_codigo', '=', $id)
					->find();
			}
			
			if ($id = $row['fact'])
			{
				$object = ORM::factory('Factor')
					->with('oDimension')
					->where('obac_id', '=', $row['obac_id'])
					->where('fact_codigo', '=', $id)
					->find();
			}
			
			if ( ! $object->loaded()) die('model not loaded');
			
			$log->partial(['id' => $object->pk()]);
			
			if ($row['A'] == 'C')
			{
				$matches = [];
				preg_match('/([^\s>]+)\s*>>\s*([^\s]+)/', $row['D'], $matches);

				//debug2($matches);

				$first = $matches[1];
				$second = $matches[2];
				
				if ($row['esta'])
				{
					$log->partial(['old_parent_id' => $object->crit_id]);
					
					if ($object->oCriterio->crit_codigo != $first)
					{
						$log->log(['done' => '*']);
						continue;
						die('wrong first code');
					}
					
					$secondObject = ORM::factory('Criterio')
						->with('oFactor:oDimension')
						->where('obac_id', '=', $row['obac_id'])
						->where('crit_codigo', '=', $second)
						->find();
					
					if ( ! $secondObject->loaded())
					{
						die('second model not loaded');
					}
					
					$object->crit_id = $secondObject->pk();
				}
				
				if ($row['crit'])
				{
					$log->partial(['old_parent_id' => $object->fact_id]);
					
					if ($object->oFactor->fact_codigo != $first)
					{
						$log->log(['done' => '*']);
						//continue;
						//die('wrong first code');
					}
					
					$secondObject = ORM::factory('Factor')
						->with('oDimension')
						->where('obac_id', '=', $row['obac_id'])
						->where('fact_codigo', '=', $second)
						->find();
					
					if ( ! $secondObject->loaded())
					{
						die('second model not loaded');
					}
					
					$object->fact_id = $secondObject->pk();
				}
				
				$log->partial(['new_parent_id' => $secondObject->pk()]);
			}
			else
			{
				if ($row['fact'])
					$column = 'fact_'.$row['A'];
				
				if ($row['crit'])
					$column = 'crit_'.$row['A'];
				
				if ($row['esta'])
					$column = 'esta_'.$row['A'];
				
				$log->partial(['old_column' => $object->$column]);
				
				$object->$column = $row['D'];
			}
			
			if ( ! $object->changed())
			{
				//$log->log(['done' => '*']);
				//continue;
				die('wtf');
			}
			
			if ($this->save)
			{
				$object->update();
			}
			
			$log->log();
		}
		
		echo $log->show();
		
		if ($this->save)
		{
			$log->write($filename);
			
			HTTP::redirect(Request::current()->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
	}
	
}
