<?php defined('SYSPATH') or die('No direct script access.');
/**
 * 
 * 2.	Merge FV’s
 *		FV1 + FV2 >> FV1
 * 
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Bulk_Etapa2 extends Bulk_Cambios {

	public function process($file)
	{
		$filename = APPPATH.'estandares/etapa2_log.csv';
		
		$this->_check_file($file);
		
		$this->_get_data($file);
		
		//echo '<html><head><meta charset="utf-8"></head><body>';
		//debug($this->_data);
		
		$log = LogTable::factory(['border' => 1]);
		
		if (file_exists($filename))
		{
			echo 'Changes already made';
			
			$log->read($filename);
			$log->stop();
		}
		
		foreach ($this->_data as $row)
		{
			extract($row);
			
			if ($STEP != 2) continue;
			
			if ($DONE) continue;
			
			$estandares = $this->get_estandares($obac_id);
			
			//debug2($row);
			$matches = [];
			preg_match('/([^\s\+]+)\s*\+\s*([^\s>]+)\s*>>\s*([^\s]+)/', $A, $matches);
			
			//debug($matches);
			
			$first = $matches[1];
			$second = $matches[2];
			
			$last = $matches[3];
			
			$firstConcepto = ORM::factory('Concepto')
				->where('conc_codigo', '=', $first)
				->where('esta_id', '=', $estandares[$esta_id])
				->find();
			
			$secondConcepto = ORM::factory('Concepto')
				->where('conc_codigo', '=', $second)
				->where('esta_id', '=', $estandares[$esta_id])
				->find();
			
			$log->log([
				'obac_id' => $obac_id,
				'esta_id' => $estandares[$esta_id],
				'esta_codigo' => $esta_id,
				'f_code' => $first,
				'f_concepto' => $firstConcepto->conc_id,
				's_code' => $second,
				's_concepto' => $secondConcepto->conc_id,
				'auto' => '',
				'aude' => '',
				'f_cumple' => '',
				's_cumple' => '',
				'f_new_cumple' => '',
				'f_calidad' => '',
				's_calidad' => '',
				'f_new_calidad' => '',
				'f_nombre' => $firstConcepto->conc_nombre,
				'f_new_nombre' => $conc_nombre,
				'second' => json_encode($secondConcepto->as_array()),
			]);
			
			$firstConcepto->conc_nombre = $conc_nombre;
			
			$aAutoevaluacion = $this->get_autoevaluaciones($obac_id);
			
			//debug($aAutoevaluacion);
			
			foreach ($aAutoevaluacion as $oAutoevaluacion)
			{
				$oAutoevaluacionDetalle = $oAutoevaluacion
					->aDetalle
					->where('esta_id', '=', $estandares[$esta_id])
					->find();
				
				if ( ! $oAutoevaluacionDetalle->loaded()) continue;
				
				$firstConceptoDetalle = $oAutoevaluacionDetalle
					->aConcepto
					->where('conc_id', '=', $firstConcepto->conc_id)
					->find();
				
				$secondConceptoDetalle = $oAutoevaluacionDetalle
					->aConcepto
					->where('conc_id', '=', $secondConcepto->conc_id)
					->find();
				
				$log->partial([
					'obac_id' => $obac_id,
					'auto' => $oAutoevaluacion->auto_id,
					'aude' => $oAutoevaluacionDetalle->aude_id,
					'f_concepto' => $firstConceptoDetalle->code_id ?: 'not_loaded',
					's_concepto' => $secondConceptoDetalle->code_id ?: 'not_loaded',
					'f_cumple' => $firstConceptoDetalle->code_cumple,
					's_cumple' => $secondConceptoDetalle->code_cumple,
					'f_calidad' => $firstConceptoDetalle->code_calidad,
					's_calidad' => $secondConceptoDetalle->code_calidad,
					'f_nombre' => $firstConceptoDetalle->code_nombre,
				]);
				
				if ( ! $firstConceptoDetalle->loaded()) 
				{
					$log->clean();
					continue;
				}
				
				$firstConceptoDetalle->code_nombre .= '; '.$secondConceptoDetalle->code_nombre;
				$firstConceptoDetalle->code_cumple = ($firstConceptoDetalle->code_cumple AND $secondConceptoDetalle->code_cumple) ? 1 : 0;
				
				if ($firstConceptoDetalle->code_cumple)
				{
					$firstConceptoDetalle->code_calidad = max([$firstConceptoDetalle->code_calidad, $secondConceptoDetalle->code_calidad]);
				}
				else
				{
					$firstConceptoDetalle->code_calidad = NULL;
				}
				
				$log->log([
					'f_new_cumple' => $firstConceptoDetalle->code_cumple,
					'f_new_calidad' => $firstConceptoDetalle->code_calidad,
					'f_new_nombre' => $firstConceptoDetalle->code_nombre,
					'second' => json_encode($secondConceptoDetalle->as_array()),
				]);
				
				if ($this->save)
				{
					$firstConceptoDetalle->update();
					$secondConceptoDetalle->delete();
				}
			}
			
			if ($this->save)
			{
				$firstConcepto->update();
				
				DB::delete('concepto_detalle')
					->where('conc_id', '=', $secondConcepto->conc_id)
					->execute();
				
				$secondConcepto->delete();
			}
			
		}
		
		echo $log->show();
		
		if ($this->save)
		{
			$log->write($filename);
			
			HTTP::redirect(Request::current()->referrer());
		}
		
		echo Form::open().Form::submit(NULL, 'change').Form::close();
	}
	
}
