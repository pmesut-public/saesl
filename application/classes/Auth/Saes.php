<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Auth_Saes extends Auth_ORM {

	/**
	 * Checks if a session is active.
	 *
	 * @param   mixed    $role Role name string, role ORM object, or array with role names
	 * @return  boolean
	 */
	public function logged_in($role = NULL)
	{
		if ($role)
		{
			throw new Exception_Saes('Auth operation not supported anymore');
		}
		
		// Get the user from the session
		$user = $this->get_user();
		
		if ( ! $user)
			return FALSE;

		if ($user instanceof Model_User AND $user->loaded())
		{
			return TRUE;
		}
	}

	public function is_super()
	{
		$user = $this->get_user();

		return (bool) ($user->id == Model_User::SUPER_ADMIN_ID);
	}
	
	/**
	 * Logs a user in.
	 * Busca el usuario en la BD y obtiene la data.
	 * Hashea el password y matchea : status = 1 y password con los de la BD.
	 * Verifica que role_id = 3 y acre_aprobado = 1
	 *
	 * @param   string   $username
	 * @param   string   $password
	 * @param   boolean  $remember  enable autologin
	 * @return  boolean
	 */
	protected function _login($user, $password, $remember)
	{
		if ( ! is_object($user))
		{
			$username = $user;

			// Load the user
			$user = ORM::factory('User');

			$user->where($user->unique_key($username), '=', $username)->find();
		}

		if (is_string($password))
		{
			// Create a hashed password
			$password = $this->hash($password);
		}

		// If the passwords match, perform a login
		if ($user->status == Model_User::STATUS_ACTIVO AND $user->password === $password)
		{	
			if ($user->role_id == Model_Role::ROLE_REGULAR AND $user->oAcreditado->acre_aprobado != Model_Acreditado::ESTADO_ACTIVO)
			{
				return FALSE;
			}
			
			$this->remember($user, $remember);

			// Finish the login
			$this->complete_login($user);

			return TRUE;
		}

		// Login failed
		return FALSE;
	}
	
	/*
	 * 
	 * 
	 */
	protected function remember($user, $remember)
	{//debug($remember);
		if ($remember === TRUE)
		{
			// Token data
			$data = array(
				'user_id'    => $user->pk(),
				'expires'    => time() + $this->_config['lifetime'],
				'user_agent' => sha1(Request::$user_agent),
			);

			// Create a new autologin token
			$token = ORM::factory('User_Token')
						->values($data)
						->create();

			// Set the autologin cookie
			Cookie::set('authautologin', $token->token, $this->_config['lifetime']);
		}
	}

} // End Auth ORM
