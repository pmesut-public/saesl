select a.*, ae.*, ai.*, i.*, ac.*, c.*, o.*, ti.*, m.*
, concat(tiin_nombre, ' - ', obac_nombre) objeto
, moda_nombre
, concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede) inst_name
, carr_nombre
, inst_gestion
, acev_id

from acreditado a
join acreditado_evento ae using(acre_id)
join users u on a.acre_id = u.id
join acreditado_institucion ai using(acre_id)
join institucion i using(inst_id)
left join acreditado_carrera ac using(acin_id)
left join carrera c using(carr_id)
join objeto_acreditacion o using(obac_id)
join tipo_institucion ti on o.tiin_id = ti.tiin_id
join modalidad m using(moda_id)

where u.status = 1
#and a.acre_test = 0
and even_id = _even_id
and ae.acev_estado <> 5
