select tiin_nombre, inst_nombre
, sum(if(carr_id, 1, 0)) sum_carr
, if(sum(if(carr_id, 0, 1)) > 0, 'Sí', 'No') sum_inst
, concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede) inst_nombre

from acreditado a
join acreditado_institucion ai using(acre_id)
join institucion i using(inst_id)
left join acreditado_objeto ac using(acin_id)
left join carrera c using(carr_id)
join tipo_institucion using(tiin_id)
join users u on a.acre_id = u.id
where a.acre_test = 0
and a.acre_aprobado = 1
and u.status = 1

group by i.inst_id
order by tiin_id
