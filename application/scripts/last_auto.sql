select concat(tiin_nombre, ' - ', obac_nombre) objeto
, moda_nombre
, concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede) inst_name
, carr_nombre
, inst_gestion
, auto_fecha_fin
, format(sum(if((aude_cumplimiento = 1), 1, 0)) / count(esta_id) * 100, 2) + 0.0 per

from acreditado a
join acreditado_evento ae using(acre_id)
join autoevaluacion b using(acev_id)
join autoevaluacion_detalle ad using(auto_id)

join users u on a.acre_id = u.id
join acreditado_institucion ai using(acre_id)
join institucion i using(inst_id)
left join acreditado_carrera ac using(acin_id)
left join carrera c using(carr_id)
join objeto_acreditacion o using(obac_id)
join tipo_institucion ti on o.tiin_id = ti.tiin_id
join modalidad m using(moda_id)

#left 
join (
	select acre_id, max(auto_numero) max_auto_numero
	from acreditado 
	join acreditado_evento using(acre_id) 
	join autoevaluacion b using(acev_id)
	where b.auto_estado = 'cerrado'
	and b.auto_completa = 1
	#and b.auto_fecha_fin <= '2015-07-31'
	group by acre_id
) max on (a.acre_id = max.acre_id and b.auto_numero = max.max_auto_numero)

where u.status = 1
and a.acre_test = 0
and b.auto_estado = 'cerrado'
and b.auto_completa = 1
and o.tiin_id = _tiin_id
#and b.auto_fecha_fin <= '2015-07-31'
#and o.obac_id = 22

group by auto_id
#having sum(if((aude_cumplimiento = 1), 1, 0)) > 0
order by o.tiin_id, per desc
