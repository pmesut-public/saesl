select #a.acre_id, b.*, d.*
concat(auto_numero, '. ', auto_fecha_inicio) as name
, cast(concat_ws(',', sum(if(aude_valoracion <= 3, 1, 0)), sum(if(aude_valoracion <= 6, 1, 0)), sum(if(aude_valoracion <= 10, 1, 0))) as char) as data
from acreditado a
join acreditado_evento ae using(acre_id)
join autoevaluacion b using(acev_id)
join autoevaluacion_detalle ad using(auto_id)
join estandar e using(esta_id)
join factor f using(fact_id)
join dimension d using(dime_id)

where acre_id = _acre_id
and auto_estado = 'cerrado'
and auto_completa = 1

group by auto_id