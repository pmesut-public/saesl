select y.*
, count(auto_id) count
from
(
	(select distinct coalesce(inst_subtipo, 'TOTAL_U') inst_subtipo from institucion) union
	(select 'TOTAL_I') union 
	(select 'TOTAL_X')
) y
left join

(select auto_id, inst_subtipo, tiin_id
	#sum(if(tiin_id = 1,1,0)) T_
	#, sum(if(tiin_id = 3,1,0)) T_I
	#, count(auto_id) T
	from autoevaluacion b
	join acreditado_evento ae using(acev_id)
	join acreditado a using(acre_id)
	join users u on a.acre_id = u.id
	join acreditado_institucion ai using(acre_id)
	join institucion i using(inst_id)
	#left join acreditado_carrera ac using(acin_id)
	where b.auto_estado = 'cerrado'
	and b.auto_completa = 1
	and a.acre_test = 0
	and u.status = 1
) z
on
	(
		y.inst_subtipo = z.inst_subtipo or
		(y.inst_subtipo = 'TOTAL_I' AND z.tiin_id = 3) or
		(y.inst_subtipo = 'TOTAL_U' AND z.tiin_id = 1) or
		(y.inst_subtipo = 'TOTAL_X')
	)

group by y.inst_subtipo
order by y.inst_subtipo
