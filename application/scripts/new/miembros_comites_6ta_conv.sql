select a.acre_id, ae.acev_id
, e.even_id
, e.even_per_minimo
, inst_nombre institucion
, coalesce(carr_nombre, '') carrera
, max_auto_id
, auto_porcentaje

#, p.*
, pers_nombres nombres
, pers_apellidos apellidos
, pers_cargo cargo
, pers_telefono telefono
, pers_correo correo
, pers_tipo tipo
#, group_concat(b.auto_numero order by b.auto_id) auto_numeros
#, count(b.auto_id) autos
#, max(b.auto_id) max_auto_id

from acreditado a
join persona p using(acre_id)
join users u on a.acre_id = u.id
join acreditado_evento ae using(acre_id)
join evento e using(even_id)

#join autoevaluacion b using(acev_id)

join (
	select acev_id, max(auto_id) max_auto_id
	from autoevaluacion
	where auto_estado = 'cerrado'
	group by acev_id
) max on ae.acev_id = max.acev_id

join (
	select auto_id, aude_id, inst_nombre, carr_nombre
	, if(
		a.moda_id = 1 and tiin_id = 1, 
		format(coalesce((sum(if((aude_cumplimiento = 1) and (esta_opcional = 0), 1, 0)) / sum(if((esta_opcional = 0), 1, 0)) * 100), 0), 2), 
		format((sum(if((aude_cumplimiento = 1), 1, 0)) / count(esta_id) * 100), 2)
	) auto_porcentaje

	from autoevaluacion_detalle ad
	join estandar e using(esta_id)
	join autoevaluacion b using(auto_id)
	join acreditado_evento ae using(acev_id)
	join acreditado a using(acre_id)
	join acreditado_institucion ai using(acre_id)
	join institucion i using(inst_id)
	join acreditado_carrera ac using(acin_id)
	join carrera c using(carr_id)
	group by auto_id
) per on max_auto_id = per.auto_id

where even_id in (18, 19)
and u.status = 1
and a.acre_aprobado = 1
and a.acre_test = 0
and ae.acev_estado <> 5
and p.pers_tipo in ('contacto', 'comision')
#and b.auto_estado = 'cerrado'

group by acev_id, pers_id
having auto_porcentaje >= even_per_minimo

limit 10000
