select a.obac_id, o.obac_nombre, e.esta_codigo, e.esta_nivel_aceptacion
#, group_concat(auto_id) zzz
, count(auto_id) total_auto
, sum(if(aude_cumplimiento = 1,1,0)) total_cumplen
, sum(if(ad.aude_aceptacion = 1,1,0)) n1
, sum(if(ad.aude_aceptacion = 2,1,0)) n2
, sum(if(ad.aude_aceptacion = 3,1,0)) n3
, sum(if(ad.aude_aceptacion = 4,1,0)) n4
, sum(if(ad.aude_aceptacion = 5,1,0)) n5

from autoevaluacion b
join acreditado_evento ae using(acev_id)
join acreditado a using(acre_id)
join users u on a.acre_id = u.id
join objeto_acreditacion o using(obac_id)
join autoevaluacion_detalle ad using(auto_id)
join estandar e using(esta_id)

where b.auto_completa = 1
and b.auto_estado = 'cerrado'
and a.acre_test = 0
and u.status = 1
and tiin_id = 3

group by e.esta_id
order by a.obac_id, e.esta_id
