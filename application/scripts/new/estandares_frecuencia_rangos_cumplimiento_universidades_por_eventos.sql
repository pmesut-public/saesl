select a.obac_id, o.obac_nombre, e.esta_codigo#, e.esta_nivel_aceptacion#, g_name
#, group_concat(auto_id) zzz
, count(auto_id) total_auto
, sum(if(aude_cumplimiento = 1,1,0)) total_cumplen
#, sum(if(ad.aude_aceptacion = 1,1,0)) n1
#, sum(if(ad.aude_aceptacion = 2,1,0)) n2
#, sum(if(ad.aude_aceptacion = 3,1,0)) n3
#, sum(if(ad.aude_aceptacion = 4,1,0)) n4
#, sum(if(ad.aude_aceptacion = 5,1,0)) n5

, sum(if(auto_per < 40, 1, 0)) total_menor_40
, sum(if(auto_per < 40 and aude_cumplimiento = 1, 1, 0)) cumplen_menor_40

, sum(if(auto_per >= 40 and auto_per < 83, 1, 0)) total_entre_40_83
, sum(if(auto_per >= 40 and auto_per < 83 and aude_cumplimiento = 1, 1, 0)) cumplen_entre_40_83

, sum(if(auto_per > 83, 1, 0)) total_mayor_83
, sum(if(auto_per > 83 and aude_cumplimiento = 1, 1, 0)) cumplen_mayor_83

#, group_concat(auto_id) aaa
#, group_concat(auto_per) ppp

from autoevaluacion b
join acreditado_evento ae using(acev_id)
join acreditado a using(acre_id)
join users u on a.acre_id = u.id
join acreditado_institucion ai using(acre_id)
join institucion i using(inst_id)
join objeto_acreditacion o using(obac_id)
join autoevaluacion_detalle ad using(auto_id)
join estandar e using(esta_id)
#join (select 1 g_id, 'PUBLICA' g_name union select 2, 'PRIVADA' union select 3, 'TOTAL') gestion on (gestion.g_name = inst_gestion or gestion.g_name = 'TOTAL')

join (
	select auto_id
	, sum(if(aude_cumplimiento = 1, 1, 0)) / count(esta_id) * 100 auto_per
	from autoevaluacion b
	join autoevaluacion_detalle ad using(auto_id)
	group by auto_id
) per using(auto_id)

where b.auto_completa = 1
and b.auto_estado = 'cerrado'
and a.acre_test = 0
and u.status = 1
#and auto_per < 75
#and auto_per < 40
#and o.tiin_id = 3
#and inst_region = 'JUNIN'
#and auto_fecha_fin between '2015-03-01' and '2015-04-30'
#and inst_gestion = 'PRIVADA'
#and inst_gestion = 'PUBLICA'
and even_id = 1

group by /*gestion.g_id, */e.esta_id
order by a.obac_id, e.esta_id#, gestion.g_id
limit 9999
