select tiin_nombre, obac_nombre, dime_codigo, dime_titulo, fact_codigo, fact_titulo, crit_codigo, coalesce(crit_titulo, crit_descripcion) crit_nombre
, esta_codigo, coalesce(esta_titulo, esta_descripcion) esta_nombre, e.esta_tipo, e.esta_nivel_aceptacion, moda_id, conc_codigo, conc_nombre, conc_obligatorio
/* */, cump.*
from tipo_institucion ti
join objeto_acreditacion o using(tiin_id)
join dimension d using(obac_id)
join factor f using(dime_id)
join criterio c using(fact_id)	
join estandar e using(crit_id)
join concepto co using(esta_id)
/* */join (
	select esta_id, group_concat(cump_numero) cumplimientos
	, group_concat(cump_condicion) cump_condiciones
	from cumplimiento
	group by esta_id
) cump using(esta_id)

where tiin_id = 3

limit 100000
