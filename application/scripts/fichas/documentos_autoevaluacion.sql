SELECT 
documento.*
FROM documento
INNER JOIN autoevaluacion_documento USING(docu_id)
INNER JOIN autoevaluacion_medio USING(aume_id)
INNER JOIN autoevaluacion_detalle USING(aude_id)
WHERE autoevaluacion_detalle.auto_id = :autoevaluacion_id
AND documento.docu_estado = 'activo'
GROUP BY documento.docu_id;
