SELECT
factor.*,
estandar.*,
documento.*
FROM documento
INNER JOIN autoevaluacion_documento USING(docu_id)
INNER JOIN autoevaluacion_medio USING(aume_id)
INNER JOIN autoevaluacion_detalle USING(aude_id)
INNER JOIN estandar ON autoevaluacion_detalle.esta_id = estandar.esta_id
INNER JOIN factor ON estandar.fact_id = factor.fact_id
WHERE autoevaluacion_detalle.auto_id = :autoevaluacion_id;