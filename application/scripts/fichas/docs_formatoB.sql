/* Selecciona los documentos del ACRE_ID usado en formato B */
SELECT *FROM documento
INNER JOIN autoevaluacion_condicion USING(docu_id)
INNER JOIN dimension USING(dime_id)
WHERE documento.acre_id = :acre_id;