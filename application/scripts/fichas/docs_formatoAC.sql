/* Selecciona los documentos del ACRE_ID usado en formato A */
SELECT *FROM documento 
INNER JOIN autoevaluacion_formato ON documento.docu_id = autoevaluacion_formato.docu_id
INNER JOIN formato ON autoevaluacion_formato.form_id = formato.form_id
WHERE documento.acre_id = :acre_id
AND formato.form_columna = ':formato';