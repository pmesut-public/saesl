select x.*, pp.*, y.*

#, count(acre_id) count
, count(auto_id) autos
, coalesce(sum(prom), 0) suma_
, coalesce(sum(prom), 0) / if(count(auto_id), count(auto_id), 1) prom_
, format(coalesce(sum(prom), 0) / if(count(auto_id), count(auto_id), 1), 2) prom__
from
(
	(select concat(dime_codigo, '. ', dime_titulo) dime_titulo, fact_codigo, concat(fact_codigo, '. ', fact_titulo) fact_titulo
	from dimension d
	join factor f using(dime_id)
	join objeto_acreditacion o using(obac_id)
	#join criterio c using(fact_id)
	#join estandar e using(crit_id)
	where tiin_id = 3
	#and esta_clasificacion = 'M' 
	and obac_id = _obac_id) union 
	(select '1. GESTIÓN INSTITUCIONAL', '1.9', 'Promedio Dimensión') union
	(select '2. PROCESOS ACADÉMICOS'  , '2.9', 'Promedio Dimensión') union
	(select '3. SERVICIOS  DE  APOYO' , '3.9', 'Promedio Dimensión') union
	(select '4. RESULTADOS E IMPACTO' , '4.9', 'Promedio Dimensión')
) x
join
(select 1 pp_id, 'Pública' gestion union select 2 pp_id, 'Privada' gestion union select 3 pp_id, 'Todas' gestion) pp
join
(
	(select distinct inst_region from institucion) union
	(select 'xTOTAL' inst_region)
) y

left join

(
	select inst_region, inst_gestion, auto_id, inst_subtipo, a.obac_id
	, f.*, d.dime_codigo

	, coalesce(sum(aude_aceptacion), 0) suma
	, count(aude_id) count
	, coalesce(sum(aude_aceptacion), 0) / count(aude_id) raw_prom
	, format(coalesce(sum(aude_aceptacion), 0) / count(aude_id), 2) prom

	from institucion i
	join acreditado_institucion ai using(inst_id)
	join acreditado a using(acre_id)
	#join objeto_acreditacion o using(obac_id)
	join acreditado_evento ae using(acre_id)
	join autoevaluacion b using (acev_id)

	join autoevaluacion_detalle ad using(auto_id)
	join estandar e using(esta_id)
	join criterio c using(crit_id)
	join factor f using(fact_id)
	join dimension d using(dime_id)

	join users u on a.acre_id = u.id
	where a.acre_test = 0
	and b.auto_completa = 1
	and b.auto_estado = 'cerrado'
	and u.status = 1
	and tiin_id = 3
	and a.obac_id = _obac_id
	#and e.esta_clasificacion = 'M' 

	group by b.auto_id, fact_id
	#order by b.auto_id, fact_id
) z

on 
	(
		#x.fact_id = z.fact_id
		x.fact_codigo = z.fact_codigo or
		(x.fact_codigo = 1.9 and z.dime_codigo = 1) or
		(x.fact_codigo = 2.9 and z.dime_codigo = 2) or
		(x.fact_codigo = 3.9 and z.dime_codigo = 3) or
		(x.fact_codigo = 4.9 and z.dime_codigo = 4)
		#y.fact_codigo = 
	) and 
	(y.inst_region = z.inst_region or y.inst_region = 'xTOTAL') and 
	(pp.gestion = z.inst_gestion or pp.pp_id = 3) #and 

group by x.fact_codigo, pp.gestion, y.inst_region
order by x.fact_codigo, pp.pp_id, y.inst_region

limit 10000
