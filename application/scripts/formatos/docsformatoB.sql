/* Documentos del FORMATO B */
SELECT
CONCAT('Formato B') as docu_locacion,
CONCAT('', '') as estandar_codigo,
CONCAT('Condicion ', dimension.dime_codigo) as formato_nombre,
documento.*
FROM documento
INNER JOIN autoevaluacion_condicion USING(docu_id)
INNER JOIN dimension USING(dime_id)
WHERE autoevaluacion_condicion.auto_id = :autoevaluacion_id;
