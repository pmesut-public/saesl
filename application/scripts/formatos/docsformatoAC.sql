/* Documentos del FORMATO A - C */
SELECT 
CONCAT('Formato ', form_columna) as docu_locacion,
CONCAT('', '') as estandar_codigo,
formato.form_nombre as formato_nombre,
documento.* 
FROM documento
INNER JOIN autoevaluacion_formato using(docu_id)
INNER JOIN formato USING(form_id)
WHERE autoevaluacion_formato.auto_id = :autoevaluacion_id
AND formato.form_columna = ':formato';