/* Documentos de la Autoevaluación */
SELECT 
CONCAT('I', estandar.esta_codigo) as docu_locacion,
estandar.esta_codigo as estandar_codigo,
documento.*
FROM documento
INNER JOIN autoevaluacion_documento USING(docu_id)
INNER JOIN autoevaluacion_medio USING(aume_id)
INNER JOIN autoevaluacion_detalle USING(aude_id)
INNER JOIN estandar ON autoevaluacion_detalle.esta_id = estandar.esta_id
WHERE documento.docu_estado = 'activo'
AND autoevaluacion_detalle.auto_id = :autoevaluacion_id;