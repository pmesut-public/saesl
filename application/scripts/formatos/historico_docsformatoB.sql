/* Documentos Históricos del Formato B */
SELECT
CONCAT('Formato B') as docu_locacion,
CONCAT('', '') as estandar_codigo,
CONCAT('Condicion ', dimen.dime_codigo) as formato_nombre,
doc2.* 
FROM documento doc1
INNER JOIN documento doc2 ON doc1.docu_id = doc2.parent_senior_id
INNER JOIN autoevaluacion_condicion auto_cond ON doc1.docu_id = auto_cond.docu_id
INNER JOIN dimension dimen ON auto_cond.dime_id = dimen.dime_id
WHERE auto_cond.auto_id = :autoevaluacion_id;
