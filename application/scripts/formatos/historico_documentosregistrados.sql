/* Documentos Históricos de la Autoevaluación */
SELECT 
CONCAT('I', estandar.esta_codigo) as docu_locacion,
estandar.esta_codigo as estandar_codigo,
CONCAT('', '') as formato_nombre,
documento.* 
FROM documento
INNER JOIN autoevaluacion_documento ON documento.docu_id = autoevaluacion_documento.docu_id
INNER JOIN autoevaluacion_medio USING(aume_id)
INNER JOIN autoevaluacion_detalle USING(aude_id)
INNER JOIN estandar ON autoevaluacion_detalle.esta_id = estandar.esta_id
WHERE autoevaluacion_detalle.auto_id = :autoevaluacion_id
AND documento.docu_estado = 'reemplazado';


/*
 * Consulta anterior
 *
SELECT 
CONCAT('I', estandar.esta_codigo) as docu_locacion,
estandar.esta_codigo as estandar_codigo,
CONCAT('', '') as formato_nombre,
doc2.* 
FROM documento doc1
INNER JOIN documento doc2 ON doc1.docu_id = doc2.parent_senior_id
INNER JOIN autoevaluacion_documento ON doc2.docu_id = autoevaluacion_documento.docu_id
INNER JOIN autoevaluacion_medio USING(aume_id)
INNER JOIN autoevaluacion_detalle USING(aude_id)
INNER JOIN estandar ON autoevaluacion_detalle.esta_id = estandar.esta_id
WHERE autoevaluacion_detalle.auto_id = :autoevaluacion_id;
*/