SELECT 
	documento.docu_id as docu_id,
	GROUP_CONCAT(CONCAT('I', estandar.esta_codigo) separator ', ') as esta_locacion
	FROM
	documento
	INNER JOIN autoevaluacion_documento USING(docu_id)
	INNER JOIN autoevaluacion_medio USING(aume_id)
	INNER JOIN autoevaluacion_detalle USING(aude_id)
	INNER JOIN estandar ON autoevaluacion_detalle.esta_id = estandar.esta_id
	WHERE docu_id = :documento_id
	GROUP BY (docu_id);


/*
SELECT 
	autoevaluacion_detalle.aude_id as aude_id,
	autoevaluacion_medio.aume_id as aume_id,
	autoevaluacion_documento.audo_id as audo_id,
	documento.docu_id as docu_id,
	estandar.esta_id as esta_id,
	estandar.esta_codigo as esta_codigo,
	CONCAT('I', estandar.esta_codigo) as esta_locacion
	FROM
	documento
	INNER JOIN autoevaluacion_documento USING(docu_id)
	INNER JOIN autoevaluacion_medio USING(aume_id)
	INNER JOIN autoevaluacion_detalle USING(aude_id)
	INNER JOIN estandar ON autoevaluacion_detalle.esta_id = estandar.esta_id
	WHERE docu_id = :documento_id;
*/