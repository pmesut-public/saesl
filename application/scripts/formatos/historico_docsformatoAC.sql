/* Documentos Históricos de los Formatos A - C */
SELECT 
CONCAT('Formato ', form_columna) as docu_locacion,
CONCAT('', '') as estandar_codigo,
forma.form_nombre as formato_nombre,
doc2.* 
FROM documento doc1
INNER JOIN documento doc2 ON doc1.docu_id = doc2.parent_senior_id
INNER JOIN autoevaluacion_formato auto_for ON doc1.docu_id = auto_for.docu_id
INNER JOIN formato forma ON auto_for.form_id = forma.form_id
WHERE auto_for.auto_id = :autoevaluacion_id
AND forma.form_columna = ':formato';