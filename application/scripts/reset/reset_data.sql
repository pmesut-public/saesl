DELETE FROM autoevaluacion;
DELETE FROM acreditado;
DELETE FROM documento;
DELETE FROM persona;
DELETE FROM datos_institucionales;
DELETE FROM institucion WHERE inst_id > 1;
DELETE FROM users WHERE id > 1;

ALTER TABLE users AUTO_INCREMENT = 0;
ALTER TABLE institucion AUTO_INCREMENT = 0;
ALTER TABLE carrera AUTO_INCREMENT = 0;
ALTER TABLE persona AUTO_INCREMENT = 0;

ALTER TABLE autoevaluacion AUTO_INCREMENT = 0;
ALTER TABLE autoevaluacion_condicion AUTO_INCREMENT = 0;
ALTER TABLE autoevaluacion_detalle AUTO_INCREMENT = 0;
ALTER TABLE autoevaluacion_documento AUTO_INCREMENT = 0;
ALTER TABLE autoevaluacion_formato AUTO_INCREMENT = 0;
ALTER TABLE autoevaluacion_medio AUTO_INCREMENT = 0;

ALTER TABLE acreditado AUTO_INCREMENT = 0;
ALTER TABLE acreditado_documento AUTO_INCREMENT = 0;
ALTER TABLE acreditado_evento AUTO_INCREMENT = 0;
ALTER TABLE acreditado_evento_ficha AUTO_INCREMENT = 0;
ALTER TABLE acreditado_institucion AUTO_INCREMENT = 0;
ALTER TABLE acreditado_objeto AUTO_INCREMENT = 0;


ALTER TABLE objeto_acreditacion AUTO_INCREMENT = 0;
ALTER TABLE dimension AUTO_INCREMENT = 0;
ALTER TABLE factor AUTO_INCREMENT = 0;
ALTER TABLE estandar AUTO_INCREMENT = 0;
ALTER TABLE medio AUTO_INCREMENT = 0;