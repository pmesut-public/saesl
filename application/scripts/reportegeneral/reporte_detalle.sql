SELECT
autoevaluacion_detalle.*,
COUNT(autoevaluacion_medio.aume_id) as rows_aume
FROM dimension
INNER JOIN factor USING (dime_id)
INNER JOIN estandar USING (fact_id)
INNER JOIN autoevaluacion_detalle USING (esta_id)
INNER JOIN autoevaluacion_medio USING (aude_id)
WHERE autoevaluacion_detalle.auto_id = :autoevaluacion_id
GROUP BY autoevaluacion_detalle.aude_id;