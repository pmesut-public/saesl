SELECT
factor.*,
COUNT(autoevaluacion_medio.aume_id) as rows_items
FROM dimension
INNER JOIN factor USING (dime_id)
INNER JOIN estandar USING (fact_id)
INNER JOIN autoevaluacion_detalle USING (esta_id)
INNER JOIN autoevaluacion_medio USING (aude_id)
WHERE autoevaluacion_detalle.auto_id = :autoevaluacion_id
GROUP BY factor.fact_id;