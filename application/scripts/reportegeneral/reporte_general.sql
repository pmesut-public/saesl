SELECT *FROM dimension
LEFT JOIN factor USING (dime_id)
LEFT JOIN estandar USING (fact_id)
LEFT JOIN autoevaluacion_detalle USING (esta_id)
LEFT JOIN autoevaluacion_medio USING (aude_id)
LEFT JOIN autoevaluacion_documento USING (aume_id)
WHERE autoevaluacion_detalle.auto_id = :autoevaluacion_id
AND dimension.dime_estado = 'activo'
AND factor.fact_estado = 'activo'
AND estandar.esta_estado = 'activo'
ORDER BY dimension.dime_codigo, factor.fact_codigo, estandar.esta_codigo;