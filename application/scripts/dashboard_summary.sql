select * from
	(select count(acre_id) acres
	, sum(if(a.acre_aprobado = 0, 1, 0)) new_users
	, sum(if(a.acre_aprobado = 1 and o.tiin_id = 1, 1, 0)) num_univ
	, sum(if(a.acre_aprobado = 1 and o.tiin_id = 3, 1, 0)) num_inst
	from acreditado a
	join objeto_acreditacion o using(obac_id)
	join users u on a.acre_id = u.id
	where a.acre_test = 0
	and u.status = 1) foo
join
	(select count(DISTINCT(ae.auto_id)) num_auto
	from autoevaluacion ae 
	join acreditado_evento acev using(acev_id) 
	join acreditado a using(acre_id) 
	join users u on a.acre_id = u.id 
        join acreditado_institucion ai on a.acre_id = ai.acre_id
        join institucion i on i.inst_id = ai.inst_id
        left join acreditado_objeto ao on ao.acin_id = ai.acin_id
        left join carrera ca on ca.carr_id = ao.carr_id
        join autoevaluacion_detalle aud on aud.auto_id = ae.auto_id
        join estandar est on est.esta_id = aud.esta_id
	where a.acre_test = 0 
	and a.acre_aprobado = 1 
	and u.status = 1 
	and ae.auto_estado <> 'cancelado') bar
join
	(select count(*) comite_pending
	from acreditado a 
	join persona p using(acre_id) 
	join users u on a.acre_id = u.id 
	where pers_tipo = 'pending_contacto' 
	and a.acre_test = 0 
	and a.acre_aprobado = 1 
	and u.status = 1) baz
