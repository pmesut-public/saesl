select 
#a.acre_id, b.*, i.*, max.*
concat(tiin_nombre, ' - ', obac_nombre) objeto
, moda_nombre
, concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede) inst_name
, carr_nombre
, inst_gestion
#, auto_id
, max(auto_fecha_fin) auto_fecha_fin
#, cump
#, per
, per_format
#, max(b.auto_id) auto_id

from acreditado a
join acreditado_evento ae using(acre_id)
join autoevaluacion b using(acev_id)
#join autoevaluacion_detalle ad using(auto_id)

join users u on a.acre_id = u.id
join acreditado_institucion ai using(acre_id)
join institucion i using(inst_id)
left join acreditado_carrera ac using(acin_id)
left join carrera c using(carr_id)
join objeto_acreditacion o using(obac_id)
join tipo_institucion ti on o.tiin_id = ti.tiin_id
join modalidad m using(moda_id)

join (
	select auto_id
	, sum(if((aude_cumplimiento = 1), 1, 0)) cump
	, sum(if((aude_cumplimiento = 1), 1, 0)) / count(esta_id) * 100 per
	, format(sum(if((aude_cumplimiento = 1), 1, 0)) / count(esta_id) * 100, 2) + 0.0 per_format
	from autoevaluacion
	join autoevaluacion_detalle using(auto_id)
	group by auto_id
) per using(auto_id)

#left 
join (
	select acre_id
	#, max(auto_fecha_fin) max_auto_fecha_fin
	, max(per) max_per
	from acreditado 
	join acreditado_evento using(acre_id) 
	join autoevaluacion b using(acev_id)
	join (
		select auto_id
		, sum(if((aude_cumplimiento = 1), 1, 0)) / count(esta_id) * 100 per
		from autoevaluacion
		join autoevaluacion_detalle using(auto_id)
		group by auto_id
	) pp using(auto_id)
		
	where b.auto_estado = 'cerrado'
	and b.auto_completa = 1
	group by acre_id
#) max on (a.acre_id = max.acre_id and b.auto_fecha_fin = max.max_auto_fecha_fin)
) max on (a.acre_id = max.acre_id and per.per = max.max_per)

where u.status = 1
and a.acre_test = 0
and b.auto_estado = 'cerrado'
and b.auto_completa = 1
and o.tiin_id = _tiin_id

#group by auto_id
group by a.acre_id
#having per_format > 0
order by o.tiin_id, per desc
