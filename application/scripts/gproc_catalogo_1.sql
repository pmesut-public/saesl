SELECT c.*
	, if(c.id = 'R', 'RPP,RPC', group_concat(cd.id)) children 
	FROM catalogo as c
	LEFT JOIN  catalogo as cd ON c.id = cd.parent_id 
	WHERE c.id <> 'RV'
	GROUP BY c.id
	UNION
	select 'RPP','Pasantías','Pasantías',null,null,'RPT,RPA'
	UNION
	select 'RPC','Capacitación','Capacitación',null,null,'RPT,RPA';