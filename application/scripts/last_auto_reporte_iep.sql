#/*
select max.* 
, d.dime_codigo#, d.dime_titulo
, f.fact_codigo#, f.fact_titulo
, e.esta_codigo#, coalesce(e.esta_titulo, e.esta_descripcion) e_titulo
, co.conc_codigo#, co.conc_nombre

, coalesce(ad.aude_cumplimiento, 0) cumple_estandar
, coalesce(cd.code_cumple, 0) cumple_fuente

, esta_nivel_aceptacion
, aude_aceptacion

, if (ti.tiin_id = 1, coalesce(aude_cumplimiento, 0), coalesce(aude_aceptacion, 1)) cumplimiento
, if (ti.tiin_id = 1, 1, esta_nivel_aceptacion) puntaje

, if (ti.tiin_id = 1, concat(dime_codigo, '.', fact_codigo), fact_codigo) fact
, if (ti.tiin_id = 1, concat(dime_codigo, '.', fact_codigo, '.', esta_codigo), esta_codigo) esta

, i.*, cc.carr_nombre, o.obac_nombre, ti.tiin_nombre, m.moda_nombre, auto_fecha_inicio, auto_fecha_fin
, e.esta_id

from acreditado a
join acreditado_evento ae using(acre_id)
join autoevaluacion b using(acev_id)
join autoevaluacion_detalle ad using(auto_id)
join estandar e using(esta_id)
join concepto co using(esta_id)
left join concepto_detalle cd on(co.conc_id = cd.conc_id and cd.aude_id = ad.aude_id)
join criterio c using(crit_id)
join factor f using(fact_id)
join dimension d using(dime_id)

join modalidad m on a.moda_id = m.moda_id
join objeto_acreditacion o on o.obac_id = a.obac_id
join tipo_institucion ti on o.tiin_id = ti.tiin_id
join acreditado_institucion ai using(acre_id)
join institucion i using(inst_id)
left join acreditado_carrera ac using(acin_id)
left join carrera cc using(carr_id)

join (#*/
	select a.acre_id, auto_id, per, cump, count
	from acreditado a
	join users u on a.acre_id = u.id
	join acreditado_evento ae using(acre_id)
	join autoevaluacion b using(acev_id)

	join (
		select auto_id, auto_numero
		, sum(if((aude_cumplimiento = 1), 1, 0)) cump
		, count(esta_id) count
		, sum(if((aude_cumplimiento = 1), 1, 0)) / count(esta_id) * 100 per
		, format(sum(if((aude_cumplimiento = 1), 1, 0)) / count(esta_id) * 100, 2) + 0.0 per_format
		from autoevaluacion b
		join acreditado_evento ae using(acev_id)
		join autoevaluacion_detalle ad using(auto_id)

		where b.auto_estado = 'cerrado'
		and ae.acev_estado <> 5
		and b.auto_completa = 1
		and b.auto_fecha_fin <= '_date'
		group by auto_id
	) per using(auto_id)

	join (
		select acre_id
		, max(auto_numero) max_auto_numero
		from acreditado a
		join acreditado_evento ae using(acre_id) 
		join autoevaluacion b using(acev_id)
		/*join (
			select auto_id
			, sum(if((aude_cumplimiento = 1), 1, 0)) / count(esta_id) * 100 per
			from autoevaluacion
			join autoevaluacion_detalle using(auto_id)
			group by auto_id
		) pp using(auto_id)*/

		where b.auto_estado = 'cerrado'
		and ae.acev_estado <> 5
		and b.auto_completa = 1
		and b.auto_fecha_fin <= '_date'
		and a.moda_id = _moda_id
		group by acre_id
	) max on a.acre_id = max.acre_id and per.auto_numero = max_auto_numero

	where a.acre_test = 0
	and u.status = 1
	and a.obac_id = _obac_id
	group by a.acre_id
#/*
) max using(auto_id)

#order by auto_id, dime_codigo, fact_codigo, esta_codigo, conc_codigo
order by auto_id, dime_id, fact_id, esta_id, cast(substr(conc_codigo, 3) as signed) #conc_codigo
limit 100000#*/
