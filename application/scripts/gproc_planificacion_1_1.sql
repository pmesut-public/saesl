select *
, a.moda_id moda_id
, concat(dime_codigo, '. ', dime_titulo) dime
, concat(fact_codigo, '. ', fact_titulo) fact
, concat(crit_codigo, '. ', coalesce(crit_titulo, crit_descripcion)) crit
, concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta

, if(((3 - aude_calidad) / 2) > 0.5, 1, 0) aprobados 
, if(aude_cumplimiento = 1 or esta_tipo = 'satisfaccion', 'Opcionales', 'Obligatorios') tipo

from autoevaluacion_detalle ad
join estandar e using(esta_id)
join criterio c using(crit_id)
join factor f using(fact_id)
join dimension d using(dime_id)
join objeto_acreditacion o using(obac_id)
join autoevaluacion b using(auto_id)
join acreditado_evento ae using(acev_id)
join acreditado a using(acre_id)

where auto_id = _auto_id

having aprobados = 0
order by dime_id, fact_id, esta_id

/*

**** if aude_calidad = null, aprobados = 0

179│
180┤
191┐
192└
193┴
194┬
195├
196─
197┼
217┘
218┌

▒▒▒▒	SHOW
▓▓▓▓	OBLIGATORIOS
AC		cumplimiento estándar
xQ		promedio FV calidades

            ┌─────────┬─────────┐
            │  AC=1   │  AC=0   │
            ├────┬────┼────┬────┤
            │ S  │ NS │ S  │ NS │
┌───────────┼────┼────┼────┼────┤
│ xQ > 0,5  │    │    │▒▒▒▒│▓▓▓▓│ * posible upd: Satisfacción y relacionado a un sistémico (esta_opcional?)
├───────────┼────┼────┼────┼────┤
│ xQ <= 0,5 │▒▒▒▒│▒▒▒▒│▒▒▒▒│▓▓▓▓│ * 
└───────────┴────┴────┴────┴────┘

*/
