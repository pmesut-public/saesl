select *
, a.moda_id moda_id
, concat(dime_codigo, '. ', dime_titulo) dime
, concat(fact_codigo, '. ', fact_titulo) fact
, concat(crit_codigo, '. ', coalesce(crit_titulo, crit_descripcion)) crit
, concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta
, if(aude_aceptacion * ((3 - aude_calidad) / 2) >= esta_nivel_aceptacion, 1, 0) aprobados
, if(aude_cumplimiento = 1, 'Opcionales', 'Obligatorios') tipo

from autoevaluacion_detalle ad
join estandar e using(esta_id)
join criterio c using(crit_id)
join factor f using(fact_id)
join dimension d using(dime_id)
join objeto_acreditacion o using(obac_id)
join autoevaluacion b using(auto_id)
join acreditado_evento ae using(acev_id)
join acreditado a using(acre_id)

where auto_id = _auto_id

having aprobados = 0
order by dime_id, fact_id, esta_id

/*

**** if aude_calidad = null, aprobados = 0
**** AA * xQ >= ENA === (xQ > 0,5)

179│
180┤
191┐
192└
193┴
194┬
195├
196─
197┼
217┘
218┌

▒▒▒▒	SHOW
▓▓▓▓	OBLIGATORIOS
AC		cumplimiento estándar
AA		puntaje de estándar
ENA		puntaje mínimo estándar
xQ		promedio FV calidades

                 ┌────────┬────────┐
                 │  AC=1  │  AC=0  │
┌────────────────┼────────┼────────┤
│ AA * xQ >= ENA │        │▓▓▓▓▓▓▓▓│
├────────────────┼────────┼────────┤
│ AA * xQ < ENA  │▒▒▒▒▒▒▒▒│▓▓▓▓▓▓▓▓│
└────────────────┴────────┴────────┘

*/
