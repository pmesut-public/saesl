/*select count(a.acre_id) y
, concat(tiin_nombre, ' - ', obac_nombre) name
from objeto_acreditacion o
join tipo_institucion ti using(tiin_id)
join acreditado a on a.obac_id = o.obac_id 
join users u on a.acre_id = u.id
where u.status = 1
and a.acre_aprobado = 1 
and a.acre_test = 0

and tiin_id = _tiin_id

group by o.obac_id*/

select count(z.acre_id) y
, name
from (
	select obac_id, concat(tiin_nombre, ' - ', obac_nombre) name
	from objeto_acreditacion o
	join tipo_institucion ti using(tiin_id)
	where tiin_id = _tiin_id
	union (select 99, 'TOTAL')
) x
left join 
(select * from acreditado a 
join users u on a.acre_id = u.id
join objeto_acreditacion o using(obac_id)
where u.status = 1
and a.acre_aprobado = 1 
and a.acre_test = 0
) z on (x.obac_id = z.obac_id or x.obac_id = 99 and z.tiin_id = _tiin_id)

group by x.obac_id, x.name
having y > 0