
select a.acre_id, /*ti.tiin_nombre,*/ o.obac_nombre, inst_nombre, carr_nombre, auto_estado, ae.acev_id
, concat_ws(' - ', inst_nombre, inst_subtipo, inst_region, inst_prov, inst_dist, inst_nombre_sede) inst_name

# Evaluados
, sum(if(aude_estado = 'proceso', 1, 0)) sum_processed

# Total
, count(esta_id) total_est

# Cumplidos
, coalesce(sum(if(aude_cumplimiento = 1, 1, 0)), 0) sum_est

# Porcentaje
, concat(round(coalesce(sum(if(aude_cumplimiento = 1, 1, 0)), 0) / count(esta_id) * 100, 2), '%') perc

# Minedu/Sunedu
, coalesce(sum(if(coalesce(aude_cumplimiento, 0) <> 1 and esta_clasificacion = '_char', 1, 0)), 0) sum_minedu

# Sineace
, if(coalesce(sum(if(aude_cumplimiento = 1, 1, 0)), 0) / count(esta_id) * 100 <= 40, 
	sum(if(coalesce(aude_cumplimiento, 0) <> 1 and coalesce(esta_clasificacion, 0) <> '_char', 1, 0)), '-') sineace

# Procalidad
, if(coalesce(sum(if(aude_cumplimiento = 1, 1, 0)), 0) / count(esta_id) * 100 > 40, 
	sum(if(coalesce(aude_cumplimiento, 0) <> 1 and coalesce(esta_clasificacion, 0) <> '_char', 1, 0)), '-') procalidad

from acreditado a 
join users u on (a.acre_id = u.id) 
join objeto_acreditacion o using (obac_id) 
join acreditado_evento ae on ( a.acre_id = ae.acre_id and ae.even_id = _even_id) 
join acreditado_institucion ai on (a.acre_id = ai.acre_id) 
join institucion i using (inst_id) 
left join acreditado_carrera ac using (acin_id) 
left join carrera c using (carr_id) 

left join (select acev_id, max(auto_fecha_reg) max_auto_fecha_reg 
		from autoevaluacion
		where /*even_id = 6 and*/ auto_estado <> 'cancelado'
		group by acev_id) foo on ( foo.acev_id = ae.acev_id) 

left join autoevaluacion au on ( ae.acev_id = au.acev_id /*and auto_estado <> 'cancelado'*/ and max_auto_fecha_reg = au.auto_fecha_reg) 
left join autoevaluacion_detalle ad using (auto_id) 
left join estandar e using (esta_id) 

where a.acre_test = 0 
and u.status = 1 
and ae.acev_estado <> 5 
group by acre_id
order by inst_name, carr_nombre
