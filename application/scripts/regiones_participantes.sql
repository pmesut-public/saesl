select count(inst_region) count from
	(select distinct inst_region from acreditado a
	join users u on a.acre_id = u.id
	join acreditado_institucion ai using(acre_id)
	join institucion i using(inst_id)
	left join acreditado_carrera ac using(acin_id)
	where a.acre_test = 0
	and a.acre_aprobado = 1
	and u.status = 1
	) foo
