select x.*, pp.gestion, y.*#, ff.*
, count(acre_id) count

from
(
	(select distinct inst_region from institucion) union
	(select 'xTOTAL' inst_region)
) x
join (select 1 pp_id, 'Pública' gestion union select 2 pp_id, 'Privada' gestion union select 3 pp_id, 'Todas' gestion) pp
join
(
	(select distinct coalesce(inst_subtipo, 'TOTAL_U') inst_subtipo from institucion) union
	(select 'TOTAL_I') union 
	(select 'TOTAL_X')
) y

left join
(
	(select * from acreditado a
	join users u on a.acre_id = u.id
	join acreditado_institucion ai using(acre_id)
	join institucion i using(inst_id)
	left join acreditado_objeto ac using(acin_id)
	where a.acre_test = 0
	and a.acre_aprobado = 1
	and u.status = 1
	)
) z
on
	(x.inst_region = z.inst_region or x.inst_region = 'xTOTAL') and
	(pp.gestion = z.inst_gestion or pp.gestion = 'Todas') and
	(
		y.inst_subtipo = z.inst_subtipo or
		(y.inst_subtipo = 'TOTAL_I' AND z.tiin_id = 3) or
		(y.inst_subtipo = 'TOTAL_U' AND z.tiin_id = 1) or
		(y.inst_subtipo = 'TOTAL_X')
	)

group by x.inst_region, pp.gestion, y.inst_subtipo, pp.pp_id
order by x.inst_region, pp.pp_id, y.inst_subtipo
