select e.*, foo.*, e.even_id id
, count(acev_id) count
, coalesce(sum(auto_activos), 0) count_act
, coalesce(sum(auto_cerrados), 0) count_cer
from `evento` e

left join
(
	select even_id, acev_id #*
	, sum(if(auto_estado = 'activo', 1, 0)) auto_activos
	, sum(if(auto_estado = 'cerrado', 1, 0)) auto_cerrados
	from acreditado_evento ae
	join acreditado a using(acre_id)
	join acreditado_institucion ai using(acre_id)
	join institucion using(inst_id)
	join users u on (a.acre_id = u.id)
	left join autoevaluacion b using(acev_id)
	where a.acre_test = 0
	and u.status = 1
	and acev_estado in (_statuses)
	group by acev_id
) foo 
using(even_id)

where even_estado = 'activo' 
and even_tipo = _even_tipo 
and e.tiin_id = _tiin_id 

group by e.even_id
