(select #* 
	sum(if(tiin_id = 1,1,0)) Universidades
	, sum(if(tiin_id = 3,1,0)) Institutos
	, count(auto_id) TOTAL
	from autoevaluacion b
	join acreditado_evento ae using(acev_id)
	join acreditado a using(acre_id)
	join users u on a.acre_id = u.id
	join acreditado_institucion ai using(acre_id)
	join institucion i using(inst_id)
	#left join acreditado_carrera ac using(acin_id)
	where b.auto_estado <> 'cancelado'
	and a.acre_test = 0
	and u.status = 1
)
