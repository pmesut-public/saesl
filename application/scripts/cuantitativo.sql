select concat(auto_numero, '. ', auto_fecha_inicio) name
, group_concat(count) data
, group_concat(dime_codigo) ids
, group_concat(dime_titulo) dimensiones

from (
	select b.*, d.*
	, count(esta_id) count
	from acreditado a
	join acreditado_evento ae using(acre_id)
	join autoevaluacion b using(acev_id)
	join autoevaluacion_detalle ad using(auto_id)
	join estandar e using(esta_id)
	join factor f using(fact_id)
	join dimension d using(dime_id)

	where acre_id = _acre_id
	and auto_estado = 'cerrado'
	and auto_completa = 1

	group by auto_id, dime_id
) foo

group by auto_id
