select x.*, y.*, pp.*, ff.*
, count(auto_id) count
from
(
	(select distinct inst_region from institucion) union
	(select 'xTOTAL' inst_region)
) x
join
(
	(select acpi_id
	, acpi_nombre
	from acreditacion_pip ) union 
	(select 4.5 acpi_id, 'ST1U' acpi_nombre) union
	(select 5.5 acpi_id, 'ST2U' acpi_nombre) union
	(select 6.5 acpi_id, 'Subtotal SU' acpi_nombre) union
	(select 8.5 acpi_id, 'ST1T' acpi_nombre) union
	(select 9.5 acpi_id, 'ST2T' acpi_nombre) union
	(select 10.5 acpi_id, 'Subtotal ST' acpi_nombre) union
	(select 11 acpi_id, 'Total' acpi_nombre)
) y
join
(select 1 pp_id, 'Pública' gestion union select 2 pp_id, 'Privada' gestion union select 3 pp_id, 'Subtotal' gestion) pp
join
(
	select '2014-07-01' inicio, '2014-09-30' fin, 't1' trim union 
	select '2014-10-01' inicio, '2014-12-31' fin, 't2' trim union
	select '2015-01-01' inicio, '2015-03-31' fin, 't3' trim union
	select '2015-04-01' inicio, '2015-06-30' fin, 't4' trim union
	select '2014-07-01' inicio, '2015-06-30' fin, 'tt' trim
) ff
left join
(select inst_region, inst_gestion, auto_id, acpi_id, auto_fecha_fin
from institucion i
join acreditado_institucion ai using(inst_id)
join acreditado a using(acre_id)
join objeto_acreditacion o using(obac_id)
join acreditado_evento acev using(acre_id)
join autoevaluacion b using (acev_id)
join users u on a.acre_id = u.id
where a.acre_test = 0
and b.auto_completa = 1
and b.auto_estado = 'cerrado'
and u.status = 1
) z on 
	(x.inst_region = z.inst_region or x.inst_region = 'xTOTAL') and 
	(
		y.acpi_id = z.acpi_id or
		y.acpi_id = 4.5 and z.acpi_id in (1,2,3,4) or
		y.acpi_id = 5.5 and z.acpi_id in (1,2,3,4,5) or
		y.acpi_id = 6.5 and z.acpi_id in (1,2,3,4,5,6) or
		y.acpi_id = 8.5 and z.acpi_id in (7,8) or
		y.acpi_id = 9.5 and z.acpi_id in (7,8,9) or
		y.acpi_id = 10.5 and z.acpi_id in (7,8,9,10) or
		y.acpi_id = 11
	) and 
	(pp.gestion = z.inst_gestion or pp.gestion = 'Subtotal') and 
	auto_fecha_fin between inicio and fin
group by x.inst_region, y.acpi_id, pp.gestion, ff.trim
order by x.inst_region, y.acpi_id, pp.pp_id, ff.trim
limit 10000
