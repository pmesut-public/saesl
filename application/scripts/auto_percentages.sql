select y.*, ff.*
, count(auto_id) count
from
(
	(select distinct coalesce(inst_subtipo, 'TOTAL_U') inst_subtipo from institucion) union
	(select 'TOTAL_I') union 
	(select 'TOTAL_X')
) y
join
(
	select 0 lft, 20 rgt, 't1' q union 
	select 20, 40, 't2' union 
	select 40, 101, 't3' union
	select 0, 101, 'tt'
) ff
left join

(select auto_id, inst_subtipo, tiin_id
	, SUM(if(aude_cumplimiento = 1, 1, 0)) cump
	, count(aude_id) total
	, SUM(if(aude_cumplimiento = 1, 1, 0)) / count(aude_id) * 100 porc
	from autoevaluacion b
	join autoevaluacion_detalle ad using(auto_id)
	join acreditado_evento ae using(acev_id)
	join acreditado a using(acre_id)
	join users u on a.acre_id = u.id
	join acreditado_institucion ai using(acre_id)
	join institucion i using(inst_id)
	#left join acreditado_carrera ac using(acin_id)
	where b.auto_estado = 'cerrado'
	and b.auto_completa = 1
	and a.acre_test = 0
	and u.status = 1
	group by auto_id
) z
on
	(
		y.inst_subtipo = z.inst_subtipo or
		(y.inst_subtipo = 'TOTAL_I' AND z.tiin_id = 3) or
		(y.inst_subtipo = 'TOTAL_U' AND z.tiin_id = 1) or
		(y.inst_subtipo = 'TOTAL_X')
	) and
	(z.porc >= ff.lft and z.porc < ff.rgt or ff.q = 'tt')

group by y.inst_subtipo, ff.q
order by y.inst_subtipo, ff.q
