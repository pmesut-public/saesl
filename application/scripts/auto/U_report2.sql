select * from
	(select distinct esta_tipo from estandar where esta_tipo is not null) x
left join 
	(select esta_tipo
	, coalesce(sum(if(aude_cumplimiento = 1, 1, 0)), 0) cump
	, count(aude_id) total
	from autoevaluacion_detalle ad
	join estandar e using(esta_id)
	where auto_id = _auto_id
	group by esta_tipo) foo
	using(esta_tipo)
left join
	(select e.*, aude_cumplimiento, co.conc_id, conc_obligatorio, code_cumple
	, concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta
	, coalesce(code_nombre, conc_nombre) concepto
	from autoevaluacion_detalle ad
	#left join concepto_detalle cd using(aude_id)
	join estandar e using(esta_id)
	join concepto co using(esta_id)
	left join concepto_detalle cd on(co.conc_id = cd.conc_id and cd.aude_id = ad.aude_id) #using(conc_id)

	where auto_id = _auto_id
	order by e.esta_id, cast(substr(conc_codigo, 3) as signed)
	) bar using(esta_tipo)
