(SELECT *
, dime_titulo dime
, dime_total
# , dime_avance
,(select valo_equivalencia from valoracion where valo_id = FLOOR(dime_avance)) dime_avance 
, fact_titulo fact
, fact_total	
# , (fact_suma / 340) as fact_avance
, (select valo_equivalencia from valoracion where valo_id = FLOOR(fact_suma / fpond_total)) fact_avance
# , (fact_suma / fpond_total) as fact_avance
, coalesce(esta_titulo, esta_descripcion) esta
# , ((aude_valoracion * aude_ponderacion) / 340) valo
, (select valo_equivalencia from valoracion where valo_id = FLOOR((aude_valoracion * aude_ponderacion) / aude_ponderacion)) valo
# , ((aude_valoracion * aude_ponderacion) / aude_ponderacion) valo
, '' as valo_g
FROM autoevaluacion_detalle ad
JOIN estandar e USING(esta_id)
JOIN (
    SELECT f.*
	, dx.dime_codigo
	, dx.dime_titulo
	, dx.dime_total
	# , (dx.dime_suma / 340) as dime_avance
	, (dx.dime_suma / dx.dpond_total) as dime_avance
        , COUNT(fact_id) fact_total 
	, SUM(aude_ponderacion) fpond_total 
	, SUM((aude_valoracion * aude_ponderacion)) fact_suma	
	FROM autoevaluacion_detalle ad
    JOIN estandar e USING(esta_id)
    JOIN factor f USING(fact_id)
    JOIN (
        SELECT d.* 
	, COUNT(esta_id) dime_total
	, SUM(aude_ponderacion) dpond_total
	, SUM((aude_valoracion * aude_ponderacion)) dime_suma
	FROM autoevaluacion_detalle ad
        JOIN estandar e USING(esta_id)
        JOIN factor f USING(fact_id)
        JOIN dimension d USING(dime_id)
        WHERE auto_id = _auto_id
        GROUP BY dime_id
    ) dx USING(dime_id)
    WHERE auto_id = _auto_id
    GROUP BY fact_id
) fx USING(fact_id)
WHERE auto_id = _auto_id
GROUP BY esta_id)

UNION ALL

(SELECT *
, '' dime
, '' dime_total
, '' dime_avance
, '' fact
, '' fact_total
, '' fact_avance
, '' esta
, '' as valo
, (select valo_equivalencia from valoracion where valo_id = FLOOR((SUM(aude_valoracion * aude_ponderacion) / SUM(aude_ponderacion)))) valo_g	
# , SUM(aude_valoracion * aude_ponderacion) / 340 as valo_g
FROM autoevaluacion_detalle ad
JOIN estandar e USING(esta_id)
JOIN factor f USING(fact_id)
JOIN dimension d USING(dime_id)
WHERE auto_id = _auto_id)

