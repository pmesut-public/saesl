SELECT *
, dime_codigo
, dime_titulo dime
, dime_total
, (select esta_evidencia from estados where esta_id = ROUND(dime_avance)) dime_valo
, fact_codigo 
, fact_titulo fact
, (select esta_evidencia from estados where esta_id = ROUND((fact_suma / fact_total))) fact_valo 
, coalesce(esta_titulo, esta_descripcion) esta
, (select esta_evidencia from estados where esta_id = ad.aude_valoracion) valo
FROM autoevaluacion_detalle ad
JOIN estandar e USING(esta_id)
JOIN (
    SELECT f.*, ''
	, dx.dime_codigo
	, dx.dime_titulo
	, dx.dime_total
	, (dx.dime_suma / dx.dime_total) as dime_avance
	, COUNT(fact_id) fact_total 
	, SUM(aude_valoracion) fact_suma	
	FROM autoevaluacion_detalle ad
    JOIN estandar e USING(esta_id)
    JOIN factor f USING(fact_id)
    JOIN (
        SELECT d.* 
		, COUNT(esta_id) dime_total
		, SUM(aude_valoracion) dime_suma
        FROM autoevaluacion_detalle ad
        JOIN estandar e USING(esta_id)
        JOIN factor f USING(fact_id)
        JOIN dimension d USING(dime_id)
        WHERE auto_id = _auto_id
        GROUP BY dime_id
    ) dx USING(dime_id)
    WHERE auto_id = _auto_id
    GROUP BY fact_id
) fx USING(fact_id)
WHERE auto_id = _auto_id

