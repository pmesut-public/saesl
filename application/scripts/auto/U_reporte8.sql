select d.*, f.*, e.*, co.* #, ec, fc
, auto_id
, concat(dime_codigo, '. ', dime_titulo) dime
, concat(fact_codigo, '. ', fact_titulo) fact
#, concat(crit_codigo, '. ', coalesce(crit_titulo, crit_descripcion)) crit
, concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta
, concat(conc_codigo, '. ', coalesce(code_nombre, conc_nombre)) conc
, coalesce(aude_cumplimiento, 0) aude
, coalesce(code_cumple, 0) code

from autoevaluacion_detalle ad
join estandar e using(esta_id)

join concepto co using(esta_id)
left join concepto_detalle cd on(co.conc_id = cd.conc_id and cd.aude_id = ad.aude_id)
join criterio c using(crit_id)
join factor f using(fact_id)
join dimension d using(dime_id)

where auto_id = _auto_id
order by e.esta_id, cast(substr(conc_codigo, 3) as signed)
