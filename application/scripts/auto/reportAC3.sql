SELECT *
	 , dime_titulo dime
	 , fact_titulo fact
	 , esta_titulo esta
from autoevaluacion_actividad
RIGHT JOIN autoevaluacion_detalle ad USING(aude_id)
JOIN estandar e USING(esta_id)
JOIN factor f USING(fact_id)
JOIN dimension d USING(dime_id)
WHERE auto_id = _auto_id;