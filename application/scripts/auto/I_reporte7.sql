select d.*, f.*
, auto_id
, concat(dime_codigo, '. ', dime_titulo) dime
, concat(fact_codigo, '. ', fact_titulo) fact
#, concat(crit_codigo, '. ', coalesce(crit_titulo, crit_descripcion)) crit
#, concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta

#, sum(if(aude_cumplimiento = 1, 1, 0)) cump
#, count(esta_id) total
#, format(sum(if(aude_cumplimiento = 1, 1, 0)) / count(esta_id) * 100, 2) per

, sum(aude_aceptacion) cump
, sum(5) total
, format(sum(aude_aceptacion) / sum(5) * 100, 2) per

, group_concat(if(aude_cumplimiento = 1, null, esta_codigo) separator ', ') no_cumplidos

from autoevaluacion_detalle ad
join estandar e using(esta_id)
join criterio c using(crit_id)
join factor f using(fact_id)
join dimension d using(dime_id)

where auto_id = _auto_id

group by fact_id
