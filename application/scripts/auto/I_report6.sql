select *
, concat(dime_codigo, '. ', dime_titulo) dime
, concat(fact_codigo, '. ', fact_titulo) fact
, concat(crit_codigo, '. ', coalesce(crit_titulo, crit_descripcion)) crit
, concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta

, coalesce(code_nombre, conc_nombre) concepto

from autoevaluacion_detalle ad
#left join concepto_detalle cd using(aude_id)
join estandar e using(esta_id)
join concepto co using(esta_id)
left join concepto_detalle cd on(co.conc_id = cd.conc_id and cd.aude_id = ad.aude_id) #using(conc_id)
join criterio c using(crit_id)
join factor f using(fact_id)
join dimension d using(dime_id)

where auto_id = _auto_id
and code_cumple = 1
and aude_cumplimiento = 1

order by e.esta_id, cast(substr(conc_codigo, 3) as signed)
