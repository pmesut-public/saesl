select *
, concat(dime_codigo, '. ', dime_titulo) dime
, concat(fact_codigo, '. ', fact_titulo) fact
, concat(crit_codigo, '. ', coalesce(crit_titulo, crit_descripcion)) crit
, concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta

, if(aude_cumplimiento = 1, (3 - aude_calidad) / 2, '-') calidad
, if(aude_cumplimiento = 1, ((3 - aude_calidad) / 2) * aude_aceptacion, '-') aprobacion

from autoevaluacion_detalle ad
join estandar e using(esta_id)
join criterio c using(crit_id)
join factor f using(fact_id)
join dimension d using(dime_id)

where auto_id = _auto_id
