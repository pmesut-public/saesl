(SELECT *
, dime_titulo dime
, dime_total
, (select valo_titulo from valoracion where valo_id = FLOOR(dime_avance)) dime_valo 
, fact_titulo fact
, fact_total
, (select valo_titulo from valoracion where valo_id = FLOOR((fact_suma / fpond_total))) fact_valo 
, coalesce(esta_titulo, esta_descripcion) esta
, (select valo_titulo from valoracion where valo_id = FLOOR(((aude_valoracion * aude_ponderacion) / aude_ponderacion))) valo
, '' as valo_g
FROM autoevaluacion_detalle ad
JOIN estandar e USING(esta_id)
JOIN (
    SELECT f.*
    , dx.dime_codigo
    , dx.dime_titulo
    , dx.dime_total
    , (dx.dime_suma / dx.dpond_total) as dime_avance
    , COUNT(fact_id) fact_total 
    , SUM(aude_ponderacion) fpond_total 
    , SUM((aude_valoracion * aude_ponderacion)) fact_suma	
    FROM autoevaluacion_detalle ad
    JOIN estandar e USING(esta_id)
    JOIN factor f USING(fact_id)
    JOIN (
        SELECT d.*
        , COUNT(esta_id) dime_total
	, SUM(aude_ponderacion) dpond_total
	, SUM((aude_valoracion * aude_ponderacion)) dime_suma
	FROM autoevaluacion_detalle ad
        JOIN estandar e USING(esta_id)
        JOIN factor f USING(fact_id)
        JOIN dimension d USING(dime_id)
        WHERE auto_id = _auto_id
        GROUP BY dime_id
    ) dx USING(dime_id)
    WHERE auto_id = _auto_id
    GROUP BY fact_id
) fx USING(fact_id)
WHERE auto_id = _auto_id
GROUP BY esta_id)