select *
, concat(dime_codigo, '. ', dime_titulo) dime
, concat(fact_codigo, '. ', fact_titulo) fact	
, concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta

, count(esta_id) dime_total
from autoevaluacion_detalle ad
join estandar e using(esta_id)
join factor f using(fact_id)

join (

	select d.*
	, count(aude_id) dime_total

	from autoevaluacion_detalle ad
	join estandar e using(esta_id)
	join factor f using(fact_id)
	join dimension d using(dime_id)

	where auto_id = _auto_id

	group by dime_id
) foo using(dime_id)

where auto_id = _auto_id
group by fact_id
