(SELECT *
, dime_titulo dime
, dime_total
, fact_titulo fact
, fact_total
, coalesce(esta_titulo, esta_descripcion) esta
FROM autoevaluacion_detalle ad
JOIN estandar e USING(esta_id)
JOIN (
    SELECT f.*, ''
	, dx.dime_titulo
	, dx.dime_total
	, COUNT(fact_id) fact_total
	FROM autoevaluacion_detalle ad
    JOIN estandar e USING(esta_id)
    JOIN factor f USING(fact_id)
    JOIN (
        SELECT d.* 
		, COUNT(esta_id) dime_total
        FROM autoevaluacion_detalle ad
        JOIN estandar e USING(esta_id)
        JOIN factor f USING(fact_id)
        JOIN dimension d USING(dime_id)
        WHERE auto_id = _auto_id
        GROUP BY dime_id
    ) dx USING(dime_id)
    WHERE auto_id = _auto_id
    GROUP BY fact_id
) fx USING(fact_id)
WHERE auto_id = _auto_id)
