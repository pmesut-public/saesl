SELECT *
, esta
, row_fact
, dime
, dime_descripcion 
, fact
, fact_id
, esta
FROM autoevaluacion_medio am
LEFT JOIN autoevaluacion_documento ad USING(aume_id)
LEFT JOIN documento do USING(docu_id)
JOIN autoevaluacion_detalle ade ON ade.aude_id = am.aude_id AND ade.esta_id = am.esta_id
JOIN (
    SELECT e.*
    , dime_id
    , COUNT(aume_id) row_fact
    , dime_titulo dime
    , dime_descripcion
    , fact_titulo fact
    , coalesce(esta_titulo, esta_descripcion) esta
    FROM autoevaluacion_medio am
    JOIN autoevaluacion_detalle ade USING(esta_id)
    JOIN estandar e USING(esta_id)
    JOIN (
        SELECT f.*
        , dx.dime_titulo
        , dx.dime_descripcion
        FROM autoevaluacion_medio am
        JOIN autoevaluacion_detalle ade USING(esta_id)
        JOIN estandar e USING(esta_id)
        JOIN factor f USING(fact_id)
        JOIN (
            SELECT d.* 
            FROM autoevaluacion_medio am
            JOIN autoevaluacion_detalle ade USING(esta_id)
            JOIN estandar e USING(esta_id)
            JOIN factor f USING(fact_id)
            JOIN dimension d USING(dime_id)
            WHERE auto_id = _auto_id
        ) dx USING(dime_id)
        WHERE auto_id = _auto_id
        GROUP BY fact_id
    ) fx USING(fact_id)
    WHERE auto_id = _auto_id
    GROUP BY e.esta_id
) ex ON ex.esta_id = am.esta_id
WHERE auto_id = _auto_id