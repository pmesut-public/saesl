select *
, a.moda_id moda_id
, concat(dime_codigo, '. ', dime_titulo) dime
, concat(fact_codigo, '. ', fact_titulo) fact
, concat(crit_codigo, '. ', coalesce(crit_titulo, crit_descripcion)) crit
, concat(esta_codigo, '. ', coalesce(esta_titulo, esta_descripcion)) esta
, sum(if(((3 - coalesce(code_calidad, 3)) / 2) < 1, 1, 0)) cantidad_fuentes_desaprobadas
, if(aude_cumplimiento = 1 /*or esta_tipo = 'satisfaccion'*/, 'Opcionales', 'Obligatorios') tipo

from autoevaluacion_detalle ad
join concepto_detalle cd using(aude_id)
join estandar e using(esta_id)
join criterio c using(crit_id)
join factor f using(fact_id)
join dimension d using(dime_id)
join objeto_acreditacion o using(obac_id)
join autoevaluacion b using(auto_id)
join acreditado_evento ae using(acev_id)
join acreditado a using(acre_id)

where auto_id = _auto_id

group by aude_id
having cantidad_fuentes_desaprobadas > 0
order by dime_id, fact_id, esta_id

/*

**** if code_calidad = null, Q = 0

179│
180┤
191┐
192└
193┴
194┬
195├
196─
197┼
217┘
218┌

▒▒▒▒	SHOW
▓▓▓▓	OBLIGATORIOS
FA		for all
E		exists
Q(FV)	calidad de FV

               ┌─────────┬─────────┐
               │  AC=1   │  AC=0   │
               ├────┬────┼────┬────┤
               │ S  │ NS │ S  │ NS │
┌──────────────┼────┼────┼────┼────┤
│ FA Q(FV) = 1 │    │    │▓▓▓▓│▓xx▓│ * UPD: No interviene satisfacción
├──────────────┼────┼────┼────┼────┤
│ E Q(FV) < 1  │▒▒▒▒│▒▒▒▒│▓▓▓▓│▓▓▓▓│ * xx: Caso imposible
└──────────────┴────┴────┴────┴────┘

*/
