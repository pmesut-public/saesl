<?php foreach ($oAutoevaluacion->get_reportes() as $report => $params): ?>
	
	<?php if (ACL::instance()->allowed('frontend.reportes', $report)): ?>
	<a href="/reportes/view/<?= $report ?>/<?= URL::query(['id' => $oAutoevaluacion->auto_id]) ?>" class="btn btn-flat btn-md btn-default"
		data-toggle="tooltip" title="<?= $params['title'] ?>" data-placement="bottom">
		<i class="fa fa-file-text-o"></i> <span><?= $params['short'] ?></span>
	</a>
	<?php endif ?>
	
<?php endforeach ?>
