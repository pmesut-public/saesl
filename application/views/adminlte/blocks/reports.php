<?php foreach ($oAutoevaluacion->get_reportes() as $report => $params): ?>
    <?php if (ACL::instance()->allowed('frontend.reportes', $report)): ?>
        <a href="/reportes/view/<?= $report ?>/<?= URL::query(['id' => $oAutoevaluacion->auto_id]) ?>"
           class="btn btn-link" title="<?= $params['title'] ?>"><?= $params['short'] ?></a>
    <?php endif ?>
<?php endforeach ?>
