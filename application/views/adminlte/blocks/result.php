<div class="progress xs <?= ($response['estado'] == Model_Autoevaluacion::STATUS_ACTIVO) ? 
	'progress-striped active' : NULL ?> bar_progress" _data-toggle="tooltip" 
	title="<?php //= $response['no_procesados'] ?> por procesar" data-placement="bottom">

	<?php //debug($response) ?>
	<div class="progress-bar progress-bar-primary bar_cumplidos" role="progressbar" 
		style="width: <?php //= $response['per_cumplidos'] ?>%" 
		data-toggle="tooltip" title="<?php //= $response['cumplidos'] ?> cumplidos" _data-placement="_bottom">
	</div>
	<div class="progress-bar progress-bar-success bar_procesados" role="progressbar" 
		style="width: <?php //= $response['per_diferencia'] ?>%" 
		data-toggle="tooltip" title="<?php //= $response['procesados'] ?> procesados" _data-placement="bottom">
	</div>
</div>