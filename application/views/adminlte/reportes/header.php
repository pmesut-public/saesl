<?php foreach ($header as $row): ?>
<tr class="success">
	<?php foreach ($row as $col => $attributes): ?>
		
		<?php if (is_array($attributes)): ?>
		<th <?= HTML::attributes($attributes) ?> ><?= $col ?></th>
		<?php else: ?>
		<th><?= $attributes ?></th>
		<?php endif ?>
		
	<?php endforeach ?>
</tr>
<?php endforeach ?>