<h4><?= $title ?></h4>

<table class="table table-bordered table-hover table-eventos" border="1">
	<thead>
		<tr class="success">
			<th class="text-center">Región</th>
			<th class="text-center">Gestión</th>
			<th class="text-center">Comités</th>
		</tr>
	</thead>
	<?php //die(); ?>

	<tbody>

		<tr nobr="true">

		<?php //debug($result) ?>
		<?php // Por regiones ?>
		<?php foreach ($result as $val1 => $row1): ?>

			<th rowspan="3" class="<?= ($val1 == 'xTOTAL') ? 'success' : NULL ?>"><?= $val1 ?></th>

			<?php // Gestiones ?>
			<?php foreach ($row1 as $val2 => $row2): ?>

				<td class="<?= ($val1 == 'xTOTAL') ? 'success' : NULL ?>"><?= $val2 ?></td>
				
				<td class="<?= ($val1 == 'xTOTAL') ? 'success' : NULL ?>"><?= $row2['TOTAL_U']['count'] ?></td>
				
				</tr>
				
				<?php if ($val1 != 'xTOTAL' OR $val2 != 'Todas'): ?>
					<tr nobr="true">
				<?php endif ?>
				
			<?php endforeach ?>
			
		<?php endforeach ?>

	</tbody>
</table>

<style>
	.success {
		background-color: #ddd;
	}
</style>
