<h4><?= $title ?></h4>

<?php $summary = $result['xTOTAL']['Todas'] ?>
<?php //debug($summary) ?>

<table class="table table-bordered" border="1">
	<tbody>
		<tr>
			<th colspan="2">Universidades</th>
			<td class="text-right"><?= $summary['TOTAL_U']['count'] ?></td>
		</tr>
		<tr>
			<th rowspan="5">Institutos</th>

			<?php foreach (array_slice($summary, 0, 4) as $key => $val): ?>
				<td><?= $key ?></td>
				<td class="text-right"><?= $val['count'] ?></td>
			</tr>
			<tr>
			<?php endforeach ?>

			<td>TOTAL</td>
			<td class="text-right"><?= $summary['TOTAL_I']['count'] ?></td>
		</tr>
		<tr class="success">
			<th colspan="2">TOTAL</th>
			<td class="text-right"><?= $summary['TOTAL_X']['count'] ?></td>
		</tr>
	</tbody>
</table>

<h4>Regiones participantes: <?= $regiones ?></h4>

<style>
	.success {
		background-color: #ddd;
	}
</style>
