<h4><?= $title ?></h4>

<?php //debug($result) ?>

<table class="table table-bordered" border="1">
	<tbody>
		<?php foreach ($result as $row): ?>
		<tr>
			<th style="text-align: left;"><?= $row['name'] ?></th>
			<td class="text-right"><?= $row['y'] ?></td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>

<style>
	.success {
		background-color: #ddd;
	}
</style>
