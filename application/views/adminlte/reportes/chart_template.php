<?php //debug($buttons) ?>
<div class="pull-right">
	<!--button class="btn btn-flat btn-sm btn-default"><i class="fa fa-file-text-o"></i></button-->
	<div class="btn-group">
		<button class="btn btn-flat btn-sm btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
		<ul class="dropdown-menu pull-right highcharts_actions" role="menu">
			
			<?php foreach ($buttons as $button): ?>
			<li><?= $button ?></li>
			<?php endforeach ?>
			
			<!--li><a href="#" data-action="print">Imprimir</a></li>
			<li><a href="#" data-action="pdf">Exportar como PDF</a></li>
			<li><a href="#" data-action="png">Exportar como PNG</a></li>
			<li class="divider"></li>
			<li><a class="btn-excel" href="/admin/report/excel/<?= $name.URL::query($params) ?>">Exportar datos como Excel</a></li-->
		</ul>
	</div>
</div>

<div class="highcharts-wrapper">
	<div class="highcharts" data-json="<?= HTML::chars($json) ?>"></div>
</div>
