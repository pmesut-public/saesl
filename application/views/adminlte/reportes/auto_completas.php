<h4><?= $title ?></h4>

<?php //debug($result) ?>

<table class="table table-bordered" border="1">
	<tbody>
		<tr>
			<th colspan="2">Universidades</th>
			<td class="text-right"><?= $result['TOTAL_U']['count'] ?></td>
		</tr>
		<tr>
			<th rowspan="5">Institutos</th>

			<?php foreach (array_slice($result, 0, 4) as $key => $val): ?>
				<td><?= $key ?></td>
				<td class="text-right"><?= $val['count'] ?></td>
			</tr>
			<tr>
			<?php endforeach ?>

			<td>TOTAL</td>
			<td class="text-right"><?= $result['TOTAL_I']['count'] ?></td>
		</tr>
		<tr class="success">
			<th colspan="2">TOTAL</th>
			<td class="text-right"><?= $result['TOTAL_X']['count'] ?></td>
		</tr>
	</tbody>
</table>

<style>
	.success {
		background-color: #ddd;
	}
</style>
