<h4><?= $title ?></h4>

<table class="table table-bordered table-hover table-eventos" border="1">
	<thead>
		<?= $header ?>
	</thead>
	<tbody>
		<?php foreach ($result as $row): ?>
		<tr>
			<?php $styles = reset($header->header) ?>
			<?php //debug($styles) ?>
			
			<?php foreach ($row as $key => $val): ?>
			<td style="<?= Arr::path(current($styles), 'style') ?>">
				<?= $val ?>
			</td>
			<?php next($styles) ?>
			<?php endforeach ?>
			
		</tr>
		<?php endforeach ?>
	</tbody>
</table>

<style>
	.success {
		background-color: #ddd;
	}
</style>
