<h4><?= $title ?></h4>

<table class="table table-bordered table-hover table-eventos" border="1">
	<thead>
		<tr class="success">
			<th rowspan="2" class="text-center">Región</th>
			<th rowspan="2" class="text-center">Gestión</th>
			<th colspan="5" class="text-center">Comités</th>
		</tr>
		<tr class="success">
			<th class="text-center">ESFA</th>
			<th class="text-center">IESP</th>
			<th class="text-center">IEST</th>
			<th class="text-center">ISE</th>
			<th class="text-center">Total</th>
		</tr>
	</thead>
	<?php //die(); ?>

	<tbody>

		<tr nobr="true">

		<?php //debug($result) ?>
		<?php // Por regiones ?>
		<?php foreach ($result as $val1 => $row1): ?>

			<th rowspan="3" class="<?= ($val1 == 'xTOTAL') ? 'success' : NULL ?>"><?= $val1 ?></th>

			<?php // Gestiones ?>
			<?php foreach ($row1 as $val2 => $row2): ?>

				<td class="<?= ($val1 == 'xTOTAL') ? 'success' : NULL ?>"><?= $val2 ?></td>
				
				<?php // Tipos ?>
				<?php foreach (array_slice($row2, 0, 5) as $val3 => $row3): ?>
				
					<td class="text-center <?= ($val1 == 'xTOTAL' OR $val3 == 'TOTAL_I') ? 'success' : NULL ?>">
						<?= $row3['count'] ?></td>
				
				<?php endforeach ?>
				
				</tr>
				
				<?php if ($val1 != 'xTOTAL' OR $val2 != 'Todas' OR $val3 != 'TOTAL_I'): ?>
					<tr nobr="true">
				<?php endif ?>
				
			<?php endforeach ?>
			
		<?php endforeach ?>

	</tbody>
</table>

<style>
	.success {
		background-color: #ddd;
	}
</style>
