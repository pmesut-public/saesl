<h4><?= $title ?></h4>

<table class="table table-bordered table-hover table-eventos" border="1">
	<thead>
		<tr class="success">
			<th rowspan="2" colspan="2" class="text-center col-xs-6">Regiones/Carreras/Instituciones</th>
			<th rowspan="2" class="text-center col-xs-2">Tipo</th>
			<th colspan="5" class="text-center col-xs-4">Periodo reportado</th>
		</tr>
		<tr class="success">
			<th class="text-center">Trim. Jul-Set 2014</th>
			<th class="text-center">Trim. Oct-Dic 2014</th>
			<th class="text-center">Trim. Ene-Mar 2015</th>
			<th class="text-center">Trim. Abr-Jun 2015</th>
			<th class="text-center">Total acumulado</th>
		</tr>
	</thead>
	<tbody>
		<tr>
		<?php // Por regiones ?>
		<?php foreach ($result as $val1 => $row1): ?>
			<td rowspan="51"><?= $val1 ?></td>
			<?php // Tipos de acreditación ?>
			<?php foreach ($row1 as $val2 => $row2): ?>
				<?php $classes = array(
					'ST1U'        => 'success',
					'ST2U'        => 'success',
					'Subtotal SU' => 'success',
					'ST1T'        => 'success',
					'ST2T'        => 'success',
					'Subtotal ST' => 'success',
					'Total'       => 'bg-olive',
				) ?>
				<?php //debug(implode(', ', array_slice(array_keys($row1), 9, 2))) ?>
				<?php $descs = array(
					'ST1U'				=> 'Subtotal 1 (Universidades): Carreras priorizadas'.
											H_EOL.implode(', ', array_slice(array_keys($row1), 0, 4)),
					'Educación'			=> 'Carreras de Educación (Universidades)',
					'Medicina'			=> 'Carrera de Medicina (Universidades)',
					'C. de la Salud'	=> 'Carreras de Ciencias de la Salud (Universidades)',
					'Ingenierías'		=> 'Carreras de Ingeniería (Universidades)',
					'Otros'				=> 'Otras carreras (Universidades)',
					'ST2U'				=> 'Subtotal 2 (Universidades): Todas las carreras',
					'Institucional'		=>'Acreditación institucional (Universidades)',
					'Subtotal SU'		=> 'Subtotal (Universidades): Todas las carreras y acreditación institucional',
					'Form. Docente'		=> 'Carreras de Formación Docente (Institutos)',
					'C. de Salud (T)'	=> 'Carreras de Ciencias de la Salud (Institutos)',
					'ST1T'				=> 'Subtotal 1 (Institutos): Carreras priorizadas'.
											H_EOL.implode(', ', array_slice(array_keys($row1), 9, 2)),
					'Otros (T)'			=> 'Otras carreras (Institutos)',
					'ST2T'				=> 'Subtotal 2 (Institutos): Todas las carreras',
					'Institucional (T)'	=> 'Acreditación institucional (Institutos)',
					'Subtotal ST'		=> 'Subtotal (Institutos): Todas las carreras y acreditación institucional',
					//'Total'				=> '',
				) ?>
				<?php $class = Arr::get($classes, $val2) ?>
				<?php $desc = Arr::get($descs, $val2) ?>
				<td rowspan="3" class="<?= $class ?>">
					<?php if ($class): ?><strong><?php endif ?>
					<?= $val2 ?>
					<?php if ($class): ?></strong><?php endif ?>
					<?php if ($desc): ?><?= H_EOL.HTML::tag('small', $desc) ?><?php endif ?>
				</td>
				<?php // Tipos: Pública, Privada, Subtotal ?>
				<?php foreach ($row2 as $val3 => $row3): ?>
					<td><?= $val3 ?></td>
					<?php // Trimestres + Total acumulado ?>
					<?php foreach ($row3 as $val4 => $row4): ?>
						<td><?= $row4['count'] ?></td>
					<?php endforeach ?>
					</tr>
					<?php if ($val1 != 'xTOTAL' OR $val2 != 'Total' OR $val3 != 'Subtotal' OR $val4 != 'tt'): ?>
						<tr>
					<?php endif ?>
				<?php endforeach ?>
			<?php endforeach ?>
		<?php endforeach ?>
	</tbody>
</table>

<style>
	.success {
		background-color: #ddd;
	}
</style>
