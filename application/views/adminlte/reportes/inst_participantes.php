<h4><?= $title ?></h4>

<table class="table" <?php if ( ! $theme->screen): ?> border="1" <?php endif ?> >
	<tbody>
		<tr>
			<th>Tipo institución</th>
			<th>Nombre</th>
			<th>Carreras participantes</th>
			<th>Acredita institución</th>
		</tr>
		<?php foreach ($result as $row): ?>
		<tr nobr="true">
			<td><?= $row['tiin_nombre'] ?></td>
			<td><?= $row['inst_nombre'] ?></td>
			<td><?= $row['sum_carr'] ?></td>
			<td><?= $row['sum_inst'] ?></td>
		</tr>
		<?php endforeach ?>

	</tbody>
</table>

<style>
	.success {
		background-color: #ddd;
	}
</style>
