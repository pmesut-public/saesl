<h4><?= $title ?></h4>

<?php //debug2($result) ?>

<?php $q = array(
	't1' => '[0, 20%>',
	't2' => '[20%, 40%>',
	't3' => '[40%, 100%]',
	'tt' => 'TOTAL',
); ?>

<?php $l = array(
	'TOTAL_I' => 'Total Institutos',
	'TOTAL_U' => 'Total Universidades',
	'TOTAL_X' => 'TOTAL',
); ?>

<table class="table table-bordered table-hover table-eventos" border="1">
	<thead>
		<tr class="success">
			<th class="text-center">Institución</th>
			<?php foreach ($q as $key => $val): ?>
			<th class="text-center"><?= $val ?></th>
			<?php endforeach ?>
		</tr>
	</thead>
	<?php //die(); ?>

	<tbody>

		<tr>

		<?php // Por regiones ?>
		<?php foreach ($result as $val1 => $row1): ?>

			<th class="<?= ($val1 == 'TOTAL_X') ? 'success' : NULL ?>"><?= Arr::get($l, $val1, $val1) ?></th>

			<?php // Gestiones ?>
			<?php foreach ($row1 as $val2 => $row2): ?>

				<td class="text-center <?= ($val1 == 'TOTAL_X' OR $val2 == 'tt') ? 'success' : NULL ?>">
					<?= $row2['count'] ?></td>
				
			<?php endforeach ?>
			
			</tr>
			
			<?php if ($val1 != 'TOTAL_X' OR $val2 != 'tt'): ?>
				<tr>
			<?php endif ?>
			
		<?php endforeach ?>

	</tbody>
</table>

<style>
	.success {
		background-color: #ddd;
	}
</style>
