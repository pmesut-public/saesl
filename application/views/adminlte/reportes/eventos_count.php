<h4><?= $title ?></h4>

<table class="table table-hover">
	<thead>
		<tr>
			<th>Evento</th>
			<th>Fecha inicio</th>
			<th>Fecha fin</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php //debug($result) ?>
		<?php foreach ($result as $evento): ?>
			<tr>
				<td><?= $evento['even_nombre'] ?></td>
				<td><?= dateformat(date('d M Y', strtotime($evento['even_fecha_inicio']))) ?></td>
				<td><?= dateformat(date('d M Y', strtotime($evento['even_fecha_fin']))) ?></td>
				<!--td><?= Model_Evento::$tipos[$evento['even_tipo']] ?></td-->
				<td><?= $evento['count'] ?> inscritos (<?= $evento['count_cer'] ?> autoevaluaciones)</td>
				<td><a class="btn btn-flat btn-sm btn-success" href="/admin/reportes/eventos/ver/<?= $evento['id'] ?>">Ver</a></td>
			</tr>
		<?php endforeach ?>

	</tbody>
</table>
