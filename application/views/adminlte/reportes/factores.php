<?php if ( ! $theme->screen): ?>
<h4><?= $title ?></h4>
<?php endif ?>

<h5><?= $subtitle ?></h5>

<table class="table table-bordered table-hover table-eventos" border="1">
	<thead>
		<tr class="success">
			<th rowspan="2" class="text-center">Dimensión</th>
			<th rowspan="2" class="text-center">Factor</th>
			<th rowspan="2" class="text-center">Gestión</th>
			<th colspan="26" class="text-center">Regiones</th>
		</tr>
		<tr class="success">
			<?php foreach ($regiones as $region): ?>
			<th class="text-center rotate-45">
				<div><?= $region ?></div>
			</th>
			<?php endforeach ?>
		</tr>
	</thead>
	<?php //die(); ?>

	<tbody>

		<tr>

		<?php // Por dimensiones ?>
		<?php foreach ($result as $val1 => $row1): ?>

			<td rowspan="<?= /*$factores[$val1]*/ count($row1) * 3 ?>"><?= $val1 ?></td>
			<?php //die(); ?>

			<?php // Factores ?>
			<?php foreach ($row1 as $val2 => $row2): ?>

				<?php $classes = array(
					'Promedio Dimensión' => 'success',
				) ?>
				<?php $class = Arr::get($classes, $val2) ?>

				<td rowspan="3" class="<?= $class ?>">
					<?php if ($class): ?><strong><?php endif ?>
					<?= $val2 ?>
					<?php if ($class): ?></strong><?php endif ?>
				</td>

				<?php // Tipos: Pública, Privada, Subtotal ?>
				<?php foreach ($row2 as $val3 => $row3): ?>
					<td class="<?= $class ?>"><?= $val3 ?></td>

					<?php // Regiones ?>
					<?php foreach ($row3 as $val4 => $row4): ?>

						<?php $class2 = ($val4 == 'xTOTAL') ? 'bg-olive' : NULL ?>
						<td class="<?= $class ?> <?= $class2 ?>"><?= $row4['prom__'] ?></td>

					<?php endforeach ?>

					</tr>

					<?php if ($val1 != '4. RESULTADOS E IMPACTO' OR 
						$val2 != 'Promedio Dimensión' OR 
						$val3 != 'Todas' OR 
						$val4 != 'xTOTAL'): ?>
						<tr>
					<?php endif ?>

				<?php endforeach ?>

			<?php endforeach ?>

		<?php endforeach ?>

	</tbody>
</table>

<style>
	.success {
		background-color: #ddd;
	}
</style>
