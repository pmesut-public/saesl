<?php //debug($header) ?>

<?php $has_actions = ($action == 'view' AND ACL::instance()->allowed('acreditado', 'cancel_evento')) ?>

<?php if ($has_actions):
	$header->header[0][''] = ['rowspan' => 2, 'style' => 'width: 3%;'] ?>
<?php endif ?>

<h4><?= $title ?></h4>
<h5>Total: <?= count($result) ?></h5>

<table class="table table-hover table-bordered" border="1">
	<thead>
		<?= $header ?>
	</thead>
	<tbody>
		<?php //debug($result) ?>
		<?php foreach ($result as $auto): ?>
			<tr>
				<td><?= $auto['obac_nombre'] ?></td>
				<td>
					<?php if ($x = ACL::instance()->allowed('acreditado', 'view')): ?>
						<a href="/admin/acreditado/view/<?= $auto['acre_id'] ?>" 
							class="" data-title="Ver Detalle">
					<?php endif ?>

					<?= $auto['inst_name'] ?>
					<?= ($y = $auto['carr_nombre']) ? H_EOL.HTML::tag('small', $y) : $y ?>
					
					<?php if ($x): ?></a><?php endif ?>
				</td>
				
				<?php if ($auto['total_est'] == 0): ?>
					
					<td colspan="7"><b>No registra autoevaluación</b></td>
					
				<?php elseif ($auto['auto_estado'] == Model_Autoevaluacion::STATUS_ACTIVO): ?>
					
					<td colspan="7">En proceso de autoevaluación</td>
					
				<?php else: ?>
					
					<td><?= $auto['total_est'] ?></td>
					<td><?= $auto['sum_processed'] ?></td>
					<td><?= $auto['sum_est'] ?></td>
					<td><?= $auto['perc'] ?></td>
					<td><?= $auto['sum_minedu'] ?></td>
					<td><?= $auto['sineace'] ?></td>
					<td><?= $auto['procalidad'] ?></td>
					
				<?php endif ?>
					
					<?php if ($has_actions): ?>
						<td><a href="/admin/acreditado/cancel_evento/<?= $auto['acev_id'] ?>"
							class="btn btn-flat btn-sm btn-danger" data-confirm><i class="fa fa-times"></i></a></td>
					<?php endif ?>
					
			</tr>
		<?php endforeach ?>

	</tbody>
</table>
