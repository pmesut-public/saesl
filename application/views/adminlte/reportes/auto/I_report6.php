<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php // dimension + factor + criterio ?>
<?php foreach ($result as $val1 => $row1): ?>
	<table border="1" class="table table-bordered" nobr="true">
		<tbody>

			<tr class="text-center success _small">
				<th colspan="1" style="width: 20%;">Dimensión <?= $row1['dime_codigo'] ?></th>
				<th colspan="5" style="width: 80%;"><?= $row1['dime_titulo'] ?></th>
			</tr>
			<tr class="success">
				<th colspan="1" style="width: 20%;">&raquo; Factor <?= $row1['fact_codigo'] ?></th>
				<th colspan="5" style="width: 80%;"><?= $row1['fact_titulo'] ?></th>
			</tr>
			<tr class="success">
				<th colspan="1" style="width: 20%;">&raquo; &raquo; Criterio <?= $row1['crit_codigo'] ?></th>
				<td colspan="5" style="width: 80%;">
					<strong><?= $row1['crit_titulo'] ?></strong><br>
					<?= $row1['crit_descripcion'] ?>
				</td>
			</tr>

		</tbody>
	</table>

	<table border="1" class="table table-bordered">
		<thead>
			<tr class="success small" nobr="true">
				<th colspan="2" class="text-center" style="width: 50%;">ESTÁNDAR</th>
				<!--th colspan="1" class="text-center" style="width: 10%;">TIPO</th-->
				<th colspan="1" class="text-center" style="width: 10%;">CUMPLIMIENTO</th>
				<th colspan="2" class="text-center" style="width: 40%;">FUENTES DE VERIFICACION</th>
			</tr>
		</thead>
		<tbody>


		<?php // estandar ?>
		<?php foreach ($row1['data'] as $val2 => $row2): ?>
		<tr nobr="true">

			<?php 
				$rows = count($row2['data']); 
				end($row2['data']); 
				$last = key($row2['data']); 
			?>

			<td id="esta-<?= $val2 ?>" colspan="2" rowspan="<?= $rows ?>" style="width: 50%;">
				<?= $row2['esta'] ?></td>
			<!--td colspan="1" rowspan="<?= $rows ?>" class="text-center" style="width: 10%;">
				<?php //= Model_Estandar::$tipos[$row2['esta_tipo']] ?></td-->
			<td colspan="1" rowspan="<?= $rows ?>" class="text-center" style="width: 10%;">
				<?= $row2['aude_aceptacion'] ?> (Mínimo: <?= $row2['esta_nivel_aceptacion'] ?>)
			</td>

			<?php // concepto ?>
			<?php foreach ($row2['data'] as $val3 => $row3): ?>

				<td class="<?= $row3['data']['conc_obligatorio'] ? 'strong' : NULL ?>" style="width: 25%;">
					<?= $row3['data']['concepto'] ?>
				</td>
				<td class="text-center" style="width: 15%;">
					<?= ($row3['data']['code_cumple'] == 1) ? 
					Model_ConceptoDetalle::$options[$row3['data']['code_calidad']] : '--' ?>
				</td>
			</tr>

			<?php if ($val3 != $last): ?> <tr nobr="true"> <?php endif ?>

			<?php endforeach ?>

		<?php endforeach ?>

		</tbody>
	</table>

<?php endforeach ?>

<table border="1" class="table table-bordered" nobr="true">
	<tbody>
		<tr class="info">
			<th colspan="1" class="success" style="width: 20%;">
				<big>TOTAL:</big>
			</th>
			<th colspan="5" class="success" style="width: 80%;">
				<big>CUMPLEN <?= $response['cumplidos'] ?> DE LOS <?= $response['total'] ?> ESTÁNDARES SELECCIONADOS
					(<?= $response['per_cumplidos'] ?>%)</big>
			</th>
		</tr>
	</tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>
