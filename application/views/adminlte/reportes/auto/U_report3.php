<h3 class="text-center">
	<?= $title ?><br>
	
	<small>
		Autoevaluación N° <?= $oAutoevaluacion->auto_numero ?> iniciada en 
		<?= $oAutoevaluacion->auto_fecha_inicio ?>
	</small><br>
	
	<?php if ($oAutoevaluacion->auto_estado == Model_Autoevaluacion::STATUS_ACTIVO): ?>
		<small>Fecha de última modificación: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
		<small>(Autoevaluación pendiente de cierre)</small><br>
	<?php else: ?>
		<small>Fecha de cierre: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
	<?php endif ?>
	
	<small>- <?= $oAutoevaluacion->oAcreditadoEvento->oEvento->even_nombre ?> -</small>
</h3><hr>

<h4 class="text-center">
	<?= $subtitle ?><br>
	<small><?= $objeto ?></small>
</h4>

<?php //debug($result) ?>

<table border="1" class="table table-bordered">
	<thead>
		<tr class="success" nobr="true">
			<th style="width: 15%;">DIMENSIÓN</th>
			<th style="width: 15%;">FACTOR</th>
			<th style="width: 55%;">ESTÁNDAR</th>
			<th class="text-center" style="width: 5%;">CUMPLE</th>
			<th class="text-center" style="width: 5%;">CALIDAD</th>
			<th class="text-center" style="width: 5%;">APROBACIÓN</th>
		</tr>
	</thead>
	<tbody>
		
		<?php // dimension ?>
		<?php foreach ($result as $val1 => $row1): ?>
		
		<tr nobr="true">
			<th rowspan="<?= $row1['estandares'] ?>" style="width: 15%;">
				<?= $row1['dime'] ?>
			</th>
			
			<?php
				end($row1['data']);
				$last1 = key($row1['data']);
			?>
			
			<?php // factor ?>
			<?php foreach ($row1['data'] as $val2 => $row2): ?>

				<?php
					$rows = count($row2['data']);
					end($row2['data']);
					$last2 = key($row2['data']);
				?>
				
				<th rowspan="<?= $rows ?>" style="width: 15%;">
					<?= $row2['fact'] ?>
				</th>

				<?php // estandar ?>
				<?php foreach ($row2['data'] as $val3 => $row3): ?>

					<td style="width: 55%;" id="esta-<?= $val3 ?>">
						<?= $row3['data']['esta'] ?>
					</td>
					<td class="text-center" style="width: 5%;">
						<?= $row3['data']['aude_cumplimiento'] ? 'SÍ' : 'NO' ?>
					</td>
					<td class="text-center" style="width: 5%;">
						<?= numberformat($row3['data']['calidad'], 1) ?>
					</td>
					<td class="text-center" style="width: 5%;">
						<?= ($row3['data']['calidad'] > 0.5) ? 'SI' : 'NO' ?>
					</td>
					
				</tr>
				
				<?php if ($val2 != $last1 OR $val3 != $last2): ?> <tr nobr="true"> <?php endif ?>
				
				<?php endforeach ?>
				
			<?php endforeach ?>
			
		<?php endforeach ?>

		<tr class="info success" nobr="true">
			<th class="_text-right" style="width: 15%;"><big>TOTAL</big></th>
			<th colspan="2" class="text-center" style="width: 70%;">
				<big>
				Estándares evaluados: <?= $response['total'] ?><br>
				Estándares cumplidos: <?= $response['cumplidos'] ?>
					(<?= $response['per_cumplidos'] ?>%)<br>
				Estándares aprobados: <?= $response['aprobados'] ?>
					(<?= $response['per_aprobados'] ?>%)
				</big>
				<?php if ($oAcreditado->moda_id == 1): ?>
					<br>
					<big>CUMPLIMIENTO DE ESTÁNDARES OBLIGATORIOS
						(<?= $response['gproc_porcentaje'] ?>%)</big>
				<?php endif ?>
			</th>
			<th class="text-center" style="width: 5%;">
				<big><?= $response['cumplidos'] ?></big>
			</th>
			<th class="text-center" style="width: 5%;">

			</th>
			<th class="text-center" style="width: 5%;">
				<big><?= $response['aprobados'] ?></big>
				
			</th>
		</tr>
	</tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>
