<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>


<?php //debug($result); die; ?>

<table border="1" class="table table-bordered">
	<thead>
		<tr class="success" nobr="true">
			<th style="width: 20%;">DIMENSIÓN</th>
			<th class="text-center" style="width: 10%;">% AVANCE</th>
			<th style="width: 20%;">FACTOR</th>
			<th class="text-center" style="width: 10%;">% AVANCE</th>
			<th style="width: 30%;">ESTANDAR</th>
			<th class="text-center" style="width: 10%;">% AVANCE</th>
		</tr>
	</thead>

	<tbody>
		<?php //debug($result); ?>
		<?php // dimension ?>
		<?php foreach ($result as $val1 => $row1): ?>
		
		<?php if ($row1['dime'] != '') { ?>
		
		<tr nobr="true">

			<?php $rows = $row1['dime_total']; ?>
			
			<th rowspan="<?= $rows ?>" style="width: 20%;vertical-align:middle;">
				<?= $row1['dime'] ?>
			</th>
			
			<?php //$valo_dime = number_format($row1['dime_avance'] * 10, 2); ?>
			
			<td rowspan="<?= $rows ?>" class="text-center" style="width: 10%;vertical-align:middle;">
					<?= $row1['dime_avance'] ?> %
			</td>
			
			<?php
				end($row1['data']);
				$last1 = key($row1['data']);
			?>
			
			<?php // factor ?>
			<?php foreach ($row1['data'] as $val2 => $row2): ?>

				<td rowspan="<?= $row2['fact_total'] ?>" style="width: 20%;vertical-align:middle;">
					<?= $row2['fact'] ?>
				</td>
				
				<?php //$valo_fact = number_format($row2['fact_avance'] * 10, 2); ?>
				
				<td rowspan="<?= $row2['fact_total'] ?>" class="text-center" style="width: 10%;vertical-align:middle;">
					<?= $row2['fact_avance']  ?> %
				</td>
				
				<?php
					end($row2['data']);
					$last2 = key($row2['data']);
				?>
				
				<?php // estandar ?>
				<?php foreach ($row2['data'] as $val3 => $row3): ?>		

					<?php //debug($row3['data']); ?>
						<td id="esta-<?= $val3 ?>" style="width: 30%;">
							<?= $row3['data']['esta'] ?>
						</td>
						
						<?php //$valo_esta = number_format($row3['data']['valo'] * 10, 2); ?>
						
						<td class="text-center" style="width: 10%;"> 
							<?= $row3['data']['valo'] ?> %
						</td>
					</tr>

					<?php if ($val2 != $last1 OR $val3 != $last2): ?> <tr nobr="true"> <?php endif ?>

				<?php endforeach ?>
				
			<?php endforeach ?>
		
		<?php } else { ?>
				<?php //debug($row1['data']); die; ?>
			<?php //$valo_esta = number_format($row1['valo_g'] * 10, 2); ?>
			<tr class="info success" nobr="true">
				<th class="text-center" colspan="5" style="width: 90%;"><big>TOTAL</big></th>
				<th class="text-center" style="width: 10%;">
					<big><?= $row1['valo_g'] ?> %</big>
				</th>
			</tr>
		<?php } ?>
			
		<?php endforeach ?>
		
	</tbody>
	
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>
