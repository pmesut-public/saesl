<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php //debug($result) ?>

<?php // tipos ?>
<?php foreach ($result as $val1 => $row1): ?>

	<table border="1" class="table table-bordered">
		<thead>

			<tr class="text-center success">
				<th colspan="1" class="text-center" style="width: 45%;">TIPO ESTÁNDAR</th>
				<th colspan="1" class="text-center" style="width: 15%;">RESULTADO</th>
				<th colspan="2" class="text-center" style="width: 40%;">FUENTES DE VERIFICACIÓN</th>
			</tr>

			<tr class="info">
				<th colspan="4" class="success"><big><?= ucfirst($val1) ?></big></th>
			</tr>
		</thead>
		<tbody>

			<?php // estandar ?>
			<?php if ( ! isset($row1['data'][''])): ?>
			<?php foreach ($row1['data'] as $val2 => $row2): ?>

				<?php
					$rows = count($row2['data']);
					end($row2['data']);
					$last = key($row2['data']);
				?>

				<tr id="esta-<?= $val2 ?>" nobr="true">
					<td rowspan="<?= $rows ?>" style="width: 45%;">
						<?= $row2['esta'] ?></td>
					<td rowspan="<?= $rows ?>" class="text-center" style="width: 15%;">
						<?= ($row2['aude_cumplimiento'] == 1) ? 'CUMPLE' : 'NO CUMPLE' ?></td>
					
					<?php // concepto ?>
					<?php foreach ($row2['data'] as $val3 => $row3): ?>
						<td style="width: 35%;" class="<?= ($row3['data']['conc_obligatorio'] == 1) ? 'strong' : NULL ?>">
							<?= $row3['data']['concepto'] ?>
						</td>
						<td style="width: 5%;" class="text-center">
							<?= ($row3['data']['code_cumple'] == 1) ? 'SI' : 'NO' ?>
						</td>

					</tr>

					<?php if ($val3 != $last): ?> <tr nobr="true"> <?php endif ?>

					<?php endforeach ?>

			<?php endforeach ?>
			<?php endif ?>

			<tr nobr="true">
				<td colspan="4" class="text-primary">
					<strong>
						<?php if (isset($row1['data'][''])): ?>
							Ningún estándar evaluado

						<?php else: ?>
							Total por tipo de estandar: Cumplen 
							<?= $row1['cump'] ?>
							de los 
							<?= $row1['total'] ?>
							estándares seleccionados
							(<?= number_format($row1['cump'] / $row1['total'] * 100, 2) ?>%)
						<?php endif ?>
					</strong>
				</td>
			</tr>
		</tbody>
	</table>

<?php endforeach ?>

<table border="1" class="table table-bordered" nobr="true">
	<tbody>
		<tr class="info">
			<th colspan="1" class="success" style="width: 20%;">
				<big>TOTAL:</big>
			</th>
			<th colspan="3" class="success" style="width: 80%;">
				<big>CUMPLEN <?= $response['cumplidos'] ?> DE LOS <?= $response['total'] ?> ESTÁNDARES SELECCIONADOS
					(<?= $response['per_cumplidos'] ?>%)</big>
				<?php if ($oAcreditado->moda_id == 1): ?>
					<br>
					<big>CUMPLIMIENTO DE ESTÁNDARES OBLIGATORIOS
						(<?= $response['gproc_porcentaje'] ?>%)</big>
				<?php endif ?>
			</th>
		</tr>
	</tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>
