<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php //debug($result) ?>

<table border="1" class="table table-bordered">
	<thead>
		<tr class="success" nobr="true">
			<th rowspan="2" style="width: 15%;">DIMENSIÓN</th>
			<th rowspan="2" style="width: 15%;">FACTOR</th>
			<th rowspan="2" style="width: 54%;">ESTÁNDAR</th>
			<th colspan="2" class="text-center" style="width: 16%;">CORRESPONDE ATENCIÓN</th>
		</tr>
		<tr class="success" nobr="true">
			<th class="text-center" style="width: 8%;">SUNEDU</th>
			<th class="text-center" style="width: 8%;"><?= $oAutoevaluacion->attender() ?></th>
		</tr>
	</thead>
	<tbody>
		
		<?php // dimension ?>
		<?php foreach ($result as $val1 => $row1): ?>
		
		<tr nobr="true">
			<th rowspan="<?= $row1['estandares'] ?>" style="width: 15%;">
				<?= $row1['dime'] ?>
			</th>
			
			<?php
				end($row1['data']);
				$last1 = key($row1['data']);
			?>
			
			<?php // factor ?>
			<?php foreach ($row1['data'] as $val2 => $row2): ?>

				<?php
					$rows = count($row2['data']);
					end($row2['data']);
					$last2 = key($row2['data']);
				?>
				
				<th rowspan="<?= $rows ?>" style="width: 15%;">
					<?= $row2['fact'] ?>
				</th>

				<?php // estandar ?>
				<?php foreach ($row2['data'] as $val3 => $row3): ?>

					<td style="width: 54%;" id="esta-<?= $val3 ?>">
						<?= $row3['data']['esta'] ?>
					</td>
					<td class="text-center" style="width: 8%;">
						<?= ($row3['data']['esta_clasificacion'] == 'S') ? 'x' : '' ?>
					</td>
					<td class="text-center" style="width: 8%;">
						<?= ($row3['data']['esta_clasificacion'] == 'S') ? '' : 'x' ?>
					</td>
					
				</tr>
				
				<?php if ($val2 != $last1 OR $val3 != $last2): ?> <tr nobr="true"> <?php endif ?>
				
				<?php endforeach ?>
				
			<?php endforeach ?>
			
		<?php endforeach ?>

		<tr class="info success" nobr="true">
			<th colspan="2" style="width: 30%;"><big>TOTAL</big></th>
			<th colspan="3" class="text-center" style="width: 70%;">
				<big>
				Estándares evaluados: <?= $response['total'] ?><br>
				Estándares aprobados: <?= $response['cumplidos'] ?><br>
				Porcentaje de cumplimiento: <?= $response['per_cumplidos'] ?>%
				</big>
			</th>
		</tr>
		<tr class="info success" nobr="true">
			<th colspan="2" style="width: 30%;"><big>TOTAL PARA EJECUTAR</big></th>
			<th colspan="3" class="text-center" style="width: 70%;">
				<big>
				SUNEDU: <?= $response['basicos_no_cumplidos'] ?> estándares<br>
				<?= $oAutoevaluacion->attender() ?>: <?= $response['no_cumplidos'] - $response['basicos_no_cumplidos'] ?> estándares<br>
				Estándares no superados: <?= $response['no_cumplidos'] ?> estándares
				</big>
			</th>
		</tr>
	</tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>
