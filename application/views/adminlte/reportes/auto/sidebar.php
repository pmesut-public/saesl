<aside class="left-side sidebar-offcanvas">                
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		
		<ul class="sidebar-menu">
			<!--li>
				<a href="/admin">
					<i class="fa fa-dashboard"></i> <span>Home</span>
				</a>
			</li-->
			<li>
				<a href="/autoevaluaciones">
					<i class="fa fa-check-square-o"></i> <span>Autoevaluaciones</span>
				</a>
			</li>
			<?php // debug($aDimension) ?>
			<?php foreach ($aDimension as $dime => $aFactor): ?>
			
			<?php if ($dime != '') { ?>
			
			<li class="treeview active">
				<a href="#">
					<i class="fa fa-suitcase"></i>
					<span data-toggle="_tooltip" title="Condición <?= $dime ?>"><?= $dime ?></span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					
					<?php // debug($aFactor) ?>
					<?php foreach ($aFactor as $fact => $aEstandar): ?>
					<li class="treeview active">
						<a href="#">
							<i class="fa fa-angle-double-right"></i>
							<span data-toggle="_tooltip" title="Componente <?= $fact ?>"><?= $fact ?></span>
							<!--<i> class="fa fa-angle-left pull-right"></i>-->
						</a>
						<ul class="treeview-menu">
							<?php foreach ($aEstandar as $esta => $estandar): ?>
							<li data-id="<?= $estandar['esta_id'] ?>">
								<a href="#esta-<?= $estandar['esta_id'] ?>" class="li_estandar">
									<i class="fa fa-angle-double-right"></i> 
									<span data-toggle="_tooltip" title="Estándar <?= $esta ?>">Indicador <?= $estandar['esta_codigo'] ?></span>
								</a>
							</li>
							<?php endforeach ?>
							
						</ul>
					</li>
					<?php endforeach ?>
					
				</ul>
			</li>
			
			<?php } ?>
			
			<?php endforeach ?>
			
			<li>
				<a href="/autoevaluaciones">
					<i class="fa fa-check-square-o"></i> <span>Autoevaluaciones</span>
				</a>
			</li>
			
		</ul>
		
	</section>
	<!-- /.sidebar -->
</aside>
