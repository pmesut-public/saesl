<h3 class="text-center">
	<?= $title ?><br>
	
	<?php if ($oAutoevaluacion->auto_estado == Model_Autoevaluacion::STATUS_ACTIVO): ?>
		<small>Fecha de última modificación: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
	<?php else: ?>
		<small>Fecha de cierre: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
	<?php endif ?>
	
	<small>- <?= $oAutoevaluacion->oAcreditadoEvento->oEvento->even_nombre ?> -</small>
</h3><hr>

<h4 class="text-center">
	<?= $subtitle ?><br>
	<small><?= $objeto ?></small>
</h4>

<?php //$edit = TRUE ?>
<?php //echo '<pre>'.Debug::dump(get_defined_vars(), 128, 2); die() ?>

<table border="1" class="table table-bordered">
	<thead>
		<tr class="success" nobr="true">
			<th rowspan="2" style="width: 5%;">DIMENSIÓN</th>
			<th rowspan="2" style="width: 5%;">FACTOR</th>
			<th colspan="3" class="text-center" style="width: 30%;">ESTÁNDARES</th>
			<th rowspan="2" _class="text-center" style="width: 60%;">
				
				<h5 class="text-center">VALORACIÓN CUALITATIVA DE LOGROS A NIVEL DE CADA FACTOR: </h5>
				<p>Valore la incidencia del Plan de Mejora ejecutado en el cambio del resultado 
					de la autoevaluación para cada factor, considerando: Actores Internos 
					(docentes, administrativos, alumnos, otros), Entorno y Dinámica Institucional. 
					Contará, máximo, con 500 caracteres para el desarrollo.</p>
			</th>
		</tr>
		<tr class="success" nobr="true">
			<th class="text-center" style="width: 10%;">PUNTAJE AUTOEVALUACIÓN INICIAL</th>
			<th class="text-center" style="width: 10%;">PUNTAJE AUTOEVALUACIÓN FINAL</th>
			<th class="text-center" style="width: 10%;">VALORACIÓN DE LOGRO</th>
		</tr>
	</thead>
	<tbody>
		
		<?php // dimension ?>
		<?php foreach ($result_conv as $dimension => $factores): ?>
		
		<tr nobr="true">
			<th rowspan="<?= count($factores) ?>" style="width: 5%;">
				<?= $dimension ?>
			</th>
			
			<?php
				end($factores);
				$last = key($factores);
			?>
			
			<?php // factor ?>
			<?php foreach ($factores as $factor => $row): ?>
				
				<?php 
					$row_seg = $result[$dimension][$factor];
					$status = Model_AutoevaluacionInforme::get_status($row, $row_seg);
				?>
				
				<th style="width: 5%;">
					<?= $factor ?>
				</th>
				
				<td style="width: 10%;">
					<?= "{$row['per']} % ({$row['cump']} de {$row['total']})" ?>
				</td>
				<td style="width: 10%;">
					<?= "{$row_seg['per']} % ({$row_seg['cump']} de {$row_seg['total']})" ?>
				</td>
				
				<td style="width: 10%; _background-color: <?= Model_AutoevaluacionInforme::$colores[$status] ?>"
					class="<?= Model_AutoevaluacionInforme::$colores[$status] ?>">
					<div>(<?= str_repeat('I', $status) ?>)</div>
					Estándares:
					<?= $row_seg['no_cumplidos'] ?: '-' ?>
				</td>
				
				<td style="width: 60%;" class="informe_factores">
					<?php if ($edit): ?>
					<textarea name="<?= "informe_factores[$factor][auif_descripcion]" ?>" 
						placeholder="Ingrese la valoración del factor" 
						class="form-control" rows="3" maxlength="500" required 
						><?= Arr::path($informe_factores, "$factor|auif_descripcion", NULL, '|') ?></textarea>
					<input type="hidden" name="<?= "informe_factores[$factor][auif_id]" ?>" 
						value="<?= Arr::path($informe_factores, "$factor|auif_id", NULL, '|') ?>" />
					<input type="hidden" name="<?= "informe_factores[$factor][fact_id]" ?>" 
						value="<?= $row['fact_id'] ?>" />
					<input type="hidden" name="<?= "informe_factores[$factor][fact_codigo]" ?>" 
						value="<?= $factor ?>" />
					<?php else: ?>
					<p class="form-control-static"><?= Arr::path($informe_factores, "$factor|auif_descripcion", NULL, '|') ?></p>
					<?php endif ?>
				</td>
				
			</tr>
			
			<?php if ($factor != $last): ?> <tr nobr="true"> <?php endif ?>
			
			<?php endforeach ?>
			
		<?php endforeach ?>
	</tbody>
	<tfoot class="informe_descripcion">
		<tr nobr="true">
			<th colspan="6">
				INFORMACIÓN A NIVEL GENERAL
			</th>
		</tr>
		<tr>
			<td colspan="6">
				<strong>- Cuál es su opinión en relación a la implementación del Plan de Mejora? Por qué?</strong>
				
				<?php if ($edit): ?>
				<textarea name="informe[descripcion][a]" class="form-control" rows="5" required 
					><?= Arr::path($informe, "descripcion.a") ?></textarea>
				<?php else: ?>
				<p class="form-control-static"><?= Arr::path($informe, "descripcion.a") ?></p>
				<?php endif ?>
				
			</td>
		</tr>
		<tr>
			<td colspan="6">
				<strong>- Qué medidas se han tomado o se van a tomar para asegurar la sostenibilidad 
					de las mejoras con un criterio de mejora continua? Enumere y desarrolle su respuesta.</strong>
				
				<?php if ($edit): ?>
				<textarea name="informe[descripcion][b]" class="form-control" rows="5" required 
					><?= Arr::path($informe, "descripcion.b") ?></textarea>
				<?php else: ?>
				<p class="form-control-static"><?= Arr::path($informe, "descripcion.b") ?></p>
				<?php endif ?>
				
			</td>
		</tr>
		
	</tfoot>
</table><br>

<div>
	(I) Logro por debajo del 25% de lo esperado, 
	(II) Logro entre el 25% y el 75% de lo esperado, 
	(III) Logro entre el 75% y el 100% de lo esperado
</div><br>

<div class="box">
	
	<div class="box-body informe_firma">
		
		<table class="table table-condensed" border="1" nobr="true">
			<thead>
				<tr>
					<th colspan="2"><h4>Datos de la persona responsable</h4></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th style="width: 30%;" class="text-right">
						<label class="control-label">Apellidos y nombres *</label>
					</th>
					<td style="width: 70%;">
						<?php if ($edit): ?>
						<input type="text" name="informe[descripcion][nombre]" 
							value="<?= Arr::path($informe, "descripcion.nombre") ?>" 
							class="form-control" required>
						<?php else: ?>
						<p class="form-control-static"><?= Arr::path($informe, "descripcion.nombre") ?></p>
						<?php endif ?>
					</td>
				</tr>
				<tr>
					<th style="width: 30%;" class="text-right">
						<label class="control-label">Cargo *</label>
					</th>
					<td style="width: 70%;">
						<?php if ($edit): ?>
						<input type="text" name="informe[descripcion][cargo]" 
							value="<?= Arr::path($informe, "descripcion.cargo") ?>" 
							class="form-control" required>
						<?php else: ?>
						<p class="form-control-static"><?= Arr::path($informe, "descripcion.cargo") ?></p>
						<?php endif ?>
					</td>
				</tr>
				<tr>
					<th style="width: 30%;" class="text-right">
						<label class="control-label">Fecha y firma</label>
					</th>
					<td style="width: 70%;">
						<br><br><br>
						__________________________
						<?php if ($edit): ?>
						<input type="hidden" name="informe[descripcion][date]" value="<?= date('Y-m-d') ?>" 
							class="form-control">
						<input type="hidden" name="informe[descripcion][date_local]" value="<?= dateformat(date('d \d\e F \d\e\l Y')) ?>" 
							class="form-control">
						<p class="form-control-static text-right"><?= dateformat(date('d \d\e F \d\e\l Y')) ?></p>
						<?php else: ?>
						<p class="form-control-static text-right"><?= Arr::path($informe, 'descripcion.date_local') ?></p>
						<?php endif ?>
					</td>
				</tr>
			</tbody>
		</table>
		
	</div>
</div>

<style>
	.bg-red {
		background-color: rgb(255, 0, 0) !important;
	}
	.bg-yellow {
		background-color: rgb(255, 255, 0) !important;
		color: #000 !important;
	}
	.bg-green {
		background-color: rgb(0, 166, 90) !important;
	}
</style>

<?= Theme_View::factory('reportes/auto/footer') ?>
