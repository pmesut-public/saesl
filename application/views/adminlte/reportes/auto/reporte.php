	<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<table border="1" class="table table-bordered" style="background: #FFFFFF;">
    <tbody>
        <?php //debug($result) ?>
        <?php $d = 1; ?>
		<?php // debug($result) ?>
		<?php foreach ($result as $vald => $rowd): ?> <!-- Dimensiones -->
                <tr>
                    <td colspan="2" style="width: 15%;"><?= $rowd['dime']; ?></td>
                    <td colspan="7" style="width: 85%;"><?= $rowd['dime_descripcion']; ?></td>
                </tr>
                <?php if ($d == 1) { ?>
                <tr nobr="true">
                  <td class="success" style="width: 15%;text-align: center;vertical-align: middle;" colspan="2" rowspan="2">Componente</td>
                  <td class="success" style="width: 15%;text-align: center;vertical-align: middle;" colspan="2" rowspan="2">Indicador</td>
                  <td class="success" style="width: 70%;text-align: center;" colspan="5">Medios de Verificación</td>
                </tr>
                <tr class="success" nobr="true">
                  <td style="width: 21%;text-align: center;" colspan="2">Descripción</td>
                  <td style="width: 21%;text-align: center;">Documentos</td>
                  <!--<td style="width: 7%;text-align: center;">Estado</td>-->
                  <td style="width: 14%;text-align: center;">Observaciones / Comentarios</td>
                  <td style="width: 14%;text-align: center;">Consideraciones</td>
                </tr>
                <?php } ?>
                
                <?php //debug($rowd['data']) ?>
                <?php //echo count($rowd['data']); ?>
                <?php $f = 1; $rowFor= 0; $rf = ''; ?>
                <?php foreach ($rowd['data'] as $valf => $rowf) : ?>
                <?php foreach ($rowf['data'] as $vale => $rowFE){
                    if ($rf != '' && $rf == $rowf['fact_id']) {
                        $rowFor += count($rowFE['data']);
                    }
                    else {
                        $rowFor = count($rowFE['data']);
                    }
                    $rf = $rowf['fact_id'];                    
                }
                ?>
                    <tr>
                        <td rowspan="<?=$rowFor;//$rowf['row_fact']?>" style="width: 3%;vertical-align: top;"><?=$f?></td>
                        <td rowspan="<?=$rowFor?>" style="width: 12%;vertical-align: top;"><?=$rowf['fact']?></td>
                        
                        <?php end($rowf['data']); $last1 = key($rowf['data']); ?>
                        <?php //debug($rowf['data']) ?>
                        <?php $e = 1; ?>
                        <?php foreach ($rowf['data'] as $vale => $rowe) : ?>
                        <?php $rowEst = count($rowe['data']); ?>
                            <td rowspan="<?=$rowEst?>" style="width: 3%;vertical-align: top;"><?=$e?></td>
                            <td id="esta-<?= $rowe['esta_id'] ?>" rowspan="<?=$rowEst?>" style="width: 12%;vertical-align: top;"><?=$rowe['esta']?></td>
                            
                            <?php  end($rowe['data']); $last2 = key($rowe['data']); ?>
                            <?php //debug($rowe) ?>
                            <?php $m = 1; ?>
                            <?php foreach ($rowe['data'] as $valm => $rowm) : ?>
                            <?php //debug($rowm) ?>
                                <td style="width: 5%;vertical-align: top;"><?=$m?></td>
                                <td style="width: 16%;vertical-align: top;"><?=$rowm['data']['aume_descripcion']?></td>
                                <td style="width: 21%;vertical-align: top;">
                                    <?php $aFiles = Controller_Frontend_Reportes::get_document($rowm['aume_id']) ?>
                                    <table class="" border="0">
<!--					<thead>
                                            <tr>
                                                <th style="width: 80%; text-align: center;">Nombre del MV</th>
                                                <th style="width: 20%; text-align: center;">Estado</th>
                                            </tr>
                                        </thead>-->
                                        <tbody>
                                            <?php foreach ($aFiles as $oFiles): ?>
                                            <tr>
                                                <td style="width: 80%;"><spam>&#8226;&nbsp;</spam> <a href="<?= URL::base('http', TRUE).'file/'. $oFiles->docu_path ?>"><?= $oFiles->docu_titulo ?></a></td>
                                                    <!--<td style="width: 20%; text-align: center;"><?= Model_AutoevaluacionDocumento::get_status_by_report($oFiles->audo_estado) ?></td>-->
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="width: 14%;vertical-align: top;"><?=$rowm['data']['aume_observacion']?></td>
                                <td style="width: 14%;vertical-align: top;"><?=$rowm['data']['aume_consideracion']?></td>
                                </tr>
                                <?php if ($vale != $last1 OR $valm != $last2): ?> <tr nobr="true"> <?php endif ?>
                            <?php $m++; endforeach ?>
                        <?php $e++; endforeach ?>
                <?php $f++; endforeach ?>
                <?php $d++; endforeach ?>
    </tbody>
	
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>


