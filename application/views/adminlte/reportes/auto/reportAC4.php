<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php // debug($result); die; ?>

<table border="1" class="table table-bordered">
	<tbody>
		<?php $data = Controller_Frontend_Reportes::get_data_reportAC4($result) ?>
		<?php // debug2($data) ?>
		<?php foreach ($data['data'] as $val1 => $oDime): ?> <!-- Dimensiones -->
			<tr style="background: #dff0d8;">
				<td style="width: 10%;">Dimensión <?= $oDime['codigo'] ?></td>
				<td colspan="7" style="width: 90%;"><?= $oDime['dime'] ?></td>
			</tr>				
			<?php foreach ($oDime['aFact'] as $val2 => $oFact): ?>  <!-- Factores -->
				<tr style="background: #dff0d8;">
					<td style="width: 10%;">&raquo; Factor <?= $oFact['codigo'] ?></td>
					<td colspan="7" style="width: 90%;"><?= $oFact['fact'] ?></td>
				</tr>			
				<tr nobr="true">
					<td rowspan="2" colspan="2" style="width: 20%; text-align: center;vertical-align: middle;background: #ecf3dc;">Estandar</td>
					<td colspan="3" style="width: 30%; text-align: center;vertical-align: middle;background: #ecf3dc;">Actividades Registradas</td>
					<td rowspan="2" style="width: 20%; text-align: center;vertical-align: middle;background: #ecf3dc;">Evidencias</td>
					<td rowspan="2" style="width: 20%; text-align: center;vertical-align: middle;background: #ecf3dc;">Justificación</td>
					<td rowspan="2" style="width: 10%; text-align: center;vertical-align: middle;background: #ecf3dc;">Estado del Estandar</td>
				</tr>
				<tr>
					<td style="width: 4%; text-align: center; vertical-align: middle;background: #ecf3dc;">Tipo</td>
					<td style="width: 4%; text-align: center; vertical-align: middle;background: #ecf3dc;">N°</td>
					<td style="width: 22%;text-align: center;vertical-align: middle;background: #ecf3dc;">Detalle</td>
				</tr>
				<?php foreach ($oFact['aEsta'] as $val3 => $oEsta): ?>  <!-- Estándares -->
					<tr>
						<td rowspan="<?= $oEsta['rowspan'] ?>" colspan="2" style="width: 20%;">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th colspan="2"><?= $oEsta['esta'] ?></th>
									</tr>
									<tr>
										<td style="text-align: justify;" colspan="2"><?= $oEsta['descripcion'] ?></td>
									</tr>
									<tr>
										<th colspan="2">Criterios</th>
									</tr>
									<?php foreach ($oEsta['aCriterios'] as $codigo => $criterio): ?>
										<tr>
											<td style="text-align: justify;" colspan="2"><?= $codigo . '.- ' . $criterio ?></td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</td>
						<?php $d = FALSE ?>
						<?php if (count($oEsta['aActi']) > 0): ?>
							<?php foreach ($oEsta['aActi'] as $val4 => $oActividad): ?>  <!-- Actividades-->
								<?php if ($d): ?>
								<tr>
								<?php endif ?>
								<td style="text-align: center;vertical-align: middle; width: 4%;"><?= $oActividad['tipo'] ?></td>
								<td style="text-align: center;vertical-align: middle; width: 4%;"><?= $oActividad['numeracion'] ?></td>
								<td style="vertical-align: middle; width: 22%;"><?= $oActividad['acti'] ?></td>
								<?php if (!$d): ?>
									<td rowspan="<?= $oEsta['rowspan'] ?>" style="width: 20%;text-align: center;vertical-align: middle;">
										<?php if (count($oEsta['aEvidencias']) > 0): ?>
											<table class="table table-bordered">
												<tbody>
													<?php foreach ($oEsta['aEvidencias'] as $codigo => $evidencia): ?>
														<tr>
															<td style="text-align: justify;"><?= $evidencia ?></td>
														</tr>
													<?php endforeach ?>
												</tbody>
											</table>
										<?php else: ?>
											Sin Evidencias
										<?php endif ?>
									</td>
									<td rowspan="<?= $oEsta['rowspan'] ?>" style="text-align: <?php echo (!empty($oEsta['justificacion'])) ? 'justify' : 'center' ?>;vertical-align: middle; width: 20%;"><?= (!empty($oEsta['justificacion'])) ? $oEsta['justificacion'] : 'Sin Justificación' ?></td>
									<td rowspan="<?= $oEsta['rowspan'] ?>" style="text-align: center;vertical-align: middle; width: 10%;"><?= ($oEsta['estado']->loaded()) ? $oEsta['estado']->esta_titulo : 'Sin Estado' ?></td>
								<?php endif ?>
								<?php $d = TRUE ?>
							</tr>
						<?php endforeach ?>						
					<?php else: ?> <!--Si no tiene actividades-->
					<td colspan="3" style="text-align: center;vertical-align: middle; width: 30%;">Sin actividades</td>
					<td rowspan="<?= $oEsta['rowspan'] ?>" style="width: 20%;text-align: center;vertical-align: middle;">
						<?php if (count($oEsta['aEvidencias']) > 0): ?>
							<table class="table table-bordered">
								<tbody>
									<?php foreach ($oEsta['aEvidencias'] as $codigo => $evidencia): ?>
										<tr>
											<td style="text-align: justify;"><?= $evidencia ?></td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						<?php else: ?>
							Sin Evidencias
						<?php endif ?>
					</td>
					<td rowspan="<?= $oEsta['rowspan'] ?>" style="text-align: center;vertical-align: middle; width: 20%;"><?= (!empty($oEsta['justificacion'])) ? $oEsta['justificacion'] : 'Sin Justificación' ?></td>
					<td rowspan="<?= $oEsta['rowspan'] ?>" style="text-align: center;vertical-align: middle; width: 10%;"><?= ($oEsta['estado']->loaded()) ? $oEsta['estado']->esta_titulo : 'Sin Estado' ?></td>
				</tr>
			<?php endif ?>
			<?php if ($oEsta['estado_id'] == 6): ?>
				<tr style="background: rgb(234, 234, 234);">
					<th colspan="2" style="text-align: center;vertical-align: middle;">Nivel de Cumplimiento del Estandar</th>
					<th colspan="3" style="text-align: center;vertical-align: middle;">Actividades que faltan o sobran</th>
					<th colspan="3" style="text-align: center;vertical-align: middle;">Impacto esperado en el cumplimiento del estandar</th>
				</tr>
				<tr style="background: rgb(234, 234, 234);">
					<td colspan="2" style="text-align: center;vertical-align: middle;"><?= $oEsta['pregunta1'] ?></td>
					<td colspan="3" style="text-align: center;vertical-align: middle;"><?= $oEsta['pregunta2'] ?></td>
					<td colspan="3" style="text-align: center;vertical-align: middle;"><?= $oEsta['pregunta3'] ?></td>
				</tr>
			<?php elseif ($oEsta['estado_id'] == 9): ?>
				<tr style="background: rgb(234, 234, 234);">
					<th colspan="2" style="text-align: center;vertical-align: middle;">Nivel de Cumplimiento del Estandar</th>
					<th colspan="6" style="text-align: center;vertical-align: middle;">Impacto esperado en el cumplimiento del estandar</th>
				</tr>
				<tr style="background: rgb(234, 234, 234);">
					<td colspan="2" style="text-align: center;vertical-align: middle;"><?= $oEsta['pregunta1'] ?></td>
					<td colspan="6" style="text-align: center;vertical-align: middle;"><?= $oEsta['pregunta3'] ?></td>
				</tr>
			<?php endif ?>
		<?php endforeach ?>
	<?php endforeach ?>
<?php endforeach ?>
</tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>


