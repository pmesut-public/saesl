<h3 class="text-center">
	<?= $title ?><br>
	
	<?php if ($oAutoevaluacion->auto_estado == Model_Autoevaluacion::STATUS_ACTIVO): ?>
		<small>Fecha de última modificación: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
	<?php else: ?>
		<small>Fecha de cierre: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
	<?php endif ?>
	
	<small>- <?= $oAutoevaluacion->oAcreditadoEvento->oEvento->even_nombre ?> -</small>
</h3><hr>

<h4 class="text-center">
	<?= $subtitle ?><br>
	<small><?= $objeto ?></small>
</h4>

<table border="1" class="table table-bordered">
	<thead>
		<tr class="success" nobr="true">
			<th class="text-center" rowspan="2" style="width: 5%;">DIMENSIÓN</th>
			<th class="text-center" rowspan="2" style="width: 5%;">FACTOR</th>
			<th class="text-center" rowspan="1" colspan="6" style="width: 50%;">ESTANDAR</th>
			<th class="text-center" rowspan="1" colspan="3" style="width: 40%;">FV</th>
		</tr>
		<tr class="success" nobr="true">
			<th class="text-center" style="width: 25%;">DESCRIPCION</th>
			<th class="text-center" style="width: 5%;">PUNTAJE MÍNIMO</th>
			<th class="text-center" style="width: 5%;">PUNTAJE A. INICIAL</th>
			<th class="text-center" style="width: 5%;">PUNTAJE A. FINAL</th>
			<th class="text-center" style="width: 5%;">CUMPLIMIENTO A. INICIAL</th>
			<th class="text-center" style="width: 5%;">CUMPLIMIENTO A. FINAL</th>
			<th class="text-center" style="width: 30%;">DESCRIPCION</th>
			<th class="text-center" style="width: 5%;">CUMPLIMIENTO A. INICIAL</th>
			<th class="text-center" style="width: 5%;">CUMPLIMIENTO A. FINAL</th>
		</tr>
	</thead>
	<tbody>
		
		<?php $dime = $fact = $esta = NULL; ?>
		<?php foreach ($result as $index => $row): ?>
			<?php //debug($row) ?>
			
			<?php $row_conv = $result_conv[$index] ?>
			
			<tr>
				
				<?php if ($row['dime_codigo'] != $dime): ?>
					<th rowspan="<?= $dime_count[$row['dime_codigo']] ?>" style="width: 5%;">
						<?= $row['dime_codigo'] ?>
					</th>
				<?php endif ?>
				
				<?php if ($row['fact_codigo'] != $fact): ?>
					<th rowspan="<?= $fact_count[$row['fact_codigo']] ?>" style="width: 5%;">
						<?= $row['fact_codigo'] ?>
					</th>
				<?php endif ?>
				
				<?php if ($row['esta_codigo'] != $esta): ?>
					<td rowspan="<?= $esta_count[$row['esta_codigo']] ?>" style="width: 25%;">
						<?= $row['esta'] ?>
					</td>
					<td rowspan="<?= $esta_count[$row['esta_codigo']] ?>" style="width: 5%;">
						<?= $row['esta_nivel_aceptacion'] ?>
					</td>
					<td rowspan="<?= $esta_count[$row['esta_codigo']] ?>" style="width: 5%;">
						<?= $row_conv['aceptacion'] ?>
					</td>
					<td rowspan="<?= $esta_count[$row['esta_codigo']] ?>" style="width: 5%;">
						<?= $row['aceptacion'] ?>
					</td>
					<td rowspan="<?= $esta_count[$row['esta_codigo']] ?>" style="width: 5%;">
						<?= $row_conv['aude'] ?>
					</td>
					<td rowspan="<?= $esta_count[$row['esta_codigo']] ?>" style="width: 5%;">
						<?= $row['aude'] ?>
					</td>
				<?php endif ?>
				
				<td style="width: 30%;">
					<p class="<?= $row['conc_obligatorio'] ? 'strong' : NULL ?>"><?= $row['conc'] ?></p>
				</td>
				<td style="width: 5%;">
					<?= $row_conv['code'] ?>
				</td>
				<td style="width: 5%;">
					<?= $row['code'] ?>
				</td>
				
				<?php
					$dime = $row['dime_codigo'];
					$fact = $row['fact_codigo'];
					$esta = $row['esta_codigo'];
				?>
			</tr>
			
		<?php endforeach ?>
		
	</tbody>
</table><br>

<?= Theme_View::factory('reportes/auto/footer') ?>
