	<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php // debug($result); die; ?>

<table border="1" class="table table-bordered" style="background: #FFFFFF;">
	<thead>
		<tr class="success" nobr="true">
			<th rowspan="2" style="width: 14%; text-align: center;vertical-align: middle;">DIMENSIÓN</th>
			<th rowspan="2" style="width: 18%; text-align: center;vertical-align: middle;">FACTOR</th>
			<th colspan="4" style="width: 48%; text-align: center;vertical-align: middle;">Actividades Registradas</th>
			<th rowspan="2" style="width: 10%; text-align: center;vertical-align: middle;">Nivel de importancia para el cumplimiento del Factor</th>
			<th rowspan="2" style="width: 10%; text-align: center;vertical-align: middle;">Nivel de avance en el logro de la Actividad (%)</th>
		</tr>
		<tr>
			<th style="width: 4%; text-align: center; vertical-align: middle; background: #dff0d8;">Tipo</th>
			<th style="width: 4%; text-align: center; vertical-align: middle; background: #dff0d8;">N°</th>
			<th style="width: 30%;background: #dff0d8; text-align: center;vertical-align: middle;">Detalle</th>
			<th style="width: 10%;background: #dff0d8; text-align: center;vertical-align: middle;">Estándares Relacionados</th>
		</tr>
	</thead>

	<tbody>

		<?php $data = Controller_Frontend_Reportes::get_data_reportFAE($result) ?>
		<?php // debug($data) ?>
		<?php foreach ($data['data'] as $val1 => $oDime): ?> <!-- Dimensiones -->
			<?php $d = FALSE ?>
				<tr>
					<td rowspan="<?= $oDime['rowspan'] + $oDime['numFact'] + 1 ?>" style="vertical-align: middle; width: 14%;"><?= $oDime['dime'] ?></td>
					
			<?php foreach ($oDime['aFact'] as $val2 => $oFact): ?>  <!-- Factores -->
				<?php if($d): ?>
					<tr>
				<?php endif?>
						
				<?php 
					$d = TRUE; 
					$f = FALSE;
				?>
				<td rowspan="<?= $oFact['rowspan'] ?>" style="vertical-align: middle; width: 18%;"><?= $oFact['fact'] ?></td>
                <?php // debug($oFact) ?>
                    
                <?php if (count($oFact['aActi']) > 0): ?>
                    <?php foreach ($oFact['aActi'] as $val4 => $oActividad): ?>  <!-- Actividades-->
                        <?php if($d AND $f): ?>
                            <tr>
                        <?php endif?>
                            <td style="text-align: center;vertical-align: middle; width: 4%;"><?= $oActividad['tipo'] ?></td>
                            <td style="text-align: center;vertical-align: middle; width: 4%;"><?= $oActividad['numeracion'] ?></td>
                            <td style="vertical-align: middle; width: 30%;"><?= $oActividad['acti'] ?></td>
                            <td style="vertical-align: middle; width: 10%;"><?= $oActividad['estandares'] ?></td>
                            <td style="text-align: center;vertical-align: middle; width: 10%;"><?= $oActividad['importancia'] ?><?= isset($oActividad['importancia']) ? '%' : NULL ?></td>
                            <td style="text-align: center;vertical-align: middle; width: 10%;"><?= $oActividad['avance'] ?><?= isset($oActividad['avance']) ? '%' : NULL ?></td>
                        </tr>
                        <?php $f = TRUE ?>
                    <?php endforeach ?>						
                <?php else: ?> <!--Si no tiene actividades-->
                    <?php if($d AND $f): ?>
                        <tr>
                    <?php endif?>
                        <td style="text-align: center;vertical-align: middle; width: 4%;"></td>
                        <td style="text-align: center;vertical-align: middle; width: 4%;"></td>
                        <td colspan="1" style="text-align: left;vertical-align: middle; width: 30%;">Sin actividades</td>
                        <td style="text-align: center;vertical-align: middle; width: 10%;"></td>
                        <td style="text-align: center;vertical-align: middle; width: 10%;"></td>
                        <td style="text-align: center;vertical-align: middle; width: 10%;"></td>
                    </tr>
                <?php endif ?>
                    
				<tr style="background: #F9F9F9;">
					<td colspan="5" style="text-align: center; font-weight: bold; width: 66%;">TOTAL POR FACTOR</td>
					<td style="text-align: center; font-weight: bold;"><?= $oFact['importancia'] ?>%</td>
					<td style="text-align: center; font-weight: bold;"><?= $oFact['avance'] ?>%</td>
				</tr>
			<?php endforeach ?>
			<tr style="background: #F9F9F9;">
				<td colspan="5" style="text-align: center; font-weight: bold; width: 66%;">TOTAL POR DIMENSIÓN</td>
				<td style="text-align: center; font-weight: bold; width: 10%;"><?= $oDime['importancia'] ?>%</td>
				<td style="text-align: center; font-weight: bold; width: 10%;"><?= $oDime['avance'] ?>%</td>
			</tr>
		<?php endforeach ?>
		<tr style="background: #F9F9F9;">
			<td></td>
			<td colspan="5" style="text-align: center; font-weight: bold; width: 66%;">TOTAL AUTOEVALUACIÓN</td>
			<td style="text-align: center; font-weight: bold; width: 10%;"><?= $data['importancia'] ?>%</td>
			<td style="text-align: center; font-weight: bold; width: 10%;"><?= $data['avance'] ?>%</td>
		</tr>
	</tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>


