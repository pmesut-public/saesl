<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php // debug($result); die; ?>

<table border="1" class="table table-bordered" style="background: #FFFFFF;">
	<thead>
		<tr class="success" nobr="true">
			<th rowspan="2" colspan="2" style="width: 17%; text-align: center;vertical-align: middle;">DIMENSIÓN</th>
			<th rowspan="2" colspan="2" style="width: 17%; text-align: center;vertical-align: middle;">FACTOR</th>
			<th rowspan="2" colspan="2" style="width: 17%; text-align: center;vertical-align: middle;">ESTANDAR</th>
			<th colspan="8" style="width: 49%; text-align: center;vertical-align: middle;">Actividades Registradas</th>
		</tr>
		<tr>
			<th style="width: 3%; text-align: center; vertical-align: middle; background: #dff0d8;">Tipo</th>
			<th style="width: 3%; text-align: center; vertical-align: middle; background: #dff0d8;">N°</th>
			<th style="width: 18%;background: #dff0d8; text-align: center;vertical-align: middle;">Detalle</th>
			<th colspan="2" style="width: 10%;background: #dff0d8; text-align: center;vertical-align: middle;">Importancia de la actividad</th>
			<th colspan="3" style="width: 15%;background: #dff0d8; text-align: center;vertical-align: middle;">Logro de la actividad</th>
		</tr>
		<tr>
			<th style="width: 4%">Peso</th>
			<th style="width: 13%"></th>
			<th style="width: 4%">Peso</th>
			<th style="width: 13%"></th>
			<th style="width: 4%">Peso</th>
			<th style="width: 13%"></th>
			<th style="width: 3%"></th>
			<th style="width: 3%"></th>
			<th style="width: 18%"></th>
			<th style="width: 5%">Peso sobre el FACTOR  </th>
			<th style="width: 5%">Según Matriz de Calidad</th>
			<th style="width: 5%">% de Implementación</th>
			<th style="width: 5%">% Sobre el peso del Factor</th>
			<th style="width: 5%">Sobre Matriz de calidad</th>
		</tr>
	</thead>
	<tbody>
		<?php $data = Controller_Frontend_Reportes::get_data_reportDFEA($result, TRUE) ?>

		<!-- Dimensiones -->
		<?php foreach ($data['data'] as $val1 => $oDime): ?> 
			<?php $d = FALSE ?>
			<tr>
				<td rowspan="<?= $oDime['rowspan'] + $oDime['numFact'] + $oDime['num_esta'] + 1 ?>" style="vertical-align: middle; width: 4%;"><?= $oDime['peso'] ?>%</td>
				<td rowspan="<?= $oDime['rowspan'] + $oDime['numFact'] + $oDime['num_esta'] + 1 ?>" style="vertical-align: middle; width: 13%;"><?= $oDime['dime'] ?></td>

				<!-- Factores -->
				<?php foreach ($oDime['aFact'] as $val2 => $oFact): ?>  
					<?php if ($d): ?>
					<tr>
					<?php endif ?>

					<?php
					$d = TRUE;
					$f = FALSE;
					?>
					<td rowspan="<?= $oFact['rowspan'] + $oFact['num_esta'] ?>" style="vertical-align: middle; width: 4%;"><?= $oFact['peso'] ?>%</td>
					<td rowspan="<?= $oFact['rowspan'] + $oFact['num_esta'] ?>" style="vertical-align: middle; width: 13%;"><?= $oFact['fact'] ?></td>

					<!-- Estándares -->
					<?php foreach ($oFact['aEsta'] as $val3 => $oEsta): ?>  
						<?php if ($d AND $f): ?>
						<tr>
						<?php endif ?>

						<?php
						$f = TRUE;
						$e = FALSE;
						?>
						<td rowspan="<?= $oEsta['rowspan'] ?>" style="vertical-align: middle; width: 4%;"><?= $oEsta['peso'] ?>%</td>
						<td rowspan="<?= $oEsta['rowspan'] ?>" style="vertical-align: middle; width: 13%;"><?= $oEsta['esta'] ?></td>

						<?php if (count($oEsta['aActi']) > 0): ?>
							<!-- Actividades-->
							<?php foreach ($oEsta['aActi'] as $val4 => $oActividad): ?>  
								<?php if ($d AND $f AND $e): ?>
								<tr>
								<?php endif ?>
								<td style="text-align: center;vertical-align: middle; width: 3%;"><?= $oActividad['tipo'] ?></td>
								<td style="text-align: center;vertical-align: middle; width: 3%;"><?= $oActividad['numeracion'] ?></td>
								<td style="vertical-align: middle; width: 18%;"><?= $oActividad['acti'] ?></td>
								<td style="text-align: center;vertical-align: middle; width: 5%;"><?= $oActividad['importancia'] ?><?= isset($oActividad['importancia']) ? '%' : NULL ?></td>
								<td style="text-align: center;vertical-align: middle; width: 5%;"><?= $oActividad['impor_matriz'] ?><?= isset($oActividad['impor_matriz']) ? '%' : NULL ?></td>
								<td style="text-align: center;vertical-align: middle; width: 5%;"><?= $oActividad['avance'] ?><?= isset($oActividad['avance']) ? '%' : NULL ?></td>
								<td style="text-align: center;vertical-align: middle; width: 5%;"><?= $oActividad['logro_peso'] ?><?= isset($oActividad['logro_peso']) ? '%' : NULL ?></td>
								<td style="text-align: center;vertical-align: middle; width: 5%;"><?= $oActividad['logro_matriz'] ?><?= isset($oActividad['logro_matriz']) ? '%' : NULL ?></td>
							</tr>
							<?php $e = TRUE ?>
						<?php endforeach ?>						
					<?php else: ?>

						<!--Si no tiene actividades-->
						<?php if ($d AND $f AND $e): ?>
							<tr>
							<?php endif ?>
							<td colspan="3" style="text-align: center;vertical-align: middle; width: 24%;">Sin actividades</td>
							<td style="text-align: center;vertical-align: middle; width: 5%;"></td>
							<td style="text-align: center;vertical-align: middle; width: 5%;"></td>
							<td style="text-align: center;vertical-align: middle; width: 5%;"></td>
							<td style="text-align: center;vertical-align: middle; width: 5%;"></td>
							<td style="text-align: center;vertical-align: middle; width: 5%;"></td>
						</tr>
					<?php endif ?>
					<?php $e = TRUE ?>
					<tr>
						<td style="text-align: center;vertical-align: middle; width: 4%;"></td>
						<td style="text-align: center;vertical-align: middle; width: 13%;"></td>
						<td style="text-align: center;vertical-align: middle; width: 3%;"></td>
						<td style="text-align: center;vertical-align: middle; width: 3%;"></td>
						<td style="text-align: center;vertical-align: middle; width: 18%;"></td>
						<td style="text-align: center;vertical-align: middle; width: 5%;"></td>
						<td style="text-align: center;vertical-align: middle; width: 5%; font-weight: bold;"><?= $oEsta['import_matriz'] ?>%</td>
						<td style="text-align: center;vertical-align: middle; width: 5%;"></td>
						<td style="text-align: center;vertical-align: middle; width: 5%;"></td>
						<td style="text-align: center;vertical-align: middle; width: 5%;"></td>
					</tr>
				<?php endforeach ?>
				<tr style="background: #F9F9F9;">
					<td colspan="7" style="text-align: center; font-weight: bold; width: 58%;">TOTAL POR FACTOR</td>
					<td style="text-align: center; font-weight: bold; width: 5%"><?= $oFact['importancia'] ?>%</td>
					<td style="text-align: center; font-weight: bold; width: 5%"><?= $oFact['impor_matriz'] ?>%</td>
					<td style="text-align: center; font-weight: bold; width: 5%"><?= $oFact['avance'] ?>%</td>
					<td style="text-align: center; font-weight: bold; width: 5%"><?= $oFact['logro_peso'] ?>%</td>
					<td style="text-align: center; font-weight: bold; width: 5%"><?= $oFact['logro_matriz'] ?>%</td>
				</tr>
			<?php endforeach ?>
			<tr style="background: #F9F9F9;">
				<td colspan="7" style="text-align: center; font-weight: bold; width: 58%;">TOTAL POR DIMENSIÓN</td>
				<td style="text-align: center; font-weight: bold; width: 5%;"><?= $oDime['importancia'] ?>%</td>
				<td style="text-align: center; font-weight: bold; width: 5%"><?= $oDime['impor_matriz'] ?>%</td>
				<td style="text-align: center; font-weight: bold; width: 5%;"><?= $oDime['avance'] ?>%</td>
				<td style="text-align: center; font-weight: bold; width: 5%"><?= $oDime['logro_peso'] ?>%</td>
				<td style="text-align: center; font-weight: bold; width: 5%"><?= $oDime['logro_matriz'] ?>%</td>
			</tr>
		<?php endforeach ?>
		<tr style="background: #F9F9F9;">
			<td colspan="2" style="width: 17%;"></td>
			<td colspan="7" style="text-align: center; font-weight: bold; width: 58%;">TOTAL AUTOEVALUACIÓN</td>
			<td style="text-align: center; font-weight: bold; width: 5%;"><?= $data['importancia'] ?>%</td>
			<td style="text-align: center; font-weight: bold; width: 5%"><?= $data['impor_matriz'] ?>%</td>
			<td style="text-align: center; font-weight: bold; width: 5%;"><?= $data['avance'] ?>%</td>
			<td style="text-align: center; font-weight: bold; width: 5%"><?= $data['logro_peso'] ?>%</td>
			<td style="text-align: center; font-weight: bold; width: 5%"><?= $data['logro_matriz'] ?>%</td>
		</tr>
	</tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>


