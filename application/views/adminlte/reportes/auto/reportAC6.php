<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php // debug($result); die; ?>

<table border="1" class="table table-bordered">
	<tbody>
		<?php $data = Controller_Frontend_Reportes::get_data_reportAC4($result, TRUE) ?>
		<?php // debug2($data) ?>
		<tr style="background: #dff0d8;">
			<td style="width: 10%;"></td>
			<td colspan="9" style="width: 80%;text-align: right;font-weight: bold;">Logro General de Actividades:</td>
			<td style="width: 10%;font-weight: bold;"><?= $data['avance'] ?>%</td>
		</tr>				
		<?php foreach ($data['data'] as $val1 => $oDime): ?> <!-- Dimensiones -->
			<tr style="background: #dff0d8;">
				<td style="width: 10%;">Dimensión <?= $oDime['codigo'] ?></td>
				<td colspan="10" style="width: 90%;"><?= $oDime['dime'] ?></td>
			</tr>				
			<?php foreach ($oDime['aFact'] as $val2 => $oFact): ?>  <!-- Factores -->
				<tr style="background: #dff0d8;">
					<td style="width: 10%;">&raquo; Factor <?= $oFact['codigo'] ?></td>
					<td colspan="10" style="width: 90%;"><?= $oFact['fact'] ?></td>
				</tr>			
				<tr nobr="true">
					<td rowspan="2" colspan="2" style="width: 20%; text-align: center;vertical-align: middle;background: #ecf3dc;">Estandar</td>
					<td colspan="3" style="width: 30%; text-align: center;vertical-align: middle;background: #ecf3dc;">Actividades Registradas</td>
					<td colspan="2" style="width: 16%; text-align: center;vertical-align: middle;background: #ecf3dc;">Importancia de la actividadr</td>
					<td colspan="3" style="width: 24%; text-align: center;vertical-align: middle;background: #ecf3dc;">Logro de la actividad</td>
					<td rowspan="2" style="width: 10%; text-align: center;vertical-align: middle;background: #ecf3dc;">Estado del Estandar</td>
				</tr>
				<tr>
					<td style="width: 4%; text-align: center; vertical-align: middle;background: #ecf3dc;">Tipo</td>
					<td style="width: 4%; text-align: center; vertical-align: middle;background: #ecf3dc;">N°</td>
					<td style="width: 22%;text-align: center;vertical-align: middle;background: #ecf3dc;">Detalle</td>
					<td style="width: 8%;text-align: center;vertical-align: middle;background: #ecf3dc;">Peso sobre el Factor</td>
					<td style="width: 8%;text-align: center;vertical-align: middle;background: #ecf3dc;">Según matríz de calidad</td>
					<td style="width: 8%;text-align: center;vertical-align: middle;background: #ecf3dc;">% de implementación</td>
					<td style="width: 8%;text-align: center;vertical-align: middle;background: #ecf3dc;">% sobre el peso del factor</td>
					<td style="width: 8%;text-align: center;vertical-align: middle;background: #ecf3dc;">Sobre la matriz de calidad</td>
				</tr>
				<?php foreach ($oFact['aEsta'] as $val3 => $oEsta): ?>  <!-- Estándares -->
					<tr>
						<td rowspan="<?= $oEsta['rowspan'] ?>" colspan="2" style="width: 20%;">
							<table class="table table-bordered">
								<tbody>
									<tr>
										<th colspan="2"><?= $oEsta['esta'] ?></th>
									</tr>
									<tr>
										<td style="text-align: justify;" colspan="2"><?= $oEsta['descripcion'] ?></td>
									</tr>
									<tr>
										<th colspan="2">Criterios</th>
									</tr>
									<?php foreach ($oEsta['aCriterios'] as $codigo => $criterio): ?>
										<tr>
											<td style="text-align: justify;" colspan="2"><?= $codigo . '.- ' . $criterio ?></td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</td>

						<?php $d = FALSE ?>
						<?php if (count($oEsta['aActi']) > 0): ?>
							<?php foreach ($oEsta['aActi'] as $val4 => $oActividad): ?>  <!-- Actividades -->
								<?php if ($d): ?>
								<tr>
								<?php endif ?>
								<td style="text-align: center;vertical-align: middle; width: 4%;"><?= $oActividad['tipo'] ?></td>
								<td style="text-align: center;vertical-align: middle; width: 4%;"><?= $oActividad['numeracion'] ?></td>
								<td style="vertical-align: middle; width: 22%;"><?= $oActividad['acti'] ?></td>
								<td style="text-align: center;vertical-align: middle; width: 8%;"><?= $oActividad['importancia'] ?><?= isset($oActividad['importancia']) ? '%' : NULL ?></td>
								<td style="text-align: center;vertical-align: middle; width: 8%;"><?= $oActividad['impor_matriz'] ?><?= isset($oActividad['impor_matriz']) ? '%' : NULL ?></td>
								<td style="text-align: center;vertical-align: middle; width: 8%;"><?= $oActividad['avance'] ?><?= isset($oActividad['avance']) ? '%' : NULL ?></td>
								<td style="text-align: center;vertical-align: middle; width: 8%;"><?= $oActividad['logro_peso'] ?><?= isset($oActividad['logro_peso']) ? '%' : NULL ?></td>
								<td style="text-align: center;vertical-align: middle; width: 8%;"><?= $oActividad['logro_matriz'] ?><?= isset($oActividad['logro_matriz']) ? '%' : NULL ?></td>
								<?php if (!$d): ?>
									<td rowspan="<?= $oEsta['rowspan'] ?>" style="text-align: center;vertical-align: middle; width: 10%;"><?= ($oEsta['estado']->loaded()) ? $oEsta['estado']->esta_titulo : 'Sin Estado' ?></td>
								<?php endif ?>
								<?php $d = TRUE ?>
							</tr>
						<?php endforeach ?>						
					<?php else: ?> 
						<!--Si no tiene actividades-->
					<td colspan="3" style="text-align: center;vertical-align: middle; width: 30%;">Sin actividades</td>
					<td style="width: 8%;text-align: center;vertical-align: middle;"></td>
					<td style="width: 8%;text-align: center;vertical-align: middle;"></td>
					<td style="width: 8%;text-align: center;vertical-align: middle;"></td>
					<td style="width: 8%;text-align: center;vertical-align: middle;"></td>
					<td style="width: 8%;text-align: center;vertical-align: middle;"></td>
					<td style="text-align: center;vertical-align: middle; width: 10%;"><?= ($oEsta['estado']->loaded()) ? $oEsta['estado']->esta_titulo : 'Sin Estado' ?></td>
				</tr>
			<?php endif ?>
			<tr style="font-weight: bold;">
				<td style="text-align: center;" colspan="5"></td>
				<td style="text-align: center;"></td>
				<td style="text-align: center;"><?= $oEsta['impor_matriz'] ?>%</td>
				<td style="text-align: center;"></td>
				<td style="text-align: center;"></td>
				<td style="text-align: center;"></td>
				<td></td>
			</tr>
			<?php if ($oEsta['estado_id'] == 6): ?>
				<tr style="background: rgb(234, 234, 234);">
					<th colspan="2" style="text-align: center;vertical-align: middle;">Nivel de Cumplimiento del Estandar</th>
					<th colspan="4" style="text-align: center;vertical-align: middle;">Actividades que faltan o sobran</th>
					<th colspan="5" style="text-align: center;vertical-align: middle;">Impacto esperado en el cumplimiento del estandar</th>
				</tr>
				<tr style="background: rgb(234, 234, 234);">
					<td colspan="2" style="text-align: center;vertical-align: middle;"><?= $oEsta['pregunta1'] ?></td>
					<td colspan="4" style="text-align: center;vertical-align: middle;"><?= $oEsta['pregunta2'] ?></td>
					<td colspan="5" style="text-align: center;vertical-align: middle;"><?= $oEsta['pregunta3'] ?></td>
				</tr>
			<?php elseif ($oEsta['estado_id'] == 9): ?>
				<tr style="background: rgb(234, 234, 234);">
					<th colspan="2" style="text-align: center;vertical-align: middle;">Nivel de Cumplimiento del Estandar</th>
					<th colspan="9" style="text-align: center;vertical-align: middle;">Impacto esperado en el cumplimiento del estandar</th>
				</tr>
				<tr style="background: rgb(234, 234, 234);">
					<td colspan="2" style="text-align: center;vertical-align: middle;"><?= $oEsta['pregunta1'] ?></td>
					<td colspan="9" style="text-align: center;vertical-align: middle;"><?= $oEsta['pregunta3'] ?></td>
				</tr>
			<?php endif ?>
		<?php endforeach ?>
		<tr style="font-weight: bold;">
			<td style="text-align: center;" colspan="5">Total por Factor</td>
			<td style="text-align: center;"><?= $oFact['importancia'] ?>%</td>
			<td style="text-align: center;"><?= $oFact['impor_matriz'] ?>%</td>
			<td style="text-align: center;"><?= $oFact['avance'] ?>%</td>
			<td style="text-align: center;"><?= $oFact['logro_peso'] ?>%</td>
			<td style="text-align: center;"><?= $oFact['logro_matriz'] ?>%</td>
			<td></td>
		</tr>
	<?php endforeach ?>
	<tr style="font-weight: bold;">
		<td style="text-align: center;" colspan="5">Total por Dimensión</td>
		<td style="text-align: center;"><?= $oDime['importancia'] ?>%</td>
		<td style="text-align: center;"><?= $oDime['impor_matriz'] ?>%</td>
		<td style="text-align: center;"><?= $oDime['avance'] ?>%</td>
		<td style="text-align: center;"><?= $oDime['logro_peso'] ?>%</td>
		<td style="text-align: center;"><?= $oDime['logro_matriz'] ?>%</td>
		<td></td>
	</tr>
<?php endforeach ?>
<tr style="font-weight: bold;">
	<td style="text-align: center;" colspan="5">Total por Autoevaluación</td>
	<td style="text-align: center;"><?= $data['importancia'] ?>%</td>
	<td style="text-align: center;"><?= $data['impor_matriz'] ?>%</td>
	<td style="text-align: center;"><?= $data['avance'] ?>%</td>
	<td style="text-align: center;"><?= $data['logro_peso'] ?>%</td>
	<td style="text-align: center;"><?= $data['logro_matriz'] ?>%</td>
	<td></td>
</tr>
</tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>


