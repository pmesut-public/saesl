<h3 class="text-center">
	<?= $title ?><br>
	
	<?php if ($oAutoevaluacion->auto_estado == Model_Autoevaluacion::STATUS_ACTIVO): ?>
		<small>Fecha de última modificación: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
		<small>(Autoevaluación pendiente de cierre)</small><br>
	<?php else: ?>
		<small>Fecha de cierre: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
	<?php endif ?>
	
	<small>- <?php //= $oAutoevaluacion->oAcreditadoEvento->oEvento->even_nombre ?> <?= $subtitle ?> -</small>
        <br />
        <small><?= $objeto ?></small>
</h3><hr>

<h4 class="text-center">
    <strong>Reporte del Estado de las Condiciones Básicas de calidad</strong>
	<?php //= $subtitle ?><br>
	<!--<small><?php //= $objeto ?></small>-->
</h4>
