<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php //debug($result) ?>

<table border="1" class="table table-bordered">
	<thead>
		<tr class="success" nobr="true">
			<th style="width: 20%;">DIMENSIÓN</th>
			<th style="width: 25%;">FACTOR</th>
			<th style="width: 39%;">ESTÁNDAR</th>
			<th class="text-center" style="width: 8%;">NIVEL</th>
			<th class="text-center" style="width: 8%;">META</th>
		</tr>
	</thead>
	<tbody>
		
		<?php // dimension ?>
		<?php foreach ($result as $val1 => $row1): ?>
		
		<tr nobr="true">
			<th rowspan="<?= $row1['estandares'] ?>" style="width: 20%;">
				<?= $row1['dime_codigo'] ?>. <?= $row1['dime_titulo'] ?>
			</th>
			
			<?php
				end($row1['data']);
				$last1 = key($row1['data']);
			?>
			
			<?php // factor ?>
			<?php foreach ($row1['data'] as $val2 => $row2): ?>

				<?php
					$rows = count($row2['data']);
					end($row2['data']);
					$last2 = key($row2['data']);
				?>
				
				<th rowspan="<?= $rows ?>" style="width: 25%;">
					<?= $row2['fact_codigo'] ?>. <?= $row2['fact_titulo'] ?>
				</th>

				<?php // estandar ?>
				<?php foreach ($row2['data'] as $val3 => $row3): ?>

					<td style="width: 39%;" id="esta-<?= $val3 ?>">
						<?= $row3['data']['esta'] ?>
					</td>
					<td class="text-center" style="width: 8%;">
						<?= $row3['data']['aude_aceptacion'] ?>
					</td>
					<td class="text-center" style="width: 8%;">
						<?= $row3['data']['esta_nivel_aceptacion'] ?>
					</td>
					
				</tr>
				
				<?php if ($val2 != $last1 OR $val3 != $last2): ?> <tr nobr="true"> <?php endif ?>
				
				<?php endforeach ?>
				
			<?php endforeach ?>
			
		<?php endforeach ?>

		<tr class="info success" nobr="true">
			<th class="_text-right" style="width: 20%;"><big>TOTAL</big></th>
			<th colspan="2" class="text-center" style="width: 64%;">
				<big>
				Estándares evaluados: <?= $response['total'] ?><br>
				Estándares cumplidos: <?= $response['cumplidos'] ?><br>
				Cumplimiento: <?= $response['per_cumplidos'] ?>%
				</big>
			</th>
			<th class="text-center" style="width: 8%;">
				<big><?= $response['puntaje_total'] ?></big>
			</th>
			<th class="text-center" style="width: 8%;">
				<big><?= $response['puntaje_meta'] ?></big>
			</th>
		</tr>
	</tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>
