<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php // dimension + factor + criterio ?>
<?php //debug($result); ?>

<table border="1" class="table table-bordered">
    <tbody>
        <tr style="background-color: #dff0d8;">
            <td colspan="2" style="width: 65%;">&nbsp;</td>
            <td style="width: 35%;">Estado</td>
        </tr>
        <?php $dm = 1 ?> 
                <?php foreach ($result as $val1 => $row1): ?>
                    <?php //debug($row1); ?>
                        <tr style="background-color: #dff0d8;">
                                    <td colspan="2" style="width: 65%;"><?= $row1['dime'] ?></td>
                                    <td style="width: 35%;"><?php //= $row1['dime_valo'] ?></td>
                                </tr>

                                <?php $ft = 1 ?>
                                <?php foreach ($row1['data'] as $val2 => $row2): ?>
                                    <tr style="background-color: #dff0d8;">
                                        <td colspan="2" style="width: 65%;"><?= $row2['fact'] ?></td>
                                        <td style="width: 35%;"><?php //= $row2['fact_valo'] ?></td>
                                    </tr>
                                    
                                    <!-- Cabecera de los standares y criterios -->
                                    <tr style="background-color: #E0FFFF;">        
                                        <td style="width: 25%; text-align: center;">ESTANDAR</td>
                                        <td style="width: 40%; text-align: center;">CRITERIOS A EVALUAR</td>
                                        <td style="width: 35%; text-align: center;">&nbsp;</td>
                                    </tr>
                                        
                                    <?php $st = 1 ?>
                                    <?php foreach ($row2['data'] as $val3 => $row3): ?>
                                    <?php //debug($row3); ?>
                                    <tr style="background-color: #FFFAF0;">
                                            <td id="esta-<?= $val3 ?>" style="width: 25%;"><?= '<b>'. $row3['data']['esta_titulo'] .':</b> '. $row3['data']['esta_descripcion']?></td>
                                            <td style="width: 40%;">
                                                <?php $ct = 1 ?>
                                                    <?php foreach ($oAutoevaluacion->get_criterios($row3['data']['aude_id']) as $criterio): ?>
                                                        <p><?= $ct ?>. <?= $criterio->aucr_descripcion ?></p>
                                                    <?php $ct++ ?>
                                                <?php endforeach ?>
                                            </td>
                                            <td style="width: 35%;"><?= $row3['data']['valo'] ?></td>
                                        </tr>
                                        
                                        <?php if ($row3['data']['aude_pregunta1'] > 0) { ?>
                                        <tr style="background-color: #A9A9A9;">
                                            <td style="width: 25%;">Nivel de cumplimiento del estándar</td>
                                            
                                            <?php if ($row3['data']['aude_justificacion'] != '') { ?>
                                                <td style="width: 40%;">Actividades que faltan o sobran</td>
                                                <td style="width: 35%;">Impacto esperado en el cumplimiento del estándar</td>
                                            <?php } else { ?>
                                                <td colspan="2" style="width: 75%;">Impacto esperado en el cumplimiento del estándar</td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td style="width: 25%; text-align: center;"><?= $row3['data']['aude_pregunta1'] ?></td>
                                            
                                            <?php if ($row3['data']['aude_justificacion'] != '') { ?>
                                                <td style="width: 40%;"><?= $row3['data']['aude_justificacion'] ?></td>
                                                <td style="width: 35%;"><?= $row3['data']['aude_comentario'] ?></td>
                                            <?php } else { ?>
                                                <td colspan="2" style="width: 75%;"><?= $row3['data']['aude_comentario'] ?></td>
                                            <?php } ?>

                                        </tr>
                                        <?php } ?>
                                        
                                        <?php $st++ ?>
                                    <?php endforeach ?>

                                    <?php $ft++ ?>
                                <?php endforeach ?>

                                <?php $dm++ ?> 

                <?php endforeach ?>
    </tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>
