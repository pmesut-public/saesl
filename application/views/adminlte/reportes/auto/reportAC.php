<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php // dimension + factor + criterio ?>
<?php //debug($result); ?>

<table border="1" class="table table-bordered" style="background: #FFFFFF;">
    <tbody>
		<?php // debug($result) ?>
		<?php $dm = 1 ?> 
		<?php foreach ($result as $val1 => $row1): ?>
			<?php //debug($row1); ?>
			<tr style="background-color: #dff0d8;">
				<td style="width: 15%;">Dimensión <?= $dm ?></td>
				<td colspan="6" style="width: 85%;"><?= $row1['dime'] ?></td>
			</tr>

			<?php $ft = 1 ?>
			<?php foreach ($row1['data'] as $val2 => $row2): ?>
				<tr style="background-color: #ecf3dc;">
					<td style="width: 15%;">&raquo; Factor <?= $ft ?></td>
					<td colspan="6" style="width: 85%;"><?= $row2['fact'] ?></td>
				</tr>
				<tr style="background: #F9F9F9;">        
					<td colspan="2" style="width: 30%; text-align: center;vertical-align: middle;">Estandar</td>
					<td style="width: 10%; text-align: center;vertical-align: middle;">Tipo</td>
					<td style="width: 10%; text-align: center;vertical-align: middle;">N°</td>
					<td style="width: 30%; text-align: center;vertical-align: middle;">Detalle</td>
					<td style="width: 10%; text-align: center;vertical-align: middle;">Nivel de importancia para  el cumplimiento del Factor</td>
					<td style="width: 10%; text-align: center;vertical-align: middle;">Nivel de Avance en el logro de la Actividad (%)</td>
				</tr>
				<?php $st = 1 ?>
				<?php foreach ($row2['data'] as $val3 => $row3): ?>
					<?php $aActividades = Model_AutoevaluacionDetalle::get_autoactividad_by_estandar($row3['data']['aude_id']) ?>
					<?php $aActividadeRelacionada = Model_AutoevaluacionDetalle::get_autoactividad_relacionada($row3['data']['aude_id']) ?>
					<?php $num_actividades = count($aActividades) + count($aActividadeRelacionada) ?>
					<tr>
						<td colspan="2" rowspan="<?= $num_actividades + 1 ?>" style="width: 30%;"><?= $row3['data']['esta_titulo'] ?></td>
						<?php if ($num_actividades == 0): ?>
						<td colspan="5" style="text-align: center; width: 70%;">Sin actividades</td>
						<?php endif ?>
					</tr>
					<?php foreach ($aActividades as $oActividades): ?>
						<tr>
							<td style="width: 10%; text-align: center;vertical-align: middle;">D</td>
							<td style="width: 10%; text-align: center;vertical-align: middle;"><?= $oActividades->auac_numeracion ?></td>
							<td style="width: 30%; vertical-align: middle;"><?= $oActividades->auac_titulo ?></td>
							<td style="width: 10%; text-align: center;vertical-align: middle;"><?= $oActividades->auac_importancia_factor ?>%</td>
							<td style="width: 10%; text-align: center;vertical-align: middle;"><?= $oActividades->auac_avance ?>%</td>
						</tr>
					<?php endforeach; ?>

					<?php foreach ($aActividadeRelacionada as $oActividadeRelacionada): ?>
						<tr>
							<td style="width: 10%; text-align: center;vertical-align: middle;"><?= $oActividadeRelacionada->get_tipo_relación() ?></td>
							<td style="width: 10%; text-align: center;vertical-align: middle;"><?= $oActividadeRelacionada->auac_numeracion ?></td>
							<td style="width: 30%;"></td>
							<td style="width: 10%;"></td>
							<td style="width: 10%;"></td>
						</tr>
					<?php endforeach; ?>

					<?php $st++ ?>
				<?php endforeach ?>

				<?php $ft++ ?>
			<?php endforeach ?>

			<?php $dm++ ?> 

		<?php endforeach ?>
    </tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>
