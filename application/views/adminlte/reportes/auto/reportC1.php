<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php // dimension + factor + criterio ?>
<?php //debug($result); ?>

<table border="1" class="table table-bordered">
    <tbody>
        <tr style="background-color: #dff0d8;">
            <td colspan="3" style="width: 55%;">&nbsp;</td>
            <td colspan="2" style="width: 45%;">Nivel de Avance</td>
        </tr>
        <?php $dm = 1 ?> 
                <?php foreach ($result as $val1 => $row1): ?>
                    <?php //debug($row1); ?>
                        <tr style="background-color: #dff0d8;">
                                    <td style="width: 10%;">Dimensión <?= $dm ?></td>
                                    <td colspan="2" style="width: 45%;"><?= $row1['dime'] ?></td>
                                    <td colspan="2" style="width: 45%;"><?= $row1['dime_valo'] ?></td>
                                </tr>

                                <?php $ft = 1 ?>
                                <?php foreach ($row1['data'] as $val2 => $row2): ?>
                                    <tr style="background-color: #dff0d8;">
                                        <td style="width: 10%;">&raquo; Factor <?= $ft ?></td>
                                        <td colspan="2" style="width: 45%;"><?= $row2['fact'] ?></td>
                                        <td colspan="2" style="width: 45%;"><?= $row2['fact_valo'] ?></td>
                                    </tr>

                                    <?php $st = 1 ?>
                                    <?php foreach ($row2['data'] as $val3 => $row3): ?>
                                    <?php //debug($row3); ?>
                                    <tr style="background-color: #ecf3dc;">
                                            <td id="esta-<?= $val3 ?>" style="width: 10%;">&raquo; &raquo; Estándar <?= $st ?></td>
                                            <td colspan="2" style="width: 45%;"><?= '<b>'. $row3['data']['esta_titulo'] .':</b> '. $row3['data']['esta_descripcion']?></td>
                                            <td colspan="2" style="width: 45%;"><?= $row3['data']['valo'] ?></td>
                                        </tr>
                                        <tr>        
                                            <td colspan="2" style="width: 25%;">Criterios:</td>
                                            <td style="width: 50%;">Justificación:</td>
                                            <td style="width: 13%;">Evidencias:</td>
                                            <td style="width: 12%;">Comentarios:</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="width: 25%;">
                                                        <table border="1" class="table table-bordered">
                                                            <?php $ct = 1 ?>
                                                            <?php foreach ($oAutoevaluacion->get_criterios($row3['data']['aude_id']) as $criterio): ?>
                                                                <tr>
                                                                    <td style="width: 10%;"><?= $ct ?></td>
                                                                    <td style="width: 90%;"><?= $criterio->aucr_descripcion ?></td>
                                                                </tr>
                                                                <?php $ct++ ?>
                                                            <?php endforeach ?>
                                                        </table>
                                                    </td>
                                                    <td style="width: 50%;">
                                                        <table border="1" class="table table-bordered">
                                                            <tr>
                                                                <td style="width: 100%;"><?= $row3['data']['aude_justificacion'] ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 13%;">
                                                        <table border="1" class="table table-bordered">
                                                            <?php $ev = 1 ?>
                                                            <?php foreach ($oAutoevaluacion->get_evidencias($row3['data']['aude_id']) as $evidencia): ?>
                                                                <tr>
                                                                    <td style="width: 10%;"><?= $ev ?></td>
                                                                    <td style="width: 90%;"><?= $evidencia->auev_titulo ?></td>
                                                                </tr>
                                                                <?php $ev++ ?>
                                                            <?php endforeach ?>
                                                        </table>
                                                    </td>
                                                    <td style="width: 12%;">
                                                        <table border="1" class="table table-bordered">
                                                            <tr>
                                                                <td style="width: 100%;"><?= $row3['data']['aude_comentario'] ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                        <?php $st++ ?>
                                    <?php endforeach ?>

                                    <?php $ft++ ?>
                                <?php endforeach ?>

                                <?php $dm++ ?> 

                <?php endforeach ?>
    </tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>
