<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<?php //debug($result); die; ?>

<table border="1" class="table table-bordered">
	<thead>
		<tr class="success" nobr="true">
			<th style="width: 23%;">DIMENSIÓN</th>
			<th class="text-center" style="width: 8%;">% AVANCE</th>
			<th style="width: 23%;">FACTOR</th>
			<th class="text-center" style="width: 8%;">% AVANCE</th>
			<th style="width: 30%;">ESTANDAR</th>
			<th class="text-center" style="width: 8%;">% AVANCE</th>
		</tr>
	</thead>

	<tbody>
		
		<?php foreach ($result as $val1 => $row1): ?>
		
		<tr nobr="true">

			<?php
				$rows = count($row1['data']);
				end($row1['data']);
				$last = key($row1['data']);
			?>
			
			<th rowspan="<?= $rows ?>">
				<?= $row1['dime'] ?>
			</th>
			
			<?php // factor/estandar?>
			<?php foreach ($row1['data'] as $val2 => $row2): ?>
			
				<td id="esta-<?= $val2 ?>">
					% avance
				</td>
				<td>
					<?= $row2['data']['fact'] ?>
				</td>
				<td class="text-center">
					% avance
				</td>
				<td>
					<?= $row2['data']['esta'] ?>
				</td>
				<td class="text-center">
					% avance
				</td>
			</tr>
			
			<?php if ($val2 != $last): ?><tr nobr="true"><?php endif ?>
			
			<?php endforeach ?>
		
			<!--		
			<tr class="_success text-primary" nobr="true">
				<th colspan="3" class="text-right" style="width: 76%;">
					Cumplimiento por Dimensión</th>
				<th class="text-center" style="width: 8%;">
					<?= 'dime_cump' ?> de <?= 'dime_total' ?></th>
				<th class="text-center" style="width: 8%;">
					<?= 'dime_cump'?>%</th>
				<th class="text-center" style="width: 8%;">
					<?= 'dime_puntaje' ?></th>
			</tr> -->
			
		<?php endforeach ?>
		
		<!-- <tr class="info success" nobr="true">
			<th colspan="3" class="text-right" style="width: 76%;">
				<big>TOTAL CUMPLIMIENTO</big></th>
			<th class="text-center" style="width: 8%;">
				<big><?= 'cumplidos' ?> de <?= 'total' ?></big></th>
			<th class="text-center" style="width: 8%;">
				<big><?= 'per_cumplidos' ?>%</big></th>
			<th class="text-center" style="width: 8%;">
				<big><?= 'puntaje_cumplidos' ?></big></th>
		</tr> -->
		
	</tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>
