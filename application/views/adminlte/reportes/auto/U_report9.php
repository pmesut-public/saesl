<?= Theme_View::factory('reportes/auto/header', get_defined_vars()) ?>

<table border="1" class="table table-bordered" nobr="true">
	<thead>
		<tr class="success _small">
			<th class="text-center" colspan="3" style="width: 60%;">Modelo ICACIT</th>
			<th class="text-center" colspan="2" style="width: 20%;">Modelo SINEACE</th>
			<th class="text-center" colspan="2" style="width: 20%;">RESULTADOS</th>
		</tr>
		<tr class="text-center success _small">
			<th class="text-center" colspan="1" style="width: 6%;">Criterios</th>
			<th class="text-center" colspan="2" style="width: 54%;">REQUISITOS</th>
			<th class="text-center" colspan="1" style="width: 10%;">ESTANDARES CON RELACION DIRECTA</th>
			<th class="text-center" colspan="1" style="width: 10%;">ESTANDARES CON RELACION INDIRECTA</th>
			<th class="text-center" colspan="1" style="width: 10%;">ESTANDARES CUMPLIDOS</th>
			<th class="text-center" colspan="1" style="width: 10%;">ESTANDARES NO CUMPLIDOS</th>
		</tr>
	</thead>

	<tbody>
	<?php //debug($result); ?>
	<?php foreach ($aCriterio as $val1 => $row1): ?>
		<tr nobr="true">

			<?php 
				$rows = count($row1['data']);
				end($row1['data']);
				$last = key($row1['data']);
			?>

			<td rowspan="<?= $rows ?>" style="width: 6%;">
				<?= 'Criterio '.$row1['iccr_codigo'].' '.$row1['iccr_titulo'] ?></td>

			<?php foreach ($row1['data'] as $val2 => $row2): ?>
				<?php $oRequisito = $row2['data'] ?>
				<td style="width: 4%;"><?= $oRequisito['icre_codigo'] ?></td>
				<td style="width: 50%;"><?= $oRequisito['icre_descripcion'] ?></td>

				<?php if($aRequisitoIcacit[$val2]['tipo'] != 'SV'): ?>
					<td class="text-center" style="width: 10%;"><?= $oRequisito['icre_relacion_directa'] ?></td>
					<td class="text-center" style="width: 10%;"><?= $oRequisito['icre_relacion_indirecta'] ?></td>
					<td class="text-center" style="width: 10%;">
						<?= implode(', ', $aRequisitoIcacit[$val2]['cumplen']) ?>
					</td>
					<td class="text-center" style="width: 10%;">
						<?= implode(', ', $aRequisitoIcacit[$val2]['no_cumplen']) ?>
					</td>
				<?php else : ?>
					<td class="text-center " colspan="2" style="width: 20%;">SIN VINCULACION</td>
					<td class="text-center " colspan="2" style="width: 20%;"></td>
				<?php endif; ?> 

			</tr>
			
			<?php if ($val2 != $last): ?> <tr nobr="true"> <?php endif ?>

			<?php endforeach ?>

	<?php endforeach ?>

	</tbody>
</table>

<table border="1" class="table table-bordered" nobr="true">
	<tbody>
		<tr class="info">
			<th colspan="5" class="success" style="width: 80%;">
				<?php $resumen = $oAutoevaluacion->get_resumen_incacit($aRequisitoIcacit); //debug($resumen); ?>
				<p> Resultado: 
				</p>
				<p>
					Se cumplieron <?= count($resumen['cumplidos']) ?> 
					de los <?= count($resumen['estandares']) ?> estándares (<?= round(count($resumen['cumplidos']) * 100 /  (count($resumen['estandares']) ?: 1)) ?> %) 
					que vinculan el modelo ICACIT al del SINEACE.
				</p>
				<p>
					De los <?= $resumen['vinculados'] ?> de <?= count($aRequisitoIcacit) ?> REQUISITOS (<?= round($resumen['vinculados'] * 100 / (count($aRequisitoIcacit) ?: 1)) ?> %)
					que vinculan estos modelos, se han cubierto: 
					<?php $resumen_vinculados = $resumen['vinculados'] ?: 1; ?>
					<?= $resumen['CT'] ?> (<?= round($resumen['CT'] * 100 / $resumen_vinculados) ?> %) TOTALMENTE, 
					<?= $resumen['CP'] ?> (<?= round($resumen['CP'] * 100 / $resumen_vinculados) ?> %) PARCIALMENTE y 
					<?= $resumen['NC'] ?> (<?= round($resumen['NC'] * 100 / $resumen_vinculados) ?> %) en ninguna medida.
				</p>
			</th>
		</tr>
	</tbody>
</table>

<?= Theme_View::factory('reportes/auto/footer') ?>