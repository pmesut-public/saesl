<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>
	
	<?php /*if ($theme->screen): ?>
	<div class="list-buttons">
		<?php foreach ($buttons as $button): ?>
			<?= $button ?>
		<?php endforeach ?>
	</div>
	<?php endif*/ ?>
	
	<!-- Main content -->
	
	<section class="content">
		
		<div class="row">
			<div class="col-sm-6">
				
				<div class="box">

					<div class="box-body box-responsive">
						<div class="pull-right _list-buttons">
							<?php foreach ($fn_buttons('comites_region') as $button): ?>
								<?= $button ?>
							<?php endforeach ?>
						</div>
						
						<?= $comites_region ?>

					</div><!-- /.box-body -->

				</div><!-- /.box -->
				
			</div>
			
			<div class="col-sm-6">
				
				<div class="box">
					
					<div class="box-body">
						<div class="pull-right _list-buttons">
							<?php foreach ($fn_buttons('summary_comites') as $button): ?>
								<?= $button ?>
							<?php endforeach ?>
						</div>
						
						<?= $summary_comites ?>
						
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-6">
				
				<div class="box">
					<div class="box-body">
						
						<h4>Resumen Autoevaluaciones</h4>
						
						<table class="table">
							<tbody>
								<?php foreach (current($total_autoevaluaciones) as $key => $val): ?>
								<tr>
									<th><?= $key ?></th>
									<td class="text-right"><?= $val ?></td>
								</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-6">
				
				<div class="box">
					<div class="box-body">
						<div class="pull-right _list-buttons">
							<?php foreach ($fn_buttons('auto_completas') as $button): ?>
								<?= $button ?>
							<?php endforeach ?>
						</div>
						
						<?= $auto_completas ?>
						
					</div>
				</div>
				
			</div>
			
			<div class="col-sm-6">
				
				<div class="box">
					<div class="box-body">
						<div class="pull-right _list-buttons">
							<?php foreach ($fn_buttons('auto_percentages') as $button): ?>
								<?= $button ?>
							<?php endforeach ?>
						</div>
						
						<?php //= $auto_percentages ?>
						
					</div>
				</div>
				
			</div>
		</div>

	</section><!-- /.content -->

	
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
