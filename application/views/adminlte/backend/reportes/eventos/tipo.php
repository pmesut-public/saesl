<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<!-- Main content -->
	<section class="content">

		<?php foreach ($eventos as $title => $evento): ?>
		<div class="box">
			<!--div class="box-header">
				<h3 class="box-title"><?= $title ?></h3>
			</div-->
			<div class="box-body">

				<?= $evento ?>

			</div><!-- /.box-body -->

		</div><!-- /.box -->
		<?php endforeach ?>

	</section><!-- /.content -->

	
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
