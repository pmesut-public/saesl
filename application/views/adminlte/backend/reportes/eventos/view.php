<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>
	
	<!--div class="list-buttons">
		<?php /*foreach ($table->buttons as $button): ?>
			<?= $button ?>
		<?php endforeach*/ ?>
	</div-->
	
	<!-- Main content -->
	<section class="content">

		<div class="box">
			<!--div class="box-header pagination-top">
				<div class="pagination-wrapper clearfix">
					<div class="pagination-info pull-left">
						Total: < ?= $table->pagination->total_items ? >
					</div><!-- /.box-header -- >
				</div>
			</div-->
			<div class="box-body box-responsive">

				<div class="pull-right _list-buttons">
					<?php foreach ($buttons as $button): ?>
					<?= $button ?>
					<?php endforeach ?>
				</div>
				
				<?= $autos ?>

			</div><!-- /.box-body -->

		</div><!-- /.box -->

	</section><!-- /.content -->

	
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
