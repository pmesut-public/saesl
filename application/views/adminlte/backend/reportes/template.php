<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>
	
	<div class="list-buttons">
		<?php foreach ($buttons as $button): ?>
			<?= $button ?>
		<?php endforeach ?>
	</div>
	
	<!-- Main content -->
	<section class="content">

		<div class="box">
			
			<div class="box-body box-responsive">

				<?= $content ?>

			</div><!-- /.box-body -->

		</div><!-- /.box -->

	</section><!-- /.content -->

	
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
