<style>
	.rotate-45 {
		height: 100px;
	}
	.rotate-45 > div {
		width: 30px;
		transform: rotate(270deg);
	}
</style>

<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>
	
	<?php if ($theme->screen): ?>
	<div class="list-buttons">
		<?php foreach ($buttons as $button): ?>
			<?= $button ?>
		<?php endforeach ?>
	</div>
	<?php endif ?>
	
	<!-- Main content -->
	
	<section class="content">
		
		<div class="box box-solid">
			
			<?php if ($theme->screen): ?>
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<?php foreach ($aObjeto as $id => $title): ?>
					<li role="presentation" class="<?php //= ($id == $obac_id) ? 'active' : NULL ?>">
						<a href="/admin/reportes/factores/institutos?_obac_id=<?= $id ?>"><?= $title ?></a></li>
					<?php endforeach ?>
				</ul>
			</div>
			<?php endif ?>
			
			<div class="box-body box-responsive">

				<?php //= $factores ?>

			</div><!-- /.box-body -->

		</div><!-- /.box -->

	</section><!-- /.content -->

	
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
