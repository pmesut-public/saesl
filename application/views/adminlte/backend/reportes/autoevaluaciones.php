<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>
	
	<div class="list-buttons">
		<?php foreach ($buttons as $button): ?>
			<?= $button ?>
		<?php endforeach ?>
	</div>
	
	<!-- Main content -->
	<section class="content">

		<div class="box box-solid">
			
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<?php foreach (Model_ObjetoAcreditacion::$tipos as $id => $title): ?>
					<li role="presentation" class="<?= ($id == $tiin_id) ? 'active' : NULL ?>">
						<a href="?_tiin_id=<?= $id ?>"><?= $title ?></a></li>
					<?php endforeach ?>
				</ul>
			</div>
			
			<div class="box-body box-responsive">

				Total: <?= count($content->result) ?>
				<?= $content ?>

			</div><!-- /.box-body -->

		</div><!-- /.box -->

	</section><!-- /.content -->

	
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
