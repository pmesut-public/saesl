<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>
	
	<?php if ( ! $print): ?>
	<div class="list-buttons">
		<?php foreach ($buttons as $button): ?>
			<?= $button ?>
		<?php endforeach ?>
	</div>
	<?php endif ?>
	
	<!-- Main content -->
	<section class="content">

		<?php if ( ! $print): ?>
		<div class="row">
			<div class="col-sm-5 col-sm-offset-7">
				<?= $filter_view ?>
			</div>
		</div>
		<?php endif ?>
	
		<div class="box">
			
			<div class="box-header pagination-top">
				<div class="pagination-wrapper clearfix">
					<div class="pagination-info pull-left">
						Total: <?= $count ?>
					</div>
				</div>
			</div>
			
			<div class="box-body box-responsive">

				<table class="table table-bordered table-hover table-eventos" border="1">
					<thead>
						<tr class="success">
							<?php foreach ($labels as $label): ?>
								<?= HTML::tag('th', $label) ?>
							<?php endforeach ?>
						</tr>
					</thead>

					<tbody>
						
						<?php foreach ($data as $row): ?>
						<tr>
							<?php foreach ($labels as $column => $label): ?>
								<?= HTML::tag('td', $row[$column]) ?>
							<?php endforeach ?>
						</tr>
						<?php endforeach ?>
						
					</tbody>
				</table>

			</div><!-- /.box-body -->

		</div><!-- /.box -->

	</section><!-- /.content -->

	
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
