<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<!--div class="list-buttons">
		<a href="<?= URL::query(array('print' => 1, 'pdf' => 1)) ?>" class="btn btn-flat btn-default btn-pdf" target="_blank">
			<i class="fa fa-download"></i> PDF</a>
		<a href="<?= URL::query(array('print' => 1, 'csv' => 1)) ?>" class="btn btn-flat btn-default btn-excel">
			<i class="fa fa-file-text-o"></i> CSV</a>
		<a href="<?= URL::query(array('print' => 1, 'excel' => 1)) ?>" class="btn btn-flat btn-default btn-excel">
			<i class="fa fa-file-text"></i> Excel</a>
		<a href="<?= URL::query(array('print' => 1)) ?>" class="btn btn-flat btn-default btn-print">
			<i class="fa fa-print"></i> Imprimir</a>
	</div-->
	
	<!-- Main content -->
	<?php //$table ?>
	<section class="content">

		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Detalles de contacto</h4>
			</div>

			<div class="box-body">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-2 control-label">Nombres</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->pers_nombres ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Apellidos</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->pers_apellidos ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Número</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->pers_telefono ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->pers_correo ?></p>
						</div>
					</div>
				</form>
			</div>

		</div>
		
		<div class="box">
			<div class="box-header pagination-top">
				<?= $table->pagination ?>
			</div><!-- /.box-header -->
			
			<div class="box-body box-responsive">
				
				<table class="table table-bordered table-hover _dataTable">
					<thead>
						<tr>
							<?php foreach ($table->thead as $th): ?>
								<?php echo $th ?>
							<?php endforeach ?>
						</tr>
					</thead>

					<tbody>

						<?php foreach ($table->tbody as $id => $row): ?>
							<tr>
								<?php foreach ($row as $td): ?>
									<?php echo $td ?>
								<?php endforeach ?>
							</tr>
						<?php endforeach ?>

					</tbody>
				</table>
				
			</div><!-- /.box-body -->
			
			<div class="box-footer pagination-bottom">
				<?= $table->pagination ?>
			</div>
			
		</div><!-- /.box -->

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
