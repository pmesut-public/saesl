<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<div class="list-buttons">
		<a href="<?= URL::query(array('print' => 1, 'pdf' => 1)) ?>" class="btn btn-flat btn-default btn-pdf" target="_blank">
			<i class="fa fa-download"></i> PDF</a>
		<a href="<?= URL::query(array('print' => 1, 'csv' => 1)) ?>" class="btn btn-flat btn-default btn-excel">
			<i class="fa fa-file-text-o"></i> CSV</a>
		<a href="<?= URL::query(array('print' => 1, 'excel' => 1)) ?>" class="btn btn-flat btn-default btn-excel">
			<i class="fa fa-file-text"></i> Excel</a>
		<a href="<?= URL::query(array('print' => 1)) ?>" class="btn btn-flat btn-default btn-print">
			<i class="fa fa-print"></i> Imprimir</a>
	</div>
	
	<!-- Main content -->
	<?php //$table ?>
	<section class="content">

		<div class="row">
			<div class="col-sm-7">
				<?= $table->searches ?>
			</div>
			<div class="col-sm-5">
				<?= $table->filters ?>
			</div>
		</div>
		
		<div class="box">
			<div class="box-header pagination-top">
				<?= $table->pagination ?>
			</div><!-- /.box-header -->
			
			<div class="box-body box-responsive">
				
				<table class="table table-bordered table-hover _dataTable">
					<thead>
						<tr>
							<?php foreach ($table->thead as $th): ?>
								<?php echo $th ?>
							<?php endforeach ?>
							
							<th class="th-actions"></th>
							
						</tr>
					</thead>

					<tbody>

						<?php foreach ($table->tbody as $id => $row): ?>
							<tr>
								<?php foreach ($row as $td): ?>
									<?php echo $td ?>
								<?php endforeach ?>
								
								<td class="td-actions">
									
									<!--a href="/admin/acreditado/view/<?= $id ?>" 
									   class="btn btn-sm btn-flat btn-default" data-title="Ver Detalle">
										<i class="fa fa-list-ul"></i></a-->
									
									<?php if (ACL::instance()->allowed('acreditadomensaje', 'view')): ?>
									<a href="/admin/acreditadomensaje/view/<?= $id ?>" 
									   class="btn btn-sm btn-flat btn-facebook" data-title="Ver mensajes">
										<i class="fa fa-search-plus"></i></a>
									<?php endif ?>
									
								</td>
								
							</tr>
						<?php endforeach ?>

					</tbody>
				</table>
				
			</div><!-- /.box-body -->
			
			<div class="box-footer pagination-bottom">
				<?= $table->pagination ?>
			</div>
			
		</div><!-- /.box -->

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
