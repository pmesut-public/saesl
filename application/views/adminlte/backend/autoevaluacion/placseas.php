<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<div class="list-buttons">
		<a href="<?= URL::query(array('print' => 1, 'excel' => 1)) ?>" class="btn btn-flat btn-default btn-excel">
			<i class="fa fa-file-text"></i> Excel</a>
		<a href="<?= URL::query(array('print' => 1)) ?>" class="btn btn-flat btn-default btn-print">
			<i class="fa fa-print"></i> Imprimir</a>
	</div>
	
	<!-- Main content -->
	<?php //$table ?>
	<section class="content">

		<div class="row">
			<div class="col-sm-7">
				<?= $table->searches ?>
			</div>
			<div class="col-sm-5">
				<?= $table->filters ?>
			</div>
		</div>
		
		<div class="box">
			<div class="box-header pagination-top">
				<?= $table->pagination ?>
			</div><!-- /.box-header -->
			
			<div class="box-body box-responsive">
				
				<table class="table table-bordered table-hover table-striped table-autos">
					<thead>
						<tr>
							<th><input class="input-all" type="checkbox"></th>
							
							<?php foreach ($table->thead as $th): ?>
								<?php echo $th ?>
							<?php endforeach ?>
							
							<th>Foo</th>
							<th class="th-actions"></th>
							
						</tr>
					</thead>

					<tbody>

						<?php foreach ($table->tbody as $id => $row): ?>
							<tr>
								<td>
									<!-- <?= $id ?> -->
									<input class="input-autoevaluacion" type="checkbox" data-id="<?= $table->data[$id]->id ?>">
								</td>
								
								<?php foreach ($row as $td): ?>
									<?php echo $td ?>
								<?php endforeach ?>
								
								<td>
									<?php
										$assessment = $table->data[$id];
										
										$proxy = new PlacseasProxy($assessment);
										$reportes = $proxy->get_reportes();
										$reportes_placseas = $proxy->get_reportes_placseas();
										//debug($reportes_placseas);
										//debug($reportes);
									?>
									
									<?php foreach ($reportes as $report => $params): ?>
										
										<a href="#"><?= $params['short'] ?></a>
										<?= Arr::get($reportes_placseas, $report) ? 'Sí' : 'No' ?>
											
									<?php endforeach ?>
								</td>
								
								<td class="td-actions">
									
									<?php if (ACL::instance()->allowed('acreditado', 'view')): ?>
									<a href="/admin/acreditado/view/<?= $table->data[$id]->id ?>" 
										class="btn btn-sm btn-flat btn-success" data-title="Ver detalle">
										<i class="fa fa-list-ul"></i></a>
									<?php endif ?>
									
									<?php if (ACL::instance()->allowed('autoevaluacion', 'generate_reports')): ?>
									<a href="/admin/autoevaluacion/generate_reports/<?= $id ?>" class="btn btn-sm btn-flat btn-dropbox"
										title="Generar reportes" data-confirm>
										<i class="fa fa-bolt"></i></a>
									<?php endif ?>
									
									<?php if (ACL::instance()->allowed('autoevaluacion', 'delete')): ?>
									<a href="/admin/autoevaluacion/delete/<?= $id ?>" class="btn btn-sm btn-flat btn-danger" 
										title="Eliminar autoevaluación (También del PLACSEAS)" data-confirm>
										<i class="fa fa-trash-o"></i></a>
									<?php endif ?>
									
								</td>
								
							</tr>
						<?php endforeach ?>

					</tbody>
				</table>
				
			</div><!-- /.box-body -->
			
			<div class="box-footer pagination-bottom">
				<?= $table->pagination ?>
			</div>
			
		</div><!-- /.box -->

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
