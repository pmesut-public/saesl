<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<div class="list-buttons">
		
		<a href="<?= URL::query(array('print' => 1, 'excel' => 1)) ?>" class="btn btn-flat btn-default btn-excel">
			<i class="fa fa-file-text"></i> Excel</a>
		<a href="<?= URL::query(array('print' => 1)) ?>" class="btn btn-flat btn-default btn-print">
			<i class="fa fa-print"></i> Imprimir</a>
	</div>
	
	<!-- Main content -->
	<?php //= $table ?>
	<section class="content">

		<div class="row">
			<div class="col-sm-5">
				<?= $table->searches ?>
			</div>
			<div class="col-sm-7">
				<?= $table->filters ?>
				
				<div class="box box-default hidden-print">
					<div class="box-body">
						<div class="box-tools pull-right">
							<button class="btn btn-flat btn-default btn-xs" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
						
						<div class="form-group">
							<label>Fecha de cierre por rango:</label>
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" class="form-control pull-right" id="fecha_cierre" 
									data-start="<?= $start_date ?>" data-end="<?= $end_date ?>">
								<div class="input-group-btn">
									<button type="button" class="btn btn-default remove_filtro_fecha">
										<i class="fa fa-ban" title="Remover filtro"></i></button>
								</div>
							</div><!-- /.input group -->
						</div>
						
					</div>
				</div>
				
				<!--div class="box box-default hidden-print">
					<div class="box-body">
						<div class="box-tools pull-right">
							<button class="btn btn-flat btn-default btn-xs" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
						<?= Table::makefilter('extra', 'Ver últimos mensajes') ?>
					</div>
				</div-->
				
			</div>
		</div>
		
		<div class="box">
			<div class="box-header pagination-top">
				<?= $table->pagination ?>
			</div><!-- /.box-header -->
			
			<div class="box-body box-responsive">
				
				<table class="table table-bordered table-hover table-striped table-autos">
					<thead>
						<tr>
							<th><input class="input-all" type="checkbox"></th>
							
							<?php foreach ($table->thead as $th): ?>
								<?php echo $th ?>
							<?php endforeach ?>
							
							<th class="th-actions"></th>
							
						</tr>
					</thead>

					<tbody>

						<?php foreach ($table->tbody as $id => $row): ?>
							<tr>
								<td>
									<input class="input-autoevaluacion" type="checkbox" data-id="<?= $table->data[$id]->id ?>">
								</td>
								
								<?php foreach ($row as $td): ?>
									<?php echo $td ?>
								<?php endforeach ?>
								
								<td class="td-actions">
									
									<?php if (ACL::instance()->allowed('acreditado', 'force')): ?>
									<a href="/admin/acreditado/force/<?= $table->data[$id]->id ?>" 
										class="btn btn-sm btn-flat btn-info" data-title="Ver autoevaluaciones"><i class="fa fa-eye"></i></a>
									<?php endif ?>
									
									<?php if (ACL::instance()->allowed('frontend.reportes')): ?>
									<!-- <a href="/reportes/view/report1<?php //= URL::query(['id' => $id, 'acre_id' => $table->data[$id]->id]) ?>" 
										class="btn btn-sm btn-flat btn-info"
										title="Ver reportes"><i class="fa fa-file-text-o"></i></a> -->
									<?php endif ?>
									
									<?php  if (ACL::instance()->allowed('autoevaluacion', 'force_finish')
										AND $table->data[$id]->auto_estado == Model_Autoevaluacion::STATUS_ACTIVO): ?>
									<a href="/admin/autoevaluacion/force_finish/<?= $id ?>" class="btn btn-sm btn-flat btn-dropbox" 
										data-title="Cerrar autoevaluación"><i class="fa fa-bolt"></i></a>
									<?php endif ?>
								</td>
								
							</tr>
						<?php endforeach   ?>

					</tbody>
				</table>
				
			</div><!-- /.box-body -->
			
			<div class="box-footer pagination-bottom">
				<?= $table->pagination ?>
			</div>
			
		</div><!-- /.box -->

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
