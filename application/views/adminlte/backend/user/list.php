<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<div class="list-buttons">
		<?php if (ACL::instance()->allowed('user', 'new')): ?>
		<a href="/admin/user/new" class="btn btn-flat btn-primary">
			<i class="fa fa-plus"></i> Nuevo</a>
		<?php endif ?>
		
		<a href="<?= URL::query(array('print' => 1, 'excel' => 1)) ?>" class="btn btn-flat btn-default btn-excel">
			<i class="fa fa-file-text"></i> Excel</a>
		<a href="<?= URL::query(array('print' => 1)) ?>" class="btn btn-flat btn-default btn-print">
			<i class="fa fa-print"></i> Imprimir</a>
	</div>
	
	<!-- Main content -->
	<?php //$table ?>
	<section class="content">

		<div class="row">
			<div class="col-sm-7">
				<?= $table->searches ?>
			</div>
			<div class="col-sm-5">
				<?= $table->filters ?>
			</div>
		</div>
		
		<div class="box">
			<div class="box-header pagination-top">
				<?= $table->pagination ?>
			</div><!-- /.box-header -->
			
			<div class="box-body box-responsive">
				
				<table class="table table-bordered table-hover _dataTable table-middle">
					<thead>
						<tr>
							<?php foreach ($table->thead as $th): ?>
								<?php echo $th ?>
							<?php endforeach ?>
							
							<th class="th-actions"></th>
							
						</tr>
					</thead>

					<tbody>

						<?php foreach ($table->tbody as $id => $row): ?>
							<tr>
								<?php foreach ($row as $td): ?>
									<?php echo $td ?>
								<?php endforeach ?>
								
								<td class="td-actions">
									<?php if ($table->data[$id]->is_acreditado()): ?>
										<a href="/admin/acreditado/view/<?= $id ?>" class="btn btn-sm btn-flat btn-default" data-title="Ver"><i class="fa fa-list-ul"></i></a>
									<?php else: ?>
										<a href="/admin/user/edit/<?= $id ?>" class="btn btn-sm btn-flat btn-success" data-title="Editar"><i class="fa fa-pencil"></i></a>
										<a href="/admin/user/delete/<?= $id ?>" class="btn btn-sm btn-flat btn-danger" data-title="Eliminar"><i class="fa fa-trash-o"></i></a>
									<?php endif ?>
								</td>
								
							</tr>
						<?php endforeach ?>

					</tbody>
				</table>
				
			</div><!-- /.box-body -->
			
			<div class="box-footer pagination-bottom">
				<?= $table->pagination ?>
			</div>
			
		</div><!-- /.box -->

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
