<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Datos generales</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form role="form" class="form-horizontal" method="post">
				<div class="box-body">
					
					<?php if ($errors): ?>
					<div class="row">
						<div class="col-sm-6 col-sm-offset-2">
							<div class="callout callout-danger fade in text-left" role="alert">
								<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
								<h4>Error!</h4>
								
								<?php foreach ($errors as $error): ?>
								<p><?= $error ?></p>
								<?php endforeach ?>

							</div>
						</div>
					</div>
					<?php endif ?>
					
					<div class="form-group ">
						<label for="inst_nombre" class="col-sm-2 control-label">Username</label>
						<div class="col-sm-6">
							<p class="form-control-static"><?= $oUser->username ?></p>
						</div>
					</div>
					<div class="form-group ">
						<label for="inst_nombre" class="col-sm-2 control-label">Correo</label>
						<div class="col-sm-6">
							<p class="form-control-static"><?= $oUser->email ?></p>
						</div>
					</div>

					<div class="form-group ">
						<label for="old_password" class="col-sm-2 control-label">Contraseña antigua</label>
						<div class="col-sm-6">
							<input type="password" class="form-control" name="old_password" id="old_password" placeholder="Contraseña antigua">
						</div>
					</div>			
					<div class="form-group ">
						<label for="new_password" class="col-sm-2 control-label">Contraseña nueva</label>
						<div class="col-sm-6">
							<input type="password" class="form-control" name="new_password" id="new_password" placeholder="Contraseña nueva">
						</div>
					</div>			
					<div class="form-group ">
						<label for="confirm_password" class="col-sm-2 control-label">Confirmar</label>
						<div class="col-sm-6">
							<input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirmar">
						</div>
					</div>			

					<input type="hidden" name="referrer" value="http://saesdemo.procalidad.gob.pe/admin/institucion">

				</div><!-- /.box-body -->

				<div class="box-footer">
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-flat btn-primary">Guardar</button>
							<a href="http://saesdemo.procalidad.gob.pe/admin/institucion" class="btn btn-flat btn-default">Cancelar</a>
						</div>
					</div>
				</div>
			</form>
		</div>
		
	</section><!-- /.content -->
</aside><!-- /.right-side -->
