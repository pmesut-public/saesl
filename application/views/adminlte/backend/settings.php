<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1><?= $title ?> <small></small></h1>

		<?= Breadcrumb::build() ?>

	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Datos</h3>
			</div><!-- /.box-header -->
			
			<!-- form start -->
			<form role="form" class="form-horizontal" method="post">
				<div class="box-body">

					<div class="form-group">
						<label for="site_shortname" class="col-sm-2 control-label">Nombre corto del sitio</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="site_shortname" 
								id="site_shortname" value="<?= $config->site_shortname ?>" 
								placeholder="Admin email" required>
						</div>
					</div>
					<div class="form-group">
						<label for="site_title" class="col-sm-2 control-label">Nombre del sitio</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="site_title" 
								id="site_title" value="<?= $config->site_title ?>" 
								placeholder="Admin email" required>
						</div>
					</div>

					<div class="form-group">
						<label for="site_url" class="col-sm-2 control-label">URL del sitio</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="site_url" 
								id="site_url" value="<?= $config->site_url ?>" 
								placeholder="Admin email" required>
						</div>
					</div>
					
					<div class="form-group">
						<label for="admin_mail" class="col-sm-2 control-label">Email de recepcion de notificaciones</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="admin_mail" 
								id="admin_mail" value="<?= $config->admin_mail ?>" 
								placeholder="Admin email" required>
						</div>
					</div>

					<div class="form-group">
						<label for="noreply_mail" class="col-sm-2 control-label">Email de solo envío</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="noreply_mail" 
								id="noreply_mail" value="<?= $config->noreply_mail ?>" 
								placeholder="Admin email" required>
						</div>
					</div>

					<div class="form-group">
						<label for="mb_file_size" class="col-sm-2 control-label">Tamaño máximo de subida de archivo (MB)</label>
						<div class="col-sm-6">
							<input type="number" class="form-control" name="mb_file_size" 
								id="mb_file_size" value="<?= $config->mb_file_size ?>" 
								placeholder="Tamaño archivo MB" required>
						</div>
					</div>

					
				</div><!-- /.box-body -->

				<div class="box-footer">
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-flat btn-primary">Guardar</button>
						</div>
					</div>
				</div>
			</form>
			
		</div>

	</section><!-- /.content -->
</aside>
