<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<div class="list-buttons">
		<?php 
		/*
		 * Botónes pdf, csv, excel e imprimir	 
		 */
		?>
		<a href="<?= URL::query(array('print' => 1, 'pdf' => 1)) ?>" class="btn btn-flat btn-default btn-pdf" target="_blank">
			<i class="fa fa-download"></i> PDF</a>
		<a href="<?= URL::query(array('print' => 1, 'csv' => 1)) ?>" class="btn btn-flat btn-default btn-excel">
			<i class="fa fa-file-text-o"></i> CSV</a>
		<a href="<?= URL::query(array('print' => 1, 'excel' => 1)) ?>" class="btn btn-flat btn-default btn-excel">
			<i class="fa fa-file-text"></i> Excel</a>
		<a href="<?= URL::query(array('print' => 1)) ?>" class="btn btn-flat btn-default btn-print">
			<i class="fa fa-print"></i> Imprimir</a>
	</div>
	
	<!-- Main content -->
	<?php //$table ?>
	<section class="content">

		<div class="row">
			<?php 
				/*
				 * Buscador por instituto y por carrera
				 */
			?>
			<div class="col-sm-5">
				<?= $table->searches ?>
			</div>
			<div class="col-sm-7">
				<?= $table->filters ?>

				<div class="box box-default">
					<div class="box-body">
						<div class="box-tools pull-right">
							<button class="btn btn-default btn-flat btn-xs" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
						<!--<?= Table::makefilter('extra', 'Ver más campos') ?> -->
						<?= Table::makefilter('no_eval', 'Sin autoevaluaciones') ?>
					</div>
				</div>
				
			</div>
		</div>
		
		<?php /*if ($change_committee): ?>
			<h4>
				Solicitudes de cambio del comité de calidad
				<small><a href="/admin/acreditado"> Ver todos</a></small>
			</h4>
		<?php endif*/ ?>
		
		<div class="box">
			<div class="box-header pagination-top">
				<?= $table->pagination ?>
			</div><!-- /.box-header -->
			
			<div class="box-body box-responsive">
				
				<table class="table table-bordered table-hover _dataTable">
					<thead>
						<tr>
							<?php foreach ($table->thead as $th): ?>
								<?php echo $th ?>
							<?php endforeach ?>
							
							<th class="th-actions"></th>
							
						</tr>
					</thead>

					<tbody>

						<?php foreach ($table->tbody as $id => $row): ?>
							<tr>
								<?php foreach ($row as $td): ?>
									<?php echo $td ?>
								<?php endforeach ?>
								
								<td class="td-actions">
									
									<?php if (ACL::instance()->allowed('acreditado', 'force')): ?>
									<a href="/admin/acreditado/force/<?= $id ?>" 
										class="btn btn-sm btn-flat btn-info" data-title="Entrar como usuario del comité de calidad" 
										data-confirm="Esta acción forzará el acceso como el usuario. Usar con precaución. Continuar?">
										<i class="fa fa-user"></i></a>
									<?php endif ?>
									
									<?php if (ACL::instance()->allowed('acreditado', 'view')): ?>
									<a href="/admin/acreditado/view/<?= $id ?>" 
										class="btn btn-sm btn-flat btn-success" data-title="Ver comite de calidad">
										<i class="fa fa-group"></i></a>
									<?php endif ?>
									
									<?php if (ACL::instance()->allowed('autoevaluacion', 'index')): ?>
									<a href="/admin/autoevaluacion?filter__id=<?= $id ?>" 
										class="btn btn-sm btn-flat btn-default" data-title="Ver Autoevaluaciones">
										<i class="fa fa-list-ol"></i></a>
									<?php endif ?>
									
									<?php //if (ACL::instance()->allowed('frontend.eventos', 'index')): ?>
									<!-- <a href="/eventos?acre_id=<?php //= $id ?>" 
										class="btn btn-sm btn-flat btn-default" data-title="Ver eventos">
										<i class="fa fa-list-ol"></i></a> -->
									<?php //endif ?>
									
									<?php if (ACL::instance()->allowed('acreditado', 'delete')): ?>
									<a href="/admin/acreditado/delete/<?= $id ?>" 
										class="btn btn-sm btn-flat btn-danger" data-title="Eliminar" data-confirm>
										<i class="fa fa-trash-o"></i></a>
									<?php endif ?>
									
								</td>
								
							</tr>
						<?php endforeach ?>

					</tbody>
				</table>
				
			</div><!-- /.box-body -->
			
			<div class="box-footer pagination-bottom">
				<?= $table->pagination ?>
			</div>
			
		</div><!-- /.box -->

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
