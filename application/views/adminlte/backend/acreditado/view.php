<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Detalles</h4>
			</div>
			
			<div class="box-body">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-2 control-label">Objeto de Licenciamiento</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oObjetoAcreditacion->objeto_nombre() ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Modalidad</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oModalidad->moda_nombre ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Institución</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oInstitucion()->name ?></p>
						</div>
					</div>
                                    
					<?php //if ($oAcreditado->oCarrera()->loaded()): ?>
					<!--	<div class="form-group">
							<label class="col-sm-2 control-label">Carrera</label>
							<div class="col-sm-10">
								<p class="form-control-static"><?php //= $oAcreditado->oCarrera()->oCarrera->carr_nombre ?></p>
							</div>
						</div> -->
					<?php //endif ?>
                                    
					<div class="form-group">
						<label class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oContacto()->pers_correo ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Nombre de usuario</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oUser->username ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Fecha registro</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->acre_fecha_reg ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Documento de autorización del Comité</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<?php if ($oAcreditado->documento()): ?>
									<a href="/file/<?= $oAcreditado->documento() ?>" target="_blank">Ver documento</a>
								<?php else: ?>
									--
								<?php endif ?>
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Situación</label>
						<div class="col-sm-10">
							<?php if (ACL::instance()->allowed('acreditado', 'activate') 
								AND $oAcreditado->tipo == Model_Acreditado::ESTADO_REGISTRADO): ?>
								<a href="/admin/acreditado/activate/<?= $oAcreditado->acre_id ?>" class="btn btn-success">Aprobar</a>
							<?php else: ?>
								<p class="form-control-static"><span class="label label-default">
									<?= $oAcreditado->tipo() ?></span></p>
							<?php endif ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Usuario de prueba</label>
						<div class="col-sm-10">
							<?php if (ACL::instance()->allowed('acreditado', 'mark_test') 
								AND ! $oAcreditado->acre_test): ?>
								<a href="/admin/acreditado/mark_test/<?= $oAcreditado->acre_id ?>" class="btn btn-info">Marcar</a>
							<?php else: ?>
								<p class="form-control-static"><span class="label label-info">
									<?= $oAcreditado->acre_test ? 'Sí' : 'No' ?>
									</span></p>
							<?php endif ?>
							<?php //else: ?>
								<!--p class="form-control-static"><span class="label label-info">Sí</span></p-->
							<?php //endif ?>
						</div>
					</div>
				</form>
			</div>
			
		</div><!-- /.box -->
		
		<!--<div class="box">
			<div class="box-header">
				<h4 class="box-title">Eventos</h4>
				
				<div class="pull-right box-tools">
					
					<?php //if (ACL::instance()->allowed('frontend.eventos', 'index')): ?>
					<a class="btn btn-flat btn-default" title="Ver eventos" 
						href="/eventos?acre_id=<?= $oAcreditado->acre_id ?>">
						<i class="fa fa-list-ol"></i> Ver eventos</a>
					<?php //endif ?>
						
					<?php if (ACL::instance()->allowed('acreditado', 'add_seguimiento') AND $aSeguimiento): ?>
					<div class="btn-group">
						<button type="button" class="btn btn-flat btn-default dropdown-toggle" 
							data-toggle="dropdown" aria-expanded="false">
							Seleccionar <span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<?php foreach ($aSeguimiento as $key => $val): ?>
							<li><a href="/admin/acreditado/add_seguimiento/<?= $oAcreditado->acre_id ?>?even_id=<?= $key ?>">
								<?=$val ?></a></li>
							<?php endforeach ?>
						</ul>
					</div>
					<?php endif ?>
					
				</div>
				
			</div>
			
			<div class="box-body">
				<table class="table">
					<thead>
						<tr>
							<th>Evento</th>
							<th>Inicio</th>
							<th>Fin</th>
							<th>Estado</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($aEvento as $oEvento): ?>
							<tr>
								<td><?= $oEvento->oEvento->even_nombre ?></td>
								<td><?= $oEvento->oEvento->even_fecha_inicio ?></td>
								<td><?= $oEvento->oEvento->even_fecha_fin ?></td>
								<td><?= $oEvento->estado() ?></td>
								<td>
									<?php if (ACL::instance()->allowed('acreditado', 'add_autoevaluacion') AND $oEvento->admin_can_create_new_autoevaluacion()): ?>
										<a href="/admin/acreditado/add_autoevaluacion/<?= $oAcreditado->acre_id.URL::query(['acev_id' => $oEvento->acev_id]) ?>" 
											title="Crear nueva autoevaluación en este evento" data-confirm 
											class="btn btn-flat btn-success"><i class="fa fa-plus"></i></a>
									<?php endif ?>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			
		</div>--> <!-- /.box -->
		
		<?php if ($seguimientos): ?>
		<!--<div class="box">
			<div class="box-header">
				<h4 class="box-title">Eventos de seguimiento disponibles</h4>
			</div>
			
			<div class="box-body">
				<table class="table">
					<thead>
						<tr>
							<th>Evento</th>
						</tr>
                                        </thead>
					<tbody>						
						<?php foreach ($seguimientos as $evento): ?>
							<tr>
								<td><?= @$evento["even_nombre"] ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			
		</div>--> <!-- /.box -->
		<?php endif ?>
		
		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Miembros del comité</h4>
			</div>
			
			<div class="box-body box-responsive">
				<table class="table committee">
					<thead>
						<tr>
							<th>Nombres</th>
							<th>Apellidos</th>
							<th>Cargo</th>
							<th>Correo</th>
							<th>Teléfono</th>
							<th>Contacto</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($aPersona as $oPersona): ?>
							<tr>
								<td><p class="form-control-static"><?= $oPersona->pers_nombres ?></p></td>
								<td><p class="form-control-static"><?= $oPersona->pers_apellidos ?></p></td>
								<td><p class="form-control-static"><?= $oPersona->pers_cargo ?></p></td>
								<td><p class="form-control-static"><?= $oPersona->pers_correo ?></p></td>
								<td><p class="form-control-static"><?= $oPersona->pers_telefono ?></p></td>
								<td>
									<p class="form-control-static">
										<?php if ($oPersona->pers_tipo == "contacto"):?> 
											<span class="label label-success">Sí</span> 
										<?php else: ?>
											<span class="label label-default">No</span>
										<?php endif ?>
									</p>
								</td>
								<!--<span class="label label-success">Success</span>-->
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		
		<?php if ($aNewPersona->count()): ?>
		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Nuevo comité pendiente de aprobación</h4>
			</div>
			
			<div class="box-body box-responsive">
				<table class="table committee">
					<thead>
						<tr>
							<th>Nombres</th>
							<th>Apellidos</th>
							<th>Cargo</th>
							<th>Correo</th>
							<th>Teléfono</th>
							<th>Contacto</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($aNewPersona as $oPersona): ?>
							<tr>
								<td><p class="form-control-static"><?=$oPersona->pers_nombres?></p></td>
								<td><p class="form-control-static"><?=$oPersona->pers_apellidos?></p></td>
								<td><p class="form-control-static"><?=$oPersona->pers_cargo?></p></td>
								<td><p class="form-control-static"><?=$oPersona->pers_correo?></p></td>
								<td><p class="form-control-static"><?=$oPersona->pers_telefono?></p></td>
								<td>
									<p class="form-control-static">
										<?php if ($oPersona->pers_tipo == "pending_contacto"):?> 
											<span class="label label-success">Sí</span> 
										<?php else: ?>
											<span class="label label-default">No</span>
										<?php endif ?>
									</p>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table><br>
				
				<label>Documento de autorización del Comité</label>
				<p class="form-control-static">
					<?php if ($oAcreditado->documento_pendiente()): ?>
						<a href="/file/<?= $oAcreditado->documento_pendiente() ?>" target="_blank">Ver documento</a>
					<?php else: ?>
						--
					<?php endif ?>
				</p>
				<br>
				
				<?php if (ACL::instance()->allowed('acreditado', 'approve_comision')): ?>
				<div class="text-right">
					<a href="/admin/acreditado/approve_comision/<?= $oAcreditado->acre_id ?>" class="btn btn-primary">
						<i class="fa fa-check"></i> Aprobar</a>
				</div>
				<?php endif ?>
			</div>
		</div>
		<?php endif ?>
		
	</section><!-- /.content -->
</aside><!-- /.right-side -->
