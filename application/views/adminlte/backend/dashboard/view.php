<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<?php //debug($data_mapa); ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<?php if (ACL::instance()->allowed('dashboard', 'new_users')): ?>
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>
							<?= $new_users | '0' ?>
						</h3>
						<p>
							Nuevos usuarios registrados
						</p>
					</div>
					<div class="icon">
						<i class="ion ion-person-add"></i>
					</div>
					<a href="/admin/acreditado?filter__tipo=<?= Model_Acreditado::ESTADO_REGISTRADO ?>" class="small-box-footer">
						Ver <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			<?php endif ?>
			
			<div class="col-lg-3 col-xs-6">
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>
							<?= $num_univ | '0' ?>
						</h3>
						<p>
							Comités de Calidad activos - Universidades
						</p>
					</div>
					<div class="icon">
						<i class="fa fa-suitcase"></i>
					</div>
					<a href="/admin/acreditado/universidad?filter__tipo=1" class="small-box-footer">
						Ver <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-xs-6">
				<div class="small-box bg-maroon">
					<div class="inner">
						<h3>
							<?= $num_auto | '0' ?>
						</h3>
						<p>
							Total autoevaluaciones realizadas
						</p>
					</div>
					<div class="icon">
						<i class="fa fa-check-square-o"></i>
					</div>
					<a href="/admin/autoevaluacion" class="small-box-footer">
						Ver <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			
			<?php if (ACL::instance()->allowed('dashboard', 'comite_pending')): ?>
			<div class="col-lg-3 col-xs-6">
				<div class="small-box bg-gray">
					<div class="inner">
						<h3>
							<?= $comite_pending | '0' ?>
						</h3>
						<p>
							Solicitudes de cambio de comité
						</p>
					</div>
					<div class="icon">
						<i class="fa fa-list-alt"></i>
					</div>
					<a href="/admin/acreditado/committee" class="small-box-footer">
						Ver <i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			<?php endif ?>
		</div>
		
		<div class="row ">
			
			<div class="col-sm-12">
				<div class="box box-danger">
					<!--div class="box-header">
						<h3 class="box-title">Universidades por tipo de acreditación</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-flat btn-sm btn-default"><i class="fa fa-file-text-o"></i></button>
						</div>
					</div-->
					
					<div class="box-body chart-responsive">
						
						<?= $pie_univ ?>
						
					</div><!-- /.box-body -->
				</div>
			</div>
			
			<div class="col-sm-12">
				<div class="box">
					<!--div class="box-header">
						<h3 class="box-title"><?= $inst_participantes->title ?></h3>
					</div--><!-- /.box-header -->
					<div class="box-body _no-padding box-instituciones">
						<div class="pull-right _list-buttons">
							<a href="/admin/report/excel/inst_participantes" class="btn btn-flat btn-default btn-excel">
								<i class="fa fa-file-text"></i> 
								Excel </a>
							<a href="/admin/report/pdf/inst_participantes" class="btn btn-flat btn-default">
								<i class="fa fa-download"></i> 
								PDF </a>
						</div>
						
						<?= $inst_participantes ?>
						
					</div><!-- /.box-body -->
				</div><!-- /.box -->

			</div>
			
			
			<div class="col-sm-12">
				<div class="box box-danger">
					<!--div class="box-header">
						<h3 class="box-title">Universidades participantes por región</h3>
					</div-->
					<div class="box-body chart-responsive">
						
						<?php //Chart::factory('BarAcreditadosU') ?>
						<?= $bar_univ ?>
						
					</div><!-- /.box-body -->
				</div>
			</div>
			
			<!--div class="col-sm-12 " id="div_imagen_mapa">
				<div class="box box-danger ">
					<div class="box-header">
						<h3 class="text-center">Comités de Calidad por Regiones / Zonas</h3>
					</div>
					<div class="text-center">
						
						<img id="img_mapa" class="" src="/media/saes_lte/img/mapa.jpg" >
						
					</div>
				</div>
			</div>
			
			<div class="col-sm-12 box box-danger" id="div_iframe_mapa">
				<div class="col-sm-3"></div>
				<div class="col-sm-7">
					<div class="">
						<div class="text-center">
							<h3 class="box-title">Comités de Calidad por Regiones / Zonas</h3>
						</div>
						<div class="text-center">
							
							<iframe id="mapa_iframe" style="height: 712px; width: 100%; border: 0;" src="http://procalidad.gob.pe/documentos/mapa-educacion-superiorv2/mapa-sl/mapa-peru-acreditacion.html"></iframe>
							
							<input type="hidden" id="input_mapa_iframe" value='<?//= $data_mapa ?>' >
						</div>
					</div>
				</div>
				
				<div class="col-sm-2"></div>
			</div-->
			
		</div>
		
		

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>