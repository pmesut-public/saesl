<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Carga masiva
			<small>Arbol de estándares</small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Subir archivo CSV</h4>
			</div>
			
			<div class="box-body">
				
				<!--div class="alert alert-info alert-dismissable">
					<i class="fa fa-info"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Importante!</b>
					__
				</div-->
				
				<form role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
					
					<div class="form-group ">
						<label for="fact_codigo" class="col-sm-2 control-label">Archivo .csv</label>
						<div class="col-sm-6">
							<input type="file" class="form-control " name="file" id="file" placeholder="File" required>
							<p class="help-block">
								El archivo debe contener cabeceras con campos según el tipo de institución:
							</p>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-flat btn-primary">Subir</button>
							<a href="/admin" class="btn btn-flat btn-default">Volver</a>
						</div>
					</div>
				</form>
				
			</div>
		</div>

		
		
	</section><!-- /.content -->
</aside><!-- /.right-side -->
