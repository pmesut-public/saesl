<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Carga masiva
			<small>Arbol de estándares</small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Subir archivo CSV</h4>
			</div>
			
			<div class="box-body">
				
				<!--div class="alert alert-info alert-dismissable">
					<i class="fa fa-info"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Importante!</b>
					__
				</div-->
				
				<form role="form" class="form-horizontal" method="post" enctype="multipart/form-data">

					<div class="form-group">
						<label for="obac_id" class="col-sm-2 control-label">Objeto de acreditación</label>
						<div class="col-sm-6">
							<select class="form-control" name="obac_id" id="obac_id" required>
								<option value="">--</option>
								
								<?php foreach (Model_ObjetoAcreditacion::get_tipos() as $key => $val): ?>
									<option value="<?= $key ?>"><?= $val ?></option>
								<?php endforeach ?>
								
							</select>
						</div>
					</div>
					
					<div class="form-group ">
						<label for="fact_codigo" class="col-sm-2 control-label">Archivo .csv</label>
						<div class="col-sm-6">
							<input type="file" class="form-control " name="file" id="file" placeholder="File" required>
							<p class="help-block">
								El archivo debe contener cabeceras con campos según el tipo de institución:
							</p>
							<h4>Universidades:</h4>
							<p>
								DIMENSION, DNOMBRE, FACTOR, FNOMBRE, CRITERIO, CNOMBRE, CDESC, ESTANDAR, 
								EDESC, ETIPO, PONDERACION, FUENTE DE VERIFICACION, NOMBRE DE LAS FUENTES, 
								CONDICION, MODALIDAD
							</p>
							<h4>Institutos:</h4>
							<p>
								DIMENSION, DESCRIPCION DIMENSION, FACTOR, DESCRIPCION FACTOR, CRITERIO, 
								NOMBRE CRITERIO, ESTANDAR, NOMBRE ESTANDAR, DESCRIPCION DEL ESTANDAR, 
								ACEPTACION, PONDERACION, FUENTE DE VERIFICACION, Nombres de las Fuentes, 
								NIVEL ACEPTACION, NOMBRE ACEPTACION, CONDICION, MODALIDAD
							</p>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-flat btn-primary">Subir</button>
							<a href="/admin" class="btn btn-flat btn-default">Volver</a>
						</div>
					</div>
				</form>
				
			</div>
		</div>

		
		
	</section><!-- /.content -->
</aside><!-- /.right-side -->
