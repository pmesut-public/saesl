<aside class="left-side sidebar-offcanvas">                
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<img src="/media/saes_lte/img/logo.png" class="img-responsive img-logo">
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->

		<?php //debug($sidebar) ?>
		<ul class="sidebar-menu">
			<?php foreach ($sidebar as $title => $subtree): ?>
				
				<?php if (is_string($subtree)): ?>
				
				<li>
					<a href="/admin/<?= $subtree ?>">
						<i class="fa fa-list-ul"></i> <span><?= $title ?></span>
					</a>
				</li>
				
				<?php else: ?>
				
				<li class="treeview">
					<a href="#">
						<i class="fa fa-list-ul"></i>
						<span><?= $title ?></span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<?php foreach ($subtree as $subtitle => $path): ?>
						<li><a href="/admin/<?= $path ?>"><i class="fa fa-angle-double-right"></i> <?= $subtitle ?></a></li>
						<?php endforeach ?>
					</ul>
				</li>
				
				<?php endif ?>
				
			<?php endforeach ?>
		</ul
		
	</section>
	<!-- /.sidebar -->
</aside>
