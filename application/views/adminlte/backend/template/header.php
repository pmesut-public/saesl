<header class="header">
	<a href="/" class="logo" style="font-size: 20px">
		<!-- Add the class icon to your logo image or logo icon to add the margining -->
		<strong>SAES licenciamiento</strong>
		<!--img src="/media/saes_lte/img/logo-white.png" class="img-responsive"-->
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		
		<?php if (isset($oAcreditado)): ?>
		<p class="navbar-text"><?= $oAcreditado->subtitle ?></p>
		<?php else: ?>
		<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<?php endif ?>
		
		<div class="navbar-right">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i>
						<span><?= $auth->get_user()->username ?> <i class="caret"></i></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header bg-light-blue">
							<!--img src="/media/adminlte/img/avatar3.png" class="img-circle" alt="User Image" /-->
							<p>
								<a href="/admin/usuario" title="Cambiar contraseña">Perfil: <?= ACL::instance()->get_role() ?></a>
								<small>Fecha: <?= date('d/m/Y') ?></small>
								
								<?php if (ACL::instance()->allowed('server_info')): ?>
								<small>SERVER: <?= Kohana::$server_name ?></small>
								<small>ENV: <?= $_SERVER['KOHANA_ENV'] ?></small>
								<small>BUILD: <?= SAES_BUILD ?></small>
								<?php endif ?>
							</p>
						</li>
						<!-- Menu Body -->
						<!--li class="user-body">
							<div class="col-xs-4 text-center">
								<a href="#">Followers</a>
							</div>
							<div class="col-xs-4 text-center">
								<a href="#">Sales</a>
							</div>
							<div class="col-xs-4 text-center">
								<a href="#">Friends</a>
							</div>
						</li-->
						
						<?php if (isset($oAcreditado)): ?>
						<li class="user-body">
							<!--<div class="col-xs-6 text-left">
								<a href="/eventos<?php //= URL::query() ?>" class="btn btn-default">Ver eventos</a>
							</div>-->
                                                        <div class="col-xs-6 text-left">
								<a href="/eventos/objeto" class="btn btn-default">Menu inicio</a>
							</div>
						</li>
						<li class="divider"></li>
						<li>
							<a href="/autoevaluaciones<?= URL::query() ?>">
								<i class="fa fa-check-square-o"></i> Ver autoevaluaciones</a>
						</li>
						<!--<li>
							<a href="/autoevaluaciones?stats&acre_id=<?php //= $oAcreditado->acre_id ?>">
								<i class="fa fa-bar-chart-o"></i> Ver estadísticas</a>
						</li>-->
						<?php endif ?>
						
						<!-- Menu Footer-->
						<li class="user-footer">
							
							<?php if (isset($oAcreditado)): ?>
							<div class="pull-left">
								<a href="/admin" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Volver al admin</a>
							</div>
							<?php endif ?>
							
							<div class="pull-right">
								<a href="/welcome/salir" class="btn btn-default btn-flat">Salir</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
