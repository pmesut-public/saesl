<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<!-- Main content -->
	<section class="content">
		
		<form class="form-horizontal" role="form">
			
			<div class="box">
				<div class="box-header">
					<h4 class="box-title">Modalidad y Categoría de Financiamiento - Miembros del Comité de Calidad Institucional</h4>
				</div>
				
				<div class="box-body">
					
					<div class="form-group">
						<label class="col-sm-2 control-label">Modalidad</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oModalidad->moda_nombre ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Categoría de Financiamiento</label>
						<div class="col-sm-10">
							<div class="radio">
								<?php foreach ($aCatFin as $key => $val): ?>
									<label>
										<input name="cat_fin" value="<?= $key ?>" type="radio"> <?= $val ?>
									</label>
								<?php endforeach ?>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
		</form>
		
	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
