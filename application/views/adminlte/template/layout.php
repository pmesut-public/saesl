<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>SAES licenciamiento - PROCALIDAD</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Developed by Sergio Melendez -->
		
		<?= $theme->get_css() ?>
		
	</head>
	<body class="skin-blue <?= $body_class ?>">
		<!-- header logo: style can be found in header.less -->

		<?= $header ?>

		<div class="wrapper row-offcanvas row-offcanvas-left">
			<!-- Left side column. contains the logo and sidebar -->

			<?= $sidebar ?>

			<!-- Right side column. Contains the navbar and content of the page -->

			<?= $messages ?>
			
			<?= $content ?>
			
			<?= $footer ?>

		</div><!-- ./wrapper -->
		
		<!--div class="popup"></div-->
		
		<div class="modal fade" id="myModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="myModalLabel">Conectando..</h4>
					</div>
					<div class="modal-body">
						<div class="progress sm progress-striped active">
							<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
								<span class="sr-only">20%</span>
							</div>
						</div>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
		<?= $theme->get_js() ?>
		<?= $theme->get_extra() ?>
		
	</body>
</html>
