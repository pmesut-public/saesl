<script>
	window.__CONTEXT__ = <?= $result ?>;
	
    window.__INITIAL_STATE__ = {};
</script>

<aside class="right-side strech">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Seleccionar un evento para autoevaluación - PROCESO DE LICENCIAMIENTO
			<small></small>
		</h1>		
		
		<?= Breadcrumb::build() ?>
		
	</section>
	
	<!-- Main content -->
	<section class="content">
    <div id="root"></div>
	</section><!-- /.content -->

</aside><!-- /.right-side -->
