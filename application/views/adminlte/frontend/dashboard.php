<aside class="right-side strech">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Ver perfil
			<small></small>
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Detalles</h4>
			</div>
			
			<div class="box-body">
				<form class="form-horizontal" role="form" method="post">
					<div class="form-group">
						<label class="col-sm-2 control-label">Acreditación</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oObjetoAcreditacion->objeto_nombre() ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Institución</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oInstitucion()->name ?></p>
						</div>
					</div>
					<?php if ($oAcreditado->oCarrera()->loaded()): ?>
					<div class="form-group">
						<label class="col-sm-2 control-label">Carrera</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oCarrera()->oCarrera->carr_nombre ?></p>
						</div>
					</div>
					<?php endif ?>
					<div class="form-group">
						<label class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oContacto()->pers_correo ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Nombre de usuario</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oUser->username ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Contraseña</label>
						<div class="col-sm-10">
							<a href="/profile?change_password=1" class="btn btn-default">Cambiar</a>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">Fecha registro</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->acre_fecha_reg ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Documento de autorización del Comité</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<?php if($oAcreditado->documento()): ?>
									<a href="/file/<?= $oAcreditado->documento() ?>" target="_blank">Ver documento</a>
								<?php else: ?>
									--
								<?php endif ?>
							</p>
						</div>
					</div>
				</form>
			</div>
			
		</div><!-- /.box -->
		
		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Miembros del comité de calidad</h4>
			</div>
			
			<div class="box-body box-responsive">
				
				<div class="alert alert-info alert-dismissable">
					<i class="fa fa-info"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Importante!</b>
					
					Si desea cambiar el comité de calidad, deberá ingresar los miembros de la misma, 
					subir un nuevo documento que autorice la creación del mismo y enviar sus cambios para ser aprobados.
					
				</div>
				
			</div>
		</div>

	</section><!-- /.content -->
</aside><!-- /.right-side -->
