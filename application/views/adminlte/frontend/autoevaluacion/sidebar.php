<aside class="left-side sidebar-offcanvas">                
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- sidebar menu: : style can be found in sidebar.less -->
		
		<!--div class="buttons">
			<a href="/autoevaluacion/discard" class="btn btn-app btn-danger" 
				data-toggle="tooltip" title="Descartar autoevaluación" data-placement="bottom"
				data-confirm="Está seguro de descartar su autoevaluación?">
				<i class="fa fa-trash-o"></i> Descartar
			</a>
			<a href="/autoevaluacion/finish" class="btn btn-app btn-primary" 
				data-toggle="tooltip" title="Terminar autoevaluación (Debe guardar todos los estándares)" data-placement="bottom">
				<i class="fa fa-check-square-o"></i> Terminar
			</a>
			<a href="/autoevaluacion/save_session" class="btn btn-app btn-success" 
				data-toggle="tooltip" title="Guardar autoevaluación (Podrá retomarla luego)" data-placement="bottom">
				<i class="fa fa-save"></i> Guardar
			</a>
		</div-->
		
		<ul class="sidebar-menu">
			<!--li>
				<a href="/admin">
					<i class="fa fa-dashboard"></i> <span>Home</span>
				</a>
			</li-->
			<li>
				<a href="/autoevaluaciones">
					<i class="fa fa-check-square-o"></i> <span>Autoevaluaciones</span>
				</a>
			</li>

			<?php foreach ($aDimension as $dime => $aFactor): ?>
			<li class="treeview active">
				<a href="#">
					<i class="fa fa-suitcase"></i>
					<span data-toggle="_tooltip" title="Dimensión <?= $dime ?>"><?= $dime ?></span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					
					<?php foreach ($aFactor as $fact => $aCriterio): ?>
					<li class="treeview active">
						<a href="#">
							<i class="fa fa-angle-double-right"></i>
							<span data-toggle="_tooltip" title="Factor <?= $fact ?>"><?= $fact ?></span>
							<!--i class="fa fa-angle-left pull-right"></i-->
						</a>
						<ul class="treeview-menu">
							
							<?php foreach ($aCriterio as $crit => $aEstandar): ?>
							<li class="treeview active">
								<a href="#">
									<i class="fa fa-angle-double-right"></i>
									<span data-toggle="_tooltip" title="Criterio <?= $crit ?>"><?= $crit ?></span>
									<!--i class="fa fa-angle-left pull-right"></i-->
								</a>
								<ul class="treeview-menu">
									
									<?php foreach ($aEstandar as $codigo => $estandar): ?>
									<li data-id="<?= $estandar['esta_id'] ?>">
										<a href="#esta-<?= $estandar['esta_id'] ?>" class="li_estandar">
											<i class="fa fa-angle-double-right"></i> 
											<span data-toggle="_tooltip" title="<?= $estandar['esta'] ?>">Estándar <?= $codigo ?></span>
										</a>
									</li>
									<?php endforeach ?>
									
								</ul>
							</li>
							<?php endforeach ?>
							
						</ul>
					</li>
					<?php endforeach ?>
					
				</ul>
			</li>
			<?php endforeach ?>
			
			<li>
				<a href="/autoevaluaciones">
					<i class="fa fa-check-square-o"></i> <span>Autoevaluaciones</span>
				</a>
			</li>
			
		</ul>
		
	</section>
	<!-- /.sidebar -->
</aside>
