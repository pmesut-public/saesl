<?= $header ?>

<header class="header-auto">
	
	<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</a>
	
	<div class="result">
		<span><?php //= $response['resultado'] ?></span>
		
		<?= Theme_View::factory('blocks/result', compact('response')) ?>

	</div>
	
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small></small>
		</h1>
	</section>
	
	<div class="buttons">
		<a href="/autoevaluacion/discard" class="btn btn-flat btn-md btn-danger" 
			data-toggle="tooltip" title="Descartar autoevaluación" data-placement="bottom"
			data-confirm="Está seguro de descartar su autoevaluación?">
			<i class="fa fa-trash-o"></i> <span>Cancelar</span>
		</a>
		<a href="/autoevaluacion/finish" class="btn btn-flat btn-md btn-primary" 
			data-toggle="tooltip" title="Terminar autoevaluación (Debe guardar todos los estándares)" data-placement="bottom">
			<i class="fa fa-check-square-o"></i> <span>Terminar</span>
		</a>
		<a href="/autoevaluacion/save_session" class="btn btn-flat btn-md btn-success" 
			data-toggle="tooltip" title="Guardar autoevaluación (Podrá retomarla luego)" data-placement="bottom">
			<i class="fa fa-save"></i> <span>Guardar</span>
		</a>
	</div>
</header>
