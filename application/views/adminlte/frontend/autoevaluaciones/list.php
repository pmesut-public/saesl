<aside class="right-side strech">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Autoevaluaciones realizadas | <a href="/documentos<?= URL::query() ?>">Documentos</a>
			<small></small>
		</h1>
		<?= Breadcrumb::build() ?>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-body box-responsive">
				
				<table class="table table-bordered table-hover table-middle">
					<thead>
						<tr>
							<th>Número</th>
							<th>Evento</th>
                            <!--<th>Objeto de autoevaluación</th>-->
							<th>Fecha de inicio</th>
							<th>Fecha de cierre</th>
							<th>Estado</th>
							<th>Autoevaluación</th>
							<th class="hidden-print">Reportes</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($aAutoevaluacion as $oAutoevaluacion): ?>
						<tr>
							<td><?= $oAutoevaluacion->auto_numero ?></td>
							<td><?= $oAutoevaluacion->oAcreditadoEvento->oEvento->even_nombre ?></td>
							<td><?= $oAutoevaluacion->auto_fecha_inicio ?></td>
							<td><?= $oAutoevaluacion->auto_completa == Model_Autoevaluacion::AUTOEVALUACION_COMPLETA ? $oAutoevaluacion->auto_fecha_fin : '-' ?></td>
							<td><?= $oAutoevaluacion->auto_estado ?></td>
							<td class="text-center">
								<?php if ($oAutoevaluacion->auto_completa == Model_Autoevaluacion::AUTOEVALUACION_COMPLETA): ?>
									<span class="icon-2x icon-success" title="Autoevaluación Completa">
										<i class="fa fa-check-circle"></i>
									</span>
								<?php endif ?>

								<?php if ($oAutoevaluacion->auto_completa == Model_Autoevaluacion::AUTOEVALUACION_PENDIENTE): ?>
									<span class="icon-2x icon-warning" title="Autoevaluación Pendiente">
										<i class="fa fa-warning"></i>
									</span>
								<?php endif ?>
							</td>
							<td class="hidden-print">
                                <?php foreach ($oAutoevaluacion->get_reportes()  as $report => $params): ?>
                                    <a href="/reportes/view/<?= $report ?>/<?= URL::query(['id' => $oAutoevaluacion->auto_id]) ?>" class="btn btn-link" title="<?= $params['title'] ?>">
                                    	<?= $params['short'] ?>
                                    </a>
                                <?php endforeach; ?>
								<?php //= Theme_View::factory('blocks/reports', compact('oAutoevaluacion')) ?>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
				
			</div>
		</div>
		
		<?php
			/*
			if (Session::instance()->get('oAcreditado')): ?>
				<?php if ($oAcreditado->oAutoevaluacionActiva()->loaded()): ?>
                    <?php $_url = ($oAcreditado->oEventoActual()->oEvento->even_objeto == 1) ? 'mejora' : 'autoevaluacion'; ?>
					<a href="/<?=$_url?>/calculator" class="btn btn-flat btn-success">Retomar Autoevaluación activa</a>
				<?php endif ?>
			<?php endif
			*/
		?>
                                
        <?php if (Session::instance()->get('oAcreditado')): ?>
			<?php if ($oAcreditado->oAutoevaluacionActiva()->loaded()): ?>
                <?php //$_url = ($oAcreditado->oEventoActual()->oEvento->even_objeto == 1) ? 'mejora' : 'autoevaluacion'; ?>
				<a href="/autoevaluacion/calculator" class="btn btn-flat btn-success"><?= __('Retomar Autoevaluación activa') ?></a>
			<?php elseif ($oAcreditado->oEventoActual()->loaded()): ?>
				<a href="/eventos/objeto" class="btn btn-flat btn-success"><?= __('Iniciar nueva Autoevaluación') ?></a>
			<?php else: ?>
				<a href="/eventos/objeto" class="btn btn-flat btn-success"><?= __('Seleccionar nuevo evento para autoevaluación') ?></a>
			<?php endif ?>
		<?php endif ?>

		<!-- <a class="btn btn-flat btn-primary" data-toggle="collapse" data-target=".stats">Ver estadísticas</a> -->
		
		<div class="collapse stats">
			<hr>
			<div class="row">
				<div class="col-sm-6">
					<div class="box">
						<div class="box-body">
							<?php //= $report1 ?>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box">
						<div class="box-body">
							<?php //= $report2 ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- /.content -->
</aside><!-- /.right-side -->
