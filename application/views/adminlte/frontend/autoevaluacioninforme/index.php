<aside class="right-side strech">
	<!-- Content Header (Page header) -->
	<!--section class="content-header">
		<h1>
			Autoevaluaciones realizadas | <a href="/eventos<?= URL::query() ?>">Eventos</a>
			<small></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section-->
	
	<!-- Main content -->
	<section class="content">
		
		<div class="box">
			<div class="box-body box-responsive">
				
				<form method="post" class="form-horizontal">
					
					<?= $report ?>
					
					<a class="btn btn-flat btn-success" href="" />Guardar sin terminar</a>
					<input class="btn btn-flat btn-primary" type="submit" value="Terminar" />
					
				</form>
				
			</div>
		</div>
		
	</section><!-- /.content -->
</aside><!-- /.right-side -->
