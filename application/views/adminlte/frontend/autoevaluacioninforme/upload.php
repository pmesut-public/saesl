<aside class="right-side strech">
	<!-- Content Header (Page header) -->
	<!--section class="content-header">
		<h1>
			Autoevaluaciones realizadas | <a href="/eventos<?= URL::query() ?>">Eventos</a>
			<small></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section-->

	<!-- Main content -->
	<section class="content">
		
		<div class="alert alert-info _alert-dismissable">
			<i class="fa fa-info"></i>
			<!--button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button-->
			<!--h3>Ficha guardada!</h3-->
			
			<button type="button" data-url="/autoevaluacionInforme/pdf" 
				_target="_blank" class="btn btn-flat btn-sm btn-github pull-right download_ficha">
				<i class="fa fa-download"></i>
				Descargar ficha</button>
			
			<p>Para finalizar, deberá descargar <i class="fa fa-download"></i> esta ficha, 
				firmarla y subirla <i class="fa fa-upload"></i> escaneada.
			</p>
		</div>
		
		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Subir documento</h4>
			</div>
			<div class="box-body">
				<form method="post" enctype="multipart/form-data">
					<div class="form-group">
						<input type="file" name="file" class="form-control" required>
					</div>
					
					<a href="/autoevaluacionInforme/edit" 
						class="btn btn-flat btn-default pull-right">
						<i class="fa fa-arrow-left"></i>
						Volver a editar</a>
					<button type="submit" name="action" class="btn btn-flat btn-primary">
						<i class="fa fa-upload"></i> Subir ficha firmada</button>
				</form>
			</div>
		</div>
		
		<div class="box">
			<div class="box-body box-responsive">
				<?= $report ?>
			</div>
		</div>
		
		<div class="clearfix">
			<a href="/autoevaluacionInforme/edit" 
				class="btn btn-flat btn-default pull-right">
				<i class="fa fa-arrow-left"></i>
				Volver a editar</a>
		</div>
		
	</section><!-- /.content -->
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
