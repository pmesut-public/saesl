<header class="header regular">
	<a href="/" class="logo">
		<!-- <img src="/media/saes_lte/img/logo-white-o.png" class="icon">-->
		<!-- Add the class icon to your logo image or logo icon to add the margining -->
		<strong>SAES licenciamiento</strong>
		<!--img src="/media/saes_lte/img/logo-white.png" class="img-responsive"-->
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		
		<p class="navbar-text navbar-left hidden-xs" title="<?= $welcome ?>"><?= $welcome ?></p>
		
		<div class="navbar-right">
			<ul class="nav navbar-nav">
				
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i>
						<span><?= $auth->get_user()->username ?> <i class="caret"></i></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header bg-light-blue">
							<p>
								Evento actual: <?= $oEventoActual->oEvento->even_nombre ?: 'Ninguno' ?>
								<small>Fecha: <?= date('d/m/Y') ?></small>
							</p>
						</li>
						<!-- Menu Body -->
						
						<li class="user-body">
							<!-- <div class="col-xs-6 text-left">
								<a href="/eventos" class="btn btn-default">Mis eventos</a>
							</div> -->
                                                    <div class="col-xs-6 text-left">
							<a href="/eventos/objeto" class="btn btn-default">Menu inicio</a>
                                                    </div>
						</li>
						
						<li>
							<a href="/autoevaluaciones"><i class="fa fa-check-square-o"></i> Mis autoevaluaciones</a>
						</li>
						<!-- <li>
							<a href="/autoevaluaciones?stats"><i class="fa fa-bar-chart-o"></i> Ver estadísticas</a>
						</li> -->
						<li class="divider"></li>
						<li>
							<a href="/media/Manual de Usuario SAES.pdf"><i class="fa fa-exclamation-circle"></i> Manual de usuario</a>
						</li>
						
						<!-- Menu Footer-->
						<li class="user-footer">
							
							<div class="pull-left">
								<a href="/profile" class="btn btn-default btn-flat">Perfil</a>
							</div>
							
							<div class="pull-right">
								<a href="/welcome/salir" class="btn btn-default btn-flat">Salir</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
