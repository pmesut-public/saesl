<?= $header ?>

<header class="header-auto">
	
	<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</a>
	
	<div class="result">
		<?php //debug($response) ?>
		<span><?php //= $response['resultado'] ?></span>
		
		<?= Theme_View::factory('blocks/result', compact('response')) ?>
		
	</div>
	
	<!--section class="content-header">
		<h1>
			<small></small>
		</h1>
	</section-->
	
	<div class="buttons">
		<?php //debug($reports) ?>
		
		<?php //= Theme_View::factory('blocks/reports_header', compact('oAutoevaluacion')) ?>
		
	</div>
</header>
