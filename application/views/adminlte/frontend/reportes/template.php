<aside class="right-side">
	
	<!-- Main content -->
	<section class="content">
		
		<div class="pull-right list-buttons">
		<?php foreach ($buttons as $button): ?>
			<?= $button ?>
		<?php endforeach ?>
		</div>
		
		<?= $report ?>
		
	</section>
	
</aside>

<iframe class="iframe"></iframe>
