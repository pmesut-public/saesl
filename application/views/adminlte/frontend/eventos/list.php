<aside class="right-side strech">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<a href="/autoevaluaciones<?= URL::query() ?>">Mis autoevaluaciones</a> | Eventos en los cuales he participado | <a href="/documentos<?= URL::query() ?>">Documentos</a>
			<small></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>
	<!-- Main content -->
	<section class="content">

		<div class="box">
			<!--div class="box-header">
				<h4 class="box-title">Miembros del comité de calidad</h4>
			</div-->
			
			<div class="box-body box-responsive">
				
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Nombre</th>
                                                        <th>Objeto de autoevaluación</th>
							<th>Inicio de Evento</th>
							<th>Fin de Evento</th>
							<th>Estado</th>
							<th class="hidden-print"></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($aAcreditadoEvento as $oAcreditadoEvento): ?>
							<tr>
								<td><!-- <?= $oAcreditadoEvento->acev_id ?> -->
									<?= $oAcreditadoEvento->oEvento->even_nombre ?></td>
                                                                <td><?= Model_Evento::$objetos[$oAcreditadoEvento->oEvento->even_objeto]; ?></td>
								<td><?= $oAcreditadoEvento->oEvento->even_fecha_inicio ?></td>
								<td><?= $oAcreditadoEvento->oEvento->even_fecha_fin ?></td>
								<td><?= $oAcreditadoEvento->estado() ?></td>
								<td class="hidden-print">
									
									<?php //if ($oAcreditadoEvento->count_fichas): ?>
									<?php //foreach ($oAcreditadoEvento->aAcreditadoFicha->find_all() as $oFicha): ?>
									<!-- <a href="/eventos/view/<?php // = $oFicha->acfi_id ?>" 
										class="btn btn-flat btn-sm btn-default" 
										title="Ver ficha <?php //= $oFicha->oEventoFicha->evfi_nombre ?>">
										<i class="fa fa-list-alt"></i>
									</a> -->
									<?php //endforeach ?>
									<?php //endif ?>
									
									<?php if (Session::instance()->get('oAcreditado') AND
										$oAcreditadoEvento->can_be_cancelled()): ?>
										
										<form action="/event/discard" method="post" class="inline">
											<button class="btn btn-flat btn-sm btn-danger" title="Cancelar evento"
												data-confirm="Esta acción cancelará el evento actual y eliminará la(s) autoevaluación(es) asociada(s). Está seguro(a)?">
												<i class="fa fa-ban"></i></button>
										</form>
										
									<?php endif ?>

								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>

				
			</div>
		</div>
		
		<?php if (Session::instance()->get('oAcreditado')): ?>
			<?php if ($oAcreditado->oAutoevaluacionActiva()->loaded()): ?>
                        <?php $_url = ($oAcreditado->oEventoActual()->oEvento->even_objeto == 1) ? 'mejora' : 'autoevaluacion'; ?>
				<a href="/<?=$_url?>/calculator" class="btn btn-flat btn-success"><?= __('Retomar Autoevaluación activa') ?></a>
			<?php elseif ($oAcreditado->oEventoActual()->loaded()): ?>
				<a href="/eventos/objeto" class="btn btn-flat btn-success"><?= __('Iniciar nueva Autoevaluación') ?></a>
			<?php else: ?>
				<a href="/eventos/objeto" class="btn btn-flat btn-success"><?= __('Seleccionar nuevo evento para autoevaluación') ?></a>
			<?php endif ?>
		<?php endif ?>
                               <?php //debug(Session::instance()); ?>
		
	</section><!-- /.content -->
</aside><!-- /.right-side -->