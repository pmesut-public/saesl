<aside class="right-side strech">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<a href="/autoevaluaciones<?= URL::query() ?>">Mis autoevaluaciones</a> |<!-- Eventos en los cuales he participado |--> <a href="/documentos<?= URL::query() ?>">Documentos</a>
			<small></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>
	<!-- Main content -->
	<section class="content text-center">

		<h3 class="box-title">Seleccione el Objeto de Autoevaluación</h3>
		
                <a href="/ponderaciones/index/<?php echo $oEvento->mejora();?>" class="btn btn-flat btn-info" style="padding: 15px; margin-bottom: 10px; font-size: 1.5em;"><?= __('Proceso de Mejora del Estándar') ?></a>
		<a href="/ponderaciones/index/<?php echo $oEvento->logro();?>" class="btn btn-flat btn-info" style="padding: 15px; margin-bottom: 10px; font-size: 1.5em;"><?= __('Logro del Estándar') ?></a>
		
	</section><!-- /.content -->
</aside><!-- /.right-side -->