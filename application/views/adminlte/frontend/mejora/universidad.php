<script>
    window.__CONTEXT__ = <?= $context ?>;
    window.__INITIAL_STATE__ = <?= $initial ?>;
</script>

<aside class="right-side strech">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="alert alert-info alert-dismissable ficha-info">
            <i class="fa fa-info"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <ul>
                <li>
                    Cada sección cuenta con un estado
                        (
                            <label class="badge bg-green" title="Guardado"><i class="fa fa-save"></i></label> 
                            Guardado,
                            <label class="badge bg-yellow" title="Pendiente"><i class="fa fa-exclamation"></i></label> 
                            Pendiente,
                            <label class="badge bg-yellow" title="Guardando..."><i class="fa fa-spin fa-spinner"></i></label>
                            Guardando...
						), siendo GUARDADO el estado con el que usted debe cerrar su ficha
                </li>
                <li>
                    El estado de cada una de las secciones determina el estado de la ficha
                </li>
            </ul>
        </div>	
		
		<?= Breadcrumb::build() ?>
		
	</section>
	
	<!-- Main content -->
	<section class="content">
    <div id="root"></div>
	</section><!-- /.content -->

</aside><!-- /.right-side -->