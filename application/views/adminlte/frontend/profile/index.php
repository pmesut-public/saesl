<aside class="right-side strech">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Ver perfil
			<small></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Detalles</h4>
			</div>
			
			<div class="box-body">
				<form class="form-horizontal" role="form" method="post">
					<div class="form-group">
						<label class="col-sm-2 control-label">Objeto de Licenciamiento:</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oObjetoAcreditacion->objeto_nombre() ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Institución</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oInstitucion()->name ?></p>
						</div>
					</div>
					<?php if ($oAcreditado->oCarrera()->loaded()): ?>
					<div class="form-group">
						<label class="col-sm-2 control-label">Carrera</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oCarrera()->oCarrera->carr_nombre ?: '--' ?></p>
						</div>
					</div>
					<?php endif ?>
					<div class="form-group">
						<label class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oContacto()->pers_correo ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Nombre de usuario</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->oUser->username ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Contraseña</label>
						<div class="col-sm-10">
							<a href="/profile?change_password=1" class="btn btn-default">Cambiar</a>
						</div>
					</div>
					
					<?php if ($change_password): ?>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="old_password">Contraseña antigua</label>
							<div class="col-sm-10">
								<input class="form-control" type="password" name="old_password" id="old_password" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="password">Nueva contraseña</label>
							<div class="col-sm-10">
								<input class="form-control" type="password" name="password" id="password" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="password_confirm">Confirmar nueva contraseña</label>
							<div class="col-sm-10">
								<input class="form-control" type="password" name="password_confirm" id="password_confirm" required>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-10 col-sm-offset-2">
								<button type="submit" class="btn btn-flat btn-primary">Aceptar</button>
							</div>
						</div>
					<?php endif ?>
					
					<div class="form-group">
						<label class="col-sm-2 control-label">Fecha registro</label>
						<div class="col-sm-10">
							<p class="form-control-static"><?= $oAcreditado->acre_fecha_reg ?></p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Documento de autorización del Comité</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<?php if($oAcreditado->documento()): ?>
									<a href="/file/<?= $oAcreditado->documento() ?>" target="_blank">Ver documento</a>
								<?php else: ?>
									--
								<?php endif ?>
							</p>
						</div>
					</div>
				</form>
			</div>
			
		</div><!-- /.box -->
		
		<div class="box">
			<div class="box-header">
				<h4 class="box-title">Miembros del comité de calidad</h4>
			</div>
			
			<div class="box-body box-responsive">
				
				<div class="alert alert-info alert-dismissable">
					<i class="fa fa-info"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<b>Importante!</b>
					Si desea cambiar el comité de calidad, deberá ingresar los miembros de la misma, 
					subir un nuevo documento que autorice la creación del mismo y enviar sus cambios para ser aprobados.
				</div>
				
				<?php if ($change): ?>
					
					<div class="text-right">
						<?= Theme::button('cancel', NULL, '?') ?>
					</div><br>
					
					<form class="form_committee" method="post" enctype="multipart/form-data">
						
						<table class="table committee">
							<thead>
								<tr>
									<th>Contacto</th>
									<th>Nombres</th>
									<th>Apellidos</th>
									<th>Cargo</th>
									<th>Correo</th>
									<th>Número</th>
									<th class="th-actions"></th>
								</tr>
							</thead>
							<tbody>
								<?php for ($i = 0; $i < 3; $i++): ?>
								<tr>
									<td><input name="contact" value="<?= $i ?>" class="change_comitte_contact" type="radio" required /></td>
									<td><input name="first_names[]" type="text" class="form-control" required /></td>
									<td><input name="last_names[]" type="text" class="form-control" required /></td>
									<td><input name="positions[]" type="text" class="form-control" required /></td>
									<td><input name="emails[]" type="email" class="form-control" required /></td>
									<td><input name="phones[]" type="text" class="form-control"
										title="Solo números (mínimo 7 dígitos)" pattern="\d{7,}" required /></td>
									<td class="td-actions">
										<button type="button" class="btn btn-xs btn-flat btn-danger remove_tr" title="Eliminar fila">
											<i class="glyphicon glyphicon-remove"></i></button>
									</td>
								</tr>
								<?php endfor ?>
							</tbody>
						</table><br>
						
						<div class="text-right">
							<button class="btn btn-flat btn-default" type="button" title="Agregar miembro" id="signup-member-add">
								<i class="fa fa-plus"></i>
							</button>
						</div><hr>
						
						<div class="form-group">
							<label for="file">Adjuntar Documento que autoriza la Creación del Comité de Autoevaluación/Calidad</label>
							<input type="file" name="file" id="file" class="form-control" required>
						</div>
						
						<button type="submit" class="btn btn-flat btn-primary">Enviar cambios</button>
						
					</form>
					
				<?php else: ?>
				
					<div class="text-right">
						<?= Theme::button('new_comite', array(
							'title' => 'Nuevo comité',
							'btn_class' => 'btn btn-primary',
							'icon_class' => '',
						), '?change=1') ?>
					</div><br>
					
					<table class="table committee">
						<thead>
							<tr>
								<?php foreach ($table->thead as $th): ?>
									<?php echo $th ?>
								<?php endforeach ?>
							</tr>
						</thead>

						<tbody>

							<?php foreach ($table->tbody as $id => $row): ?>
								<tr>
									<?php foreach ($row as $td): ?>
										<?php echo $td ?>
									<?php endforeach ?>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table><br>
				
				<?php endif ?>
				
			</div>
		</div>

	</section><!-- /.content -->
</aside><!-- /.right-side -->
