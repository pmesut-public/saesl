<section class="content">

	<header>
		<h3 class="text-center">

			<?= $title ?><br>

			<?php if ($oAutoevaluacion->auto_estado == Model_Autoevaluacion::STATUS_ACTIVO): ?>
				<small>Fecha de última modificación: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
				<small>(Autoevaluación pendiente de cierre)</small><br>
			<?php else: ?>
				<small>Fecha de cierre: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
			<?php endif ?>

			<small><?= $subtitle ?></small><br />
			<small><?= $objeto ?></small>
		</h3>
		<hr>
		<h4 class="text-center">
			<strong>Reporte del Estado de las Condiciones Básicas de calidad</strong>
		</h4>
	</header>

	<div class="table-responsive">
		<table class="table table-bordered" border="1">
			<tbody>
				<?php foreach ($aReporte as $dime_id => $arFactores): ?>

					<?php

						$aDimensiones[$dime_id]['dime_cumple'] = true;

						foreach ($arFactores as $fact_id => $arEstandar) {

							$aFactores[$fact_id]['fact_cumple'] = true;

							foreach ($arEstandar as $esta_id => $arDetalle) {

								foreach ($arDetalle as $aude_id => $arMedio) {
									
									if ($aDetalle[$aude_id]['indi_estado'] != Model_Estandar::CUMPLE ) {
											
										$aFactores[$fact_id]['fact_cumple'] = false;	
									}		

								}
								
							}

							if ($aFactores[$fact_id]['fact_cumple'] != Model_Estandar::CUMPLE ) {
									
								$aDimensiones[$dime_id]['dime_cumple'] = false;	
							}
					
						}

					?>

					<tr>
	                    <td colspan="7" style="width: 85%;"><?= $aDimensiones[$dime_id]['dime_descripcion'] ?></td>
	                    <td colspan="2" style="width: 15%;" class="text-center">
	                    	<strong>
								Estado: <?= $aDimensiones[$dime_id]['dime_cumple'] == true ? 'Cumple' : 'No Cumple' ?>
							</strong>
	                    </td>
	                </tr>
	                <tr>
	                  	<td class="success td-center" colspan="2" rowspan="3" style="width: 15%"><div class="text-center">Componente</div></td>
	                  	<td class="success td-center" colspan="2" rowspan="3" style="width: 15%"><div class="text-center">Indicador</div></td>
	                  	<td class="success td-center" colspan="5" style="width: 70%"><div class="text-center">Medios de Verificación</div></td>
	                </tr>
	                <tr class="success">
	                 	<td class="td-center" colspan="2" width="20%"><div class="text-center">Descripción</div></td>
	                  	<td class="td-center" rowspan="2" width="35%"><div class="text-center">Documentos</div></td>
	                  	<td class="td-center" rowspan="2" width="15%"><div class="text-center">Observaciones / Comentarios</div></td>
	                </tr>
	                <tr class="success">
	                	<td class="td-center" colspan="2" width="20%"><div class="text-center">Consideraciones</div></td>
	                </tr>
	                <?php foreach ($arFactores as $fact_id => $arEstandar): ?>
						
						<?php $rowsMedioFactor = $aFactores[$fact_id]['rows_items']; ?>

						<?php foreach ($arEstandar as $esta_id => $arDetalle): ?>

							<?php $rowsMedioEstandar = $aEstandares[$esta_id]['rows_items']; ?>

							<?php foreach ($arDetalle as $aude_id => $arMedio): ?>

								<?php $i = 1 ?>

		                		<?php foreach ($arMedio as $aume_id => $arAutoDocumentos): ?>
		                				<tr>
		                					<?php if ($rowsMedioFactor == $aFactores[$fact_id]['rows_items']): ?>
		                						<td width="2%" rowspan="<?= ($rowsMedioFactor * 2) ?>">
		                							<?= $aFactores[$fact_id]['fact_codigo'] ?>
		                						</td>
		                						<td width="13%" rowspan="<?= ($rowsMedioFactor * 2) ?>">
		                							<?= $aFactores[$fact_id]['fact_titulo'] ?>
		                							<div style="margin-top: 10px">
		                								<strong>
		                									Estado:
		                									<?= $aFactores[$fact_id]['fact_cumple'] == true ? 'Cumple' : 'No Cumple' ?>
		                								</strong>
		                							</div>
		                						</td>
		                					<?php endif ?>
		                					<?php if ($rowsMedioEstandar == $aEstandares[$esta_id]['rows_items']): ?>
		                						<td width="2%" rowspan="<?= ($rowsMedioEstandar * 2) ?>">
		                							<?= $aEstandares[$esta_id]['esta_codigo'] ?>	
		                						</td>
		                						<td width="13%" id="esta-<?= $aEstandares[$esta_id]['esta_id']  ?>" rowspan="<?= ($rowsMedioEstandar * 2) ?>">
		                							<?= $aEstandares[$esta_id]['esta_titulo'] ?>
		                							<div style="margin-top: 10px">
		                								<strong>
		                									Estado:
		                									<?= $aDetalle[$aude_id]['indi_estado'] == Model_Estandar::CUMPLE ? 'Cumple' : 'No Cumple' ?>
		                								</strong>
		                							</div>	
		                						</td>
		                					<?php endif ?>
		                					<td width="2%" rowspan="2"><?= $i ?></td>
				                			<td width="18%"><?= $aMedios[$aume_id]['aume_descripcion'] ?></td>
				                			<td>
				                				<?php if (count($arAutoDocumentos) > 0): ?>
				                					<div class="documents-td">
				                						<table class="table table-bordered table-middle" border="1">
				                						<!--<thead>
				                							<tr>
				                								<th width="40%">Documento</th>
				                								<th width="20%" class="text-center">Estado</th>
				                								<th width="20%" class="text-center">Calidad</th>
				                								<th width="20%" class="text-center">Vigencia</th>
				                							</tr>
				                						</thead>-->
				                						<tbody>
				                							<?php foreach ($arAutoDocumentos as $element): ?>
																<?php $documento = $element[0]; ?>
				                								<?php if ($aDocumentos[$documento['docu_id']]['docu_estado'] == Model_Documento::ACTIVO || $aDocumentos[$documento['docu_id']]['docu_estado'] == Model_Documento::ELIMINADO): ?>
				                									<tr>
						                								<td width="100%" class="text-left">
																			<?= $aDocumentos[$documento['docu_id']]['docu_titulo'] ?>																		
																		</td>
						                								<!--<td width="20%" class="text-center">
						                									<?= Model_Documento::$estados[$aDocumentos[$documento['docu_id']]['_docu_estado']] ?>
						                								</td>
						                								<td width="20%" class="text-center">
						                									<?= Model_Documento::$sub_estados[$aDocumentos[$documento['docu_id']]['sub_estado']]['display'] ?>
						                								</td>
						                								<td width="20%" class="text-center">
						                									<?php if ($aDocumentos[$documento['docu_id']]['_docu_estado']): ?>
						                										<?= Model_Documento::$vigencias_documento[$aDocumentos[$documento['docu_id']]['_docu_vigencia']] ?>
						                									<?php else: ?>
						                										-
						                									<?php endif ?>
						                								</td>-->
						                							</tr>
				                								<?php endif ?>
				                							<?php endforeach ?>
				                						</tbody>
				                					</table>
				                					</div>
				                				<?php else: ?>
				                					<div class="text-center">
				                						No hay documentos
				                					</div>
				                				<?php endif ?>
				                			</td>
				                			<td><?= $aMedios[$aume_id]['aume_observacion'] ?></td>
		                				</tr>
		                				<tr>
											<td colspan="3">
												<?= $aMedios[$aume_id]['aume_consideracion'] ?>
											</td>
										</tr>
		                			<?php 
		                				$i++;
		                				$rowsMedioFactor--;
		                				$rowsMedioEstandar--;
		                			?>

		                		<?php endforeach ?>

							<?php endforeach ?>

						<?php endforeach ?>

	                <?php endforeach ?>

				<?php endforeach ?>
			</tbody>
		</table>
	</div>

</section>

<style type="text/css">
	.title{
		margin-top: 20px;
	}
	.subtitle{

	}
	.text-center{
		text-align: center !important;
	}
	.table{
		width: 100%;
		max-width: 100;	
	}
	.table-center th,
	.table-center td{
	  	vertical-align: middle !important;
	  	text-align: center !important;
	}
	.table-bordered th,
	.table-bordered td{
		padding: 2px;
	}
	.documents-td table{
		width: 98%;
	}
	.success{
		background-color: #dff0d8;
	}
</style>