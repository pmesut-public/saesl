<section class="report-container">
	<header class="header-auto">
		<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
	</header>

	<aside class="left-side sidebar-offcanvas">                
		<section class="sidebar">
			<ul class="sidebar-menu">
				<li>
					<a href="/autoevaluaciones">
						<i class="fa fa-check-square-o"></i> <span>Autoevaluaciones</span>
					</a>
				</li>
				<?php foreach ($aDimensiones as $dime_id => $dimension): ?>
				<li class="treeview active">
					<a href="#">
						<i class="fa fa-suitcase"></i>
						<p data-toggle="_tooltip" title="Dimensión <?= $dimension['dime_titulo'] ?>">
							<?= $dimension['dime_titulo'] ?>	
						</p>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<?php foreach ($aFactores as $fact_id => $factor): ?>
						<?php if ($factor['dime_id'] == $dimension['dime_id']): ?>
						<li class="treeview active">
							<a href="#">
								<i class="fa fa-angle-double-right"></i>
								<span data-toggle="_tooltip" title="Factor <?= $factor['fact_titulo'] ?>">
									<?= $factor['fact_titulo'] ?>
								</span>
							</a>
							<ul class="treeview-menu">
								<?php foreach ($aEstandares as $esta_id => $estandar): ?>
								<?php if ($factor['fact_id'] == $estandar['fact_id']): ?>
									<li data-id="<?= $estandar['esta_id'] ?>">
										<a href="#esta-<?= $estandar['esta_id'] ?>" class="li_estandar">
											<i class="fa fa-angle-double-right"></i> 
											<span data-toggle="_tooltip" title="Estándar <?= $estandar['esta_codigo'] ?>">
												Estándar <?= $estandar['esta_codigo'] ?>
											</span>
										</a>
									</li>
								<?php endif ?>
								<?php endforeach ?>
							</ul>
						</li>
						<?php endif ?>
						<?php endforeach ?>
					</ul>
				</li>
				<?php endforeach ?>
				<li>
					<a href="/autoevaluaciones">
						<i class="fa fa-check-square-o"></i> <span>Autoevaluaciones</span>
					</a>
				</li>
			</ul>
		</section>
	</aside>

	<aside class="right-side">
		<!-- Main content -->
		<section class="content">

			<section class="content-actions">
				<div class="row">
					<div class="col-sm-6">
						<?php if ($oAutoevaluacion->auto_estado == Model_Autoevaluacion::STATUS_ACTIVO): ?>
							<div class="text-left">
								<a href="/autoevaluacion/calculator" class="btn btn-flat btn-primary" title="Regresar a la Ficha de Formatos">
									<i class="fa fa-caret-left"></i> Volver a Autoevaluación
								</a>
							</div>
						<?php endif ?>
					</div>
					<div class="col-sm-6">
						<div class="text-right">
							<a href="<?= $buttonData['path'].(URL::query() ? URL::query().'&' : '?') ?>download=pdf" class="btn btn-default btn-flat" title="Descargar PDF">
								<i class="fa fa-download"></i> PDF
							</a>
							<a href="<?= $buttonData['path'].(URL::query() ? URL::query().'&' : '?') ?>download=excel" class="btn btn-default btn-flat" title="Descargar Excel">
								<i class="fa fa-file-text"></i> Excel
							</a>
						</div>
					</div>
				</div>
			</section>
			
			<header>
				<h3 class="text-center">

					<span class="header-title"><?= $title ?></span><br>

					<?php if ($oAutoevaluacion->auto_estado == Model_Autoevaluacion::STATUS_ACTIVO): ?>
						<small>Fecha de última modificación: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
						<small>(Autoevaluación pendiente de cierre)</small><br>
					<?php else: ?>
						<small>Fecha de cierre: <?= $oAutoevaluacion->auto_fecha_fin ?></small><br>
					<?php endif ?>

					<small><?= $subtitle ?></small><br />
					<small><?= $objeto ?></small>
				</h3>
				<hr>
				<h4 class="text-center">
					<strong>Reporte del Estado de las Condiciones Básicas de calidad</strong>
				</h4>
			</header>

			<div class="table-responsive">
				<table class="table table-bordered">
					<tbody>
						<?php foreach ($aReporte as $dime_id => $arFactores): ?>

							<?php

								$aDimensiones[$dime_id]['dime_cumple'] = true;

								foreach ($arFactores as $fact_id => $arEstandar) {

									$aFactores[$fact_id]['fact_cumple'] = true;

									foreach ($arEstandar as $esta_id => $arDetalle) {

										foreach ($arDetalle as $aude_id => $arMedio) {
											
											if ($aDetalle[$aude_id]['indi_estado'] != Model_Estandar::CUMPLE ) {
													
												$aFactores[$fact_id]['fact_cumple'] = false;	
											}		

										}
										
									}

									if ($aFactores[$fact_id]['fact_cumple'] != Model_Estandar::CUMPLE ) {
											
										$aDimensiones[$dime_id]['dime_cumple'] = false;	
									}
							
								}

							?>

							<tr>
			                    <td colspan="7">
			                    	<strong><?= $aDimensiones[$dime_id]['dime_titulo'] ?></strong>
			                    </td>
			                    <td>
			                    	<strong>
    									Estado:
    									<?= $aDimensiones[$dime_id]['dime_cumple'] == true ? 'Cumple' : 'No Cumple' ?>
    								</strong>
			                    </td>
			                </tr>
			                <tr nobr="true">
			                  	<td class="success td-center" colspan="2" rowspan="3" width="15%">Componente</td>
			                  	<td class="success td-center" colspan="2" rowspan="3" width="15%">Indicador</td>
			                  	<td class="success td-center" colspan="5" width="75%">Medios de Verificación</td>
			                </tr>
			                <tr class="success" nobr="true">
			                 	<td class="td-center" colspan="2" width="30%">Descripción</td>
			                  	<td class="td-center" rowspan="2" width="30%">Documentos</td>
			                  	<td class="td-center" rowspan="2" width="30%">Observaciones / Comentarios</td>
			                </tr>
			                <tr class="success">
			                	<td class="td-center" colspan="2" width="40%">Consideraciones</td>
			                </tr>
			                <?php foreach ($arFactores as $fact_id => $arEstandar): ?>
								
								<?php $rowsMedioFactor = $aFactores[$fact_id]['rows_items']; ?>

								<?php foreach ($arEstandar as $esta_id => $arDetalle): ?>

									<?php $rowsMedioEstandar = $aEstandares[$esta_id]['rows_items']; ?>

									<?php foreach ($arDetalle as $aude_id => $arMedio): ?>

										<?php $i = 1 ?>

				                		<?php foreach ($arMedio as $aume_id => $arAutoDocumentos): ?>
				                				<tr>
				                					<?php if ($rowsMedioFactor == $aFactores[$fact_id]['rows_items']): ?>
				                						<td rowspan="<?= ($rowsMedioFactor * 2) ?>">
				                							<?= $aFactores[$fact_id]['fact_codigo'] ?>
				                						</td>
				                						<td rowspan="<?= ($rowsMedioFactor * 2) ?>">
				                							<?= $aFactores[$fact_id]['fact_titulo'] ?>
				                							<div style="margin-top: 10px">
				                								<strong>
				                									Estado:
				                									<?= $aFactores[$fact_id]['fact_cumple'] == true ? 'Cumple' : 'No Cumple' ?>
				                								</strong>
				                							</div>
				                						</td>
				                					<?php endif ?>
				                					<?php if ($rowsMedioEstandar == $aEstandares[$esta_id]['rows_items']): ?>
				                						<td rowspan="<?= ($rowsMedioEstandar * 2) ?>">
				                							<?= $aEstandares[$esta_id]['esta_codigo'] ?>		
				                						</td>
				                						<td id="esta-<?= $aEstandares[$esta_id]['esta_id']  ?>" rowspan="<?= ($rowsMedioEstandar * 2) ?>">
				                							<?= $aEstandares[$esta_id]['esta_titulo'] ?>
				                							<div style="margin-top: 10px">
				                								<strong>
				                									Estado:
				                									<?= $aDetalle[$aude_id]['indi_estado'] == Model_Estandar::CUMPLE ? 'Cumple' : 'No Cumple' ?>
				                								</strong>
				                							</div>
				                						</td>
				                					<?php endif ?>
				                					<td width="2%" rowspan="2"><?= $i ?></td>
						                			<td width="22%"><?= $aMedios[$aume_id]['aume_descripcion'] ?></td>
						                			<td width="30%">
						                				<?php if (count($arAutoDocumentos) > 0): ?>
						                					<table class="table table-bordered table-middle">
						                						<!--<thead>
						                							<tr>
						                								<th>Documento</th>
						                								<th class="text-center">Estado</th>
						                								<th class="text-center">Calidad</th>
						                								<th class="text-center">Vigencia</th>
						                							</tr>
						                						</thead>-->
						                						<tbody>
						                							<?php foreach ($arAutoDocumentos as $element): ?>
																		<?php $documento = $element[0]; ?>

																		<?php if ($aDocumentos[$documento['docu_id']]['docu_estado'] == Model_Documento::ACTIVO || $aDocumentos[$documento['docu_id']]['docu_estado'] == Model_Documento::ELIMINADO): ?>
																				
																			<tr>																				
								                								<td class="text-left">
																					<a href="/file/<?= $aDocumentos[$documento['docu_id']]['docu_path'] ?>">
																					<?= $aDocumentos[$documento['docu_id']]['docu_titulo'] ?>
																					</a>																					
																				</td>
								                								<!--td class="text-center">
								                									<?php
								                										$color = $aDocumentos[$documento['docu_id']]['_docu_estado'] ? 'color-success' : 'color-warning';
								                										$title = Model_Documento::$estados[$aDocumentos[$documento['docu_id']]['_docu_estado']];
								                									?>
								                									<div class="documento-icon <?= $color ?>">
								                										<i class="fa fa-file" title="<?= $title ?>"></i>
								                									</div>
								                								</td>
								                								<td class="text-center">
								                									<?php
																						$texto = $aDocumentos[$documento['docu_id']]['_docu_estado'] ? 'C' : 'P';
																						$bg    = '';
																						$title = Model_Documento::$sub_estados[$aDocumentos[$documento['docu_id']]['sub_estado']]['display'];
								                										switch ($aDocumentos[$documento['docu_id']]['sub_estado']) {
								                											case 5: $bg = 'background-success'; break;
								                											case 4: $bg = 'background-warning'; break;
								                											case 3: $bg = 'background-danger';  break;
								                										}

								                										$bg = ($aDocumentos[$documento['docu_id']]['_docu_estado'] == Model_Documento::CONCLUIDO) ? $bg : 'background-danger'; 
								                									?>
								                									<div class="btn-circle <?= $bg ?>" title="<?= $title ?>">
								                										<span><?= $texto ?></span>
								                									</div>
								                								</td>
								                								<td class="text-center">
								                									<?php
																						$bg    = '';
																						$title = Model_Documento::$vigencias_documento[$aDocumentos[$documento['docu_id']]['_docu_vigencia']];
								                										switch ($aDocumentos[$documento['docu_id']]['_docu_vigencia']) {
								                											case 1: $bg = 'background-success'; break;
								                											case 3: $bg = 'background-warning'; break;
								                											case 5: $bg = 'background-danger';  break;
								                										}
								                									?>
								                									<?php if ($aDocumentos[$documento['docu_id']]['_docu_estado']): ?>
								                										<div class="btn-circle <?= $bg ?>" title="<?= $title ?>"></div>
								                									<?php else: ?>
								                										-
								                									<?php endif ?>
								                								</td>-->
								                							</tr>

																		<?php endif ?>

						                							<?php endforeach ?>
						                						</tbody>
						                					</table>
						                				<?php else: ?>
						                					<div class="alert alert-warning">
						                						No hay documentos
						                					</div>
						                				<?php endif ?>
						                			</td>
						                			<td width="30%">
						                				<?= $aMedios[$aume_id]['aume_observacion'] ?>
						                			</td>
				                				</tr>
				                				<tr>
													<td colspan="3">
														<?= $aMedios[$aume_id]['aume_consideracion'] ?>
													</td>
												</tr>
				                			<?php 
				                				$i++;
				                				$rowsMedioFactor--;
				                				$rowsMedioEstandar--;
				                			?>

				                		<?php endforeach ?>

									<?php endforeach ?>

								<?php endforeach ?>

			                <?php endforeach ?>

						<?php endforeach ?>
					</tbody>
				</table>
			</div>

		</section>
	</aside>
</section>

<style type="text/css">
	.content-actions{
		position: absolute;
	    width: 100%;
	    top: 0;
	    left: 0;
	    padding: 0 15px !important;
	}
	.treeview{

	}
	.header-title{
		line-height: 34px;
	}
	.treeview > a{
		position: relative;
	}
	.treeview > a > i:first-child{
	    float: left;
	    line-height: 20px;
	}
	.treeview > a > p{
	    display: inline-block;
	    margin: 0;
	    max-width: 150px;
	    white-space: nowrap;
	    overflow: hidden;
	    text-overflow: ellipsis;
	}
	.treeview > a > i:last-child{
	    margin-top: 3px; */
	}
</style>