<aside class="content-side">
	<!-- Main content -->
	<section class="content">

		<section class="content-actions">
			<div class="row">
				<div class="col-sm-6">
					<div class="text-left">
						<a href="/formatos" class="btn btn-flat btn-primary">
							<i class="fa fa-caret-left"></i> Volver a Formatos
						</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="text-right">
						<a href="<?= $buttonData['path'].(URL::query() ? URL::query().'&' : '?') ?>download=pdf" class="btn btn-default btn-flat">
							<i class="fa fa-download"></i> PDF
						</a>
						<a href="<?= $buttonData['path'].(URL::query() ? URL::query().'&' : '?') ?>download=excel" class="btn btn-default btn-flat">
							<i class="fa fa-file-text"></i> Excel
						</a>
					</div>
				</div>
			</div>
		</section>

		<section class="content-filters">
			<div class="box box-primary">
				<div class="box-header">
					<h4 class="box-title" data-toggle="_collapse" data-parent="#accordion" href="#collapse-searches">
						Fitros
					</h4>
					<div class="box-tools pull-right">
						<button class="btn btn-flat btn-primary btn-xs" data-widget="collapse">
							|<i class="fa fa-minus"></i>
						</button>
						<button class="btn btn-flat btn-primary btn-xs" data-widget="remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
				</div>
				
				<div class="box-body">
					<form action="?" method="GET" role="form">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Medio de Verificación</label>
									<input type="text" name="documento" class="form-control <?= (isset($requestParams['documento']) && $requestParams['documento']) ? 'input-active' : '' ?>" value="<?= isset($requestParams['documento']) ? $requestParams['documento'] : '' ?>" placeholder="Buscar documento">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group <?= (isset($requestParams['esta_codigo']) && $requestParams['esta_codigo']) ? 'select2-active' : '' ?>">
									<label>Estándar</label>
									<select name="esta_codigo" class="form-control select2">
										<option value="">-- Seleccione Estándar --</option>
										<?php foreach ($aEstandares as $estandar): ?>
											<?php if (isset($requestParams['esta_codigo']) && $requestParams['esta_codigo'] == $estandar['esta_codigo']): ?>
												<option value="<?= $estandar['esta_codigo'] ?>" selected="selected"><?= $estandar['esta_titulo'] ?></option>
											<?php else: ?>
												<option value="<?= $estandar['esta_codigo'] ?>"><?= $estandar['esta_titulo'] ?></option>
											<?php endif ?>
										<?php endforeach ?>
									</select>
								</div>
							</div>
							<div class="col-sm-1">
								<label class="invisible">Buscar</label>
								<button type="submit" class="btn btn-primary btn-block">
									<i class="fa fa-search"></i> Filtrar
								</button>
							</div>
							<div class="col-sm-1">
								<label class="invisible">Buscar</label>
								<a href="/formatos/documentosmedioverificacion" class="btn btn-default btn-block">
									<i class="fa fa-eraser"></i> Limpiar
								</a>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label>Vigencia</label>
									<input type="date" name="vig_desde" id="vig_desde" class="form-control unstyled <?= (isset($requestParams['vig_desde']) && $requestParams['vig_desde']) ? 'input-active' : '' ?>" value="<?= isset($requestParams['vig_desde']) ? $requestParams['vig_desde'] : '' ?>">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label class="invisible">Vigencia</label>
									<input type="date" name="vig_hasta" id="vig_hasta" class="form-control unstyled <?= (isset($requestParams['vig_hasta']) && $requestParams['vig_hasta']) ? 'input-active' : '' ?>" value="<?= isset($requestParams['vig_hasta']) ? $requestParams['vig_hasta'] : '' ?>">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label>Holgura</label>
									<input type="number" min="0" step="1" max="10000" name="olg_desde" id="olg_desde" class="form-control form-control <?= (isset($requestParams['olg_desde']) && ($requestParams['olg_desde'] || $requestParams['olg_desde'] === '0') ) ? 'input-active' : '' ?>" value="<?= isset($requestParams['olg_desde']) ? $requestParams['olg_desde'] : '' ?>" placeholder="Desde">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label class="invisible">Holgura</label>
									<input type="number" min="0" step="1" max="10000" name="olg_hasta" id="olg_hasta" class="form-control <?= (isset($requestParams['olg_hasta']) && $requestParams['olg_hasta']) ? 'input-active' : '' ?>" value="<?= isset($requestParams['olg_hasta']) ? $requestParams['olg_hasta'] : '' ?>" placeholder="Hasta">
								</div>
							</div>
							<div class="col-sm-4">
								
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>

		<section class="content-render">
			
			<header>
				<h3 class="text-center title">
					Estatus - Vigencia de los Medios de Verificación de la Autoevaluación al <?= date('d/m/Y') ?>
				</h3>
				<br>
				<h4 class="text-center subtitle">
				    <strong><?= $oInstitucion->inst_nombre ?></strong>
				</h4>
				<br>
			</header>

			<div class="box-header pagination-top">
			    <?= $pagination ?>
			</div>

			<section>
				<div class="table-responsive">
					<table class="table table-hover table-bordered table-center" border="1">
						<thead>
							<tr>
								<th rowspan="2" class="success">N°</th>
								<th rowspan="2" class="success">Fichas</th>
								<th rowspan="2" class="success text-nowrap">Estado</th>
								<th rowspan="2" class="success text-nowrap">Nivel de Calidad</th>
								<th rowspan="2" class="success">Indicadores relacionados</th>
								<th colspan="4" class="success">Periodo de Vigencia</th>
								<th rowspan="2" class="success">Plazo requerido en <br> Días para Actualización</th>
								<th rowspan="2" class="success">Días restantes <br> para perdida de vigencia</th>
								<th rowspan="2" class="success">Sugerencia</th>
							</tr>
							<tr>
								<th class="success">Desde</th>
								<th class="success">Hasta</th>
								<th class="success">Días</th>
								<th class="success">Edad Actual</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1; ?>
							<?php foreach ($oDocumentos as $docu_id => $aDocumentos[]): ?>
								<tr>
									<td><?= $i ?><?php $i++ ?></td>
									<td>
										<a href="/file/<?= $aDocumentos[$docu_id]['docu_path'] ?>" title="<?= $docu_id ?>" download>
											<?= $aDocumentos[$docu_id]['docu_titulo'] ?>
										</a>
									</td>
									<td class="text-nowrap"><?= $aDocumentos[$docu_id]['rd_estado'] ?></td>
									<td class="text-nowrap"><?= $aDocumentos[$docu_id]['rd_calidad'] ?></td>
									<td><?= $aDocumentos[$docu_id]['rd_ind_form'] ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_periodo_desde'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_periodo_hasta'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_periodo_dias'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_edad'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_plazo_estimado'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['dr_olgura'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['dr_sugerencia'] : '-' ?></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</section>

			<div class="box-footer pagination-bottom">
			    <?= $pagination ?>
			</div>

		</section>

	</section>
</aside>