<aside class="content-side">
	<!-- Main content -->
	<section class="content">

		<section class="content-actions">
			<div class="row">
				<div class="col-sm-6">
					<div class="text-left">
						<a href="/formatos" class="btn btn-flat btn-primary">
							<i class="fa fa-caret-left"></i> Volver a Formatos
						</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="text-right">
						<a href="<?= $buttonData['path'].(URL::query() ? URL::query().'&' : '?') ?>download=pdf" class="btn btn-default btn-flat">
							<i class="fa fa-download"></i> PDF
						</a>
						<a href="<?= $buttonData['path'].(URL::query() ? URL::query().'&' : '?') ?>download=excel" class="btn btn-default btn-flat">
							<i class="fa fa-file-text"></i> Excel
						</a>
					</div>
				</div>
			</div>
		</section>

		<section class="content-render">
			
			<header>
				<h3 class="text-center title">
					Documentos Históricos al <?= date('d/m/Y') ?>
				</h3>
				<br>
				<h4 class="text-center subtitle">
				    <strong><?= $oInstitucion->inst_nombre ?></strong>
				</h4>
				<br>
			</header>

			<div class="box-header pagination-top">
			    <?= $pagination ?>
			</div>

			<section>
				<div class="table-responsive">
					<table class="table table-hover table-bordered table-center" border="1">
						<thead>
							<tr>
								<th rowspan="2" class="success">N°</th>
								<th rowspan="2" class="success">Medio de Verificación</th>
								<th rowspan="2" class="success">Estado</th>
								<th rowspan="2" class="success">Nivel de Calidad</th>
								<th rowspan="2" colspan="2" class="success">Indicadores/Formatos relacionados</th>
								<th colspan="4" class="success">Periodo de Vigencia</th>
								<th rowspan="2" class="success">Plazo requerido en <br> Días para Actualizacion</th>
								<th rowspan="2" class="success">Fecha de Reemplazo</th>
								<th rowspan="2" class="success">Días restantes <br> para perdida de vigencia</th>
							</tr>
							<tr>
								<th class="success">Desde</th>
								<th class="success">Hasta</th>
								<th class="success">Días</th>
								<th class="success">Edad</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1; ?>
							<?php foreach ($oDocumentos as $docu_id => $aDocumentos[]): ?>
								<tr>
									<td><?= $i ?><?php $i++ ?></td>
									<td>
										<a href="/file/<?= $aDocumentos[$docu_id]['docu_path'] ?>" title="<?= $docu_id ?>" download>
											<?= $aDocumentos[$docu_id]['docu_titulo'] ?>
										</a>
									</td>
									<td><?= $aDocumentos[$docu_id]['rd_estado'] ?></td>
									<td><?= $aDocumentos[$docu_id]['rd_calidad'] ?></td>
									<td><?= $aDocumentos[$docu_id]['rd_ind_form'] ?></td>
									<td><?= $aDocumentos[$docu_id]['formato_nombre'] ? $aDocumentos[$docu_id]['formato_nombre'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_periodo_desde'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_periodo_hasta'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_periodo_dias'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_edad'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_plazo_estimado'] : '-' ?></td>
									<td><?= $aDocumentos[$docu_id]['dh_fecha_reemplazo'] ?></td>
									<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['dh_diferencia_dias'] : '-' ?></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</section>

			<div class="box-footer pagination-bottom">
			    <?= $pagination ?>
			</div>

		</section>

	</section>
</aside>