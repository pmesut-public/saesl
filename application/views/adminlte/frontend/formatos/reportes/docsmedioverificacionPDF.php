<header>
	<h3 class="text-center title">
		Estatus - Vigencia de los Medios de Verificación de la Autoevaluación al <?= date('d/m/Y') ?>
	</h3>
	<br>
	<h4 class="text-center subtitle">
	    <strong><?= $oInstitucion->inst_nombre ?></strong>
	</h4>
	<br>
</header>
<section>
	<div class="table-responsive">
		<table class="table table-hover table-bordered table-center" border="1">
			<thead>
				<tr>
					<th rowspan="2" class="success">N°</th>
					<th rowspan="2" class="success">Fichas</th>
					<th rowspan="2" class="success text-nowrap">Estado</th>
					<th rowspan="2" class="success text-nowrap">Nivel de Calidad</th>
					<th rowspan="2" class="success">Indicadores relacionados</th>
					<th colspan="4" class="success">Periodo de Vigencia</th>
					<th rowspan="2" class="success">Plazo requerido en <br> Días para Actualización</th>
					<th rowspan="2" class="success">Días restantes <br> para perdida de vigencia</th>
					<th rowspan="2" class="success">Sugerencia</th>
				</tr>
				<tr>
					<th class="success">Desde</th>
					<th class="success">Hasta</th>
					<th class="success">Días</th>
					<th class="success">Edad Actual</th>
				</tr>
			</thead>
			<tbody>
				<?php $i=1; ?>
				<?php foreach ($oDocumentos as $docu_id => $aDocumentos[]): ?>
					<tr>
						<td><?= $i ?><?php $i++ ?></td>
						<td>
							<a href="/file/<?= $aDocumentos[$docu_id]['docu_path'] ?>" title="<?= $docu_id ?>" download>
								<?= $aDocumentos[$docu_id]['docu_titulo'] ?>
							</a>
						</td>
						<td class="text-nowrap"><?= $aDocumentos[$docu_id]['rd_estado'] ?></td>
						<td class="text-nowrap"><?= $aDocumentos[$docu_id]['rd_calidad'] ?></td>
						<td><?= $aDocumentos[$docu_id]['rd_ind_form'] ?></td>
						<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_periodo_desde'] : '-' ?></td>
						<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_periodo_hasta'] : '-' ?></td>
						<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_periodo_dias'] : '-' ?></td>
						<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_edad'] : '-' ?></td>
						<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['rd_plazo_estimado'] : '-' ?></td>
						<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['dr_olgura'] : '-' ?></td>
						<td><?= $aDocumentos[$docu_id]['estados'] == Model_Documento::CONCLUIDO ? $aDocumentos[$docu_id]['dr_sugerencia'] : '-' ?></td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</section>

<style type="text/css">
	.title{
		margin-top: 20px;
	}
	.subtitle{

	}
	.text-center{
		text-align: center;
	}
	.table-center th,
	.table-center td{
	  	vertical-align: middle !important;
	  	text-align: center !important;
	}
</style>