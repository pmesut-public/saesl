<script>
    window.__CONTEXT__ = <?= $context ?>;
	
    window.__INITIAL_STATE__ = <?= $initial ?>;
</script>

<aside class="right-side strech">
	<!-- Content Header (Page header) -->
	<section class="content-header">		
		
		<?= Breadcrumb::build() ?>
		
	</section>
	
	<!-- Main content -->
	<section class="content">
    <div id="root"></div>
	</section><!-- /.content -->

</aside><!-- /.right-side -->
