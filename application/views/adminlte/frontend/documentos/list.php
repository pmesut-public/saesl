<script>
    window.__CONTEXT__ = {};
    window.__INITIAL_STATE__ = <?= $result ?>;
</script>

<aside class="right-side strech">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<a href="/autoevaluaciones<?= URL::query() ?>">Mis autoevaluaciones</a> |<!-- <a href="/eventos<?= URL::query() ?>">Eventos</a> |--> Gestión de documentos
			<small></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<!-- Main content -->
	<section class="content">
    <div id="root"></div>
	</section><!-- /.content -->
</aside><!-- /.right-side -->
