<form class="form-sign signin" role="form" method="post">
	
	<h3 class="heading">
		Sistema de Autoevaluación de la Educación Superior - 
		<a href="#">SAES licenciamiento</a>
	</h3>
	
	<img src="/media/saes/img/logo.jpg" class="logo img-responsive" alt="<?= __($site_title) ?>">
	
	<?php if ($error): ?>
		<div class="alert alert-danger"><?= $error ?></div>
	<?php endif ?>
	<hr>
	
	<p class="help-block"><?= __($message) ?></p>
	<div class="form-group">
		<input name="email" type="text" class="form-control control-sign" placeholder="<?= __('Ingrese su correo') ?>" required autofocus>
	</div>
	
	<button class="btn btn-lg btn-primary btn-block" type="submit"><?= __('Enviar') ?></button><br>
	
	<div class="form-group actions text-center">
		<a class="btn btn-link" href="/welcome"><?= __('Ingresar') ?></a> |
		<a class="btn btn-link" href="/welcome/register"><?= __('Registrarse') ?></a>
	</div>
</form>
