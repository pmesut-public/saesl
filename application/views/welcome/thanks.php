<form class="form-sign signin" role="form" method="post">
	
	<h3 class="heading">
		Sistema de Autoevaluación de la Educación Superior - 
		<a href="#">SAES licenciamiento</a>
	</h3>
	
	<img src="/media/saes/img/logo.jpg" class="logo img-responsive" alt="<?= __($site_title) ?>">
	
	<hr>
	
	<p class="help-block text-center"><?= __('Gracias por registrarse en el sistema.'
		//. ' Ahora puede acceder con su usuario y clave.'
		. ' Un administrador debe aprobar su registro.'
		. ' Le enviaremos un correo con la información de ingreso.') ?></p>
	
	<div class="form-group actions text-center">
		<a class="btn btn-link" href="/welcome"><?= __('Ingresar') ?></a> |
		<a class="btn btn-link" href="/welcome/register"><?= __('Registrarse') ?></a>
	</div>
</form>
