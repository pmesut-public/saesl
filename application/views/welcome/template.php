<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= __($site_title) ?></title>
		<!-- Developed by Sergio Melendez -->
                
		<?= $styles ?>

	</head>
	<body>
		<div class="container">

			<?= $content ?>
			
			<?= $footer ?>

		</div>

		<?= $scripts ?>
		
	</body>
</html>
