<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= __($site_title) ?></title>
		<!-- Developed by Daniel Zamora <danielzam.c@gmail.com> -->
		<?= HTML::style('/media/saes/css/normalize.css') ?>
        <?= HTML::style('/media/saes/css/signLogin.css') ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <?= HTML::script('/media/saes/js/html5shiv.js') ?>
            <?= HTML::script('/media/saes/js/respond.min.js') ?>            
        <![endif]-->
	</head>
	<body>
		<div class="container">
                    
            <?= $header ?>
                    
			<?= $content ?>
			
			<div class="Container-bottom">
                <div class="Container-bottom-wrap">
                    <div class="centered-box">
                        <p>Jr. Rousseau N° 465 - San Borja / Central (01) 605-8960</p>
                        <p>procalidad@procalidad.gob.pe / ProCalidad - © Copyright 2014 - <?= date('Y')?></p>
                    </div>
                </div>
            </div>
		</div>

		<?= HTML::script('media/saes/js/jquery.min.js') ?>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <?= HTML::script('media/saes/js/sign.js') ?>       

	</body>
</html>
