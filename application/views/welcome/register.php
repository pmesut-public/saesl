<form class="form-sign signup" role="form" method="post" onsubmit="return validateForm();" enctype="multipart/form-data">
	
	<h3 class="heading">
		Sistema de Autoevaluación de la Educación Superior<br>
		<a href="/">SAES licenciamiento</a>
	</h3>
	
	<img src="/media/saes/img/logo.jpg" class="logo img-responsive" alt="<?= __($site_title) ?>">
	
	<?php if ($error): ?>
		<div class="alert alert-danger input-flat"><?= $error ?></div>
	<?php endif ?>
	<hr>
	<!--input type="text" name="foo" class="form-control input-flat foo" 
		placeholder="Nombres" pattern="\d{5,}" required minlength="5" title="Sólo números (mínimo 5 caracteres)"-->
	
	<fieldset>
		<legend>Registro de Usuarios y Comités de Calidad</legend>
		<div class="row">
			<div class="col-sm-6 form-group">
				<select name="tiin_id" class="form-control placeholder ajax input-flat" required
						data-action="getInstitution" data-target="#signup-institution">
					<?= HTML::default_option('Seleccione Tipo de Institución') ?>
					<?php foreach ($aTipoInstitucion as $key => $val): ?>
						<option value="<?= $key ?>"><?= $val ?></option>
					<?php endforeach ?>
				</select>
			</div>
			<div class="col-sm-6 form-group">
				<select name="inst_id" class="form-control placeholder ajax input-flat select2" required 
						id="signup-institution" 
						data-action="getObac" data-target="#signup-obac">
					<?= HTML::default_option('Seleccione institución') ?>
				</select>
			</div>
			<div class="col-sm-6 form-group">
				<select name="obac_id" class="form-control placeholder ajax input-flat" required
						id="signup-obac" 
						data-action="getCareerObac" data-target="#signup-career">
					<?= HTML::default_option('Seleccione Objeto de Evaluación') ?>
				</select>
                <input type="hidden" name="objeto_eval" id="objeto_eval" />
			</div>
			<div class="col-sm-6 form-group">
                <div class="content-carr_id">
					<select name="carr_id" class="form-control control-sign placeholder input-flat select2" required id="signup-career">
						<?= HTML::default_option('Seleccione carrera') ?>
					</select>
             	</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 form-group">
				<select name="moda_id" class="form-control placeholder input-flat" required>
					<?= HTML::default_option('Seleccione el estado') ?>
                    <?php //= HTML::default_option('Seleccione el estado de acreditación') ?>
					<?php foreach ($aModalidad as $key => $val): ?>
						<option value="<?= $key ?>"><?= $val ?></option>
					<?php endforeach ?>
				</select>
			</div>
            <div class="col-sm-6 form-group">
                <span class="btn btn-flat btn-success" style="cursor:pointer;" data-toggle="modal" data-target="#rdi">	Registro de datos institucionales
                </span>
			</div>
		</div>
        <div class="row">
            <div class="col-sm-12 form-group">
                <label for="file">
                	<span style="color:#FF0000">(Opcional)</span> Adjuntar formato con datos institucionales
               	</label>
                <input name="filedi" type="file" id="filedi" class="form-control input-flat" accept=".xls, .xlsx">
            </div>
        </div>
		
		<!--p class="help-block">
			Si no encuentra su carrera registrada, puede solicitarla <a href="/welcome/carrera">aquí</a>
		</p-->
		
	</fieldset>
	
	<fieldset>
		<legend><?= __('Comité de Calidad') ?></legend>
		<div class="table-responsive">
			<table class="table committee">
				<thead>
					<tr>
						<th><?= __('Contacto') ?></th>
						<th><?= __('Nombres') ?></th>
						<th><?= __('Apellidos') ?></th>
						<th><?= __('Cargo') ?></th>
						<th><?= __('Correo') ?></th>
						<th><?= __('Teléfono') ?></th>
						<th class="th-actions"></th>
					</tr>
				</thead>
				<tbody>
					<?php for ($i = 0; $i < 3; $i++): ?>
					<tr>
						<td>
							<div class="radio text-center">
								<input name="contact" value="<?= $i ?>" type="radio" required>
							</div>
						</td>
						<td>
							<input type="text" name="first_names[]" class="form-control input-flat" 
							placeholder="Nombres" required />
						</td>
						<td>
							<input type="text" name="last_names[]" class="form-control input-flat" 
							placeholder="Apellidos" required />
						</td>
						<td>
							<input type="text" name="positions[]" class="form-control input-flat" 
							placeholder="Cargo" required />
						</td>
						<td>
							<input type="email" name="emails[]" class="form-control input-flat" 
							placeholder="Correo" required />
						</td>
						<td>
							<input type="text" name="phones[]" class="form-control input-flat input_phone"
							placeholder="Teléfono" title="Solo números (mínimo 7 dígitos)" 
							pattern="\d{7,}" minlength="7" required />
						</td>
						<td class="td-actions">
							<button type="button" class="btn btn-xs btn-flat btn-danger remove_tr" title="Eliminar miembro">
								<i class="glyphicon glyphicon-remove"></i>
							</button>
						</td>
					</tr>
					<?php endfor ?>
				</tbody>
			</table>
		</div>
		<button class="btn btn-success pull-right btn-flat" type="button" title="<?= __('Agregar miembro') ?>" id="signup-member-add">
			<span class="glyphicon glyphicon-plus"></span> Agregar miembro
		</button>
		<div class="clearfix"></div><br>
		<div class="form-group">
			<label for="file">Adjuntar Documento que autoriza la Creación del Comité de Autoevaluación/Calidad</label>
			<input name="file" type="file" id="file" class="form-control input-flat" required>
		</div>
	</fieldset>
	
	<fieldset>
		<legend><?= __('Autenticación') ?></legend>
		
		<div class="row">
			<div class="col-sm-4 form-group">
				<input name="username" type="text" class="form-control ajax input-flat" 
					placeholder="<?= __('Elija un Usuario') ?>" data-action="/welcome/uniqueUser"
					title="Sólo números, letras o guiones (mínimo 4 caracteres)" 
					pattern="[a-zA-Z0-9\-_]{4,}" minlength="4" required />
			</div>
			<div class="col-sm-4 form-group">
				<input name="password" type="password" class="form-control input-flat" 
					id="password" 
					placeholder="<?= __('Elija una contraseña') ?>" 
					title="(mínimo 4 caracteres)"
					pattern=".{4,}" minlength="4" required />
				<p class="text-danger hidden">Las contraseñas no coinciden</p>
			</div>
			<div class="col-sm-4 form-group">
				<input name="confirm_password" type="password" class="form-control input-flat" 
					id="password_confirm" 
					placeholder="<?= __('Confirme su contraseña') ?>" 
					title="(mínimo 4 caracteres)"
					pattern=".{4,}" minlength="4" required />
			</div>
		</div>
	</fieldset>
	
	<div class="g-recaptcha" data-sitekey="6LfW4QkoAAAAAG3dNBkCZP_2fYeyTo3xx-Zh18wa"></div>
	<br/>
	<button class="btn btn-lg btn-primary btn-block btn-flat" type="submit"><?= __('Registrarse') ?></button><br>
	
	<div class="form-group actions text-center">
		<a class="btn btn-link" href="/"><?= __('Volver') ?></a>
	</div>
</form>

<!-- Modal -->
<div aria-labelledby="myModalLabel" class="modal fade" id="rdi" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Registro de datos institucionales</h4>
            </div>
            <div class="modal-body">
                Aca va la explicación de todo lo referido al excel y el registro de datos institucionales.
                <p>
                    <a href="../../media/formato-datos.xlsx">
                        Descargar excel con formato
                    </a>
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" type="button">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
	.heading{
		text-align: center;
		line-height: 32px;
		margin-top: 0;
	}
</style>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script>
    function validateForm() {
        var response = grecaptcha.getResponse();
        if (response.length === 0) {
            // reCAPTCHA not verified, display an error message or take action accordingly
            alert("Por favor completa el reCAPTCHA.");
            return false; // Prevent form submission
        }
        // reCAPTCHA verified, proceed with form submission
        return true;
    }
</script>