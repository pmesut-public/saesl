<div class="Container-center">
    <div class="Container-center-left" style="padding-top: 131px;">
        <div class="Container-center-left-wrap">
            <div class="centered-box">
                <div class="Container-center-left-wrap-offset" style="padding-bottom: 30px;">
                    <h1>SAES licenciamiento</h1>
                    <h3>Sistema de Autoevaluación<br> de la Educación Superior</h3>
                    <form id="form-login" role="form" method="post" onsubmit="return validateForm();">
                        <input type="text" name="username" placeholder="<?= __('Usuario') ?>" required autofocus>
                        <input name="password" type="password" placeholder="<?= __('Contraseña') ?>" required>
                        <div class="remember-forgot">
                            <span>
                                <label><input type="checkbox" value="remember-me"> <?= __('Recordarme') ?></label>
                            </span>
                            <a href="/welcome/forgot"><?= __('Olvidé mi contraseña') ?></a>
                            <div class="fixed-box"></div>
                        </div>
                        <br/>
                        <div class="g-recaptcha" data-sitekey="6LfW4QkoAAAAAG3dNBkCZP_2fYeyTo3xx-Zh18wa"></div>
                        <button type="submit" id="btn-register" ><?= __('Ingresar') ?></button>                     
                        <p>¿Aún no tienes una cuenta? <a href="/welcome/register">Regístrate aquí</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="Container-center-right" style="padding-top: 151px;">
        <div class="Container-center-right-wrap">
            <div class="centered-box">
                <div class="Container-center-right-wrap-offset">
                    <p class="how-to-register"><?= $titu ?></p>
                    <iframe src="https://www.youtube.com/embed/<?= $url ?>?controls=1" frameborder="0"></iframe>
                    <p class="know-more">Conoce más del SAES licenciamiento</p>
                    <div class="line-stippled"></div>
                    <p class="on-saes">Sobre el <span>SAES licenciamiento</span></p>
                    <p class="paragraph">
                        Es una herramienta de DIAGNOSTICO de libre acceso, está basada en el Modelo de Licenciamiento de Universidades desarrollado por la SUNEDU. Apoya a las UNIVERSIDADES  en su proceso de AUTOEVALUACIÓN  y como instrumento de REGISTRO de la información generada en este proceso de LICENCIAMIENTO.
                    </p>
                </div>
                <div style="margin: 30px 0 20px 0;">
                    <a class="boton-video" href="/welcome/index/b-HFBP6Ne5k">Registro</a>
                    <a class="boton-video" href="/welcome/index/eKQqPRAj2m4">Autoevaluación</a>
                    <!--<a class="boton-video" href="/welcome/index/FsQSUfA5D-w">Autoevaluación Logro de Estándar</a>-->
                    <!--<a class="boton-video" href="/">Consultas</a>-->
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script>
    function validateForm() {
        var response = grecaptcha.getResponse();
        if (response.length === 0) {
            // reCAPTCHA not verified, display an error message or take action accordingly
            alert("Por favor completa el reCAPTCHA.");
            return false; // Prevent form submission
        }
        // reCAPTCHA verified, proceed with form submission
        return true;
    }
</script>