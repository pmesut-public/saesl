<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '079061cdd74f0f594506e329fcf60adf9bf1d417',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '079061cdd74f0f594506e329fcf60adf9bf1d417',
            'dev_requirement' => false,
        ),
        'tecnickcom/tcpdf' => array(
            'pretty_version' => '6.6.2',
            'version' => '6.6.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../tecnickcom/tcpdf',
            'aliases' => array(),
            'reference' => 'e3cffc9bcbc76e89e167e9eb0bbda0cab7518459',
            'dev_requirement' => false,
        ),
    ),
);
