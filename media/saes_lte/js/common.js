/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

// Saes object
var Saes = {};

/*
 * The functions inside this block will get executed AFTER page load, 
 * overwritting previous functionality as event handlers, if they exist
 */
$(function() {
	
    // Add collapse and remove events to boxes =================================
	
	$("[data-widget='collapse']").unbind().click(function() {
		//Find the box parent        
		var box = $(this).parents(".box").first();
		//Find the body and the footer
		var bf = box.find(".box-body, .box-footer");
		if (!box.hasClass("collapsed-box")) {
			box.addClass("collapsed-box");
			//Convert minus into plus
			$(this).children(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
			bf.slideUp(200);
		} else {
			box.removeClass("collapsed-box");
			//Convert plus into minus
			$(this).children(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
			bf.slideDown(200);
		}
	});

	$("[data-widget='remove']").unbind().click(function() {
		//Find the box parent        
		var box = $(this).parents(".box").first();
		box.slideUp(200);
	});
        
        if (jQuery.fn.select2) {
            $('.select2').select2();
        }
	
});

/*
 * Functions inside this block will get executed inmediatly 
 * (i.e. this callbacks will get registered BEFORE page load)
 */
(function($) {
	"use strict";
	
	// Alerts
	
	setTimeout(function () { $('.alert.fade.in').removeClass('in'); }, 2000);
	
	// Sidebar =================================================================
	
	$('.sidebar-menu li > a').each(function() {
		var item = this;
		
		//if (item.href == window.location.href) {
		if ($(item).attr('href') == window.location.pathname) {
			$(item).parent().addClass('active');
			$(item).parents('.treeview').addClass('active');
		}
	});
	
	// Select ajax =============================================================
	
	$('select.ajax').change(getOptions);
	$('select.ajax').each(getOptions);
	
	function getOptions() {
		var $this = $(this),
			val = $this.val(),
			path = $this.data('path'),
			url = path + val,
			$target = $($this.data('target'));
		
		$.get(url).done(function (data) {
			setOptions(data, $target);
		});
	}
	
	function setOptions(data, $target) {
		$target.html(data);

		if ($target.attr('data-selected')) {
			
			$target
				.find('option[value=' + $target.attr('data-selected') + ']')
				.prop('selected', true);
		}
	}
	
	// Data confirm ============================================================
	
	$('[data-confirm]').click(function () {
		var msg = $(this).data('confirm') || 'Está usted seguro(a)?';
		return confirm(msg);
	});
	
	// Reports =================================================================
	
	$('.highcharts').each(function () {
		var $this = $(this),
			//json = $this.data('json');
			json = JSON.parse($this.attr('data-json'), reviver);
			console.log(json);
		
		$this.highcharts(json);
		
	});
	
	function reviver (key, value) {
		var x;
		
		//console.log(key, value);
		if (key && typeof(value) === 'string' && (x = value.match(/function(?:[\s\S]*?){([\s\S]*)}/))) {
			//console.log(x[1]);
			return Function(x[1]);
		}
		return value;
	}
	
	$('.highcharts_actions a[data-action]').click(function (e) {
		e.preventDefault();
		
		var $this = $(this),
			action = $this.data('action'),
			highcharts = $this.closest('.box-body').find('.highcharts').highcharts();
			//console.log(this);
		
		switch(action) {
			case 'print': highcharts.print(); 
				break;
			case 'pdf': highcharts.exportChart({type:'application/pdf'});
				break;
			case 'png': highcharts.exportChart();
				break;
		}
	});
	
}(jQuery));
