/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Saes object
var Saes = {};

/*
 * The functions inside this block will get executed AFTER page load, 
 * overwritting previous functionality as event handlers, if they exist
 */
$(function() {
	/*     
     * Add collapse and remove events to boxes
     */
	$("[data-widget='collapse']").unbind().click(function() {
		//Find the box parent        
		var box = $(this).parents(".box").first();
		//Find the body and the footer
		var bf = box.find(".box-body, .box-footer");
		if (!box.hasClass("collapsed-box")) {
			box.addClass("collapsed-box");
			//Convert minus into plus
			$(this).children(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
			bf.slideUp(200);
		} else {
			box.removeClass("collapsed-box");
			//Convert plus into minus
			$(this).children(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
			bf.slideDown(200);
		}
	});

	$("[data-widget='remove']").unbind().click(function() {
		//Find the box parent        
		var box = $(this).parents(".box").first();
		box.slideUp(200);
	});
	
	/*
	 * Needs 
	 */
	var context = '.table-autos tbody';
	$('.iCheck-helper', context).click(function (evt) {
		
		var $this = $(this);
		var element = $this.prev('input[type="checkbox"]');
		var parent = $this.closest('tr');
		
		if (element.length) {
			
			var pIndex = parent.index();
			
			if (evt.shiftKey) {
				
				if (Saes.last >= 0) {
					
					var //last_checked = $('tbody input[type="checkbox"]:nth(' + Saes.last + ')').prop('checked'),
						current_checked = ! $('input[type="checkbox"]:nth(' + pIndex + ')', context).prop('checked'),
						// here, current checkbox has already been clicked, that's why we use the opposite
						
						isChecked = current_checked ? 'uncheck' : 'check';
					
					$('input[type="checkbox"]', context).each(function(i, e) {
						
						if ((i - pIndex) * (i - Saes.last) <= 0) {
							// this checkbox is in the range
							$(e).iCheck(isChecked);
						}
					});
				}
				
			} else {
				//Saes.last = pIndex;
			}
			
			Saes.last = pIndex;
			
		}
	});
	
	$('.committee input').iCheck('destroy');
	
});

/*
 * Functions inside this block will get executed inmediatly 
 * (i.e. this callbacks will get registered BEFORE page load)
 */
(function($) {
	"use strict";
	
	// Sidebar =================================================================
	// /template/sidebar
	
	$('.sidebar-menu li > a').each(function() {
		var item = this;
		
		//if (item.href == window.location.href) {
		if ($(item).attr('href') == window.location.pathname) {
			$(item).parent().addClass('active');
			$(item).parents('.treeview').addClass('active');
		}
	});
	
	// All toggled =============================================================
	// /admin/autoevaluacion/list
	
	$('.input-all').on('ifToggled', function () {
		
		if ($(this).prop('checked'))
			$('.input-autoevaluacion').iCheck('check');
		else
			$('.input-autoevaluacion').iCheck('uncheck');
		
		Saes.last = $(this).index();
		console.log(Saes.last);
	});
	
	// Btn massive sms status ==================================================
	// /admin/autoevaluacion/list
	
	$('body').on('ifToggled', setBtnMassiveStatus);
	setBtnMassiveStatus();
	
	function setBtnMassiveStatus() {
		
		if ($('.input-autoevaluacion:checked').length) {
			$('.btn-mensaje-general').removeClass('disabled');
		} else {
			$('.btn-mensaje-general').addClass('disabled');
		}
		
	}
	
	// Btn massive sms click ===================================================
	// /admin/autoevaluacion/list
	
	$('.btn-mensaje-general').click(function (e) {
		e.preventDefault();
		
		var ids = $.map($('.input-autoevaluacion:checked'), function (e) {
			return $(e).data('id');
		});
		
		if (ids.length) {
			
			var path = $(this).attr('href') + '?ids=' + ids.join(',');
			sendMessages(path);
			
		}
		
	});
	
	// Send sms
	$('.btn-mensaje-comite').click(function (e) {
		e.preventDefault();
		
		var path = $(this).attr('href');
		
		sendMessages(path);
	});
	
	function sendMessages(path) {
		
		$('#myModal').modal('show');
		
		$.get(path)
			.done(function (data) {
				$('#myModal').html(data);
			});
	}
	
	// Select ajax =============================================================
	
	$('select.ajax').change(getOptions);
	$('select.ajax').each(getOptions);
	
	function getOptions() {
		var $this = $(this),
			val = $this.val(),
			path = $this.data('path'),
			url = path + val,
			$target = $($this.data('target'));
		
		$.get(url).done(function (data) {
			setOptions(data, $target);
		});
	}
	
	function setOptions(data, $target) {
		$target.html(data);

		if ($target.attr('data-selected')) {
			
			$target.find('option[value=' + $target.attr('data-selected') + ']').prop('selected', true);
		}
	}
	
	// Reports =================================================================
	// /admin/dashboard
	
	$('.flot-chart-content').each(function () {
		var $this = $(this),
			properties = $this.data('properties'),
			data = $this.data('json');
		
		console.log(data);
		console.log(properties);
		
		$.plot($this, data, properties);
	});
	
	// Committee member add ====================================================
	// /profile?change=1
	
	$('#signup-member-add').click(function () {
		var $table = $('.committee'),
			$tbody = $('tbody', $table),
			$row = $('tr:first', $tbody).clone(),
			n = $tbody.children().length;
		
		console.log(n);
		
		$row.find('input[type=radio]').iCheck('uncheck').val(n);
		$row.find('input[type=text]').val('').prop('required', false);
		$row.appendTo($tbody);
	});
	
	// Committee contact change
	
	$('.committee').on('change', 'input[type=radio]', changeContact);
	//changeContact.call($('.committee input[type=radio]:checked'));
	
	function changeContact () {
		var $tr = $(this).parents('tr');
		$('.committee').find('input[type=text]').prop('required', false);
		$tr.find('input[type=text]').prop('required', true);	
	}
	
	// New users add ===========================================================
	// /users
	
	$('#new-users-add').click(function () {
		var $table = $('.new-users'),
			$tbody = $('tbody', $table),
			$row = $('tr:first', $tbody).clone();
		
		$row.find('select').val('');
		$row.find('input[type=text]').val('');
		$row.appendTo($tbody);
	});
	
	// New users ajax
	
	$('.new-users').on('change', '.new-users-ajax', newUsersOptions);
	
	function newUsersOptions() {
		var $this = $(this),
			val = $this.val(),
			path = $this.data('path'),
			url = path + val,
			$target = $this.closest('tr').find($this.data('target'));
		
		$.get(url).done(function (data) {
			setOptions(data, $target);
		});
	}
	
	// New users remove
	
	$('.new-users').on('click', '.new-users-remove', function (e) {
		e.preventDefault();
		
		var $this = $(this),
			$row = $this.closest('tr');
		
		if ($row.index())
			$row.remove();
	});
	
	// Autoevaluacion evento ===================================================
	// /autoevaluaciones/nueva
	
	$('#auto_even_id').change(toggleTreeEstandares);
	$('#auto_even_id').each(toggleTreeEstandares);
	
	function toggleTreeEstandares() {
		var $this = $(this),
			val = $this.val(),
			$tree = $('#tree-estandares');
		
		if (val === '')
			return;
		
		if (val === '0') {
			$tree.find('select').prop('required', true).prop('disabled', false);
			$tree.find('.start-select-all').prop('disabled', false);
		} else {
			$tree.find('select').prop('required', false).prop('disabled', true);
			$tree.find('.start-select-all').prop('disabled', true);
			$tree.find('select').first().find('option').prop('selected', true);
			$tree.find('select').first().change();
		}
	}
	
	// Select ajax multiple ====================================================
	
	$('select.ajax-multiple').change(getOptionsMultiple);
	$('select.ajax-multiple').each(getOptionsMultiple);
	
	function getOptionsMultiple() {
		if ( ! $(this).val()) return;
		
		var $this = $(this),
			action = $this.data('action'),
			val = $this.val().join('-'),
			url = action + val + '?multiple=true',
			$target = $($this.data('target'));
		
		$.get(url).done(function (data) {
			var val = $('#auto_even_id').val();
			
			$target.html(data);
			console.log(val);
			if (val) {
				$target.find('option').prop('selected', true);
				$target.change();
			}
		});
	}
	
	// Select all
	
	$('.start-select-all').click(function () {
		var $select = $(this).parent().find('select');
		
		$select.find('option').prop('selected', true);
		if ($select.hasClass('ajax-multiple')) $select.each(getOptionsMultiple);
	});
	
	// assessment/actividades ==================================================
	
	// http://api.jqueryui.com/draggable/
	/*$('.estandares-list li').draggable({
		helper: 'clone',
		scope: 'estandares'
	});
	
	var droppableObject = {
		scope: 'estandares',
		hoverClass: "ui-state-active",
		drop: function (e, ui) {
			console.log(e);
			console.log(ui);
			
			var $this = $(this),
				$ul = $this.find('ul'),
				$li = ui.helper.clone().removeAttr('style').removeClass('ui-draggable-dragging'),
				id = $li.data('id'),
				$original = $('.estandares-list [data-id=' + id + ']'),
				count = $original.find('.label').html();
			
			//console.log($original);
		
			$li.find('.handle, .label').remove();
			
			//ui.helper.addClass('foo');
			
			if ( ! $ul.find('[data-id=' + id + ']').length)
			{
				$ul.append($li);
				$original.find('.label').html(++count).removeClass('label-danger').addClass('label-info');
			}
			
		}
	};
	
	// http://api.jqueryui.com/droppable/
	$('.actividades-list').droppable(droppableObject);
	
	$('.btn-add-actividad').click(function (e) {
		e.preventDefault();
		
		var	$panel = $('.panel-group'),
			has_new = $panel.find('.panel-new').length,
			$model = $('.activity-model .panel').clone();
		
		if ( ! has_new) {
			$panel.find('.panel-collapse.in').removeClass('in');
			$model.appendTo($panel);
			$model.find('.actividades-list').droppable(droppableObject);
		}
	});
	
	$('.panel-group').on('click', '.btn-remove-actividad', function (e) {
		e.preventDefault();
		
		var $this = $(this),
			action = $this.attr('href'),
			$parent = $this.closest('.panel'),
			$estandares = $parent.find('.actividades-list li'),
			is_new = $parent.hasClass('panel-new');
		
		if (is_new) {
			$estandares.each(removeEstandar);
			$parent.remove();
		}
		else {
			$.post(action).done(function (data) {
				
				if (data == 'ok') {
					$estandares.each(removeEstandar);
					$parent.remove();
				} else {
					alert(data);
				}
				
			});
		}
		
	});
	
	$('.panel-group').on('click', '.btn-remove-estandar', function (e) {
		e.preventDefault();
		
		var $this = $(this),
			$parent = $this.closest('li');
			
		$parent.each(removeEstandar);
		$parent.remove();
	});
	
	function removeEstandar (i, estandar) {
		console.log(estandar);
		
		var id = $(estandar).data('id'),
			$original = $('.estandares-list li[data-id=' + id + ']'),
			$counter = $original.find('.label'),
			count = $counter.html();
		
		$counter.html(--count);
		if (count == 0) $counter.removeClass('label-info').addClass('label-danger');
	}
	
	$('.panel-group').on('submit', 'form', function (e) {
		e.preventDefault();
		
		var $form = $(this),
			$fieldset = $form.children(),
			$list = $form.find('.actividades-list'),
			$button = $form.find('.btn-save-actividad'),
			action = $form.attr('action'),
			data = $form.serialize(),
			
			$panel = $form.closest('.panel'),
			is_new = $panel.hasClass('panel-new'),
			$a = $panel.find('a[data-toggle=collapse]'),
			$div = $panel.find('.panel-collapse'),
			$btn_delete = $form.find('.btn-remove-actividad')
			;
		
		$button.prop('disabled', true);
		$fieldset.prop('disabled', true)
		$list.find('.actividades-list').droppable('disable');
		
		$.post(action, data).done(function (data) {
			console.log(data);
			
			data = JSON.parse(data);
			if (data.status == 'ok') {
				
				if (is_new) {
					$a.attr('href', $a.attr('href') + data.id);
					$div.attr('id', $div.attr('id') + data.id);
					$form.attr('action', $form.attr('action') + data.id);
					$btn_delete.attr('href', $btn_delete.attr('href') + data.id);
					$panel.removeClass('panel-new');
				}
				
				$a.html(data.descripcion)
				
			}
			else alert(data.msg);
			
			$button.prop('disabled', false);
			$fieldset.prop('disabled', false)
			$list.find('.actividades-list').droppable('enable');
			
		});
		
		
	});*/
	
	// Misc ====================================================================
	
	// header text
	setNavbarTextWidth();
	
	// box instituciones participantes
	$('.box-instituciones').slimscroll({
		height: 500
	});
	
	/*$('.estandares-wrapper').slimscroll({
		height: 500
	});*/
	
	// Popover form
	$('.popover-form').popover({
		html: true,
		placement: 'left',
		content: function () {
			console.log($(this).next().html())
			return $(this).next().html();
		}
	});
	
	// Fichas ==================================================================
	
	// Common
	
	if ($('.ficha').length) {
		
		$('body')
			.css('position', 'relative')
			.attr('data-scroll', 'scroll')
			.scrollspy({
				target: '.sidebar',
				offset: 100,
			});
		
		var institucional = ($('.firmas').children().length == 1),
			universidad = ! $('[name=cod_mod]').length;
		
	}
	
	var add_row = function (id) {
		
		return function () {
			
			var $list = $(this).parent().find('tbody'),
				$item = $list.children().first().clone(),
				number = id || +$list.children().last().data('id') + 1;

			$item.attr('data-id', number);
			$item.find('input').each(function () {
				var $this = $(this),
					name = $this.attr('name'),
					pattern = /\[\d+\](?!.*\[\d+\])/, // not followed by... (último)
					text = '[' + number + ']';

				$this.val('').attr('name', name.replace(pattern, text));
			});

			$list.append($item);
			$('body').scrollspy('refresh');
		}
	},
	
	remove_row = function () {
		$(this).closest('tr').remove();
		$('body').scrollspy('refresh');
	},
	
	sort = function () {
		var $wrapper = $(this);
		
		$wrapper.children().sort(function (a, b) {
			
			return +$(a).data('id') - +$(b).data('id');
		})
		.appendTo( $wrapper );
	}
	
	$('.ficha').on('click', '.add_tr', add_row());
	
	$('.ficha').on('click', '.remove_tr', remove_row);
	
	// =========================================================================
	
	// Blur carrera nombre
	
	$('.ficha').on('blur', '.carrera_nombre', function () {
		var $this = $(this),
			val = $this.val(),
			$sede = $this.closest('.filial_item'),
			title = $sede.find('.nombre_sede').text(),
			name = title + ': ' + val,
			
			sede = +$sede.data('id'),
			carr = +$this.closest('tr').data('id'),
			number = sede * 100 + carr;
		
		// Table carreras
		var $table_carreras = $('.table_carreras tbody');
		
		$table_carreras.each(function () {
			var $tbody = $(this),
				$input = $tbody.find('[data-id="' + (number) + '"] td:first-child input');
			
			$input.val(name);
		});
		
		if ( ! institucional) return;
		
		// Sidebar
		var $li = $('.sidebar_menu').find('[data-id=' + number + ']');
		$li.find('span').html(name);
		
		// Ficha
		var $item = $('.carreras_list[data-id=' + sede + '] > .carreras_item[data-id=' + carr + ']');
		//var $item = $('.carreras_item[data-id=' + number + ']');
		$item.find('.carrera_nombre').val(val);
	});
	
	// Blur carrera area
	
	$('.ficha').on('blur', '.carrera_area', function () {
		var $this = $(this),
			val = $this.val(),
			$sede = $this.closest('.filial_item'),
			
			sede = +$sede.data('id'),
			carr = +$this.closest('tr').data('id');
		
		if ( ! institucional) return;
		
		// Ficha
		var $item = $('.carreras_list[data-id=' + sede + '] > .carreras_item[data-id=' + carr + ']');
		$item.find('.carrera_area').val(val);
	});
	
	var add_carrera = function () {
		var $tbody = $(this).parent().find('tbody'),
			$sede = $tbody.closest('.filial_item'),
			title = $sede.find('.nombre_sede').text(),
			
			sede = +$tbody.closest('.filial_item').data('id'),
			carr = +$tbody.children().last().data('id'), // We don't need to add 1 'cause the new one was already created
			number = sede * 100 + carr;
		
		// Table carreras
		var $table_carreras = $('.table_carreras tbody');
		$table_carreras.each(add_row(number));
		$table_carreras.each(sort);
		
		if ( ! institucional) {
			$('body').scrollspy('refresh');
			return;
		}
		
		// Sidebar
		var	$ul = $('.sidebar_menu'),
			$li = $ul.children('[data-id]').first().clone(), // get the 2nd one
			href = $li.find('a').attr('href').replace(/\d+/, number);
		
		$li.attr('data-id', number);
		$li.find('a').attr('href', href);
		$li.find('span').text(title + ': ');
		$ul.append($li);
		$ul.each(sort);
		
		// Ficha
		var $list = $('.carreras_list[data-id=' + sede + ']'),
			$item = $('.carreras_list[data-id=0]').children('.carreras_item').first().clone(),
					//$list.children('.carreras_item').first().clone(),
			id = $item.attr('id').replace(/\d+/, number);
			;
			
		$item.attr('data-id', carr);
		$item.attr('id', id);
		
		$item.find('.add_tr').parent().find('tbody tr:not(:first-child)')
			.remove();
		
		$item.find('input').each(function () {
			var $this = $(this),
				name = $this.attr('name'),
				pattern1 = /\[\d+\]/, // will replace only first one
				text1 = '[' + sede + ']',
				pattern2 = /(\[\d\]\[[^\]]+\])\[\d\]/, // Will match everything before second one as $1 and will replace second one
				text2 = '$1[' + carr + ']';
			
			$this.val('').attr('name', name.replace(pattern1, text1).replace(pattern2, text2));
		});
		
		$list.append($item);
		$('body').scrollspy('refresh');
	},
	
	remove_carrera = function () {
		
		var $this = $(this),
			$sede = $this.closest('.filial_item'),
			
			sede = +$sede.data('id'),
			carr = +$this.closest('tr').data('id'),
			number = sede * 100 + carr;
		
		$this.each(remove_row);
		
		// Table carreras
		var $table_carreras = $('.table_carreras tbody');
		$table_carreras.children('[data-id=' + number + ']').remove();
		
		if ( ! institucional) {
			$('body').scrollspy('refresh');
			return;
		}
		
		// Sidebar
		var $li = $('.sidebar_menu').find('[data-id=' + number + ']');
		$li.remove();
		
		// Ficha
		var $item = $('.carreras_list[data-id=' + sede + '] > .carreras_item[data-id=' + carr + ']');
		$item.remove();
		
		$('body').scrollspy('refresh');
	};
	
	// Add carrera
	$('.ficha').on('click', '.add_carrera', add_carrera);
	
	// Remove carrera
	$('.ficha').on('click', '.remove_carrera', function() {
		var $this = $(this);
		
		if ( institucional && ! confirm('Esto eliminará también la ficha asociada a la carrera. Continuar?'))
			return;
		
		$this.each(remove_carrera);
		$('body').scrollspy('refresh');
	});
	
	// Add filial
	$('.add_filial').click(function () {
		var $this = $(this),
			$list = $this.closest('.box').find('.filial_list'),
			$item = $list.children().first().clone(),
			number = +$list.children().last().data('id') + 1,
			title = 'Filial ' + number,
			institucional = ($('.firmas').children().length == 1);
			
		$item.attr('data-id', number);
		$item.find('.box-title').html(title);
		$item.find('tbody tr').slice(1).remove();
		$item.find('input').each(function() {
			var $this = $(this),
				name = $this.attr('name'),
				pattern = /\[\d+\]/, // will replace only first one
				text = '[' + number + ']';
			
			$this.val('').attr('name', name.replace(pattern, text));
		});
		
		$item.find('[type=hidden]').val(title);
		$list.append($item);
		
		// Sedes
		if (institucional) {
			var $sedes_list = $('.sedes_list'),
				$sede_item = $sedes_list.children().first().clone();

			$sede_item.attr('data-id', number);
			$sede_item.children('.carreras_item').remove();
			$sede_item.children('h3').text(title);

			$sedes_list.append($sede_item);
		}
		
		$item.find('.add_carrera').each(add_carrera);
		$('body').scrollspy('refresh');
	});
	
	// Remove filial
	
	$('.filial_list').on('click', '.remove_filial', function () {
		if (institucional && ! confirm('Esto eliminará también las fichas asociadas a las carreras. Continuar?'))
			return;
		
		var $item = $(this).closest('.filial_item'),
			$rows = $item.find('tbody tr'),
			number = +$item.data('id');
		
		$rows.each(remove_carrera);
		
		$item.remove();
		
		// Sedes
		if (institucional)
		{
			var $sede = $('.carreras_list[data-id=' + number + ']');
			$sede.remove();
		}
		
		$('body').scrollspy('refresh');
	});
	
	// =========================================================================
	
	$('.download_ficha').click(function () {
		var url = $(this).data('url');
		$('iframe').attr('src', url);
	});
	
	$('[data-confirm]').click(function () {
		var msg = $(this).data('confirm') || 'Está usted seguro(a)?';
		
		return confirm(msg);
	});
	
	$('.ficha').on('blur', '[pattern]', function () {
		var $this = $(this),
			val = $this.val(),
			regex = new RegExp($this.attr('pattern')),
			required = $this.prop('required'),
			text = 'Sólo se permiten números o los valores N/A o S/D';
		
		$this.tooltip({'title': text, 'trigger': 'manual'});
		
		if ( ! regex.test(val))
		{
			if (val !== '' || required) {
				$this.tooltip('show');
				setTimeout(function () { $this.tooltip('hide'); }, 2000);
			}
			//else
				//alert('ok');
				//$this.tooltip('hide');
		}
		//else $this.tooltip('hide');
			
			
	});
	
	$('.ficha').on('blur', '.calc_total > tr > td', function () {
		var $this = $(this),
			nth_index = $this.index() + 1,
			$tbody = $this.closest('tbody'),
			$row = $this.parent().find('>td>input'),
			$column = $tbody.find('td:nth-child(' + nth_index + ')>input'),
			$last_row = $tbody.children(':last-child').find('>td>input'),
			calc_row = $row.filter('[readonly]').length,
			calc_col = $column.filter('[readonly]').length;
		
		if (calc_row) {
			calc_total($row);
		}
		
		if (calc_col) {
			calc_total($column);
		}
		
		if (calc_row && calc_col) {
			calc_total($last_row);
		}
		
		function calc_total($set) {
			var $last = $set.slice(-1),
				$rest = $set.slice(0,-1);
			
			var	total = $rest.get().reduce(function (p, c) {
					var val = +$(c).val() || 0;
					return p + val;
				}, 0);
			
			$last.val(total);
		}
		
	});
	
	$('form.ficha').submit(function () {
		var success = true;
		
		$('button[type=submit]').first().focus();
		
		$('.cuerpo_academico').each(function () {
			
			success = false;
			
			var $this = $(this),
				ded = +$this.find('[data-tipo=ded]').val(),
				con = +$this.find('[data-tipo=con]').val(),
				cat = +$this.find('[data-tipo=cat]').val(),
				nom = +$this.find('[data-tipo=nom]').val();
			
			if (ded != con) {
				$('html, body').scrollTop($this.offset().top);
				$this.find('[data-tipo=ded],[data-tipo=con]')
					.addClass('warning').delay(1000).queue(function () {
						$(this).removeClass('warning').dequeue();
					});
				alert('Los totales de Dedicación y Condición deben coincidir');
				return false;
			}
			
			if (universidad && cat != nom) {
				$('html, body').scrollTop($this.offset().top);
				$this.find('[data-tipo=cat],[data-tipo=nom]')
					.addClass('warning').delay(1000).queue(function () {
						$(this).removeClass('warning').dequeue();
					});
				alert('Los totales de Nombrados y Categoría Docente deben coincidir');
				return false;
			}
			
			success = true;
			
		});
		
		//alert(success);
		
		return success;
	});
	
}(jQuery));

$(window).resize(setNavbarTextWidth);

function setNavbarTextWidth() {
	
	var $text = $('.navbar-text');
		header = $('.header').outerWidth(),
		logo = $('.logo').outerWidth(),
		navbar = $('.navbar-right').outerWidth();
	
	$text.css('width', header - logo - navbar - 15);
}
