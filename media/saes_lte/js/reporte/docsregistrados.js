$(function() {

	$('#vig_desde').on('blur', function(){

		var $vig_desde = $(this),
			$vig_hasta = $('#vig_hasta');

			$vig_hasta.attr('min', $(this).val());

			if ($vig_desde.val() != '') {

				$vig_hasta.attr('required', 'required');
			}
			else{

				$vig_hasta.removeAttr('required');
			}

			if($vig_hasta.val() != '' && $vig_desde.val() > $vig_hasta.val()){

				alert("Fecha de vigencia de inicio no puede ser que la segunda fecha.");
				$vig_desde.val('');
			}
	});

	$('#vig_hasta').on('blur', function(){

		var $vig_hasta = $(this),
			$vig_desde = $('#vig_desde');

			if ($vig_hasta.val() != '') {

				$vig_desde.attr('required', 'required');
			}
			else{

				$vig_desde.removeAttr('required');
			}

			if($vig_desde.val() != '' && $vig_desde.val() > $vig_hasta.val()){

				alert("La segunda fecha no puede ser menor que la fecha de inicio.");
				$vig_hasta.val('');
			}
	});


	$('#olg_desde').on('blur', function(){

		var $olg_desde = $(this),
			$olg_hasta = $('#olg_hasta');

			$olg_hasta.attr('min', $(this).val());

			if ($olg_desde.val() != '') {

				$olg_hasta.attr('required', 'required');
			}
			else{

				$olg_hasta.removeAttr('required');
			}

			if($olg_hasta.val() != '' && $olg_desde.val() > $olg_hasta.val()){

				alert("El número de inicio de la holgura no puede ser que al segundo.");
				$olg_hasta.val('');
			}
	});

	$('#olg_hasta').on('blur', function(){

		var $olg_hasta = $(this),
			$olg_desde = $('#olg_desde');

			if ($olg_hasta.val() != '') {

				$olg_desde.attr('required', 'required');
			}
			else{

				$olg_desde.removeAttr('required');
			}

			if($olg_hasta.val() != '' && $olg_hasta.val() < $olg_desde.val()){

				alert("El segundo número de holgura no puede ser menor que el primero.");
				$olg_hasta.val('');
			}
	});
});