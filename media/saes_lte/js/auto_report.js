/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

Saes.scroll = {
	'offset'	: 100,
	'duration'	: '500',
	'animate'	: function (target) {
		$('html,body').animate({scrollTop:$(target).offset().top - this.offset}, this.duration); // Animate the scroll to this link's href value
	},
	'setData'	: function (data) {
		$('#result').html(data.result);
	}
};

/*
 * The functions inside this block will get executed AFTER page load, 
 * overwritting previous functionality as event handlers, if they exist
 */
$(function() {
	hash = document.location.hash;
	
	if (hash) {
		Saes.scroll.animate(hash);
	}
});

/*
 * Functions inside this block will get executed inmediatly 
 * (i.e. this callbacks will get registered BEFORE page load)
 */
(function($) {
	
}(jQuery));
