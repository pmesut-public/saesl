/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

/*
 * The functions inside this block will get executed AFTER page load, 
 * overwritting previous functionality as event handlers, if they exist
 */
$(function() {
	
});

/*
 * Functions inside this block will get executed inmediatly 
 * (i.e. this callbacks will get registered BEFORE page load)
 */
(function($) {
	
	// FICHA002 ================================================================
	
	// Common
	
	if ($('.ficha').length) {
		
		$('body')
			.css('position', 'relative')
			.attr('data-scroll', 'scroll')
			.scrollspy({
				target: '.sidebar',
				offset: 100,
			});
		
		//var institucional = ($('.firmas').children().length == 1),
		//	universidad = ! $('[name=cod_mod]').length;
		
	}
	
	var add_row = function /*(id) {
		
		return function*/ () {
			
			var $list = $(this).parent().find('tbody'),
				$item = $list.children().first().clone(),
				number = +$list.children().last().data('id') + 1;

			$item.attr('data-id', number);
			$item.children().first().html(number + 1);
			$item.find('input').each(function () {
				var $this = $(this),
					name = $this.attr('name'),
					pattern = /\[\d+\](?!.*\[\d+\])/, // not followed by... (último)
					text = '[' + number + ']';

				$this.val('').attr('name', name.replace(pattern, text));
			});

			$list.append($item);
			$('body').scrollspy('refresh');
		//}
	},
	
	remove_row = function () {
		$(this).closest('tr').remove();
		$('body').scrollspy('refresh');
	};
	
	/*sort = function () {
		var $wrapper = $(this);
		
		$wrapper.children().sort(function (a, b) {
			
			return +$(a).data('id') - +$(b).data('id');
		})
		.appendTo( $wrapper );
	}*/
	
	$('.ficha').on('click', '.add_tr', add_row/*()*/);
	
	$('.ficha').on('click', '.remove_tr', remove_row);
	
	// =========================================================================
	
	$('.download_ficha').click(function () {
		var url = $(this).data('url');
		$('iframe').attr('src', url);
	});
	
	$('.ficha').on('blur', '[pattern]', function () {
		var $this = $(this),
			val = $this.val(),
			regex = new RegExp($this.attr('pattern')),
			required = $this.prop('required'),
			text = 'Sólo se permiten números o los valores N/A o S/D';
		
		$this.tooltip({'title': text, 'trigger': 'manual'});
		
		if ( ! regex.test(val))
		{
			if (val !== '' || required) {
				$this.tooltip('show');
				setTimeout(function () { $this.tooltip('hide'); }, 2000);
			}
		}
	});
	
	$('.ficha').on('blur', '.calc_total > tr > td', function () {
		var $this = $(this),
			nth_index = $this.index() + 1,
			$tbody = $this.closest('tbody'),
			$row = $this.parent().find('>td>input'),
			$column = $tbody.find('td:nth-child(' + nth_index + ')>input').slice(0, 6),
			$last_row = $tbody.children().slice(5, 6).find('>td>input'),
			calc_row = $row.filter('[readonly]').length,
			calc_col = $column.filter('[readonly]').length,
			$1perc_row = $tbody.children().slice(6, 7).find('>td>input'),
			$total_row = $tbody.children().slice(7).find('>td>input');
		
		if (calc_row) {
			calc_total($row);
		}
		
		if (calc_col) {
			calc_total($column);
		}
		
		if (calc_row && calc_col) {
			calc_total($last_row);
		}
		
		$1perc_row.each(function (a,b) {
			var val = (+$last_row.eq(a).val() * 0.01).toFixed(2);
			$(this).val(val);
		});
		$total_row.each(function (a,b) {
			var val = (+$last_row.eq(a).val() * 1.01).toFixed(2);
			$(this).val(val);
		});
		
		
		
		function calc_total($set) {
			var $last = $set.slice(-1),
				$rest = $set.slice(0,-1);
			
			var	total = $rest.get().reduce(function (p, c) {
					var val = +$(c).val() || 0;
					return p + val;
				}, 0);
			
			$last.val(total);
		}
		
	});
	
	/*$('form.ficha').submit(function () {
		var success = true;
		
		$('button[type=submit]').first().focus();
		
		$('.cuerpo_academico').each(function () {
			
			success = false;
			
			var $this = $(this),
				ded = +$this.find('[data-tipo=ded]').val(),
				con = +$this.find('[data-tipo=con]').val(),
				cat = +$this.find('[data-tipo=cat]').val(),
				nom = +$this.find('[data-tipo=nom]').val();
			
			if (ded != con) {
				$('html, body').scrollTop($this.offset().top);
				$this.find('[data-tipo=ded],[data-tipo=con]')
					.addClass('warning').delay(1000).queue(function () {
						$(this).removeClass('warning').dequeue();
					});
				alert('Los totales de Dedicación y Condición deben coincidir');
				return false;
			}
			
			if (universidad && cat != nom) {
				$('html, body').scrollTop($this.offset().top);
				$this.find('[data-tipo=cat],[data-tipo=nom]')
					.addClass('warning').delay(1000).queue(function () {
						$(this).removeClass('warning').dequeue();
					});
				alert('Los totales de Nombrados y Categoría Docente deben coincidir');
				return false;
			}
			
			success = true;
			
		});
		
		//alert(success);
		
		return success;
	});*/
	
}(jQuery));
