/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

Saes.scroll = {
	'offset'	: 100 + 96,
	'duration'	: '500',
	'animate'	: function (target) {
		$('html,body').animate({scrollTop:$(target).offset().top - this.offset}, this.duration); // Animate the scroll to this link's href value
	},
	'setData'	: function (data) {
		$('#result').html(data.result);
	}
};

/*
 * The functions inside this block will get executed AFTER page load, 
 * overwritting previous functionality as event handlers, if they exist
 */
$(function() {
	hash = document.location.hash;
	
	if (hash) {
		Saes.scroll.animate(hash);
	}
	
	$('input[type=checkbox]').iCheck('destroy');
	$('input[type=radio]').iCheck('destroy');
	
	// - Accomplishment
	$('.concept-accomp').change(toggleQuality);
	$('.concept-accomp').each(toggleQuality);
	
	function toggleQuality () {
		var $this = $(this),
			$quality = $this.closest('.row').find('.concept-quality'),
			$file = $this.closest('.row').find('.btn_file_upload'),
			$file_view = $this.closest('.row').find('.btn_file_view'),
			$file_input = $file_view.siblings('input');
		
		if ($this.prop('checked')) {
			$quality.prop('required', true).prop('disabled', false);
			$file.removeClass('disabled').tooltip();
			if ( ! $file_input.val())
				$file.find('input[type=file]').prop('required', true);
		} else {
			// Si no es necesario subir la FV o no hay ningún archivo subido, o sí lo hay y se ha confirmado su eliminación:
			if ( ! $file.length || ! $file_input.val() || confirm($this.data('confirm-title'))) {
				$quality.val('').addClass('placeholder').prop('required', false).prop('disabled', true);
				$file.addClass('disabled').tooltip('destroy')
					.find('input[type=file]').prop('required', false);
				$file_view.tooltip('destroy').addClass('disabled');
				$file_input.val('');
				return;
			}
			$this.prop('checked', true);
		}
	}
	
	// - Accomplishment level
	$('.accomplishment-level').change(setConceptRequired);
	$('.accomplishment-level[checked]').each(setConceptRequired);
	
	function setConceptRequired() {
		var $this = $(this),
			$addons = $this.closest('.row').find('.concept-addon'),
			$checks = $this.closest('.row').find('.concept-accomp'),
			//$files = $this.closest('.row').find('.concept-file'),
			condicion = $this.data('condicion').toString(),
			i, n = condicion.length;
		
		$addons.removeClass('required')
			.tooltip('destroy');
		
		$checks.prop('required', false);
		
		for (i = 0; i < n; i++) {
			if (condicion[i] == 1) {
				$addons.eq(i)
					.addClass('required')
					.tooltip({'title': 'Obligatorio'});
				
				$checks.eq(i).prop('required', true);
			}
		}
	}
	
	$('.concept-file').change(function () {
		var $this = $(this),
			$inputs = $this.parent(),
		
			$concepto = $this.closest('.input-group'),
			$upload = $concepto.find('.file_upload'),
			$process = $concepto.find('.file_process'),
			$file_view = $concepto.find('.btn_file_view'),
			$filename = $file_view.siblings('input'),
			
			$form = $this.closest('.form-standard'),
			$btn_save = $form.find('.btn-standard-save'),
			
			$outside = $('.form_outside'),
			form, xhr;
		
		if ( ! $this.val()) return;
		//console.log($filename);return;
		
		// processing...
		$upload.addClass('hidden');
		$process.removeClass('hidden');
		$btn_save.prop('disabled', true);
		$file_view.addClass('disabled').tooltip('destroy');
		
		//console.log($file_view);
		//$file_view.removeAttr('disabled').tooltip();return;
		
		// submit
		$inputs.appendTo($outside);
		form = $outside.ajaxSubmit();
		xhr = form.data('jqxhr');
		
		// done
		xhr.done(function (data) {
			console.log('success');
			
			$filename.val(data);
			$file_view.attr('href', '/file/' + data);
			$this.prop('required', false);
			finish();
			
		}).error(function (x,y,z) {
			console.log('error', x, y, z);
			
			alert('No se pudo subir el archivo. Por favor inténtelo nuevamente');
			finish();
		});
		
		function finish() {
			$this.val('');
			$upload.append($inputs).removeClass('hidden');
			$process.addClass('hidden');
			$btn_save.prop('disabled', false);
			
			if ($filename.val()) {
				$file_view.removeClass('disabled').tooltip();
			}
		}
		
	})
});

/*
 * Functions inside this block will get executed inmediatly 
 * (i.e. this callbacks will get registered BEFORE page load)
 */
(function($) {
	
	// Calculator ==============================================================
	// autoevaluacion/calculator
	
	$('.li_estandar').click(function () {
		var target = $(this).attr('href');
		console.log(target);

		Saes.scroll.animate(target);
	});
	
	// Cumplidas
	$('tr.processed').each(function () {
		var id = $(this).data('id'),
			$li = $('.sidebar-menu').find('li[data-id=' + id + ']');
		
		$li.addClass('processed');
		
		
	})
	
	// - Alternative document
	/*$('.alt-doc-cb').change(toggleAlternative);
	$('.alt-doc-cb').each(toggleAlternative);
	
	function toggleAlternative () {
		var $this = $(this),
			$altDoc = $this.closest('.row').find('.alt-doc');
		
		if ($this.prop('checked')) {
			$altDoc.removeClass('hidden').find('input').prop('required', true);
		} else {
			$altDoc.addClass('hidden').find('input')./*val('').*-/prop('required', false);
		}
	}*/
	
	// - Form Standard
	$('.form-standard').submit(function (e) {
		e.preventDefault();
		
		var $this = $(this),
			$action = $this.attr('action'),
			$data = $this.serialize(),
			$button = $this.find('.btn-standard-save'),
			$tr = $this.closest('tr'),
			id = $tr.data('id'),
			$li = $('.sidebar-menu').find('li[data-id=' + id + ']');
		
		//console.log($data);
		$button.prop('disabled', true);
		
		$.post($action, $data).done(function (data) {
			console.log(data);
			
			data = JSON.parse(data);
			if (data.status == 'ok') {
				var $result = $('.result'),
					$span = $result.find('span'),
					$bar_cumplidos = $result.find('.bar_cumplidos'),
					$bar_procesados = $result.find('.bar_procesados'),
					$progress = $result.find('.bar_progress');
				
				$span.html(data.resultado);
				$bar_cumplidos.css('width', data.per_cumplidos + '%');
				$bar_procesados.css('width', data.per_diferencia + '%');
				
				$bar_cumplidos.attr('data-original-title', $bar_cumplidos.attr('data-original-title').replace(/\d+/, data.cumplidos))
				$bar_procesados.attr('data-original-title', $bar_procesados.attr('data-original-title').replace(/\d+/, data.procesados))
				$progress.attr('title', $progress.attr('title').replace(/\d+/, data.no_procesados))
				
				$button.prop('disabled', false);
				$tr.addClass('processed');
				$li.addClass('processed');
			} else {
				alert('Ha ocurrido un error al intentar guardar el estándar.\n' + 
					'Por favor vuelva a cargar la página (Para mayor seguridad presione Ctrl + F5)\n' + 
					'Si el problema persiste, contacte a un administrador.');
			}
		});
	});
	
	$(".table-wrapper").scroll(function(){
        $(".table-standard")
            .scrollLeft($(".table-wrapper").scrollLeft());
    });
    $(".table-standard").scroll(function(){
        $(".table-wrapper")
            .scrollLeft($(".table-standard").scrollLeft());
    });
	
	
}(jQuery));



(function($) {
	
	// autoevaluacion/actividades ==================================================
	
	/*$('.estandares-wrapper').slimscroll({
		height: 500
	});*/
	
	// http://api.jqueryui.com/draggable/
	/*$('.estandares-list li').draggable({
		helper: 'clone',
		scope: 'estandares'
	});
	
	var droppableObject = {
		scope: 'estandares',
		hoverClass: "ui-state-active",
		drop: function (e, ui) {
			console.log(e);
			console.log(ui);
			
			var $this = $(this),
				$ul = $this.find('ul'),
				$li = ui.helper.clone().removeAttr('style').removeClass('ui-draggable-dragging'),
				id = $li.data('id'),
				$original = $('.estandares-list [data-id=' + id + ']'),
				count = $original.find('.label').html();
			
			//console.log($original);
		
			$li.find('.handle, .label').remove();
			
			//ui.helper.addClass('foo');
			
			if ( ! $ul.find('[data-id=' + id + ']').length)
			{
				$ul.append($li);
				$original.find('.label').html(++count).removeClass('label-danger').addClass('label-info');
			}
			
		}
	};
	
	// http://api.jqueryui.com/droppable/
	$('.actividades-list').droppable(droppableObject);
	
	$('.btn-add-actividad').click(function (e) {
		e.preventDefault();
		
		var	$panel = $('.panel-group'),
			has_new = $panel.find('.panel-new').length,
			$model = $('.activity-model .panel').clone();
		
		if ( ! has_new) {
			$panel.find('.panel-collapse.in').removeClass('in');
			$model.appendTo($panel);
			$model.find('.actividades-list').droppable(droppableObject);
		}
	});
	
	$('.panel-group').on('click', '.btn-remove-actividad', function (e) {
		e.preventDefault();
		
		var $this = $(this),
			action = $this.attr('href'),
			$parent = $this.closest('.panel'),
			$estandares = $parent.find('.actividades-list li'),
			is_new = $parent.hasClass('panel-new');
		
		if (is_new) {
			$estandares.each(removeEstandar);
			$parent.remove();
		}
		else {
			$.post(action).done(function (data) {
				
				if (data == 'ok') {
					$estandares.each(removeEstandar);
					$parent.remove();
				} else {
					alert(data);
				}
				
			});
		}
		
	});
	
	$('.panel-group').on('click', '.btn-remove-estandar', function (e) {
		e.preventDefault();
		
		var $this = $(this),
			$parent = $this.closest('li');
			
		$parent.each(removeEstandar);
		$parent.remove();
	});
	
	function removeEstandar (i, estandar) {
		console.log(estandar);
		
		var id = $(estandar).data('id'),
			$original = $('.estandares-list li[data-id=' + id + ']'),
			$counter = $original.find('.label'),
			count = $counter.html();
		
		$counter.html(--count);
		if (count == 0) $counter.removeClass('label-info').addClass('label-danger');
	}
	
	$('.panel-group').on('submit', 'form', function (e) {
		e.preventDefault();
		
		var $form = $(this),
			$fieldset = $form.children(),
			$list = $form.find('.actividades-list'),
			$button = $form.find('.btn-save-actividad'),
			action = $form.attr('action'),
			data = $form.serialize(),
			
			$panel = $form.closest('.panel'),
			is_new = $panel.hasClass('panel-new'),
			$a = $panel.find('a[data-toggle=collapse]'),
			$div = $panel.find('.panel-collapse'),
			$btn_delete = $form.find('.btn-remove-actividad')
			;
		
		$button.prop('disabled', true);
		$fieldset.prop('disabled', true)
		$list.find('.actividades-list').droppable('disable');
		
		$.post(action, data).done(function (data) {
			console.log(data);
			
			data = JSON.parse(data);
			if (data.status == 'ok') {
				
				if (is_new) {
					$a.attr('href', $a.attr('href') + data.id);
					$div.attr('id', $div.attr('id') + data.id);
					$form.attr('action', $form.attr('action') + data.id);
					$btn_delete.attr('href', $btn_delete.attr('href') + data.id);
					$panel.removeClass('panel-new');
				}
				
				$a.html(data.descripcion)
				
			}
			else alert(data.msg);
			
			$button.prop('disabled', false);
			$fieldset.prop('disabled', false)
			$list.find('.actividades-list').droppable('enable');
			
		});
		
	});*/
	
}(jQuery));
