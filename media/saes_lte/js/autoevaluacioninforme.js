/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

/*
 * Functions inside this block will get executed inmediatly 
 * (i.e. this callbacks will get registered BEFORE page load)
 */
(function($) {
	
	$('.download_ficha').click(function () {
		var url = $(this).data('url');
		$('iframe').attr('src', url);
	});
	
	$('.informe_factores textarea').blur(save_informe_factores);
	
	function save_informe_factores() {
		var data = $(this).siblings('input').addBack().serialize();
		
		$.post('/autoevaluacionInforme/saveFactorData', data).done(after_save);
	}
	
	function after_save(data) {
		console.log(data);
	}
	
	$('.informe_descripcion, .informe_firma').find('textarea, input').blur(save_informe_data);
	
	function save_informe_data() {
		var data = $('.informe_descripcion, .informe_firma').find('textarea, input').serialize();
		
		$.post('/autoevaluacionInforme/saveInformeData', data).done(after_save);
	}
	
}(jQuery));
