/**
 *
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */

/*
 * The functions inside this block will get executed AFTER page load, 
 * overwritting previous functionality as event handlers, if they exist
 */
$(function() {
	
	// Select autoevaluaciones =================================================
	
	var context = '.table-autos tbody';
	$('.iCheck-helper', context).click(function (evt) {
		
		var $this = $(this);
		var element = $this.prev('input[type="checkbox"]');
		var parent = $this.closest('tr');
		
		if (element.length) {
			
			var pIndex = parent.index();
			
			if (evt.shiftKey) {
				
				if (Saes.last >= 0) {
					
					var current_checked = ! $('input[type="checkbox"]:nth(' + pIndex + ')', context).prop('checked'),
						// here, current checkbox has already been clicked, that's why we use the opposite
						
						isChecked = current_checked ? 'uncheck' : 'check';
					
					$('input[type="checkbox"]', context).each(function(i, e) {
						
						if ((i - pIndex) * (i - Saes.last) <= 0) {
							// this checkbox is in the range
							$(e).iCheck(isChecked);
						}
					});
				}
				
			} else {
				//Saes.last = pIndex;
			}
			
			Saes.last = pIndex;
			
		}
	});
	
	$('.committee input').iCheck('destroy');
});

/*
 * Functions inside this block will get executed inmediatly 
 * (i.e. this callbacks will get registered BEFORE page load)
 */
(function($) {
	
	// All toggled =============================================================
	// /admin/autoevaluacion/list
	
	$('.input-all').on('ifToggled', function () {
		
		if ($(this).prop('checked'))
			$('.input-autoevaluacion').iCheck('check');
		else
			$('.input-autoevaluacion').iCheck('uncheck');
		
		Saes.last = $(this).index();
		console.log(Saes.last);
	});
	
	// Btn massive sms status ==================================================
	// /admin/autoevaluacion/list
	
	$('body').on('ifToggled', setBtnMassiveStatus);
	setBtnMassiveStatus();
	
	function setBtnMassiveStatus() {
		
		if ($('.input-autoevaluacion:checked').length) {
			$('.btn-mensaje-general').removeClass('disabled');
		} else {
			$('.btn-mensaje-general').addClass('disabled');
		}
		
	}
	
	// Btn massive sms click ===================================================
	// /admin/autoevaluacion/list
	
	$('.btn-mensaje-general').click(function (e) {
		e.preventDefault();
		
		var ids = $.map($('.input-autoevaluacion:checked'), function (e) {
			return $(e).data('id');
		});
		
		if (ids.length) {
			
			var path = $(this).attr('href') + '?ids=' + ids.join(',');
			sendMessages(path);
			
		}
		
	});
	
	function getDate(date, def) {
		//console.log(date, date.split('-').reverse().join('/'), def);
		return date ? date.split('-').reverse().join('/') : def;
	}
	
	function getQuery() {
		var data = {};
		
		location.search.substr(1).split('&').forEach(function (q) {
			var s = q.split('='),
				k = s[0],
				v = s[1] && decodeURIComponent(s[1]);
			
			if (k) data[k] = [v];
		});
		
		return new function () {
			this.data = data;
			this.toString = function () { return setQuery(this.data); };
			this.set = function (key, val) { this.data[key] = val; return this; };
			this.remove = function (key) { delete this.data[key]; return this; }
		};
	}
	
	function setQuery(data) {
		var query = [];
		
		for (var i in data) {
			query.push(i.toString() + '=' + data[i].toString());
		}
		
		return '?' + query.join('&');
	}
	
	// Rango fechas
	$('#fecha_cierre').each(function () {
		var $this = $(this),
			startDate = getDate($this.data('start')),
			endDate = getDate($this.data('end')),
			options = {
				format: getDate('YYYY-MM-DD'),
				startDate: startDate, 
				endDate: endDate
			},
			val = startDate && endDate ? startDate + ' - ' + endDate : undefined;
			
		$this.val(val);
		
		function cb(a, b) {
			console.log(a,b);
			var start = a.format('YYYY-MM-DD'),
				end = b.format('YYYY-MM-DD'),
				query = getQuery().set('start_date', start).set('end_date', end);
			
			//console.log(a, b, query);
			location.href = location.pathname + query;
		}
		
		$this.daterangepicker(options, cb);
	});
	
	$('.remove_filtro_fecha').click(function () {
		location.href = location.pathname + getQuery().remove('start_date').remove('end_date');
	});
	
	
	/*}, function (a, b) { 
		//console.log(a,b); 
		var start = a.format('YYYY-MM-DD'),
			end = b.format('YYYY-MM-DD');
		console.log(start, end);
	});*/
	
	/*$('#fecha_cierre').on('change', function(ev, picker) {
		console.log(ev, picker);
		//console.log(picker.startDate.format('YYYY-MM-DD'));
		//console.log(picker.endDate.format('YYYY-MM-DD'));
	});*/
	
	// Send sms
	$('.btn-mensaje-comite').click(function (e) {
		e.preventDefault();
		
		var path = $(this).attr('href');
		
		sendMessages(path);
	});
	
	function sendMessages(path) {
		
		$('#myModal').modal('show');
		
		$.get(path)
			.done(function (data) {
				$('#myModal').html(data);
			});
	}
	
	// Reports =================================================================
	// /admin/dashboard
	
	/*$('.flot-chart-content').each(function () {
		var $this = $(this),
			properties = $this.data('properties'),
			data = $this.data('json');
		
		console.log(data);
		console.log(properties);
		
		$.plot($this, data, properties);
	});*/
	
	/*$('.highcharts').each(function () {
		var $this = $(this),
			//json = $this.data('json');
			json = JSON.parse($this.attr('data-json'), reviver);
			console.log(json);
		
		$this.highcharts(json);
		
	});
	
	function reviver (key, value) {
		
		//console.log(key, value);
		if (key && typeof(value) === 'string' && (x = value.match(/function(?:[\s\S]*?){([\s\S]*)}/))) {
			//console.log(x[1]);
			return Function(x[1]);
		}
		return value;
	}
	
	$('.highcharts_actions a[data-action]').click(function (e) {
		e.preventDefault();
		
		var $this = $(this),
			action = $this.data('action'),
			highcharts = $this.closest('.box-body').find('.highcharts').highcharts();
			//console.log(this);
		
		switch(action) {
			case 'print': highcharts.print(); 
				break;
			case 'pdf': highcharts.exportChart({type:'application/pdf'});
				break;
			case 'png': highcharts.exportChart();
				break;
		}
	});*/
	
	
	// box instituciones participantes
	$('.box-instituciones').slimscroll({
		height: 500
	});
	
	// Acreditado view =================================================================
	// /admin/acreditado/view
	
	// Popover form
	/*$('.popover-form').popover({
		html: true,
		placement: 'left',
		content: function () {
			console.log($(this).next().html())
			return $(this).next().html();
		}
	});*/
	
	
}(jQuery));

	//Imagen mapa en dashboard, que muestra iframe.
	$('div#div_imagen_mapa').on('click',function(){
		$('div#div_imagen_mapa').css('display','none');
		$('div#div_iframe_mapa').css('display','block');
		
		//setTimeout(function(){
			var mapa_iframe = document.getElementById('mapa_iframe').contentWindow;
			
			var data_serie = document.getElementById('input_mapa_iframe').value
			//mapa_iframe.postMessage(JSON.stringify(gg), "http.//procalidad.gob.pe")
			mapa_iframe.postMessage(data_serie, "*")
			console.log(data_serie);
		//}, 5000 );
	});