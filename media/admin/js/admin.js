jQuery(function ($) {
	
	// Search
	/*$('.btn_search').click(function () {
		var form = $(this).data('target');
		
		$(form).submit();
	});*/
	
	/*$('.btn_print').click(function () {
		var query = $(this).data('query');
		
		window.open(query, 'Print', 'menubar=0,location=0,height=700,width=760');
	});
	
	$('.btn_csv, .btn_excel').click(function () {
		var iframe = $(this).data('target'),
			query = $(this).data('query');
		
		$(iframe).attr('src', query);
	});*/
	
	// btn print ===============================================================
	$('.btn-print').click(function (e) {
		e.preventDefault();
		//window.print();
		window.open(this.href, 'Print', 'menubar=0,location=0,height=700,width=800');
	});
	
	// btn excel ===============================================================
	$('.btn-excel').click(function (e) {
		e.preventDefault();
		$('.iframe').attr('src', this.href);
		
	});
	
	// Columns ordering
	$('th.order').click(function () {
		var url = $(this).data('url');
		
		if (url) window.location = url;
	});
	
	// Types
	$('.date').datetimepicker({
		pickTime: false
	});
		/*'singleDatePicker': true,
		//'showDropdowns': true,
		//'format': 'MM/DD/YYYY'
		'format': 'YYYY-MM-DD'
	});*/
    
    // Objeto de autoevaluación
    var objeto = $('#even_objeto').val();
    objetoAuto(objeto);
        
    $('#even_objeto').on('change', function() {
        var obj = $('#even_objeto').val();
        objetoAuto(obj);
    });

    $(".nivel-ajax-institucion").change(function () {
        var $this = $(this),
            val = $this.val(),
            path = $this.data('path'),
            inst_id = $this.data('id'),
            target = $this.data('target')
        url = path + inst_id + '-' + val;
        //$target = $($this.data('target'));

        $.get(url).done(function (response) {
            $(target).html(response)
        });
    })

    $(".all-hidden").parent().parent().addClass('hidden')

    /**
     * Obtiene los objetos de acreditación.
     */
    $(".tipo-ajax-institucion").change(function () {

        var $this = $(this),
            val   = $this.val(),
            path  = "/ajax/get_tiin_objetos_institucion/",
            url   = path + val;

        $.get(url).done(function (response) {

            $("#inst_obac_id").html(response)
        });

        var path     = "/ajax/get_subtipos_institucion/",
            url_subt = path + val;

        $.get(url_subt).done(function (response) {
            console.log('okk');
            $("#subt_id").html(response)
        });
    })
    
//    $("select.ajax-institucion-tiin_id").change(function(){
//         var $this = $(this),
//            val = $this.val(),
//            path = $this.data('path'),
//            inst_id = $this.data('obac_tipo_acreditacion'),
//            target = $this.data('target');
//            url = path + inst_id + '-' + val;
//            //$target = $($this.data('target'));
//            $.get(url).done(function (response) {
//                $(target).html(response)
//            });
//                    
//        var path = "/ajax/get_subtipos_institucion/",
//            url_subt = path + val;
//
//        $.ajax({
//            url: path + val,
//            type: 'GET',
//            success: function(response){
//                $("#inst_subtipo").html(response);
//                $('#inst_subtipo').val('');
//            }
//        });
//    });
    
    //Edit Institucion
    $tipoInst = $("select.ajax-institucion-tiin_id").val();

    if($tipoInst){

        getObjetosAcreditacion($tipoInst);  

        getSubtiposInstitucion($tipoInst);
    }

    $("select.ajax-institucion-tiin_id").change(function () {  

        $("select.ajax-institucion-tiin_id").removeAttr('data-obac');
        $("select.ajax-institucion-tiin_id").removeAttr('data-subtipo'); 

        getObjetosAcreditacion($(this).val());

        getSubtiposInstitucion($(this).val());

    });
    
    /**
     * Obtiene los objetos de acreditación por el tipo de Institución.
     */
    function getObjetosAcreditacion($value) {
        var val  = $value,
            path = "/ajax/get_tiin_objetos_institucion/";

        $.ajax({
            url: path + val,
            type: 'GET',
            success: function(response){
                $("#inst_obac_id").html(response);
                $("#inst_obac_id").val($("select.ajax-institucion-tiin_id").attr('data-obac'));              
              }
        });
    }

    /**
     * Obtiene los Subtipos de acreditación por el tipo de Institución.
     */
    function getSubtiposInstitucion($value) {
        var val      = $value,
            path     = "/ajax/get_subtipos_institucion/",
            url_subt = path + val;

        $.ajax({
            url: path + val,
            type: 'GET',
            success: function(response){
                $("#subt_id").html(response);
                $('#subt_id').val($("select.ajax-institucion-tiin_id").attr('data-subtipo'));  
            }
        });
    }
    
    $("select.ajax-carrera-inst_id").change(function(){
         var $this = $(this),
            val = $this.val(),
            path = $this.data('path'),
            inst_id = $this.data('obac_tipo_acreditacion'),
            target = $this.data('target');
            url = path + inst_id + '-' + val;
            //$target = $($this.data('target'));
            $.get(url).done(function (response) {
                $(target).html(response)
            });
    });
    
    if (jQuery.fn.select2) {
        $('select.select2').select2({
            width: '100%',
            placeholder: 'Seleccionar'
        });
    }
});

function objetoAuto(valor)
{
    if (valor == 1)
    {
        $('#even_ponderacion').attr('disabled', 'disabled');
    }
    else
    {
        $('#even_ponderacion').removeAttr('disabled');
    }
}