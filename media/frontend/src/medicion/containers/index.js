import React, { Component } from 'react';
import { connect } from 'react-redux';

import { messageLeavePage } from '../messages';
import connectOnLeavePage from 'common/hoc/connectOnLeavePage';
import TablaMedicion from './TablaMedicion';
import SystemStatus from './SystemStatus';
import { getSummaryStatus } from '../reducer';

const App = (props) => (
  <div className="medicion">
    <SystemStatus />
    <h1>Medición de Actividades</h1>
    <hr />
    <TablaMedicion />
    <SystemStatus />
  </div>
);

const mapStateToProps = (state) => ({
  isFetching: getSummaryStatus(state) !== 'ALL_STORED',
});

export default connect(mapStateToProps)(connectOnLeavePage(App, messageLeavePage));
