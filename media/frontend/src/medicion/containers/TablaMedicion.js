import React, { Component } from 'react';
import groupBy from 'lodash/groupBy';
import flatMap from 'lodash/flatMap';

import _keyBy from 'lodash/keyBy';
import { connect } from 'react-redux';

import EstandaresList from '../components/EstandaresList';
import ActividadFactor from '../actividades/containers/ActividadFactor';
import ActividadAvance from '../actividades/containers/ActividadAvance';
import DetalleAvance from '../detalles/containers/DetalleAvance';
import DetalleFactor from '../detalles/containers/DetalleFactor';
import { getActividades } from '../actividades/reducer';
import { getDetalle } from '../detalles/reducer';
import { getContext } from 'common/context';
import { Table, TableHeader } from 'common/components/table';


const headers = [
  {display: 'DIMENSION', customStyle: {width: '10%'}},
  {display: 'FACTOR', customStyle: {width: '10%'}},
  {display: 'Tipo', customStyle: { width: '5%'}},
  {display: 'Actividades Registradas', customStyle: {width: '40%'}},
  {display: 'Estándares relacionados', customStyle: {width: '15%'}},
  {display: 'Nivel de importancia para el cumplimiento del factor %', customStyle: {width: '10%'}},
  {display: 'Nivel de avance en el logro de la actividad %', customStyle: {width: '10%'}},
];

const EmptyTd = ({colSpan = 1}) => <td colSpan={colSpan}  style={{textAlign: 'center'}}>-</td>;

const TotalTd = ({value}) =>
        <td className={`text-center ${Math.round(value) != 100 ? 'bg-danger text-danger': ''}`}>{value} %</td>

const isLetter = (c) => c.toLowerCase() != c.toUpperCase();
const getShortName = (name) => {
  let acc = '';
  for (var i = 0; i < name.length; i++) {
    if (isLetter(name[i])) { break;}
    acc += name[i];
  }
  return acc;
}

class TablaMedicion extends Component {

  render() {
    const { actividadesGroupedByFactor } = this.props;

    let dimensiones = getContext().dimensiones;
    const factoresGrouped = groupBy(getContext().factores, f => f.dime_id);
    dimensiones = dimensiones.map(d => {
      const size = factoresGrouped[d.id]
              .map(f => actividadesGroupedByFactor[f.id] ? actividadesGroupedByFactor[f.id].length: 1)
              .reduce((acc, v) => acc + v, 0);
      return ({...d, size: size +  factoresGrouped[d.id].length })
    })

    return (
      <Table bordered>
          <TableHeader headers={headers}/>
          <tbody>
          {
            dimensiones.map(d => {
              return flatMap(factoresGrouped[d.id], (f, fi) => {

                const shortFactorName = getShortName(f.display);
                let totalFactor = 0;
                let totalAvance = 0;
                if (actividadesGroupedByFactor[f.id] && actividadesGroupedByFactor[f.id].length) {
                  return ([ flatMap(actividadesGroupedByFactor[f.id], (a, ai) => {
                    const nivelFactor = +a.nivel_factor || 0;;
                    totalFactor += nivelFactor;
                    totalAvance += nivelFactor * (+ a.nivel_avance|| 0);
                    const isBaseEstandar = a.fact_id == f.id;

                    return (
                      <tr className="actividad_row" data-cod={shortFactorName}>
                        {
                          fi === 0 && ai == fi && (
                            <td rowSpan={d.size}>{d.display}</td>
                          )
                        }
                        {
                          ai === 0 && (
                            <td rowSpan={actividadesGroupedByFactor[f.id].length}>{f.display}</td>
                          )
                        }
                        <td>{ isBaseEstandar ? 'D': 'R'}</td>
                        <td>{a.act_cod} <div className="actividad--descripcion">{a.descripcion}</div> </td>
                        <td><EstandaresList estandares={a.estandares} baseEstandar={a.baseEstandar} /></td>
                        <td>
                        {
                          isBaseEstandar ?
                            <ActividadFactor id={a.act_id} fact_id={a.fact_id}/>:
                            <DetalleFactor id={a.act_id} aude_id={a.rel_aude_id} fact_id={f.id}/>
                        }
                        </td>
                        <td>
                        {
                          isBaseEstandar ?
                            <ActividadAvance id={a.act_id} fact_id={a.fact_id}/> :
                            <DetalleAvance id={a.act_id} aude_id={a.rel_aude_id} fact_id={f.id}/>
                        }
                        </td>
                      </tr>
                    )
                  }),
                  (
                    <tr className="total_row factor_row"  data-cod={shortFactorName}>
                      <td colSpan="4" style={{textAlign: 'center'}}>TOTAL</td>
                      <TotalTd value={totalFactor.toFixed(2)} />
                      <td className="text-align">{(totalAvance / 100.0).toFixed(2)} %</td>
                    </tr>
                  )])
                }

                return ([
                  <tr>
                    {
                      fi === 0  && (
                          <td rowSpan={d.size}>{d.display}</td>
                      )
                    }
                    <td rowSpan={1}>{f.display}</td>
                    <EmptyTd />
                    <EmptyTd />
                    <EmptyTd />
                    <EmptyTd />
                    <EmptyTd />
                  </tr>,
                  <tr className="total_row_empty">
                    <td colSpan="4" style={{textAlign: 'center'}}>TOTAL</td>
                    <EmptyTd />
                    <EmptyTd />
                  </tr>
                ])
              })
            })
          }
          </tbody>
        </Table>
    );
  }
}

const mapStateToProps = (state) => {
  const estandaresByFactor = _keyBy(getContext().estandares, e => e.id);
  const autoDetallesByAude = _keyBy(getContext().auto_detalles, e => e.aude_id);
  const actividadesGroupedByFactor = {};

  getActividades(state).toJS()
          .forEach(a => {
            const aude_id = a.aude_id;
            const esta_id = autoDetallesByAude[aude_id].esta_id;
            const fact_id = estandaresByFactor[esta_id].fact_id;

            a.estandares = [aude_id].concat(a.estandares);
            const act = {
              act_id: a.auac_id,
              fact_id,
              aude_id,
              act_cod: a.act_cod,
              nivel_avance: a.nivel_avance,
              descripcion: a.descripcion,
              nivel_factor: a.nivel_factor,
              baseEstandar: getContext().estandares.filter(e => e.id == esta_id)[0].esta_codigo,
              estandares: a.estandares.map(aude_id => {
                const esta_id = autoDetallesByAude[aude_id].esta_id;
                const estandar = getContext().estandares.filter(e => e.id == esta_id)[0];

                return estandar.esta_codigo;
              }).sort()
            };

            a.estandares.map(aude_id => {
                const esta_id = autoDetallesByAude[aude_id].esta_id;
                const estandar = getContext().estandares.filter(e => e.id == esta_id)[0];
                if (!(estandar.fact_id in actividadesGroupedByFactor)) {
                  actividadesGroupedByFactor[estandar.fact_id] = [];
                }
                const actNotFound = actividadesGroupedByFactor[estandar.fact_id]
                  .filter(a => a.act_id == act.act_id ).length == 0;
                let nivel_avance = a.nivel_avance;
                let nivel_factor = a.nivel_factor;
                if (a.aude_id != aude_id) {
                  const detalle = getDetalle(state, a.auac_id, aude_id);
                  nivel_avance = detalle.get('nivel_avance');
                  nivel_factor = detalle.get('nivel_factor');
                }
                if (actNotFound) {
                  actividadesGroupedByFactor[estandar.fact_id].push({
                    ...act,
                    rel_aude_id: aude_id,
                    nivel_avance,
                    nivel_factor,
                  });
                }

            })
          });

  return {
    actividadesGroupedByFactor,
  };
};

export default connect(mapStateToProps)(TablaMedicion);
