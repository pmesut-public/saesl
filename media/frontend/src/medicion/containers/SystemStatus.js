import React from 'react';
import { connect } from 'react-redux';

import { getContext } from 'common/context';
import Button from 'common/components/buttons';
import { getSummaryStatus } from '../reducer';
import { saveAll } from '../actividades/actions';
import { isFetching, getErrors } from 'autoevaluacion/tablaAuto/reducer';

const StatusBadges = ({
  status,
}) => {
  let attr = {
    background: null,
    icon: null,
    title: null,
  };

  switch (status) {
  case 'ALL_STORED':
    attr = {
      background: 'green',
      icon: 'fa-save',
      title: 'Guardado',
    };
    break;

  case 'ALL_HAS_CHANGED':
    attr = {
      background: 'yellow',
      title: 'Pendiente',
      icon: 'fa-exclamation',
    };
    break;

  case 'ALL_FETCHING':
    attr = {
      background: 'yellow',
      title: 'Guardando...',
      icon: 'fa-spin fa-spinner',
    };
    break;

  case 'ALL_ERROR':
    attr = {
      background: 'red',
      title: 'Error al guardar',
      icon: 'fa-exclamation-triangle',
    };
  }

  return (
    <label className={`badge bg-${attr.background}`}>
      <i title={attr.title} className={`fa ${attr.icon}`}></i>
    </label>
  );
};

const SystemStatus = ({
  summaryStatus,
  saveAll,
  closeFicha,
  leave,
  close,
  isFetching,
  errors,
}) => (
 <div className="well system-status">

    <div className="pull-right">
    Estado de la ficha: {" "}
    <StatusBadges status={summaryStatus} />
    </div>
    <Button
      warning
      onClick={leave}
      disabled={isFetching}
    >Volver a la autoevaluación</Button>
    <Button
      success
      onClick={saveAll}
     disabled={isFetching}
    >Guardar sin terminar</Button>
    {" "}
  {errors[0] && errors[0].length > 0 && (
    <div className="alert alert-danger">
        <div>Existen errores en las siguientes factores: </div>
    {errors[0]}
    </div>)
    }
 </div>
);



const mapStateToProps = (state) => ({
  summaryStatus: getSummaryStatus(state),
  isFetching: isFetching(state.get('system_status')),
  errors: getErrors(state.get('system_status')),
  leave: () => {
    window.location.href = '/autoevaluacion/calculator';
  },
});

const mapDispatchToProps = {
  saveAll,
};

export default connect(mapStateToProps, mapDispatchToProps)(SystemStatus);
