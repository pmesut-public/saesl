import constants from 'medicion/constants';
const {
  UPDATE_DETALLE,
} = constants;

import { getContext } from 'common/context';

const updateDetalle = (aude_id, auac_id, prop, value) => ({
  type: UPDATE_DETALLE,
  aude_id,
  auac_id,
  prop,
  value
});


export const _updateDetalleAvance = (aude_id, id, value) =>
  updateDetalle(aude_id, id, 'nivel_avance', value);

export const updateDetalleAvance = (fact_id, aude_id, id, value) => dispatch => {
  try {
    const found = getContext().vinculosFactor[fact_id][id];
    if (found) {
      found.ids.forEach(customId => {
        dispatch(_updateDetalleAvance(customId.split('_')[1], id, value));
      });
    }
  } catch(e) {console.log('e', e)}

  dispatch(_updateDetalleAvance(aude_id, id, value));
};

export const _updateDetalleFactor = (aude_id, id, value) =>
  updateDetalle(aude_id, id, 'nivel_factor', value);

export const updateDetalleFactor = (fact_id, aude_id, id, value) => dispatch => {
  try {
    const found = getContext().vinculosFactor[fact_id][id];
    if (found) {
      found.ids.forEach(customId => {
        dispatch(_updateDetalleFactor(customId.split('_')[1], id, value));
      });
    }
  } catch(e) {console.log('e', e)}

  dispatch(_updateDetalleFactor(aude_id, id, value));
};
