import React from 'react';
import { connect } from 'react-redux';

import { getDetalle } from '../reducer';
import CellPercentageInput from 'medicion/components/CellPercentageInput';
import { updateDetalleFactor } from '../actions';

const mapStateToProps = (state, { aude_id, id }) => ({
  value: getDetalle(state, id, aude_id).get('nivel_factor'),
});

const mapDispatchToProps = (dispatch, { fact_id, aude_id, id }) => ({
  onChange: (value) => dispatch(updateDetalleFactor(fact_id, aude_id, id, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CellPercentageInput);
