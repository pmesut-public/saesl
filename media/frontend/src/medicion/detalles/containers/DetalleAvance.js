import React from 'react';
import { connect } from 'react-redux';

import { getDetalle } from '../reducer';
import { updateDetalleAvance } from '../actions';

const CellPercentageInput = ({value}) => (
  <div>{value}</div>
)

const mapStateToProps = (state, { aude_id, id }) => ({
  disabled: true,
  value: getDetalle(state, id, aude_id).get('nivel_avance'),
});

const mapDispatchToProps = (dispatch, { fact_id, aude_id, id }) => ({
  onChange: (value) => dispatch(updateDetalleAvance(fact_id, aude_id, id, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CellPercentageInput);
