import { fromJS } from 'immutable';
import { createReducer } from 'redux-immutablejs';

import constants from 'medicion/constants';
const {
  UPDATE_DETALLE,
  ALL_STORED,
} = constants;

const initialState = fromJS({
  ids: [],
  items: {},
});

export default createReducer(initialState, {
  [ALL_STORED]: (state) => state.withMutations(state => {
    state.get('ids').forEach(id => {
      state.setIn(['items', id, 'hasChanged'], false);
    });
  }),

  [UPDATE_DETALLE]: (state, { aude_id, auac_id, prop, value }) =>
    state.setIn(['items', `${auac_id}_${aude_id}`, prop], value)
      .setIn(['items', `${auac_id}_${aude_id}`, 'hasChanged'], true),
});

export const getDetalle = (state, auac_id, aude_id) =>
  state.getIn(['auto_detalle_actividad', 'items', `${auac_id}_${aude_id}`]);

export const getDetalleIds = (state) =>
  state.getIn(['auto_detalle_actividad', 'ids']);

export const getDetalles = (state) =>
  state.getIn(['auto_detalle_actividad', 'ids'])
  .map(id => state.getIn(['auto_detalle_actividad', 'items', id]));

const mapString = (value) => typeof value == 'string' && value.length == 0 ? null: +value;
export const getPayloadDetalles = (state) =>
  getDetalles(state).toJS()
  .filter(a => a.hasChanged)
  .map(a => {
    const {hasChanged, nivel_factor, nivel_avance, ...props} = a;
    return {
      ...props,
      nivel_avance: mapString(nivel_avance),
      nivel_factor: mapString(nivel_factor),
    };
  });

export const getStatusDetalles = (state) =>
  state.getIn(['auto_detalle_actividad', 'ids'])
  .map(id => fromJS({
    hasChanged: state.getIn(['auto_detalle_actividad', 'items', id, 'hasChanged'])
  }));
