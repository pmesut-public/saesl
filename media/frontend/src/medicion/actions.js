import zipWith from 'lodash/zipWith';

import constants from 'autoevaluacion/constants';
const {
  SET_ERRORS,
  ALL_STORED,
  ALL_FETCHING,
  ALL_ERROR,
  FINISHED_REQUEST,
} = constants;
// import { formatEstandarShortName } from 'common/formatter';
// import { getContext } from 'common/context';
// import { getPayloadEstandares, getIdByEstandar }
//   from './estandares/reducer';
// import { getPayloadCriterios, getCriterios }
//   from './estandares/criterios/reducer';
// import { getPayloadEvidencias, getEvidencias }
//   from './estandares/evidencias/reducer';
// import { getPayloadDocumentos, getFiles }
//   from './estandares/documentos/reducer';
// import { getPayloadActividades } from './estandares/actividades/reducer';

export const setErrors = (errors) => ({
  type: SET_ERRORS,
  errors,
});

export const finishedRequest = () => ({
  type: FINISHED_REQUEST,
});

export const allStored = () => ({
  type: ALL_STORED,
});

export const allFetching = (id) => ({
  type: ALL_FETCHING,
  id,
});

export const allError = (msg) => dispatch => {
  dispatch({
    type: ALL_ERROR,
  });
  alert(msg);
};

const removeOverlay = () => {
  const elem = document.getElementById('auto_overlay');
  elem.parentElement.removeChild(elem);
};

const allSaveStored = () => dispatch => {
  removeOverlay();
  dispatch(allStored());
};

const allSaveFetching = () => dispatch => {
  var overlay = document.createElement('div');
  overlay.id = 'auto_overlay';
  var inner = document.createElement('div');
  inner.id = 'auto_inner_overlay';
  inner.innerHTML = '<div><i class="fa fa-refresh fa-animate"></i> Guardando</div>';
  overlay.appendChild(inner);
  document.querySelector('body').appendChild(overlay);

  dispatch(allFetching());
};

const allSaveError = (msg) => dispatch => {
  removeOverlay();
  dispatch(allError(msg));
};

const extractErrors = (getState) => {

  const inlineErrors = [...document.querySelectorAll('.medicion__row')]
          .map((item, i) => ($(item).find('.bg-danger').length > 0 || $(estandarRaws[i]).find('.bg-danger').length > 0) ? 'Faltan llenar campos obligatorios': '');

  // return logicErrors
  //   .map((errors, i) => ({
  //     esta_id: getContext().estandares[i].id,
  //     title: 'Estándar: ' + getContext().estandares[i].display,
  //     errors: errors + inlineErrors[i]
  //   }))
  //   .filter(item => item.errors.length > 0);
  return [];
};


const getPayload = (getState) => {
  return {
    auto_detalles: getPayloadEstandares(getState().get('auto_detalles')),
  };
};

const saveAllData = (dispatch, getState, api, cb) => {
  const payload = getPayload(getState);

  const shouldNotSave = Object.keys(payload)
          .map(k => payload[k].length)
          .reduce((acc, i) => acc + i, 0) === 0;


  const zipErrors = extractErrors(getState);
  console.log('zip', zipErrors);
  if (cb) {
    dispatch(setErrors(zipErrors));
  }

  if (shouldNotSave && !cb) {
    return;
  }

  
  api.saveAll({
    data: payload,
    onSuccess: () => {
      dispatch(allSaveStored());
      if (cb) {
        cb(zipErrors);
      }
    },
    onError: (err) => {
      console.log('err', err);
      dispatch(allSaveError());
    }
  });
};

export const saveAll = () => (dispatch, getState, {api}) => {
  saveAllData(dispatch, getState, api);
};
