import request from 'common/request';

const urls = {
  saveAll: '/medicion/save',
};

const createRequest = (url) => (props) => request({...props, url});

export const saveAll = createRequest(urls.saveAll);
