import { fromJS } from 'immutable';
import keyBy from 'lodash/keyBy';

import reducer from './reducer';

const getActId = a => a.auac_id;
const getDetalleId = d => `${d.auac_id}_${d.aude_id}`;

export default (state, context) => {
  let data = fromJS(reducer(undefined, {}));

  const estandaresByFactor = keyBy(context.estandares, e => e.id);
  const autoDetallesByAude = keyBy(context.auto_detalles, e => e.aude_id);

  const vinculosFactor = {};
  if (state.auto_detalle_actividad) {
    data = data
      .setIn(['auto_detalle_actividad', 'ids'], fromJS(state.auto_detalle_actividad.map(getDetalleId)))
      .setIn(['auto_detalle_actividad', 'items'], fromJS(keyBy(state.auto_detalle_actividad, getDetalleId)));
  }

  if (state.auto_actividades) {
    state.auto_actividades.forEach(act => {
      act.estandares.forEach(aude_id => {
        const esta_id = autoDetallesByAude[aude_id].esta_id;
        const fact_id = context.estandares.filter(e => e.id == esta_id)[0].fact_id;

        if (!(fact_id in vinculosFactor)) {
          vinculosFactor[fact_id] = {};
        }
        if (!(act.auac_id in vinculosFactor[fact_id])) {
          vinculosFactor[fact_id][act.auac_id] = {
            ids: [],
            nivel_avance: null,
            nivel_factor: null,
          };
        }

        const customId = `${act.auac_id}_${aude_id}`;
        const detalle = data.getIn(['auto_detalle_actividad', 'items', customId]);
        vinculosFactor[fact_id][act.auac_id].ids.push(customId);

        if (detalle &&
            +vinculosFactor[fact_id][act.auac_id].nivel_avance < +detalle.get('nivel_avance')) {
          vinculosFactor[fact_id][act.auac_id].nivel_avance = detalle.get('nivel_avance');
        }
        if (detalle &&
            +vinculosFactor[fact_id][act.auac_id].nivel_factor < +detalle.get('nivel_factor')) {
          vinculosFactor[fact_id][act.auac_id].nivel_factor = detalle.get('nivel_factor');
        }
      });
    });
  }

  Object.keys(vinculosFactor).forEach(fact_id => {
    Object.keys(vinculosFactor[fact_id]).forEach(act_id => {
      const {
        ids,
        nivel_avance,
        nivel_factor,
      } = vinculosFactor[fact_id][act_id];

      ids.forEach(customId => {
        const found = data .getIn(['auto_detalle_actividad', 'items', customId]);
        if (found) {
          data = data
            .setIn(['auto_detalle_actividad', 'items', customId, 'nivel_factor'], nivel_factor);
        }
        const avance = state.auto_actividades.filter(a => a.auac_id == act_id)[0].nivel_avance;
        data = data 
          .setIn(['auto_detalle_actividad', 'items', customId, 'nivel_avance'], avance)

        if (found && (
          avance != found.get('nivel_avance')) || 
          nivel_factor != data.getIn(['auto_detalle_actividad', 'items', customId, 'nivel_factor']
        )) {
          data = data
            .setIn(['auto_detalle_actividad', 'items', customId, 'hasChanged'], true);
        }
      });

    });
  });

  if (state.auto_actividades) {
    state.auto_actividades.forEach(act => {
      const esta_id = autoDetallesByAude[act.aude_id].esta_id;
      const fact_id = context.estandares.filter(e => e.id == esta_id)[0].fact_id;
      if (vinculosFactor[fact_id][act.auac_id]) {

        vinculosFactor[fact_id][act.auac_id].ids.forEach(customId => {
          const found = data .getIn(['auto_detalle_actividad', 'items', customId]);
          if (found) {
            data = data
              .setIn(['auto_detalle_actividad', 'items', customId, 'nivel_factor'], act.nivel_factor)
              .setIn(['auto_detalle_actividad', 'items', customId, 'nivel_avance'], act.nivel_avance);
          }
        });
      }
    });

    data = data
      .setIn(['auto_actividades', 'ids'], fromJS(state.auto_actividades.map(getActId)))
      .setIn(['auto_actividades', 'items'], fromJS(keyBy(state.auto_actividades, getActId)));
  }
    console.log(context);

  console.log('initial data medicion', data.toJS());
  context.vinculosFactor = vinculosFactor;
  return {
    initialState: data,
    context,
  };
}
