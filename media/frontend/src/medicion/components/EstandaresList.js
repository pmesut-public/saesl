import React from 'react';

const EstandaresList = ({ estandares, baseEstandar }) => (
  <div>
    {
      estandares.sort().map((e, i) =>
        <label key={e} className={`estandar ${e == baseEstandar ? 'base--estandar': ''}`}>
          {`E${e}`}
        </label>
      )
    }
 </div>
);

export default EstandaresList;
