import React from 'react';
import { InputField } from 'common/components/Form';

const CellPercentageInput = ({ value, onChange }) => (
    <InputField
  value={value || ''}
  onChange={onChange}
  required
  pattern={'^(100|[0-9]{1,2})%?$'}
  errorMsg='Debe ingresar un número, entre 0 y 100'
    />
);

export default CellPercentageInput;
