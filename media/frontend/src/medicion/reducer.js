import { fromJS } from 'immutable';
import { createReducer, combineReducers } from 'redux-immutablejs';

import system_status, { summaryStatus } from 'autoevaluacion/tablaAuto/reducer';
import auto_actividades, { getStatusActividades } from './actividades/reducer';
import auto_detalle_actividad, { getStatusDetalles } from './detalles/reducer';

export default combineReducers({
  system_status,
  auto_actividades,
  auto_detalle_actividad,
});

export const getSummaryStatus = (state) => {
  const statuses = [
    ...getStatusActividades(state).toJS(),
    ...getStatusDetalles(state).toJS(),
  ];

  const summaryHasChanged = statuses.map(s => s.hasChanged).reduce((acc, s) => acc || s, false);
  return summaryStatus(state.getIn(['system_status', 'status']), summaryHasChanged);
}
