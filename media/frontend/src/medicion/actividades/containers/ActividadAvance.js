import React from 'react';
import { connect } from 'react-redux';

import { getActividad } from '../reducer';
import { InputField } from 'common/components/Form';
import { updateActividadAvance } from '../actions';

const ActividadAvance = ({value, onChange}) => (
  <InputField
    value={value || ''}
    onChange={onChange}
    required
    pattern={'^(100|[0-9]{1,2})%?$'}
     errorMsg='Debe ingresar un número, entre 0 y 100'
  />
);

const mapStateToProps = (state, { id }) => ({
  value: getActividad(state, id).get('nivel_avance')
});

const mapDispatchToProps = (dispatch, { fact_id, id }) => ({
  onChange: (value) => dispatch(updateActividadAvance(fact_id, id, value))
});

export default connect(mapStateToProps, mapDispatchToProps)(ActividadAvance);
