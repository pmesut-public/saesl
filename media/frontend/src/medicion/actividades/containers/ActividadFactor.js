import React from 'react';
import { connect } from 'react-redux';

import { getActividad } from '../reducer';
import { InputField } from 'common/components/Form';
import { updateActividadFactor } from '../actions';

const ActividadFactor = ({value, onChange}) => (
  <InputField
    value={value || ''}
    onChange={onChange}
    required
    pattern={'^(100|[0-9]{1,2})%?$'}
    errorMsg='Debe ingresar un número, entre 0 y 100'
  />
);

const mapStateToProps = (state, { id }) => ({
  value: getActividad(state, id).get('nivel_factor')
});

const mapDispatchToProps = (dispatch, { fact_id, id }) => ({
  onChange: (value) => dispatch(updateActividadFactor(fact_id, id, value))
});

export default connect(mapStateToProps, mapDispatchToProps)(ActividadFactor);
