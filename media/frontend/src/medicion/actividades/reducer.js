import { fromJS } from 'immutable';
import { createReducer } from 'redux-immutablejs';

import constants from 'medicion/constants';
const {
  UPDATE_ACTIVIDAD,
  ALL_STORED,
} = constants;

const initialState = fromJS({
  ids: [],
  items: {},
});

export default createReducer(initialState, {
  [ALL_STORED]: (state) => state.withMutations(state => {
    state.get('ids').forEach(id => {
      state.setIn(['items', id, 'hasChanged'], false);
    });
  }),

  [UPDATE_ACTIVIDAD]: (state, {id, prop, value}) =>
    state.setIn(['items', id, prop], value)
      .setIn(['items', id, 'hasChanged'], true),
});

export const getActividad = (state, id) => state.getIn(['auto_actividades', 'items', id]);

export const getActividades = (state) =>
  state.getIn(['auto_actividades', 'ids'])
  .map(id => state.getIn(['auto_actividades', 'items', id]));

const mapString = (value) => typeof value == 'string' && value.length == 0 ? null: +value;

export const getPayloadActividades = (state) =>
  getActividades(state).toJS()
    .filter(a => a.hasChanged)
    .map(a => ({
      auac_id: a.auac_id,
      nivel_factor:  mapString(a.nivel_factor),
      nivel_avance: mapString(a.nivel_avance),
    }))

export const getStatusActividades = (state) =>
  state.getIn(['auto_actividades', 'ids'])
  .map(id => fromJS({
    hasChanged: state.getIn(['auto_actividades', 'items', id, 'hasChanged'])
  }));
