import _union from 'lodash/union';

import constants from 'medicion/constants';
const {
  UPDATE_ACTIVIDAD,
  SET_ERRORS,
  ALL_STORED,
  ALL_FETCHING,
  ALL_ERROR,
  FINISHED_REQUEST,
} = constants;


import { getContext } from 'common/context';

import { _updateDetalleFactor, _updateDetalleAvance } from '../detalles/actions';
import { getPayloadDetalles, getDetalleIds } from '../detalles/reducer';
import { getPayloadActividades } from './reducer';

const updateActividad = (id, prop, value) => ({
  type: UPDATE_ACTIVIDAD,
  id,
  prop,
  value
});

export const updateActividadFactor = (fact_id, id, value) => dispatch => {
  try {
    const found = getContext().vinculosFactor[fact_id][id];
    if (found) {
      found.ids.forEach(customId => {
        dispatch(_updateDetalleFactor(customId.split('_')[1], id, value));
      });
    }

  } catch(e) {console.log('e', e)}

  dispatch(updateActividad(id, 'nivel_factor', value));
};
export const updateActividadAvance = (fact_id, id, value) => (dispatch, getState) => {
  try {
    getDetalleIds(getState())
      .filter(vinculoId => vinculoId.split('_')[0] == id)
      .forEach(vinculoId => {
        const [actId, aude_id] = vinculoId.split('_');
        dispatch(_updateDetalleAvance(aude_id, actId, value));
      })
  } catch(e) {console.log('e', e)}

  dispatch(updateActividad(id, 'nivel_avance', value));
};

export const setErrors = (errors) => ({
  type: SET_ERRORS,
  errors,
});

export const finishedRequest = () => ({
  type: FINISHED_REQUEST,
});

export const allStored = () => ({
  type: ALL_STORED,
});

export const allFetching = (id) => ({
  type: ALL_FETCHING,
  id,
});

export const allError = (msg) => dispatch => {
  dispatch({
    type: ALL_ERROR,
  });
  alert(msg);
};

const removeOverlay = () => {
  const elem = document.getElementById('auto_overlay');
  elem.parentElement.removeChild(elem);
};

const allSaveStored = () => dispatch => {
  removeOverlay();
  dispatch(allStored());
};

const allSaveFetching = () => dispatch => {
  var overlay = document.createElement('div');
  overlay.id = 'auto_overlay';
  var inner = document.createElement('div');
  inner.id = 'auto_inner_overlay';
  inner.innerHTML = '<div><i class="fa fa-refresh fa-animate"></i> Guardando</div>';
  overlay.appendChild(inner);
  document.querySelector('body').appendChild(overlay);

  dispatch(allFetching());
};

const allSaveError = (msg) => dispatch => {
  removeOverlay();
  dispatch(allError(msg));
};

const getClassErrors = (className) =>
        [...document.querySelectorAll(className)]
        .map((item, i) => $(item).find('.bg-danger').length > 0 ?
             ''+$(item).data('cod'): '')
        .filter(e => e.length);

export const saveAll = () => (dispatch, getState, { api }) => {
  const auto_actividades = getPayloadActividades(getState());
  const auto_detalle_actividad = getPayloadDetalles(getState());
  dispatch(allSaveFetching());

  const dataErrors = getClassErrors('.actividad_row');
  const factorErrors = getClassErrors('.factor_row');

  dispatch(setErrors([_union(dataErrors, factorErrors).sort().join(' , ')]));

  api.saveAll({
    data: {
      auto_actividades,
      auto_detalle_actividad,
    },
    onSuccess: (res) => {
      dispatch(allSaveStored());
    },
    onError: (err) => {
      dispatch(allSaveError());
      console.log('err', err);
    }
  })
}
