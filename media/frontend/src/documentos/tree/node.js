import cx from 'classnames';
import React, { Component } from 'react';

class UITreeNode extends Component {
  renderCollapse = () => {
    const { index } = this.props;

    if (index.children && index.children.length) {
      const { collapsed } = index.node;

      return (
        <i
        className={cx(collapsed ? 'glyphicon glyphicon-chevron-right' : 'glyphicon glyphicon-chevron-down')}
          onMouseDown={e => e.stopPropagation()}
        />
      );
    }

    return null;
  };

  renderChildren = () => {
    const { index, tree, dragging } = this.props;

    if (index.children && index.children.length) {
      const childrenStyles = {
        paddingLeft: this.props.paddingLeft
      };

      return (
        <div className="children" style={childrenStyles}>
          {index.children.map(child => {
            const childIndex = tree.getIndex(child);

            return (
              <UITreeNode
                tree={tree}
                index={childIndex}
                onSelect={this.props.onSelect}
                selected={this.props.selected}
                key={childIndex.id}
                dragging={dragging}
                paddingLeft={this.props.paddingLeft}
                onCollapse={this.props.onCollapse}
                onDragStart={this.props.onDragStart}
              />
            );
          })}
        </div>
      );
    }

    return null;
  };

  render() {
    const { tree, index, dragging, selected } = this.props;
    const { node } = index;
    const styles = {};
    const selec = selected[node.id];

    return (
      <div
        className={cx('m-node', {
          placeholder: index.id === dragging
        }, {selected: selec})}
        style={styles}
      >
        <div className="inner " ref="inner" onMouseDown={this.handleMouseDown}
      onClick={this.handleCollapse}
        >
          {this.renderCollapse()}
          {tree.renderNode(node)}
        </div>
        {node.collapsed ? null : this.renderChildren()}
      </div>
    );
  }

  handleCollapse = e => {
    e.stopPropagation();
    const nodeId = this.props.index.id;

    this.props.onSelect(this.props.index.node);
    if (this.props.onCollapse) {
      this.props.onCollapse(nodeId);
    }
  };

  handleMouseDown = e => {
    const nodeId = this.props.index.id;
    const dom = this.refs.inner;

    // if (this.props.onDragStart) {
    //   this.props.onDragStart(nodeId, dom, e);
    // }
  };
}

module.exports = UITreeNode;
