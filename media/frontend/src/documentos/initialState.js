import { fromJS } from 'immutable';

import reducer from './reducer';

export default (state) => {
  let data = fromJS(reducer(undefined, {}));

  if (!state || !state.files) {
    return data;
  }

  data = data.set('files', fromJS(
	  state.files.map(f => Object.assign(f, {name: f.display}))
	));
  return data;
}
