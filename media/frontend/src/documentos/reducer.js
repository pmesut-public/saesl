import { fromJS } from 'immutable';
import { createReducer, combineReducers } from 'redux-immutablejs';

import toolbar from './toolbar/reducer';
import files from './fileViewer/reducer';
import status from 'status/reducer';

export default combineReducers({toolbar, files, status});
