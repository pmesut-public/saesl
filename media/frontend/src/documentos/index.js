import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import App from './containers'
import createStore from './store'
import getInitialState from './initialState'
import { setContext } from 'common/context'

const context = window.__CONTEXT__ || {}
let initialState = window.__INITIAL_STATE__
if (initialState.estandares) {
  context.estandares = initialState.estandares
}
if (initialState.ind) {
  context.ind = initialState.ind
}
context.estados = context.estados || initialState.estados
context.sub_stados = context.estados || initialState.sub_estados

if (!context.hasOwnProperty('editable')) {
  context.editable = false
}
setContext({ ...initialState, ...context })
initialState = getInitialState(initialState, context)

const store = createStore(initialState)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
