import React, { Component } from 'react'
import { connect } from 'react-redux'

import { getFiles } from '../reducer'
import FileItem from './FileItem'

const FileList = ({ files, ref }) => (
  <div style={{ height: '500px', overflowY: 'auto' }} ref={ref}>
    {files.map(file => <FileItem key={file.get('id')} id={file.get('id')} />)}
  </div>
)

const mapStateToProps = state => ({
  files: getFiles(
    state.get('files'),
    state.getIn(['toolbar', 'filterName']),
    state.getIn(['toolbar', 'sortBy'])
  ).filter(f => !f.get('formato')),
})

export default connect(mapStateToProps)(FileList)
