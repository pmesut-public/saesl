import React, { Component } from 'react'
import { connect } from 'react-redux'

import { getContext } from 'common/context'
import { getFiles } from '../reducer'
import { isSelectable, getCurrentView } from 'documentos/toolbar/reducer'
import { loadDocumentos } from '../actions'
import Folder from './Folder'
import FileList from './FileList'
import Vigencia from '../Vigencia'
import TreeView from '../TreeView'

class FileViewer extends Component {
  setRef = el => (this.el = $(el))

  componentDidMount() {
    if (this.props.id) {
      this.props.loadDocumentos(this.props.id)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.filesSize < nextProps.filesSize && this.el) {
      this.el.scrollTop(10000)
    }
  }

  render() {
    const { currenView } = this.props
    switch (currenView) {
      case 0:
        return <FileList ref={this.setRef} />
      case 1:
        return <Folder />
      case 2:
        return <TreeView />
      case 3:
        return <Vigencia />
    }
  }
}

const mapStateToProps = state => ({
  filesSize: getFiles(
    state.get('files'),
    state.getIn(['toolbar', 'filterName']),
    state.getIn(['toolbar', 'sortBy'])
  ).size,
  currenView: getCurrentView(state.get('toolbar')),
})

const mapDispatchToProps = {
  loadDocumentos,
}

export default connect(mapStateToProps, mapDispatchToProps)(FileViewer)
