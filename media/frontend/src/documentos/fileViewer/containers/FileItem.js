import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col } from 'react-bootstrap'

import { isSelectable } from 'documentos/toolbar/reducer'
import { FakeFileItem, FileItemDetails } from '../components'
import { isCurrentActionFetching } from 'status/reducer'
import {
  removeFileAPI,
  renameFileAPI,
  updatePropFile,
  toggleFileSelection,
} from '../actions'
import { getFile } from '../reducer'

const fileImg =
  'http://icons.iconarchive.com/icons/zhoolego/material/512/Filetype-Docs-icon.png'

class FileItem extends Component {
  _downloadFile = () =>
    $('<form></form>')
      .attr('action', '/file/' + this.props.file.get('path'))
      .appendTo('body')
      .submit()
      .remove()

  render() {
    const {
      file,
      removeFile,
      isFetching,
      isCurrentFetching,
      renameFile,
      cleanStatus,
      toggleFileSelection,
      showRemove,
    } = this.props

    let {
      path,
      name,
      fetching,
      error,
      loading,
      msg,
      status,
      typeError,
      newName,
      dbId,
      selected,
      esta_ids,
      past_esta_ids,
      estado,
      sub_estado,
      docu_fecha_reg,
      fecha_inicio,
      fecha_fin,
      da,
      formato,
    } = file

    const canRename = typeError === 'EXISTING_FILE'
    const shouldUseFakeItem = (loading || error) && !canRename

    let fileComponent
    if (shouldUseFakeItem) {
      fileComponent = (
        <FakeFileItem
          error={error}
          name={name}
          fecha_reg={docu_fecha_reg}
          msg={msg}
          canRename={canRename}
          onRename={() => renameFile(dbId, name)}
        />
      )
    } else {
      fileComponent = (
        <FileItemDetails
          path={path}
          renameFile={renameFile}
          onSelect={toggleFileSelection}
          name={name}
          fecha_reg={docu_fecha_reg}
          fetching={fetching || isCurrentFetching}
          removeFile={removeFile}
          downloadFile={this._downloadFile}
          fileImg={fileImg}
          canRename={canRename}
          status={status}
          msg={msg}
          canRenameError={canRename}
          selected={selected}
          cleanStatus={cleanStatus}
          showRemove={showRemove}
          esta_ids={esta_ids}
          past_esta_ids={past_esta_ids}
          estado={estado}
          sub_estado={sub_estado}
          fecha_inicio={fecha_inicio}
          fecha_fin={fecha_fin}
          da={da}
          formato={formato}
        />
      )
    }

    return (
      <Col xs={6} sm={4} md={3} lg={2}>
        {fileComponent}
      </Col>
    )
  }
}

const mapStateToProps = (state, { id }) => ({
  file: getFile(state.get('files'), id),
  isCurrentFetching: isCurrentActionFetching(state.get('status'), id),
  showRemove: !isSelectable(state.get('toolbar')),
})

const mapDispatchToProps = (dispatch, { id }) => ({
  removeFile: () => dispatch(removeFileAPI(id)),
  renameFile: (name, cb) => dispatch(renameFileAPI(id, name, cb)),
  cleanStatus: () => dispatch(updatePropFile(id, 'msg', '')),
  toggleFileSelection: () => dispatch(toggleFileSelection(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(FileItem)
