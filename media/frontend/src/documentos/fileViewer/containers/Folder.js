import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Thumbnail, Glyphicon, Col, Row } from 'react-bootstrap'

import { mapStateToProps } from '../TreeView'
import FileItem from './FileItem'
import Button from 'common/components/buttons'
import { getContext } from 'common/context'
import { groupFilesByEstandar, getFiles } from '../reducer'
import { isFetching } from 'status/reducer'

const folderIcon =
  'http://icons.iconarchive.com/icons/dakirby309/simply-styled/256/Blank-Folder-icon.png'

const filesText = (files, num) => {
  if (!files) return ''
  const len = files.length
  return len + ' item' + (len == 1 ? '' : 's')
}

class Folders extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.state = {
      folderSelected: [],
    }
  }

  backFolder = () => {
    const { folderSelected } = this.state
    folderSelected.pop()
    this.setState({ folderSelected })
  }

  selectFolder = f => {
    const { folderSelected } = this.state
    this.setState({ folderSelected: folderSelected.concat(f) })
  }

  renderFolders() {
    const { tree } = this.props
    const { folderSelected } = this.state

    let folders = []
    const topLevel = folderSelected[folderSelected.length - 1]

    if (!topLevel) {
      folders = tree.children
    } else {
      folders = topLevel.children
    }
    if (!folders.length || (folders && !folders[0].children)) {
      return this.renderFiles(folders)
    }

    return folders.map((folder, i) => {
      const title = filesText(folder.children, folder.files)

      return (
        <Col xs={6} md={2} lg={2} key={folder.id}>
          <div
            className="thumbnail"
            onClick={() => this.selectFolder(folder)}
            style={{ cursor: 'pointer' }}
          >
            <h4 className="text-center">Carpetas = {folder.folders}</h4>
            <h4 className="text-center">Items = {folder.files}</h4>
            <img src={folderIcon} title={title} height="80" width="80" />
            <div className="caption text-center">{folder.module}</div>
          </div>
        </Col>
      )
    })
  }

  renderFiles(files) {
    // const {
    //   files,
    //   folders
    // } = this.props;

    const filteredFiles = files
    // const filteredFiles = files
    //   .filter(f => folders[this.state.folderSelected].file_ids.indexOf(f.get('id')) > -1);

    return filteredFiles.map(file => <FileItem key={file.id} id={file.id} />)
  }

  renderHeader = (folder, anyIsFetching) => (
    <div className="row" style={{ marginRight: 0 }}>
      <Col md={12}>
        <Button
          style={{ marginLeft: '15px' }}
          onClick={this.backFolder}
          disabled={anyIsFetching}
        >
          Atrás
        </Button>
        <h4 style={{ display: 'inline-block', marginLeft: '5px' }}>
          {' '}
          {folder.module} : {filesText(folder.children, folder.files)}
        </h4>
      </Col>
    </div>
  )

  render() {
    const { tree, anyIsFetching } = this.props

    const { folderSelected } = this.state

    const topLevel = folderSelected[folderSelected.length - 1]

    return (
      <div style={{ height: '500px', overflowY: 'auto' }}>
        {topLevel &&
          topLevel.children &&
          this.renderHeader(topLevel, anyIsFetching)}
        {this.renderFolders()}
      </div>
    )
  }
}

// const mapStateToProps = (state) => ({
//   folders: groupFilesByEstandar(state.get('files'), getContext().estandares),
//   files: getFiles(
//     state.get('files'),
//     state.getIn(['toolbar', 'filterName']),
//     state.getIn(['toolbar', 'sortBy'])
//   ),
//   anyIsFetching: isFetching(state.get('status')),
// });

const mapState = state => {
  return {
    ...mapStateToProps(state),
    anyIsFetching: isFetching(state.get('status')),
  }
}

const mapDispatchToProps = {}

export default connect(mapState, mapDispatchToProps)(Folders)
