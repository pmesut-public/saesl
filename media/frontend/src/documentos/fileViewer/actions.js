import { v4 } from 'node-uuid'

import { getFiles } from 'autoevaluacion/tablaAuto/estandares/documentos/reducer'
import * as messages from '../messages'
import constants from '../constants'

import { getContext, setContext } from 'common/context'
const {
  LOAD_FILES,
  TOGGLE_FILE_SELECTION,
  ADD_FILE,
  REMOVE_FILE,
  FILE_UPLOADED,
  FILE_UPLOADED_ERROR,
  REMOVE_UPLOADED_ERROR,
  REMOVING_FILE,
  ADD_REAL_FILE,
  UPDATE_PROP_FILE,
} = constants

import { statusSuccess, statusFetching, statusError } from 'status/actions'

import { isSelectable } from 'documentos/toolbar/reducer'
import { getFile } from './reducer'

export const updatePropFile = (id, prop, value) => ({
  type: UPDATE_PROP_FILE,
  id,
  prop,
  value,
})

const addRealFile = props => ({
  type: ADD_FILE,
  ...props,
})

const addFakeFile = (id, name) => ({
  type: ADD_FILE,
  id,
  name,
  loading: true,
  msg: 'Subiendo archivo :',
})

const updateFile = (name, dbId) => ({
  type: FILE_UPLOADED,
  name,
  dbId,
})

export const removeFile = id => ({
  type: REMOVE_FILE,
  id,
})

const fileUploadedError = (
  id,
  msg = 'Error al subir el archivo:',
  typeError = '',
  name
) => ({
  type: FILE_UPLOADED_ERROR,
  id,
  msg,
  typeError,
  name,
})

const removeUploadedError = () => ({
  type: REMOVE_UPLOADED_ERROR,
})

export const onFileUploadedError = (uuid, msg) => dispatch => {
  dispatch(statusError())
  dispatch(fileUploadedError(uuid, msg, 'ERROR'))
}

export const uploadFile = (actionId, name, uuid) => dispatch => {
  dispatch(removeUploadedError())
  dispatch(statusFetching(actionId))
  dispatch(addFakeFile(uuid, name))
}

export const onUploadErrorExistingName = data => dispatch => {
  //if existing file
  const {
    name,
    status,
    msg,
    uuid,
    id,
    path,
    estado,
    sub_estado,
    fecha_inicio,
    fecha_fin,
    da,
  } = data
  dispatch(statusSuccess())
  dispatch(removeFile(uuid))
  dispatch(
    addRealFile({
      path,
      id,
      name,
      status,
      msg,
      display: name,
      estado,
      sub_estado,
    })
  )
}

export const renameFileAPI = (dbId, name, cb) => (
  dispatch,
  _,
  { apiDocumentos }
) => {
  dispatch(statusFetching(dbId))

  apiDocumentos.renameFile({
    data: {
      id: dbId,
      display: name,
    },
    onSuccess: ({ id, display: name }) => {
      dispatch(statusSuccess())
      dispatch(updatePropFile(id, 'name', name))
      cb()
    },
    onError: res => {
      console.log('err rename', res)
      dispatch(statusError())
      let msg = res.msg
      console.log('msg', msg)
      let status = res.status
      if (res.status === 'EXISTING_FILE') {
        msg += '. Intente el nombre ' + res.display + ' .'
      }
      if (res.status === 'DENIED') {
        status = 'EXISTING_FILE'
      }
      console.log(status)
      dispatch(fileUploadedError(dbId, msg, status))
    },
  })
}

export const fileUploaded = props => (dispatch, getState) => {
  dispatch(removeFile(props.uuid))
  dispatch(statusSuccess())
  dispatch(addRealFile(props))
}

const removingFile = (id, fetching) => ({
  type: REMOVING_FILE,
  id,
  fetching,
})

export const removeFileAPI = id => (dispatch, getState, { apiDocumentos }) => {
  if (!confirm(messages.CONFIRM_REMOVE_FILE)) {
    return
  }

  dispatch(statusFetching(id))
  dispatch(removingFile(id, true))

  apiDocumentos.removeFile({
    data: { id },
    onSuccess: res => {
      dispatch(statusSuccess())
      dispatch(removeFile(id))
    },
    onError: res => {
      dispatch(statusError())
      dispatch(removingFile(id, false))
      const name = getFile(getState().get('files'), id).get('name')
      alert(
        res.msg ||
          `El archivo ${name} no se pudo eliminar. Intentelo nuevamente`
      )
    },
  })
}

const loadFiles = files => ({
  type: LOAD_FILES,
  files,
})

export const loadDocumentos = id => (dispatch, getState, { apiDocumentos }) => {
  dispatch(loadFiles([]))
  apiDocumentos.getFiles({
    data: {},
    onSuccess: res => {
      /*
    onError: () => {
      const res = {
        lic_a: [{ for_id: '1', display: 'la1' }],
        lic_c: [],

        files: [
          {
            fecha_inicio: '12/12/2005',
            fecha_fin: '12/12/2005',
            estado: 0,
            sub_estado: 8,
            id: '1',
            display: 'apq.doc',
            path: 'asdfadf',
            esta_ids: [],
            past_esta_ids: [],
            for_ids: [1],
            past_for_ids: [],
            cond_ids: [],
            past_cond_ids: [],
          },
          {
            fecha_inicio: '10/12/2005',
            fecha_fin: '10/12/2005',
            estado: 0,
            sub_estado: 8,
            id: '9',
            display: 'testerino.doc',
            path: 'testerinopath',
            esta_ids: [],
            past_esta_ids: [],
            for_ids: [9],
            past_for_ids: [],
            cond_ids: [],
            past_cond_ids: [],
          },
        ],
      }
      */
      setContext({
        ind: res.ind,
        ...getContext(),
        lic_a: res.lic_a,
        lic_c: res.lic_c,
        cond: res.cond,
        auto_detalles: res.auto_detalles,
        auto_medio: res.auto_medio,
      })
      const existingFiles = getFiles(
        getState().get('auto_documentos'),
        id
      ).toJS()
      const filesWithSelection = res.files
        .map(f => ({
          ...f,
          selected: existingFiles.filter(ef => ef.docu_id == f.id).length !== 0,
        }))
        .map(f => ({ ...f, audo_estado: '', name: f.display }))
      dispatch(loadFiles(filesWithSelection))
    },
    onError: err => {
      console.log('err', err)
    },
  })
}

export const toggleFileSelection = id => (dispatch, getState) => {
  if (!isSelectable(getState().get('toolbar'))) {
    return
  }

  dispatch({
    type: TOGGLE_FILE_SELECTION,
    id,
  })
}
