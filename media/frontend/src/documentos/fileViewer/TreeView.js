import React from 'react';
import Tree from 'documentos/tree/react-ui-tree';
import { connect } from 'react-redux';
import { Glyphicon } from 'react-bootstrap';
import _ from 'lodash';

import { createZip, createZipTree } from 'documentos/api';
import { getContext } from 'common/context';

import { toggleFileSelection } from './actions';
import Button, { RemoveButton } from 'common/components/buttons';
class App extends React.Component {
  constructor(props) {
    super(props);

    const selection = props.selection.map(s => ({
      id: s.id,
      name: s.display || s.module,
    }));
    const selected = selection.reduce(
      (acc, f) => ({ ...acc, [f.id]: true }),
      {}
    );

    this.state = {
      active: null,
      tree: props.tree,
      selected,
      selection,
      display: '',
      path: '',
      zipping: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { filesLength } = this.props;
    const variosFolder = _.last(this.state.tree.children);
    if (prevProps.filesLength < filesLength && !variosFolder.collapsed) {
      const { tree } = this.state;
      const newFile = _.last(_.last(this.props.tree.children).children);
      if (newFile.display) {
        tree.children[tree.children.length - 1].children.push(newFile);
        this.setState({ tree });
      }
    }
  }

  renderNode = node => {
    const isLeaf = !node.children;
    return (
      <span className={`${isLeaf ? '' : 'leaf'}`}>
        <span className="wrapper">
          <Glyphicon glyph={`${isLeaf ? 'file' : 'folder-open'}`} />{' '}
          {node.module}
        </span>
      </span>
    );
  };

  onClickNode = node => {
    if (!node.children) {
      let { selected, selection } = this.state;
      selected[node.id] = !selected[node.id];
      this.props.toggleFileSelection(node.id);
      if (selected[node.id]) {
        selection.push({ id: node.id, name: node.module });
      } else {
        selection = selection.filter(n => n.id !== node.id);
      }
      this.setState({
        selected,
        selection,
        display: '',
        path: '',
      });
    }
  };

  generateZip = () => {
    this.setState({ zipping: true });
    createZip({
      data: { ids: this.state.selection.map(s => s.id) },
      onSuccess: ({ display = 'display', path = 'path' }) => {
        this.setState({
          zipping: false,
          display,
          path,
        });
      },
      onError: () => {
        this.setState({
          zipping: false,
        });
      },
    });
  };

  generateZipTree = () => {
    this.setState({
      tree: { ...this.state.tree, zipping: true },
    });
    createZipTree({
      data: {},
      onSuccess: ({ display = 'display', path = 'path' }) => {
        this.setState({
          tree: { ...this.state.tree, zipping: false, display, path },
        });
      },
      onError: (err) => {
        this.setState({
          tree: { ...this.state.tree, zipping: false },
        });
      },
    });
  };

  renderDownloadTreeButton = () => {
    const { zipping, path, display } = this.state.tree;
    if (path && !zipping) {
      return (
        <a href={`/file/${path}`} target="_blank">
          <Button primary>
            <Glyphicon glyph="download-alt" /> {display}{' '}
          </Button>
        </a>
      );
    }

    if (zipping) {
      return (
        <Button info disabled>
          Generando arbol en zip ...
        </Button>
      );
    }

    return (
      <Button info onClick={this.generateZipTree}>
        Generar arbol en zip
      </Button>
    );
  };

  renderButton = () => {
    const { selection, path, display, zipping } = this.state;

    if (path && display) {
      return (
        <a href={`/file/${path}`} target="_blank">
          <Button primary>
            <Glyphicon glyph="download-alt" /> {display}{' '}
          </Button>
        </a>
      );
    }

    const len = selection.length;
    if (len && !zipping) {
      return (
        <Button info onClick={this.generateZip}>
          Generar zip para {len} archivo{len > 1 ? 's' : ''}
        </Button>
      );
    }
    if (selection.length && zipping) {
      return (
        <Button info disabled>
          Generando archivo zip ...
        </Button>
      );
    }
    return null;
  };

  render() {
    const len = this.state.selection.length;
    const { path, display } = this.state;
    return (
      <div className="app container-fluid">
        <div className="">
          <div className="tree col-md-4">
            <Tree
              onSelect={this.onClickNode}
              selected={this.state.selected}
              paddingLeft={20}
              tree={this.state.tree}
              onChange={this.handleChange}
              isNodeCollapsed={this.isNodeCollapsed}
              renderNode={this.renderNode}
            />
          </div>
          <div className="tree-selection col-md-4">
            {this.renderDownloadTreeButton()}
            <div className="panel panel-default">
              <div className="panel-heading">
                Archivos seleccionados
                {this.renderButton()}
              </div>
              <div
                className="panel-body panel-tree"
                style={{ maxHeight: 400, minHeight: 400 }}
              >
                <ul className="items-container">
                  {this.state.selection.map(node => (
                    <li key={node.id} className="selection-item">
                      <RemoveButton
                        onClick={() => this.onClickNode(node)}
                        size="btn-xs"
                      />
                      <span style={{ marginLeft: '5px' }}>{node.name}</span>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleChange = tree => {
    this.setState({
      tree: tree,
    });
  };

  updateTree = () => {
    const { tree } = this.state;
    tree.children.push({ module: 'test' });
    this.setState({
      tree: tree,
    });
  };
}

const isLetter = c => c.toLowerCase() != c.toUpperCase();
const getShortName = name => {
  let acc = '';
  for (var i = 0; i < name.length; i++) {
    if (isLetter(name[i])) {
      break;
    }
    acc += name[i];
  }
  return acc;
};

const getIndicadores = (cond_id, acc) =>
  getContext()
    .ind.filter(e => e.cond_id == cond_id)
    .map(e => ({
      module: 'Indicador ' + e.cod,
      id: 'ind' + e.cod,
      children: acc[e.ind_id] || [],
      files: acc[e.ind_id] ? acc[e.ind_id].length : 0,
      collapsed: true,
      folders: 0,
      level: 2,
    }));

export const mapStateToProps = state => {
  const formatoA = [];
  const formatoB = [];
  const formatoC = [];
  const acc = {};
  const aloneFiles = [];
  const selection = [];
  const filesLength = state.get('files').size;
  state.get('files').forEach(f => {
    const file = {
      id: f.get('id'),
      display: f.get('display'),
      module: f.get('display'),
      path: f.get('path'),
    };
    f.get('for_ids').forEach(for_id => {
      const tipoA =
        getContext().lic_a.filter(l => l.for_id == for_id).length > 0;
      const tipoC =
        getContext().lic_c.filter(l => l.for_id == for_id).length > 0;
      if (tipoA) {
        formatoA.push(file);
      }
      if (tipoC) {
        formatoC.push(file);
      }
    });
    f.get('cond_ids').forEach(cond_id => {
      const isCond =
        getContext().cond.filter(c => c.cond_id == cond_id).length > 0;
      if (isCond) {
        formatoB.push(file);
      }
    });
    //es aude_id
    f.get('esta_ids').forEach(esta_id => {
      // const aude_id = getContext().auto_medio.filter(m => m.aume_id == aume_id)[0].aude_id;
      // const esta_id = getContext().auto_detalles.filter(d => d.aude_id == aude_id)[0].esta_id;
      if (!acc[esta_id]) {
        acc[esta_id] = [];
      }
      acc[esta_id].push(file);
    });

    if (f.get('selected')) {
      selection.push(file);
    }

    if (
      !f.get('for_ids').size &&
      !f.get('cond_ids').size &&
      !f.get('esta_ids').size
    ) {
      aloneFiles.push(file);
    }
  });

  const medios = getContext().cond.map(c => {
    const children = getIndicadores(c.cond_id, acc);
    return {
      module: 'Condicion ' + getShortName(c.display),
      id: 'cond' + c.cond_id,
      children,
      files: children.reduce((acc, c) => acc + c.files, 0),
      collapsed: true,
      folders: children.length,
      level: 1,
    };
  });

  const tree = {
    module: 'documentos',
    children: [
      {
        module: 'Formato A',
        id: 'fa',
        collapsed: true,
        children: formatoA,
        files: formatoA.length,
        level: 0,
        folders: 0,
      },
      {
        module: 'Formato B',
        id: 'fb',
        collapsed: true,
        children: formatoB,
        files: formatoB.length,
        level: 0,
        folders: 0,
      },
      {
        module: 'Formato C',
        id: 'fc',
        collapsed: true,
        children: formatoC,
        files: formatoC.length,
        level: 0,
        folders: 0,
      },
      {
        module: 'Medios de verificación',
        id: 'mv',
        collapsed: true,
        children: medios,
        files: medios.reduce((acc, m) => acc + m.files, 0),
        folders: medios.length,
        level: 0,
      },
    ],
  };
  if (aloneFiles.length) {
    tree.children.push({
      id: 'varios',
      module: 'Varios',
      collapsed: true,
      children: aloneFiles,
      files: aloneFiles.length,
      folders: 0,
      level: 0,
    });
  }
  return { tree, selection, filesLength };
};

export default connect(
  state => {
    const stateMapped = mapStateToProps(state);
    stateMapped.tree.children = stateMapped.tree.children.filter(
      leaf => leaf.module.indexOf('Formato') == -1
    );
    return stateMapped;
  },
  { toggleFileSelection }
)(App);
