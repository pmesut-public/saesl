import React from 'react';
import { Thumbnail, Glyphicon } from 'react-bootstrap';

import { GlyphiconRefresh } from 'common/components/media';
import { Center } from 'common/components/Layout';

export default ({
  msg,
  name,
  error,
  canRename,
  onRename,
}) => (
  <Thumbnail>
    <Center danger={error}>
    {
      error ?
        <Glyphicon glyph="alert"/>:
        <GlyphiconRefresh />
    }
      {`${msg} ${name}`}
    </Center>
    <Center danger={error}>
    {
      canRename &&
        <button onClick={onRename}>Renombrar</button>
    }
    </Center>
  </Thumbnail>
);
