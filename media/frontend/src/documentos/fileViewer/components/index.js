import FakeFileItem from './FakeFileItem';
import FileItemDetails from './FileItemDetails';

export {
  FakeFileItem,
  FileItemDetails,
};
