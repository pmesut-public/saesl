import React from 'react';
import { Thumbnail, Glyphicon, ButtonToolbar } from 'react-bootstrap';
import classNames from 'classnames';

import Status from 'common/components/Status';
import StatusBadge from 'common/components/StatusBadge';
import * as colors from 'common/colors';
import { getContext } from 'common/context';
import { InputField } from 'common/components/Form';
import Button from 'common/components/buttons';
import { GlyphiconRefresh } from 'common/components/media';
import {
  Center,
  PullLeft,
  PullRight,
  Clearfix,
} from 'common/components/Layout';
import { formatEstandarShortName } from 'common/formatter';
import {
  getVigenciaValues,
  vigenciaTitle,
  getEstadoLetter,
} from 'licenciamiento/formatos/containers/Formato';

const styleLetter = {
  position: 'absolute',
  color: 'black',
  left: '50%',
  top: '50%',
  transform: 'translate(-50%, -50%)',
};

const formatIds = (arr, test) =>
  arr.size
    ? arr
        .toJS()
        .map(id =>
          formatEstandarShortName(
            getContext().estandares.filter(esta => {
              if (esta.hasOwnProperty('esta_id')) {
                return id == esta.esta_id;
              }
              return id == esta.id;
            })[0]
          )
        )
        .join(',')
    : '-';

export default class FileItemDetails extends React.Component {
  state = {
    isEditing: false,
    tempName: '',
    ext: '',
  };

  componentDidMount() {
    if (!this.props.msg || !this.props.msg.length) return;
    this.props.cleanStatus();
    // this.timeout = setTimeout(() => {
    // }, 3000);
  }

  toggleEditing = e => {
    if (e) {
      e.stopPropagation();
    }
    const nameSplited = this.props.name.split('.');
    this.setState({
      isEditing: !this.state.isEditing,
      tempName: nameSplited.slice(0, nameSplited.length - 1).join('.'),
      ext: nameSplited.slice(-1).join(''),
    });
  };

  updateTempName = tempName => {
    this.setState({
      tempName,
    });
  };

  updateName = e => {
    e.stopPropagation();
    this.props.renameFile(
      this.state.tempName + '.' + this.state.ext,
      this.toggleEditing
    );
  };

  render() {
    const {
      formato,
      canRenameError,
      name,
      fecha_reg,
      fetching,
      removeFile,
      downloadFile,
      fileImg,
      msg,
      showRemove,
      onSelect,
      selected,
      esta_ids,
      past_esta_ids,
      estado,
      sub_estado,
      fecha_inicio,
      fecha_fin,
    } = this.props;

    const vigenciaData = getVigenciaValues(this.props);

    const { isEditing, tempName } = this.state;

    const classnames = classNames('thumbnail', { selected: selected });

    const removeButton = !esta_ids.size &&
      getContext().editable && (
        <PullLeft>
          <Button disabled={fetching} danger onClick={removeFile}>
            {fetching ? <GlyphiconRefresh /> : <Glyphicon glyph="remove" />}
          </Button>
        </PullLeft>
      );
    const vigencia =
      fecha_inicio && fecha_fin ? `${fecha_inicio}-${fecha_fin} \n` : '';

    // const fileTitle = `${name} \n E: ${formatIds(esta_ids)} \n Ea: ${formatIds(past_esta_ids)}`;
    const fileTitle = `${name}\n ${vigencia} E: ${formatIds(esta_ids)}`;

    const editButton = !esta_ids.size &&
      !isEditing &&
      getContext().editable && (
        <PullRight>
          <Button info onClick={this.toggleEditing} disabled={fetching}>
            <Glyphicon glyph="pencil" />
          </Button>
        </PullRight>
      );

    const downloadButton = !isEditing && (
      <PullRight>
        <a href={`/file/${this.props.path}`} target="_blank">
          <Button>
            <Glyphicon glyph="download-alt" />
          </Button>
        </a>
      </PullRight>
    );

    const styleMsg = { padding: '8px', marginBottom: '0' };

    //const bg = getContext().estados[0].id == estado ? colors.yellow : colors.green;

    const bg = esta_ids.size ? colors.green : colors.yellow;

    const usedTitle = esta_ids.size ? (
      <h4 className="text-center">{esta_ids.size} Medios</h4>
    ) : (
      <h4 className="text-center">Sin uso</h4>
    );

    return (
      <div
        className={classnames}
        onClick={formato ? () => null : onSelect}
        style={{ backgroundColor: bg }}
      >
        {!canRenameError &&
          msg && (
            <div className="alert alert-success" style={styleMsg}>
              {msg}
            </div>
          )}
        {canRenameError &&
          msg && (
            <div className="alert alert-danger" style={styleMsg}>
              {msg}
            </div>
          )}

        {!isEditing && usedTitle}
        {!isEditing &&
          !(!canRenameError && msg) && (
            <div style={{ textAlign: 'center' }}>
              <img
                src={fileImg}
                title={fileTitle}
                height="80"
                width="80"
                style={{ display: 'inline-block', verticalAlign: 'middle' }}
              />
              {/*<div style={{ display: 'inline-block', verticalAlign: 'middle' }}>
                {getContext().sub_estados.filter(s => s.id == sub_estado)
                  .length > 0 && (
                  <StatusBadge
                    size="25px"
                    estado={sub_estado}
                    list={getContext().sub_estados}
                  >
                    <div style={styleLetter}>{getEstadoLetter(estado)}</div>
                  </StatusBadge>
                )}

                <div style={{ width: '10px', height: '5px' }} />
                {estado == getContext().estados[1].id &&
                  vigenciaData.a && (
                    <Status
                      size="25px"
                      status={vigenciaData.a}
                      title={vigenciaTitle[vigenciaData.a]}
                    />
                  )}
                  </div>*/}
            </div>
          )}
        <div className="caption">
          {isEditing && (
            <InputField
              value={tempName}
              required
              onChange={this.updateTempName}
              disabled={fetching}
            />
          )}
          {isEditing && (
            <Button primary onClick={this.updateName} disabled={fetching}>
              save
            </Button>
          )}
          {isEditing && (
            <Button onClick={this.toggleEditing} disabled={fetching}>
              cancel
            </Button>
          )}

          {!isEditing && (
            <div>            
            <Center
              extraClass="text-ellipsis"
              title={name}
              style={{ maxHeight: '20px', overflow: 'hidden' }}
            >
              {name}
            </Center>
            <Center
              extraClass="text-ellipsis"
              title={name}
              style={{ maxHeight: '20px', overflow: 'hidden' }}
            >
              {fecha_reg}
            </Center>
            </div>            
          )}

          {removeButton}
          {downloadButton}
          {editButton}

          <Clearfix />
        </div>
      </div>
    );
  }
}
