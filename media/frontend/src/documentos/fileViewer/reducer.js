import { fromJS } from 'immutable'
import { createReducer } from 'redux-immutablejs'
import _ from 'lodash'

import { formatEstandarShortName } from 'common/formatter'
import constants from '../constants'
const {
  LOAD_FILES,
  TOGGLE_FILE_SELECTION,
  ADD_FILE,
  REMOVE_FILE,
  FILE_UPLOADED,
  FILE_UPLOADED_ERROR,
  REMOVE_UPLOADED_ERROR,
  REMOVING_FILE,
  UPDATE_PROP_FILE,
} = constants

const initialState = fromJS([])

const findIndexBy = (state, prop, value) =>
  state.findKey(file => file.get(prop) == value)

export default createReducer(initialState, {
  [LOAD_FILES]: (state, { files }) =>
    fromJS(files.map(f => ({ ...f, name: f.display }))),

  [ADD_FILE]: (state, { type, ...props }) =>
    state.push(
      fromJS({
        ...props,
        esta_ids: [],
        past_esta_ids: [],
        for_ids: [],
        past_for_ids: [],
        cond_ids: [],
        past_cond_ids: [],
      })
    ),

  [REMOVE_FILE]: (state, { id }) => state.filter(file => file.get('id') !== id),

  [FILE_UPLOADED]: (state, { name, dbId }) => {
    const idx = findIndexBy(state, 'name', name)
    return state.setIn([idx, 'loading'], false).setIn([idx, 'id'], dbId)
  },

  [FILE_UPLOADED_ERROR]: (state, { id, msg, typeError = '', name = '' }) => {
    const idx = findIndexBy(state, 'id', id)
    return state
      .setIn([idx, 'msg'], msg)
      .setIn([idx, 'typeError'], typeError)
      .setIn([idx, 'error'], true)
  },

  [TOGGLE_FILE_SELECTION]: (state, { id }) => {
    const idx = findIndexBy(state, 'id', id)
    return state.updateIn([idx, 'selected'], selected => !selected)
  },

  [REMOVING_FILE]: (state, { id, fetching }) => {
    const idx = findIndexBy(state, 'id', id)
    return state.setIn([idx, 'fetching'], fetching)
  },

  [REMOVE_UPLOADED_ERROR]: state => state.filter(file => !file.get('error')),

  [UPDATE_PROP_FILE]: (state, { id, prop, value }) => {
    const idx = findIndexBy(state, 'id', id)
    const nextProp = prop === 'name' ? 'display' : prop
    return state.setIn([idx, prop], value).setIn([idx, nextProp], value)
  },
})

export const getFiles = (files, filter = '', sortBy = '') => {
  const list =
    filter.length > 0
      ? files.filter(file => file.get('name').indexOf(filter) > -1)
      : files

  if (!sortBy.length) return list

  const multiplier = {
    'A-Z': -1,
    'Z-A': 1,
  }[sortBy]

  return sortBy.length > 0
    ? list.sort(
        (a, b) =>
          a.get('name') > b.get('name') ? -1 * multiplier : 1 * multiplier
      )
    : list
}

export const getFile = (files, id) => files.find(file => file.get('id') === id)

export const getSelectedFiles = files => files.filter(f => f.get('selected'))

// export const groupFilesByEstandar = (files, estandares) => {
//   const grouped = {};
//   const noEstandar = [];
//   files.forEach(f => {
//     if (!f.get('esta_ids').size) {
//       return noEstandar.push(f.get('id'));
//     }
//     f.get('esta_ids').map(id => {
//       if (!grouped[id]) {
//         grouped[id] = [];
//       }
//       grouped[id].push(f.get('id'))
//     });
//   });
//   console.log(grouped);

//   let folders = estandares
//     .map(e =>
//       ({
//         id: e.esta_id || e.id,
//         display: 'Estándar ' + formatEstandarShortName(e),
//         file_ids: grouped[+(e.esta_id || e.id)] || []
//       })
//     );
//   folders = [{id: 'varios', display: 'Varios', file_ids: noEstandar}].concat(folders);
//   console.log(JSON.stringify(folders));
//   return folders;
// }
