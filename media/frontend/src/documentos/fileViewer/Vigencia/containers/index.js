import React, { Component } from 'react'
import { connect } from 'react-redux'

import { getFiles } from '../../reducer'
import { updatePropFile } from '../../actions'
import format from 'date-fns/format'
import { Table, TableHeader } from 'common/components/table'
import VigenciaRow from '../components/VigenciaRow'
import { subHeaders } from 'licenciamiento/formatos/containers'

const headers = [
  {
    display: 'Documentos',
    customStyle: { rowSpan: 2 },
  },
  { display: 'Estado', customStyle: { rowSpan: 2 } },
  { display: 'Vigencia', customStyle: { colSpan: 3 } },
  {
    display: `Estado act. al: ${format(new Date(), 'DD/MM/YYYY')}`,
    customStyle: { colSpan: 2 },
  },
]

class Vigencia extends Component {
  render() {
    const { files, updatePropFile } = this.props

    return (
      <div className="col-md-12">
        <Table bordered>
          <thead>
            <TableHeader.Row headers={headers} />
            <TableHeader.Row headers={subHeaders} />
          </thead>
          <tbody>
            {files.map(file => (
              <VigenciaRow
                key={file.get('id')}
                file={file}
                onChange={updatePropFile}
              />
            ))}
          </tbody>
        </Table>
      </div>
    )
  }
}

const mapState = state => ({
  files: getFiles(
    state.get('files'),
    undefined,
    state.getIn(['toolbar', 'sortBy'])
  ),
})

export default connect(mapState, { updatePropFile })(Vigencia)
