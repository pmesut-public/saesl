import React, { Component } from 'react'

import Status from 'common/components/Status'
import { InputField } from 'common/components/Form'
import RangeDatePicker from 'common/components/Form/RangeDatePicker'
import { getContext } from 'common/context'
import StatusBadge from 'common/components/StatusBadge'
import { getVigenciaValues } from 'licenciamiento/formatos/containers/Formato'

class VigenciaRow extends Component {
  onRangeChange = (fechaInicio, fechaFin) => {
    const { file, onChange } = this.props
    onChange(file.get('id'), 'fecha_inicio', fechaInicio)
    onChange(file.get('id'), 'fecha_fin', fechaFin)
  }
  render() {
    const { file, onChange } = this.props

    const { id, display, sub_estado, fecha_inicio, fecha_fin, da } = file

    const rangeSelected = fecha_inicio && fecha_fin
    const vigencia = getVigenciaValues(file)

    return (
      <tr>
        <td>{display}</td>
        <td>
          <StatusBadge
            estado={sub_estado}
            list={getContext().sub_estados}
            style={{ margin: '0 auto' }}
          />
        </td>
        <td>
          <RangeDatePicker
            onChange={this.onRangeChange}
            startTime={fecha_inicio}
            endTime={fecha_fin}
          />
        </td>
        <td style={{ minWidth: '100px' }}>{vigencia.dv}</td>
        <td style={{ minWidth: '100px' }}>
          <InputField
            value={da || ''}
            onChange={val => onChange(id, 'da', val)}
            pattern={'[0-9]+$'}
            disabled={!rangeSelected}
            required
          />
        </td>
        <td>{vigencia.ed}</td>
        <td>
          <Status status={vigencia.a} style={{ margin: '0 auto' }} />
        </td>
      </tr>
    )
  }
}

export default VigenciaRow
