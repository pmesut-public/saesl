import keyMirror from 'keymirror';

const constants = keyMirror({
  TEST: null,

  UPDATE_PROP: null,
  TOGGLE_FILE_SELECTION: null,
  LOAD_FILES: null,
  ADD_FILE: null,
  REMOVE_FILE: null,
  FILE_UPLOADED: null,
  FILE_UPLOADED_ERROR: null,
  REMOVE_UPLOADED_ERROR: null,
  REMOVING_FILE: null,
  UPDATE_PROP_FILE: null,
  DENIED: null,
});

export default constants;
