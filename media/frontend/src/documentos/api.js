import request from 'common/request'

const urls = {
  delete: '/documentos/delete',
  createZip: '/documentos/create_zip',
  createZipTree: '/documentos/create_tree_zip',
  // rename: '/autoevaluacion/rename',
  rename: '/documentos/rename',
  getList: '/documentos/get',
  // getList: '/autoevaluacion/list',
}

export const removeFile = props => request({ ...props, url: urls.delete })

export const renameFile = props => request({ ...props, url: urls.rename })

export const getFiles = props => request({ ...props, url: urls.getList })

export const createZip = props => request({ ...props, url: urls.createZip })

export const createZipTree = props =>
  request({ ...props, url: urls.createZipTree })
