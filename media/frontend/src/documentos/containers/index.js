import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Panel, Grid } from 'react-bootstrap'

import { connectOnLeavePage } from 'common/hoc'
import { messageLeavePage } from '../messages'
import { isFetching } from 'status/reducer'
import ToolBar from '../toolbar/containers'
import FileViewer from '../fileViewer/containers'

const App = () => (
  <div className="box">
    <div className="box-header">
      <ToolBar />
    </div>
    <div className="box-body box-responsive">
      <FileViewer />
    </div>
  </div>
)

const mapStateToProps = state => ({
  isFetching: isFetching(state.get('status')),
})

export default connect(mapStateToProps)(
  connectOnLeavePage(App, messageLeavePage)
)
