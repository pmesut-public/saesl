import constants from '../constants';
const {
  UPDATE_PROP,
} = constants;

const updateProp = (prop, value) => ({
  type: UPDATE_PROP,
  prop,
  value,
});

export const filterByName = (name) => updateProp('filterName', name);

export const sortBy = (sort) => updateProp('sortBy', sort);

export const changeView = (view) => updateProp('currentView', view);
