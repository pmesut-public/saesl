import React from 'react';
import { connect } from 'react-redux';

const AddFileButton = ({
  setRef,
  disabled,
  currentFetching,
}) => (
  <button
    type="button"
    ref={setRef}
    disabled={currentFetching}
    className="btn btn-primary btn-flat col-md-2 col-md-offset-4" >
    {'Subir archivo ' }
    {
      currentFetching ?
        <i className='fa fa-refresh fa-animate' />:
        <i className='fa fa-upload' />
    }
  </button>
);

export default AddFileButton;
