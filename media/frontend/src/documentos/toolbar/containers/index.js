import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { v4 } from 'node-uuid';
import classNames from 'classnames';
import { Modal } from 'react-bootstrap';
import ModalReact from 'react-modal';

import FileStatusPicker from 'common/components/FileStatusPicker';
import AddFileButton from '../components/AddFileButton';
import { InputField, SelectField } from 'common/components/Form';
import Button from 'common/components/buttons';
import { filterByName, sortBy, changeView } from '../actions';
import {
  fileUploaded,
  uploadFile,
  removeFile,
  onFileUploadedError,
  onUploadErrorExistingName,
  toggleFileSelection,
} from '../../fileViewer/actions';
import { getFiles } from '../../fileViewer/reducer';
import { replaceFiles } from 'autoevaluacion/tablaAuto/estandares/documentos/actions';
import { getId, getCurrentView } from '../reducer';
import { isCurrentActionFetching, isFetching } from 'status/reducer';
import { getContext } from 'common/context';

const options = [{ id: 'A-Z', name: 'A-Z' }, { id: 'Z-A', name: 'Z-A' }];

const views = [
  { id: 0, label: 'General' },
  { id: 1, label: 'Agrupado' },
  { id: 2, label: 'Árbol' },
];

class ToolBar extends Component {
  state = { isOpenModal: false };

  componentDidMount() {
    this.activatePlugin();
  }

  openModal = () => {
    this.setState({
      isOpenModal: true,
    });
  };
  closeModal = () => this.setState({ isOpenModal: false });

  renderModal = () => {
    const { isOpenModal } = this.state;
    const { showVigencia } = this.props;

    const modalStyles = {
      overlay: { zIndex: 10 },
      content: {
        maxWidth: showVigencia ? '650px' : '400px',
        minHeight: '300px',
        left: '50%',
        transform: 'translateX(-50%)',
      },
    };

    const found = this.props.files
      .filter(f => !f.get('formato'))
      .find(
        f => f.get('display') == this.fileName || f.get('name') == this.fileName
      );
    const replaceId = found ? found.get('id') : '';

    return (
      <ModalReact style={modalStyles} isOpen={isOpenModal} contentLabel="Modal">
        <Modal.Header>
          <Modal.Title>Seleccionar estado del documento</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {isOpenModal && (
            <FileStatusPicker
              replaceId={replaceId}
              showVigencia={showVigencia}
              estados={getContext().estados}
              subEstados={getContext().sub_estados}
              onCancel={this.closeModal}
              onSubmit={estados => {
                console.log('on submit', estados);
                this.estados = estados;
                this.uploadFile(_.mapKeys(estados, (v, k) => _.snakeCase(k)));
                this.closeModal();
              }}
            />
          )}
        </Modal.Body>
      </ModalReact>
    );
  };

  uploadFile = (extraParams = {}) => {
    const { uploadFile, actionId } = this.props;
    const uuid = v4();
    uploadFile(actionId, this.fileName, uuid);
    this.uploader.setOption('multipart_params', { uuid, ...extraParams });
    this.uploader.start();
  };

  onFilesAdded = (uploader, files, c) => {
    if (uploader.files.length > 1) {
      for (var i = 0; i < uploader.files.length - 1; i++) {
        uploader.removeFile(uploader.files[i].id);
      }
    }
    this.fileName = files[0].name;
    this.estados = {
      da:"",
      estado: "1",
      fecha_fin: "",
      fecha_inicio: "",
      replaceFile: "",
      replaceId: "",
      subEstado: "5",
    };
    this.uploadFile(_.mapKeys(this.estados, (v, k) => _.snakeCase(k)));
    //this.openModal();    
  };

  onFileUploaded = (fileUploader, _, res) => {
    let {
      id,
      display: name,
      path,
      msg,
      status,
      fecha_inicio,
      fecha_fin,
      da,
      reemplazos,
    } = JSON.parse(res.response);
    const uuid = fileUploader.settings.multipart_params.uuid;
    if (status !== 'OK') {
      return this.props.onUploadErrorExistingName({
        id,
        name,
        path,
        msg,
        status,
        uuid,
        display: name,
        estado: this.estados.estado,
        sub_estado: this.estados.subEstado,
        fecha_inicio,
        fecha_fin,
        da,
      });
    }
    if (this.props.currentView !== 0) {
      alert(
        'Archivo subido exitosamente. El archivo se puede visualizar en al carpeta Varios'
      );
    }
    this.props.fileUploaded({
      uuid,
      id,
      name,
      path,
      display: name,
      estado: this.estados.estado,
      sub_estado: this.estados.subEstado,
      fecha_inicio,
      fecha_fin,
      da,
    });
    console.log('this', this.estados);
    if (this.estados.replaceId) {
      this.props.replaceFiles(this.estados.replaceId, id, reemplazos);
      setTimeout(() => {
        this.props.toggleFileSelection(id);
      }, 0);
      this.props.removeFile(this.estados.replaceId);
    }
  };

  // TODO
  onError = (a, b, c) => {
    console.log(a, b, c);
    if (b.status == '413') {
      return alert('El archivo es demasiado grande.');
    }
    if (!a.settings.multipart_params) return;
    const uuid = a.settings.multipart_params.uuid;

    if (this.props.currentView !== 0) {
      alert('Ocurrio un error al subir el archivo');
    }
    //this.props.onFileUploadedError(uuid, 'Error al subir el archivo ')
    //this.onFileUploaded(
    //{
    //settings: {
    //multipart_params: {
    //uuid: '1234',
    //},
    //},
    //},
    //null,
    //{
    //response: JSON.stringify({
    //uuid: '1234',
    //id: Date.now() + 'GG',
    //display: b.file.name,
    //path: 'asdf/asdf.pdf',
    //status: 'OK',
    //}),
    //}
    //)
    // console.log('error', a, b, c);
    // this.props.onFileUploadedError(b.file.name);
  };

  setRef = e => (this.button = e);

  activatePlugin() {
    this.settings = {
      url: '/documentos/create',
      browse_button: this.button,
      multi_selection: false,
    };

    try {
      this.uploader = new plupload.Uploader(this.settings);
      this.uploader.bind('FilesAdded', this.onFilesAdded);
      this.uploader.bind('FileUploaded', this.onFileUploaded);
      this.uploader.bind('Error', this.onError);
      this.uploader.init();
    } catch (e) {
      console.log(e);
    }
  }

  removePlugin() {
    this.uploader.unbind('FilesAdded', this.onFilesAdded);
    this.uploader.unbind('FileUploaded', this.onFileUploaded);
    this.uploader.unbind('Error', this.onError);
    this.uploader.destroy();
    this.uploader = null;
  }

  sortBy = e => this.props.sortBy(e.target.value);

  _onSubmit = e => e.preventDefault();

  render() {
    const {
      toolbar,
      filterByName,
      isFetching,
      isCurrentFetching,
      currentView,
      changeView,
      anyIsFetching,
    } = this.props;

    return (
      <form onSubmit={this._onSubmit} className="panel-heading form-inline">
        {this.renderModal()}
        <Row>
          <InputField
            classWrapper="form-group col-md-3"
            instantUpdate
            value={toolbar.get('filterName')}
            onChange={filterByName}
            placeholder="Buscar por nombre"
          />
          <SelectField
            label="Ordenar "
            classWrapper="form-group col-md-3"
            value={toolbar.get('sortBy')}
            onChange={this.sortBy}
            options={options}
          />
          {getContext().editable && (
            <AddFileButton
              setRef={this.setRef}
              disabled={isFetching}
              currentFetching={isCurrentFetching}
            />
          )}
        </Row>
        <Row>
          <div
            className="form-group col-md-12 view-buttons"
            style={{ marginTop: '10px' }}
          >
            <div className="btn-group">
              {views.map(v => (
                <Button
                  primary
                  key={v.id}
                  disabled={anyIsFetching}
                  style={{}}
                  extraClass={classNames({ active: currentView === v.id })}
                  onClick={() => changeView(v.id)}
                >
                  {v.label}
                </Button>
              ))}
            </div>
          </div>
        </Row>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  files: getFiles(state.get('files')),
  toolbar: state.get('toolbar'),
  isCurrentFetching: isCurrentActionFetching(
    state.get('status'),
    getId(state.get('toolbar'))
  ),
  actionId: getId(state.get('toolbar')),
  currentView: getCurrentView(state.get('toolbar')),
  anyIsFetching: isFetching(state.get('status')),
});

const mapDispatchToProps = {
  filterByName,
  sortBy,
  uploadFile,
  fileUploaded,
  onFileUploadedError,
  onUploadErrorExistingName,
  changeView,
  removeFile,
  replaceFiles,
  toggleFileSelection,
};

export default connect(mapStateToProps, mapDispatchToProps)(ToolBar);
