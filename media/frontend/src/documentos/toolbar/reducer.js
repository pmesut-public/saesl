import { fromJS } from 'immutable'
import { createReducer } from 'redux-immutablejs'

import constants from '../constants'
const { UPDATE_PROP } = constants

const initialState = fromJS({
  filterName: '',
  sortBy: '',
  ADD_ID: 'ADD_ID',
  selectable: false,
  currentView: 0,
})

export default createReducer(initialState, {
  [UPDATE_PROP]: (state, { prop, value }) => state.set(prop, value),
})

export const getId = state => state.get('ADD_ID')
export const isSelectable = state => state.get('selectable')
export const getCurrentView = state => state.get('currentView')
