import createStore from 'common/createStore';
import * as apiDocumentos from './api';
import rootReducer from './reducer';

export default initialState => createStore({initialState, api: {apiDocumentos}, rootReducer});
