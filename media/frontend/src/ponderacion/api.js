import request from 'common/request';

const urls = {
  save: '/ponderaciones/save',
  close: '/ponderaciones/close',
  newAutoevaluacion: '/autoevaluaciones/new',
};

const createRequest = (url) => (props) => request({...props, url});

export const savePonderacion = createRequest(urls.save);

export const closePonderacion = createRequest(urls.close);

export const newAutoevaluacion = createRequest(urls.newAutoevaluacion);

