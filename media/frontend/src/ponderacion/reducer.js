import { fromJS } from 'immutable';
import { createReducer, combineReducers } from 'redux-immutablejs';

import status from 'status/reducer';
import settings from './settings/reducer';
import matriz from './matriz/reducer';

export default combineReducers({status, settings, matriz});
