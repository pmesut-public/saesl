import keyMirror from 'keymirror';

const constants = keyMirror({
  NAVIGATE: null,
  UPDATE_EVENT: null,
  UPDATE_TIPO_AUTOEVALUACION: null,

  UPDATE_PESO_FACTOR: null,
  UPDATE_PESO_ESTANDAR: null,
  CREATE_BASE_STRUCTURE: null,
  LOAD_PAST_PONDERACION: null,
  LOAD_PAST_EVALUACION: null,

  SET_ESTANDARES_PESO: null,
  UPDATE_MODIFY_MATRIX: null,

  CLEAR_HAS_CHANGED: null,
});

export default constants;
