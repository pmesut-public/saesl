import React from 'react';
import map from 'lodash/map';
import flatMap from 'lodash/flatMap';
import groupBy from 'lodash/groupBy';
import { connect } from 'react-redux';
import classNames from 'classnames';
import _ from 'lodash';

import {
  getPesoFactor,
  getEstandaresPesoByFactor,
} from '../reducer';
import RowMatriz from './RowMatriz';
import { getContext } from 'common/context';

const round2Decimals = num => _.floor(+num * 100) / 100;
const MAX = 100;
const danger = (value) => classNames({'bg-danger text-danger': round2Decimals(value) !== round2Decimals(MAX)});

const TableBodyMatriz = ({factorTotal, pesoFactor}) => {
  const estandares = groupBy(getContext().estandares, e => e.fact_id);
  const factores = groupBy(
    getContext().factores.map(f => ({...f, size: estandares[f.id].length + 1})),
    f => f.dime_id
  );
  const dimensiones = getContext().dimensiones
    .map(d => ({
      ...d,
      size: factores[d.id].reduce((acc, f) => acc + f.size, 0)
    }));

  return (
      <tbody>
      {
        flatMap(dimensiones, d =>
          flatMap(factores[d.id], (f, i) =>
            [...map(estandares[f.id], (e, j) => (
              <RowMatriz
                key={e.id}
                shouldRenderDimension= {i === 0 && j === 0}
                dimension={d}
                shouldRenderFactor={j === 0}
                factor={f}
                estandar={e}
              />
            )),
             <tr key={f.id + 'resultado'}style={{fontWeight: 'bold'}} className={danger(factorTotal[f.id])}>
               <td/>
               <td>{round2Decimals(factorTotal[f.id])}%</td>
             <td>{round2Decimals(factorTotal[f.id] * pesoFactor[f.id]) / 100}</td>
             </tr>
            ]
          )
        )
      }
    </tbody>
  );
};

const mapStateToProps = (state) => {
  const factorTotal = getContext().factores
          .reduce((acc, f) => ({...acc, [f.id]: round2Decimals(getEstandaresPesoByFactor(state.get('matriz'), f.id))}), {});

  const pesoFactor = getContext().factores
    .reduce(
      (acc, f) => ({...acc, [f.id]: getPesoFactor(state.get('matriz'), f.id)}),
      {}
    );

  return {
    pesoFactor,
    factorTotal: factorTotal,
  }
}

export default connect(mapStateToProps)(TableBodyMatriz);
