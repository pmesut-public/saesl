import React from 'react';
import map from 'lodash/map';
import { connect } from 'react-redux';
import { Alert } from 'react-bootstrap';

import Button from 'common/components/buttons';
import NavigationBar from'../../components/NavigationBar';
import {connectOnLeavePage} from 'common/hoc';
import { isFetching } from 'status/reducer';
import * as messages from '../../messages';
import {
  savePonderacion,
  closePonderacion,
  onLeaveMatriz,
} from '../actions';
import { canEditMatriz } from '../../settings/reducer';
import {
  canModify,
  getFactoresTotal,
  getEstandaresTotal,
} from '../reducer';
import { Table } from 'common/components/table';

import { getContext } from 'common/context';
import TableBodyMatriz from './TableBodyMatriz';
import TableFooterMatriz from './TableFooterMatriz';
import CustomizePonderacion from './CustomizePonderacion';

const Percentage = 'Porcentajes';

const headers = [
  {display: 'Dimensiones'},
  {display: Percentage},
  {display: 'Factores'},
  {display: '% sobre el factor'},
  {display: 'Estandares'},
  {display: '% sobre la matriz de calidad'},
  {display: 'Peso', width: '4%'},
];

const TableHeader = ({
  headers,
  canEdit
}) => {
  let filteredHeaders = headers;
  if (!canEdit) {
    filteredHeaders = filteredHeaders.filter(f => f.display !==Percentage);
  }
  filteredHeaders = filteredHeaders.map(f => {
    let width = '33%';
    if (canEdit) {
      if (f.width) {
        width = f.width;
      } else {
        width = f.display === Percentage || f.display.indexOf('%') > -1 ? '6%': '26%';
      }
    }
    return {...f, width};
  });
  return (
    <thead>
      <tr>
      {
        map(filteredHeaders , ({display, customClass, width}, i) => (
            <th key={i} style={{width}}>{display}</th>
        ))
      }
      </tr>
    </thead>
  );
};

const App = ({
  onBack,
  canEdit,
  onNextView,
  disabled,
  isFetching,
  editInfo,
  showPeso,
  savePonderacion,
  canModify,
}) => {
  let extraButton = () => {};
  if (canModify === '1') {
    extraButton = () => (<Button disabled={disabled} primary onClick={savePonderacion}>Guardar ponderacion</Button>);
  }
  const navigation = (
    <NavigationBar
      backButton={{
        onClick: onBack,
        disabled,
      }}
      extraButton={extraButton}
      nextButton={{
        onClick: onNextView,
        disabled: isFetching,
        title: 'Guardar y cerrar'
      }}
    />
  );

  return (
    <div>
      {navigation}
      {editInfo && (<Alert bsStyle="info">{editInfo}</Alert>)}
      <Table bordered>
        <TableHeader headers={headers} canEdit={canEdit}/>
        {
          canEdit && <TableFooterMatriz />
        }
        <TableBodyMatriz />
      </Table>
      {editInfo && (<Alert bsStyle="info">{editInfo}</Alert>)}
    {
      canEdit && <CustomizePonderacion />
    }
    {navigation}
    </div>
  )
};

const mapStateToProps = (state) => ({
  canEdit: canEditMatriz(state.get('settings'), getContext().eventos),
  isFetching: isFetching(state.get('status')) || (
    state.getIn(['matriz', 'hasChanged']) && (getFactoresTotal(state.get('matriz')) !== 100 || getEstandaresTotal(state.get('matriz')) !== 100 * getContext().factores.length )) ,
  disabled: isFetching(state.get('status')),
  editInfo: canEditMatriz(state.get('settings'), getContext().eventos) && +canModify(state.get('matriz'))? messages.EDIT_MATRIZ: '',
  canModify: canModify(state.get('matriz')),
});

const mapDispatchToProps = (dispatch, { onNext, onBack }) => ({
  onBack: () => dispatch(onLeaveMatriz(onBack)),
  onNextView: () => {
    console.log('next');
    dispatch(closePonderacion(onNext));
  },
  savePonderacion: () => dispatch(savePonderacion()),
});

export default connect(mapStateToProps, mapDispatchToProps)(connectOnLeavePage(App, messages.UNSAVED_CHANGED_MATRIZ));
