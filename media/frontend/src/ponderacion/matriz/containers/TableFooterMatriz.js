import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import _ from 'lodash';

import { getFactoresTotal, getEstandaresTotal } from '../reducer';


const round2Decimals = num => _.floor(+num * 100) / 100;
const MAX = 100;

const danger = (value) => classNames({'bg-danger text-danger': round2Decimals(value) !== round2Decimals(MAX)});

const TableFooterMatriz = ({
  factoresTotal,
  estandaresTotal,
}) => (
  <tfoot>
    <tr>
    <td/>
    <td className={danger(factoresTotal)}>{round2Decimals(factoresTotal)}%</td>
    <td/>
    <td className={danger(factoresTotal)}>{round2Decimals(factoresTotal)}%</td>
    <td colSpan={3}></td>
    </tr>
  </tfoot>
);

const mapStateToProps = (state) => ({
  factoresTotal: getFactoresTotal(state.get('matriz')),
  estandaresTotal: getEstandaresTotal(state.get('matriz')),
});

export default connect(mapStateToProps)(TableFooterMatriz);
