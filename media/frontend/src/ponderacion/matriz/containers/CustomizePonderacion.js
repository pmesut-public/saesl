import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row } from 'react-bootstrap';

import Button from 'common/components/buttons';
import { isFetching } from 'status/reducer';
import { RadioField, SelectField } from 'common/components/Form';
import { getContext } from 'common/context';
import {
  setPonderacionEstandar,
  clearPonderacion,
  toggleMakeChanges,
  savePonderacion,
  choosePastAutoId,
} from '../actions';
import { canModify, getComboPastAutoId } from '../reducer';

const options = [{
  id: '1',
  text: 'SI'
}, {
  id: '0',
  text: 'NO'
}];

const CustomizePonderacion = ({
  setPonderacionEstandar,
  clearPonderacion,
  canModify,
  updateMatriz,
  savePonderacion,
  comboPastAutoId,
  choosePastAutoId,
  disabled,
}) => (
  <div >
    <div >
    <Row>
      <RadioField
        label="Hacer cambios?"
        options={options}
        selected={canModify}
        onChange={updateMatriz}
        disabled={disabled}
      />
    </Row>
    <hr/>
    {
      canModify === '1' &&
      (
        <div className="row">
          <label className="col-lg-1 col-md-2 text-right">Cargar ponderación anterior</label>
          <SelectField
            classWrapper="col-md-3"
            value={comboPastAutoId}
            onChange={choosePastAutoId}
            options={getContext().autoevaluaciones}
            disabled={disabled}
          />

          <Button disabled={disabled} onClick={clearPonderacion} extraClass="col-md-1">Limpiar</Button>
          {' '}
          <Button disabled={disabled} onClick={setPonderacionEstandar} extraClass="col-md-1">Estandar</Button>
        </div>
      )
    }
</div>
  </div>
);

const mapStateToProps = (state) => ({
  canModify: canModify(state.get('matriz')),
  comboPastAutoId: getComboPastAutoId(state.get('matriz')),
  disabled: isFetching(state.get('status')),
});

const mapDispatchToProps = (dispatch) => ({
  setPonderacionEstandar: () => dispatch(setPonderacionEstandar(
    getContext().estandares,
    getContext().factores,
  )),
  clearPonderacion: () => dispatch(clearPonderacion(
    getContext().estandares,
    getContext().factores,
  )),
  updateMatriz: e => dispatch(toggleMakeChanges(e.target.value, getContext())),
  savePonderacion: () => dispatch(savePonderacion()),
  choosePastAutoId: ({target}) => dispatch(choosePastAutoId(target.value, getContext().autoevaluaciones, getContext().factores))
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomizePonderacion);
