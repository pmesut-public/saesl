import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';

import { isFetching } from 'status/reducer';
import {
  getPesoFactor,
  getPesoEstandar,
  getPesoDimension,
  isEditable,
  isPesoFactorCorrect,
} from '../reducer';
import { updatePesoFactor, updatePesoEstandar } from '../actions';
import TdMatriz from '../components/TdMatriz';
import { getContext } from 'common/context';
import { canEditMatriz } from '../../settings/reducer';
import { toNumber } from 'common/formatter';

const round2Decimals = num => _.floor(+num * 100) / 100;

class RowMatriz extends Component {
  render() {
    const {
      editable,
      shouldRenderDimension,
      dimension,
      shouldRenderFactor,
      factor,
      estandar,
      pesoDimension,
      pesoFactor,
      pesoEstandar,
      updatePesoFactor,
      updatePesoEstandar,
      isPesoFactorCorrect,
      disabled,
      showPeso,
    } = this.props;

    return (
      <tr>
        {
          shouldRenderDimension &&
            <td rowSpan={dimension.size}>{dimension.display}</td>
        }
        {
          showPeso && shouldRenderDimension &&
            <td rowSpan={dimension.size}>{pesoDimension}</td>
        }
        {
          shouldRenderFactor &&
            <td rowSpan={factor.size}>{factor.display}</td>
        }
        {
          showPeso && shouldRenderFactor &&
            <TdMatriz
              disabled={disabled}
              editable={editable}
              value={pesoFactor}
              onChange={updatePesoFactor}
              rowSpan={factor.size}
            />
        }
        <td>{estandar.display}</td>
        {
          showPeso && (
            <TdMatriz
              disabled={pesoFactor=='0' || pesoFactor == '' || disabled}
              editable={editable}
              value={pesoEstandar}
              onChange={updatePesoEstandar}
            />
          )
        }
        <td>{round2Decimals(toNumber(pesoEstandar)*toNumber(pesoFactor))/100}%</td>
      </tr>
    );
  }
};

const mapStateToProps = (
  state, {
    factor,
    estandar,
    dimension,
    shouldRenderDimension
  }
) => ({
  pesoFactor: getPesoFactor(state.get('matriz'), factor.id),
  pesoEstandar: getPesoEstandar(state.get('matriz'), estandar.id),
  pesoDimension: getPesoDimension(state.get('matriz'), dimension.id),
  isPesoFactorCorrect: isPesoFactorCorrect(state.get('matriz'), estandar.id),
  editable: canEditMatriz(state.get('settings'), getContext().eventos) &&
    isEditable(state.get('matriz')),
  disabled: isFetching(state.get('status')),
  showPeso: canEditMatriz(state.get('settings'), getContext().eventos),
});

const mapDispatchToProps = (dispatch, { factor, estandar }) => ({
  updatePesoFactor: (value) => dispatch(updatePesoFactor(factor.id, value)),
  updatePesoEstandar: (value) => dispatch(updatePesoEstandar(estandar.id, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RowMatriz);
