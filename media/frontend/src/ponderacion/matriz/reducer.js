import { fromJS } from 'immutable';
import { createReducer } from 'redux-immutablejs';
import groupBy from 'lodash/groupBy';
import times from 'lodash/times';
import constant from 'lodash/constant';
import sumBy from 'lodash/sumBy';

import { toNumber } from 'common/formatter';
import constants from '../constants';
const {
  UPDATE_PESO_FACTOR,
  UPDATE_PESO_ESTANDAR,

  CREATE_BASE_STRUCTURE,
  SET_ESTANDARES_PESO,
  UPDATE_MODIFY_MATRIX,
  LOAD_PAST_PONDERACION,
  LOAD_PAST_EVALUACION,
  CLEAR_HAS_CHANGED,
} = constants;

const initialState = fromJS({
  pesos: {},
  hasChanged: false,
  willCustomizeMatrix: '0',
  comboPastAutoId: '',
});

const createWithPeso = (peso) =>
  (acc, {id, display, ...rest}) => {
    acc[id] = {...rest, peso};
    return acc;
  };

const createWeightedArray = (totalWeight, len) => {
  const itemWeight = parseInt(totalWeight / len);
  return [
    ...times(len - 1, constant(itemWeight)),
    itemWeight + (totalWeight % len)
  ];
};

const getPathFactor = (id) => ['pesos', 'factores',  '' + id];
const getPathEstandar = (id) => ['pesos', 'estandares',  '' + id];
const getEstandaresFromFactor = (state, id) =>
  state.getIn([...getPathFactor(id), 'estandares']);

const sumarEstandaresFromFactor = (state, factId) => {
  const estandares = getEstandaresFromFactor(state, factId);
  return estandares.toJS().reduce(
    (acc, e) => acc + toNumber(state.getIn([...getPathEstandar(e), 'peso'])),
    0
  );
};

export default createReducer(initialState, {
  [UPDATE_MODIFY_MATRIX]: (state, {value}) => state.set('willCustomizeMatrix', value),

  [UPDATE_PESO_FACTOR]: (state, {fact_id, value}) =>
    state.setIn([...getPathFactor(fact_id), 'peso'], value)
      .set('hasChanged', true),

  [UPDATE_PESO_ESTANDAR]: (state, {esta_id, value}) =>
    state.setIn([...getPathEstandar(esta_id), 'peso'], value)
      .set('hasChanged', true),

  [CLEAR_HAS_CHANGED]: (state) => state.set('hasChanged', false),

  [CREATE_BASE_STRUCTURE]: (state, {factores, estandares}) => {
    const groupedEstandares = groupBy(estandares, e => e.fact_id);
    const factoresWithWeight = factores
      .map(f => ({...f, estandares: groupedEstandares[f.id].map(e => e.id)}))
      .reduce(createWithPeso(''), {});

    const estandaresWithWeight = estandares.reduce(createWithPeso(''), {});

    return state.set('pesos', fromJS({
      factores: factoresWithWeight,
      estandares: estandaresWithWeight,
    }));
  },

  [LOAD_PAST_EVALUACION]: (state, {autoId, estandares, factores}) =>
    state.withMutations(state => {
      state.set('comboPastAutoId', autoId);
      estandares
        .forEach(e => state.setIn([...getPathEstandar(e.esta_id), 'peso'], toNumber(e.ponderacion)));

      factores.forEach(f => {
        // const sumFactor = sumBy(estandares.filter(e => e.fact_id == f.id), e => toNumber(e.ponderacion));
        state.setIn([...getPathFactor(f.id), 'peso'], toNumber(f.ponderacion));
      });
      state.set('hasChanged', true);
    }),

  [LOAD_PAST_PONDERACION]: (state, {pastPonderacion}) =>
    state.withMutations(state => {
      pastPonderacion.estandares
        .forEach(e => state.setIn([...getPathEstandar(e.esta_id), 'peso'], e.ponderacion));

      pastPonderacion.factores
        .forEach(f => {
          state.setIn([...getPathFactor(f.fact_id), 'peso'], f.ponderacion);
        });
    }),

  [SET_ESTANDARES_PESO]: (state, { estandarPeso, estandares, factores, customEstandarPeso, customFactorPeso}) =>
    state.withMutations(state => {
      if (customEstandarPeso) {
        const estandaresGrouped = groupBy(estandares, e => e.fact_id);
        Object.keys(estandaresGrouped).forEach(k => {
          estandaresGrouped[k].forEach((e, i) => {
            state.setIn(
              [...getPathEstandar(e.id), 'peso'],
              estandaresGrouped[k].length == i + 1 ?
                round2Decimals(100 - customEstandarPeso[e.id] * i):
                customEstandarPeso[e.id]
            )
          })
        })
          // estandares
          //   .forEach((e, i) => {
          //     if (estandares[i + 1] && estandares[i + 1].fact_id !== estandares[i + 1].fact_id)
          //           };

        // })
        factores
          .forEach((f, i) => {
            state.setIn(
              [...getPathFactor(f.id), 'peso'],
              i + 1 == factores.length ? round2Decimals(100 - customFactorPeso * i):customFactorPeso
            )
          });
      } else {
        estandares
          .forEach(e => state.setIn([...getPathEstandar(e.id), 'peso'], estandarPeso));
      }

      state.set('hasChanged', true);
    }),
});

const round2Decimals = num => _.floor(num * 100) / 100;

const sumPeso = (acc, item) => acc + toNumber(item.get('peso'));

export const getPesoFactor = (state, id) =>
  state.getIn([...getPathFactor(id), 'peso']);

export const getPesoEstandar = (state, id) =>
  state.getIn([...getPathEstandar(id), 'peso']);

const getTotalWeight = (state, selector) =>
  state.getIn(['pesos', selector])
    .reduce(sumPeso, 0);

export const getFactoresTotal = (state) => getTotalWeight(state, 'factores');

export const getEstandaresTotal = (state) => getTotalWeight(state, 'estandares');

export const getEstandaresPesoByFactor = (state, factId) =>
  state.getIn(['pesos', 'estandares']).valueSeq()
    .filter(f => f.get('fact_id') === factId)
    .reduce(sumPeso, 0);

export const getPesoDimension = (state, dimeId) =>
  round2Decimals(state.getIn(['pesos', 'factores']).valueSeq()
    .filter(f => f.get('dime_id') === dimeId)
                 .reduce(sumPeso, 0));

export const isPesoFactorCorrect = (state, estId) => {
  const factId = state.getIn([...getPathEstandar(estId), 'fact_id']);
  return getEstandaresPesoByFactor(state, factId) != getPesoFactor(state, factId);
};

export const canModify = (state) => state.get('willCustomizeMatrix');

export const isEditable = (state) => canModify(state) !== '0';

export const getComboPastAutoId = (state) => state.get('comboPastAutoId');

export const extractPonderacion = (state, factores, estandares) => ({
  factores: factores.map(f => ({
    fact_id: f.id,
    ponderacion: toNumber(getPesoFactor(state, f.id)),
  })),
  estandares: estandares.map(e => ({
    esta_id: e.id,
    fact_id: e.fact_id,
    ponderacion: toNumber(getPesoEstandar(state, e.id))
  })),
});

export const extractSettingsPonderacion = (state) => ({
  willCustomizeMatrix: state.get('willCustomizeMatrix'),
  comboPastAutoId: state.get('comboPastAutoId'),
});

export const setSettingsPonderacion = (state, settings) =>
  state
    .set('willCustomizeMatrix', settings.willCustomizeMatrix || '0')
    .set('comboPastAutoId', settings.comboPastAutoId);
