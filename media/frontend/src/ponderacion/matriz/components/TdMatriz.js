import React from 'react';

import { DisplayOrInput } from 'common/components/Form';

const TdMatriz = ({
  onChange,
  rowSpan = 1,
  value,
  editable,
  ...props
}) => (
  <td rowSpan={rowSpan}>
    <DisplayOrInput
      {...props}
      required={true}
      editable={editable}
      onChange={onChange}
      value={value}
      pattern={'^((100|100.0|100.00)|([0-9]{1,2}(\\.[0-9]{1,2})?))$'}
    />
  </td>
);

export default TdMatriz;
