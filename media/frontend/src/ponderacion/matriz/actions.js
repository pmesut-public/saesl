import _ from 'lodash';

import * as messages from '../messages';


import constants from '../constants';
const {
  CREATE_BASE_STRUCTURE,
  UPDATE_PESO_FACTOR,
  UPDATE_PESO_ESTANDAR,
  SET_ESTANDARES_PESO,
  UPDATE_MODIFY_MATRIX,
  LOAD_PAST_PONDERACION,
  LOAD_PAST_EVALUACION,
  CLEAR_HAS_CHANGED,
} = constants;

import {
  statusFetching,
  statusSucess,
  statusError,
  statusStored,
  onSuccess,
  onError,
} from 'status/actions';

import { canEditMatriz, extractSettings } from '../settings/reducer';
import { getContext } from 'common/context';
import { extractPonderacion, extractSettingsPonderacion } from './reducer';

export const updatePesoFactor = (fact_id, value) => ({
  type: UPDATE_PESO_FACTOR,
  fact_id,
  value,
});

export const updatePesoEstandar = (esta_id, value) => ({
  type: UPDATE_PESO_ESTANDAR,
  esta_id,
  value,
});


export const setPonderacionEstandar = (estandares, factores, customEstandarPeso) =>
  (dispatch) => {
    const estandarPeso = getPesosEstandar(getContext().estandares, getContext().factores);
    dispatch(_setPonderacionEstandar(getContext().estandares, getContext().factores,
                                     estandarPeso.estandares, estandarPeso.factor));
  }

const _setPonderacionEstandar = (estandares, factores, customEstandarPeso, customFactorPeso) => ({
  type: SET_ESTANDARES_PESO,
  estandares,
  factores,
  customEstandarPeso,
  customFactorPeso,
});

export const clearPonderacion = (estandares, factores) => ({
  type: SET_ESTANDARES_PESO,
  estandares,
  factores,
  estandarPeso: 0,
});

const updateMatriz = (value) => ({
  type: UPDATE_MODIFY_MATRIX,
  value,

});

const createBaseStructure = (factores, estandares) => ({
  type: CREATE_BASE_STRUCTURE,
  factores,
  estandares,
});

const generatePonderacionPayload = (state, context) => ({
  ponderacion: extractPonderacion(
    state.get('matriz'), context.factores,
    context.estandares
  ),
  settings: {
    ...extractSettings(state.get('settings')),
    ...extractSettingsPonderacion(state.get('matriz')),
  }
});

export const savePonderacion = (cb = () => {}) => (dispatch, getState, {api}) => {
  const payload = extractPonderacion(
    getState().get('matriz'), getContext().factores,
    getContext().estandares
  );
  function extra () {
    dispatch(clearHasChanged())
    cb();
  }
  dispatch(statusFetching());
  api.savePonderacion({
    data: payload,
    onSuccess: _.partial(onSuccess, dispatch, _, extra),
    onError: _.partial(onError, dispatch, _),
  });
};

export const onLeaveMatriz = (cb) => (dispatch, getState) => {
  const hasChanged = getState().getIn(['matriz', 'hasChanged']);
  if (hasChanged && !confirm(messages.UNSAVED_CHANGED_MATRIZ)) {
    return;
  }
  cb();
};

export const closePonderacion = (cb = () => {}) => (dispatch, getState, {api}) => {
  if (document.querySelectorAll('.bg-danger.text-danger').length) {
    return alert(messages.ERRORS_MATRIZ);
  }
  const payload = extractPonderacion(
    getState().get('matriz'), getContext().factores,
    getContext().estandares
  );
  function extra () {
    dispatch(clearHasChanged())
    cb();
  }
  dispatch(statusFetching());
  api.closePonderacion({
    data: payload,
    onSuccess: _.partial(onSuccess, dispatch, _, extra),
    onError: _.partial(onError, dispatch, _),
  });
};

const loadPonderacion = (pastPonderacion) => ({
  type: LOAD_PAST_PONDERACION,
  pastPonderacion,
});

const MEJORA = 1;

const round2Decimals = num => _.floor(num * 100) / 100;

const getPesosEstandar = (estandares, factores) => {
  const estandaresGrouped = _.groupBy(estandares, e => e.fact_id);
  const estandaresPeso = estandares.reduce(
    (acc, e) => ({...acc, [e.id]: round2Decimals(100/estandaresGrouped[e.fact_id].length)}),
    {}
  );
  return {
    estandares: estandaresPeso,
    factor: round2Decimals(100 / factores.length)
  };
};

export const loadMatrizView = (context, goStep) => (dispatch, getState) => {
  const showPeso = canEditMatriz(getState().get('settings'), getContext().eventos);

  dispatch(createBaseStructure(context.factores, context.estandares));
  if (_.hasIn(context, 'ponderaciones.cerrada')) {
    dispatch(loadPonderacion(context.ponderaciones.cerrada));
  } else {
    const estandarPeso = getPesosEstandar(getContext().estandares, getContext().factores);
    dispatch(_setPonderacionEstandar(getContext().estandares, getContext().factores,
                                     estandarPeso.estandares, estandarPeso.factor));
  }
  dispatch(clearHasChanged());

  if (!goStep) return;

  if (showPeso && context.type != MEJORA) {
    goStep(1);
  } else {
    dispatch(closePonderacion(() => goStep(2)));
  }
};

export const backButtonLastView = (goStep) => (dispatch, getState) => {
  const showPeso = canEditMatriz(getState().get('settings'), getContext().eventos);

  if (showPeso && getContext().type != MEJORA) {
    goStep(1);
  } else {
    dispatch(closePonderacion(() => goStep(0)));
  }
}

export const toggleMakeChanges = (value, context) => (dispatch, getState) => {
  const hasChanged = getState().getIn(['matriz', 'hasChanged']);
  const hasBorrador = _.hasIn(context, 'ponderaciones.borrador');
  if (hasChanged && !confirm(messages.UNSAVED_CHANGED_MATRIZ)) {
    return;
  }
  if (value === '0' || (value === '1' && !hasBorrador) ) {
    dispatch(loadMatrizView(context));
  }
  else {
    dispatch(loadPonderacion(context.ponderaciones.borrador));
  }
  dispatch(clearHasChanged());
  dispatch(updateMatriz(value));
};

const loadPastEvaluacion = (autoId, estandares, factores) => ({
  type: LOAD_PAST_EVALUACION,
  autoId,
  estandares,
  factores,
});

const clearHasChanged = () => ({
  type: CLEAR_HAS_CHANGED,
});

export const choosePastAutoId = (autoId, autoevaluaciones, factores) => (dispatch) => {
  const estandares = autoevaluaciones.filter(a => a.id === autoId)[0].data.estandares;
  dispatch(loadPastEvaluacion(autoId, estandares, factores));
};
