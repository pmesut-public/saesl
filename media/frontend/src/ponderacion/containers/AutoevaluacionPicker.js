import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row } from 'react-bootstrap';

import NavigationBar from'../components/NavigationBar';
import { getContext } from 'common/context';
import { updateEvento } from '../settings/actions';
import { loadMatrizView } from '../matriz/actions';

import { SelectField  }from 'common/components/Form/';
import { getEvent } from '../settings/reducer';

const EventoPicker = ({
  eventId,
  updateEvento,
  onNext,
}) => (
  <div>
    <Row>
      <SelectField
        label="Evento:"
        classWrapper="form-group col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4"
        value={eventId}
        onChange={updateEvento}
        options={getContext().eventos}
      />
    </Row>
    <NavigationBar
      nextButton={{
        onClick: onNext,
        disabled: !eventId
      }}
    />
  </div>
);

const mapStateToProps = (state) => ({
  eventId: getEvent(state.get('settings')),
});

const mapDispatchToProps = (dispatch, { goStep }) => ({
  updateEvento: ({target}) => dispatch(updateEvento(target.value)),
  onNext: () => dispatch(loadMatrizView(getContext(), goStep)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EventoPicker);
