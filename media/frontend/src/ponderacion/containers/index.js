import React, { Component } from 'react';
import { connect } from 'react-redux';

import { navigate } from '../settings/actions';
import { getCurrentStep } from '../settings/reducer';
import { Step, Steps } from 'common/components/navigation';
import AutoevaluacionPicker from './AutoevaluacionPicker';
import Matriz from '../matriz/containers';
import ConfigurarAutoevaluacion from './ConfigurarAutoevaluacion';

const App = (props) => (
  <Steps {...props}>
    <Step title={"Seleccionar evento"} comp={AutoevaluacionPicker} />
    <Step title={"Matriz de Calidad"} comp={Matriz}/>
    <Step title={"Configurar autoevaluación"} comp={ConfigurarAutoevaluacion}/>
  </Steps>
);

const mapStateToProps = (state) => ({
  currentStep: getCurrentStep(state.get('settings')),
});

const mapDispatchToProps = {
  navigate,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
