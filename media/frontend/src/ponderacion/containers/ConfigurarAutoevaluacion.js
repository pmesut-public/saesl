import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row } from 'react-bootstrap';

import NavigationBar from'../components/NavigationBar';
import { isFetching } from 'status/reducer';
import { getContext } from 'common/context';
import { getAutoId, getEvent } from '../settings/reducer';
import { RadioField } from 'common/components/Form';
import { updateAutoevaluacion } from '../settings/actions';
import { backButtonLastView } from '../matriz/actions';
import constants from '../constants';

const createInput = (name, value) =>
  $("<input>")
    .attr("type", "hidden")
    .attr("name", name)
    .val(value);

class ConfigurarAutoevaluacion extends Component {
  _onNext = () => {
    const {
      tipoAuto,
      eventId,
    } = this.props;

    var form = $(document.createElement('form'));
    $(form).attr("method", "POST");
    $(form).attr('action', '/autoevaluaciones/new');
    $(form).append($(createInput('tipo_auto', tipoAuto)));
    $(form).append($(createInput('even_id', eventId)));
    form.appendTo( document.body )
    $(form).submit();
  }

  render() {
    const {
      tipoAuto,
      updateAutoevaluacion,
      options,
      goBack,
    } = this.props;

    return (
      <div>
        <Row>
          <RadioField
            classWrapper='col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4'
            options={options}
            selected={tipoAuto}
            onChange={updateAutoevaluacion}
          />
        </Row>
        <NavigationBar
          nextButton={{
            onClick: this._onNext,
            disabled: !tipoAuto,
          }}
          backButton={{
            onClick: goBack,
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  tipoAuto: getAutoId(state.get('settings')),
  eventId: getEvent(state.get('settings')),
  options: [{
    id: 'NUEVO',
    text: 'Iniciar autoevaluación en blanco'
  }, {
    id: 'COPIA',
    text: 'Copiar autoevaluación anterior',
    disabled: !getContext().autoevaluaciones || !getContext().autoevaluaciones.length,
  }],
});

const mapDispatchToProps = (dispatch, { goStep }) => ({
  updateAutoevaluacion: (e) => dispatch(updateAutoevaluacion(e.target.value)),
  goBack: () => dispatch(backButtonLastView(goStep))
});

export default connect(mapStateToProps, mapDispatchToProps)(ConfigurarAutoevaluacion);

