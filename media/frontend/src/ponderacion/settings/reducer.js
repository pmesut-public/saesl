import { fromJS } from 'immutable';
import { createReducer } from 'redux-immutablejs';

import constants from '../constants';
const {
  NAVIGATE,
  UPDATE_EVENT,
  UPDATE_TIPO_AUTOEVALUACION,
} = constants;

const initialState = fromJS({
  currentStep: 0,
  eventId: null,
  autoId: null,
});

export default createReducer(initialState, {
  [NAVIGATE]: (state, {nextStep}) => state.set('currentStep', nextStep),

  [UPDATE_EVENT]: (state, {eventId}) => state.set('eventId', eventId),

  [UPDATE_TIPO_AUTOEVALUACION]: (state, {autoId}) =>
    state.set('autoId', autoId),
});

export const getCurrentStep = (state) => state.get('currentStep');

export const getEvent = (state) => state.get('eventId') || '';

export const getAutoId = (state) => state.get('autoId');

export const canEditMatriz = (state, eventos) => {
  const eventId = getEvent(state);
  return eventId && !!+eventos.filter(e => e.id == eventId)[0].editable;
};

export const extractSettings = (state) => ({
  currentStep: state.get('currentStep'),
  eventId: state.get('eventId'),
});

export const setSettings = (state, settings) =>
  state
    .set('currentStep', settings.currentStep || 0)
    .set('eventId', settings.eventId);
