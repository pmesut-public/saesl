import constants from '../constants';
const {
  NAVIGATE,
  UPDATE_EVENT,
  UPDATE_TIPO_AUTOEVALUACION,
} = constants;

export const navigate = (nextStep) => ({
  type: NAVIGATE,
  nextStep,
});

export const updateEvento = (eventId) => ({
  type: UPDATE_EVENT,
  eventId,
});

export const updateAutoevaluacion = (autoId) => ({
  type: UPDATE_TIPO_AUTOEVALUACION,
  autoId,
});
