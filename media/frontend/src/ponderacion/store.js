import createStore from 'common/createStore';
import * as api from './api';
import rootReducer from './reducer';

export default initialState => createStore({initialState, api: {api}, rootReducer});
