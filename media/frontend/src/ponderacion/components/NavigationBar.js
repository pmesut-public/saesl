import React from 'react';

import Button from 'common/components/buttons';
import { Clearfix, PullLeft, PullRight } from 'common/components/Layout';

const NavigationBar = ({
  backButton,
  nextButton,
  extraButton = () => {},
}) => (
    <div style={{marginTop: '5px', marginBottom: '5px'}}>
    {
      backButton && (
        <PullLeft>
          <Button info {...backButton}>Atrás</Button>
        </PullLeft>
      )
    }
    {
      nextButton && (
        <PullRight>
          {extraButton()}
        {' '}
          <Button info {...nextButton}>{nextButton.title || 'Siguiente'}</Button>
        </PullRight>
      )
    }

    <Clearfix/>
  </div>
);

export default NavigationBar;
