import React from 'react';

import Button from 'common/components/buttons';

const NavigationButton = ({
  children,
  ...props,
}) => (
  <Button {...props} info>{children}</Button>
);

export default NavigationButton;
