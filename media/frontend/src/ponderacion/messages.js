export const UNSAVED_CHANGED_MATRIZ = 'Los cambios no fueron guardados. Desea perder los cambios?';

export const EDIT_MATRIZ = 'El proceso de Autoevaluación requiere que usted asigne PORCENTAJES a los FACTORES y ESTANDARES. Tiene las opciones de cargar una ponderación anterior. Recuerde que la suma de los porcentajes en las DIMENSIONES y FACTORES deben sumar 100%, para el caso de los ESTANDARES, la suma de los PORCENTAJES de todos los estándares que corresponden a un FACTOR deben totalizar 100%.';

export const ERRORS_MATRIZ = 'No se puede continuar porque existen errores en la matriz';
