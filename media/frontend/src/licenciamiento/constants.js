import keyMirror from 'keymirror';

const constants = keyMirror({
  UPDATE_FORMATO: null,
  UPDATE_CONDICION: null,

  SET_ERRORS: null,
  FINISHED_REQUEST: null,
  ALL_HAS_CHANGED: null,
  ALL_ERROR: null,
  ALL_FETCHING: null,
  ALL_STORED: null,
});

export default constants;
