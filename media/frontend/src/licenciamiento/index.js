import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import App from './containers/';
import createStore from './store';
import getInitialState from './initialState';
import { setContext } from 'common/context';

let context = window.__CONTEXT__;
console.log(context);
const initialState = getInitialState(window.__INITIAL_STATE__, context);

setContext(context);
const store = createStore(initialState, context);

render(
    <Provider store={store} >
    <App />
    </Provider>,
  document.getElementById('root')
);
