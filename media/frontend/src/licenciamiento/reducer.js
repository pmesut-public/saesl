import { fromJS } from 'immutable';
import { createReducer, combineReducers } from 'redux-immutablejs';

import system_status, { summaryStatus } from 'autoevaluacion/tablaAuto/reducer';
import formatos from './formatos/reducer';
import condiciones from './condiciones/reducer';
import ui from './ui'
import { getStatusFromKey } from 'status/reducer';

export default combineReducers({
  ui,
  system_status,
  formatos,
  condiciones,
});

export const getSummaryStatus = (state) => {
  const statuses = [
    ...getStatusFromKey(state, 'formatos').toJS(),
    ...getStatusFromKey(state, 'condiciones').toJS(),
  ];

  const summaryHasChanged = statuses.map(s => s.hasChanged).reduce((acc, s) => acc || s, false);
  return summaryStatus(state.getIn(['system_status', 'status']), summaryHasChanged);
};
