import { fromJS } from 'immutable'
import { createReducer } from 'redux-immutablejs'
import _pick from 'lodash/pick'

import constants from 'licenciamiento/constants'
const { UPDATE_CONDICION, ALL_STORED } = constants

const initialState = fromJS({ ids: [], items: {} })

export default createReducer(initialState, {
  [ALL_STORED]: state =>
    state.withMutations(state => {
      state.get('ids').forEach(id => {
        state.setIn(['items', id, 'hasChanged'], false)
      })
    }),

  [UPDATE_CONDICION]: (state, { id, prop, value }) => {
    let tempState = state
      .setIn(['items', id, prop], value)
      .setIn(['items', id, 'hasChanged'], true)
    if (prop === 'docu_id' && !value) {
      tempState = tempState.setIn(['items', id, 'estado'], '')
    }
    return tempState
  },
})

export const getCondiciones = state =>
  state
    .getIn(['condiciones', 'ids'])
    .map(id => state.getIn(['condiciones', 'items', id]))

export const getPayloadCondicion = state =>
  getCondiciones(state)
    .filter(cond => cond.get('hasChanged'))
    .toJS()
    .map(o => _pick(o, ['cond_id', 'fecha_inicio', 'fecha_fin', 'da']))
