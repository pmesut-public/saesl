import React, { Component } from 'react'
import { connect } from 'react-redux'

import format from 'date-fns/format'
import Condicion from './Condicion'
import { getCondiciones } from '../reducer'
import { updateCondicion, removeFile } from '../actions'
import { getOperadorView } from 'licenciamiento/ui'

import { subHeaders } from 'licenciamiento/formatos/containers'
import { Table, TableHeader } from 'common/components/table'

const headers = [
  {
    display: 'Condiciones básicas de calidad',
    customStyle: {},
  },
  //{ display: 'Indicadores', customStyle: { width: '24%', rowSpan: 2 } },
  { display: 'Documento', customStyle: { width: '15%' } },
  /*{ display: 'Vigencia', customStyle: { width: '8%' } },*/
  /*
  { display: 'Estado', customStyle: { width: '8%', rowSpan: 2 } },
  {
    display: `Estado act. al: ${format(new Date(), 'DD/MM/YYYY')}`,
    customStyle: { width: '15%', colSpan: 2 },
  },
  */
]

const headersOperador = [
  {
    display: 'Condiciones básicas de calidad',
    customStyle: { width: '60%', colSpan: 4 },
  },
  { display: 'Formatos', customStyle: { width: '40%', colSpan: 2 } },
  /*
  { display: 'Indicadores', customStyle: { width: '25%' } },
  { display: 'Documento' },
  { display: 'Calidad' },
  { display: 'Vigencia' },
  */
]

const headers2 = [
  { display: 'Estado', customStyle: { rowSpan: 2, width: '30%' } },
  { display: 'Evaluacion de indicadores', customStyle: { colSpan: 3 } },
  { display: 'Información institucional.', customStyle: { rowSpan: 2, width: '12%' } },
  { display: 'Documento', customStyle: { rowSpan: 2, width: '12%' } },
]

const headers3 = [
  { display: 'N°', customStyle: { width: '5%'} },
  { display: 'Resultados', customStyle: { width: '24%'} },
  { display: 'Vigencia', customStyle: { width: '10%'} },
]

class Condiciones extends Component {
  render() {
    const {
      updateCondicion,
      condiciones,
      removeFile,
      operadorView,
    } = this.props

    return (
      <div className={`formatos`}>
        <h3 className="text-center">Formatos de Licenciamiento B</h3><br/>
        <Table bordered>
          {operadorView ? (
            <thead>
              <TableHeader.Row headers={headersOperador} />
              <TableHeader.Row headers={headers2} />
              <TableHeader.Row headers={headers3} />
            </thead>
          ) : (
            <thead style={{ verticalAlign: 'middle' }}>
              <TableHeader.Row headers={headers} />
            </thead>
          )}

          <tbody>
            {condiciones.map((c, i) => (
              <Condicion
                operadorView={operadorView}
                condicion={c}
                index={i + 1}
                key={c.get('cond_id')}
                removeFile={removeFile}
                onChange={updateCondicion}
              />
            ))}
          </tbody>
        </Table>
      </div>
    )
  }
}

const mapStateToProps = (state, { tipo }) => ({
  condiciones: getCondiciones(state, tipo),
  operadorView: getOperadorView(state),
})

const mapDispatchToProps = { updateCondicion, removeFile }

export default connect(mapStateToProps, mapDispatchToProps)(Condiciones)
