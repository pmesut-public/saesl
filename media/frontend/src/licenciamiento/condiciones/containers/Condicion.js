import React, { Component } from 'react';
import roman from 'roman-numeral';

import * as messages from 'licenciamiento/messages';
import { Modal } from 'react-bootstrap';
import ModalReact from 'react-modal';
import FileStatusPicker from 'common/components/FileStatusPicker';
import StatusBadge from 'common/components/StatusBadge';
import Status from 'common/components/Status';
import RangeDatePicker from 'common/components/Form/RangeDatePicker';
import FilePreview from '../../components/FilePreview';
import FileUpload from '../../containers/FileUpload';
import { urls } from 'licenciamiento/api';
import { InputField } from 'common/components/Form';
import { getContext } from 'common/context';
import { getVigenciaValues } from 'licenciamiento/formatos/containers/Formato';
import Indicadores, {
  getIndicadorAverage,
  getIndicadorColor,
} from './Indicadores';

const letter = {
  '0': 'P',
  '1': 'C',
};

const styleLetter = {
  position: 'absolute',
  color: 'black',
  left: '50%',
  top: '50%',
  transform: 'translate(-50%, -50%)',
};

const bg = {
  '': '',
  yellow: 'bg-warning',
  green: 'bg-success',
  red: 'bg-danger',
};

const colors = {
  yellow: '#fcf8e3',
  green: '#dff0d8',
  red: '#f2dede',
  black: '#f9f9f9',
};

const vigenciaTitle = {
  green: 'Vigente',
  yellow: 'Próximo a perder Vigencia',
  red: 'Fuera de vigencia',
};

const getBgColor = id => {
  const indicadores = getContext()
    .ind.filter(ind => ind.cond_id == id)
    .map(ind => {
      return { ...ind, average: getIndicadorAverage(ind.ind_id) };
    });
  let average = -1;
  if (indicadores.length) {
    average =
      indicadores.reduce((acc, i) => acc + (i.average > 0 ? i.average : 0), 0) /
      indicadores.length;
  }
  if (average < 0) return colors.black;
  if (average < 3) {
    return colors.red;
  }
  if (average == 3) {
    return colors.yellow;
  }
  return colors.green;
};

class Condicion extends Component {
  state = { isOpenModal: false };
  onChange = (key, value) => {
    this.props.onChange(this.props.condicion.get('cond_id'), key, value);
  };

  onRangeChange = (fechaInicio, fechaFin) => {
    this.onChange('fecha_inicio', fechaInicio);
    this.onChange('fecha_fin', fechaFin);
  };

  onSelect = ({ target }) => {
    const { tipo, condicion } = this.props;
    this.onChange('estado', target.value);
  };

  onFileUploaded = ({ docu_id, display, path }) => {
    const { condicion } = this.props;
    const id = condicion.get('cond_id');
    this.onChange('path', path);
    this.onChange('file_name', display);
    this.onChange('docu_id', docu_id);
    this.onChange('fecha_inicio', '');
    this.onChange('fecha_fin', '');
    this.onChange('da', null);
    this.onChange('estado', this.estados.estado);
    this.onChange('sub_estado', this.estados.subEstado);
  };
  openModal = next => {
    this.callbackUploadFile = next;
    this.setState({
      isOpenModal: true,
    });
  };
  closeModal = () => this.setState({ isOpenModal: false });
  renderModal = () => {
    const { isOpenModal } = this.state;
    const modalStyles = {
      overlay: { zIndex: 10 },
      content: {
        maxWidth: '400px',
        maxHeight: '300px',
        left: '50%',
        transform: 'translateX(-50%)',
      },
    };

    return (
      <ModalReact style={modalStyles} isOpen={isOpenModal} contentLabel="Modal">
        <Modal.Header>
          <Modal.Title>Desea subir el archivo?</Modal.Title>
        </Modal.Header>
        {isOpenModal && (
          <FileStatusPicker
            hideExtraInfo={true}
            estados={getContext().estados}
            subEstados={getContext().sub_estados}
            onCancel={this.closeModal}
            onSubmit={this.uploadFile}
          />
        )}
      </ModalReact>
    );
  };

  uploadFile = estados => {
    this.estados = estados;
    this.closeModal();
    this.callbackUploadFile(
      _.mapKeys(_.pick(estados, ['estado', 'subEstado']), (v, k) =>
        _.snakeCase(k)
      )
    );
  };

  removeFile = () => {
    const { condicion } = this.props;
    this.props.removeFile(condicion.get('cond_id'), condicion.get('docu_id'));
  };

  onBeforeReplace = () => {
    if (!confirm(messages.CONFIRM_REPLACE_FILE)) {
      return;
    }
    this.button.click();
  };

  render() {
    const { condicion, index, operadorView: vistaOperador } = this.props;

    const {
      cond_id,
      display,
      estado,
      sub_estado,
      docu_id,
      path,
      title,
      file_name,
      loading,
      fecha_inicio,
      fecha_fin,
      da,
      cumplen,
      noCumplen,
      noAplican,
      noEvaluado,
      docIncompletos,
      vigenciaA,
      condicionEstado,
      repetidos,
    } = condicion;

    const rangeSelected = fecha_inicio && fecha_fin;
    const vigencia = getVigenciaValues(condicion);
    const statusPending = estado != getContext().estados[1].id;

    let backgroundColor = '';
    if (!vistaOperador) {
      backgroundColor = docu_id ? 'bg-success' : '';
    } else {
      backgroundColor = bg[condicionEstado ? '' + condicionEstado : ''];
    }

    return (
      <tr>
        <td className={backgroundColor}>
          <strong>Condicion {roman.convert(index)} :</strong>
          {display}
          {vistaOperador && (
            <div className="well" style={{ padding: '5px' }}>
              <div>
                <strong>Indicadores :</strong>
              </div>
              <br />
              <div>
                <div style={{ float: 'left', width: '50%' }}>
                  {cumplen} SI cumplen
                </div>
                <div style={{ float: 'right', width: '50%' }}>
                  {noCumplen} NO cumplen
                </div>
                <div style={{ clear: 'both' }} />
              </div>
              <br />
              <div>
                <div style={{ float: 'left', width: '50%' }}>
                  {noEvaluado} NO evaluado
                </div>
                <div style={{ float: 'right', width: '50%' }}>
                  {noAplican} NO aplican
                </div>
                <div style={{ clear: 'both' }} />
              </div>
              <div>
                <div style={{ float: 'left', width: '50%' }}>
                  {docIncompletos} doc. incompletos
                </div>
                <div style={{ clear: 'both' }} />
              </div>
            </div>
          )}
          {vistaOperador &&
            repetidos && (
              <div className="alert alert-info">
                Existen documentos repetidos
              </div>
            )}
        </td>
        {vistaOperador && (
          <td style={{ fontSize: '33px' }}>
            {getContext().ind.filter(ind => ind.cond_id == cond_id).length}
          </td>
        )}
        {vistaOperador && (
          <td className="text-center">
            <Indicadores id={cond_id} />
          </td>
        )}
        {vistaOperador && (
          <td className="text-center">
            {vigenciaA && (
              <Status
                status={vigenciaA}
                style={{ margin: '0 auto' }}
                title={vigenciaTitle[vigenciaA]}
              />
            )}
          </td>
        )}
        {vistaOperador && (
          <td className={docu_id ? 'bg-success' : ''}>{title}</td>
        )}
        <td>
          {this.renderModal()}
          {vistaOperador && docu_id ? (
            <FilePreview hideRemove fileName={file_name} path={path} />
          ) : null}
          {!vistaOperador &&
            (docu_id ? (
              <FilePreview
                onRemove={this.removeFile}
                fileName={file_name}
                path={path}
              >
                <span style={{ position: 'relative' }}>
                  <FileUpload
                    title="Reemplazar"
                    onBeforeUpload={this.openModal}
                    disabled={loading}
                    innerRef={ref => (this.button = ref)}
                    url={urls.addDocumento}
                    onSuccess={this.onFileUploaded}
                    params={{ id: cond_id, tipo: 'COND', reemplazar: 1 }}
                  />
                  <div
                    onClick={this.onBeforeReplace}
                    style={{
                      cursor: 'pointer',
                      position: 'absolute',
                      top: '-4px',
                      bottom: '-7px',
                      right: 0,
                      left: 0,
                      zIndex: 1,
                    }}
                  />
                </span>
              </FilePreview>
            ) : (
              <FileUpload
                onBeforeUpload={this.openModal}
                disabled={loading}
                url={urls.addDocumento}
                onSuccess={this.onFileUploaded}
                params={{ id: cond_id, tipo: 'COND', reemplazar: 0 }}
              />
            ))}
        </td>
        {false && (
          <td>
            <Status status={condicionEstado} style={{ margin: '0 auto' }}>
              <div style={styleLetter}>{letter[estado]}</div>
            </Status>
          </td>
        )}
        {false && (
          <td>
            <RangeDatePicker
              onChange={this.onRangeChange}
              startTime={fecha_inicio}
              endTime={fecha_fin}
              disabled={!docu_id}
            />
          </td>
        )}

        {false && <td>{vigencia.dv}</td>}
        {false && (
          <td>
            <InputField
              value={da || ''}
              onChange={val => this.onChange('da', val)}
              disabled={!rangeSelected}
              pattern={'^[1-9]+$'}
              required
            />
          </td>
        )}
        {false && <td>{vigencia.ed}</td>}
      </tr>
    );
  }
}

export default Condicion;
