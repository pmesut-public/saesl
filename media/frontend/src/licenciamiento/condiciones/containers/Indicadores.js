import React, { Component } from 'react';
import _groupBy from 'lodash/groupBy';

import { getContext } from 'common/context';

const doc_estados = {
  green: 5,
  yellow: 3,
  red: 1,
};

export const getIndicadorAverage = ind_id => {
  const detalle = getContext().auto_detalles.filter(
    de => de.esta_id == ind_id
  )[0];
  if (!detalle) return -2;
  if (detalle.aude_estado == getContext().estado_indicadores[2].id) {
    return -3;
  }
  if (detalle.aude_estado == getContext().estado_indicadores[0].id) {
    return -2;
  }
  if (detalle.aude_estado != getContext().estado_indicadores[1].id) {
    return -1;
  }
  const docGrouped = _groupBy(getContext().auto_documentos, d => d.aume_id);

  let total = 0;
  let cant = 0;
  if (!getContext().auto_medio) return -1;
  getContext()
    .auto_medio.filter(m => m.aude_id == detalle.aude_id)
    .forEach(m => {
      docGrouped[m.aume_id] &&
        docGrouped[m.aume_id]
          .filter(d => d.audo_estado == getContext().estados[1].id)
          .forEach(d => {
            const found = _.find(
              getContext().sub_estados,
              s => s.id == d.audo_subestado
            );
            if (found) {
              total += +found.id;
              //total += doc_estados[found.color]
            } else {
              total += 0;
            }
            cant++;
          });
    });
  if (!cant) return -1;
  return Math.floor(total / (cant || 1));
};

export const getIndicadorColor = average => {
  if (average == -3) return colors.black2;
  if (average == -2) return colors.blue;
  if (average == -1) return colors.black;
  if (average < 4) {
    return colors.red;
  }
  if (average < 5) {
    return colors.yellow;
  }
  return colors.green;
};

const colors = {
  black2: 'black',
  yellow: '#f39c12',
  green: '#00a65a',
  red: '#f56954',
  black: 'white',
  blue: 'lightblue',
};

class Indicadores extends Component {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    const { id } = this.props;
    const indicadores = getContext()
      .ind.filter(ind => ind.cond_id == id)
      .map(ind => {
        return {
          ...ind,
          color: getIndicadorColor(getIndicadorAverage(ind.ind_id)),
        };
      });

    //<div className="indicadores__total">{indicadores.length}</div>
    return (
      <div className="indicadores__wrapper">
        <div className="">
          {indicadores.map(ind => (
            <div
              key={ind.ind_id}
              style={{
                borderColor: ind.color,
                borderRadius:
                  ind.color == colors.blue || ind.color == colors.black2
                    ? '50%'
                    : 0,
              }}
              className={`indicadores__item`}
            >
              {ind.cod}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default Indicadores;
