import constants from 'licenciamiento/constants'
const { UPDATE_CONDICION, ALL_STORED } = constants

import * as messages from 'licenciamiento/messages'

export const updateCondicion = (id, prop, value) => ({
  type: UPDATE_CONDICION,
  id,
  prop,
  value,
})

export const removeFile = (id, docu_id) => (dispatch, getState, { api }) => {
  if (!confirm(messages.CONFIRM_REMOVE_FILE)) {
    return
  }
  dispatch(updateCondicion(id, 'loading', true))
  api.removeDocumento({
    data: {
      docu_id,
      id,
      tipo: 'COND',
    },
    onSuccess: () => {
      dispatch(updateCondicion(id, 'loading', false))
      dispatch(updateCondicion(id, 'path', null))
      dispatch(updateCondicion(id, 'file_name', null))
      dispatch(updateCondicion(id, 'docu_id', null))
      dispatch(updateCondicion(id, 'estado', null))
      dispatch(updateCondicion(id, 'sub_estado', null))
      dispatch(updateCondicion(id, 'fecha_inicio', ''))
      dispatch(updateCondicion(id, 'fecha_fin', ''))
      dispatch(updateCondicion(id, 'da', null))
    },
    onError: err => {
      console.log(err)
      dispatch(updateCondicion(id, 'loading', false))
    },
  })
}
