import { fromJS } from 'immutable'
import { createReducer } from 'redux-immutablejs'

const UPDATE_PROP = 'ui/UPDATE_PROP'
const TOGGLE_VIEW = 'ui/TOGGLE_VIEW'

export const toggleOperadorView = () => ({
  type: TOGGLE_VIEW,
})

const initialState = fromJS({
  operadorView: true,
})

export default createReducer(initialState, {
  [TOGGLE_VIEW]: state =>
    state.update('operadorView', operadorView => !operadorView),
})

export const getOperadorView = state => state.getIn(['ui', 'operadorView'])
