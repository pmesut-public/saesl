export const messageLeavePage = 'Existen datos sin guardar. Desea salir?'

export const CONFIRM_REMOVE_FILE = '¿Desea eliminar el archivo?'

export const CONFIRM_REPLACE_FILE =
  '¿Está seguro que desea reemplazar el archivo?'
