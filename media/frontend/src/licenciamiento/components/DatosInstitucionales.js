import React from 'react';

const Panel = ({title, content, first}) => (
  <div className={`col-lg-3 col-md-3 col-sm-6`}>
    <div className="panel panel-info ">
      <div className="panel-heading">
        <h3 className="panel-title">{title}</h3>
      </div>
      <div className="panel-body">{content}</div>
    </div>
  </div>
)

const DatosInstitucionales = ({
  institucion,
  modalidad,
  gestion,
  region,
}) => (
  <div>
    <div className="row">
      <Panel title="INSTITUCION" content={institucion} first/>
      <Panel title="MODALIDAD" content={modalidad} />
      <Panel title="GESTION" content={gestion} first/>
      <Panel title="REGION" content={region} />
    </div>
  </div>
);

export default DatosInstitucionales;
