import React from 'react'
import { Glyphicon } from 'react-bootstrap'

import { RemoveButton } from 'common/components/buttons'

const FilePreview = ({
  disabled,
  onRemove,
  fileName,
  path,
  children,
  hideRemove = false,
}) => (
  <div style={{ textAlign: 'center' }}>
    {!hideRemove && (
      <RemoveButton
        title="Eliminar archivo"
        onClick={onRemove}
        disabled={disabled}
      />
    )}
    <a
      className="btn btn-flat btn-primary"
      href={`/file/${path}`}
      style={{ marginLeft: '5px', marginRight: '5px' }}
      target="_blank"
      title={fileName}
    >
      <Glyphicon glyph="download-alt" />
    </a>
    {children}
  </div>
)

export default FilePreview
