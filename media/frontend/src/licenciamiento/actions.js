import zipWith from 'lodash/zipWith';

import { getPayloadCondicion, getCondiciones } from './condiciones/reducer';
import { getPayloadFormato, getFormatos } from './formatos/reducer';
import { getContext } from 'common/context';

import constants from 'autoevaluacion/constants';
const {
  SET_ERRORS,
  ALL_STORED,
  ALL_FETCHING,
  ALL_ERROR,
  FINISHED_REQUEST,
} = constants;

export const setErrors = errors => ({
  type: SET_ERRORS,
  errors,
});

export const finishedRequest = () => ({
  type: FINISHED_REQUEST,
});

export const allStored = () => ({
  type: ALL_STORED,
});

export const allFetching = id => ({
  type: ALL_FETCHING,
  id,
});

const allSaveError = msg => dispatch => {
  removeOverlay();
  dispatch(allError(msg));
};

export const allError = (msg = 'Ocurrió un error') => dispatch => {
  dispatch({
    type: ALL_ERROR,
  });
  alert(msg);
};

const removeOverlay = () => {
  const elem = document.getElementById('auto_overlay');
  if (!elem) return;
  elem.parentElement.removeChild(elem);
};

const allSaveStored = () => dispatch => {
  removeOverlay();
  dispatch(allStored());
};

const addOverlay = () => {
  var overlay = document.createElement('div');
  overlay.id = 'auto_overlay';
  var inner = document.createElement('div');
  inner.id = 'auto_inner_overlay';
  inner.innerHTML =
    '<div><i class="fa fa-refresh fa-animate"></i> Guardando</div>';
  overlay.appendChild(inner);
  document.querySelector('body').appendChild(overlay);
};

const getPayload = state => {
  return {
    lic_a: getPayloadFormato(state, 'A'),
    lic_c: getPayloadFormato(state, 'C'),
    cond: getPayloadCondicion(state),
  };
};

const formatos = ['A', 'B', 'C'];

const extractErrors = getState => {
  const inlineErrors = [...document.querySelectorAll('.formatos')].map(
    (item, i) => ($(item).find('.bg-danger').length > 0 ? formatos[i] : '')
  );
  const fileErrors = [
    getFormatos(getState(), 'A').filter(f => !f.get('path')).size
      ? formatos[0]
      : '',
    getCondiciones(getState()).filter(f => !f.get('path')).size
      ? formatos[1]
      : '',
    getFormatos(getState(), 'C').filter(f => !f.get('path')).size
      ? formatos[2]
      : '',
  ];

  const vigenciaErrors = [
    getFormatos(getState(), 'A').filter(
      f => !new RegExp('^[0-9]+$').test(f.get('da'))
    ).size
      ? formatos[0]
      : '',
    '',
    /*
    getCondiciones(getState()).filter(
      f => !new RegExp('^[0-9]+$').test(f.get('da'))
    ).size
      ? formatos[1]
      : '',
      */
    getFormatos(getState(), 'C').filter(
      f =>
        f.get('estado') == getConcluidoEstadoId()
          ? !new RegExp('^[0-9]+$').test(f.get('da'))
          : false
    ).size
      ? formatos[2]
      : '',
  ];
  console.log(vigenciaErrors);

  return [
    '',
    //inlineErrors
    //.map((errors, i) => ({
    //errors: inlineErrors[i],
    //}))
    //.filter(item => item.errors.length > 0)
    //.map(item => item.errors)
    //.join(' ,'),
    fileErrors.filter(e => e.length).join(','),
    vigenciaErrors.filter(e => e.length).join(','),
  ];
};

export const getConcluidoEstadoId = () => getContext().estados[1].id;

export const cancelEvento = url => (dispatch, getState, { api }) => {
  api.getEstadoFichas({
    data: {},
    onSuccess: ({
      condiciones_basicas_cancelado,
      autoevaluacion_formatos_cancelado,
    }) => {
      let msg = '';
      if (!condiciones_basicas_cancelado) {
        msg =
          '\nDebe cancelar la Autoevaluación de las Condiciones Básicas de Calidad';
      }
      if (!autoevaluacion_formatos_cancelado) {
        msg += ' \nDebe cancelar la Autoevaluación de los formatos A,B,C';
      }
      if (msg.length) {
        return alert('No se pudo cancelar el evento:' + msg);
      }
      window.location.href = url;
    },
    onError: () => {},
  });
};

const saveAllData = (dispatch, getState, api, cb) => {
  const payload = getPayload(getState());
  console.log(payload);

  const shouldNotSave =
    Object.keys(payload)
      .map(k => payload[k].length)
      .reduce((acc, i) => acc + i, 0) === 0;

  const zipErrors = extractErrors(getState);
  if (cb) {
    dispatch(setErrors(zipErrors));
  }

  if (shouldNotSave && !cb) {
    return;
  }

  dispatch(allFetching());
  addOverlay();

  api.saveAll({
    data: payload,
    onSuccess: () => {
      dispatch(allSaveStored());
      if (cb) {
        cb(zipErrors);
      }
    },
    onError: err => {
      console.log('err', err);
      dispatch(allSaveError());
    },
  });
};

export const saveAll = () => (dispatch, getState, { api }) =>
  saveAllData(dispatch, getState, api);

const finish = api => {
  api.finish({
    onSuccess: () => {
      console.log('success');
      window.location.href = '/autoevaluaciones';
    },
    onError: err => {
      alert(err.msg);
    },
  });
  // var form = $(document.createElement('form'));
  // $(form).attr("method", "POST");
  // $(form).attr('action', '/formatos/finish');
  // form.appendTo( document.body );
  // $(form).submit();
};

export const closeEvento = () => (dispatch, getState, { api }) => {
  saveAllData(dispatch, getState, api, err => {
    dispatch(allSaveStored());
    if (err.join('').length) {
      return alert('Existen errores. No se puede finalizar la ficha');
    }
    api.finish({
      onSuccess: () => {
        console.log('success');
        api.autoevaluacionStatus({
          onSuccess: ({ estado }) => {
            if (!estado) {
              return alert(
                'Existen errores en la autoevaluación, debe corregirlos para poder cerrar la ficha'
              );
            }
            var form = $(document.createElement('form'));
            $(form).attr('method', 'POST');
            $(form).attr('action', '/formatos/finish_evento');
            form.appendTo(document.body);
            $(form).submit();
          },
          onError: err => {
            alert(err.msg);
          },
        });
      },
      onError: err => {
        alert(err.msg);
      },
    });
  });
};

export const closeFicha = () => (dispatch, getState, { api }) => {
  saveAllData(dispatch, getState, api, err => {
    dispatch(allSaveStored());
    if (err.join('').length) {
      return alert('Existen errores. No se puede finalizar la ficha');
    }
    finish(api);
  });
};
