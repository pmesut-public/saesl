import { fromJS } from 'immutable'
import { createReducer } from 'redux-immutablejs'
import _pick from 'lodash/pick'

import constants from 'licenciamiento/constants'
const { UPDATE_FORMATO, ALL_STORED } = constants

const initialState = fromJS({ ids: [], items: {} })
const convertId = (id, tipo) => `${id}_${tipo}`

export default createReducer(initialState, {
  [ALL_STORED]: state =>
    state.withMutations(state => {
      state.get('ids').forEach(id => {
        state.setIn(['items', id, 'hasChanged'], false)
      })
    }),

  [UPDATE_FORMATO]: (state, { id, tipo, prop, value }) => {
    let tempState = state
      .setIn(['items', convertId(id, tipo), prop], value)
      .setIn(['items', convertId(id, tipo), 'hasChanged'], true)
    if (prop === 'docu_id' && !value) {
      tempState = tempState.setIn(['items', convertId(id, tipo), 'estado'], '')
    }
    return tempState
  },
})

export const getFormatos = (state, tipo) =>
  state
    .getIn(['formatos', 'ids'])
    .filter(id => id.indexOf(tipo) > -1)
    .map(id => state.getIn(['formatos', 'items', id]))

export const getPayloadFormato = (state, tipo) =>
  getFormatos(state, tipo)
    .filter(formato => formato.get('hasChanged'))
    .toJS()
    .map(o => _pick(o, ['for_id', 'da', 'fecha_inicio', 'fecha_fin']))
