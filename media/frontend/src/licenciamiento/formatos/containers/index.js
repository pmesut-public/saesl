import React, { Component } from 'react'
import { connect } from 'react-redux'

import format from 'date-fns/format'
import Formato from './Formato'
import { getFormatos } from '../reducer'
import { getOperadorView } from 'licenciamiento/ui'
import { updateFormato, removeFile } from '../actions'

import { Table, TableHeader } from 'common/components/table'

const headers = [
  {
    display: 'Informacion institucional',
    customStyle: { width: '23%', rowSpan: 2, style: {textAlign: 'center'} },
  },
  { display: 'Documento', customStyle: { width: '15%', rowSpan: 2 } },
  { display: 'Estado', customStyle: { width: '8%', rowSpan: 2, style: {textAlign: 'center'} } },
  { display: 'Periodo de vigencia', customStyle: { width: '25%', colSpan: 3, style: {textAlign: 'center'} } },
  {
    display: `Estado act. al: ${format(new Date(), 'DD/MM/YYYY')}`,
    customStyle: { width: '15%', colSpan: 2 },
  },
]

export const subHeaders = [
  { display: 'Periodo de vigencia', customStyle: { colSpan: 2 , style: {textAlign: 'center'}} },
  { display: '#DA', customStyle: { width: '5%',style: {textAlign: 'center'} } },
  { display: '#ED', customStyle: {style: {textAlign: 'center'}} },
  { display: '#A', customStyle: {style: {textAlign: 'center'}} },
]

const headersOperador = [
  { display: 'Información institucional' },
  { display: 'Documento' },
  { display: 'Calidad' },
  { display: 'Vigencia' },
]

class Formatos extends Component {
  render() {
    const {
      tipo,
      updateFormato,
      formatos,
      title,
      removeFile,
      operadorView,
    } = this.props

    return (
      <div className={`formatos`}>
        <h3 className="text-center">Formatos de Licenciamiento {tipo}</h3><br/>
        <Table bordered>
          {operadorView ? (
            <thead>
              <TableHeader.Row headers={headersOperador} />
            </thead>
          ) : (
            <thead>
              <TableHeader.Row headers={headers} />
              <TableHeader.Row headers={subHeaders} />
            </thead>
          )}
          <tbody>
            {formatos.map(f => (
              <Formato
                operadorView={operadorView}
                formato={f}
                key={f.get('for_id')}
                onChange={updateFormato}
                removeFile={removeFile}
                tipo={tipo}
              />
            ))}
          </tbody>
        </Table>
      </div>
    )
  }
}

const mapStateToProps = (state, { tipo }) => ({
  formatos: getFormatos(state, tipo),
  operadorView: getOperadorView(state),
})

const mapDispatchToProps = { updateFormato, removeFile }

export default connect(mapStateToProps, mapDispatchToProps)(Formatos)
