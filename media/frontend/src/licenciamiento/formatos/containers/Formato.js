import React, { Component } from 'react'
import { Modal } from 'react-bootstrap'
import ModalReact from 'react-modal'
import Button from 'common/components/buttons'
import _ from 'lodash'
import differenceInDays from 'date-fns/difference_in_days'

import * as messages from 'licenciamiento/messages'
import FileStatusPicker from 'common/components/FileStatusPicker'
import RangeDatePicker from 'common/components/Form/RangeDatePicker'
import Status from 'common/components/Status'
import FilePreview from '../../components/FilePreview'
import StatusBadge from 'common/components/StatusBadge'
import FileUpload from '../../containers/FileUpload'
import { urls } from 'licenciamiento/api'
import { SelectField, InputField } from 'common/components/Form'
import { getContext } from 'common/context'

const bg = {
  '': '',
  '0': 'bg-warning',
  '1': 'bg-success',
}

const letter = {
  '0': 'P',
  '1': 'C',
}

export const getEstadoLetter = estado => letter[estado]

const styleLetter = {
  position: 'absolute',
  color: 'black',
  left: '50%',
  top: '50%',
  transform: 'translate(-50%, -50%)',
}

export const vigenciaTitle = {
  green: 'Vigente',
  yellow: 'Próximo a perder Vigencia',
  red: 'Fuera de vigencia',
}

export const getConcluidoEstadoId = () => getContext().estados[1].id

export const getVigenciaValues = formato => {
  const { fecha_inicio, fecha_fin, da } = formato

  const res = {
    dv: '-',
    ed: '-',
    a: '',
  }
  if (!fecha_inicio || !fecha_fin) {
    return res
  }
  res.dv = differenceInDays(fecha_fin, fecha_inicio)
  res.ed = res.dv - differenceInDays(new Date(), fecha_inicio)
  console.log(res.dv, res.ed, fecha_fin, fecha_inicio, da)
  res.a =
    res.ed > +(da || 0)
      ? 'green'
      : res.ed <= da && res.ed > 0 ? 'yellow' : 'red'
  return res
}

class Formato extends Component {
  state = {
    isOpenModal: false,
  }
  onSelect = ({ target }) => {
    const { tipo, formato } = this.props
    this.onChange('estado', target.value)
  }

  onChange = (prop, value) => {
    const { tipo, formato } = this.props
    this.props.onChange(tipo, formato.get('for_id'), prop, value)
  }

  onRangeChange = (fechaInicio, fechaFin) => {
    this.onChange('fecha_inicio', fechaInicio)
    this.onChange('fecha_fin', fechaFin)
  }

  onFileUploaded = ({ docu_id, display, path }) => {
    const { tipo, formato } = this.props
    this.onChange('path', path)
    this.onChange('path', path)
    this.onChange('file_name', display)
    this.onChange('docu_id', docu_id)
    this.onChange('fecha_inicio', '')
    this.onChange('fecha_fin', '')
    this.onChange('da', null)
    this.onChange('estado', this.estados.estado)
    this.onChange('sub_estado', this.estados.subEstado)
  }

  openModal = next => {
    this.callbackUploadFile = next
    this.setState({
      isOpenModal: true,
    })
  }

  uploadFile = estados => {
    this.estados = estados
    this.closeModal()
    this.callbackUploadFile(
      _.mapKeys(_.pick(estados, ['estado', 'subEstado']), (v, k) =>
        _.snakeCase(k)
      )
    )
  }

  closeModal = () => this.setState({ isOpenModal: false })

  removeFile = () => {
    const { tipo, formato } = this.props
    this.props.removeFile(tipo, formato.get('for_id'), formato.get('docu_id'))
  }

  renderModal = () => {
    const { isOpenModal } = this.state

    const modalStyles = {
      overlay: { zIndex: 10 },
      content: {
        maxWidth: '400px',
        maxHeight: '300px',
        left: '50%',
        transform: 'translateX(-50%)',
      },
    }

    return (
      <ModalReact style={modalStyles} isOpen={isOpenModal} contentLabel="Modal">
        <Modal.Header>
          <Modal.Title>Seleccionar estado del documento</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {isOpenModal && (
            <FileStatusPicker
              estados={getContext().estados}
              subEstados={getContext().sub_estados}
              onCancel={this.closeModal}
              onSubmit={this.uploadFile}
            />
          )}
        </Modal.Body>
      </ModalReact>
    )
  }
  onBeforeReplace = () => {
    if (!confirm(messages.CONFIRM_REPLACE_FILE)) {
      return
    }
    this.button.click()
  }

  render() {
    const { formato, operadorView: vistaOperador } = this.props
    const {
      display,
      estado,
      docu_id,
      path,
      file_name,
      loading,
      sub_estado,
      fecha_inicio,
      fecha_fin,
      da,
    } = formato
    const rangeSelected = fecha_inicio && fecha_fin
    const vigencia = getVigenciaValues(formato)
    const statusPending = estado != getConcluidoEstadoId()

    return (
      <tr>
        <td className={bg[estado ? '' + estado : '']}>{display}</td>
        <td>
          {this.renderModal()}
          {vistaOperador && docu_id ? (
            <FilePreview hideRemove fileName={file_name} path={path} />
          ) : null}
          {!vistaOperador &&
            (docu_id ? (
              <FilePreview
                onRemove={this.removeFile}
                fileName={file_name}
                path={path}
              >
                <span style={{ position: 'relative' }}>
                  <FileUpload
                    onBeforeUpload={this.openModal}
                    disabled={loading}
                    url={urls.addDocumento}
                    onSuccess={this.onFileUploaded}
                    innerRef={ref => (this.button = ref)}
                    params={{
                      id: formato.get('for_id'),
                      tipo: 'FOR',
                      reemplazar: 1,
                    }}
                  />
                  <div
                    onClick={this.onBeforeReplace}
                    style={{
                      cursor: 'pointer',
                      position: 'absolute',
                      top: '-4px',
                      bottom: '-7px',
                      right: 0,
                      left: 0,
                      zIndex: 1,
                    }}
                  />
                </span>
              </FilePreview>
            ) : (
              <FileUpload
                onBeforeUpload={this.openModal}
                disabled={loading}
                url={urls.addDocumento}
                onSuccess={this.onFileUploaded}
                params={{
                  id: formato.get('for_id'),
                  tipo: 'FOR',
                  reemplazar: 0,
                }}
              />
            ))}
        </td>
        <td>
          {sub_estado && (
            <StatusBadge
              estado={sub_estado}
              list={getContext().sub_estados}
              style={{ margin: '0 auto' }}
            >
              <div style={styleLetter}>{letter[estado]}</div>
            </StatusBadge>
          )}
        </td>
        {!vistaOperador &&
          (!statusPending ? (
            <td>
              <RangeDatePicker
                onChange={this.onRangeChange}
                startTime={fecha_inicio}
                endTime={fecha_fin}
                disabled={!docu_id}
              />
            </td>
          ) : (
            <td />
          ))}

        {!vistaOperador && (!statusPending ? <td>{vigencia.dv}</td> : <td />)}
        {!vistaOperador &&
          (!statusPending ? (
            <td>
              <InputField
                value={da || ''}
                onChange={val => this.onChange('da', val)}
                pattern={'^[0-9]+$'}
                disabled={!rangeSelected}
                required
              />
            </td>
          ) : (
            <td />
          ))}
        {!vistaOperador && (!statusPending ? <td>{vigencia.ed}</td> : <td />)}
        <td>
          {vigencia.a && (
            <Status
              status={vigencia.a}
              style={{ margin: '0 auto' }}
              title={vigenciaTitle[vigencia.a]}
            />
          )}
        </td>
      </tr>
    )
  }
}

export default Formato
