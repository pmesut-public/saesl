import constants from 'licenciamiento/constants'
const { UPDATE_FORMATO, ALL_STORED } = constants
import * as messages from 'licenciamiento/messages'

export const updateFormato = (tipo, id, prop, value) => ({
  type: UPDATE_FORMATO,
  tipo,
  id,
  prop,
  value,
})

export const removeFile = (tipo, id, docu_id) => (
  dispatch,
  getState,
  { api }
) => {
  if (!confirm(messages.CONFIRM_REMOVE_FILE)) {
    return
  }
  dispatch(updateFormato(tipo, id, 'loading', true))
  api.removeDocumento({
    data: {
      docu_id,
      id,
      tipo: 'FOR',
    },
    onSuccess: () => {
      dispatch(updateFormato(tipo, id, 'loading', false))
      dispatch(updateFormato(tipo, id, 'path', null))
      dispatch(updateFormato(tipo, id, 'file_name', null))
      dispatch(updateFormato(tipo, id, 'docu_id', null))
      dispatch(updateFormato(tipo, id, 'docu_id', null))
      dispatch(updateFormato(tipo, id, 'estado', null))
      dispatch(updateFormato(tipo, id, 'sub_estado', null))
      dispatch(updateFormato(tipo, id, 'fecha_inicio', ''))
      dispatch(updateFormato(tipo, id, 'fecha_fin', ''))
      dispatch(updateFormato(tipo, id, 'da', null))
    },
    onError: err => {
      console.log(err)
      dispatch(updateFormato(tipo, id, 'loading', false))
    },
  })
}
