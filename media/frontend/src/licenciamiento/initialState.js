import { fromJS } from 'immutable';
import keyBy from 'lodash/keyBy';
import _groupBy from 'lodash/groupBy';

import reducer from './reducer';
import { getVigenciaValues } from './formatos/containers/Formato';

const getCondId = c => c.cond_id;
const getFormatoId = tipo => item => `${item.for_id}_${tipo}`;
const getFormatoIdTypeA = getFormatoId('A');
const getFormatoIdTypeC = getFormatoId('C');

const getColorEstado = average => {
  if (average < 0) return 'red';
  if (average < 1) return 'yellow';
  return 'green';
};

const colors = {
  red: -1,
  yellow: 0,
  green: 1,
};

const compare = (va, va2) => {
  if (va2 == '') return false;
  if (va == null) {
    return true;
  }
  return colors[va] > colors[va2];
};

export default (state, context) => {
  let data = fromJS(reducer(undefined), {});

  if (state.lic_a) {
    data = data
      .setIn(['formatos', 'ids'], fromJS(state.lic_a.map(getFormatoIdTypeA)))
      .setIn(
        ['formatos', 'items'],
        fromJS(keyBy(state.lic_a, getFormatoIdTypeA))
      );
  }

  if (state.lic_c) {
    data = data
      .updateIn(['formatos', 'ids'], (arr = fromJS([])) =>
        arr.concat(fromJS(state.lic_c.map(getFormatoIdTypeC)))
      )
      .updateIn(['formatos', 'items'], (items = fromJS({})) =>
        items.merge(fromJS(keyBy(state.lic_c, getFormatoIdTypeC)))
      );
  }
  if (state.cond) {
    const docGrouped = _groupBy(context.auto_documentos, d => d.aume_id);
    state.cond = state.cond.map(c => {
      let estado = 0;
      let estadoContador = 0;
      let cumplen = 0;
      let noAplican = 0;
      let noCumplen = 0;
      let noEvaluado = 0;
      let docIncompletos = 0;
      let fileNames = {};
      let vigenciaA = null;

      context.ind.filter(ind => ind.cond_id == c.cond_id).forEach(indicador => {
        const detalle = context.auto_detalles.filter(
          de => de.esta_id == indicador.ind_id
        )[0];

        const siCumple =
          detalle && detalle.aude_estado == context.estado_indicadores[1].id;
        if (siCumple) {
          cumplen++;
        }

        const noCumple =
          detalle && detalle.aude_estado == context.estado_indicadores[0].id;
        if (noCumple) noCumplen++;

        const noAplica =
          detalle && detalle.aude_estado == context.estado_indicadores[2].id;
        if (noAplica) {
          noAplican++;
        }

        const noEstado =
          detalle &&
          (detalle.aude_estado == '' ||
            detalle.aude_estado == null ||
            detalle.aude_estado == undefined);
        if (noEstado) noEvaluado++;

        if (context.auto_medio && detalle) {
          context.auto_medio
            .filter(m => m.aude_id == detalle.aude_id)
            .forEach(m => {
              const hasDocumentos = docGrouped[m.aume_id];

              const cumpleId = context.estados[1].id;
              if (hasDocumentos) {
                docGrouped[m.aume_id]
                  .filter(d => d.audo_estado == cumpleId)
                  .forEach(d => {
                    let vigA = getVigenciaValues(d).a;
                    if (compare(vigenciaA, vigA)) {
                      vigenciaA = vigA;
                    }

                    estado +=
                      colors[
                        _.find(
                          context.sub_estados,
                          s => s.id == d.audo_subestado
                        ).color
                      ];
                    estadoContador++;
                  });
              }

              hasDocumentos &&
                hasDocumentos.forEach(d => {
                  const name = d.display;
                  if (!(name in fileNames)) {
                    fileNames[name] = 0;
                  }
                  fileNames[name]++;
                });

              docIncompletos += hasDocumentos
                ? docGrouped[m.aume_id].filter(
                    d => d.audo_estado == context.estados[0].id
                  ).length
                : 0;
            });
        }
      });

      return {
        ...c,
        vigenciaA,
        cumplen,
        noCumplen,
        noEvaluado,
        noAplican,
        docIncompletos,
        repetidos: _.values(fileNames).filter(v => v > 1).length > 0,
        condicionEstado:
          estadoContador == 0 ? '' : getColorEstado(estado / estadoContador),
      };
    });
    data = data
      .setIn(['condiciones', 'ids'], fromJS(state.cond.map(getCondId)))
      .setIn(['condiciones', 'items'], fromJS(keyBy(state.cond, getCondId)));
  }

  console.log('licenciamiento', data.toJS());
  return data;
};
