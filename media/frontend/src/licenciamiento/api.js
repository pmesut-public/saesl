import request from 'common/request';

export const urls = {
  saveAll: '/formatos/save',
  finish: '/formatos/finish',
  addDocumento: '/formatos/documento_new',
  removeDocumento: '/formatos/documento_remove',
  autoevaluacionStatus: '/formatos/autoevaluacion_status',
  getEstadoFichas: '/formatos/estado_fichas',
};

const createRequest = url => props => request({ ...props, url });

export const saveAll = createRequest(urls.saveAll);
export const finish = createRequest(urls.finish);
export const addDocumento = createRequest(urls.addDocumento);
export const removeDocumento = createRequest(urls.removeDocumento);
export const autoevaluacionStatus = createRequest(urls.autoevaluacionStatus);
export const getEstadoFichas = createRequest(urls.getEstadoFichas);
