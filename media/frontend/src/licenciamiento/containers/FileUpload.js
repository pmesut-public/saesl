import React, { Component } from 'react';

class FileUpload extends Component {
  state = {
    uploading: false,
  }
  componentDidMount() {
    this.activatePlugin();
  }
  componentWillUnmount() {
    this.removePlugin();
  }

  activatePlugin() {
    this.settings = {
      url: this.props.url,
      browse_button: this.button,
      multi_selection: false,
    };

    this.uploader = new plupload.Uploader(this.settings);
    this.uploader.bind('FilesAdded', this.onFilesAdded);
    this.uploader.bind('FileUploaded', this.onFileUploaded);
    this.uploader.bind('Error', this.onError);
    this.uploader.init();
  }

  removePlugin() {
    this.uploader.unbind('FilesAdded', this.onFilesAdded);
    this.uploader.unbind('FileUploaded', this.onFileUploaded);
    this.uploader.unbind('Error', this.onError);
    this.uploader.destroy();
    this.uploader = null;
  }

  uploadFile = (extraParams = {}) => {
    this.setState({ uploading: true });
    this.uploader.setOption('multipart_params', {...this.props.params, ...extraParams});
    this.uploader.start();
  }

  onFilesAdded = (_, files) => {
    console.log('files, added', this.props);
    if (this.props.onBeforeUpload) {
      this.props.onBeforeUpload(this.uploadFile);
    } else {
      this.uploadFile();
    }
  }

  onFileUploaded = (fileUploader, _, res) => {
    this.setState({ uploading: false });
    let {
      status,
      msg,
      ...rest,
    } = JSON.parse(res.response);
    // id,
    // display: name,
    // path,
    // msg,
    // status,

    if (status === 'OK') {
      return this.props.onSuccess(rest);
    }
    alert(msg);
  };

  onError = (a, b, c) => {
    this.setState({ uploading: false });
    console.log(a, b, c);
    // this.props.onSuccess({
    //   docu_id: '1',
    //   display: 'nuevo file.pdf',
    //   path: 'path/gg',
    // });
  }

  setRef = (e) => {
    this.button = e;
    if (this.props.innerRef) {
      this.props.innerRef(e);
    }
  }

  render() {
    const { uploading } = this.state;
    const { title = "Subir archivo", innerRef } = this.props;
    return (
      <button
        title={title}
        type="button"
        disabled={uploading}
        ref={this.setRef}
        className="btn btn-primary btn-flat" 
      >
      {
        uploading ?
          <i className='fa fa-refresh fa-animate' />:
          <i className='fa fa-upload' />
      }
      </button>
    );
  }
}

export default FileUpload;
