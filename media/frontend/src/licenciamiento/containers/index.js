import React, { Component } from 'react'
import { connect } from 'react-redux'

import Button from 'common/components/buttons'
import classNames from 'classnames'
import { getOperadorView } from 'licenciamiento/ui'
import { getContext } from 'common/context'
import { messageLeavePage } from '../messages'
import connectOnLeavePage from 'common/hoc/connectOnLeavePage'
import SystemStatus from './SystemStatus'
import { getSummaryStatus } from '../reducer'
import DatosInstitucionales from '../components/DatosInstitucionales'
import Formatos from '../formatos/containers'
import Condiciones from '../condiciones/containers'
import { toggleOperadorView } from 'licenciamiento/ui'
import { saveAll } from '../actions'

class App extends Component {
  componentDidMount() {
    const TIME = 1000 * 60 * 15;
    const {saveAll} = this.props;
    setInterval(saveAll, TIME)
  }
  render() {
    const props = this.props;

    return (
      <div className="medicion container-fluid">
        <SystemStatus />
        <DatosInstitucionales
          institucion={getContext().institucion}
          modalidad={getContext().modalidad}
          gestion={getContext().gestion}
          region={getContext().region}
        />
        <hr />
        <div className="row">
          <div
            className={`btn-group ${props.operadorView ? 'col-lg-12' : 'col-lg-12'}`}
          >
            <Button
              primary
              extraClass={classNames({ active: !props.operadorView })}
              disabled={!props.operadorView}
              onClick={props.toggleOperadorView}
            >
              Vista operador
            </Button>
            <Button
              primary
              extraClass={classNames({ active: props.operadorView })}
              disabled={props.operadorView}
              onClick={props.toggleOperadorView}
            >
              Vista detalle
            </Button>
          </div>
        </div>
        {props.operadorView && (
          <div className="row">
            <div className="col-lg-3 col-md-12">
              <Formatos tipo="A" />             
            </div>
            <div className="col-lg-6 col-md-12">
              <Condiciones />              
            </div>
            <div className="col-lg-3 col-md-12">
              <Formatos tipo="C" />
            </div>
          </div>
        )}
        {!props.operadorView && (
          <div className="row">
            <div className="col-md-12">
              <Formatos tipo="A" />
            </div>
          </div>
        )}
        {!props.operadorView && (
          <div className="row">
            <div className="col-md-12">
              <Condiciones />
            </div>
          </div>
        )}
        {!props.operadorView && (
          <div className="row">
            <div className="col-md-12">
              <Formatos tipo="C" />
            </div>
          </div>
        )}
        <SystemStatus />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isFetching: getSummaryStatus(state) !== 'ALL_STORED',
  operadorView: getOperadorView(state),
})

export default connect(mapStateToProps, { toggleOperadorView, saveAll })(
  connectOnLeavePage(App, messageLeavePage)
)
