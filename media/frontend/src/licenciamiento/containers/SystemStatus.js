import React from 'react';
import { connect } from 'react-redux';

import { getContext } from 'common/context';
import Button from 'common/components/buttons';
import { getSummaryStatus } from '../reducer';
import { saveAll, closeFicha, closeEvento, cancelEvento } from '../actions';
import { isFetching, getErrors } from 'autoevaluacion/tablaAuto/reducer';

const StatusBadges = ({ status }) => {
  let attr = {
    background: null,
    icon: null,
    title: null,
  };

  switch (status) {
    case 'ALL_STORED':
      attr = {
        background: 'green',
        icon: 'fa-save',
        title: 'Guardado',
      };
      break;

    case 'ALL_HAS_CHANGED':
      attr = {
        background: 'yellow',
        title: 'Pendiente',
        icon: 'fa-exclamation',
      };
      break;

    case 'ALL_FETCHING':
      attr = {
        background: 'yellow',
        title: 'Guardando...',
        icon: 'fa-spin fa-spinner',
      };
      break;

    case 'ALL_ERROR':
      attr = {
        background: 'red',
        title: 'Error al guardar',
        icon: 'fa-exclamation-triangle',
      };
  }

  return (
    <label className={`badge bg-${attr.background}`}>
      <i title={attr.title} className={`fa ${attr.icon}`} />
    </label>
  );
};

const displayBlock = {
  display: 'block',
};

const onClickWTF = e => {
  $('.menu').toggleClass('d-hidden');
  $('.menuWrapper').addClass('d-hidden');
  /*
  var tag = document.getElementById(id).classList.contains('d-hidden')
  if (tag) {
    document.getElementById(id).classList.remove('d-hidden')
  } else {
    document.getElementById(id).classList.add('d-hidden')
  }
  */
};

const SystemStatus = ({
  summaryStatus,
  saveAll,
  closeFicha,
  closeEvento,
  leave,
  close,
  isFetching,
  errors,
  goToReporte,
  goToRepositorio,
  goToReporteVigenciaABC,
  goToReporteDocumentosHistoricos,
  goToReporteMV,
  cancelarAuto,
  cancelarEvento,
}) => (
  <div className="well system-status status-formatos">
    <div className="menuWrapper d-hidden">
      <a className="btn btn-success" onClick={onClickWTF}>
        Menú
      </a>
    </div>
    <div className="menu  clearfix"> {/** d-hidden */}
      <Button
        danger
        extraClass="pull-left"
        style={{ marginRight: '5px' }}
        onClick={cancelarEvento}
      >
        Cancelar Evento
      </Button>
      <Button danger onClick={cancelarAuto} disabled={isFetching}>
        Cancelar Autoevaluación de formatos A,B,C
      </Button>

      <div className="pull-right">
        <div>
          <div className="pull-right status-ficha">
            Estado de la ficha: <StatusBadges status={summaryStatus} />
          </div>
          <Button
            extraClass="pull-right"
            style={{ marginRight: '5px' }}
            warning
            onClick={leave}
            disabled={isFetching}
          >
            Volver a la autoevaluación
          </Button>
          <Button
            extraClass="pull-right"
            style={{ marginRight: '5px' }}
            success
            onClick={saveAll}
            disabled={isFetching}
          >
            Guardar sin terminar
          </Button>
          <Button
            extraClass="pull-right"
            style={{ marginRight: '5px' }}
            primary
            onClick={closeFicha}
            disabled={isFetching}
          >
            Finalizar ficha
          </Button>
          <Button
            extraClass="pull-right"
            style={{ marginRight: '5px' }}
            primary
            onClick={closeEvento}
            disabled={isFetching}
          >
            Finalizar Evento
          </Button>
        </div>
      </div>
    </div>
    <div className="row" style={{ margin: '5px 0' }}>
      <Button
        primary
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        onClick={goToRepositorio}
      >
        Repositorio
      </Button>
      <Button
        primary
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        onClick={goToReporte}
      >
        Reporte General
      </Button>
      <Button
        primary
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        onClick={goToReporteDocumentosHistoricos}
      >
        Documentos Históricos
      </Button>
      <Button
        primary
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        onClick={goToReporteVigenciaABC}
      >
        Vigencia - A-B-C
      </Button>
      <Button
        primary
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        onClick={goToReporteMV}
      >
        Vigencia - MV
      </Button>
    </div>
    {(errors[0] || errors[1] || errors[2]) && (
      <div className="alert alert-danger">
        {errors[0] && (
          <div>
            Faltan llenar datos de vigencia en los formatos (ver vista
            Operador): {errors[0]}
          </div>
        )}
        {errors[1] && (
          <div>
            Debe agregar un documento en los siguientes formatos {errors[1]}
          </div>
        )}
        {errors[2] && (
          <div>
            Existen errores en los datos de vigencia en los formatos (ver vista
            Operador): {errors[2]}
          </div>
        )}
      </div>
    )}
  </div>
);

const mapStateToProps = state => ({
  summaryStatus: getSummaryStatus(state),
  isFetching: isFetching(state.get('system_status')),
  errors: getErrors(state.get('system_status')),
  cancelarAuto: () => {
    if (!confirm('¿Está seguro que desea cancelar la autoevaluación?')) return;
    window.location.href = '/formatos/cancelar_autoevaluacion';
  },

  leave: () => {
    window.location.href = '/autoevaluacion/calculator';
  },
  goToReporte: () => {
    window.location.href = '/formatos/reporte';
  },
  goToRepositorio: () => {
    window.location.href = '/documentos'
  },
  goToReporteVigenciaABC: () => {
    window.location.href = '/formatos/documentosformatos';
  },
  goToReporteDocumentosHistoricos: () => {
    window.location.href = '/formatos/documentoshistoricos';
  },
  goToReporteMV: () => {
    window.location.href = '/formatos/documentosmedioverificacion';
  },
});

const mapDispatchToProps = dispatch => ({
  saveAll: () => dispatch(saveAll()),
  closeFicha: () => {
    if (!confirm('¿Está seguro que desea finalizar formatos?')) return;
    dispatch(closeFicha());
  },
  closeEvento: () => {
    if (!confirm('¿Está seguro que desea finalizar el evento?')) return;
    dispatch(closeEvento());
  },
  cancelarEvento: () => {
    if (
      !confirm(`Para cancelar el evento deberá usted previamiente:
      - Haber cancelado la Autoevaluación de las Condiciones Básicas de Calidad
      - Haber cancelado la Autoevaluación de los formatos A,B,C
    `)
    )
      return;
    if (!confirm('¿Está seguro que desea cancelar el evento?')) return;
    dispatch(cancelEvento('/formatos/cancelar_evento'));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SystemStatus);
