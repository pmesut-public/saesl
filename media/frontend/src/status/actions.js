import constants from './constants';
const {
  STATUS_FETCHING,
  STATUS_SUCCESS,
  STATUS_ERROR,
  STATUS_STORED,
} = constants;

export const statusFetching = (id) => ({
  type: STATUS_FETCHING,
  id,
});

export const statusSuccess = () => ({
  type: STATUS_SUCCESS,
});

export const statusError = (error) => ({
  type: STATUS_ERROR,
  error,
});

export const statusStored = () => ({
  type: STATUS_STORED,
});

export const onSuccess = (dispatch, _, cb = () => {}) => {
  dispatch(statusSuccess());
  console.log('ok');
  cb();
};

export const onError = (dispatch, error) => {
  console.log('err', error);
  dispatch(statusError(error));
};
