import keyMirror from 'keymirror';

const constants = keyMirror({
  STATUS_FETCHING: null,
  STATUS_SUCCESS: null,
  STATUS_ERROR: null,
  STATUS_STORED: null,

  STORED: null,
  ERROR: null,
  SUCCESS: null,
  FETCHING: null,
});

export default constants;
