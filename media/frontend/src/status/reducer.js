import { fromJS } from 'immutable';
import { createReducer } from 'redux-immutablejs';

import constants from './constants';
const {
  STATUS_FETCHING,
  STATUS_SUCCESS,
  STATUS_ERROR,
  STATUS_STORED,

  STORED,
  ERROR,
  SUCCESS,
  FETCHING,
} = constants;

const initialState = fromJS({
  state: STORED,
  errors: [],
  id: null,
});

// TODO refactor
export default createReducer(initialState, {
  [STATUS_ERROR]: (state, { error }) =>
    state
      .set('state', ERROR)
      .set('errors', [error]),

  [STATUS_FETCHING]: (state, { id }) =>
    state
      .set('state', FETCHING)
      .set('id', id),

  [STATUS_SUCCESS]: (state) => state.set('state', SUCCESS),

  [STATUS_STORED]: (state) => state.set('state', STORED),
});

export const isFetching = (state) => state.get('state') === FETCHING;

export const isCurrentActionFetching = (state, id) =>
  isFetching(state) &&
    state.get('id') === id;

export const getStatusFromKey = (state, key) =>
  state.getIn([key, 'ids'])
  .map(id => fromJS({
    hasChanged: state.getIn([key, 'items', id, 'hasChanged'])
  }));

