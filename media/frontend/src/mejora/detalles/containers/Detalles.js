import React, { Component } from 'react';
import { connect } from 'react-redux';
import _filter from 'lodash/filter';
import _map from 'lodash/map';

import { getContext } from 'common/context';
import { Table, TableHeader } from 'common/components/table';

const headers = [
  {display: 'ESTANDAR', customStyle: {width: '30%'}},
  {display: 'CRITERIOS A EVALUAR', customStyle: {width: '60%'}},
];

const getCriterios = aude_id =>
  _filter(getContext().auto_criterios, c => c.aude_id == aude_id);

const Detalles = ({display, descripcion, aude_id, mede_id}) => (
        <div>
          <h4><strong>{display}</strong></h4>
          {descripcion}
        {
          _map(getCriterios(aude_id), c => (
              <p key={c.aucr_id}>{c.descripcion}</p>
          ))
        }
        </div>
);

const mapStateToProps = () => ({});

export default connect(mapStateToProps)(Detalles);
