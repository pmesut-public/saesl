import { fromJS } from 'immutable';
import { createReducer } from 'redux-immutablejs';
import _pick from 'lodash/pick';

import { getContext } from 'common/context';
import constants from '../constants';
const {
  UPDATE_PROP,
  ALL_STORED,
} = constants;

const initialState = fromJS({});

export default createReducer(initialState, {
  [UPDATE_PROP]: (state, {id, prop, value}) => {
    if (prop === 'mede_estado') {
      return state
        .setIn([id, 'hasChanged'], true)
        .setIn([id, prop], value)
        .setIn([id, 'mede_nivel'], '')
        .setIn([id, 'mede_actividades'], '')
        .setIn([id, 'mede_impacto'], '');
    }

    return state
      .setIn([id, 'hasChanged'], true)
      .setIn([id, prop], value);
  },

  [ALL_STORED]: (state) =>
    state.map(v => v.set('hasChanged', false))
});

export const getDetalleEstado = (state, id) => state.getIn([id, 'mede_estado']);

export const getEstadoIndex = (state, id) =>  {
  let index = -1;
  const estadoId = getDetalleEstado(state, id);

  for (let i = 0, len = getContext().estados.length; i < len; i++) {
    if (getContext().estados[i].id == estadoId) {
      index = i + 1;
    }
  }
  return index;
}

export const getDetalleNivel = (state, id) => state.getIn([id, 'mede_nivel']);

export const getDetalle = (state, id) => state.get(id);

export const getStatusDetalles = (state) => {
  const mutableState = state.toJS();
  return Object.keys(mutableState).map(k => _pick(mutableState[k], 'hasChanged'));
}

export const getPayloadDetalles = state => {
  const mutableState = state.toJS();

  return Object.keys(mutableState)
    .filter(k => mutableState[k].hasChanged)
    .map(k => _pick(mutableState[k], ['esta_id', 'mede_id', 'mede_estado',
                                      'mede_nivel', 'mede_actividades', 'mede_impacto'])
    );
}
