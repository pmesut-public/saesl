import _ from 'lodash';

import constants from '../constants';
const {
  UPDATE_PROP,
} = constants;

export const updateProp = (id, prop, value) => ({
  type: UPDATE_PROP,
  id,
  prop,
  value
});

export const updateEstado = _.partial(updateProp, _, 'mede_estado', _);
export const updateNivel = _.partial(updateProp, _, 'mede_nivel', _);
export const updateActividades = _.partial(updateProp, _, 'mede_actividades', _);
export const updateImpacto = _.partial(updateProp, _, 'mede_impacto', _);
