import request from 'common/request';

const urls = {
  save: '/mejora/save',
};

const createRequest = (url) => (props) => request({...props, url});

export const saveAll = createRequest(urls.save);
