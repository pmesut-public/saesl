import React from 'react';

import flatMap from 'lodash/flatMap';
import map from 'lodash/map';
import groupBy from 'lodash/groupBy';

import { getContext } from 'common/context';
import { Table, TableHeader } from 'common/components/table';
import DimensionRow from '../containers/DimensionRow';
import FactorRow from '../containers/FactorRow';
import EstandarRow from '../containers/EstandarRow';
import ConditionalRow from '../containers/ConditionalRow';

const headers = [
  {display: 'ESTANDAR', customStyle: {width: '30%' }},
  {display: 'CRITERIOS A EVALUAR', customStyle: {width: '45%' }},
  {display: 'ESTADO', customStyle: {width: '25%' }},
];

const Body = ({list, cond = false}) => (
  <tbody>
    {flatMap(list, (e, iE) => [
        <EstandarRow key={e.id} {...e} />,
        <ConditionalRow key={e.id + '1'} {...e}/>
    ])}
  </tbody>
);

const MejoraBody = () => {
  const dimensiones = getContext().dimensiones;
  const factoresGrouped = groupBy(getContext().factores, f => f.dime_id);
  const estandaresGrouped = groupBy(getContext().estandares, e => e.fact_id);
  return (
      <tbody>
      {
        flatMap(dimensiones, (d, iD) => [
            <DimensionRow {...d} key={d.id} />,
          ...flatMap(factoresGrouped[d.id], (f, iF) => [
              <FactorRow {...f} key={d.id +''+f.id} />,
              <tr key={d.id + '' + f.id+ '1'}><td colSpan="3">
              <Table bordered >
                <TableHeader headers={headers} className="estandar__table"/>
                 <Body list = {estandaresGrouped[f.id]} />
              </Table>
              </td></tr>
          ])
        ])
      }
    </tbody>
  );
};

export default MejoraBody;
