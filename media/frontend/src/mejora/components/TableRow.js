import React from 'react';

const TableRow = ({display, descripcion, valoracion}) => (
  <tr>
    <td colSpan="2">
      <h4><strong>{display}</strong></h4>
      {descripcion}
    </td>
    <td>{valoracion}</td>
  </tr>
);

export default TableRow;
