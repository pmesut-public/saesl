import React from 'react';

import { Table, TableHeader } from 'common/components/table';
import MejoraBody from './MejoraBody';

const headers = [
  {display: 'Dimensión', customStyle: {width: '5%'}},
  {display: 'Factor', customStyle: {width: '70%'}},
  {display: 'Estado', customStyle: {width: '25%'}},
];

const TableMejora = () => (
  <Table bordered>
    <TableHeader headers={headers}/>
    <MejoraBody />
  </Table>
);

export default TableMejora;
