import constants from './constants';
const {
  SET_ERRORS,
  ALL_STORED,
  ALL_FETCHING,
  ALL_ERROR,
  FINISHED_REQUEST,
} = constants;

import { getPayloadDetalles } from './detalles/reducer';

import { formatEstandarShortName } from 'common/formatter';
import { getContext } from 'common/context';

const addOverlay = () => {
  var overlay = document.createElement('div');
  overlay.id = 'auto_overlay';
  var inner = document.createElement('div');
  inner.id = 'auto_inner_overlay';
  inner.innerHTML = '<div><i class="fa fa-refresh fa-animate"></i> Guardando</div>';
  overlay.appendChild(inner);
  document.querySelector('body').appendChild(overlay);
}

export const setErrors = (errors) => ({
  type: SET_ERRORS,
  errors,
});

export const allFetching = (id) => ({
  type: ALL_FETCHING,
  id,
});

export const allError = (msg) => ({
  type: ALL_ERROR,
  msg,
});

const getPayload = (getState) => ({
  mejo_detalles: getPayloadDetalles(getState().get('mejo_detalles'))
})

const removeOverlay = () => {
  const elem = document.getElementById('auto_overlay');
  elem.parentElement.removeChild(elem);
};

const allSaveError = (msg) => dispatch => {
  removeOverlay();
  dispatch(allError(msg));
  alert(msg);
};


export const allStored = () => ({
  type: ALL_STORED,
});



export const saveAllData = (dispatch, getState, api, cb) => {
  const payload = getPayload(getState);

  const shouldNotSave = !payload.mejo_detalles.length;

  const inlineErrors = [...document.querySelectorAll('.estandar__row')]
          .map((item, i) =>
               ($(item).find('.bg-danger').length > 0 ||
                ( $(item).next().attr('class') === 'conditional__row' &&
                  $(item).next().find('.bg-danger').length > 0
                ) ) ?
               'Faltan llenar campos obligatorios': '');

  const errors = inlineErrors
    .map((err, i) => ({
      esta_id: getContext().estandares[i].id,
      title: 'Estándar ' + formatEstandarShortName(getContext().estandares[i]),
      errors: inlineErrors[i]
    }))
    .filter(e => e.errors.length);

  if (cb) {
    dispatch(setErrors(errors));
  }

  if (shouldNotSave && !cb) {
    return;
  }

  console.log('saveAll payload', payload);
  addOverlay();
  dispatch(allFetching());

  api.saveAll({
    data: payload,
    onSuccess: () => {
      removeOverlay();
      dispatch(allStored());
      if (cb) {
        cb(errors);
      }
    },
    onError: (err) => {
      console.log('err', err);
      dispatch(allSaveError('Ocurrio un error al guardar'));
    }
  });
}

export const saveAll = () => (dispatch, getState, {api}) => {
  saveAllData(dispatch, getState, api);
}

const finish = () => {
  var form = $(document.createElement('form'));
  $(form).attr("method", "POST");
  $(form).attr('action', '/mejora/finish');
  form.appendTo( document.body );
  $(form).submit();
};

export const closeFicha = () => (dispatch, getState, {api}) => {
  console.log('finish');
  saveAllData(dispatch, getState, api, (err) => {
    if (err.length) {
      return alert('Existen errores. No se puede finalizar la ficha');
    }
    finish();
  });
};
