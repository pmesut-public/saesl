import keyMirror from 'keymirror';

const constants = keyMirror({
  UPDATE_PROP: null,
  ALL_HAS_CHANGED: null,
  ALL_ERROR: null,
  ALL_FETCHING: null,
  ALL_STORED: null,
  SET_ERRORS: null,
  FINISHED_REQUEST: null,
});

export default constants;
