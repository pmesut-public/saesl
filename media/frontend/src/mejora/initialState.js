import { fromJS } from 'immutable';
import _keyBy from 'lodash/keyBy';

const addKeyAccess = (state, context) => {
  const autoDetallesKeys = _keyBy(context.auto_detalles, 'esta_id');
  const mejoDetallesKey = _keyBy(state.mejo_detalles, 'esta_id');
  const mapEstandarDetalle = context.estandares.reduce((acc, e) => {
    acc[e.id] = {
      aude_id: autoDetallesKeys[e.id].aude_id,
      mede_id: mejoDetallesKey[e.id].mede_id,
    }
    return acc;
  }, {});
  context.mapEstandarDetalle = mapEstandarDetalle;
  return context;
}

export default (state, context) => {
  let rawState = state;
  const updatedContext = addKeyAccess(state, context);
  const keys = ['mede_estado', 'mede_nivel', 'mede_actividades', 'mede_impacto'];
  rawState.mejo_detalles.forEach(d => {
    keys.forEach(k => {
      if (!d[k]) {
        d[k]= '';
      }
    })
  })
  rawState.mejo_detalles = _keyBy(rawState.mejo_detalles, 'mede_id');

  return {
    state: fromJS(rawState),
    context: updatedContext,
  };
}
