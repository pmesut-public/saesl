import React from 'react';
import { connect } from 'react-redux';

import { getContext } from 'common/context';
import Button from 'common/components/buttons';
import { getSummaryStatus } from '../reducer';
import { saveAll, closeFicha } from '../actions';
import { isFetching, getErrors } from 'autoevaluacion/tablaAuto/reducer';

const StatusBadges = ({
  status,
}) => {
  let attr = {
    background: null,
    icon: null,
    title: null,
  };

  switch (status) {
  case 'ALL_STORED':
    attr = {
      background: 'green',
      icon: 'fa-save',
      title: 'Guardado',
    };
    break;

  case 'ALL_HAS_CHANGED':
    attr = {
      background: 'yellow',
      title: 'Pendiente',
      icon: 'fa-exclamation',
    };
    break;

  case 'ALL_FETCHING':
    attr = {
      background: 'yellow',
      title: 'Guardando...',
      icon: 'fa-spin fa-spinner',
    };
    break;

  case 'ALL_ERROR':
    attr = {
      background: 'red',
      title: 'Error al guardar',
      icon: 'fa-exclamation-triangle',
    };
  }

  return (
    <label className={`badge bg-${attr.background}`}>
      <i title={attr.title} className={`fa ${attr.icon}`}></i>
    </label>
  );
};

const SystemStatus = ({
  summaryStatus,
  saveAll,
  closeFicha,
  leave,
  close,
  isFetching,
  errors,
}) => (
 <div className="well system-status">

    <div className="pull-right">
    Estado de la ficha: {" "}
    <StatusBadges status={summaryStatus} />
    </div>
    <Button
      danger
      onClick={close}
      disabled={isFetching}
    >Cancelar mejora</Button>
    <Button
      warning
      onClick={leave}
      disabled={isFetching}
    >Salir del proceso de mejora</Button>
    <Button
      success
      onClick={saveAll}
     disabled={isFetching}
    >Guardar sin terminar</Button>
    <Button
      primary
      onClick={closeFicha}
      disabled={isFetching}
    >Finalizar ficha</Button>{" "}
    {errors.length > 1 && (
    <div className="alert alert-danger">
        <div>No se pudo finalizar la ficha porque existen errores en las siguientes secciones: </div>
    {errors}
    </div>)
    }
 </div>
);



const mapStateToProps = (state) => ({
  summaryStatus: getSummaryStatus(state),
  isFetching: isFetching(state.get('system_status')),
  errors: getErrors(state.get('system_status')).map(e => e.title).join(',') + '.',
  leave: () => {
    window.location.href = '/mejora';
  },
  close: () => {
    if (!confirm('Esta seguro que desea cerrar la ficha?')) {
      return;
    }
    var form = $(document.createElement('form'));
    $(form).attr("method", "POST");
    $(form).attr('action', '/mejora/cancel');
    form.appendTo( document.body )
    $(form).submit();
  },
});

const mapDispatchToProps = {
  saveAll,
  closeFicha,
};

export default connect(mapStateToProps, mapDispatchToProps)(SystemStatus);
