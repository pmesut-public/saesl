import React, { Component} from 'react';
import { connect } from 'react-redux';

import TableRow from '../components/TableRow';

const FactorRow = (props) => (
  <TableRow {...props} />
);

const mapStateToProps = () => ({});

export default connect(mapStateToProps)(FactorRow);
