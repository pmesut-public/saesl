import React from 'react';
import { connect } from 'react-redux';

import { updateNivel, updateActividades, updateImpacto } from '../detalles/actions';
import { getEstadoIndex, getDetalle} from '../detalles/reducer';
import { getContext } from 'common/context';
import { SelectField, TextAreaField } from 'common/components/Form';
import { Table, TableHeader } from 'common/components/table';

const headers = [
  { display: 'Con las actividades que ha desarrollado hasta esta etapa, ¿qué nivel de cumplimiento del estándar ,en una escala de 1 a 10, considera que ha alcanzado? (revise la descripción del estándar):', customStyle: {width: '30%'} },
  { display: 'Del nivel que usted se ha calificado, ¿considera que le faltan (o sobran) actividades?', customStyle: {width: '35%'}},
  { display: 'De su evaluación actual, ¿la implementación de las actividades listadas tiene el impacto esperado en el cumplimiento del estándar?', customStyle: {width: '35%'}},
];

const option9Headers = [
  headers[0],
  { display: 'De todo lo avanzado ¿la implementación de las actividades listadas tiene el impacto esperado? ' }
];

const Row = ({ headers, children }) => (
  <tr className="conditional__row"><td colSpan="3">
    <Table bordered>
    <TableHeader headers={headers}/>
      <tbody>{children}</tbody>
    </Table>
  </td></tr>
);

const niveles = [];
for (let i = 1; i <=10;i++) {
  niveles.push({display: ''+i, id: ''+i})
}

const ConditionalRow = ({
  estadoIndex,
  detalle,
  updateNivel,
  updateActividades,
  updateImpacto
}) => {
  const SelectTd = (
    <td>
      <SelectField
        required
        options={niveles}
        value={detalle.get('mede_nivel')}
        onChange={updateNivel}
      />
    </td>
  );

  const ImpactoTd = (
    <td>
      <TextAreaField
        maxLength="20000"
        rows="6"
        required
        value={detalle.get('mede_impacto')}
        onChange={updateImpacto}
      />
    </td>
  );

  if (estadoIndex === 6) {
    return (
      <Row headers={headers}>
        <tr>
          {SelectTd}
        <td>
          <TextAreaField
            maxLength="20000"
            rows="6"
            required
            value={detalle.get('mede_actividades')}
            onChange={updateActividades}
          />
        </td>
          {ImpactoTd}
        </tr>
      </Row>
    );
  }
  if (estadoIndex === 9) {
    return (
      <Row headers={option9Headers}>
        <tr>
          { SelectTd }
          { ImpactoTd }
        </tr>
      </Row>
    );
  }
  return null;
}

const mapStateToProps = (state, { id }) => ({
  estadoIndex: getEstadoIndex(state.get('mejo_detalles'), getContext().mapEstandarDetalle[id].mede_id),
  detalle: getDetalle(state.get('mejo_detalles'), getContext().mapEstandarDetalle[id].mede_id),
});

const mapDispatchToProps = (dispatch, {id}) => {
  const mejoraId = getContext().mapEstandarDetalle[id].mede_id;
  return {
    updateNivel: (e) => dispatch(updateNivel(mejoraId, e.target.value)),
    updateActividades: (val) => dispatch(updateActividades(mejoraId, val)),
    updateImpacto: (val) => dispatch(updateImpacto(mejoraId, val)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConditionalRow);

