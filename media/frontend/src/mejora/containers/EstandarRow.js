import React, { Component } from 'react';
import { connect } from 'react-redux';
import _map from 'lodash/map';
import _filter from 'lodash/filter';

import { getContext } from 'common/context';
import { updateEstado } from '../detalles/actions';
import { SelectField } from 'common/components/Form';
import { getDetalleEstado } from '../detalles/reducer';


const getCriterios = aude_id =>
        _filter(getContext().auto_criterios, c => c.aude_id == aude_id);

const EstandarRow = ({ id, display, descripcion, updateEstado, estado, className }) => (
    <tr className={`estandar__row `}>
    <td className={className}>
    <h4><strong>{display}</strong></h4>
    {descripcion}
    </td>
    <td className={className}>
    {
      _map(getCriterios(getContext().mapEstandarDetalle[id].aude_id), c => (
          <p key={c.aucr_id}>{c.descripcion}</p>
      ))
    }
    </td>
    <td>
      <SelectField
        required
        options={getContext().estados}
        value={estado}
        onChange={updateEstado}
      />
    </td>
  </tr>
);

const mapStateToProps = (state, { id }) => {
  const estado = getDetalleEstado(state.get('mejo_detalles'), getContext().mapEstandarDetalle[id].mede_id);

  return {
    className: estado.length ? 'estado_picked': 'estado_missing',
    estado,
  }
};

const mapDispatchToProps = (dispatch, { id }) => ({
  updateEstado: (e) =>
    dispatch(updateEstado(getContext().mapEstandarDetalle[id].mede_id, e.target.value))
});

export default connect(mapStateToProps, mapDispatchToProps)(EstandarRow);
