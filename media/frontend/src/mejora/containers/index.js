import React, { Component } from 'react';
import { connect } from 'react-redux';

import connectOnLeavePage from 'common/hoc/connectOnLeavePage';
import Table from '../components/';
import SystemStatus from './SystemStatus';
import { getSummaryStatus } from '../reducer';
import { canNotLeave } from '../systemStatus';

export const messageLeavePage = 'Existen datos sin guardar. Desea salir?';

class App extends Component {
  render() {
    return (
      <div className="mejora">
        <SystemStatus />
        <Table />
        <SystemStatus />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  isFetching: getSummaryStatus(state) !== 'ALL_STORED',
});

export default connect(mapStateToProps)(connectOnLeavePage(App, messageLeavePage));
