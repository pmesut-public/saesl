import { fromJS } from 'immutable';
import { createReducer } from 'redux-immutablejs';
import _ from 'lodash';

import constants from './constants';
const {
  ALL_HAS_CHANGED,
  ALL_ERROR,
  ALL_FETCHING,
  ALL_STORED,
  SET_ERRORS,
  FINISHED_REQUEST,
} = constants;


const initialState = fromJS({
  status: ALL_STORED,
  errors: [],
});

export default createReducer(initialState, {
  [FINISHED_REQUEST]: (state) => state.set('status', ALL_STORED),

  [ALL_STORED]: (state) => state.set('status', ALL_STORED),

  [ALL_FETCHING]: (state, {id}) => state.set('status', ALL_FETCHING).set('id', id),

  [ALL_ERROR]: (state) => state.set('status', ALL_ERROR),

  [SET_ERRORS]: (state, {errors}) => state.set('errors', errors),
});

export const isFetching = (state) => state.get('status') === ALL_FETCHING;
export const getErrors = (state) => state.get('errors');
export const getEstandarErrors = (state, esta_id) =>
  state.get('errors')
  .filter(e => e.esta_id == esta_id)
  .map(e => e.errors)[0];
export const canNotLeave = (state) => state.get('status') !== ALL_STORED;

export const summaryStatus = (status, summaryHasChanged) => {
  if (status === ALL_FETCHING) {
    return ALL_FETCHING;
  }
  if (status === ALL_ERROR) {
    return ALL_ERROR;
  }
  if (status === ALL_STORED && !summaryHasChanged) {
    return ALL_STORED;
  }
  return ALL_HAS_CHANGED;
};
