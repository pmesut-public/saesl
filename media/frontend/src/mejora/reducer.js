import { createReducer, combineReducers } from 'redux-immutablejs';

import mejo_detalles, { getStatusDetalles } from './detalles/reducer';
import system_status, { summaryStatus } from './systemStatus';

export default combineReducers({
  mejo_detalles,
  system_status,
});

export const getSummaryStatus = (state) => {
  const statuses = [
    ...getStatusDetalles(state.get('mejo_detalles')),
  ];
  const summaryHasChanged = statuses.map(s => s.hasChanged).reduce((acc, s) => acc || s, false);
  return summaryStatus(state.getIn(['system_status', 'status']), summaryHasChanged);
}

