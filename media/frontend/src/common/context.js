let context = {};

export const setContext = ctx => context = ctx;
export const getContext = () => context;
