import React, { Component, createElement } from 'react'
//import { validateField } from '../../utils/index';
export const validateField = ({ pattern, value, required }, messages = {}) => {
  const {
    msgRequired = 'Campo requerido',
    msgPatternFailed = 'No coincide con el formato solicitado',
  } = messages
  if (required && value === '') {
    return msgRequired
  }
  if (!required && value === '') {
    return ''
  }
  if (pattern && pattern.length && !new RegExp(pattern).test(value)) {
    return msgPatternFailed
  }
  return ''
}

const connectToValidate = params => {
  return wrappedComponent => {
    class HOCValidate extends Component {
      constructor(props) {
        super(props)
        this.onChange = this.onChange.bind(this)
        this.onBlur = this.onBlur.bind(this)
        this.setRef = this.setRef.bind(this)

        this.state = {
          msg: '',
        }
      }

      setRef(e) {
        this.el = e
      }

      componentDidMount() {
        if (this.el.tagName === 'TEXTAREA') {
          this.el.pattern = this.el.getAttribute('data-pattern')
        }
        this.setState({ msg: validateField(this.el) })
        this.$el = $(this.el)
        this.$el.tooltip({
          title: this.$el.data('error-msg'),
          trigger: 'manual',
        })
      }

      componentDidUpdate(prevProps) {
        if (prevProps.required != this.props.required) {
          if (this.el.tagName === 'TEXTAREA') {
            this.el.pattern = this.el.getAttribute('data-pattern')
          }
          this.setState({ msg: validateField(this.el) })
          this.$el = $(this.el)
          this.$el.tooltip({
            title: this.$el.data('error-msg'),
            trigger: 'manual',
          })
        }
      }
      /*componentWillReceiveProps(nextProps) {
        console.log('hoc', nextProps.value, this.state.value)
        if (nextProps.value !== this.state.value) {
          this.setState({ value: nextProps.value })
        }
      }
      */

      onChange(e) {
        this.onBlur(e)
      }

      onBlur(e) {
        const target = e.target
        const msg = validateField(target)
        const $el = this.$el

        this.setState({ msg })

        if (msg.length) {
          if (target.value !== '' || target.required) {
            $el.tooltip('show')
            setTimeout(() => $el.tooltip('hide'), 2000)
          }
        }

        if (target.name) {
          this.props.onChange(e)
        } else {
          this.props.onChange(target.value)
        }
      }

      render() {
        const attr = {
          ...this.props,
          required: this.props.required,
          pattern: this.props.pattern,
          name: this.props.name,
          className: this.props.className,
          msg: this.state.msg,
          onChange: this.onChange,
          onBlur: this.onBlur,
          setRef: this.setRef,
          //this.props.required ?
          placeholder: this.props.placeholder, //|| "* campo requerido" :
          //null,
        }

        return createElement(wrappedComponent, { ...attr })
      }
    }

    return HOCValidate
  }
}

export default connectToValidate
