import React, { Component } from 'react';

export default (component, message) => {
  class HOC extends Component {
    componentDidMount() {
      $(window).bind('beforeunload', () =>
        this.props.isFetching ? message : undefined
      );
    }

    render() {
      return React.createElement(component, {...this.props});
    }
  }

  return HOC;
};
