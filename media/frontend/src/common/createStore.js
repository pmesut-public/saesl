import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

export default ({ initialState, api, rootReducer }) => {
  /*
  const createStoreWithMiddleware = applyMiddleware(
    thunk.withExtraArgument({...api})
  )(createStore);
  const store = createStoreWithMiddleware(rootReducer, initialState);
  */
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(applyMiddleware(thunk.withExtraArgument({ ...api })))
  )

  return store
}
