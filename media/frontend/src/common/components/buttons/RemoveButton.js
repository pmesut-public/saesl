import React from 'react';

const RemoveButton = ({
  onClick,
  text = '',
  title = '',
  editable = true,
  disabled,
  size='',
}) => (
  editable ?
    <button
  onClick={onClick}
  type="button"
  className={`btn btn-flat btn-danger btn-delete ${size}`}
  title={title}
  disabled={disabled}
    >
		<i className="fa fa-times"></i>
  </button> :
    <div />
);

export default RemoveButton;
