import React from 'react';

const AddButton = ({
  onClick,
  text = '',
  title = '',
  editable = true,
  disabled,
  size='md',
}) => (
  editable ?
    <button
      onClick={onClick}
      type="button"
      className={`btn btn-${size} btn-flat btn-primary`}
      title={title}
      disabled={disabled}
    >
		<i className="fa fa-plus"></i>
    {text}
  </button> :
    <div />
);

export default AddButton;
