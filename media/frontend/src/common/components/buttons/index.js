import React from 'react';

const getStyle = (danger, success, info, primary, warning) =>
  danger ? 'danger':
    success? 'success':
      info? 'info':
        primary? 'primary':
          warning? 'warning':
            'default';

const Button = ({
  children,
  danger,
  success,
  info,
  primary,
  warning,
  disabled,
  onClick,
  title='',
  extraClass = '',
  style={marginLeft: '5px'},
  size='btn-sm'
}) => (
    <button
      type="button"
      title={title}
      className={`btn btn-${getStyle(danger, success, info, primary, warning)} ${size} ${extraClass}`}
      style={style}
      disabled={disabled}
      onClick={onClick}>
    {children}
  </button>
);

import AddButton from './AddButton';
import RemoveButton from './RemoveButton';
export {
  AddButton,
  RemoveButton,
};

export default Button;
