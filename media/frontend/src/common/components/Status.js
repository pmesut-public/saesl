import React from 'react'

const colors = {
  green: '#28a745',
  yellow: '#ffc107',
  red: '#dc3545',
}

const StatusBadge = ({
  status,
  size = '40px',
  style = {},
  title,
  children,
}) => (
  <div
    title={title}
    style={{
      position: 'relative',
      backgroundColor: colors[status],
      height: size,
      width: size,
      borderRadius: '50%',
      border: '1px solid black',
      ...style,
    }}
  >
    {children}
  </div>
)

export default StatusBadge
