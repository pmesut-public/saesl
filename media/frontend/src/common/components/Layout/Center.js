import React from 'react';

const Center = ({
  children,
  danger,
  extraClass = '',
  ...props,
}) => (
  <div
  className={`text-center ${danger ? 'alert-danger': ''} ${extraClass}`} {...props}>
    {children}
  </div>
);

export default Center;
