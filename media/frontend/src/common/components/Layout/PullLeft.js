import React from 'react';

export default ({ children }) => (
  <div className="pull-left">{children}</div>
);
