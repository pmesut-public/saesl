import React from 'react';

export default ({ children }) => (
    <div className="pull-right">{children}</div>
);
