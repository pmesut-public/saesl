import React, { Component } from 'react';

export default class Steps extends Component {
  _navigate = (step) => {
    const {
      children,
      currentStep,
      navigate,
    } = this.props;

    const limit = React.Children.count(children);
    const nextStep = currentStep + step;

    if (nextStep < limit && nextStep > -1) {
      navigate(nextStep);
    }
  }

  _goStep = (step) => this.props.navigate(step);
  _goNext = () => this._navigate(1);
  _goBack = () => this._navigate(-1);

  render() {
    const {
      children,
      currentStep,
    } = this.props;

    const navs = React.Children.map(children, (el, i) => (
      <li
        role="presentation"
        className={i === currentStep ? 'active': ''}
        key={i}
      >
        <a href="#">{el.props.title}</a>
      </li>
    ));

    const currentTab = React.cloneElement(
      children[currentStep],
      {onNext: this._goNext, onBack: this._goBack, goStep: this._goStep}
    );
    return (
      <div>
        <ul className="nav nav-tabs nav-justified no-pointer">
        {navs}
        </ul>
        {currentTab}
      </div>
    );
  }
}
