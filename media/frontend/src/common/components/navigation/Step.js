import React from 'react';

const Step = ({
  goStep,
  onNext,
  onBack,
  comp,
}) => (
  <div>{React.createElement(comp, {onNext, onBack, goStep})}</div>
);

export default Step;
