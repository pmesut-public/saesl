import React from 'react';

const icon = {
  'danger': 'exclamation-triangle',
  'info': 'exclamation',
  'success': 'check',
};

const Alert = ({
  message,
  onClose,
  type = 'danger',
}) => (
    <div className="alert-wrapper">
    <div className={`alert alert-${type} text-center`} role="alert">
    <i className="fa fa-ban"></i>
    <button
      type="button"
      className="close"
      onClick={onClose}
    >
    <span aria-hidden="true" >×</span>
    <span className="sr-only">Close</span>
    </button>
    <strong>
    <i className={`fa fa-${icon[type]}`}></i>
    </strong> {message}
  </div>
    </div>
);

export default Alert;
