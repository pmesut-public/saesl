import React from 'react';

const GlyphiconRefresh = () => <i className='fa fa-refresh fa-animate'/>;

export default GlyphiconRefresh;
