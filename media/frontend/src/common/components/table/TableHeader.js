import React from 'react'

const TableHeaderRow = ({ headers, className }) => (
  <tr className={className}>
    {headers.map(({ display, customStyle = {}, ref = () => {} }, i) => (
      <th className="text-center" key={i} {...customStyle}>
        <div ref={ref}>{display}</div>
      </th>
    ))}
  </tr>
)

const TableHeader = ({ headers, className }) => (
  <thead>
    <TableHeaderRow headers={headers} className={className} />
  </thead>
)

TableHeader.Row = TableHeaderRow
export default TableHeader
