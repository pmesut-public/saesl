import React from 'react';
import classNames from 'classnames';

export const Table = ({
  bordered = false,
  children,
}) => {

  const tableClass = classNames({
    'table': true,
    'table-bordered': bordered,
  });

  return (
    <table className={tableClass}>
      {children}
    </table>
  );
};

export default Table;
