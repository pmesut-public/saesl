import React from 'react'
import map from 'lodash/map'

const RadioField = ({
  name,
  label,
  labelClass = 'col-xs-3 col-sm-2 control-label',
  options,
  selected,
  onChange,
  editable,
  radioClass = 'col-xs-9',
  disabled = false,
  classWrapper = '',
}) => {
  const comp = map(options, o => (
    <div className="radio" key={o.id}>
      <label>
        <input
          value={o.id}
          name={name}
          type="radio"
          checked={selected === o.id}
          onChange={onChange}
          disabled={disabled || o.disabled}
        />
        {o.text}
      </label>
    </div>
  ))
  return (
    <div className={`form-group ${classWrapper}`}>
      <label className={labelClass}>{label}</label>
      <div className={radioClass}>{comp}</div>
    </div>
  )
}

export default RadioField
