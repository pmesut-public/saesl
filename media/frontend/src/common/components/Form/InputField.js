import React from 'react';

import connectToValidate from '../../hoc/connectToValidate';

const InputField = ({
  onChange,
  msg,
  onBlur,
  errorMsg,
  setRef,
  classWrapper='',
  instantUpdate,
  value,
  ...props,
}) => (
    <div
     className={`${classWrapper}`}
    >
    <input
      {...props}
      value={value}
      ref={setRef}
      className={`form-control ${ msg.length ? 'bg-danger text-danger' : '' }`}
      data-error-msg={ errorMsg || 'Debe ingresar un número' }
      type="text"
      onChange={onChange}
      onBlur={onBlur}
    />
    </div>
);

export default connectToValidate()(InputField);
