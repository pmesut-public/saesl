import React, { Component } from 'react'
import format from 'date-fns/format'
import esLocale from 'date-fns/locale/es'

import { DatePicker } from 'common/components/Form'

const showDate = date =>
  date ? format(date, 'DD/MM/YYYY', { locale: esLocale }) : 'Seleccione'

const START = 'start_date'
const END = 'end_date'

class RangePicker extends Component {
  state = {
    isOpenModal: false,
    type: START,
  }
  onClose = () => this.setState({ isOpenModal: false })

  openModal = type => {
    this.setState({
      isOpenModal: true,
      type,
    })
  }
  onSelect = date => {
    if (this.state.type === START) {
      this.props.onStartChange(date)
    } else {
      this.props.onEndChange(date)
    }
    this.onClose()
  }

  render() {
    const { isOpenModal, type } = this.state
    const { startTime, endTime } = this.props

    return (
      <div>
        <DatePicker
          isOpen={isOpenModal}
          minDate={type === END ? startTime : null}
          onClose={this.onClose}
          onSelect={this.onSelect}
        />
        <DatePicker.Button
          title="Elegir fecha inicial"
          onClick={() => this.openModal(START)}
        />
        <DatePicker.Button
          title="Elegir fecha fin"
          onClick={() => this.openModal(END)}
        />
        <div>{showDate(startTime)}</div>
        <div>{showDate(endTime)}</div>
      </div>
    )
  }
}

export default RangePicker
