import React from 'react';
import _ from 'lodash';

const SelectField = ({
  onChange,
  msg = '',
  value,
  classWrapper = '',
  name,
  defaultOption="Seleccione",
  options,
  setRef,
  label,
  ...props,
}) => (
    <div className={classWrapper}>

    {label && <label className="control-label">{label}</label> }
    <select
      {...props}
      className={`form-control ${ (!value || !value.length) ? 'bg-danger' : '' }`}
      ref={setRef}
      name={name}
      value={value}
      onChange={onChange}
    >
    <option value="">{defaultOption}</option>
    {
      _.map(options, ({ id, name, display }) => (
        <option
          key={id}
          value={id}
          title={name || display}
        >
          {name || display}
        </option>
      ))
    }
  </select>
    </div>
);

export default SelectField;
