import React from 'react';

import InputField from './InputField';

const DisplayOrInput = ({
  editable,
  onChange,
  value,
  ...props,
}) => (
  editable ?
    <InputField
      instantUpdate
      value={'' + value}
      onChange={onChange}
      {...props}
    /> :
    <div>{value}</div>
);

export default DisplayOrInput;
