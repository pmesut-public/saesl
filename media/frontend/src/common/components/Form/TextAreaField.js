import React, { Component } from 'react';

import connectToValidate from '../../hoc/connectToValidate';

class TextAreaInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value || '',
    };
  }

  onChange = (e) => {
    this.setState({value: e.target.value});
  }

  onBlur = (e) => {
    this.props.onChange(e);
  }

  render() {
    const {
      msg,
      onBlur,
      errorMsg,
      setRef,
      pattern,
      classWrapper='',
      value,
      updateOnBlur,
      ...props,
    } = this.props;
    return (
        <div
      className={classWrapper}
        >
        <textarea
      value={this.state.value}
      {...props}
      data-pattern={pattern}
      ref={setRef}
      className={`form-control ${ msg.length ? 'bg-danger text-danger' : '' }`}
      data-error-msg={ props.errorMsg || 'Campo requerido' }
      data-error-msg={ errorMsg || 'Campo requerido' }
      onChange={this.onChange}
      onBlur={this.onBlur}
        />
        </div>
    );
  }
}

export default connectToValidate()(TextAreaInput);
