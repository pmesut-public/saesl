import React, { Component } from 'react'
import InfiniteCalendar from 'react-infinite-calendar'
import Modal from 'react-modal'
import format from 'date-fns/format'
import Button from 'common/components/buttons'

const CustomButton = ({
  onClick,
  title = 'Elegir fecha',
  disabled = false,
}) => (
  <Button
    onClick={onClick}
    info
    title={title}
    size="btn-md"
    extraClass="btn-flat"
    disabled={disabled}
    style={{ marginRight: '5px' }}
  >
    <i className="fa fa-table" />
  </Button>
)

const customStyles = {
  content: {
    position: 'absolute',
    top: '20px',
    bottom: 'auto',
    left: '50%',
    right: 'auto',
    transform: 'translate(-50%, 0)',
  },
}

const locale = {
  locale: require('date-fns/locale/es'),
  headerFormat: 'dddd, D MMM',
  weekdays: ['Dom', 'Lun', 'Mar', 'Mier', 'Jue', 'Vier', 'Sab'],
  blank: 'Selecciona un dia',
  todayLabel: {
    long: 'Dia actual',
    short: 'Hoy.',
  },
  ...locale,
}

class DatePicker extends Component {
  onSelect = date => {
    this.props.onSelect(date)
  }

  render() {
    const { isOpen, onClose, date, minDate } = this.props
    const options = {
      locale,
      onSelect: this.onSelect,
      selected: date,
    }
    if (minDate) {
      options.minDate = minDate
    }
    return (
      <Modal
        isOpen={isOpen}
        style={customStyles}
        onRequestClose={onClose}
        contentLabel="datepicker"
      >
        <InfiniteCalendar {...options} />
      </Modal>
    )
  }
}

DatePicker.Button = CustomButton
export default DatePicker
