import React, { Component } from 'react'
import InfiniteCalendar, { withRange, Calendar } from 'react-infinite-calendar'
import Modal from 'react-modal'
import format from 'date-fns/format'
import esLocale from 'date-fns/locale/es'

import { DatePicker } from 'common/components/Form'

const showDate = date =>
  date ? format(date, 'DD/MM/YYYY', { locale: esLocale }) : ''

const CalendarWithRange = withRange(Calendar)

const customStyles = {
  overlay: { zIndex: 10 },
  content: {
    position: 'absolute',
    top: '20px',
    bottom: 'auto',
    left: '50%',
    right: 'auto',
    transform: 'translate(-50%, 0)',
    zIndex: 1,
  },
}

const locale = {
  locale: esLocale,
  headerFormat: 'dddd, D MMM',
  weekdays: ['Dom', 'Lun', 'Mar', 'Mier', 'Jue', 'Vier', 'Sab'],
  blank: 'Selecciona un dia',
  todayLabel: {
    long: 'Dia actual',
    short: 'Hoy.',
  },
  ...locale,
}

class RangeDatePicker extends Component {
  state = {
    isOpen: false,
  }
  onSelect = event => {
    if (event.eventType == 3) {
      this.props.onChange(event.start, event.end)
      this.setState({ isOpen: false })
    }
  }

  render() {
    const { isOpen } = this.state
    const { startTime, endTime, disabled } = this.props
    const options = {
      locale,
      width: 600,
      onSelect: this.onSelect,
      date: new Date(),
    }
    const hasDates = startTime && endTime
    options.selected = null
    if (hasDates) {
      options.selected = {
        start: startTime,
        end: endTime,
      }
    }
    return (
      <div>
        <Modal
          isOpen={isOpen}
          style={customStyles}
          onRequestClose={() => this.setState({ isOpen: false })}
          contentLabel="rangepicker"
        >
          <InfiniteCalendar {...options} Component={CalendarWithRange} />
        </Modal>

        <DatePicker.Button
          title="Elegir rango de fechas"
          onClick={() => this.setState({ isOpen: true })}
          disabled={disabled}
        />
        <span>
          {hasDates
            ? `${showDate(startTime)} - ${showDate(endTime)}`
            : 'Seleccione'}
        </span>
      </div>
    )
  }
}

export default RangeDatePicker
