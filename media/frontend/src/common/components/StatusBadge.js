import React from 'react'
import _ from 'lodash'

import Status from './Status'

const findItem = (estado, list) => _.find(list, e => e.id == estado)

const hasEstado = estado => estado !== '' && estado != null

const getBgColor = (estado = '', list) => {
  const gg = hasEstado(estado)
  return hasEstado(estado) ? findItem(estado, list).color : ''
}

const StatusBadge = ({
  estado,
  list = [],
  size = '40px',
  style = {},
  children,
}) => (
  <Status
    status={getBgColor(estado, list)}
    size={size}
    style={style}
    title={hasEstado(estado) ? findItem(estado, list).display : ''}
  >
    {children}
  </Status>
)

export default StatusBadge
