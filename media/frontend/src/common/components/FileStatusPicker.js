import React, { Component } from 'react'
import { SelectField, InputField, RadioField } from 'common/components/Form'
import Button from 'common/components/buttons'
import format from 'date-fns/format'

import { Table, TableHeader } from 'common/components/table'
import Status from 'common/components/Status'
import RangeDatePicker from 'common/components/Form/RangeDatePicker'
import {
  getVigenciaValues,
  vigenciaTitle,
} from 'licenciamiento/formatos/containers/Formato'
import { subHeaders } from 'licenciamiento/formatos/containers'

const headers = [
  { display: 'Vigencia', customStyle: { colSpan: 3 } },
  {
    display: `Estado act. al: ${format(new Date(), 'DD/MM/YYYY')}`,
    customStyle: { colSpan: 2, width: '40%' },
  },
]

const isValid = (num, pattern) => new RegExp(pattern).test(num)

class FileStatusPicker extends Component {
  state = {
    estado: '',
    subEstado: '',
    da: '',
    fecha_inicio: '',
    fecha_fin: '',
    replaceFile: '',
  }

  onRangeChange = (fecha_inicio, fecha_fin) => {
    this.setState({
      fecha_inicio,
      fecha_fin,
    })
  }

  onChange = ({ target: { value, name } }) => {
    this.setState({
      [name]: value,
    })
    if (name === 'estado') {
      this.setState({
        subEstado: '',
        da: '',
        fecha_inicio: '',
        fecha_fin: '',
      })
    }
  }
  onSubmit = () => {
    const { fecha_inicio, fecha_fin, replaceFile } = this.state
    const { replaceId } = this.props

    this.props.onSubmit({
      ...this.state,
      fecha_inicio: fecha_inicio.toJSON ? fecha_inicio.toJSON() : '',
      fecha_fin: fecha_fin.toJSON ? fecha_fin.toJSON() : '',
      replaceId: replaceId && replaceFile ? replaceId : '',
    })
  }
  render() {
    const {
      onCancel,
      estados,
      subEstados,
      showVigencia,
      hideExtraInfo,
      replaceId,
    } = this.props
    const {
      estado,
      subEstado,
      da,
      fecha_inicio,
      fecha_fin,
      replaceFile,
    } = this.state

    const subOptions = subEstados.filter(sub => sub.parentId == estado)
    const rangeSelected = fecha_inicio && fecha_fin
    const vigencia = getVigenciaValues(this.state)
    const statusPending = estado != estados[1].id
    const formDisabled =
      (hideExtraInfo
        ? false
        : !subEstado ||
          (showVigencia && !statusPending
            ? !isValid(da, '^[0-9]+$')
            : false)) || (replaceId ? replaceFile === '' : false)

    const options = [
      {
        id: 0,
        text: 'No',
      },
      {
        id: 1,
        text: 'Si',
      },
    ]

    return (
      <div>
        {replaceId && (
          <RadioField
            label={
              'Ya existe un archivo con el mismo nombre desea reemplazarlo?'
            }
            labelClass="col-xs-8 control-label"
            radioClass="col-xs-3"
            options={options}
            selected={replaceFile}
            onChange={e => {
              const value = +e.target.value
              if (value == 0) {
                return onCancel()
              }
              this.setState({ replaceFile: value })
            }}
          />
        )}
        {!hideExtraInfo && (
          <div className="form-group">
            <SelectField
              classWrapper=""
              defaultOption="Seleccione estado"
              options={estados}
              value={estado}
              name="estado"
              onChange={this.onChange}
            />
          </div>
        )}

        {!hideExtraInfo && (
          <div className="form-group">
            <SelectField
              classWrapper=""
              defaultOption="Seleccione subestado"
              options={subOptions}
              value={subEstado}
              name="subEstado"
              disabled={!estado}
              onChange={this.onChange}
            />
          </div>
        )}
        {showVigencia &&
          !statusPending && (
            <Table bordered>
              <thead>
                <TableHeader.Row headers={headers} />
                <TableHeader.Row headers={subHeaders} />
              </thead>
              <tbody>
                <tr>
                  <td>
                    <RangeDatePicker
                      disabled={statusPending}
                      onChange={this.onRangeChange}
                      startTime={fecha_inicio}
                      endTime={fecha_fin}
                    />
                  </td>
                  <td style={{ minWidth: '40px' }}>{vigencia.dv}</td>
                  <td style={{ minWidth: '70px' }}>
                    <InputField
                      value={da || ''}
                      onChange={da => this.setState({ da })}
                      pattern={'^[0-9]+$'}
                      disabled={!rangeSelected}
                      required
                    />
                  </td>
                  <td>{vigencia.ed}</td>
                  <td>
                    {vigencia.a && (
                      <Status
                        status={vigencia.a}
                        style={{ margin: '0 auto' }}
                        title={vigenciaTitle[vigencia.a]}
                      />
                    )}
                  </td>
                </tr>
              </tbody>
            </Table>
          )}
        <Button danger onClick={onCancel}>
          Cancelar
        </Button>

        <Button primary onClick={this.onSubmit} disabled={formDisabled}>
          Subir archivo
        </Button>
      </div>
    )
  }
}

export default FileStatusPicker
