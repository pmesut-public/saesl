import axios from 'axios'

export default ({ url, data = {}, method = 'POST', onSuccess, onError }) => {
  axios({
    url,
    method,
    data,
  })
    .then(({ data }) => {
      if (!data || data.status !== 'OK') {
        throw data
      }
      onSuccess(data)
      //onError(data)
    })
    .catch(onError)
  //.catch(onSuccess)
}
