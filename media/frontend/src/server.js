var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('../webpack.config');

const PORT = 3001;
const mockServerTarget = {
  target: 'http://localhost:3002',
};

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  proxy: {
    '/documentos/*': mockServerTarget,
    '/ponderaciones/*': mockServerTarget,
    '/autoevaluacion/*': mockServerTarget,
  }
}).listen(PORT, 'localhost', function (err, result) {
  if (err) {
    console.log(err);
  }

  console.log('Listening at localhost: %s', PORT);
});
