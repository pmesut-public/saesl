import createStore from 'common/createStore';
import * as api from './api';
import * as apiDocumentos from 'documentos/api';
import rootReducer from './reducer';

export default initialState =>
  createStore({initialState, api: {api, apiDocumentos}, rootReducer});
