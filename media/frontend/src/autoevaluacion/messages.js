const tryAgain = 'Intentelo nuevamente.';

export const ERROR_CREATE_ACTIVIDAD = 'No se pudo crear la actividad.' + tryAgain;
export const ERROR_REMOVE_ACTIVIDAD = 'No se pudo eliminar la actividad.' + tryAgain;
export const ERROR_CREATE_CRITERIO = 'No se pudo crear el criterio.' + tryAgain;
export const ERROR_REMOVE_CRITERIO = 'No se pudo eliminar el criterio.' + tryAgain;
export const ERROR_CREATE_EVIDENCIA = 'No se pudo crear la evidencia.' + tryAgain;
export const ERROR_REMOVE_EVIDENCIA = 'No se pudo eliminar la evidencia.' + tryAgain;

export const CONFIRM_REMOVE_ACTIVIDAD = '¿Desa eliminar la actividad?';
export const CONFIRM_REMOVE_CRITERIO = '¿Desa eliminar el criterio?';
export const CONFIRM_REMOVE_EVIDENCIA = '¿Desa eliminar la evidencia?';

export const messageLeavePage = 'Existen datos sin guardar. Desea salir?';
