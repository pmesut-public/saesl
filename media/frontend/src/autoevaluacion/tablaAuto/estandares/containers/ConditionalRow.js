import React from 'react';
import { connect } from 'react-redux';

import { updateNivel, updateActividades, updateImpacto } from '../actions';
import { getValoracionIndex, getDetalleById } from '../reducer';
import { getContext } from 'common/context';
import { SelectField, TextAreaField } from 'common/components/Form';
import { Table, TableHeader } from 'common/components/table';

const SIX_VALUE = 5;
const NINE_VALUE = 8;

const headers = [
  { display: 'Con las actividades que ha desarrollado hasta esta etapa, ¿qué nivel de cumplimiento del estándar ,en una escala de 1 a 10, considera que ha alcanzado? (revise la descripción del estándar):', customStyle: {width: '30%'} },
  { display: 'Del nivel que usted se ha calificado, ¿considera que le faltan (o sobran) actividades?', customStyle: {width: '35%'}},
  { display: 'De su evaluación actual, ¿la implementación de las actividades listadas tiene el impacto esperado en el cumplimiento del estándar?', customStyle: {width: '35%'}},
];

const option9Headers = [
  headers[0],
  { display: 'De todo lo avanzado ¿la implementación de las actividades listadas tiene el impacto esperado? ' }
];

const Row = ({ headers, children }) => (
  <tr className="conditional__row"><td colSpan="5">
    <Table bordered>
    <TableHeader headers={headers}/>
      <tbody>{children}</tbody>
    </Table>
  </td></tr>
);


const ConditionalRow = ({
  estadoIndex,
  detalle,
  updateNivel,
  updateActividades,
  updateImpacto
}) => {
  const SelectTd = (
    <td>
      <SelectField
        required
        options={getContext().niveles}
        value={detalle.get('aude_nivel')}
        onChange={updateNivel}
      />
    </td>
  );

  const ImpactoTd = (
    <td>
      <TextAreaField
        maxLength="20000"
        rows="6"
        required
        value={detalle.get('aude_impacto')}
        updateOnBlur
        onChange={updateImpacto}
      />
    </td>
  );

  if (estadoIndex === SIX_VALUE) {
    return (
      <Row headers={headers}>
        <tr>
          {SelectTd}
        <td>
          <TextAreaField
            maxLength="20000"
            rows="6"
            required
            value={detalle.get('aude_actividades')}
            updateOnBlur
            onChange={updateActividades}
          />
        </td>
          {ImpactoTd}
        </tr>
      </Row>
    );
  }
  if (estadoIndex === NINE_VALUE) {
    return (
      <Row headers={option9Headers}>
        <tr>
          { SelectTd }
          { ImpactoTd }
        </tr>
      </Row>
    );
  }
  return null;
}

const mapStateToProps = (state, { id }) => ({
  estadoIndex: getValoracionIndex(state.get('auto_detalles'), id),
  detalle: getDetalleById(state.get('auto_detalles'), id),
});

const mapDispatchToProps = (dispatch, { id }) => {
  return {
    updateNivel: (e) => dispatch(updateNivel(id, e.target.value)),
    updateActividades: (val) => dispatch(updateActividades(id, val)),
    updateImpacto: (val) => dispatch(updateImpacto(id, val)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConditionalRow);

