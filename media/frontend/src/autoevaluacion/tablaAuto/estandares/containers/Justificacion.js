import React from 'react';
import { connect } from 'react-redux';

import { TextAreaField } from 'common/components/Form';
import { getJustificacion } from '../reducer';
import { updateJustificacion } from '../actions';

const Justificacion = ({
  updateJustificacion,
  value,
  required,
}) =>(
  <div>
    <TextAreaField
      required={required}
      value={value}
      onChange={updateJustificacion}
      rows="10"
    />
  </div>
);

const mapStateToProps = (state, {id}) => ({
  value: getJustificacion(state.get('auto_detalles'), id),
});

const mapDispatchToProps = (dispatch, {id}) => ({
  updateJustificacion: (value) => dispatch(updateJustificacion(id, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Justificacion);
