import * as messages from 'autoevaluacion/messages';

import constants from 'autoevaluacion/constants';
const {
  MERGE_FILES_SELECTED,
  UPDATE_EVIDENCIA,
  ADD_EVIDENCIA,
  REMOVE_EVIDENCIA,
} = constants;

import {
  allFetching,
  allError,
  finishedRequest,
} from 'autoevaluacion/tablaAuto/actions';

const addEvidencia = (id, aude_id) => ({
  type: ADD_EVIDENCIA,
  id,
  aude_id,
});

export const addEvidenciaAPI = (esta_id, aude_id) => (dispatch, _, {api}) => {
  dispatch(allFetching());
  api.createEvidencia({
    data: {esta_id},
    onSuccess: ({ aume_id }) => {
      dispatch(finishedRequest());
      dispatch(addEvidencia(aume_id, aude_id));
    },
    onError: (err) => {
      dispatch(allError(messages.ERROR_CREATE_EVIDENCIA));
      console.log('err', err);
    },
  });
};

export const updateEvidencia = (id,prop, value) => ({
  type: UPDATE_EVIDENCIA,
  prop,
  id,
  value,
});

const removeEvidencia = (id) => ({
  type: REMOVE_EVIDENCIA,
  id,
});

export const removeEvidenciaAPI = (id) => (dispatch, _, {api}) => {
  if (!confirm(messages.CONFIRM_REMOVE_EVIDENCIA)) {
    return;
  }
  dispatch(allFetching());
  api.deleteEvidencia({
    data: {aume_id: id},
    onSuccess: () => {
      dispatch(finishedRequest());
      dispatch(removeEvidencia(id));
    },
    onError: (err) => {
      dispatch(allError(messages.ERROR_REMOVE_EVIDENCIA));
      console.log('err', err);
    },
  });
};


