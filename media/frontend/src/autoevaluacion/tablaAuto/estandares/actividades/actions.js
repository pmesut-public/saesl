import constants from 'autoevaluacion/constants';
const {
  ADD_ACTIVIDAD,
  REMOVE_ACTIVIDAD,
  UPDATE_ACTIVIDAD,
  UPDATE_VINCULOS,
  UPDATE_PROP_ACTIVIDAD,
} = constants;

import {
  allFetching,
  allError,
  finishedRequest,
} from 'autoevaluacion/tablaAuto/actions';

import * as messages from 'autoevaluacion/messages';

import { fromJS } from 'immutable';
import difference from 'lodash/difference';
import intersection from 'lodash/intersection';

import { getActividad } from './reducer';

const updateProp = (id, prop, value) => ({
  type: UPDATE_PROP_ACTIVIDAD,
  id,
  prop,
  value,
});

const _updateVinculos = (id, added, removed) => ({
  type: UPDATE_VINCULOS,
  id,
  added,
  removed,
})

export const updateVinculos = (id, estandares, cb) => (dispatch, getState, { api }) => {
  const pastEstandares = getActividad(getState(), id).get('estandares').toJS();
  const addedEstandares = difference(estandares, pastEstandares);
  const removedEstandares = difference(pastEstandares, estandares);
  const intersectionEstandares = intersection(estandares, pastEstandares);

  api.actividadUpdateVinculacion({
    data: {
      auac_id: id,
      new_estandares: addedEstandares,
      delete_estandares: removedEstandares,
    },
    onSuccess: () => {
      dispatch(_updateVinculos(id, addedEstandares, removedEstandares));
      dispatch(updateProp(id, 'estandares', fromJS(estandares)));
      cb();
    },
    onError: (err) => {
      dispatch(allError('No se pudo vincular la actividad'));
      console.log('err', err)
    },
  })
}

const _addActividad = (actividad, aude_id) => ({
  type: ADD_ACTIVIDAD,
  actividad,
  aude_id,
})

export const addActividad = (aude_id) => (dispatch, _, { api }) => {
  dispatch(allFetching());

  api.createActividad({
    data: {aude_id},
    onSuccess: ({ auac_id, act_cod }) => {
      dispatch(finishedRequest());
      const act = {
        id: auac_id,
        auac_id,
        descripcion: '',
        aude_id,
        estandares: [],
        act_cod,
      }
      dispatch(_addActividad(act, aude_id));
    },
    onError: (err) => {
      dispatch(allError(messages.ERROR_CREATE_ACTIVIDAD));
      console.log('err', err);
    }
  })
}

const _removeActividad = (act_id, aude_id) => ({
  type: REMOVE_ACTIVIDAD,
  id: act_id,
  aude_id,
});

export const removeActividad = (act_id, aude_id) => (dispatch, getState, { api }) => {
  const estandar = getActividad(getState(), act_id).get('aude_id');
  const estandares = getActividad(getState(), act_id).get('estandares');

  if (estandar == aude_id) {
    if (!confirm(messages.CONFIRM_REMOVE_ACTIVIDAD)) {
      return;
    }

    dispatch(allFetching());
    api.deleteActividad({
      data: {auac_id: act_id},
      onSuccess: () => {
        dispatch(finishedRequest())
        dispatch(_removeActividad(act_id, aude_id));
      },
      onError: (err) => {
        dispatch(allError(messages.ERROR_REMOVE_ACTIVIDAD));
        console.log('err', err)
      }
    });
  }
  else {
    dispatch(allFetching());
    const addedEstandares = [];
    const removedEstandares = [aude_id];
    const insersectionEstandares = estandares.filterNot(e => e == aude_id);

    console.log(insersectionEstandares.toJS());
    api.actividadUpdateVinculacion({
      data: {
        auac_id: act_id,
        new_estandares: addedEstandares,
        delete_estandares: removedEstandares,
      },
      onSuccess: () => {
        dispatch(finishedRequest())
        dispatch(_updateVinculos(act_id, addedEstandares, removedEstandares));
        dispatch(updateProp(act_id, 'estandares', insersectionEstandares));
      },
      onError: (err) => {
        dispatch(allError('No se puedo desvincular la actividad'));
        console.log('err', err);
      }
    });
  }
}

export const updateActividad = (id, value) => ({
  type: UPDATE_ACTIVIDAD,
  id,
  value
})
