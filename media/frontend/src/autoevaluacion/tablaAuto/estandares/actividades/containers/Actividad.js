import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal from 'react-modal';

import VinculosActividades from './VinculosActividades';
import { TextAreaField } from 'common/components/Form';
import Button, { RemoveButton } from 'common/components/buttons';
import { removeActividad, updateActividad } from '../actions';
import { getActividad } from '../reducer';

class Actividad extends Component {
  state = {isOpen: false};

  onRemove = () =>
    this.props.removeActividad(
      this.props.actividad.get('id'),
      this.props.aude_id
    );

  onChange= (value) => this.props.updateActividad(this.props.actividad.get('id'), value);

  openMatriz = () => this.setState({ isOpen: true });
  close = () => this.setState({ isOpen: false });

  render() {
    const {
      act_cod,
      descripcion,
      estandares,
    } = this.props.actividad;

    const {
      isFetching,
      aude_id,
    } = this.props;

    const isBaseActividad = aude_id == this.props.actividad.get('aude_id');

    return (
      <div className="row row_item" >
        <div className="col-sm-1">{act_cod}</div>
        {
          !isBaseActividad ?
            <div className="col-sm-9 actividad__readonly">{descripcion}</div> :
          <TextAreaField
            classWrapper="col-sm-9"
            value={descripcion}
            onChange={this.onChange}
            rows="10"
            required
            maxLength="20000"
          />
        }
        <div className="actividad_button_wrapper">
        {
          isBaseActividad &&
            <Button
            onClick={this.openMatriz}
            info
            title={"Matriz"}
            disabled={isFetching}
            size="btn-md"
            extraClass="btn-flat"
            style={{}}
          >
            <i className="fa fa-table"></i>
          </Button>
        }
        <RemoveButton
      disabled={isFetching}
      onClick={this.onRemove}
      title={isBaseActividad ? 'Eliminar actividad': 'Eliminar vínculo'}
        />

        </div>
        <Modal
          isOpen={this.state.isOpen}
          contentLabel="Modal"
        >
        <VinculosActividades
          id={this.props.actividad.get('id')}
          aude_id={aude_id}
          estandares={estandares}
          close={this.close}
          updateVinculos={this.props.updateVinculos}
        />
        </Modal>
      </div>
    );
  }
}

export default Actividad;
