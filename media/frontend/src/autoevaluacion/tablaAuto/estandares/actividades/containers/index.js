import React, { Component } from 'react';
import { connect } from 'react-redux';

import { addActividad, removeActividad, updateActividad, updateVinculos } from '../actions';
import { getActividades } from '../reducer';
import { AddButton } from 'common/components/buttons';
import { isFetching } from 'autoevaluacion/tablaAuto/reducer';
import Actividad from './Actividad';

class Actividades extends Component {
  render() {
    const {
      actividades,
      isFetching,
      addActividad,
      aude_id,
      updateActividad,
      removeActividad,
      updateVinculos,
    } = this.props;

    return (
      <div>
        {actividades.map(act =>
           <Actividad
             key={act.get('id')}
             aude_id={aude_id}
             actividad={act}
             isFetching={isFetching}
             updateActividad={updateActividad}
             removeActividad={removeActividad}
             updateVinculos={updateVinculos}
           />
        )}
        <AddButton
          onClick={addActividad}
          text="Nueva actividad"
          size="md"
          disabled={isFetching}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, { id }) => ({
  aude_id: id,
  actividades: getActividades(state, id),
  isFetching: isFetching(state.get('system_status'))
});

const mapDispatchToProps = (dispatch, { id }) => ({
  addActividad: () => dispatch(addActividad(id)),
  updateActividad: (actId, value) => dispatch(updateActividad(actId, value)),
  removeActividad: (actId) => dispatch(removeActividad(actId, id)),
  updateVinculos: (id, estandares, cb) => dispatch(updateVinculos(id, estandares, cb)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Actividades);
