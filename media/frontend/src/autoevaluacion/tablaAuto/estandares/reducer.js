import { fromJS } from 'immutable';
import { createReducer } from 'redux-immutablejs';
import pick from 'lodash/pick';
import { getContext } from 'common/context';
import constants from 'autoevaluacion/constants';
const {
  UPDATE_JUSTIFICACION,
  UPDATE_COMENTARIO,
  UPDATE_DETALLE_ESTANDAR_VALORACION,
  ALL_STORED,
  UPDATE_DETALLE_PROP,
} = constants;
const JUSTIFICACION_KEY = 'aude_justificacion';
const COMENTARIO_KEY = 'aude_comentario';
const initialState = fromJS({
  ids: [],
  detalles: {},
});
export default createReducer(initialState, {
  [UPDATE_JUSTIFICACION]: (state, { id, value }) =>
    state
      .setIn(['detalles', id, JUSTIFICACION_KEY], value)
      .setIn(['detalles', id, 'hasChanged'], true),
  [UPDATE_COMENTARIO]: (state, { id, value }) =>
    state
      .setIn(['detalles', id, COMENTARIO_KEY], value)
      .setIn(['detalles', id, 'hasChanged'], true),

  [UPDATE_DETALLE_ESTANDAR_VALORACION]: (state, { id, value }) => {
    const detalle_id = getIdByEstandar(state, id);
    return state
      .setIn(['detalles', detalle_id, 'hasChanged'], true)
      .setIn(['detalles', detalle_id, 'aude_valoracion'], value);
      // .setIn(['detalles', detalle_id, 'aude_nivel'], '')
      // .setIn(['detalles', detalle_id, 'aude_actividades'], '')
      // .setIn(['detalles', detalle_id, 'aude_impacto'], '');
  },
  [UPDATE_DETALLE_PROP]: (state, {id, prop, value}) =>
    state
      .setIn(['detalles', id, 'hasChanged'], true)
      .setIn(['detalles', id, prop], value)
      .setIn(['detalles', id, prop], value)
    ,

  [ALL_STORED]: state =>
    state.withMutations(state => {
      state
        .get('ids')
        .forEach(id => state.setIn(['detalles', id, 'hasChanged'], false));
    }),
});

export const getDetalleById = (state, id) =>
  state.getIn(['detalles', id]);

export const getIdByEstandar = (state, esta_id) =>
  state
    .get('ids')
    .find(id => state.getIn(['detalles', id, 'esta_id']) == esta_id);

export const getDetalleByEstandar = (state, esta_id) =>
  state.getIn(['detalles', getIdByEstandar(state, esta_id)]);

const getValoracionByEstandar = (state, esta_id) => {
  return state.getIn(['detalles', esta_id, 'aude_valoracion']);
};

export const isRequiredByValoracion = (state, esta_id) => true
  // getValoracionByEstandar(state, esta_id) != getContext().valoraciones[0].id;

export const shouldActividadesBeRequired = (state, esta_id) => true
  // getValoracionByEstandar(state, esta_id) == getContext().valoraciones[2].id;


export const getJustificacion = (state, id) =>
  state.getIn(['detalles', id, JUSTIFICACION_KEY]);

export const getComentario = (state, id) =>
  state.getIn(['detalles', id, COMENTARIO_KEY]);

// const transformValoracionId = id => {
//   if (!id) return 0;
//   return getContext().valoraciones.filter(v => v.id == id)[0].value;
// };

// export const getValoraciones = state => {
//   let valor = 0;
//   state.get('ids').forEach(id => {
//     let valoracionId = state.getIn(['detalles', id, 'aude_valoracion']);
//     valor += transformValoracionId(isNaN(valoracionId) ? 0 : +valoracionId);
//   });
//   return valor / state.get('ids').size;
// };

export const getValoracion = (state, esta_id) =>
  state.getIn(['detalles', getIdByEstandar(state, esta_id), 'aude_valoracion']);

export const getStatusEstandares = state =>
  state.get('ids').map(id =>
    fromJS({
      hasChanged: state.getIn(['detalles', id, 'hasChanged']),
    })
  );

export const getValoracionIndex = (state, idDetalle) => {
  // const valoracionId = state.getIn(['detalles', idDetalle, 'aude_valoracion']);

  // for (let i = 0, len = getContext().valoraciones.length; i < len ; i++) {
  //   if (getContext().valoraciones[i].id == valoracionId) {
  //     return i;
  //   }
  // }
  return -1;
}

export const getPayloadEstandares = state =>
  state
    .get('ids')
    .filter(id => state.getIn(['detalles', id, 'hasChanged']))
    .map(id => state.getIn(['detalles', id]))
    .toJS()
  .map(o => ({
    esta_id: o.esta_id,
    aude_id: o.aude_id,
    aude_estado: o.aude_valoracion,
  }));


export const getDetalles = state => state.get('detalles');
