import constants from 'autoevaluacion/constants';
const {
  UPDATE_JUSTIFICACION,
  UPDATE_COMENTARIO,
  UPDATE_DETALLE_ESTANDAR_VALORACION,
  UPDATE_DETALLE_PROP,
} = constants;

export const updateNivel = (id, value) => updateProp(id, 'aude_nivel', value);
export const updateActividades = (id, value) => updateProp(id, 'aude_actividades', value);
export const updateImpacto = (id, value) => updateProp(id, 'aude_impacto', value);

const updateProp = (id, prop, value) => ({
  type: UPDATE_DETALLE_PROP,
  id,
  prop,
  value,
});

export const updateJustificacion = (id, value) => ({
  type: UPDATE_JUSTIFICACION,
  id,
  value,
});

export const updateComentario = (id, value) => ({
  type: UPDATE_COMENTARIO,
  id,
  value,
});

export const updateDetalleEstandarValoracion = (id, value) => ({
  type: UPDATE_DETALLE_ESTANDAR_VALORACION,
  id,
  value,
});
