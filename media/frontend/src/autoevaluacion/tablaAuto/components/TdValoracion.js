import React from 'react';
import { getContext } from 'common/context';

const formatValoracion = (num) => {
  const valoraciones = getContext().valoraciones;
  let index = 0;
  for (let i = 0; i < valoraciones.length - 1; i++) {
    console.log(i, +num >  +valoraciones[i].value, +num <= +valoraciones[i+1].value)
    if (+num >  +valoraciones[i].value && +num <= +valoraciones[i+1].value) {
      index = i + 1;
    }
  }

  if (+num == +valoraciones[valoraciones.length - 1]) {
    index = valoraciones.length - 1;
  }

  return valoraciones[index].display;
};

const TdValoracion = ({
  valoracion,
}) => (
  <td>
    {
        false && (
          formatValoracion(valoracion)
        )
    }
  </td>
);

export default TdValoracion;
