import React from 'react';
import { connect } from 'react-redux';
import flatMap from 'lodash/flatMap';
import groupBy from 'lodash/groupBy';

import { getEstandarErrors } from '../reducer';
import { updateDetalleEstandarValoracion } from '../estandares/actions';
import { SelectField } from 'common/components/Form';
import EstandarRowInformation from '../estandares/components';
import { getValoracion, getIdByEstandar } from '../estandares/reducer';
import { getEvidencias } from '../estandares/evidencias/reducer';
import { allFileWithoutStatus } from '../estandares/documentos/reducer';
import { getContext } from 'common/context';
import TdValoracion from '../components/TdValoracion';
import { getFiles } from '../estandares/documentos/reducer';

const sum = (acc, i) => acc + i;

const getDimensionValoracion = (state, dime_id) => {
  const factores = getContext()
    .factores.filter(f => f.dime_id == dime_id)
    .map(f => f.id);
  const total = factores
    .map(id => +getFactorValoracion(state, id))
    .reduce(sum, 0);
  return (total / factores.length).toFixed(2);
};

const transformValoracionId = id => {
  if (!id) return 0;
  return +getContext().valoraciones.filter(v => v.id == id)[0].value;
};

const getFactorValoracion = (state, fact_id) => {
  const estandares = getContext().estandares.filter(e => e.fact_id == fact_id);
  const total = estandares
    .map(e =>
      transformValoracionId(getValoracion(state.get('auto_detalles'), e.id))
    )
    .reduce(sum, 0);
  return (total / estandares.length).toFixed(2);
};

const DimensionRow = connect((state, { id }) => ({
  valoracion: '',
}))(({ display, descripcion, valoracion }) => (
  <tr>
    <td colSpan="2">
      <h4>
        <strong>{display}</strong>
      </h4>
      {descripcion}
    </td>
    <TdValoracion valoracion={valoracion} />
  </tr>
));

const FactorRow = connect((state, { id }) => ({
  valoracion: '',
}))(({ display, descripcion, valoracion }) => (
  <tr>
    <td colSpan="2">
      <h4>
        <strong>{display}</strong>
      </h4>
      {descripcion}
    </td>
    <TdValoracion valoracion={valoracion} />
  </tr>
));

class RawEstandarRow extends React.Component {
  componentDidUpdate() {
    const {
      valoracion,
      onChange,
      documentosPendientes,
      everyEvidenciaHasFiles,
    } = this.props;
    if (!everyEvidenciaHasFiles && valoracion == getContext().estado[1].id) {
      onChange({ target: { value: '' } });
      return alert(
        'Para seleccionar este estado debe tener por lo menos un documento en cada evidencia'
      );
    }
    if (documentosPendientes && getContext().estado[1].id == valoracion) {
      onChange({ target: { value: '' } });
      alert(
        'Existen documentos vinculados que aún se encuentran pendientes de atención'
      );
    }
  }

  render() {
    const {
      display,
      descripcion,
      valoracion,
      onChange,
      errors,
      hideValoracion,
      documentosPendientes,
    } = this.props;
console.log(errors)
    return (
      <tr className="estandar__raw">
        <td />
        <td colSpan="1">
          <h4>
            <strong>{display}</strong>
          </h4>
          {descripcion}
          {errors &&
            errors.length > 0 && (
              <div className="alert alert-danger">{` ${errors}`}</div>
            )}
        </td>
        <td>
          {/** ROL EVALUADOR */}
          {
            <SelectField
              defaultOption="Seleccione"
              options={getContext().estado}
              required
              value={valoracion}
              onChange={onChange}
            />
          }
        </td>
      </tr>
    );
  }
}
const shouldShowValoracion = (state, id) => {
  const detalleId = getIdByEstandar(state.get('auto_detalles'), id);
  return getEvidencias(state.get('auto_evidencias'), detalleId)
    .map(e =>
      allFileWithoutStatus(state.get('auto_documentos'), e.get('auev_id'))
    )
    .toJS()
    .reduce((acc, item) => acc && item, true);
};
const mapEstandarState = (state, { id }) => {
  const detalleId = getIdByEstandar(state.get('auto_detalles'), id);
  const evidencias = getEvidencias(state.get('auto_evidencias'), detalleId);
  let everyEvidenciaHasFiles = true;
  let hasPendingFiles = false;
  evidencias.forEach(e => {
    hasPendingFiles =
      hasPendingFiles ||
      getFiles(state.get('auto_documentos'), e.get('auev_id')).filter(
        f => f.get('audo_estado') == getContext().estados[0].id
      ).size > 0;
    everyEvidenciaHasFiles =
      everyEvidenciaHasFiles &&
      getFiles(state.get('auto_documentos'), e.get('auev_id')).size > 0;
  });

  return {
    valoracion: getValoracion(state.get('auto_detalles'), id),
    errors: getEstandarErrors(state.get('system_status'), id),
    documentosPendientes: hasPendingFiles,
    everyEvidenciaHasFiles,
  };
};
const EstandarRow = connect(mapEstandarState, (dispatch, { id }) => ({
  onChange: e => dispatch(updateDetalleEstandarValoracion(id, e.target.value)),
}))(RawEstandarRow);

const TableBodyAuto = ({ dimensiones, factoresGrouped, estandaresGrouped, mediosGrouped }) => (
  <tbody>
    {flatMap(dimensiones, (d, iD) => [
      <DimensionRow {...d} key={d.id} />,
      ...flatMap(factoresGrouped[d.id], (f, iF) => [
        <FactorRow {...f} key={d.id + '' + f.id} />,
        ...flatMap(estandaresGrouped[f.id], e => [
          <EstandarRow key={d.id + '' + f.id + '' + e.id} {...e} id={e.id} />,
          ...flatMap(mediosGrouped[e.id], m => [
            <EstandarRowInformation
              key={d.id + '' + f.id + '' + e.id + '' + m.id + 'info'}
              id={e.id}
              medio={m}
            />,
          ]),
        ]),
      ]),
    ])}
  </tbody>
);

const mapStateToProps = () => ({
  dimensiones: getContext().dimensiones,
  factoresGrouped: groupBy(getContext().factores, f => f.dime_id),
  estandaresGrouped: groupBy(getContext().estandares, e => e.fact_id),  
  mediosGrouped: groupBy(getContext().medios, e => e.esta_id),
});

export default connect(mapStateToProps)(TableBodyAuto);
