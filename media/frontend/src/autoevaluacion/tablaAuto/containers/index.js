import React from 'react';

import { Table, TableHeader } from 'common/components/table';
import TableBodyAuto from './TableBodyAuto';

const headers = [
  {display: '', customStyle: {width: '5%'}},
  {display: 'Indicadores', customStyle: {width: '75%'}},
  {display: 'Estado', customStyle: {width: '20%'}},
];

const TablaAuto = () => (
  <div>
    <Table bordered>
      <TableHeader headers={headers}/>
      <TableBodyAuto />
    </Table>
  </div>
);

export default TablaAuto;
