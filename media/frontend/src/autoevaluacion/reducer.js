import { fromJS } from 'immutable';
import { createReducer, combineReducers } from 'redux-immutablejs';

import status from 'status/reducer';
import auto_criterios, { getStatusCriterios }
  from './tablaAuto/estandares/criterios/reducer';
import auto_detalles, { getStatusEstandares }
  from './tablaAuto/estandares/reducer';
import auto_evidencias, { getStatusEvidencias }
  from './tablaAuto/estandares/evidencias/reducer';
import auto_documentos, { getStatusDocumentos }
  from './tablaAuto/estandares/documentos/reducer';
import system_status, { summaryStatus } from './tablaAuto/reducer';
import auto_actividades, { getStatusActividades }
  from './tablaAuto/estandares/actividades/reducer';

import toolbar from 'documentos/toolbar/reducer';
import files from 'documentos/fileViewer/reducer';


export default combineReducers({
  status,
  auto_criterios,
  auto_detalles,
  auto_evidencias,
  toolbar,
  files,
  auto_documentos,
  system_status,
  auto_actividades,
});

export const getSummaryStatus = (state) => {
  const statuses = [
    ...getStatusEstandares(state.get('auto_detalles')).toJS(),
    ...getStatusCriterios(state.get('auto_criterios')).toJS(),
    ...getStatusEvidencias(state.get('auto_evidencias')).toJS(),
    ...getStatusDocumentos(state.get('auto_documentos')).toJS(),
    ...getStatusActividades(state.get('auto_actividades')).toJS(),
  ];

  const summaryHasChanged = statuses.map(s => s.hasChanged).reduce((acc, s) => acc || s, false);
  return summaryStatus(state.getIn(['system_status', 'status']), summaryHasChanged);
};
