import keyMirror from 'keymirror'

const constants = keyMirror({
  UPDATE_CRITERIO: null,
  ADD_CRITERIO: null,
  REMOVE_CRITERIO: null,

  UPDATE_DETALLE_PROP: null,
  UPDATE_JUSTIFICACION: null,
  UPDATE_COMENTARIO: null,

  SET_ERRORS: null,
  FINISHED_REQUEST: null,
  ALL_HAS_CHANGED: null,
  ALL_ERROR: null,
  ALL_FETCHING: null,
  ALL_STORED: null,
  UPDATE_DETALLE_ESTANDAR_VALORACION: null,
  UPDATE_FILE_ESTADO: null,
  UPDATE_PROP_FILE: null,
  MERGE_FILES_SELECTED: null,
  UPDATE_EVIDENCIA: null,
  ADD_EVIDENCIA: null,
  REMOVE_EVIDENCIA: null,
  REPLACE_DOCS: null,

  ADD_ACTIVIDAD: null,
  REMOVE_ACTIVIDAD: null,
  UPDATE_ACTIVIDAD: null,
  UPDATE_VINCULOS: null,
  UPDATE_PROP_ACTIVIDAD: null,
})

export default constants
