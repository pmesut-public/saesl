import React, { Component } from 'react';
import { connect } from 'react-redux';

import { canNotLeave } from '../tablaAuto/reducer';
import { messageLeavePage } from '../messages';
import connectOnLeavePage from 'common/hoc/connectOnLeavePage';
import TablaAuto from '../tablaAuto/containers';
import SystemStatus from './SystemStatus';
import { getSummaryStatus } from '../reducer';
import { saveAll } from '../tablaAuto/actions'

class App extends Component {
  componentDidMount() {
    const TIME = 1000 * 60 * 15;
    const {saveAll} = this.props;
    setInterval(saveAll, TIME)
  }
	render() {
		return (
		  <div className="autoevaluacion">
		    <SystemStatus />
		    <TablaAuto />
		    <SystemStatus />
		  </div>
		)
	}
}

const mapStateToProps = (state) => ({
  isFetching: getSummaryStatus(state) !== 'ALL_STORED',
});

export default connect(mapStateToProps, { saveAll })(connectOnLeavePage(App, messageLeavePage));
