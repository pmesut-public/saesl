import request from 'common/request';

const urls = {
  actividadNew: '/autoevaluacion/actividad_new',
  actividadDelete: '/autoevaluacion/actividad_delete',
  actividadUpdateVinculacion: '/autoevaluacion/actividad_update_vinculacion',
  criterioNew: '/autoevaluacion/criterio_new',
  criterioDelete: '/autoevaluacion/criterio_delete',
  evidenciaNew: '/autoevaluacion/medio_new',
  evidenciaDelete: '/autoevaluacion/medio_delete',
  documentosNew: '/autoevaluacion/documento_new',
  documentosBatch: '/autoevaluacion/documento_batch_update',
  documentosDelete: '/autoevaluacion/documento_delete',
  saveAll: '/autoevaluacion/save',
  formatos: '/autoevaluacion/formatos',
  validateFiles: '/autoevaluacion/validar_documentos',
};

const createRequest = url => props => request({ ...props, url });

export const formatos = createRequest(urls.formatos);
export const createActividad = createRequest(urls.actividadNew);
export const deleteActividad = createRequest(urls.actividadDelete);
export const actividadUpdateVinculacion = createRequest(
  urls.actividadUpdateVinculacion
);
export const createCriterio = createRequest(urls.criterioNew);
export const deleteCriterio = createRequest(urls.criterioDelete);
export const createEvidencia = createRequest(urls.evidenciaNew);
export const deleteEvidencia = createRequest(urls.evidenciaDelete);
export const documentosCreate = createRequest(urls.documentosNew);
export const documentosDelete = createRequest(urls.documentosDelete);
export const documentosBatch = createRequest(urls.documentosBatch);
export const saveAll = createRequest(urls.saveAll);
export const validateFiles = createRequest(urls.validateFiles);
