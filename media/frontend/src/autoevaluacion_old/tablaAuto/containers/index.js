import React from 'react';

import { Table, TableHeader } from 'common/components/table';
import TableBodyAuto from './TableBodyAuto';

const headers = [
  {display: '', customStyle: {width: '5%'}},
  {display: 'Indicadores', customStyle: {width: '65%'}},
  {display: 'Estado', customStyle: {width: '30%'}},
];

const TablaAuto = () => (
  <div>
    <Table bordered>
      <TableHeader headers={headers}/>
      <TableBodyAuto />
    </Table>
  </div>
);

export default TablaAuto;
