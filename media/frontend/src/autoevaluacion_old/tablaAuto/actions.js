import zipWith from 'lodash/zipWith';
import _keyBy from 'lodash/keyBy';
import _ from 'lodash';

import initialState from 'medicion/initialState';
import constants from 'autoevaluacion/constants';
const {
  SET_ERRORS,
  ALL_STORED,
  ALL_FETCHING,
  ALL_ERROR,
  FINISHED_REQUEST,
} = constants;
import {
  isRequiredByValoracion,
  shouldActividadesBeRequired,
} from './estandares/reducer';

import { formatEstandarShortName } from 'common/formatter';
import { getContext } from 'common/context';
import { getIds } from './estandares/actividades/reducer';
import { getPayloadEstandares, getIdByEstandar } from './estandares/reducer';
import {
  getPayloadCriterios,
  getCriterios,
} from './estandares/criterios/reducer';
import {
  getPayloadEvidencias,
  getEvidencias,
} from './estandares/evidencias/reducer';
import {
  getPayloadDocumentos,
  getFiles,
} from './estandares/documentos/reducer';
import { getPayloadActividades } from './estandares/actividades/reducer';

export const setErrors = errors => ({
  type: SET_ERRORS,
  errors,
});

export const finishedRequest = () => ({
  type: FINISHED_REQUEST,
});

export const allStored = () => ({
  type: ALL_STORED,
});

export const allFetching = id => ({
  type: ALL_FETCHING,
  id,
});

export const allError = (msg = 'Ocurrió un error.') => dispatch => {
  dispatch({
    type: ALL_ERROR,
  });
  alert(msg);
};

const removeOverlay = () => {
  const elem = document.getElementById('auto_overlay');
  if (elem) {
    elem.parentElement.removeChild(elem);
  }
};

const allSaveStored = () => dispatch => {
  removeOverlay();
  dispatch(allStored());
};

const allSaveFetching = () => dispatch => {
  var overlay = document.createElement('div');
  overlay.id = 'auto_overlay';
  var inner = document.createElement('div');
  inner.id = 'auto_inner_overlay';
  inner.innerHTML =
    '<div><i class="fa fa-refresh fa-animate"></i> Guardando</div>';
  overlay.appendChild(inner);
  document.querySelector('body').appendChild(overlay);

  dispatch(allFetching());
};

const allSaveError = msg => dispatch => {
  removeOverlay();
  dispatch(allError(msg));
};

const getErrors = errors =>
  [
    errors[0] ? 'Se debe agregar por lo menos un medio de verificacion. ' : '',
    errors[1]
      ? 'Si el indicador cumple debe tener por lo menos un documento. '
      : '',
    errors[2] ? 'Cada documento debe tener un estado concluído. ' : '',
  ].join('');

const extractErrors = getState => {
  const errors = getState()
    .getIn(['auto_detalles', 'ids'])
    .toJS()
    .map(id => {
      const evidenciasIds = getEvidencias(getState().get('auto_evidencias'), id)
        .map(e => e.get('auev_id'))
        .toJS();
      //TODO check no validation at all
      // if( !isRequiredByValoracion(getState().get('auto_detalles'), id) ){
      //   return {
      //     id,
      //     errors: [ false, false, false, false, false ],
      //   }
      // }
      const estado = getState().getIn([
        'auto_detalles',
        'detalles',
        id,
        'aude_valoracion',
      ]);
      return {
        id,
        errors: [
          estado != getContext().estado[2].id && evidenciasIds.length === 0,
          estado != getContext().estado[1].id || !estado
            ? false
            : evidenciasIds
                .map(
                  evId => getFiles(getState().get('auto_documentos'), evId).size
                )
                .reduce((acc, i) => acc + i, 0) == 0,
          estado != getContext().estado[2].id &&
            evidenciasIds
              .map(
                evId =>
                  getFiles(getState().get('auto_documentos'), evId)
                    .toJS()
                    .filter(f => f.audo_estado == getContext().estados[0].id)
                    .length > 0
              )
              .reduce((acc, f) => acc || f, false),
        ],
      };
    })
    .reduce((acc, info) => {
      acc[info.id] = info.errors;
      return acc;
    }, {});

  const logicErrors = getContext().estandares.map(e =>
    getErrors(
      errors[getIdByEstandar(getState().get('auto_detalles'), e.id)] || [
        false,
        false,
        false,
      ]
    )
  );

  const estandarRaws = [...document.querySelectorAll('.estandar__raw')];
  const inlineErrors = [...document.querySelectorAll('.estandar__row')].map(
    (item, i) =>
      $(item).find('.bg-danger').length > 0 ||
      $(estandarRaws[i]).find('.bg-danger').length > 0
        ? 'Faltan llenar campos obligatorios'
        : ''
  );

  return logicErrors
    .map((errors, i) => ({
      esta_id: getContext().estandares[i].id,
      title: getContext().estandares[i].display,
      errors: errors + inlineErrors[i],
    }))
    .filter(item => item.errors.length > 0);
};

const getPayload = getState => {
  return {
    auto_detalles: getPayloadEstandares(getState().get('auto_detalles')),
    auto_medio: getPayloadEvidencias(getState().get('auto_evidencias')),
    auto_documentos: getPayloadDocumentos(getState().get('auto_documentos')),
  };
};

const saveAllData = (dispatch, getState, api, cb, validate) => {
  const payload = getPayload(getState);

  const shouldNotSave =
    Object.keys(payload)
      .map(k => payload[k].length)
      .reduce((acc, i) => acc + i, 0) === 0;

  const zipErrors = extractErrors(getState);
  if (cb) {
    dispatch(setErrors(zipErrors));
  }

  if (shouldNotSave && !cb) {
    return;
  }

  console.log('saveAll payload', payload);
  dispatch(allSaveFetching());

  api.saveAll({
    data: { ...payload, estado: zipErrors.length > 0 ? 0 : 1 },
    onSuccess: () => {
      if (cb && !validate) {
        cb(zipErrors);
        return dispatch(allSaveStored());
      }
      if (cb && validate) {
        cb(zipErrors);
      } else {
        dispatch(allSaveStored());
      }
    },
    onError: err => {
      console.log('err', err);
      dispatch(allSaveError());
    },
  });
};

export const saveAll = cb => (dispatch, getState, { api }) => {
  if (cb) {
    return saveAllData(dispatch, getState, api, cb);
  }
  saveAllData(dispatch, getState, api);
};

const finish = () => {
  var form = $(document.createElement('form'));
  $(form).attr('method', 'POST');
  $(form).attr('action', '/autoevaluacion/finish');
  form.appendTo(document.body);
  $(form).submit();
};
const validate = value => !!+value;

const validateAvanceAndFactor = item =>
  validate(item.nivel_factor) && validate(item.nivel_avance);

const validateMedicion = data => {
  const obj = initialState(data.__INITIAL_STATE__, data.__CONTEXT__);
  const medicionData = data.__INITIAL_STATE__;
  let noError = true;
  noError = medicionData.auto_detalle_actividad
    .map(validateAvanceAndFactor)
    .reduce((acc, v) => acc && v, noError);
  noError = medicionData.auto_actividades
    .map(validateAvanceAndFactor)
    .reduce((acc, v) => acc && v, noError);
  if (!noError) {
    return false;
  }
  const grouped = {};
  const estandaresByFactor = _keyBy(data.__CONTEXT__.estandares, e => e.id);
  const autoDetallesByAude = _keyBy(
    data.__CONTEXT__.auto_detalles,
    e => e.aude_id
  );
  const actividadByFactor = _keyBy(medicionData.auto_actividades, act => {
    const esta_id = autoDetallesByAude[act.aude_id].esta_id;
    return estandaresByFactor[esta_id].fact_id;
  });
  data.__CONTEXT__.factores.forEach(f => {
    grouped[f.id] = {};
    const act = actividadByFactor[f.id];
    if (act) {
      grouped[f.id][act.auac_id] = act.nivel_factor;
    }
  });
  data.__CONTEXT__.factores.forEach(f => {
    const vinculos = obj.context.vinculosFactor[f.id];
    if (vinculos) {
      Object.keys(vinculos).forEach(act => {
        const elem = vinculos[act].ids.length && vinculos[act].ids[0];
        if (elem && !grouped[f.id][act]) {
          const [auac_id, aude_id] = elem.split('_');
          grouped[f.id][
            act
          ] = data.__INITIAL_STATE__.auto_detalle_actividad.filter(
            detalle => detalle.auac_id == auac_id && detalle.aude_id == aude_id
          )[0].nivel_factor;
        }
      });
    }
  });
  Object.keys(grouped).forEach(f => {
    const value = Object.keys(grouped[f])
      .map(k => grouped[f][k])
      .reduce((acc, item) => (acc += +item), 0);
    if (value > 0 && value != 100) {
      noError = false;
    }
  });
  return noError;
};

const isValid = (num, pattern) => new RegExp(pattern).test(num);

const validateFormatos = data => {
  const formatos = [
    ...data.__INITIAL_STATE__.lic_a,
    ...data.__INITIAL_STATE__.lic_c,
    ...data.__INITIAL_STATE__.cond,
  ];
  return _.every(
    formatos,
    f => f.docu_id && f.estado && new RegExp('^[0-9]+$').test(f.da)
  );
};

export const closeFicha = () => (dispatch, getState, { api }) => {
  api.validateFiles({
    payload: {},
    onSuccess: ({ valido, msg }) => {
      if (!valido) {
        msg
          ? alert(msg)
          : alert(
              'Existen documentos sin vinculación, no se puede terminar la ficha'
            );
        return;
      }
      saveAllData(
        dispatch,
        getState,
        api,
        err => {
          dispatch(allSaveStored());
          if (err.length) {
            return alert('Existen errores. No se puede finalizar la ficha');
          }
          finish();
          /*
      api.formatos({
        data: {},
        onSuccess: res => {
          dispatch(allSaveStored())
          const isFormatoOk = validateFormatos(res)
          if (!isFormatoOk) {
            return alert(
              'Existen errores en la vista formatos, se deben corregir para finalizar la ficha'
            )
          }
          finish()
        },
        onError: err => {
          console.log(err)
          dispatch(allSaveError())
        },
      })
      */
        },
        true
      );
    },
    onError: err => {
      console.log('err', err);
    },
  });
};
