import { fromJS } from 'immutable';
import { createReducer } from 'redux-immutablejs';

import constants from 'autoevaluacion/constants';
const {
  ADD_ACTIVIDAD,
  REMOVE_ACTIVIDAD,
  UPDATE_ACTIVIDAD,
  UPDATE_VINCULOS,
  UPDATE_PROP_ACTIVIDAD,
  ALL_STORED,
} = constants;

const initialState = fromJS({
  ids: [],
  items: {},
  estandares: {},
});

export default createReducer(initialState, {
  [ALL_STORED]: (state) => state.withMutations(state => {
    state.get('ids').forEach(id => {
      state.setIn(['items', id, 'hasChanged'], false);
    })
  }),

  [UPDATE_ACTIVIDAD]: (state, {id, value}) =>
    state.setIn(['items', id, 'descripcion'], value)
      .setIn(['items', id, 'hasChanged'], true),

  [UPDATE_VINCULOS]: (state, { added, removed, id }) =>
    state.withMutations(state => {
      removed.forEach(aude_id => {
        state
          .updateIn(['estandares', aude_id], es => es.filterNot(e => e == id))
      })
      added.forEach(aude_id => {
        state.updateIn(['estandares', aude_id], (es = fromJS([])) => es.push(id))
      })
    }),

  [UPDATE_PROP_ACTIVIDAD]: (state, { id, prop, value }) =>
    state.setIn(['items', id, prop], value),

  [ADD_ACTIVIDAD]: (state, { actividad, aude_id }) =>
    state
    .update('ids', ids => ids.push(actividad.id))
    .setIn(['items', actividad.id], fromJS(actividad))
    .updateIn(['estandares', aude_id], (es = fromJS([])) => es.push(actividad.id)),

  [REMOVE_ACTIVIDAD]: (state, { id, aude_id }) => {
    const estandares = state.getIn(['items', id, 'estandares'])
          .push(state.getIn(['items', id, 'aude_id']));
    return state.withMutations(state => {
      state.update('ids', ids => ids.filterNot(i => i == id))
        .deleteIn(['items', id])
      estandares.forEach(aude_id => {
        state
          .updateIn(['estandares', aude_id], es => es.filterNot(e => e == id))
      })
    })
  }
});

export const getIds = (state, aude_id) => {
  return state.getIn(['auto_actividades', 'estandares', aude_id]) || fromJS([]);
}

export const getActividades = (state, aude_id) => {
  return getIds(state, aude_id).map(id => state.getIn(['auto_actividades', 'items', id]))
}

export const getActividad = (state, id) =>
  state.getIn(['auto_actividades', 'items', id]);

export const getPayloadActividades = (state) =>
  state.get('ids').filter(id => state.getIn(['items', id, 'hasChanged']))
    .map(id => state.getIn(['items', id]))
    .toJS()
  .map(o => {
    return {
      auac_id: o.id,
      act_cod: o.act_cod,
      descripcion: o.descripcion
    }
  });


export const getStatusActividades = (state) =>
  state.get('ids')
  .map(id => fromJS({
    hasChanged: state.getIn(['items', id, 'hasChanged'])
  }));
