import React, { Component } from 'react';
import groupBy from 'lodash/groupBy';
import flatMap from 'lodash/flatMap';
import { connect } from 'react-redux';

import Button from 'common/components/buttons';
import { getDetalles } from '../../reducer';
import { Table, TableHeader } from 'common/components/table';
import { getContext } from 'common/context';

const headers = [
  {display: 'DIMENSION', customStyle: {width: '25%'}},
  {display: 'FACTOR', customStyle: {width: '25%'}},
  {display: 'ESTANDAR', customStyle: {width: '25%'}},
  {display: 'RELACION', customStyle: {width: '25%'}},
];

class VinculosActividades extends Component {
  constructor(props) {
    super(props);
    const estandares = props.estandares.toJS();
    const detalles = props.detalles.toJS();
    const actual = detalles[props.aude_id].esta_id;
    const selected = {[actual]: true};

    this.state = {
      actual,
      selected: estandares.reduce((acc, aude_id) => {
        acc[detalles[aude_id].esta_id] = true;
        return acc;
      }, selected),
    }
  }

  toggleSelection = (id) => {
    this.setState({
      selected: {...this.state.selected, [id]: !this.state.selected[id]},
    })
  }

  onSave = () => {
    const {
      selected
    } = this.state;
    const detalles = this.props.detalles.toJS();
    const selection = Object.keys(selected).filter(k => selected[k] && k != this.state.actual);
    const ids = selection.map(id => {
      return Object.keys(detalles).filter(k => {
        return detalles[k].esta_id == id;
      }).map(k => detalles[k].aude_id)[0];
    })
    this.props.updateVinculos(this.props.id, ids, this.props.close);
  }

  render() {
    let dimensiones = getContext().dimensiones;
    const factoresGrouped = groupBy(getContext().factores, f => f.dime_id);
    const estandaresGrouped = groupBy(getContext().estandares, e => e.fact_id);
    dimensiones = dimensiones.map(d => {
      const size = factoresGrouped[d.id]
        .map(f => estandaresGrouped[f.id].length)
        .reduce((acc, v) => acc + v, 0);
      return ({...d, size})
    })

    const Buttons = (
        <div style={{marginLeft: '-5px', marginTop: '10px', marginBottom: '10px'}}>
        <Button
		      danger
          onClick={this.props.close}
        >
        Cancelar
        </Button>
        <Button
          primary
          onClick={this.onSave}
        >
        Vincular
        </Button>
      </div>
    )

    return (
      <div>

        <h2>
        Marcar  las vinculaciones que  tiene esta actividad con el logro de otros estándares
        </h2>
        <hr/>
        {Buttons}
        <Table bordered>
          <TableHeader headers={headers}/>
          <tbody>
          {
            dimensiones.map(d => {
              return flatMap(factoresGrouped[d.id], (f, fi) => {
                return flatMap(estandaresGrouped[f.id], (e, ei) => {
                  return (
                    <tr>
                      {
                        fi === 0 && ei == fi && (
                          <td rowSpan={d.size}>{d.display}</td>
                        )
                      }
                      {
                        ei === 0 && (
                          <td rowSpan={estandaresGrouped[f.id].length}>{f.display}</td>
                        )
                      }
                      <td>{e.display}</td>
                      <td>
                      <input
                      style={{display: 'block', margin: '0 auto'}}
                        type="checkbox"
                        disabled={e.id == this.state.actual}
                        checked={!!this.state.selected[e.id]}
                        onChange={() => this.toggleSelection(e.id)}
                      />
                      </td>
                    </tr>
                  )
                })
              })
            })
          }
          </tbody>
        </Table>
        {Buttons}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  detalles: getDetalles(state.get('auto_detalles'))
});

export default connect(mapStateToProps)(VinculosActividades);
