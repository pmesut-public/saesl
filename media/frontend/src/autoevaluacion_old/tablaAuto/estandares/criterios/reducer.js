import { fromJS, Map } from 'immutable';
import { createReducer } from 'redux-immutablejs';
import pick from 'lodash/pick';

import constants from 'autoevaluacion/constants';
const {
  UPDATE_CRITERIO,
  ADD_CRITERIO,
  REMOVE_CRITERIO,
  ALL_STORED,
} = constants;

const initialState = fromJS([]);

const findIndex = (list, id) => list.findKey(e => e.get('aucr_id') === id);

export default createReducer(initialState, {
  [ADD_CRITERIO]: (state, {id, aude_id}) =>
    state.push(fromJS({descripcion: '', aucr_id: id, aude_id})),

  [REMOVE_CRITERIO]: (state, {id}) =>
    state.delete(findIndex(state, id)),

  [UPDATE_CRITERIO]: (state, {id, value}) => {
    const idx = findIndex(state, id);
    return state
      .setIn([idx, 'descripcion'], value)
      .setIn([idx, 'hasChanged'], true);
  },

  [ALL_STORED]: (state) =>
    state
    .asMutable()
    .map(item => item.set('hasChanged', false))
    .asImmutable(),

});

export const getCriterios = (state, audeId) =>
  state.filter(c => c.get('aude_id') == audeId);

export const getStatusCriterios = (state) =>
  state.map(c => fromJS({
    hasChanged: c.get('hasChanged'),
  }));

export const getPayloadCriterios = (state) =>
  state.filter(c => c.get('hasChanged'))
  .toJS()
  .map(o =>
       pick(o, ['aucr_id', 'descripcion'])
  );
