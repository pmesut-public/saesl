import constants from 'autoevaluacion/constants';
const {
  UPDATE_CRITERIO,
  ADD_CRITERIO,
  REMOVE_CRITERIO,
} = constants;

import * as messages from 'autoevaluacion/messages';

import {
  allFetching,
  allError,
  finishedRequest,
} from 'autoevaluacion/tablaAuto/actions';

const addCriterio = (id, aude_id) => ({
  type: ADD_CRITERIO,
  id,
  aude_id,
});

export const addCriterioAPI = (aude_id) => (dispatch, _, {api}) => {
  dispatch(allFetching());
  api.createCriterio({
    data: {aude_id, descripcion: ''},
    onSuccess: ({ aucr_id }) => {
      dispatch(finishedRequest())
      dispatch(addCriterio(aucr_id, aude_id));
    },
    onError: (err) => {
      dispatch(allError(messages.ERROR_CREATE_CRITERIO));
      console.log('err', err);
    },
  });
};

export const updateCriterio = (id, value) => ({
  type: UPDATE_CRITERIO,
  id,
  value,
});

const removeCriterio = (id) => ({
  type: REMOVE_CRITERIO,
  id,
});

export const removeCriterioAPI = (id) => (dispatch, _, {api}) => {
  if (!confirm(messages.CONFIRM_REMOVE_CRITERIO)) {
    return;
  }
  dispatch(allFetching());
  api.deleteCriterio({
    data: {aucr_id: id},
    onSuccess: () => {
      dispatch(finishedRequest())
      dispatch(removeCriterio(id));
    },
    onError: (err) => {
      dispatch(allError(messages.ERROR_REMOVE_CRITERIO));
      console.log('err', err)
    },
  });
};
