import React, { Component } from 'react';

import { TextAreaField } from 'common/components/Form';
import { RemoveButton } from 'common/components/buttons';

class Criterio extends Component {
  _onChange = (value) => {
    const {
      criterio,
      onChange,
    } = this.props;
    onChange(criterio.get('aucr_id'), value);
  }

  _onRemove = () => {
    const {
      criterio,
      onRemove,
    } = this.props;
    onRemove(criterio.get('aucr_id'));
  }

  render() {
    const {
      criterio,
      isFetching,
      canRemove,
      required,
    } = this.props;

    return (
      <div className="row row_item" style={{marginRight: 0}}>
        <TextAreaField
      required={required}
      classWrapper="col-sm-10"
      value={criterio.get('descripcion')}
      onChange={this._onChange}
      rows="10"
        />

        <div className="col-sm-2">
        {
          canRemove && (
            <RemoveButton
              disabled={isFetching}
              onClick={this._onRemove}
            />
          )
        }
        </div>


      </div>
    );
  }
}

export default Criterio;

