import React, { Component } from 'react';
import { connect } from 'react-redux';

import { AddButton } from 'common/components/buttons';
import { getCriterios } from '../reducer';
import { addCriterioAPI, updateCriterio, removeCriterioAPI } from '../actions';
import { isFetching } from 'autoevaluacion/tablaAuto/reducer';
import Criterio from '../components/Criterio';

class Criterios extends Component {
  render() {
    const {
      criterios,
      addCriterio,
      updateCriterio,
      removeCriterio,
      isFetching,
      required,
    } = this.props;

    return (
      <div>
        {
          criterios.map(c => (
            <Criterio
              key={c.get('aucr_id')}
              criterio={c}
              required={required}
              canRemove={criterios.size !== 1}
              onChange={updateCriterio}
              onRemove={removeCriterio}
              isFetching={isFetching}
            />
          ))
        }
        <AddButton
          onClick={addCriterio}
          text="Nuevo criterio"
          size="md"
          disabled={isFetching}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, {id}) => ({
  criterios: getCriterios(state.get('auto_criterios'), id),
  isFetching: isFetching(state.get('system_status'))
});
const mapDispatchToProps = (dispatch, {id}) => ({
  addCriterio: () => dispatch(addCriterioAPI(id)),
  updateCriterio: (criterioId, value) =>
    dispatch(updateCriterio(criterioId, value)),
  removeCriterio: (criterioId) => dispatch(removeCriterioAPI(criterioId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Criterios);
