import difference from 'lodash/difference'
import intersection from 'lodash/intersection'

import { estadoOptions } from './containers/FileItem'
import constants from 'autoevaluacion/constants'
const {
  MERGE_FILES_SELECTED,
  UPDATE_FILE_ESTADO,
  UPDATE_PROP_FILE,
  REPLACE_DOCS,
} = constants
import { getSelectedFiles } from 'documentos/fileViewer/reducer'
import {
  getFiles,
  getEvidenciaId,
  getFileEstados,
  getFileNameById,
} from './reducer'
import { getDetalleId } from '../evidencias/reducer'
import { updateDetalleEstandarValoracion } from '../actions'

export const replaceDocs = newState => ({
  type: REPLACE_DOCS,
  newState,
})

export const updateProp = (id, prop, value) => ({
  type: UPDATE_PROP_FILE,
  id,
  prop,
  value,
})

export const updateEstado = (id, value) => ({
  type: UPDATE_FILE_ESTADO,
  id,
  value,
})

const mergeFiles = (auevId, files) => ({
  type: MERGE_FILES_SELECTED,
  auevId,
  files,
})

export const updateSelection = auevId => (dispatch, getState, { api }) => {
  const selectedFiles = getSelectedFiles(getState().get('files'))
  const existingFiles = getFiles(getState().get('auto_documentos'), auevId)
  const selectedIds = selectedFiles.map(f => f.get('id')).toJS()
  //const selectedIds = ['198', '1']
  const existingIds = existingFiles.map(f => f.get('docu_id')).toJS()
  const removedFiles = difference(existingIds, selectedIds)
  const addedFiles = difference(selectedIds, existingIds)
  const intersectionIds = intersection(existingIds, selectedIds)
  const filesToRemove = removedFiles.map(
    id => existingFiles.filter(e => e.get('docu_id') == id).toJS()[0].audo_id
  )

  const payload = {
    new: addedFiles.map(id => ({
      docu_id: id,
      aume_id: auevId,
    })),
    delete: filesToRemove.map(id => ({ audo_id: id })),
  }
  console.log(removedFiles, addedFiles, intersectionIds)
  api.documentosBatch({
    data: payload,
    onSuccess: data => {
      /*
    onError: () => {
      const data = {
        documentos: [
          {
            aume_id: auevId,
            docu_id: 1177,
            audo_id: addedFiles[0].id,
            audo_estado: '0',
            audo_subestado: '7',
            path: 'uploads/licenciamiento/20380/ZtO8oQDS-20180305165337.png',
            display: '42540-200.png',
          },
        ],
        status: 'OK',
        msg: '',
      }
      */
      const newData = data.documentos.map(d => ({
        ...d,
        id: d.audo_id,
        auev_id: d.aume_id,
      }))
      const files = intersectionIds.map(
        id => existingFiles.toJS().filter(e => e.docu_id == id)[0]
      )
      const mergedFiles = newData.concat(files).map(f => ({
        ...f,
        display:
          getFileNameById(getState().get('files'), f.docu_id) || f.display,
      }))
      dispatch(mergeFiles(auevId, mergedFiles))
    },
    onError: err => console.log('err', err),
  })
}

export const replaceFiles = (pastId, newId, newFiles) => (
  dispatch,
  getState
) => {
  console.log('id', pastId, newId)
  const newFileIm = getState()
    .get('files')
    .find(d => d.get('id') == newId)
    .toJS()
  const newFile = {
    ...newFileIm,
    id: newId,
    display: newFileIm.display || newFileIm.name,
  }
  console.log('new file', newFile)
  console.log(
    'replace docs',
    getState()
      .get('auto_documentos')
      .filter(d => d.get('docu_id') == pastId)
      .toJS()
  )
  console.log(
    getState()
      .get('auto_documentos')
      .toJS()
  )
  const newState = getState()
    .get('auto_documentos')
    .filter(d => d.get('docu_id') != pastId)
    .toJS()
    .concat(
      newFiles.map(d => ({
        ...d,
        id: d.audo_id,
        name: d.titulo,
        auev_id: d.aume_id,
      }))
    )
  console.log(newState)

  dispatch(replaceDocs(newState))

  /*
    .forEach(d => {
      const auevId = d.get('auev_id')
      const existingFiles = getFiles(getState().get('auto_documentos'), auevId)
        .filter(d => d.get('audo_id') != pastId)
        .toJS()
      console.log('auevid', auevId, existingFiles.toJS())
      dispatch(
        mergeFiles(
          auevId,
          existingFiles.concat({ ...newFile, auev_id: auevId })
        )
      )
    })
    */

  console.log('end replace')
}
