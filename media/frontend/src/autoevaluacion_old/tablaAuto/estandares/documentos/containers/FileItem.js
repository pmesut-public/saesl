import React, { Component } from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import { Glyphicon } from 'react-bootstrap'

import * as colors from 'common/colors'
import { getContext } from 'common/context'
import Status from 'common/components/Status'
import { InputField } from 'common/components/Form'
import RangeDatePicker from 'common/components/Form/RangeDatePicker'
import {
  getVigenciaValues,
  vigenciaTitle,
  getEstadoLetter,
} from 'licenciamiento/formatos/containers/Formato'
import StatusBadge from 'common/components/StatusBadge'
import Button from 'common/components/buttons'
import { PullLeft, PullRight, Clearfix } from 'common/components/Layout'

const styleLetter = {
  position: 'absolute',
  color: 'black',
  left: '50%',
  top: '50%',
  transform: 'translate(-50%, -50%)',
}

class FileItem extends Component {
  onRangeChange = (fechaInicio, fechaFin) => {
    this.onChange('fecha_inicio', fechaInicio)
    this.onChange('fecha_fin', fechaFin)
  }

  onChange = (prop, value) => {
    const { file, updateProp } = this.props
    const { docu_id } = file

    updateProp(docu_id, prop, value)
  }
  

  render() {
    const { file, updateProp } = this.props
    const {
      docu_id,
      display,
      name,
      audo_estado,
      audo_subestado,
      path,
      fecha_inicio,
      fecha_fin,
      da,
    } = file

    const estados = getContext().estados
    estados[0].color = 'yellow'
    estados[1].color = 'green'
    const rangeSelected = fecha_inicio && fecha_fin
    const vigencia = getVigenciaValues(file)
    const title = file.get('display') || file.get('name')

    return (
      <div className="col-md-12 col-sm-12 file__item_v2">
        <div
          className="file__icon_v2"
          title={title}
          style={{ display: 'inline-block' }}
        >
          {/*<i
            className="fa fa-file"
            style={{
              color:
                audo_estado == getContext().estados[1].id
                  ? 'rgb(40, 167, 69)'
                  : 'rgb(255, 193, 7)',
            }}
          />*/}                  
          <a
            href={`/file/${path}`}
            target="_blank"
            title={title}
            style={{ display: 'block', marginTop: '5px' }}
          >
            <i
            className="fa fa-file"
            style={{
              color: '#758c8f',
            }}
          />
            <span style={{fontSize: '1.2rem', marginLeft: '1rem'}}>{title}</span>
            {/*<Button>
              <Glyphicon glyph="download-alt" />
          </Button>*/}
          </a>
        </div>
        {/*<div
          className="evidencia__files__toolbar"
          style={{ display: 'inline-block' }}
        >
          <div className="status__detail">
            <StatusBadge
              size="25px"
              estado={audo_subestado}
              list={getContext().sub_estados}
            >
              <div style={styleLetter}>{getEstadoLetter(audo_estado)}</div>
            </StatusBadge>
            <div style={{ width: '10px', height: '5px' }} />
            {audo_estado == getContext().estados[1].id &&
              vigencia.a && (
                <Status
                  size="25px"
                  status={vigencia.a}
                  title={vigenciaTitle[vigencia.a]}
                />
              )}
          </div>
              </div>*/}
      </div>
    )

    /*
    return (
      <tr>
        <td>
          <div className="file__icon" title={display || name}>
            <i className="fa fa-file" />
          </div>
          <a href={`/file/${path}`} target="_blank">
            <Button>
              <Glyphicon glyph="download-alt" />
            </Button>
          </a>
        </td>
        <td>
          <StatusBadge
            estado={sub_estado}
            list={getContext().sub_estados}
            style={{ margin: '0 auto' }}
          />
        </td>
        <td>
          <RangeDatePicker
            onChange={this.onRangeChange}
            startTime={fecha_inicio}
            endTime={fecha_fin}
          />
        </td>
        <td style={{ minWidth: '40px' }}>{vigencia.dv}</td>
        <td style={{ minWidth: '100px' }}>
          <InputField
            value={da || ''}
            onChange={val => this.onChange('da', val)}
            pattern={'[0-9]+$'}
            disabled={!rangeSelected}
            required
          />
        </td>
        <td>{vigencia.ed}</td>
        <td>
          <Status status={vigencia.a} style={{ margin: '0 auto' }} />
        </td>
      </tr>
    )
    */
  }
}

const mapStateToProps = () => ({})
const mapDispatchToProps = (dispatch, { file }) => ({
  updateEstado: value => dispatch(updateEstado(file.get('id'), value)),
})

export default connect(mapStateToProps, mapDispatchToProps)(FileItem)
