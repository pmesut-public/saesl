import { fromJS } from 'immutable'
import { createReducer } from 'redux-immutablejs'
import pick from 'lodash/pick'

import constants from 'autoevaluacion/constants'
const {
  MERGE_FILES_SELECTED,
  UPDATE_FILE_ESTADO,
  ALL_STORED,
  UPDATE_PROP_FILE,
  REPLACE_DOCS,
} = constants

const initialState = fromJS([])

const findIndex = (list, id) => list.findKey(e => e.get('id') === id)

export default createReducer(initialState, {
  [REPLACE_DOCS]: (state, { newState }) => fromJS(newState),
  [UPDATE_PROP_FILE]: (state, { id, prop, value }) => {
    const idx = state.findKey(e => e.get('docu_id') == id)
    return state.setIn([idx, prop], value).setIn([idx, 'hasChanged'], true)
  },

  [MERGE_FILES_SELECTED]: (state, { auevId, files }) =>
    state.filter(f => f.get('auev_id') != auevId).concat(fromJS(files)),

  [UPDATE_FILE_ESTADO]: (state, { id, value }) => {
    const idx = findIndex(state, id)
    return state
      .setIn([idx, 'audo_estado'], value)
      .setIn([idx, 'hasChanged'], true)
  },

  [ALL_STORED]: state =>
    state
      .asMutable()
      .map(item => item.set('hasChanged', false))
      .asImmutable(),
})

export const getFiles = (state, auevId) => {
  return state.filter(c => c.get('auev_id') == auevId)
}

export const getFileNameById = (state, id) => {
  const found = state.find(f => f.get('id') == id)
  return found && found.get('display')
}

export const allFileWithoutStatus = (state, auevId) => {
  const files = getFiles(state, auevId)
  if (!files.size) return true
  return files.filter(f => f.get('audo_estado') === '').size > 0
}

export const getEvidenciaId = (state, id) =>
  state.filter(f => f.get('id') == id).getIn([0, 'auev_id'])

export const getFileEstados = (state, auevId) =>
  getFiles(state, auevId).map(f => f.get('audo_estado'))

export const getStatusDocumentos = state =>
  state.map(c =>
    fromJS({
      hasChanged: c.get('hasChanged'),
    })
  )

export const getPayloadDocumentos = state =>
  state
    .filter(c => c.get('hasChanged'))
    .toJS()
    .map(o =>
      pick(o, [
        'audo_id',
        'audo_estado',
        'audo_sube',
        'fecha_inicio',
        'fecha_fin',
        'da',
      ])
    )
