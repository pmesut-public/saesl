import React, { Component } from 'react'
import { connect } from 'react-redux'

import { isFetching } from 'autoevaluacion/tablaAuto/reducer'
import { AddButton, RemoveButton } from 'common/components/buttons'
import { getIdByEstandar } from '../reducer'
import { Table, TableHeader } from 'common/components/table'
import Actividades from '../actividades/containers'
import Criterios from '../criterios/containers'
import Evidencias from '../evidencias/containers'
import { Justificacion } from '../containers'
import { getContext } from 'common/context'
import { isRequiredByValoracion } from '../reducer'
import ConditionalRow from '../containers/ConditionalRow'
import { addEvidenciaAPI } from '../evidencias/actions'

class EstandarRowInformation extends Component {
  // componentDidMount() {
  //   $(this.justificacionTitle).tooltip({
  //     'title': 'Explicación / descripción de la manera en que las actividades ejecutadas han generado el logro / no logro del estándar',
  //   });
  //   $(this.evidenciasTitle).tooltip({
  //     'title': 'Dar cuenta de la ejecución de las actividades descritas en la sección anterior',
  //   });
  //   $(this.documentosTitle).tooltip({
  //     'title': 'Si el caso lo amerita, adjuntar las evidencias documentales asociadas a la planificación / ejecución de las actividades',
  //   });
  //   $(this.actividadesTitle).tooltip({
  //     'title': 'Conjunto de acciones, procesos, situaciones (ejecutadas o por ejecutar) del programa, que responden a lo planteado en el estándar',
  //   });
  // }

  setRefJust = el => (this.justificacionTitle = el)
  setRefEvid = el => (this.evidenciasTitle = el)
  setRefDoc = el => (this.documentosTitle = el)
  setRefAct = el => (this.actividadesTitle = el)
  setRefEst = el => (this.estadoTitle = el)

  render() {
    const {
      id,
      idDetalle,
      isRequired,
      addEvidenciaAPI,
      isFetching,
    } = this.props

    const headers = [
      //{ display: '', customStyle: { width: '2%' } },
      { display: 'Medios de verificación', customStyle: { width: '25%' } },
      // {display: 'Actividades', customStyle: {width: '21%'}, ref: this.setRefAct},
      /*{
        display: 'Observaciones/comentarios',
        customStyle: { width: '22%' },
        ref: this.setRefEvid,
      },*/
      {
        display: 'Documentos',
        customStyle: { width: '22%' },
        ref: this.setRefDoc,
      },
      {
        display: 'Observaciones/comentarios',
        customStyle: { width: '22%' },
        ref: this.setRefEvid,
      },
      {
        display: 'Estado',
        customStyle: { width: '22%' },
        ref: this.setRefEst,
      },
      /*{
        display: 'Consideraciones',
        customStyle: { width: '27%' },
        ref: this.setRefJust,
      },*/
    ]

    return (
      <tr className="estandar__row">
        <td />
        <td colSpan="2">
          <Table bordered>
            <TableHeader headers={headers} />
            <Evidencias id={idDetalle} required={true} esta_id={id} />
          </Table>
          {/*<AddButton
            onClick={() => addEvidenciaAPI(id, idDetalle)}
            text="Nuevo medio de verificacion"
            size="md"
            disabled={isFetching}
        />*/}
        </td>
      </tr>
    )
  }
}

const mapStateToProps = (state, { id }) => {
  const idDetalle = getIdByEstandar(state.get('auto_detalles'), id)
  return {
    id,
    idDetalle,
    isRequired: isRequiredByValoracion(state.get('auto_detalles'), idDetalle),
    isFetching: isFetching(state.get('system_status')),
  }
}

export default connect(mapStateToProps, { addEvidenciaAPI })(
  EstandarRowInformation
)
