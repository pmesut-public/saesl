import React, { Component } from 'react'
import Modal from 'react-modal'
import { connect } from 'react-redux'

import format from 'date-fns/format'
import FileItem from '../../documentos/containers/FileItem'
import { getFiles } from '../../documentos/reducer'
import { updateProp } from '../../documentos/actions'
import Button from 'common/components/buttons'
import DocumentosPicker from './DocumentosPicker'
import { Table, TableHeader } from 'common/components/table'
import { subHeaders } from 'licenciamiento/formatos/containers'

const headers = [
  { display: '', customStyle: { rowSpan: 2, style: { maxWidth: '60px' } } },
  { display: 'Estado', customStyle: { rowSpan: 2 } },
  { display: 'Vigencia', customStyle: { colSpan: 3, style: {} } },
  { display: `Estado act. al: ${format(new Date(), 'DD/MM/YYYY')}`, customStyle: { colSpan: 2 } },
]

class Documentos extends Component {
  state = { show: false }
  toggle = () => this.setState({ show: !this.state.show })
  render() {
    const { onClose, id, files, required, updateProp, editable = true } = this.props

    return (
      <div>
        <div style={{ marginBottom: '1rem' }}>
          <Table bordered>
            <tbody>
              {files.map(f => (
                <tr>
                  <td>
                    <FileItem
                      file={f}
                      key={f.get('docu_id')}
                      updateProp={updateProp}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <div >
          <Button
            info
            size="btn-md"
            onClick={this.toggle}
            title="Modificar archivos"
          >
            <i className="fa fa-pencil" />
          </Button>

        </div>
        <Modal isOpen={this.state.show} contentLabel="Modal">
          <DocumentosPicker id={id} onClose={this.toggle} />
        </Modal>
      </div>
    )

    /*return (
      <div className="row" style={{ marginLeft: 0, marginRight: 0 }}>
        {files.map(f => <FileItem key={f.get('docu_id')} file={f} />)}
        
        {editable && <div className="col-sm-12" style={{marginTop: '1rem'}}>

          <Button
            info
            size="btn-sm"
            onClick={this.toggle}
            title="Seleccionar archivos"
          >
            <i className="fa fa-folder-open" style={{marginRight: '1rem'}} />
            Seleccionar archivos
          </Button>

          <Modal isOpen={this.state.show} contentLabel="Modal">
          <DocumentosPicker id={id} onClose={this.toggle} />
        </Modal>
        </div>}       
      </div>
    )*/

    /*
    return (
      <div>
        <Table bordered>
          <thead>
            <TableHeader.Row headers={headers} />
            <TableHeader.Row headers={subHeaders} />
          </thead>
          <tbody>
            {files.map(f => (
              <FileItem
                file={f}
                key={f.get('docu_id')}
                updateProp={updateProp}
              />
            ))}
          </tbody>
        </Table>
        <div className="colmd-2 col-sm-4">
          <Button
            info
            size="btn-md"
            onClick={this.toggle}
            title="Modificar archivos"
          >
            <i className="fa fa-pencil" />
          </Button>
        </div>
        <Modal isOpen={this.state.show} contentLabel="Modal">
          <DocumentosPicker id={id} onClose={this.toggle} />
        </Modal>
      </div>
    )
    */
  }
}

const mapStateToProps = (state, { id }) => ({
  files: getFiles(state.get('auto_documentos'), id),
})

const mapDispatchToProps = { updateProp }

export default connect(mapStateToProps, mapDispatchToProps)(Documentos)
// export default Documentos;
