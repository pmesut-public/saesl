import React from 'react'
import { connect } from 'react-redux'

import Button from 'common/components/buttons'
import ToolBar from 'documentos/toolbar/containers'
import FileViewer from 'documentos/fileViewer/containers'
import { getSelectedFiles } from 'documentos/fileViewer/reducer'
import { updateSelection } from '../../documentos/actions'

const DocumentosPicker = ({
  onClose,
  updateSelection,
  id,
  noFilesSelected,
}) => (
  <div className="box">
    <div className="box-header">
      <ToolBar showVigencia />
    </div>
    <div className="box-body box-responsive">
      <FileViewer id={id} />
    </div>
    <Button danger onClick={onClose}>
      Cancelar
    </Button>
    <Button primary onClick={updateSelection}>
      Aceptar
    </Button>
  </div>
)

const mapStateToProps = (state, { id }) => ({
  noFilesSelected: getSelectedFiles(state.get('files')).size === 0,
})

const mapDispatchToProps = (dispatch, { id, onClose }) => ({
  updateSelection: () => {
    dispatch(updateSelection(id))
    onClose()
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(DocumentosPicker)
