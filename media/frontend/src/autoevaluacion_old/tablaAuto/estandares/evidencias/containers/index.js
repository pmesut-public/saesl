import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getContext } from 'common/context';
import { getValoracion, getIdByEstandar } from '../../reducer';
import { TextAreaField, SelectField } from 'common/components/Form';
import { AddButton, RemoveButton } from 'common/components/buttons';
import { isFetching } from 'autoevaluacion/tablaAuto/reducer';
import { getEvidencias } from '../reducer';
import {
  addEvidenciaAPI,
  updateEvidencia,
  removeEvidenciaAPI,
} from '../actions';
import Evidencia from '../components';
import Documentos from './Documentos';

class Evidencias extends Component {
  render() {
    const {
      evidencias,
      addEvidencia,
      updateEvidencia,
      removeEvidencia,
      isFetching,
      required,
      id,
      valoracion,
    } = this.props;

    const requiredConsideracion = valoracion != getContext().estado[0].id;
    const requiredEvidencia = valoracion != getContext().estado[2].id;

    const editable = getContext().editable;
    const medio_estado = getContext().medio_estado;
    //console.log({editable,medio_estado, context: getContext()});

    return (
      <tbody>
        {evidencias.map(e => (
          <tr key={e.get('auev_id')}>
            {/*<td>
              {!e.get('definido') && (
                <RemoveButton
                  disabled={isFetching}
                  onClick={() => removeEvidencia(e.get('auev_id'))}
                />
              )}
              </td>*/}
            <td>
              <h4><strong>{e.get('descripcion')}</strong></h4>
             
              <div>
              <small>{e.get('consideracion')}</small>
              </div>
              {/*<Evidencia
                required={requiredEvidencia}
                canRemove={evidencias.size !== 1}
                evidencia={e}
                onChange={updateEvidencia}
                onRemove={removeEvidencia}
                isFetching={isFetching}
            />*/}
            </td>
            <td>
              <Documentos id={e.get('auev_id')} required={required} editable={editable} />
            </td>
            <td>
              {/*<small>{e.get('observacion')}</small>*/}
              {editable ? <TextAreaField
                value={e.get('observacion')}
                onChange={value =>
                  updateEvidencia(e.get('auev_id'), 'observacion', value)
                }
                rows="10"
              />: <div>{e.get('observacion')}</div>}
            </td>
            <td>
              {/*<h4><strong>{e.get('estado')}</strong></h4>*/}
              {editable ? <SelectField
                defaultOption="Seleccione"
                options={getContext().medio_estado}
                required
                value={e.get('estado')}
                onChange={value =>
                  updateEvidencia(e.get('auev_id'), 'estado', value.target.value)
                }
              />: <h4><strong>{e.get('estado')}</strong></h4>}
            </td>
            {/*<td>
              
              <TextAreaField
                required={requiredConsideracion}
                value={e.get('consideracion')}
                onChange={value =>
                  updateEvidencia(e.get('auev_id'), 'consideracion', value)
                }
                rows="10"
              />
              </td>*/}
          </tr>
        ))}
      </tbody>
    );
  }
}

const mapStateToProps = (state, { id: detalleId, esta_id }) => ({
  evidencias: getEvidencias(state.get('auto_evidencias'), detalleId),
  valoracion: getValoracion(state.get('auto_detalles'), esta_id),
  isFetching: isFetching(state.get('system_status')),
});

const mapDispatchToProps = (dispatch, { id: detalleId }) => ({
  addEvidencia: () => dispatch(addEvidenciaAPI(detalleId)),
  updateEvidencia: (id, prop, value) =>
    dispatch(updateEvidencia(id, prop, value)),
  removeEvidencia: id => dispatch(removeEvidenciaAPI(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Evidencias);
