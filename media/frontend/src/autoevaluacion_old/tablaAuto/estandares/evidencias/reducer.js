import { fromJS } from 'immutable'
import { createReducer } from 'redux-immutablejs'
import pick from 'lodash/pick'

import constants from 'autoevaluacion/constants'
const {
  UPDATE_EVIDENCIA,
  ADD_EVIDENCIA,
  REMOVE_EVIDENCIA,
  ALL_STORED,
} = constants

const initialState = fromJS([])

const findIndex = (list, id) => list.findKey(e => e.get('auev_id') === id)

export default createReducer(initialState, {
  [ADD_EVIDENCIA]: (state, { id, aude_id }) =>
    state.push(fromJS({ titulo: '', auev_id: id, aude_id })),

  [REMOVE_EVIDENCIA]: (state, { id }) => state.delete(findIndex(state, id)),

  [UPDATE_EVIDENCIA]: (state, { id, prop, value }) => {
    const idx = findIndex(state, id)
    return state.setIn([idx, prop], value).setIn([idx, 'hasChanged'], true)
  },

  [ALL_STORED]: state =>
    state
      .asMutable()
      .map(item => item.set('hasChanged', false))
      .asImmutable(),
})

export const getEvidencias = (state, audeId) =>
  state.filter(c => c.get('aude_id') == audeId)

export const getDetalleId = (state, id) => {
  return state.find(c => c.get('auev_id') == id).get('aude_id')
}

export const getStatusEvidencias = state =>
  state.map(c =>
    fromJS({
      hasChanged: c.get('hasChanged'),
    })
  )

export const getPayloadEvidencias = state =>
  state
    .filter(e => e.get('hasChanged'))
    .toJS()
    .map(o => ({
      descripcion: o.descripcion || '',
      observacion: o.observacion || '',
      consideracion: o.consideracion || '',
      estado: o.estado || '',
      aume_id: o.auev_id,
    }))
// .map(o => pick(o, ['auev_id', 'titulo']));
