import React, { Component } from 'react';

import { TextAreaField } from 'common/components/Form';

class Evidencia extends Component {
  _onChange = value => {
    const { evidencia, onChange } = this.props;
    onChange(evidencia.get('auev_id'), 'descripcion', value);
  };

  _onRemove = () => {
    const { evidencia, onRemove } = this.props;
    onRemove(evidencia.get('auev_id'));
  };

  render() {
    const { evidencia, isFetching, canRemove, required } = this.props;

    return (
      <TextAreaField
        required={required}
        value={evidencia.get('descripcion')}
        onChange={this._onChange}
        rows="10"
      />
    );
  }
}

export default Evidencia;
