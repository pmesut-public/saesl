import { fromJS } from 'immutable';
import keyBy from 'lodash/keyBy';

import reducer from './reducer';

const getDetalleId = a => a.aude_id;
const getActId = a => a.auac_id;

const getIdValoracion = (value, context) => {
  if (!value) return '';
  return context.valoraciones.filter(v => v.value == value)[0].id;
};

const getActIdsByEstandarId = (actividades) => {
  const grouped = {};
  actividades.forEach(a => {
    if (!grouped[a.aude_id]) {
      grouped[a.aude_id] = [];
    }
    grouped[a.aude_id].push(a.id);

    a.estandares.forEach(id => {
      if (!grouped[id]) {
        grouped[id] = [];
      }
      grouped[id].push(a.id);
    })
  })
  return grouped;
};

const formatDetalles = (detalle) => ({
  ...detalle,
  aude_valoracion: detalle.aude_estado,
})

const formatMedios = m => ({
  ...m,
  auev_id: m.aume_id,
})

export default (state, context) => {
  let data = fromJS(reducer(undefined, {}));

  if (state.auto_detalles) {
    state.auto_detalles = state.auto_detalles.map(d => ({
      ...d,
      aude_valoracion: d.aude_estado,
    }))
    data = data
      .setIn(['toolbar', 'selectable'], true)
      .setIn(['auto_detalles', 'detalles'], fromJS(keyBy(state.auto_detalles.map(formatDetalles), getDetalleId)))
      .setIn(['auto_detalles', 'ids'], fromJS(state.auto_detalles.map(getDetalleId)));
  }
  if (state.auto_actividades) {
    state.auto_actividades = state.auto_actividades.map(a => ({...a, id: a.auac_id}));

    data = data
      .setIn(['auto_actividades', 'ids'], fromJS(state.auto_actividades.map(getActId)))
      .setIn(['auto_actividades', 'items'], fromJS(keyBy(state.auto_actividades, getActId)))
      .setIn(['auto_actividades', 'estandares'], fromJS(getActIdsByEstandarId(state.auto_actividades)));
  }
  if (state.auto_criterios) {
    data = data
      .setIn(['auto_criterios'], fromJS(state.auto_criterios));
  }
  if (state.auto_medio) {
    console.log('here');
    data = data
      .setIn(['auto_evidencias'], fromJS(state.auto_medio.map(formatMedios)));
  }
  if (state.auto_documentos) {
    data = data
      .setIn(['auto_documentos'], fromJS(state.auto_documentos.map(formatMedios).map(d => ({...d, id: d.audo_id, name: d.titulo}))));
  }

  console.log('inital data', data.toJS());
  return data;
};
