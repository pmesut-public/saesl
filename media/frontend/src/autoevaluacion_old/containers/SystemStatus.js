import React from 'react'
import { connect } from 'react-redux'

import { getContext } from 'common/context'
import Button from 'common/components/buttons'
import { getSummaryStatus } from '../reducer'
import { saveAll, closeFicha } from '../tablaAuto/actions'
import { isFetching, getErrors } from 'autoevaluacion/tablaAuto/reducer'
import { getValoraciones } from 'autoevaluacion/tablaAuto/estandares/reducer'

const formatValoracion = num => {
  const matches = getContext().valoraciones.filter(l => l.max - num > 0)
  return matches.length > 0
    ? matches[0].display
    : getContext().valoraciones[0].display
}

const StatusBadges = ({ status }) => {
  let attr = {
    background: null,
    icon: null,
    title: null,
  }

  switch (status) {
    case 'ALL_STORED':
      attr = {
        background: 'green',
        icon: 'fa-save',
        title: 'Guardado',
      }
      break

    case 'ALL_HAS_CHANGED':
      attr = {
        background: 'yellow',
        title: 'Pendiente',
        icon: 'fa-exclamation',
      }
      break

    case 'ALL_FETCHING':
      attr = {
        background: 'yellow',
        title: 'Guardando...',
        icon: 'fa-spin fa-spinner',
      }
      break

    case 'ALL_ERROR':
      attr = {
        background: 'red',
        title: 'Error al guardar',
        icon: 'fa-exclamation-triangle',
      }
  }

  return (
    <label className={`badge bg-${attr.background}`}>
      <i title={attr.title} className={`fa ${attr.icon}`} />
    </label>
  )
}

const SystemStatus = ({
  summaryStatus,
  saveAll,
  closeFicha,
  leave,
  close,
  isFetching,
  errors,
  goToMedicion,
  goToReporte,
  goToRepositorio,
  goToReporteDocumentosHistoricos,
  goToReporteVigenciaABC,
  goToReporteMV,
  goToFormatos,
}) => (
  <div className="well system-status">
    <div>
      <div className="pull-right status-ficha">
        Estado de la ficha: <StatusBadges status={summaryStatus} />
      </div>
      {/*<Button
        style={{ marginRight: '5px' }}
        extraClass="pull-right"
        warning
        onClick={goToFormatos}
        disabled={isFetching}
>
        Ir a formatos
</Button>*/}
      <Button danger onClick={close} disabled={isFetching}>
        Cancelar autoevaluación
</Button>
     
     {<Button
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        warning
        onClick={leave}
        disabled={isFetching}
      >
        Salir de la autoevaluación
</Button>}
      <Button
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        success
        onClick={saveAll}
        disabled={isFetching}
      >
        Guardar sin terminar
      </Button>
      <Button
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        primary
        onClick={closeFicha}
        disabled={isFetching}
      >
        Finalizar ficha
      </Button>{' '}
    </div>

    {/*<div className="row" style={{ margin: '5px 0' }}>
        <Button
          primary
          extraClass="pull-right"
          style={{ marginRight: '5px' }}
          onClick={goToRepositorio}
        >
        Repositorio
      </Button>
      <Button
        primary
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        onClick={goToReporte}
      >
        Reporte General
      </Button>
      <Button
        primary
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        onClick={goToReporteDocumentosHistoricos}
      >
        Documentos Históricos
      </Button>
      <Button
        primary
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        onClick={goToReporteVigenciaABC}
      >
        Vigencia - A-B-C
      </Button>
      <Button
        primary
        extraClass="pull-right"
        style={{ marginRight: '5px' }}
        onClick={goToReporteMV}
      >
        Vigencia - MV
      </Button>
</div>]*/}
    {errors.length > 1 && (
      <div className="alert alert-danger">
        <div>
          No se pudo finalizar la ficha porque existen errores en las siguientes
          secciones:{' '}
        </div>
        {errors}
      </div>
    )}
  </div>
)

const mapStateToProps = state => ({
  summaryStatus: getSummaryStatus(state),
  isFetching: isFetching(state.get('system_status')),
  errors:
    getErrors(state.get('system_status'))
      .map(e => e.title)
      .join(',') + '.',
  leave: () => {
    window.location.href = '/autoevaluaciones'
  },
  goToFormatos: () => {
    window.location.href = '/formatos'
  },
  close: () => {
    if (!confirm('Esta seguro que desea cancelar la ficha?')) {
      return
    }
    var form = $(document.createElement('form'))
    $(form).attr('method', 'POST')
    $(form).attr('action', '/autoevaluacion/cancel')
    form.appendTo(document.body)
    $(form).submit()
  },
})

const mapDispatchToProps = dispatch => ({
  saveAll: () => dispatch(saveAll()),
  closeFicha: () => {
    if (!confirm('¿Está seguro que desea finalizar autoevaluación?')) return
    dispatch(closeFicha())
  },
  goToMedicion: () =>
    dispatch(
      saveAll(() => {
        window.location.href = '/medicion'
      })
    ),
  goToReporte: index => {
    window.location.href = '/formatos/reporte'
  },
  goToRepositorio: index => {
    window.location.href = '/documentos'
  },
  goToReporteDocumentosHistoricos: () => {
    window.location.href = '/formatos/documentoshistoricos'
  },
  goToReporteVigenciaABC: () => {
    window.location.href = '/formatos/documentosformatos'
  },
  goToReporteMV: () => {
    window.location.href = '/formatos/documentosmedioverificacion'
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(SystemStatus)
