var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'source-map',
  resolve: {
    modules: ['src', 'node_modules'],
  },
  entry: {
    documentos: './src/documentos/index',
    ponderacion: './src/ponderacion/index',
    autoevaluacion: './src/autoevaluacion/index',
    mejora: './src/mejora/index',
    medicion: './src/medicion/index',
    licenciamiento: './src/licenciamiento/index',
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
    publicPath: '/static/'
  },
  /*plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"',
    }),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    })
  ],*/
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel-loader'],
      include: path.join(__dirname, 'src'),
      exclude: /node_modules/
    }]
  }
};
