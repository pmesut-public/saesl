const uuid = require('node-uuid');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

const timeout = 4000;
// const timeout = 0;
const addTimeout = (req, _, next) => setTimeout(next, timeout);
const postLogger = (req, _, next) => {
  console.log(req.url, JSON.stringify(req.body));
  next();
};

const statusOK = {
  status: 'OK',
  msg: '',
};

app.use(postLogger, addTimeout);

app.post('/documentos/create', (req, res) => {
  const data = {};
  res.send(Object.assign({}, data, statusOK));
});

app.post('/documentos/delete', (req, res) => {
  // res.send(Object.assign({}, statusOK));
  const statusERROR = {
    status: 'DENIED',
    msg: 'No se puede eliminar el archivo porque está siendo referenciado en una evidencia',
  };
  res.send(Object.assign({id: req.body.id}, statusERROR));
});

// app.post('/documentos/rename', (req, res) => {
app.post('/autoevaluacion/rename', (req, res) => {
  const existingFileRes = {
    path: 'folder/j3kj2k1y-201702041512.pdf',
    display: 'nuevoFile new.pdf',
    id: req.body.id,
    // status: 'DENIED',
    // msg: 'ya existe un archivo con el mismo nombre'
  };
  // res.send(existingFileRes);
  res.send(Object.assign({}, existingFileRes, statusOK));
});

app.post('/ponderaciones/save', (req, res) => {
  res.send(statusOK);
});

app.post('/ponderaciones/close', (req, res) => {
  res.send(statusOK);
});

app.post('/autoevaluaciones/new', (req, res) => {
  res.send(statusOK);
});

app.post('/autoevaluacion/criterio_new', (req, res) => {
  res.send(Object.assign({}, {aucr_id: uuid.v4()}, statusOK));
});

app.post('/autoevaluacion/criterio_update', (req, res) => {
  res.send(statusOK);
});

app.post('/autoevaluacion/criterio_delete', (req, res) => {
  res.send(statusOK);
});

app.post('/autoevaluacion/evidencia_new', (req, res) => {
  res.send(Object.assign({}, {auev_id: uuid.v4()}, statusOK));
});

app.post('/autoevaluacion/evidencia_delete', (req, res) => {
  res.send(statusOK);
});

app.post('/autoevaluacion/documento_batch_update', (req, res) => {
  const documentos = req.body.new.map(d => {
    const audo_id = uuid.v4();
    return {
      auev_id: d.auev_id,
      docu_id: d.docu_id,
      audo_id,
      path: uuid.v4(),
      display: uuid.v4(),
      audo_estado: '',
    };
  });

  res.send(Object.assign({}, {documentos}, statusOK));
});

app.post('/autoevaluacion/save', (req, res) => {
  res.send(statusOK);
});

app.post('/autoevaluacion/documento_new', (req, res) => {
  const documentos = req.body.map(d => {
    const audo_id = uuid.v4();
    return {
      id: audo_id,
      auev_id: d.auev_id,
      docu_id: d.docu_id,
      audo_id,
      path: uuid.v4(),
      display: audo_id,
    };
  });

  res.send(Object.assign({}, {documentos}, statusOK));
});

// app.post('/documentos/get', (req, res) => {
  app.post('/autoevaluacion/list', (req, res) => {
  const files = [
    {path: 'folder/x7dsfj3a-201608092045.doc', display: 'Autoevaluación.pdf', id: 11},
    {path: 'folder/7sdfh312-201608092050.pdf', display: 'Criterio.docx', id: 22},
    {path: 'folder/sdf83kjx-201608092052.pdf', display: 'PEI.pdf', id: 34},
    {path: 'xdx.doc', display: 'gg.pdf', id: 12},
    {path: 'test.pdf', display: 'ggwp.docx', id: 24},
    {path: 'ggwp.pdf', display: 'ff.pdf', id: 35},
    {path: 'asdf', display: 'Autoevaluación1.pdf', id: 111},
    {path: 'folder/7sdfh312-201608092050.pdf', display: 'Criterio1.docx', id: 221},
    {path: 'folder/sdf83kjx-201608092052.pdf', display: 'PEI1.pdf', id: 341},
    {path: 'xdx.doc', display: 'gg1.pdf', id: 121},
    {path: 'test.pdf', display: 'ggwp1.docx', id: 241},
    {path: 'ggwp.pdf', display: 'ff1.pdf', id: 351},

  ];
  res.send(Object.assign({}, {files}, statusOK));
});

const PORT = 3002;
app.listen(PORT, () => console.log('mock server listening on %s', PORT));
