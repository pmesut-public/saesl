(function (root) {
  var data = {
    filter: {
      filterName: '',
      sortBy: '',
    },
    files: [{name: 'abc.doc'}, {name: 'bcd.pdf'}],
  };

  if (typeof module !== 'undefined' && module.exports) {
    module.exports = data;
  } else {
    root.__DATA__ = data;
  }
})(this);
