# Frontend setup

## Prerequisitos
* Instalar node > 5.0v

## Elegir uno de los 2 metodos
### Npm:
```
$ npm install 
$ npm start
```
### Yarn

```
$ yarn --ignore-engines
$ npm start
```

### Development setup

Run mockserver
```
npm run mockServer
```

## Vistas
[http://localhost:3001/documentos.html](Documentos)
