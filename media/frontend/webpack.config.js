var path = require('path');
var webpack = require('webpack');

var entries = [
  'webpack-dev-server/client?http://localhost:3001',
];

module.exports = {
  resolve: {
    modules: ['src', 'node_modules'],
  },
  devtool: 'source-map',
  entry: {
    documentos: entries.concat('./src/documentos/index'),
    ponderacion: entries.concat('./src/ponderacion/index'),
    autoevaluacion: entries.concat('./src/autoevaluacion/index'),
    mejora: entries.concat('./src/mejora/index'),
    medicion: entries.concat('./src/medicion/index'),
    licenciamiento: entries.concat('./src/licenciamiento/index'),
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
    publicPath: '/static/'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel-loader'],
      include: path.join(__dirname, 'src'),
      exclude: /node_modules/
    }]
  }
};
