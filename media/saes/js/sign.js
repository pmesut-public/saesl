jQuery(function ($) {
	
	// Placeholder =============================================================
	
	$('select.placeholder').each(togglePlaceholder);
	$('select.placeholder').change(togglePlaceholder);
	
	function togglePlaceholder () {
		var $this = $(this);
		if ($this.val()) $this.removeClass('placeholder');
		else $this.addClass('placeholder');
	}
	
	// Obac and inst ===========================================================
	
//	$('#signup-obac').change(function () {
//		var $this = $(this),
//			$target = $($this.data('target'));
//			val = $this.val() || 0
//			tipo = $this.children('[value=' + val + ']').data('tipo');
//
//		if ( ! val) return;
//		//console.log(tipo);
//		
//		if (tipo == 1) {
//			
//			$target.prop('required', false).hide();
//			
//		} else {
//			$target.prop('required', true).show();
//		}
//                
//            $('#objeto_eval').val(tipo);    
//		
//	});
	
	// Career other ============================================================
	
	$('#carr_id').change(function () {
		var $this = $(this),
			val = $this.val(),
			$carr_nombre = $('#carr_nombre_wrapper');
		
		if (val === '0')
		{
			$carr_nombre.removeClass('hidden').find('input').prop('required', true);
		}
		else
		{
			$carr_nombre.addClass('hidden').find('input').prop('required', false);
		}
	});
	
	// Career Name =============================================================
	
	$('#signup-credit-type').change(toggleCareerName);
	$('#signup-credit-type').each(toggleCareerName);
	//toggleCareerName.call($('#signup-credit-type'));
	
	function toggleCareerName () {
		var $this = $(this), val = $this.val();
		if (val == 'institucion') {
			$('#signup-career-name').prop('required', false).hide();
			//$('#signup-career-type').prop('required', false).hide();
		}
		else if (val == 'carrera') {
			$('#signup-career-name').prop('required', true).show();
			//$('#signup-career-type').prop('required', true).show();
		}
	}
	
	// Committee member add ====================================================
	
	$('#signup-member-add').click(function () {
		var $table = $('.committee'),
			$tbody = $('tbody', $table),
			$row = $('tr:first', $tbody).clone(),
			n = $tbody.children().length;
		
		console.log(n);
		
		$row.find('input[type=radio]').prop('checked', false).val(n);
		$row.find('input[type=text]').val('');//.prop('required', false);
		$row.appendTo($tbody);
	});
	
	// Committee member delete =================================================
	
	remove_row = function () {
		$(this).closest('tr').remove();
	},
	
	$('.committee').on('click', '.remove_tr', remove_row);
	
	// Committee contact change ================================================
	
	/*$('.committee').on('change', 'input[type=radio]', changeContact);
	changeContact.call($('.committee input[type=radio]:checked'));
	
	function changeContact () {
		var $tr = $(this).parents('tr');
		$('.committee').find('input[type=text]').prop('required', false);
		$tr.find('input[type=text]').prop('required', true);
	}*/
	
	// Select ajax =============================================================
	
	$('select.ajax').change(getOptions);
	$('select.ajax').each(getOptions);
	
	function getOptions() {
		var $this = $(this),
			id = $this.val(),
			action = $this.data('action'),
			actions = (typeof action == 'string') ? [action] : action,
			//url = ['/ajax', $action, id].join('/'),
			target = $this.data('target'),
			targets = (typeof target == 'string') ? [target] : target,
			$targets = targets.map(function (a) { return $(a); }),
			i, n = actions.length;
			
		for (i = 0; i < n; i++) {
			url = '/ajax/' + [actions[i],id].join('/');
			
			callAjax(url, $targets[i]);
		}
	}
	
	function callAjax(url, $target) {
		$.get(url).done(function (data) {
			$target.html(data);
			$target.addClass('placeholder');
		});
	}
	
	// Input ajax ==============================================================
	
	$('input.ajax').blur(getResponse);
	//$('input.ajax').each(getResponse);
	
	function getResponse() {
		var $this = $(this),
			$wrapper = $this.parent(),
			val = $this.val(),
			action = $this.data('action'),
			url = [action, val].join('/'),
			alert = ['<p class="text-danger">', '</p>'];
		
		if ( ! val) return;
		
		$.get(url).done(function (data) {
			data = JSON.parse(data);
			
			$wrapper.find('.text-danger').remove();
			
			if (data.status == 'ok') {
				$wrapper.removeClass('has-error');
			} else {
				$wrapper.addClass('has-error');
				$wrapper.append(alert[0] + data.msg + alert[1]);
			}
		});
	}
	
	// Custom validity
	
	/*function customError(e) {
		console.log(e);

		e.target.setCustomValidity('');

		if (!e.target.validity.valid && e.target.validity.tooShort) {
			e.target.setCustomValidity('Muy corto. Debería tener ' + e.target.minLength + ' caracteres');
		}
		else
		{
			e.target.setCustomValidity('');
		}
	}*/
	
	/*$('[minlength]').each(function () {
		this.oninvalid = customError;
		this.oninput = customError;
	});*/
	//Verificar si se encuentra cargada la librería select2
    
    if (jQuery.fn.select2) {
        $('select.select2').select2({
            width: '100%',
            placeholder: 'Seleccionar una opción'
        }).on("change", function (e) {
            
            var $this = $(this);
            
            if($this.attr('id') == 'signup-obac'){                
                var $target = $($this.data('target'));
                val = $this.val() || 0
                tipo = $this.children('[value=' + val + ']').data('tipo');
                if ( ! val) return;
                //console.log(tipo);
                if (tipo == 1) {
                    $('.content-carr_id').hide();
                    $target.prop('required', false).hide();
                } else {
                    $('.content-carr_id').show();
                    $target.prop('required', true).show();
                }
                $('#objeto_eval').val(tipo);
            }
        });
    }

    $("#signup-obac").change(function(event) {

    	var $this   = $(this);

    	var $target = $($this.data('target'));
		
		var val     = $this.val() || 0
		
		var tipo    = $this.children('[value=' + val + ']').data('tipo');

    	if (tipo == 1) {

            $('.content-carr_id').hide();
            $target.prop('required', false).hide();
        } else {

            $('.content-carr_id').show();
            $target.prop('required', true).show();
        }

        $('#objeto_eval').val(tipo);
    });
});

var isDirty = false,
	setDirty = function () { console.log('dirty'); isDirty = true; };

(function ($) {
	
	var lastVal,
		//isDirty = false,
		formSubmitting = false;
		//setDirty = function () { isDirty = true; };
	
	$('.signup').on('focus', 'input, select, textarea', function (e) {
		//console.log(e.target.value);
		lastVal = e.target.value;
	});
	
	$('#password, #password_confirm').blur(function () {
		var $p = $('#password'),
			p = $p.val(),
			cp = $('#password_confirm').val();
			//msg = 'Las contraseñas no coinciden';
		
		if (p && cp && p != cp) {
			$p.parent().addClass('has-error');
		} else {
			$p.parent().removeClass('has-error');
		}
	})
	
	$('.signup').on('blur', 'input, select, textarea', function (e) {
		if (e.target.value != lastVal) setDirty();
	});
	
	$('.signup').on('click', '#signup-member-add, .remove_tr', setDirty);
	
	$('.signup').submit(function () {
		formSubmitting = true;
		
		if ($('#password').val() != $('#password_confirm').val()) 
			return false;
	});
	
	$(window).bind('beforeunload', function() {
		return isDirty && ! formSubmitting ? 'Sus datos aún no han sido guardados y los perderá.' : undefined;
	});
	
})(jQuery);

//LOGIN 2
//Vermas de login
	$('a#vermas').on('click',function(){
		$('div#login1').css('display','none');
		$('div#login2').css('display','block');
	});
	
//Video de login
	$('a#video-txt').on('click',function(){
		$('div#login3').css('display','block');
		$('div#login4').css('display','none');
		$('div#login1').css('display','none');
		$('div#login2').css('display','none');
	});
	$('a#video-txt2').on('click',function(){
		$('div#login4').css('display','block');
		$('div#login3').css('display','none');
		$('div#login1').css('display','none');
		$('div#login2').css('display','none');
	});

$('img#video-img').on('click',function(){
		$('div#login1').css('display','none');
		$('div#login2').css('display','none');
		$('div#login4').css('display','none');
		$('div#login3').css('display','block');
	});
	
	$('img#video-img2').on('click',function(){
		$('div#login1').css('display','none');
		$('div#login2').css('display','none');
		$('div#login3').css('display','none');
		$('div#login4').css('display','block');
	});
//Volver a login1
	$('a#volver').on('click',function(){
		$('div#login2').css('display','none');
		$('div#login1').css('display','block');
	});
	
	$('a#volver_video').on('click',function(){
		
		$('div#login1').css('display','block');
		$('div#login3').css('display','none');
		$('div#login2').css('display','none');
		$('div#login4').css('display','none');
	});
//FIN LOGIN 2