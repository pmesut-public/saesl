<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2014 Inspired Solutions
 */

include Kohana::find_file('vendor', 'swiftmailer/lib/swift_required');
