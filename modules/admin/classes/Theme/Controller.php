<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2014 Inspired Solutions
 */
class Theme_Controller extends Controller_Template {
	
	protected $theme;
	
	public function before()
	{
		//$this->request->response = $this->response;
		
		if ($this->auto_render)
		{
			Theme::init($this->theme);
		}
	}
	
	public function after()
	{
		if ($this->auto_render)
		{
			$theme = Theme::instance();
			
			foreach ($theme->headers() as $key => $value)
			{
				$this->response->headers($key, $value);
			}
			
			$this->response->body($theme->template);
		}
	}
	
}
