<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2014 Inspired Solutions
 */
class AdminReport extends Table {
	
	protected $_default_items = 999999;
	
	protected function _get_thead_properties($column)
	{
		$column_name = Arr::get($this->_masks, $column, $column);
		
		$width = $this->_get_width($column);
		
		$class = array();
		
		$query = array(
			'order_c' => $column_name,
			'order_t' => 'asc',
		);
		
		if ($this->_request->query('order_c') == $column_name)
		{
			$order = $this->_request->query('order_t') == 'desc' ? 'asc' : 'desc';
			$class[] = $this->_request->query('order_t') == 'desc' ? 'desc' : 'asc';
			
			$query['order_t'] = $order;
		}
		
		return array(
			'class' => implode(' ', $class),
			'data-url' => URL::query($query, TRUE),
			'style' => 'width: '.$width.'%;',
		);
	}
	
}
