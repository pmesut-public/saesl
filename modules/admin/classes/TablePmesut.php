<?php defined ('SYSPATH') or die ('No direct script access.');

class TablePmesut {
	
	/**
	 *
	 * @var  Table
	 */
	//public static $current;
	
	const SIZE_XS = 1;
	const SIZE_SM = 2;
	const SIZE_MD = 3;
	const SIZE_LG = 4;
	const SIZE_XL = 5;
	
	public static $default_size = self::SIZE_MD;
	
	protected $_default_items = 40;
	
	/**
	 *
	 * @var  View
	 */
	protected $_view = 'table';
	protected $_filters_view = 'filters';
	protected $_searches_view = 'searches';
	
	protected $_query_buttons = array(
		'print' => array(
			'print' => 1,
		),
		'excel' => array(
			'print' => 1,
			'excel' => 1,
		),
		'csv' => array(
			'print' => 1,
			'csv' => 1,
		),
		'pdf' => array(
			'print' => 1,
			'pdf' => 1,
		),
	);
	
	/**
	 *
	 * @var  ORM
	 */
	protected $_model;
	
	/**
	 *
	 * @var  Request
	 */
	protected $_request;
	
	/**
	 *
	 * @var  Pagination
	 */
	protected $_pagination;
	
	/**
	 *
	 * @var  Database_Query
	 */
	protected $_query;
	
	/**
	 *
	 * @var  Database_Result
	 */
	protected $_data;
	
	protected $_filters = array();
	
	protected $_searches = array();
	
	protected $_columns = array();
	
	protected $_options = array();
	
	protected $_labels = array();
	
	protected $_sizes = array();
	
	protected $_masks = array();
	
	protected $_actions = array();
	
	protected $_bulk_actions = array();
	
	protected $_custom_text = array();
	
	protected $_title;
	
	protected $_callback;
	
	protected $_format;
	
	/**
	 * Crea una instancia de la misma clase (static)
	 * @param type $model
	 * @param Request $request
	 * @return \self
	 */
	public static function factory($model, Request $request = NULL)
	{
		/*Table::$current = */$table = new static($model, $request);
		return $table;
	}
	
	/**
	 * Crea una instancia del Modelo (model) y lo guarda en la variable de la clase (_model)
	 * Guarda los campos pertenecientes a la funcion labels() en la variable de la clase (_labels)
	 * 
	 * @param string $model
	 * @param Request $request
	 * @return Obj Pagination
	 * @return array labels
	 * @return Obj Request
	 */
	public function __construct($model, Request $request = NULL)
	{
		$model = 'Model_'.ucfirst($model);
		$this->_model = new $model;
		
		$this->_labels = $this->_model->labels();
		$this->_request = $request ? $request : Request::$current;

		$items_per_page = $this->_request->query('print') ? 999999 : ($this->_request->query('items') ?: $this->_default_items);
		
		$this->_pagination = Pagination::factory(
			array('items_per_page' => $items_per_page)
		);
	}
	
	/**
	 * Recibe un Obj tipo Database Query
	 *  si no existe retornara el valor de la variable de la clase ($_query), de lo contrario se guarda.
	 * 
	 * @param type $query
	 * @return Obj Table
	 */
	public function query($query = NULL)
	{
		if ($query === NULL)
		{
			return $this->_query;
		}
		
		$this->_query = $query;

		return $this;
	}
	
	/**
	 * Recibe un campo con los valores que se le asignarán
	 * $this->_options[$column] = $options;
	 * 
	 * @param type $column
	 * @param type $options
	 * @return Obj Table
	 */
	public function options($column = NULL, $options = NULL)
	{
		if ($column === NULL)
		{
			return $this->_options;
		}
		
		if (is_array($column))
		{
			$this->_options = $column;
		}
		else
		{
			$this->_options[$column] = $options;
		}
		
		return $this;
	}
	
	/**
	 * Crea "select" 
	 * 
	 * @param type $column
	 * @param type $name
	 * @param array $args
	 * @param type $type
	 * @return \Table
	 */
	public function filter($column, $name = NULL, array $args = NULL, $type = 'dropdown')
	{
		$this->_filters[$column] = array(
			'name' => $name ?: $this->_labels[$column],
			'type' => $type,
			'options' => $args ?: $this->_options[$column],
		);
		
		return $this;
	}
	
	public function drop_filter($column)
	{
		unset($this->_filters[$column]);
		
		return $this;
	}
	
	public function drop_column($column)
	{
		$this->_columns = array_diff($this->_columns, array($column));
		
		return $this;
	}
	
	/**
	 * 
	 * @param type $column
	 * @param type $name
	 * @param array $args
	 * @param type $type
	 * @return \Table
	 */
	public function search($column, $name = NULL, array $args = NULL, $type = 'text')
	{
		$this->_searches[$column] = array(
			'name' => $name ?: $this->_labels[$column],
			'type' => $type,
			//'options' => $args ?: $this->_options[$column],
		);
		
		return $this;
	}
	
	public function search_get_event_name($column, $name = NULL, array $args = NULL, $type = 'text')
	{
		$this->_searches[$column] = array(
			'name' => $name ?: $this->_labels[$column],
			'type' => $type,
			//'options' => $args ?: $this->_options[$column],
		);
		
		return $this;
	}
	
	/**
	 * Recibe un array de campos
	 * 
	 * @param type $column
	 * @return Obj Table
	 */
	public function columns($column = NULL)
	{
		if ($column === NULL)
		{
			return $this->_columns;
		}
		
		if (is_array($column))
		{
			$this->_columns = $column;
		}
		else
		{
			$this->_columns[] = $column;
		}
		
		return $this;
	}
	
	/**
	 * Recibe un array de campos
	 * 
	 * 
	 * @param type $column
	 * @param type $value
	 * @return \Table
	 */
	public function labels($column = NULL, $value = NULL)
	{
		if ($column === NULL)
		{
			return $this->_labels;
		}
		
		if (is_array($column))
		{
			$this->_labels = array_merge($this->_labels, $column);
		}
		else
		{
			$this->_labels[$column] = $value;
		}
		
		return $this;
	}
	
	public function format($column, callable $callable)
	{
		$this->_format[$column] = $callable;
		
		return $this;
	}
	
	/**
	 * Recibe un array de campos
	 * Se encarga de darle el tamaño utilizando las constantes definidas al principio de la clase
	 * 
	 * @param type $column
	 * @param type $value
	 * @return \Table
	 */
	public function sizes($column = NULL, $value = NULL)
	{
		if ($column === NULL)
		{
			return $this->_sizes;
		}
		
		if (is_array($column))
		{
			$this->_sizes = array_merge($this->_sizes, $column);
		}
		else
		{
			$this->_sizes[$column] = $value;
		}
		
		return $this;
	}
	
	/**
	 * Recibe columnas 
	 * 
	 * @param type $column
	 * @param type $mask
	 * @return \Table
	 */
	public function mask_thead($column = NULL, $mask = NULL)
	{
		if ($column === NULL)
		{
			return $this->_masks;
		}
		
		if (is_array($column))
		{
			$this->_masks = $column;
		}
		else
		{
			$this->_masks[$column] = $mask;
		}
		
		return $this;
	}
	
	public function title($title = NULL)
	{
		if ($title === NULL)
		{
			return $this->_title;
		}
		
		$this->_title = $title;
		
		return $this;
	}
	
	public function callback($callback)
	{
		$this->_callback = $callback;
		
		return $this;
	}
	
	public function view()
	{
		return $this->_view;
	}
	
	/**
	 * 
	 * @return type
	 */
	public function build()
	{
		$this->_prepare();
		
		$this->_view = Theme_View::factory($this->_view)
			->set('request', $this->_request)
			->set('filters', $this->get_filters())
			->set('searches', $this->get_searches())
			->set('pagination', $this->_pagination)
			->set('thead', $this->get_head())
			->set('tbody', $this->get_body())
			->set('data', $this->_data)
			->set('buttons', $this->_bulk_actions)
			->set('custom_text', $this->_custom_text);
		
		$theme = Theme::instance();
		
		$file = array(
			'name' => $this->_title,
		);
		
		if (is_callable($this->_callback))
		{
			call_user_func($this->_callback, $this);
		}
		
		if ($this->_request->query('print'))
		{
			$theme->template = Theme_View::factory('template/print');
		}
		
		if ($this->_request->query('excel'))
		{
			$file['ext'] = 'xls';
			
			$theme->css(array());

			$theme->headers('Content-Type', 'application/vnd.ms-excel');//; charset=utf-8');
			$theme->headers('Content-Disposition', 'attachment; filename='.implode('.', $file));
		}
		
		if ($this->_request->query('csv'))
		{
			$file['ext'] = 'csv';
			
			$theme->template = Theme_View::factory('template/blank');
			$theme->template->data = $this->_parse_csv();
			
			$theme->headers('Content-Type', 'text/csv');//; charset=ISO-8859-1');
			$theme->headers('Content-Disposition', 'attachment; filename='.implode('.', $file));
		}
		
		if ($this->_request->query('pdf'))
		{
			$file['ext'] = 'pdf';
			$theme->template = Theme_View::factory('template/blank');
			$theme->template->data = $this->_get_pdf();
			
			$theme->headers('Content-Type', 'application/pdf');
		}
		
		return $this->_view;
	}
	
	public function _prepare()
	{
		if ( ! $this->_columns)
		{
			$this->_columns = array_keys($this->_labels);
		}
		
		$this->_labels += $this->_model->labels();
		
		$this->_set_data();
		
		$this->_set_title();
		
		$this->_set_sizes();
	}
	
	protected function _set_data()
	{
		if ( ! $this->_query)
		{
			$this->_query = DB::select(DB::expr('SQL_CALC_FOUND_ROWS *'))->from($this->_model->table_name());
		}
		
		$this->_ordering();
		$this->_filtering();
		
		$this->_data = $this->_query
			// Pagination
			->limit($this->_pagination->items_per_page)
			->offset($this->_pagination->offset)
			// Set model
			->as_object(get_class($this->_model))
			->execute()
			// Use pk as key
			->as_array($this->_model->primary_key());
		
		$count = DB::select(array(DB::expr('FOUND_ROWS()'), 'found'))
			->execute()
			->get('found');
		
		$this->_pagination->setup(array('total_items' => $count));
	}
	
	protected function _set_title()
	{
		if ($this->_title)
			return;
		
		$this->_title = $this->_model->object_name();
	}
	
	protected function _set_sizes()
	{
		foreach ($this->_columns as $column)
		{
			if ( ! isset($this->_sizes[$column]))
			{
				$this->_sizes[$column] = self::$default_size;
			}
		}
	}
	
	protected function _ordering()
	{
		$column = $this->_request->query('order_c');
		$type = $this->_request->query('order_t');
		
		if ($column AND $type)
		{
			$this->_query->order_by($column, $type);
		}
	}
	
	// @todo split filtering and searching
	protected function _filtering()
	{
		// @todo should we use query->pendings instead of model object??
		foreach ($this->_request->query() as $key => $value)
		{
			if (substr($key, 0, 6) == 'filter' AND $value !== 'ALL')
			{
				$column = substr($key, 8);
				
				if (array_key_exists($column, $this->_model->object()))
				{
					$this->_query->where($column, '=', $value);
				}
				else
				{
					$this->_query->having($column, '=', $value);
				}
			}
			
			if (substr($key, 0, 6) == 'search' AND $value != '')
			{
				$column = substr($key, 8);
				
				$this->_query->where($column, 'like', ('%'.$value.'%'));
			}
		}
	}
	
	public function get_filters()
	{
		return $this->_filters ? Theme_View::factory($this->_filters_view)
			->set('request', $this->_request)
			->set('site', URL::site($this->_request->uri()))
			->set('filters', $this->_filters) : NULL;
	}
	
	public function get_searches()
	{
		$query = $this->_request->query();
		foreach (array_keys($this->_searches) as $column)
		{
			unset($query['search__'.$column]);
		}
		unset($query['page']);
		
		$hiddens = array();
		foreach ($query as $name => $value)
		{
			$hiddens[] = Form::hidden($name, $value);
		}
		
		return $this->_searches ? Theme_View::factory($this->_searches_view)
			->set('request', $this->_request)
			->set('hiddens', $hiddens)
			->set('searches', $this->_searches) : NULL;
	}
	
	public function get_head()
	{
		$thead = array();
		foreach ($this->_columns as $column)
		{
			$thead[] = HTML::tag('th', $this->_labels[$column], $this->_get_thead_properties($column), FALSE);
		}
		
		if ($this->_actions AND ! $this->_request->query('print'))
		{
			$thead[] = HTML::tag('th', '', array('class' => 'th-actions'));
		}
		
		return $thead;
	}
	
	public function get_body()
	{
		$tbody = array();
		foreach ($this->_data as $row)
		{
			$tr = array();
			foreach ($this->_columns as $column)
			{
				$value = $this->_get_value($row, $column);
				
				$width = $this->_get_width($column);
				
				$tr[] = HTML::tag('td', $value, array('style' => 'width: '.$width.'%;'));
			}
			
			if ($this->_actions AND ! $this->_request->query('print'))
			{
				$buttons = $this->_get_buttons($row->pk());
				
				$tr[] = HTML::tag('td', implode(' ', $buttons), array('class' => 'td-actions'));
			}
			
			$tbody[$row->pk()] = $tr;
		}
		//debug($tbody);
		return $tbody;
	}
	
	protected function _get_thead_properties($column)
	{
		$column_name = Arr::get($this->_masks, $column, $column);
		
		$width = $this->_get_width($column);
		
		$class = array('order');
		
		$query = array(
			'order_c' => $column_name,
			'order_t' => 'asc',
		);
		
		if ($this->_request->query('order_c') == $column_name)
		{
			$order = $this->_request->query('order_t') == 'desc' ? 'asc' : 'desc';
			$class[] = $this->_request->query('order_t') == 'desc' ? 'desc' : 'asc';
			
			$query['order_t'] = $order;
		}
		
		return array(
			'class' => implode(' ', $class),
			'data-url' => URL::query($query, TRUE),
			'style' => 'width: '.$width.'%;',
		);
	}
	
	protected function _get_width($column)
	{
		return numberformat(100 * $this->_sizes[$column] / array_sum($this->_sizes));
	}
	
	protected function _get_value($row, $column)
	{
		return isset($this->_options[$column][$row->$column]) ? $this->_options[$column][$row->$column] : 
			(isset($this->_format[$column]) ? call_user_func($this->_format[$column], $row) : $row->$column);
	}
	
	protected function _get_buttons($id)
	{
		$buttons = array();
		foreach ($this->_actions as $name => $view)
		{
			if (isset($view->options['path']))
			{
				$path = $view->options['path'].$id;
			}
			else
			{
				$path = '/adminPmesut/'.lcfirst($this->_request->controller()).'/'.$name.'/'.$id;
			}
			
			$buttons[] = $view->
				set(compact('path'));
		}
		
		return $buttons;
	}
	
	public static function makefilter($filter, $name)
	{
		//$table = Table::$current;
		$request = Request::$current;
		
		return Theme_View::factory('filter')
			->set('request', $request)
			->set('site', $request->url())
			->set(compact('filter', 'name'));
	}
	
	protected function _parse_csv()
	{
		$thead = array();
		$thead[] = $this->_model->primary_key();
		foreach ($this->_columns as $column)
		{
			$thead[] = $this->_labels[$column];
		}
		
		$tbody = array();
		foreach ($this->_data as $row)
		{
			$tr = array();
			$tr[] = $row->pk();
			foreach ($this->_columns as $column)
			{
				$value = $this->_get_value($row, $column);
				
				$tr[] = $value;
			}
			$tbody[] = $tr;
		}
		
		$csv = new parseCSV();
		$csv->encoding('UTF-8', 'ISO-8859-1//TRANSLIT');
		
		return $csv->output(NULL, $tbody, $thead);
	}
	
	protected function _get_pdf()
	{
		include Kohana::find_file('vendor', 'tcpdf/tcpdf');
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		//$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('SAES - PROCALIDAD');
		$pdf->SetTitle($this->_title);
		//$pdf->SetSubject('TCPDF Tutorial');
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// set default header data
		//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		//$pdf->setCellPadding('5mm');

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', 'B', 20);

		// add a page
		$pdf->AddPage();

		$pdf->Write(0, $this->_title, '', 0, 'L', true, 0, false, false, 0);

		$pdf->SetFont('helvetica', '', 8);
		
		$tbl = Theme_View::factory('template/pdf')
			->set('thead', $this->_view->thead)
			->set('tbody', $this->_view->tbody)
			->set('filters', $this->get_filters())
			->render();
		
		$pdf->writeHTML($tbl, true, false, false, false, '');
		$a = $pdf->Output('', 'S');
		//debug($a);
		//debug($pdf->getAutoPageBreak());
		return $a;

	}
	
	public function actions($actions = NULL)
	{
		if ($actions === NULL)
		{
			return $this->_actions;
		}
		
		if (is_string($actions))
		{
			$actions = array($actions);
		}
		
		foreach ($actions as $name => $options)
		{
			if (is_numeric($name))
			{
				if (class_exists('ACL') AND ACL::instance()->allowed(lcfirst($this->_request->controller()), $name))
				{
					$this->_actions[$options] = Theme::button($options);
				}
			}
			else
			{
				$this->_actions[$name] = Theme::button($name, $options);
			}
		}
		
		return $this;
	}
	
	public function bulk_actions($actions = NULL)
	{
		$controller = lcfirst($this->_request->controller());
		
		if ($actions === NULL)
		{
			return $this->_bulk_actions;
		}
		
		if (is_string($actions))
		{
			$actions = array($actions);
		}
		
		foreach ($actions as $name => $options)
		{
			if (is_numeric($name))
			{
				$path = isset($this->_query_buttons[$options]) ? 
					URL::query($this->_query_buttons[$options]) :
					Route::url('backend', array('controller' => $controller, 'action' => $options));
				
				$this->_bulk_actions[$options] = Theme::button($options)
					->set(compact('path'));
			}
			else
			{
				if ( ! $path = Arr::get($options, 'path'))
				{
					$path = isset($this->_query_buttons[$name]) ? 
						URL::query($this->_query_buttons[$name]) :
						Route::url('backend', array('controller' => lcfirst($this->_request->controller()), 'action' => $name));
				}
				
				$this->_bulk_actions[$name] = Theme::button($name, $options)
					->set(compact('path'));
			}
		}
		
		return $this;
	}
	
	public function custom_text($key = NULL, $val = NULL)
	{
		if ($key === NULL)
		{
			return $this->_custom_text;
			
		}
		
		if ($val === NULL)
		{
			$this->_custom_text = $key;
		}
		else
		{
			$this->_custom_text[$key] = $val;
		}
		
		return $this;
	}
	
}
