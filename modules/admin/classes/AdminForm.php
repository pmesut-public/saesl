<?php defined ('SYSPATH') or die ('No direct script access.');

class AdminForm {
	
	protected $_view = 'form';
	protected $_controls_view = 'control';
	
	/**
	 *
	 * @var  ORM
	 */
	protected $_model;
	
	/**
	 *
	 * @var  Request
	 */
	protected $_request;
	
	/**
	 *
	 * @var  Database_Query
	 */
	protected $_query;
	
	protected $_columns = array();
	
	protected $_options = array();
	
	protected $_labels = array();
	
	protected $_readonly = array();
	
	protected $_required = array();
	
	protected $_helptexts = array();
	
	protected $_types = array();
	
	protected $_errors = array();
	
	protected $_callables = array(
		'new' => array(),
		'edit' => array(),
	);
	
	protected $_custom = array();
	
	protected $_multiple = array();
	
	/**
	 *
	 * @var  Validation
	 */
	protected $_extra = NULL;
	
	protected $_format;
	
	protected $_new_values = array();
	
	protected $_form_attr = array();
	
	protected $_tag_attr = array();
	
	protected $_date = array();
	
	/**
	 * 
	 * @param type $model
	 * @param Request $request
	 * @return \self
	 */
	public static function factory($model, Request $request = NULL)
	{
		return new self($model, $request);
	}
	
	/**
	 * 
	 * @param string $model
	 * @param Request $request
	 */
	public function __construct($model, Request $request = NULL)
	{
		$model = 'Model_'.ucfirst($model);
		$this->_model = new $model;
		
		$this->_request = $request ? $request : Request::$current;
	}
	
	/**
	 * 
	 * @param type $query
	 * @return \Table
	 */
	public function query($query = NULL)
	{
		if ($query === NULL)
		{
			return $this->_query;
		}
		
		$this->_query = $query;
		return $this;
	}
	
	/**
	 * 
	 * @param type $column
	 * @param type $options
	 * @return \Table
	 */
	public function options($column = NULL, $options = NULL)
	{
		if ($column === NULL)
		{
			return $this->_options;
		}
		
		if (is_array($column))
		{
			$this->_options = $column;
		}
		else
		{
			$this->_options[$column] = $options;
		}

		return $this;
	}
	
	/**
	 * 
	 * @param type $column
	 * @return \Table
	 */
	public function columns($column = NULL)
	{
		if ($column === NULL)
		{
			return $this->_columns;
		}
		
		if (is_array($column))
		{
			$this->_columns = $column;
		}
		else
		{
			$this->_columns[] = $column;
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @param type $column
	 * @param type $value
	 * @return \Table
	 */
	public function labels($column = NULL, $value = NULL)
	{
		if ($column === NULL)
		{
			return $this->_labels;
		}
		
		if (is_array($column))
		{
			$this->_labels = $column;
		}
		else
		{
			$this->_labels[$column] = $value;
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @param type $column
	 * @param type $value
	 * @return \Table
	 */
	public function helptexts($column = NULL, $value = NULL)
	{
		if ($column === NULL)
		{
			return $this->_helptexts;
		}
		
		if (is_array($column))
		{
			$this->_helptexts = $column;
		}
		else
		{
			$this->_helptexts[$column] = $value;
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @param type $column
	 * @param type $value
	 * @return \AdminForm
	 */
	public function custom($column = NULL, $value = NULL)
	{
		if ($column === NULL)
		{
			return $this->_custom;
		}
		
		if (is_array($column))
		{
			$this->_custom = $column;
		}
		else
		{
			$this->_custom[$column] = $value;
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @param type $column
	 * @param type $value
	 * @return \Table
	 */
	public function types($column = NULL, $value = NULL)
	{
		if ($column === NULL)
		{
			return $this->_types;
		}
		
		if (is_array($column))
		{
			$this->_types = $column;
		}
		else
		{
			$this->_types[$column] = $value;
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @param type $column
	 * @return \Table
	 */
	public function readonly($column = NULL)
	{
		if ($column === NULL)
		{
			return $this->_readonly;
		}
		
		if (is_array($column))
		{
			$this->_readonly = $column;
		}
		else
		{
			$this->_readonly[] = $column;
		}
		
		return $this;
	}

	/**
	 * 
	 * @param type $column
	 * @return \Table
	 */
	public function required($column = NULL)
	{
		if ($column === NULL)
		{
			return $this->_required;
		}
		
		if (is_array($column))
		{
			$this->_required = $column;
		}
		else
		{
			$this->_required[] = $column;
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @return type
	 */
	public function build()
	{
		$this->_prepare();
	
		$this->_view = Theme_View::factory($this->_view)
			->set('title', $this->_request->action())
			->set('errors', $this->_errors)
			->set('form_attr', $this->_form_attr)
			->set('data_attr', $this->_tag_attr)
			->set('controls', $this->get_controls())
			->set('date', $this->_date)
			->set('back_url', $this->_get_referrer());
		//debug($this->_view);
		return $this->_view;
	}
	
	//ADD ENCTYPE MULTIPART DATA FOR UPLOAD FILE
	public function form_attr($enctype)
	{
		$this->_form_attr[array_keys($enctype)[0]] = $enctype[array_keys($enctype)[0]];

		return $this;
	}
	
	/**
	 * name tag, $attr, url
	 * @param type $attr
	 */
	public function data_attr($name_tag = NULL, $attr = NULL, $url = NULL)
	{
		if ($name_tag === NULL)
		{
			return $this->_tag_attr;
		}
		
		if (in_array($name_tag, $this->_columns))
		{
			$this->_tag_attr[$name_tag] = [
				'tag' => $name_tag,
				'attr' => 'data-' . $attr,
				'url' => $url,
			];
		}
		else
		{
			return;
		}
		
		return $this;
	}
	
	public function date($colums)
	{
		$this->_date = $colums;
		
		return $this;
	}
	
	protected function _prepare()
	{
		// If not columns set, use labels keys
		if ( ! $this->_columns)
		{
			$this->_columns = array_keys($this->_labels ?: $this->_model->labels());
		}

		//$this->_columns = array_keys($this->_labels);
		$this->_labels += $this->_model->labels();

		$this->_set_data();

		if ($this->_request->method() == 'POST')
		{
			$this->_save_data();
		}
	}
	
	public function callback($action, $mode, $callable)
	{
		$this->_callables[$action][$mode] = $callable;
		
		return $this;
	}
	
	public function extra_validation(Validation $validation)
	{
		$this->_extra = $validation;
		
		return $this;
	}
	
	public function multiple($column, $options)
	{
		$this->_multiple[$column] = $options;
		
		$this->_types[$column] = 'multiple';
		
		return $this;
	}
	
	protected function _save_data()
	{
	
		$action = $this->_request->action();

		$expected = array_diff($this->_columns, $this->_readonly);
		
		$values = array_intersect_key($this->_request->post(), $this->_model->table_columns());
		
		try
		{
			$this->_model->values($values, $expected, TRUE);
			
			if (isset($this->_callables[$action]['before']))
			{
				call_user_func($this->_callables[$action]['before'], $this->_model);
			}
			
			if ($this->_extra)
			{
				$this->_extra->labels($this->_labels);
			}
			
			$this->_model->save($this->_extra);
			
			if (isset($this->_callables[$action]['after']))
			{
				call_user_func($this->_callables[$action]['after'], $this->_model);
			}
			
			$this->_save_multiple();
			
			Session::instance()->set('info', $msg = strtr('Model :model with :pk :id :mode', array(
				':model' => $this->_model->object_name(),
				':pk' => $this->_model->primary_key(),
				':id' => $this->_model->pk(),
				':mode' => ($action == 'new') ? 'created' : 'updated',
			)));
			
			Log::access($msg);

			HTTP::redirect($this->_get_referrer());
		}
		catch (ORM_Validation_Exception $e)
		{
			$this->_errors = Arr::flatten($e->errors(''));
		}
	}
	
	protected function _save_multiple()
	{
		foreach ($this->_multiple as $column => $options)
		{
			$alias = $options['alias'];
			
			$new_keys = $this->_request->post($column) ?: [];
			
			$old_keys = $this->_model->$alias->find_all()->as_array(NULL, $this->_model->$alias->primary_key());
			
			$to_remove = array_diff($old_keys, $new_keys);
			$to_add = array_diff($new_keys, $old_keys);
			
			if ($to_remove)
			{
				$this->_model->remove($alias, $to_remove);
			}
			
			if ($to_add)
			{
				$this->_model->add($alias, $to_add);
			}
			
			/*$this->_model->remove($alias);
			
			if ($new_keys)
			{
				$this->_model->add($alias, $new_keys);
			}*/
		}
	}
	
	protected function _set_data()
	{
		if ($this->_request->action() == 'new')
		{
			$this->_model = $this->_model
				->values($this->_new_values);
			
			return;
		}
		
		$id = $this->_request->param('id');
		$pk = $this->_model->primary_key();
		
		if ( ! $this->_query)
		{
			$this->_query = DB::select()
				->from($this->_model->table_name())
				->where($pk, '=', $id);
		}
		
		$data = $this->_query
			->as_object(get_class($this->_model))
			->execute();
		
		$count = $data->count();
		
		if ( ! $count)
		{
			throw new Kohana_Exception('Model :model with :pk :id could not be found', array(
				':model' => $this->_model->object_name(),
				':pk' => $pk,
				':id' =>  $id,
			));
		}
		elseif ($count > 1)
		{
			throw new Kohana_Exception('More than one record for Model :model with :pk :id was found', array(
				':model' => $this->_model->object_name(),
				':pk' => $pk,
				':id' =>  $id,
			));
		}
		
		$this->_model = $data->current();
	}
	
	public function get_controls()
	{
		$controls = array();

		foreach ($this->_columns as $column)
		{
			$disabled = in_array($column, $this->_readonly) ? 'disabled' : NULL;
			
			if ($this->_request->action() == 'new' AND $disabled AND ! array_key_exists($column, $this->_new_values))
			{
				continue;
			}

			$control_class = isset($this->_errors[$column]) ? 'has-error' : NULL;
			
			$type = $this->_get_type($column);

			$label = $this->_labels[$column];

			$value = $this->_get_value($column);
			
			$required = in_array($column, $this->_required) ? 'required' : NULL;
			
			$options = $this->_get_options($column);

			$data_attr = $this->_get_data_attr($column);
			
			$helptext = Arr::get($this->_helptexts, $column);
			
			$custom_attr = Arr::get($this->_custom, $column);
			$custom_class= NULL;
			
			if (isset($custom_attr['class']))
			{
				$custom_class = $custom_attr['class'];
				unset($custom_attr['class']);
			}
			
			$controls[] = Theme_View::factory($this->_controls_view)
				->set(compact('control_class', 'type', 'column', 'label', 'value', 
					'disabled', 'required', 'options', 'helptext', 'custom_attr',
					'custom_class', 'data_attr'));
		}
		//debug($controls);
		return $controls;
	}
	
	protected function _get_data_attr($column)
	{
		if(in_array($column, array_keys($this->_tag_attr)))
		{
			return $this->_tag_attr[$column];
		}
		else
		{
			return;
		}
	}
	
	protected function _get_options($column)
	{
		//return isset($this->_options[$column]) ? array('' => '--') + $this->_options[$column] : array();
		$options = Arr::get($this->_options, $column, FALSE);
		
		if ($options !== FALSE)
		{
			return isset($this->_multiple[$column]) ? $options : (array('' => '--') + $options);
		}
		
		return array();
	}
	
	protected function _get_type($column)
	{
		// @temp
		if (isset($this->_types[$column]))
			return $this->_types[$column];
			
		return $this->_get_options($column) ? 'select' : 'input';
	}
	
	protected function _get_value($column)
	{
		// From POST
		if ($this->_request->post($column) !== NULL)
			return $this->_request->post($column);
		
		//return isset($this->_model->$column) ? $this->_model->$column : '';
		
		// From Model
		if (isset($this->_model->$column))
			return $this->_model->$column;
		
		// From Many To Many
		if ($options = Arr::get($this->_multiple, $column))
		{
			$model = $this->_model->{$options['alias']};
			$column_name = $options['column_name'];
			
			return /*debug*/array_keys($model
				->find_all()
				->as_array($model->primary_key(), $column_name));
		}
                
                // Load custom value to inputs
                if(isset($this->_new_values[$column]))
                        return $this->_new_values[$column];
		
		return '';
	}
	
	protected function _get_referrer()
	{
		return $this->_request->post('referrer') ?: $this->_request->referrer();
	}
	
	public function format($column, callable $callable)
	{
		$this->_format[_column] = $callable;
		
		return $this;
	}
	
	/**
	 * 
	 * @param type $column
	 * @param type $value
	 * @return \AdminForm
	 */
	public function set_values($column = NULL, $value = NULL)
	{
		if ($column === NULL)
		{
			return $this->_new_values;
		}
		
		if (is_array($column))
		{
			$this->_new_values = $column;
		}
		else
		{
			$this->_new_values[$column] = $value;
		}
		
		return $this;
	}
	
}
