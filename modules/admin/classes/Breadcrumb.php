<?php defined ('SYSPATH') or die ('No direct script access.');

class Breadcrumb {
	
	/**
	 *
	 * @var  array
	 */
	public static $items = array();
	
	protected $_view = 'breadcrumb';
	
	/**
	 * 
	 * @param type $name
	 * @param type $url
	 * @param type $icon
	 */
	public static function add($name, $url = NULL, $icon = NULL)
	{
		Breadcrumb::$items[$name] = compact('url', 'icon');
	}
	
	public static function build()
	{
		//debug(self::$items);
		
		
		return Theme_View::factory('breadcrumbs')
			->set('items', self::$items);
	}
	
}
