<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2014 Inspired Solutions
 */
class Theme {
	
	/**
	 *
	 * @var  string  default theme name
	 */
	//public static $default = 'default';
	
	/**
	 *
	 * @var  Theme  current theme
	 */
	protected static $instance;
	
	public $screen = TRUE;
	
	protected $_name;
	protected $_path;
	protected $_media_path;
	protected $_css = array();
	protected $_js = array();
	
	protected $_extra = array();
	
	protected $_headers = array();
	
	protected $_buttons = array();
	
	/**
	 * 
	 * @param  string  $name  theme name
	 * @return  Theme
	 */
	public static function instance()
	{
		if ( ! Theme::$instance)
			throw new Kohana_Exception('No theme setted.');
		
		return Theme::$instance;
	}
	
	/**
	 * 
	 * @param  string  $name
	 * @return  Theme
	 */
	public static function init($name)
	{
		Theme::$instance = $theme = new Theme($name);
		
		return $theme->load();
	}
	
	public function __construct($name)
	{
		$this->_name = $name;
		$this->_path = "views/{$name}/";
		$this->_media_path = "media/";
	}
	
	/**
	 * Load can't be called on construct. Because this method includes init.php, which uses Theme::instance
	 * @return \Theme
	 */
	public function load()
	{
		include $this->real_path('init.php');
		
		return $this;
	}
	
	public function name()
	{
		return $this->_name;
	}
	
	public function path($file = NULL)
	{
		return $this->_path.$file;
	}
	
	public function real_path($file = NULL)
	{
		return APPPATH.$this->path().$file;
	}
	
	public function media_path($file = NULL)
	{
		return $this->_media_path.$file;
	}
	
	/**
	 * 
	 * @param array $assets
	 * @return \Theme
	 */
	public function assets(array $assets)
	{
		$this->css($assets['css']);
		$this->js($assets['js']);
		
		return $this;
	}
	
	/**
	 * 
	 * @param type $group
	 * @param type $name
	 * @param type $path
	 * @return \Theme
	 */
	public function css($group, $name = NULL, $path = NULL)
	{
		if (is_array($group))
		{
			$this->_css = $group;
		}
		elseif (is_array($name))
		{
			$this->_css[$group] = $name;
		}
		else
		{
			if ( ! isset($this->_css[$group]))
			{
				$this->_css[$group] = array();
			}
			
			$this->_css[$group][$name] = $path;
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @param type $group
	 * @param type $name
	 * @param type $path
	 * @return \Theme
	 */
	public function js($group, $name = NULL, $path = NULL)
	{
		if (is_array($group))
		{
			$this->_js = $group;
		}
		elseif (is_array($name))
		{
			$this->_js[$group] = $name;
		}
		else
		{
			if ( ! isset($this->_js[$group]))
			{
				$this->_js[$group] = array();
			}
			
			$this->_js[$group][$name] = $path;
		}
		
		return $this;
	}
	
	public function extra($extra = NULL)
	{
		if ($extra === NULL)
		{
			return $this->_extra;
		}
		
		$this->_extra[] = $extra;
		
		return $this;
	}
	
	public function get_extra()
	{
		return implode(PHP_EOL, $this->_extra);
	}
	
	/**
	 * 
	 * @param type $group
	 * @return type
	 */
	public function get_css($group = NULL)
	{
		if ($group === NULL)
		{
			$styles = /*Arr::flatten*/($this->_css);
		}
		else
		{
			$styles = $this->_css[$group];
		}
		
		return $this->_compile(array('HTML', 'style'), $styles);
	}
	
	/**
	 * 
	 * @param type $group
	 * @return type
	 */
	public function get_js($group = NULL)
	{
		if ($group === NULL)
		{
			$scripts = /*Arr::flatten*/($this->_js);
		}
		else
		{
			$scripts = $this->_js[$group];
		}
		
		return $this->_compile(array('HTML', 'script'), $scripts);
	}
	
	protected function _compile($callable, $assets)
	{
		$compiled = array();
		foreach ($assets as $group)
		{
			foreach ($group as $path)
			{
				$compiled[] = call_user_func($callable, $this->media_path(ltrim($path, '/')));
			}
		}
		
		return implode(PHP_EOL, $compiled);
	}
	
	public function buttons($name = NULL, array $options = NULL)
	{
		if ($name === NULL)
		{
			return $this->_buttons;
		}
		
		if (is_array($name))
		{
			$this->_buttons = $name;
		}
		else
		{
			$this->_buttons[$name] = $options;
		}
		
		return $this;
	}
	
	public function headers($key = NULL, $val = NULL)
	{
		if ($key === NULL)
		{
			return $this->_headers;
		}
		
		if (is_array($key))
		{
			$this->_headers = $key;
		}
		else
		{
			$this->_headers[$key] = $val;
		}
		
		return $this;
	}
	
	/**
	 * 
	 * @param type $name
	 * @param type $options
	 * @param type $path
	 * @return  Theme_View
	 */
	public static function button($name, $options = NULL, $path = NULL)
	{
		$theme = Theme::instance();
		
		if ($options === NULL)
		{
			$options = $theme->_buttons[$name];
		}
		elseif (isset($theme->_buttons[$name]))
		{
			$options = array_merge($theme->_buttons[$name], $options);
		}
		
		/*if (isset($options['query']))
		{
			$path = URL::query($options['query']);
			
			// We don't need it anymore
			unset($options['query']);
		}
		else
		{
			$path = Route::url('admin', array('controller' => Request::$current->controller(), 'action' => $name));
		}*/
		
		return Theme_View::factory('button')
			->set(compact('name', 'options', 'path'));
	}
	
}
