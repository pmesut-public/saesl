<?php defined('SYSPATH') or die('No direct script access.');

return array(

	'default' => array(
		'current_page'      => array('source' => 'query_string', 'key' => 'page'), // source: "query_string" or "route"
		'total_items'       => 999999,
		'items_per_page'    => 2,
		'view'              => 'pagination',
		'auto_hide'         => FALSE,
		'first_page_in_url' => FALSE,
	),

);
