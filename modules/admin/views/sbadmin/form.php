<div class="panel panel-default">
	<div class="panel-heading">
		<i class="fa fa-bar-chart-o fa-fw"></i> <?= $title ?>
		<div class="pull-right"></div>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">
		<form class="form-horizontal" role="form" method="post">
			
			<?php if ($errors): ?>
				<div class="alert alert-danger fade in text-left" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<strong>Error!</strong>
					
					<?php foreach ($errors as $error): ?>
						<br><?= $error ?>
					<?php endforeach ?>
					
				</div>
			<?php endif ?>
			
			<?php foreach ($controls as $control): ?>
			
				<?= $control ?>
			
			<?php endforeach ?>
			
			<input type="hidden" name="referrer" value="<?= $back_url ?>" >
			
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">Guardar</button>
					<a href="<?= $back_url ?>" class="btn btn-default">Cancelar</a>
				</div>
			</div>
		</form>
		<!-- /.panel-body -->
	</div>
	<!-- /.panel -->
</div>
