<?php if ($filters): ?>

	<div class="panel panel-success panel-filters hidden-print">
		<div class="panel-heading">
			<h4 class="panel-title" data-toggle="_collapse" data-parent="#accordion" href="#collapse-filters">
				Filtros
			</h4>
		</div>
		<div id="collapse-filters" class="panel-collapse _collapse">
			<div class="panel-body">

				<?php foreach ($filters as $column => $args): ?>
					<div class="btn-group">
						<?php $btn_type = (($val = $request->query('filter__'.$column)) !== NULL) ? 'success' : 'default' ?>
						<button class="btn btn-<?= $btn_type ?> dropdown-toggle" type="button" id="dropdown_<?= $column ?>" data-toggle="dropdown">
							<?php if ($val !== NULL): ?>
								<?= $args['options'][$val] ?>
							<?php else: ?>
								<?= $args['name'] ?>
							<?php endif ?>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdown_<?= $column ?>">

							<li role="presentation"><a href="<?= $site.URL::query(array('filter__'.$column => NULL, 'page' => 1)) ?>">-</a></li>
							<?php foreach ($args['options'] as $key => $value): ?>
								<li role="presentation"><a href="<?= $site.URL::query(array('filter__'.$column => $key, 'page' => 1)) ?>"><?= $value ?></a></li>
							<?php endforeach ?>
						</ul>
					</div>
				<?php endforeach ?>
				
			</div>
		</div>
	</div>
	
<?php endif ?>