<table class="table table-bordered table-hover table-striped">
	<thead>
		<tr>
			<?php foreach ($thead as $th): ?>
				<?php echo $th ?>
			<?php endforeach ?>
		</tr>
	</thead>

	<tbody>

		<?php foreach ($tbody as $row): ?>
			<tr>
				<?php foreach ($row as $td): ?>
					<?php echo $td ?>
				<?php endforeach ?>
			</tr>
		<?php endforeach ?>

	</tbody>
</table>
