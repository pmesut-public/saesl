<?php if ($searches): ?>
	

	<div class="panel panel-info panel-searches hidden-print">
		<div class="panel-heading">
			<h4 class="panel-title" data-toggle="_collapse" data-parent="#accordion" href="#collapse-searches">
				Búsqueda
			</h4>
		</div>
		<div id="collapse-searches" class="panel-collapse _collapse">
			<div class="panel-body">

				<form class="form filters">

					<?php foreach ($hiddens as $hidden): ?>
						<?= $hidden ?>
					<?php endforeach ?>
					
					<?php foreach ($searches as $column => $args): ?>
						<div class="form-group">
							<input type="text" name="search__<?= $column ?>" 
									value="<?= $request->query('search__'.$column) ?: '' ?>"
									class="form-control" id="search__<?= $column ?>" 
									placeholder="<?= $args['name'] ?>">
						</div>
					<?php endforeach ?>
					
					<button type="submit" class="btn btn-primary">Buscar</button>
					
				</form>
				
			</div>
		</div>
	</div>
	
<?php endif ?>
