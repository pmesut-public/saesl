<div class="pagination-wrapper clearfix hidden-print">
	<div class="pagination-info pull-left">
		Total: <?= $total_items ?> / 
		<?= $total_pages ?> <?= Inflector::plural('Página', $total_pages) ?>
	</div>
	<ul class="pagination pagination-sm no-margin pull-right">

		<?php if ($first_page !== FALSE): ?>
			<li><a href="<?= HTML::chars($page->url($first_page)) ?>" rel="first">
				First</a></li>
		<?php else: ?>
			<li class="disabled"><a>First</a></li>
		<?php endif ?>

		<?php if ($previous_page !== FALSE): ?>
			<li><a href="<?= HTML::chars($page->url($previous_page)) ?>" rel="prev">
				&laquo;</a></li>
		<?php else: ?>
			<li class="disabled"><a>&laquo;</a></li>
		<?php endif ?>

		<?php for ($i = $start; $i <= $end; $i++): ?>

			<?php if ($i == $current_page): ?>
				<li class="active"><a><?= $i ?></a></li>
			<?php else: ?>
				<li><a href="<?= HTML::chars($page->url($i)) ?>">
					<?= $i ?></a></li>
			<?php endif ?>

		<?php endfor ?>

		<?php if ($next_page !== FALSE): ?>
			<li><a href="<?= HTML::chars($page->url($next_page)) ?>" rel="next">
				&raquo;</a></li>
		<?php else: ?>
			<li class="disabled"><a>&raquo;</a></li>
		<?php endif ?>

		<?php if ($last_page !== FALSE): ?>
			<li><a href="<?= HTML::chars($page->url($last_page)) ?>" rel="last">
				Last</a></li>
		<?php else: ?>
			<li class="disabled"><a>Last</a></li>
		<?php endif ?>

	</ul>
</div>
