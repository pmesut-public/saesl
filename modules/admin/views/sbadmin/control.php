<div class="form-group <?= $control_class ?>">
	<label for="<?= $column ?>" class="col-sm-2 control-label"><?= $required ? '* ' : '' ?><?= $label ?></label>
	<div class="col-sm-6">
		
		<?php switch ($type): ?><?php case 'input': ?>
			
			<input type="text" class="form-control" name="<?= $column ?>" id="<?= $column ?>" value="<?= $value ?>" placeholder="<?= $label ?>" <?= $required ?> <?= $disabled ?> >
			
		<?php break ?><?php case 'password': ?>
			
			<input type="password" class="form-control" name="<?= $column ?>" id="<?= $column ?>" value="" placeholder="<?= $label ?>" <?= $required ?> <?= $disabled ?> >
			
		<?php break ?><?php case 'select': ?>
			
			
			<select name="<?= $column ?>" id="<?= $column ?>" class="form-control" <?= $required ?> >
				
				<?php foreach ($options as $key => $val): ?>
					<option value="<?= $key ?>" <?= ($key == $value) ? 'selected' : NULL ?> ><?= $val ?></option> 
				<?php endforeach ?>
				
			</select>
			
		<?php endswitch ?>
		
		<?php if ($helptext): ?>
			<span class="help-block"><?= $helptext ?></span>
		<?php endif ?>
		
	</div>
</div>