<section class="content">

	<div class="box">
		<div class="box-header pagination-top">
			<!--h3 class="box-title">Hover Data Table</h3-->
			<?= $pagination ?>
		</div><!-- /.box-header -->
		<div class="box-body box-responsive">

			<table class="table table-bordered table-hover table-striped">
				<thead>
					<tr>
						<?php foreach ($thead as $th): ?>
							<?php echo $th ?>
						<?php endforeach ?>
					</tr>
				</thead>

				<tbody>

					<?php foreach ($tbody as $row): ?>
						<tr>
							<?php foreach ($row as $td): ?>
								<?php echo $td ?>
							<?php endforeach ?>
						</tr>
					<?php endforeach ?>

				</tbody>
			</table>

		</div><!-- /.box-body -->

		<div class="box-footer pagination-bottom">
			<?= $pagination ?>
		</div>

	</div><!-- /.box -->

</section><!-- /.content -->
