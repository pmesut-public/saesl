<?php if ($searches): ?>
	
	<div class="box box-primary hidden-print">
		<div class="box-header">
			<i class="fa fa-search"></i>
			<h4 class="box-title" data-toggle="_collapse" data-parent="#accordion" href="#collapse-searches">
				Búsqueda
			</h4>
			<div class="box-tools pull-right">
				<button class="btn btn-flat btn-primary btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
				<button class="btn btn-flat btn-primary btn-xs" data-widget="remove"><i class="fa fa-times"></i></button>
			</div>
		</div>
		<div class="box-body">

			<form class="form filters">

				<?php foreach ($hiddens as $hidden): ?>
					<?= $hidden ?>
				<?php endforeach ?>

				<?php foreach ($searches as $column => $args): ?>
					<div class="form-group">
						<input type="text" name="search__<?= $column ?>" 
								value="<?= $request->query('search__'.$column) ?: '' ?>"
								class="form-control" id="search__<?= $column ?>" 
								placeholder="<?= $args['name'] ?>">
					</div>
				<?php endforeach ?>

				<button type="submit" class="btn btn-flat btn-primary">Buscar</button>

			</form>

		</div>
	</div>
	
<?php endif ?>
