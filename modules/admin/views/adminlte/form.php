<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">Datos generales</h3>
	</div><!-- /.box-header -->
	<!-- form start --> 
	<form role="form" class="form-horizontal" method="post" <?= isset(array_keys($form_attr)[0])? array_keys($form_attr)[0]. '=' .$form_attr[array_keys($form_attr)[0]] : '' ?>>
		<div class="box-body">
			<?php if ($errors): ?>
				<div class="row">
					<div class="col-sm-6 col-sm-offset-2">
						<div class="callout callout-danger fade in text-left" role="alert">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<h4>Error!</h4>

							<?php foreach ($errors as $error): ?>
								<p><?= $error ?></p>
							<?php endforeach ?>

						</div>
					</div>
				</div>
			<?php endif ?>

			<?php foreach ($controls as $control): ?>
				<?= $control ?>
			<?php endforeach ?>

			<input type="hidden" name="referrer" value="<?= $back_url ?>" >
			
		</div><!-- /.box-body -->

		<div class="box-footer">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-flat btn-primary">Guardar</button>
					<a href="<?= $back_url ?>" class="btn btn-flat btn-default">Cancelar</a>
				</div>
			</div>
		</div>
	</form>
</div>
<script>
	window.dataForm = <?= json_encode($date) ?>
</script>	