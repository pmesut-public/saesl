<aside class="right-side">                
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?= $title ?>
			<small><?= $subtitle ?></small>
		</h1>
		
		<?= Breadcrumb::build() ?>
		
	</section>

	<div class="list-buttons">
		<?php foreach ($table->buttons as $button): ?>
			<?= $button ?>
		<?php endforeach ?>
	</div>
	
	<!-- Main content -->
	<section class="content">

		<?php if ($table->searches OR $table->filters): ?>
			<div class="row">
				
				<?php if ($table->searches): ?>
					<div class="col-sm-5">
						<?= $table->searches ?>
					</div>
				<?php endif ?>
				
				<?php if ($table->filters): ?>
					<div class="col-sm-7 <?= $table->searches ? '' : 'col-sm-offset-5' ?>">
						<?= $table->filters ?>
					</div>
				<?php endif ?>
				
			</div>
		<?php endif ?>
		
		<div class="box">
			<div class="box-header pagination-top">
				<!--h3 class="box-title">Hover Data Table</h3-->
				<?= $table->pagination ?>
			</div><!-- /.box-header -->
			<div class="box-body box-responsive">

				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<?php foreach ($table->thead as $th): ?>
								<?php echo $th ?>
							<?php endforeach ?>
						</tr>
					</thead>

					<tbody>

						<?php foreach ($table->tbody as $row): ?>
							<tr>
								<?php foreach ($row as $td): ?>
									<?php echo $td ?>
								<?php endforeach ?>
							</tr>
						<?php endforeach ?>

					</tbody>
				</table>

			</div><!-- /.box-body -->

			<div class="box-footer pagination-bottom">
				<?= $table->pagination ?>
			</div>

		</div><!-- /.box -->

	</section><!-- /.content -->

	
</aside><!-- /.right-side -->

<iframe class="iframe"></iframe>
