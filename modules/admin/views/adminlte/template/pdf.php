<?php if ($filters): ?>
	<?php foreach ($filters->filters as $name => $filter): ?>

		<?php $value =  $filters->request->query('filter__'.$name) ?>

		<?php if ($value): ?>
			<p><strong><?= $filter['name'] ?>:</strong> <?= $filter['options'][$value] ?></p>
		<?php endif ?>

	<?php endforeach ?>
<?php endif ?>

<?php /*foreach ($content->table->custom as $key => $val): ?>
	<p><strong><?= $key ?>:</strong> <?= $val ?></p>
<?php endforeach*/ ?>

<table class="table table-bordered table-hover table-striped" border="1" style="padding: 5px;">
	<thead>
		<tr style="background-color: #ddd;">
			<?php foreach ($thead as $th): ?>
				<?php echo $th ?>
			<?php endforeach ?>
		</tr>
	</thead>

	<tbody>

		<?php foreach ($tbody as $row): ?>
			<tr nobr="true">
				<?php foreach ($row as $td): ?>
					<?php echo $td ?>
				<?php endforeach ?>
			</tr>
		<?php endforeach ?>

	</tbody>
</table>
