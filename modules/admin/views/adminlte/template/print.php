<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?= $content->title ?> - <?= $content->subtitle ?></title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<?= $theme->get_css() ?>
		
	</head>
	<body class="skin-blue">
		<!-- header logo: style can be found in header.less -->

		<div class="wrapper row-offcanvas row-offcanvas-left">

			<!-- Right side column. Contains the navbar and content of the page -->

			<aside class="right-side strech">
				
				<section class="content-header">
					<h1><?= $content->title ?>
						<small><?= $content->subtitle ?></small></h1>
				</section>
			
				<section class="content">
					
					<?php if ($content->table->filters): ?>
						<?php foreach ($content->table->filters->filters as $name => $filter): ?>

							<?php $value =  $content->table->filters->request->query('filter__'.$name) ?>

							<?php if ($value): ?>
								<p><strong><?= $filter['name'] ?>:</strong> <?= $filter['options'][$value] ?></p>
							<?php endif ?>

						<?php endforeach ?>
					<?php endif ?>
					
					<?php foreach ($content->table->custom_text as $key => $val): ?>
						<p><strong><?= $key ?>:</strong> <?= $val ?></p>
					<?php endforeach ?>
					
					<p><strong>Total:</strong> <?= $content->table->pagination->total_items ?></p>
					
					<div class="box box-responsive">
						<table class="table table-bordered table-hover table-striped" border="1">
							<thead>
								<tr>
									<?php foreach ($content->table->thead as $th): ?>
										<?php echo $th ?>
									<?php endforeach ?>
								</tr>
							</thead>

							<tbody>

								<?php foreach ($content->table->tbody as $row): ?>
									<tr nobr="true">
										<?php foreach ($row as $td): ?>
											<?php echo $td ?>
										<?php endforeach ?>
									</tr>
								<?php endforeach ?>

							</tbody>
						</table>
					</div>
				</section>
				
			</aside>

		</div><!-- ./wrapper -->
		
		<?php //$theme->get_js() ?>
		<script>
			window.print();
		</script>
		
	</body>
</html>
