<div class="form-group <?= $control_class ?>">
	<label for="<?= $column ?>" class="col-sm-2 control-label"><?= $required ? '* ' : '' ?><?= $label ?></label>
	<div class="col-sm-6">
		
		<?php switch ($type): ?><?php case 'input': ?>

			<input type="text" class="form-control <?= $custom_class ?>" name="<?= $column ?>" 
				id="<?= $column ?>" value="<?= $value ?>" placeholder="<?= $label ?>" <?= isset($data_attr) ? $data_attr['attr'] . ' = ' . $data_attr['url'] : '' ?>
				<?= $required ?> <?= $disabled ?> <?= HTML::attributes($custom_attr) ?> >
			
		<?php break ?><?php case 'file': ?>
			
			<input type="file" class="form-control <?= $custom_class ?>" name="<?= $column ?>" 
				id="<?= $column ?>" placeholder="<?= $label ?>" <?= isset($data_attr) ? $data_attr['attr'] . ' = ' . $data_attr['url'] : '' ?> 
				<?= $required ?> <?= $disabled ?> <?= HTML::attributes($custom_attr) ?> >
			
		<?php break ?><?php case 'date': ?>
		
			<input type="date" class="form-control <?= $custom_class ?> _date" name="<?= $column ?>" 
				_data-date-format="YYYY-MM-DD" 
				id="<?= $column ?>" value="<?= $value ?>" placeholder="<?= $label ?>" <?= isset($data_attr) ? $data_attr['attr'] . ' = ' . $data_attr['url'] : '' ?>
				<?= $required ?> <?= $disabled ?> <?= HTML::attributes($custom_attr) ?> >
			
		<?php break ?><?php case 'password': ?>
			
			<input type="password" class="form-control <?= $custom_class ?>" name="<?= $column ?>" 
				id="<?= $column ?>" value="" placeholder="<?= $label ?>" <?= isset($data_attr) ? $data_attr['attr'] . ' = ' . $data_attr['url'] : '' ?>
				<?= $required ?> <?= $disabled ?> <?= HTML::attributes($custom_attr) ?> >
			
		<?php break ?><?php case 'select': ?>
			
			<select class="form-control <?= $custom_class ?>" name="<?= $column ?>" <?= $disabled ?> <?= isset($data_attr) ? $data_attr['attr'] . ' = ' . $data_attr['url'] : '' ?>
				id="<?= $column ?>" <?= $required ?> <?= HTML::attributes($custom_attr) ?> >
				
				<?php foreach ($options as $key => $val): ?>
					<option value="<?= $key ?>" <?= ($key == $value) ? 'selected' : NULL ?> ><?= $val ?></option> 
				<?php endforeach ?>
				
			</select>
			
		<?php break ?><?php case 'multiple': ?>
			
			<div class="checkbox">
				<?php foreach ($options as $key => $val): ?>
					<label>
						<input name="<?= $column ?>[]" value="<?= $key ?>" type="checkbox" <?= isset($data_attr) ? $data_attr['attr'] . ' = ' . $data_attr['url'] : '' ?>
							<?= in_array($key, $value) ? 'checked' : NULL ?> > <?= $val ?>
					</label><br>
				<?php endforeach ?>
			</div>
			
		<?php break ?><?php case 'textarea': ?>
			
			<textarea class="form-control <?= $custom_class ?>" name="<?= $column ?>" 
				id="<?= $column ?>" placeholder="<?= $label ?>" <?= isset($data_attr) ? $data_attr['attr'] . ' = ' . $data_attr['url'] : '' ?>
				<?= $required ?> <?= $disabled ?> <?= HTML::attributes($custom_attr) ?> 
				><?= $value ?></textarea>
			
		<?php endswitch ?>
		
		<?php if ($helptext): ?>
			<span class="help-block"><?= $helptext ?></span>
		<?php endif ?>
		
	</div>
</div>