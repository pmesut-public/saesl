<ol class="breadcrumb">
	
	<?php foreach ($items as $name => $item): ?>
		
		<?php $icon = $item['icon'] ? HTML::tag('i', NULL, array('class' => $item['icon'])) : '' ?>
		
		<?php if ($item['url']): ?>
			<li><a href="<?= $item['url'] ?>"><?= $icon ?> <?= $name ?></a></li>
		<?php else: ?>
			<li class="active"><?= $icon ?> <?= $name ?></li>
		<?php endif ?>
		
	<?php endforeach ?>
	
</ol>
