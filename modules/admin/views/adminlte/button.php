<a href="<?= $path ?>" class="<?= $options['btn_class'] ?>" 
	<?php if (isset($options['data-title'])): ?> data-title="<?= $options['data-title'] ?>" <?php endif ?>>
	<i class="<?= $options['icon_class'] ?>"></i> 
	<?php if (isset($options['title'])): ?> <?= $options['title'] ?> <?php endif ?>
</a>
