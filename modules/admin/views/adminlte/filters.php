<?php if ($filters): ?>

	<div class="box box-success hidden-print">
		<div class="box-header">
			<i class="fa fa-filter"></i>
			<h4 class="box-title">
				Filtros
			</h4>
			<div class="box-tools pull-right">
				<button class="btn btn-flat btn-success btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
				<button class="btn btn-flat btn-success btn-xs" data-widget="remove"><i class="fa fa-times"></i></button>
			</div>
		</div>
		<div class="box-body">
			
			<?php foreach ($filters as $column => $args): ?>
				<div class="btn-group">
					<?php $btn_type = (($val = $request->query('filter__'.$column)) !== NULL) ? 'success' : 'default' ?>
					<button class="btn btn-flat btn-<?= $btn_type ?> dropdown-toggle" type="button" id="dropdown_<?= $column ?>" data-toggle="dropdown">
						<?php if ($val !== NULL): ?>
							<?= $args['options'][$val] ?>
						<?php else: ?>
							<?= $args['name'] ?>
						<?php endif ?>
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu" aria-labelledby="dropdown_<?= $column ?>">

						<li role="presentation"><a href="<?= $site.URL::query(array('filter__'.$column => NULL, 'page' => 1)) ?>">-</a></li>
						<?php foreach ($args['options'] as $key => $value): ?>
							<li role="presentation"><a href="<?= $site.URL::query(array('filter__'.$column => $key, 'page' => 1)) ?>"><?= $value ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
			<?php endforeach ?>
			
		</div>
	</div>
	
<?php endif ?>