<?php $btn_type = (($val = $request->query($filter)) !== NULL) ? 'success' : 'default' ?>

<a href="<?= $site.URL::query(array($filter => ($val ? NULL : 1) )) ?>" class="btn btn-flat btn-<?= $btn_type ?>">
	<?= $name ?>
</a>
