<?php
/**
 * Description of MYPDF
 *
 * @author COMPONENTE2
 */
class TCPDF_Header extends TCPDF {

    public function Header()
	{
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
		
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
		
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
		
		$this->SetAlpha(0.2);
        $this->Image('/media/gproc_lte/img/vista-previa.png', 30,80, 160, 160, '', '', '', true, 72);
		$this->SetAlpha(1);
		
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }

}
