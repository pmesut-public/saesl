<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Common_Middleware_Auth extends Middleware {
	
	public function execute()
	{
		if ( ! Auth::instance()->logged_in())
		{
			throw new Middleware_Exception_Auth();
		}
		
		$this->controller->oUser = Auth::instance()->get_user();
	}
	
}
