<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Common_Middleware_Exception extends Kohana_Exception {
	
	protected $api_code;
	
	public function __construct($message = "", array $variables = NULL, $code = 0, \Exception $previous = NULL)
	{
		$message = $message === "" ? $this->message : $message;
		$code = $this->api_code ?: $code;
		
		parent::__construct($message, $variables, $code, $previous);
	}
	
}
