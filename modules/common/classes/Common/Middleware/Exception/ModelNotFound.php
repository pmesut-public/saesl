<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Common_Middleware_Exception_ModelNotFound extends Middleware_Exception {
	
	protected $message = ':model no encontrado (:id)';
	
}
