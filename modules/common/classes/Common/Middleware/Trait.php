<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
trait Common_Middleware_Trait {
	
	protected function handle_middleware_exceptions()
	{
		try
		{
			$this->set_middlewares();
		}
		catch (Middleware_Exception $e)
		{
			Session::instance()->set('error', $e->getMessage());
			$this->redirect('/');
		}
	}

	protected function set_middlewares()
	{
		//
	}
	
	public function middleware($name)
	{
		Middleware::factory($name, $this)->execute();
	}
	
}
