<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
trait Common_Middleware_TraitAjax {
	
	use Middleware_Trait;
	
	protected function handle_middleware_exceptions()
	{
		try
		{
			$this->set_middlewares();
		}
		catch (Middleware_Exception_Auth $e)
		{
			$this->send_error($e, 401);
		}
		catch (Middleware_Exception $e)
		{
			$this->send_error($e);
		}
	}
	
}
