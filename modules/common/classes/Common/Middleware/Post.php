<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Common_Middleware_Post extends Middleware {
	
	public function execute()
	{
		if ($this->controller->request->method() !== Request::POST)
		{
			throw new Middleware_Exception_Post();
		}
	}
	
}
