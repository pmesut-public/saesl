<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Common_Middleware_ACL extends Middleware {
	
	public function execute()
	{
		$directory = lcfirst($this->controller->request->directory());
		$controller = lcfirst($this->controller->request->controller());
		$action = $this->controller->request->action();
		
		$acl = ACL::instance();
		
		if ( ! $acl->allowed("{$directory}.{$controller}", $action))
		{
			throw new Middleware_Exception_ACL('', [
				'role' => $acl->get_role(),
				'controller' => $controller,
				'action' => $action,
			]);
		}
		
	}
	
}
