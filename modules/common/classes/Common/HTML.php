<?php defined('SYSPATH') or die('No direct script access.');

class Common_HTML extends Kohana_HTML {
	
	public static $strict = FALSE;
	
	/**
	 * 
	 * @param  string  $type
	 * @param  string  $title
	 * @param  array  $attributes
	 * @return string
	 */
	public static function tag($type, $title, array $attributes = NULL, $encode = TRUE)
	{
		//if ( ! empty($attributes)) $attributes = array_filter($attributes);
		
		return '<'.$type.HTML::attributes($attributes, $encode).'>'.$title.'</'.$type.'>';
	}
	
	public static function default_option($title = 'Seleccione')
	{
		return HTML::tag('option', __($title), array(
			'value' => '',
			//'selected',
			//'disabled',
		));
	}
	
	// overrided, patched '?' problem
	public static function anchor($uri, $title = NULL, array $attributes = NULL, $protocol = NULL, $index = TRUE)
	{
		if ($title === NULL)
		{
			// Use the URI as the title
			$title = $uri;
		}

		if ($uri === '')
		{
			// Only use the base URL
			$uri = URL::base($protocol, $index);
		}
		else
		{
			if (strpos($uri, '://') !== FALSE)
			{
				if (HTML::$windowed_urls === TRUE AND empty($attributes['target']))
				{
					// Make the link open in a new window
					$attributes['target'] = '_blank';
				}
			}
			elseif ($uri[0] !== '#' AND $uri[0] !== '?')
			{
				// Make the URI absolute for non-id anchors
				$uri = URL::site($uri, $protocol, $index);
			}
		}

		// Add the sanitized link to the attributes
		$attributes['href'] = $uri;

		return '<a'.HTML::attributes($attributes).'>'.$title.'</a>';
	}
	
	// Overrided, patched '?' problem
	public static function image($file, array $attributes = NULL, $protocol = NULL, $index = FALSE)
	{
		if ((strpos($file, '://') === FALSE) AND ($file[0] !== '?'))
		{
			// Add the base URL
			$file = URL::site($file, $protocol, $index);
		}

		// Add the image link
		$attributes['src'] = $file;

		return '<img'.HTML::attributes($attributes).' />';
	}
	
	// Overrided
	//	Added conditional encoding
	//	Added value="0"
	public static function attributes(array $attributes = NULL, $encode = TRUE)
	{
		if (empty($attributes))
			return '';

		$sorted = array();
		foreach (HTML::$attribute_order as $key)
		{
			if (isset($attributes[$key]))
			{
				// Add the attribute to the sorted list
				$sorted[$key] = $attributes[$key];
			}
		}

		// Combine the sorted attributes
		$attributes = $sorted + $attributes;

		$compiled = '';
		foreach ($attributes as $key => $value)
		{
			if ($value === NULL)
			{
				// Skip attributes that have NULL values
				continue;
			}

			if (is_int($key))
			{
				// Assume non-associative keys are mirrored attributes
				$key = $value;

				if ( ! HTML::$strict)
				{
					// Just use a key
					$value = FALSE;
				}
			}

			// Add the attribute key
			$compiled .= ' '.$key;

			if ($value OR HTML::$strict OR $value === '0')
			{
				// Add the attribute value
				$compiled .= '="'.($encode ? HTML::chars($value) : $value).'"';
			}
		}

		return $compiled;
	}
	
	public static function compile_style(array $style = NULL)
	{
		if (empty($style))
			return '';
		
		$compiled = array();
		foreach ($style as $property => $value)
		{
			$compiled[] = is_numeric($property) ?
				$value :
				"$property: $value;";
		}
		
		return implode(' ', $compiled);
	}
	
}
