<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2014 ProCalidad
 */
class Common_TreeArray {
	
	private $_data;
	
	private $_indicadores;
	
	public static function factory($data, $indicadores)
	{
		return new TreeArray($data, $indicadores);
	}
	
	public function __construct($data, $indicadores)
	{
		$this->_data = $data;
		$this->_indicadores = $indicadores;
	}
	
	public function get()
	{
		$data = $this->_data;
		//debug($data);
		
		$indicadores = $this->_indicadores;
		
		$result = array();
		
		foreach ($data as $row)
		{
			$inner = &$result;
			
			foreach ($indicadores as $indicador)
			{
				$value = $row[$indicador];
				
				if ( ! isset($inner[$value]))
					$inner[$value] = array();
				
				$inner = &$inner[$value];
				
				unset($row[$indicador]);
			}
			
			$inner = $row;
		}
		
		return $result;
	}
}
