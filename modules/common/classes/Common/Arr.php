<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Common_Arr extends Kohana_Arr {
	
	public static function filter_recursive($array, $callback = NULL)
	{
		foreach ($array as $index => $value)
		{
			if (is_array($value))
			{
				$array[$index] = Arr::filter_recursive($value, $callback);
			}
			else
			{
				$array[$index] = call_user_func($callback, $value);
			}
			
			if ( ! $array[$index])
			{
				unset($array[$index]);
			}
		}
		
		return $array;
	}
	
	public static function assoc_to_array($array, $keys)
	{
		return array_map(
			function ($key, $val) use ($keys)
			{
				return [
					$keys[0] => (string) $key,
					$keys[1] => $val,
				];
			}, 
			array_keys($array),
			$array
		);
	}
	
	public static function array_to_assoc($array, $key, $val = NULL)
	{
		return array_reduce(
			$array, 
			function ($carry, $item) use ($key, $val)
			{
				$carry[$item[$key]] = $val ? $item[$val] : $item;
				return $carry;
			},
			[]
		);
	}
	
	public static function group_by($arr, $fn)
	{
		return array_reduce($arr, function($carry, $item) use ($fn) {
			//debug2($item['id']);
			$key = $fn($item);
			$carry[$key] = isset($carry[$key]) ? array_merge($carry[$key], [$item]) : [$item];
			return $carry;
		}, []);
	}
	
}
