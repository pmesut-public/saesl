<?php defined('SYSPATH') OR die('No direct script access.');

class Common_LogTable {
	
	private $header = [];
	
	private $body = [];
	
	private $current = [];
	
	private $attributes = [];
	
	public static function factory($attributes = [])
	{
		return new LogTable($attributes);
	}
	
	public function __construct($attributes)
	{
		$this->attributes = $attributes;
	}
	
	public function set_header($array)
	{
		$this->header = $array;
		
		return $this;
	}
	
	public function partial($array)
	{
		$this->current = array_merge($this->current, $array);
		
		return $this;
	}
	
	public function clean()
	{
		$this->current = [];
		
		return $this;
	}
	
	public function log($array = [])
	{
		if ($this->current)
		{
			$array = array_merge($this->current, $array);
			
			$this->current = [];
		}
		
		$this->header = array_merge($this->header, array_diff(array_keys($array), $this->header));
		
		$this->body[] = $array;
		
		return $this;
	}
	
	public function show($attributes = [])
	{
		$head = $body = '';
		foreach ($this->header as $col)
		{
			$head .= '<th>'.$col.'</th>';
		}
		
		foreach ($this->body as $row)
		{
			$tr = '';
			foreach ($this->header as $col)
			{
				$tr .= '<td title="'.$col.'">'.Arr::get($row, $col).'</td>';
			}
			
			$body .= HTML::tag('tr', $tr);
		}
		
		return HTML::tag('table', HTML::tag('thead', $head).HTML::tag('tbody', $body), $attributes ?: $this->attributes);
	}
	
	public function stop($array = NULL)
	{
		if ($array)
		{
			$this->log($array);
		}
		
		echo $this->show();
		die();
	}
	
	public function write($filename)
	{
		$csv = new parseCSV();
		$csv->encoding('UTF-8', 'ISO-8859-1//TRANSLIT');
		
		$out = $csv->output(NULL, $this->get_body_formatted(), $this->header);
		
		file_put_contents($filename, $out);
		
		return $this;
	}
	
	private function get_body_formatted()
	{
		$body = [];
		
		foreach ($this->body as $i => $row)
		{
			$body[] = [];
			foreach ($this->header as $col)
			{
				$body[$i][] = Arr::get($row, $col);
			}
		}
		
		return $body;
	}
	
	public function read($filename)
	{
		if ( ! filesize($filename))
		{
			return;
		}
		
		$csv = new parseCSV();
		//$csv->encoding('UTF-8', 'ISO-8859-1//TRANSLIT');
		
		$csv->parse($filename);
		
		$this->header = array_keys(current($csv->data));
		
		$this->body = $csv->data;
		
		return $this;
	}
	
	public function fill($head, $body)
	{
		$this->header = $head;
		$this->body = $body;
		
		return $this;
	}
	
	public function body()
	{
		return $this->body;
	}
	
}
