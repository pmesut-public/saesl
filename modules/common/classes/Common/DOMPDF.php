<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 * @package    C2
 * @author     COMPONENTE 2 <soporte@procalidad.gob.pe>
 * @copyright  (c) 2015 ProCalidad
 */
class Common_DOMPDF {

	protected $author;
	
	protected $title;
	
	private $view;
	
	private $PDF_handler = 'DOMPDF';
	
	public static function factory()
	{
		return new PDFDOM;
	}
	
	public function set($key, $value)
	{
		$this->$key = $value;
		
		return $this;
	}
	
	public function set_handler($handler)
	{
		$this->PDF_handler = $handler;
		
		return $this;
	}
	
	public function create_handler()
	{
        //require_once(MODPATH . 'admin\vendor\dompdf\dompdf_config.inc.php');
        require_once Kohana::find_file('vendor', 'dompdf/dompdf_config.inc');

        return new $this->PDF_handler();
	}
    
	public function get()
	{
		$dompdf = $this->create_handler();

        $dompdf->load_html($this->view);

        $dompdf->render();

        $dompdf->stream("{$this->filename}.pdf");
	}
	
}
