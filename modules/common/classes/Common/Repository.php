<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Common_Repository {
	
	protected $value;
	
	public static function factory($name, $params = NULL)
	{
		$class = 'Repository_'.$name;
		
		return new $class($name, $params);
	}
	
	public function __construct($name, $params)
	{
		$this->value = ORM::factory($name, $params);
	}
	
	public function value()
	{
		return $this->value;
	}
	
	public function clear()
	{
		$this->value->clear();
		
		return $this;
	}
	
	public function __call($name, $arguments)
	{
		if ( ! method_exists($this, $name))
		{
			return call_user_func_array([$this->value, $name], $arguments);
		}
	}
	
}
