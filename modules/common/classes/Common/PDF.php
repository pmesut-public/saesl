<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
class Common_PDF {
	
	protected $author;
	
	protected $title;
	
	private $view;
	
	private $PDF_handler = 'TCPDF';
	
	public static function factory()
	{
		return new PDF;
	}
	
	public function set($key, $value)
	{
		$this->$key = $value;
		
		return $this;
	}
	
	public function set_handler($handler)
	{
		$this->PDF_handler = $handler;
		
		return $this;
	}
	
	protected function create_handler()
	{
		require_once Kohana::find_file('vendor', 'tcpdf/tcpdf');
		
		$PDF_handler = $this->PDF_handler;
		
		return new $PDF_handler(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	}
	
	public function get()
	{
		$pdf = $this->create_handler();
		
		// set document information
		$pdf->SetAuthor($this->author);
		$pdf->SetTitle($this->author);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP - 10, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		//$pdf->setCellPadding('5mm');

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', 'B', 10);

		// add a page
		$pdf->AddPage();

		$pdf->Write(0, $this->title, '', 0, 'C', true, 0, false, false, 0);

		$pdf->SetFont('helvetica', '', 8);
		
		//debug($this->view->render());
		//echo $this->view; die();
		
		$pdf->writeHTML($this->view, true, false, false, false, '');
		$response = $pdf->Output('', 'S');
		//debug($response);
		
		return $response;
	}
	
}
