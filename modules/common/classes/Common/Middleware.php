<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2015 ProCalidad
 */
abstract class Common_Middleware {
	
	/**
	 *
	 * @var  Controller
	 */
	protected $controller;
	
	public static function factory($name, Controller $controller)
	{
		$class = 'Middleware_'.$name;
		
		return new $class($controller);
	}
	
	public function __construct(Controller $controller)
	{
		$this->controller = $controller;
	}
	
	public abstract function execute();
	
}
