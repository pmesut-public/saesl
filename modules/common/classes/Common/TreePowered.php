<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @package    IS
 * @author     Sergio Melendez <helpse@gmail.com>
 * @copyright  (c) 2014 ProCalidad
 */
class Common_TreePowered {
	
	private $_data;
	
	private $_indicadores;
	
	public static function factory($data, $indicadores)
	{
		return new TreePowered($data, $indicadores);
	}
	
	public function __construct($data, $indicadores)
	{
		$this->_data = $data;
		$this->_indicadores = $indicadores;
	}
	
	public function get()
	{
		$data = $this->_data;
		
		$indicadores = $this->_indicadores;
		
		$result = array();
		
		foreach ($data as $row)
		{
			$inner = &$result;
			
			foreach ($indicadores as $indicador => $values)
			{
				$value = $row[$indicador];
				
				if ( ! isset($inner[$value]))
				{
					$inner[$value] = array();
					
					foreach ($values as $val)
					{
						$inner[$value][$val] = $row[$val];
						
						unset($row[$val]);
					}
					
					$inner[$value]['data'] = array();
				}
				
				$inner = &$inner[$value]['data'];
				
				unset($row[$indicador]);
			}
			
			$inner = $row;
		}
		
		return $result;
	}
}
